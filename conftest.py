# coding=utf-8
import pytest
from common import settings
from new_case.containerplatform.namespace.namespace import Namespace
from new_case.platform.project.project import Project
import json
import os

global_info = dict()


def input_file(content):
    """
    将global数据写入临时文件
    :param content: str
    :return:
    """
    file_path = settings.GLOBAL_INFO_FILE.split('/')[-2]
    os.makedirs(file_path, exist_ok=True)
    with open(settings.GLOBAL_INFO_FILE, 'w+') as write:
        write.write(json.dumps(content, indent=2))


@pytest.fixture(scope="session", autouse=True)
def prepare_and_clear():
    global_info = {
        "$REGION_NAME": settings.REGION_NAME,
        "$GLOBAL_REGION": settings.GLOBAL_REGION_NAME,
        "$K8S_NAMESPACE": settings.K8S_NAMESPACE,
        "$PROJECT_NAME": settings.PROJECT_NAME,
        "$IMAGE": settings.IMAGE,
        "$NODE_IPS": settings.VM_IPS,
        "$GLOBAL_ALB_NAME": settings.GLOBAL_ALB_NAME,
        "$DEFAULT_NS": settings.DEFAULT_NS
    }
    input_file(global_info)
    project = Project()
    data = {
        "project_name": settings.PROJECT_NAME,
        "regions": [settings.REGION_NAME],
        "display_name": settings.PROJECT_NAME,
        "description": "e2e test project",
    }
    create_project_result = project.create_project("./test_data/project/create_project.jinja2", data)
    assert create_project_result.status_code in (201, 409), "创建项目失败"
    namespace = Namespace()
    ns_data = {
        "namespace_name": settings.K8S_NAMESPACE,
        "display_name": settings.K8S_NAMESPACE,
        "cluster": settings.REGION_NAME,
        "project": settings.PROJECT_NAME,
        "ResourceQuota": "False",
        "morelabel": "False"
    }
    create_ns_result = namespace.create_namespace('./test_data/namespace/create_namespace.jinja2', ns_data,
                                                  region_name=settings.REGION_NAME)
    assert create_ns_result.status_code in (200, 409, 500), "创建新命名空间失败 {}".format(create_ns_result.text)
    yield
    os.remove(settings.GLOBAL_INFO_FILE)
    if not os.listdir(settings.GLOBAL_INFO_FILE.split('/')[-2]):
        os.rmdir(settings.GLOBAL_INFO_FILE.split('/')[-2])


def pytest_collection_modifyitems(items):
    """
    测试用例收集完成时，将收集到的item的name和nodeid的中文显示在控制台上
    :return:
    """
    for item in items:
        names = item.name.split("[")
        nodeids = item.nodeid.split("[")
        if len(names) > 1:
            item.name = names[0] + "[" + names[-1].encode("utf-8").decode("unicode_escape")
        if len(nodeids) > 1:
            # 报告里用例名称用的是nodeis所以nodeid也需要转换一下
            item._nodeid = nodeids[0] + "[" + nodeids[-1].encode("utf-8").decode("unicode_escape")
