import json
import sys
from time import time
from common import settings
from common.base_request import Common
from common.log import logger
from time import sleep


class Monitoring(Common):
    def get_period_time_metric_url(self):
        return 'v1/metrics/{}/query_range'.format(settings.REGION_NAME)

    def get_single_point_time_metric_url(self):
        return 'v1/metrics/{}/query'.format(settings.REGION_NAME)

    def get_list_node_url(self):
        return 'kubernetes/{}/api/v1/nodes'.format(settings.REGION_NAME)

    def get_period_time_metric(self, file, data):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_period_time_metric_url()
        times = self.generate_time_params()
        data = self.generate_data(file, data)
        data = json.loads(data)
        data['start'] = times['start_time']
        data['end'] = times['end_time']
        return self.send(method='post', path=url, data=json.dumps(data), params={})

    def get_period_time_metric_jinja2(self, file, data):
        count = 0
        data = self.generate_jinja_data(file, data)
        while count < 40:
            url = self.get_period_time_metric_url()
            times = self.generate_time_params()
            count += 1
            data.update({"start": times['start_time']})
            data.update({"end": times['end_time']})
            result = self.send(method='post', path=url, json=data, params={})
            if result.status_code == 200 and len(result.json()) > 0:
                break
            sleep(3)
        return result

    def get_single_point_time_metri(self, file, data):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_single_point_time_metric_url()
        data = self.generate_data(file, data)
        data = json.loads(data)
        data['time'] = int(time())
        return self.send(method='post', path=url, data=json.dumps(data), params={})

    def get_list_node(self):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_list_node_url()
        return self.send(method='get', path=url, params={})
