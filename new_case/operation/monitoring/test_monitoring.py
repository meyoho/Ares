import uuid
import pytest
from common.settings import RERUN_TIMES, REGION_NAME
from new_case.operation.monitoring.monitoring import Monitoring


@pytest.mark.prometheus
@pytest.mark.BAT
@pytest.mark.acp_operation
@pytest.mark.monitor
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestMonitoringSuite(object):
    def setup_class(self):
        self.monitoring = Monitoring()
        # 获取集群主机ip
        results = self.monitoring.get_list_node()
        assert results.status_code == 200, "获取主机列表失败:{}".format(results.text)
        self.node_ip = self.monitoring.get_value(results.json(), 'items.0.status.addresses.0.address')

    def 测试集群指标一段时间监控获取(self):
        # get cluster's a period time metric
        query_data = {
            "$id1": str(uuid.uuid1()),
            "$id2": str(uuid.uuid1()),
            "$id3": str(uuid.uuid1()),
            "$id4": str(uuid.uuid1()),
            "$region-name": REGION_NAME
        }
        get_result = self.monitoring.get_period_time_metric(
            "./test_data/monitoring/get_period_time_cluster_metric.json",
            query_data)
        assert get_result.status_code == 200, "集群指标一段时间监控获取失败:{}".format(get_result.text)
        assert len(get_result.json()) >= 4, "集群指标一段时间监控返回数据是空的"

    def 测试集群指标某个时间点监控获取(self):
        # get cluster's a single point time metric
        query_data = {
            "$id1": str(uuid.uuid1()),
            "$id2": str(uuid.uuid1()),
            "$id3": str(uuid.uuid1()),
            "$id4": str(uuid.uuid1()),
            "$region-name": REGION_NAME
        }
        get_result = self.monitoring.get_single_point_time_metri(
            "./test_data/monitoring/get_single_point_cluster_metric.json",
            query_data)
        assert get_result.status_code == 200, "集群指标某个时间点监控获取失败:{}".format(get_result.text)
        assert len(get_result.json()) == 4, "集群指标某个时间点监控返回是空的"

    def 测试主机指标一段时间监控获取(self):
        # get cluster's a period time metric
        query_data = {
            "$id1": str(uuid.uuid1()),
            "$id2": str(uuid.uuid1()),
            "$id3": str(uuid.uuid1()),
            "$id4": str(uuid.uuid1()),
            "$node-port": self.node_ip + ":.*",
            "$node-ip": self.node_ip
        }
        get_result = self.monitoring.get_period_time_metric(
            "./test_data/monitoring/query_node_monitoring.json",
            query_data)
        assert get_result.status_code == 200, "主机指标一段时间监控获取失败:{}".format(get_result.text)
        assert len(get_result.json()) >= 4, "主机指标一段时间监控返回是空的"
