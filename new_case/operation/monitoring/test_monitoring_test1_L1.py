import uuid
import pytest
from common.settings import RERUN_TIMES, K8S_NAMESPACE, RESOURCE_PREFIX, PROJECT_NAME, IMAGE
from new_case.operation.monitoring.monitoring import Monitoring
from new_case.containerplatform.application.application import Application
from common.delayed_assert import expect, assert_expectations


@pytest.mark.prometheus
@pytest.mark.Regression
@pytest.mark.acp_operation
@pytest.mark.monitor
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestMonitoringSuite(object):
    def setup_class(self):
        self.monitoring = Monitoring()
        self.newapp = Application()
        self.app_name = "{}-ares-monitoring-deploy".format(RESOURCE_PREFIX)
        # 获取集群主机ip
        results = self.monitoring.get_list_node()
        assert results.status_code == 200, "获取主机列表失败:{}".format(results.text)
        self.node_ip = self.monitoring.get_value(results.json(), 'items.0.status.addresses.0.address') + ':.*'
        self.teardown_class(self)
        data = {
            'project_name': PROJECT_NAME,
            'namespace': K8S_NAMESPACE,
            'app_name': self.app_name,
            'deployment_name': self.app_name,
            'image': IMAGE,
            'replicas': 1
        }
        create_result = self.newapp.create_app(
            './test_data/newapp/create_app.jinja2', data
        )
        assert create_result.status_code == 200, "新版应用创建失败 {}".format(create_result.text)
        app_status = self.newapp.get_app_status(K8S_NAMESPACE, self.app_name, 'Running')
        assert app_status, "创建应用后，验证应用状态出错：app: {} is not running".format(self.app_name)

    def teardown_class(self):
        self.newapp.delete_app(K8S_NAMESPACE, self.app_name)
        self.newapp.check_exists(self.newapp.common_app_url(K8S_NAMESPACE, self.app_name), 404)

    def 测试不同指标一段时间监控获取(self):
        # get cluster's a period time metric
        data_List = [
            {
                "aggregator": "avg",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "'cluster.cpu.utilization'"},
                                 {"name": "kind", "value": "Cluster"}]
            },
            {
                "aggregator": "max",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "'cluster.memory.utilization'"},
                                 {"name": "kind", "value": "Cluster"}]
            },
            {
                "aggregator": "min",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "'cluster.node.total.count'"},
                                 {"name": "kind", "value": "Cluster"}]
            },
            {
                "aggregator": "min",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "'cluster.resource.request.cpu.utilization'"},
                                 {"name": "kind", "value": "Cluster"}]
            },
            {
                "aggregator": "avg",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "'cluster.resource.request.memory.utilization'"},
                                 {"name": "kind", "value": "Cluster"}]
            },
            {
                "aggregator": "max",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "'cluster.node.not.ready.count'"},
                                 {"name": "kind", "value": "Cluster"}]
            },
            {
                "aggregator": "avg",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "'node.cpu.utilization'"},
                                 {"name": "name", "value": self.node_ip}, {"name": "kind", "value": "Node"}]
            },
            {
                "aggregator": "max",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "'node.memory.utilization'"},
                                 {"name": "name", "value": self.node_ip}, {"name": "kind", "value": "Node"}]
            },
            {
                "aggregator": "min",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "'node.network.transmit_bytes'"},
                                 {"name": "name", "value": self.node_ip}, {"name": "kind", "value": "Node"}]
            },
            {
                "aggregator": "avg",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "'node.disk.read.bytes'"},
                                 {"name": "name", "value": self.node_ip}, {"name": "kind", "value": "Node"}]
            },
            {
                "aggregator": "max",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "'node.resource.request.memory.utilization'"},
                                 {"name": "name", "value": self.node_ip[0:-3]}, {"name": "kind", "value": "Node"}]
            },
            {
                "aggregator": "min",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "'node.disk.written.bytes'"},
                                 {"name": "name", "value": self.node_ip}, {"name": "kind", "value": "Node"}]
            },
            {
                "aggregator": "avg",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "'node.resource.request.cpu.utilization'"},
                                 {"name": "name", "value": self.node_ip[0:-3]}, {"name": "kind", "value": "Node"}]
            },
            {
                "aggregator": "max",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "'node.load.15'"},
                                 {"name": "name", "value": self.node_ip}, {"name": "kind", "value": "Node"}]
            },
            {
                "aggregator": "min",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "'node.load.1'"},
                                 {"name": "name", "value": self.node_ip}, {"name": "kind", "value": "Node"}]
            },
            {
                "aggregator": "avg",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "'node.load.5'"},
                                 {"name": "name", "value": self.node_ip}, {"name": "kind", "value": "Node"}]
            },
            {
                "aggregator": "max",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "'node.disk.io.time'"},
                                 {"name": "name", "value": self.node_ip}, {"name": "kind", "value": "Node"}]
            },
            {
                "aggregator": "min",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "'node.network.receive_bytes'"},
                                 {"name": "name", "value": self.node_ip}, {"name": "kind", "value": "Node"}]
            },
            {
                "aggregator": "avg",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "'node.disk.utilization'"},
                                 {"name": "name", "value": self.node_ip}, {"name": "kind", "value": "Node"}]
            },
            {
                "aggregator": "max",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "'node.disk.utilization'"},
                                 {"name": "name", "value": self.node_ip}, {"name": "kind", "value": "Node"}]
            },
            {
                "aggregator": "avg",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "workload.cpu.utilization"},
                                 {"name": "namespace", "value": K8S_NAMESPACE},
                                 {"name": "kind", "value": "Deployment"},
                                 {"name": "name", "value": self.app_name}
                                 ]
            },
            {
                "aggregator": "max",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "workload.network.transmit_bytes"},
                                 {"name": "namespace", "value": K8S_NAMESPACE},
                                 {"name": "kind", "value": "Deployment"},
                                 {"name": "name", "value": self.app_name}
                                 ]
            },
            {
                "aggregator": "min",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "workload.memory.utilization"},
                                 {"name": "namespace", "value": K8S_NAMESPACE},
                                 {"name": "kind", "value": "Deployment"},
                                 {"name": "name", "value": self.app_name}
                                 ]
            },
            {
                "aggregator": "avg",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "workload.network.receive_bytes"},
                                 {"name": "namespace", "value": K8S_NAMESPACE},
                                 {"name": "kind", "value": "Deployment"},
                                 {"name": "name", "value": self.app_name}
                                 ]
            },
            {
                "aggregator": "max",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "pod.memory.utilization"},
                                 {"name": "namespace", "value": K8S_NAMESPACE},
                                 {"name": "kind", "value": "Deployment"},
                                 {"name": "name", "value": self.app_name},
                                 {"name": "pod_name", "value": self.app_name}],
                "group_by": "pod_name"
            },
            {
                "aggregator": "min",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "pod.cpu.utilization"},
                                 {"name": "namespace", "value": K8S_NAMESPACE},
                                 {"name": "kind", "value": "Deployment"},
                                 {"name": "name", "value": self.app_name},
                                 {"name": "pod_name", "value": self.app_name}],
                "group_by": "pod_name"
            },
            {
                "aggregator": "avg",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "pod.network.receive_bytes"},
                                 {"name": "namespace", "value": K8S_NAMESPACE},
                                 {"name": "kind", "value": "Deployment"},
                                 {"name": "name", "value": self.app_name},
                                 {"name": "pod_name", "value": self.app_name}],
                "group_by": "pod_name"
            },
            {
                "aggregator": "max",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "pod.network.transmit_bytes"},
                                 {"name": "namespace", "value": K8S_NAMESPACE},
                                 {"name": "kind", "value": "Deployment"},
                                 {"name": "name", "value": self.app_name},
                                 {"name": "pod_name", "value": self.app_name}],
                "group_by": "pod_name"
            },
            {
                "aggregator": "min",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "container.cpu.utilization"},
                                 {"name": "namespace", "value": K8S_NAMESPACE},
                                 {"name": "kind", "value": "Deployment"},
                                 {"name": "name", "value": self.app_name},
                                 {"name": "pod_name", "value": self.app_name}],
                "group_by": "container_name"
            },
            {
                "aggregator": "max",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "container.memory.utilization"},
                                 {"name": "namespace", "value": K8S_NAMESPACE},
                                 {"name": "kind", "value": "Deployment"},
                                 {"name": "name", "value": self.app_name},
                                 {"name": "pod_name", "value": self.app_name}],
                "group_by": "container_name"
            },
            {
                "aggregator": "avg",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "container.network.receive_bytes"},
                                 {"name": "namespace", "value": K8S_NAMESPACE},
                                 {"name": "kind", "value": "Deployment"},
                                 {"name": "name", "value": self.app_name},
                                 {"name": "pod_name", "value": self.app_name}],
                "group_by": "container_name"
            },
            {
                "aggregator": "max",
                "id": str(uuid.uuid1()),
                "query_labels": [{"name": "__name__", "value": "container.network.transmit_bytes"},
                                 {"name": "namespace", "value": K8S_NAMESPACE},
                                 {"name": "kind", "value": "Deployment"},
                                 {"name": "name", "value": self.app_name},
                                 {"name": "pod_name", "value": self.app_name}],
                "group_by": "container_name"
            }
        ]
        for data in data_List:
            get_result = self.monitoring.get_period_time_metric_jinja2(
                "./test_data/monitoring/monitoring.jinja2", data)
            expect(get_result.status_code == 200, "指标{}监控获取失败:{}".format(data, get_result.text))
            if get_result.status_code != 200:
                continue
            expect(len(get_result.json()) > 0 and
                   self.monitoring.get_value(get_result.json(), '0.metric.__query_id__') == data["id"], "指标{}监控返回数据不正确".format(data))
        assert_expectations()
