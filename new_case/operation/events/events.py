import sys
from common.base_request import Common
from common.log import logger


class Events(Common):

    def get_kevents_url(self):
        return "v4/events"

    def resource_kevent_pagination(self, payload, url, query="items.0"):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        payload.update({"size": 1, "pageno": 1, "fuzzy": "false"})
        page1 = self.send(method="GET", path=url, params=payload)
        if page1.status_code != 200:
            return False
        logger.info("列表1的结果:{}".format(page1.json()))
        name1 = self.get_value(page1.json(), query)
        payload.update({"size": 1, "pageno": 2, "fuzzy": "false"})
        page2 = self.send(method="GET", path=url, params=payload)
        if page2.status_code != 200:
            return False
        logger.info("列表2的结果:{}".format(page2.json()))
        name2 = self.get_value(page2.json(), query)
        if name1 == name2:
            return False
        return True
