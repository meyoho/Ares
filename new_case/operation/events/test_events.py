import pytest
from new_case.operation.events.events import Events
from common.settings import RERUN_TIMES, K8S_NAMESPACE


@pytest.mark.hedwig
@pytest.mark.BAT
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestEventSuite(object):

    def setup_class(self):
        self.events = Events()
        self.ns_name = K8S_NAMESPACE

    def 测试获取k8s事件(self):
        # 获取默认过去30分钟k8s事件
        payload = {"cluster": self.events.region_name, "page": 1, "page_size": 20}
        kevents_results = self.events.get_kevents(payload)
        assert kevents_results, "获取默认过去30分钟k8s事件返回体是空的"
