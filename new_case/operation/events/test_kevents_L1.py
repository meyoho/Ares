import pytest
from new_case.operation.events.events import Events
from common.settings import RERUN_TIMES, K8S_NAMESPACE, REGION_NAME, RESOURCE_PREFIX, PROJECT_NAME, IMAGE
from new_case.containerplatform.application.application import Application


@pytest.mark.hedwig
@pytest.mark.Regression
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=0)
class TestEventSuiteL1(object):

    def setup_class(self):
        self.events = Events()
        self.newapp = Application()
        self.app_name = "{}-ares-kevents-deploy".format(RESOURCE_PREFIX)
        self.teardown_class(self)
        data = {
            'project_name': PROJECT_NAME,
            'namespace': K8S_NAMESPACE,
            'app_name': self.app_name,
            'deployment_name': self.app_name,
            'image': IMAGE,
            'replicas': 1
        }
        create_result = self.newapp.create_app(
            './test_data/newapp/create_app.jinja2', data
        )
        assert create_result.status_code == 200, "新版应用创建失败 {}".format(create_result.text)
        app_status = self.newapp.get_app_status(K8S_NAMESPACE, self.app_name, 'Running')
        assert app_status, "创建应用后，验证应用状态出错：app: {} is not running".format(self.app_name)

    def teardown_class(self):
        self.newapp.delete_app(K8S_NAMESPACE, self.app_name)

    def 测试获取k8s事件_必填参数_带可选参数name(self):
        payload = {"cluster": REGION_NAME, "name": self.app_name, "page": 1, "page_size": 20, "fuzzy": False}
        kevents_results = self.events.get_kevents(payload)
        assert kevents_results, "获取k8s事件_必填参数_带可选参数name失败"

    def 测试获取k8s事件_必填参数_带可选参数kind(self):
        payload = {"cluster": REGION_NAME,
                   "kind": "Deployment,Application,Pod,EndPoint,Service,HorizontalPodAutoscaler,ReplicaSet,DaemonSet,StatefulSet",
                   "page": 1, "page_size": 20}
        kevents_results = self.events.get_kevents(payload)
        assert kevents_results, "获取k8s事件_必填参数_带可选参数kind失败"

    def 测试获取k8s事件_必填参数_带可选参数namespace(self):
        payload = {"cluster": REGION_NAME, "namespace": K8S_NAMESPACE, "page": 1, "page_size": 20}
        kevents_results = self.events.get_kevents(payload)
        assert kevents_results, "获取k8s事件_必填参数_带可选参数namespace失败"

    def 测试k8s事件面板分页失败(self):
        url = self.events.get_kevents_url()
        payload = {"cluster": REGION_NAME}
        payload.update(self.events.generate_time_params())
        get_result = self.events.resource_kevent_pagination(payload, url)
        assert get_result, "k8s事件面板分页测试失败"
