import pytest
from common.check_status import checker
from common.settings import RERUN_TIMES, K8S_NAMESPACE, DEFAULT_NS, PROJECT_NAME
from new_case.operation.alarm_template.alarm_template import Alarm_Template
from new_case.operation.alarm_template.conftest import update_data_List, L1_template_name
from new_case.operation.alarm.alarm import Alarm


@pytest.mark.mars
@pytest.mark.Regression
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=0)
@pytest.mark.skipif(checker.smtp_availabel() is False, reason="邮箱服务器无法访问")
class TestAlarm_TemplateSuiteCreatealarm():
    def setup_class(self):
        self.alarm_template = Alarm_Template()
        self.alarm = Alarm()

    @pytest.mark.parametrize("data", update_data_List, ids=L1_template_name)
    def 测试不同类型告警模板创建告警(self, data, Prepare_Template):
        ns_name = K8S_NAMESPACE if data['kind'] == "workload" else DEFAULT_NS
        if data['kind'] == "node":
            data.update({"resource_name": Prepare_Template["node_name"]})
        elif data['kind'] == "workload":
            data.update({"resource_name": Prepare_Template["app_name"], "application": Prepare_Template["app_name"],
                         "kind": "Deployment", "project": PROJECT_NAME})
        if data['description'] == "alarm-action":
            data.update({"test_notifications": [
                {"name": Prepare_Template["notification_name"],
                 "namespace": Prepare_Template["notification_namespace"]}]})
        data.update(
            {
                "kind": data["kind"].capitalize(),
                "ns_name": ns_name,
            })

        for item in data["rules"]:
            n_data = {
                "application": data.get("application", ""),
                "resource_name": data["resource_name"],
                "cluster_name": data["cluster_name"],
                "kind": data["kind"],
                "ns_name": data["ns_name"],
                "project": data.get("project", ""),
                "threshold": str(item["threshold"])
            }
            if data['kind'] != "workload":
                n_data.pop('application')
                n_data.pop('project')
            item.update(n_data)
            item.update({"expr": self.alarm.get_alarm_expr(item, Prepare_Template)})
        if data.get('resourceVersion'):
            del data['resourceVersion']
        create_result = self.alarm.create_alarm_jinja2("./test_data/alarm/alarm.jinja2", data, ns_name)
        assert create_result.status_code == 201, "模板创建告警{}失败:{}".format(data["alarm_name"], create_result.text)
        value = self.alarm.generate_jinja_data("verify_data/alarm/verify_alarm.jinja2", data)
        assert self.alarm.is_sub_dict(value, create_result.json()), \
            "模板创建告警{}比对数据失败，返回数据:{},期望数据:{}".format(data["alarm_name"], create_result.json(), value)
