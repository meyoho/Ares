import pytest
from common.check_status import checker
from common.settings import RERUN_TIMES, K8S_NAMESPACE, DEFAULT_NS, USERNAME, GLOBAL_REGION_NAME, AUDIT_UNABLED
from new_case.operation.alarm_template.alarm_template import Alarm_Template
from new_case.operation.alarm.alarm import Alarm
from new_case.operation.alarm_template.conftest import update_data_List, L1_template_name


@pytest.mark.mars
@pytest.mark.Regression
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=0)
@pytest.mark.skipif(checker.smtp_availabel() is False, reason="邮箱服务器无法访问")
class TestAlarm_TemplateSuiteDelete():

    def setup_class(self):
        self.alarm_template = Alarm_Template()
        self.alarm = Alarm()

    @pytest.mark.parametrize("data", update_data_List, ids=L1_template_name)
    def 测试不同类型告警模板删除(self, data):
        delete_result = self.alarm_template.delete_alarm_template(data["alarm_template_name"])
        assert delete_result.status_code == 200, "删除告警模板{}失败:{}".format(data["alarm_template_name"],
                                                                        delete_result.text)
        assert self.alarm_template.check_exists(self.alarm_template.get_alarm_template_url(data["alarm_template_name"]),
                                                404, params="")
        ns_name = K8S_NAMESPACE if data['kind'] == "Deployment" else DEFAULT_NS
        delete_result = self.alarm.delete_alarm(data['alarm_name'], ns_name)
        assert delete_result.status_code == 200, "删除模板创建告警{}失败:{}".format(data['alarm_name'], delete_result.text)
        assert self.alarm.check_exists(self.alarm.get_alarm_url(ns_name, data['alarm_name']), 404,
                                       params="")

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 测试告警模板删除审计(self):
        payload = {"user_name": USERNAME, "operation_type": "delete", "resource_type": "alerttemplates",
                   "resource_name": update_data_List[0]['alarm_template_name']}
        result = self.alarm_template.search_audit(payload)
        payload.update({"namespace": "", "region_name": GLOBAL_REGION_NAME})
        values = self.alarm_template.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.alarm_template.is_sub_dict(values, result.json()), "审计数据不符合预期"
