import pytest
from common.settings import RERUN_TIMES
from new_case.operation.alarm_template.alarm_template import Alarm_Template
from new_case.operation.alarm_template.conftest import update_data_List, L1_template_name


@pytest.mark.mars
@pytest.mark.Regression
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=0)
class TestAlarm_TemplateSuiteGet():
    def setup_class(self):
        self.alarm_template = Alarm_Template()

    @pytest.mark.parametrize("data", update_data_List, ids=L1_template_name)
    def 测试获取告警模板列表_带参数name(self, data):
        get_result = self.alarm_template.search_alarm_template(data["alarm_template_name"])
        assert get_result.status_code == 200, \
            "获取告警模板列表_名称{}失败:{}".format(data["alarm_template_name"], get_result.text)
