import pytest
from common.check_status import checker
from common.settings import RERUN_TIMES, CASE_TYPE, DEFAULT_NS
from new_case.operation.alarm_template.alarm_template import Alarm_Template
from new_case.operation.alarm.alarm import Alarm
from new_case.operation.alarm_template.conftest import create_L0_data, update_L0_List


@pytest.mark.mars
@pytest.mark.BAT
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=0)
@pytest.mark.skipif(checker.smtp_availabel() is False, reason="邮箱服务器无法访问")
class TestAlarm_TemplateSuite(object):
    def setup_class(self):
        self.alarm_template = Alarm_Template()
        self.alarm = Alarm()
        self.teardown_class(self)

    def teardown_class(self):
        if CASE_TYPE not in ("prepare", "upgrade", "delete"):
            self.alarm_template.delete_alarm_template(create_L0_data['alarm_template_name'])
            self.alarm.delete_alarm(update_L0_List['alarm_name'], DEFAULT_NS)

    @pytest.mark.prepare
    def 测试告警模板创建(self, Prepare_Template):
        # create alarm template
        create_L0_data.update({
            "test_notifications": [{"name": Prepare_Template["notification_name"],
                                    "namespace": Prepare_Template["notification_namespace"]}]})
        create_result = self.alarm_template.create_alarm_template_jinja2(
            "./test_data/alarm_template/alarm_template.jinja2", create_L0_data)
        assert create_result.status_code == 201, "创建告警模板失败:{}".format(create_result.text)
        value = self.alarm_template.generate_jinja_data(
            "verify_data/alarm_template/verify_alarm_template.jinja2", create_L0_data)
        assert self.alarm_template.is_sub_dict(value, create_result.json()), \
            "创建类型是集群的告警模板比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

    @pytest.mark.upgrade
    def 测试获取告警模板详情页(self, Prepare_Template):
        # get alarm detail
        create_L0_data.update({
            "test_notifications": [{"name": Prepare_Template["notification_name"],
                                    "namespace": Prepare_Template["notification_namespace"]}]})
        detail_result = self.alarm_template.get_alarm_template_detail(create_L0_data['alarm_template_name'])
        assert detail_result.status_code == 200, "获取告警模板详情页失败:{}".format(detail_result.text)
        value = self.alarm_template.generate_jinja_data(
            "verify_data/alarm_template/verify_alarm_template.jinja2", create_L0_data)
        assert self.alarm_template.is_sub_dict(value, detail_result.json()), \
            "类型是集群的告警模板详情页比对数据失败，返回数据:{},期望数据:{}".format(detail_result.json(), value)

    @pytest.mark.upgrade
    def 测试告警模板列表页(self, Prepare_Template):
        # get alarm list
        create_L0_data.update({
            "test_notifications": [{"name": Prepare_Template["notification_name"],
                                    "namespace": Prepare_Template["notification_namespace"]}]})
        list_result = self.alarm_template.list_alarm_template()
        assert list_result.status_code == 200, "获取告警模板列表页失败:{}".format(list_result.text)
        assert create_L0_data['alarm_template_name'] in list_result.text, "创建的告警模板在告警列表中"
        content = self.alarm_template.get_k8s_resource_data(list_result.json(), create_L0_data['alarm_template_name'],
                                                            list_key="items")
        value = self.alarm_template.generate_jinja_data(
            "verify_data/alarm_template/verify_alarm_template.jinja2", create_L0_data)
        assert self.alarm_template.is_sub_dict(value, content), \
            "告警模板列表比对数据失败，返回数据:{},期望数据:{}".format(content, value)

    @pytest.mark.upgrade
    def 测试告警模板更新(self, Prepare_Template):
        # update alarm
        update_L0_List.update({
            "resourceVersion": self.alarm_template.get_resourceVersion(update_L0_List['alarm_template_name']),
            "test_notifications": [{"name": Prepare_Template["notification_name"],
                                    "namespace": Prepare_Template["notification_namespace"]}]})
        update_result = self.alarm_template.update_alarm_template_jinja2(
            update_L0_List['alarm_template_name'], "./test_data/alarm_template/alarm_template.jinja2", update_L0_List)
        assert update_result.status_code == 200, "更新告警模板失败:{}".format(update_result.text)
        update_L0_List.update(
            {"resourceVersion": self.alarm_template.get_resourceVersion(update_L0_List['alarm_template_name'])})
        value = self.alarm_template.generate_jinja_data(
            "verify_data/alarm_template/verify_alarm_template.jinja2", update_L0_List)
        assert self.alarm_template.is_sub_dict(value, update_result.json()), \
            "更新告警模板比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

    @pytest.mark.upgrade
    def 测试模板创建告警(self, Prepare_Template):
        # template create alarm
        del (update_L0_List["resourceVersion"])
        update_L0_List.update(
            {"test_notifications": [{"name": Prepare_Template["notification_name"],
                                     "namespace": Prepare_Template["notification_namespace"]}],
             "kind": update_L0_List["kind"].capitalize()
             })
        for item in update_L0_List["rules"]:
            item.update({
                "resource_name": update_L0_List["resource_name"],
                "cluster_name": update_L0_List["cluster_name"],
                "kind": update_L0_List["kind"],
                "ns_name": update_L0_List["ns_name"],
                "threshold": str(item["threshold"])
            })
            item.update({"expr": self.alarm.get_alarm_expr(item, Prepare_Template)})
        create_result = self.alarm.create_alarm_jinja2("./test_data/alarm/alarm.jinja2", update_L0_List, DEFAULT_NS)
        assert create_result.status_code == 201, "模板创建告警失败:{}".format(create_result.text)
        value = self.alarm.generate_jinja_data("verify_data/alarm/verify_alarm.jinja2", update_L0_List)
        assert self.alarm.is_sub_dict(value, create_result.json()), \
            "模板创建告警比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

    @pytest.mark.upgrade
    def 测试告警模板删除(self):
        # delete alarm
        delete_result = self.alarm_template.delete_alarm_template(create_L0_data['alarm_template_name'])
        assert delete_result.status_code == 200, "删除告警模板失败:{}".format(delete_result.text)
        assert self.alarm_template.check_exists(
            self.alarm_template.get_alarm_template_url(create_L0_data['alarm_template_name']), 404, params="")

    @pytest.mark.delete
    def 测试模板创建告警删除(self):
        # template create alarm delete
        delete_result = self.alarm.delete_alarm(update_L0_List['alarm_name'], DEFAULT_NS)
        assert delete_result.status_code == 200, "删除模板创建告警失败:{}".format(delete_result.text)
        assert self.alarm.check_exists(self.alarm.get_alarm_url(DEFAULT_NS, update_L0_List['alarm_name']), 404,
                                       params="")
