import pytest
from common.check_status import checker
from common.settings import RERUN_TIMES, USERNAME, GLOBAL_REGION_NAME, AUDIT_UNABLED
from new_case.operation.alarm_template.alarm_template import Alarm_Template
from new_case.operation.alarm_template.conftest import create_data_List, L1_template_name


@pytest.mark.mars
@pytest.mark.Regression
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=0)
@pytest.mark.skipif(checker.smtp_availabel() is False, reason="邮箱服务器无法访问")
class TestAlarm_TemplateSuitePost():
    def setup_class(self):
        self.alarm_template = Alarm_Template()

    @pytest.mark.parametrize("data", create_data_List, ids=L1_template_name)
    def 测试不同类型告警模板创建(self, data, Prepare_Template):
        # 删除资源
        self.alarm_template.delete_alarm_template(data["alarm_template_name"])
        if data["description"] == "alarm-action":
            data.update({
                "test_notifications": [{"name": Prepare_Template["notification_name"],
                                        "namespace": Prepare_Template["notification_namespace"]}]
            })
        create_result = self.alarm_template.create_alarm_template_jinja2(
            "./test_data/alarm_template/alarm_template.jinja2", data)
        assert create_result.status_code == 201, "创建告警模板{}失败:{}".format(data['alarm_template_name'],
                                                                        create_result.text)
        value = self.alarm_template.generate_jinja_data(
            "verify_data/alarm_template/verify_alarm_template.jinja2", data)
        assert self.alarm_template.is_sub_dict(value, create_result.json()), \
            "创建告警模板{}比对数据失败，返回数据:{},期望数据:{}".format(data['alarm_template_name'], create_result.json(), value)

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 测试告警模板创建审计(self):
        payload = {"user_name": USERNAME, "operation_type": "create", "resource_type": "alerttemplates",
                   "resource_name": create_data_List[0]['alarm_template_name']}
        result = self.alarm_template.search_audit(payload)
        payload.update({"namespace": "", "region_name": GLOBAL_REGION_NAME, "code": 201})
        values = self.alarm_template.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.alarm_template.is_sub_dict(values, result.json()), "审计数据不符合预期"
