import sys
from common.base_request import Common
from common.log import logger


class Alarm_Template(Common):
    def get_create_alarm_template_url(self):
        return 'apis/aiops.alauda.io/v1beta1/alerttemplates'

    def get_alarm_template_url(self, alarm_template_name):
        return 'apis/aiops.alauda.io/v1beta1/alerttemplates/{}'.format(alarm_template_name)

    def create_alarm_template_jinja2(self, file, data):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_create_alarm_template_url()
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=url, json=data, params={})

    def update_alarm_template_jinja2(self, alarm_template_name, file, data):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_alarm_template_url(alarm_template_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='PUT', path=url, json=data, params={})

    def get_alarm_template_detail(self, alarm_template_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_alarm_template_url(alarm_template_name)
        return self.send(method='GET', path=url, params={})

    def delete_alarm_template(self, alarm_template_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_alarm_template_url(alarm_template_name)
        return self.send(method='DELETE', path=url, params={})

    def list_alarm_template(self):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_create_alarm_template_url()
        payload = {"limit": 20}
        return self.send(method='GET', path=url, params=payload)

    def search_alarm_template(self, alarm_template_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_create_alarm_template_url()
        payload = {"limit": 20, "fieldSelector=metadata.name": alarm_template_name}
        return self.send(method='GET', path=url, params=payload)

    def get_alarm_rule_name(self, alarm_template_name):
        url = self.get_alarm_template_url(alarm_template_name)
        result = self.send(method='GET', path=url, params={})
        alarm_rule_name = self.get_value(result.json(), 'spec.templates.0.name')
        return alarm_rule_name

    def get_resourceVersion(self, alarm_template_name):
        url = self.get_alarm_template_url(alarm_template_name)
        response = self.send(method='GET', path=url, params={})
        resourceVersion = self.get_value(response.json(), 'metadata.resourceVersion')
        return resourceVersion
