import pytest
from common.check_status import checker
from common.settings import RERUN_TIMES, USERNAME, GLOBAL_REGION_NAME, AUDIT_UNABLED
from new_case.operation.alarm_template.alarm_template import Alarm_Template
from new_case.operation.alarm_template.conftest import update_data_List, L1_template_name


@pytest.mark.mars
@pytest.mark.Regression
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=0)
@pytest.mark.skipif(checker.smtp_availabel() is False, reason="邮箱服务器无法访问")
class TestAlarm_TemplateSuitePut():
    def setup_class(self):
        self.alarm_template = Alarm_Template()

    @pytest.mark.parametrize("data", update_data_List, ids=L1_template_name)
    def 测试不同类型告警模板更新(self, data, Prepare_Template):
        if data['description'] == 'alarm-action':
            data.update({
                "test_notifications": [{"name": Prepare_Template["notification_name"],
                                        "namespace": Prepare_Template["notification_namespace"]}]
            })
        data.update({
            "resourceVersion": self.alarm_template.get_resourceVersion(data['alarm_template_name'])
        })
        update_result = self.alarm_template.update_alarm_template_jinja2(
            data['alarm_template_name'], "./test_data/alarm_template/alarm_template.jinja2", data)
        assert update_result.status_code == 200, "更新告警模板{}失败:{}".format(data['alarm_template_name'],
                                                                        update_result.text)
        data.update({
            "resourceVersion": self.alarm_template.get_resourceVersion(data['alarm_template_name'])
        })
        value = self.alarm_template.generate_jinja_data(
            "verify_data/alarm_template/verify_alarm_template.jinja2", data)
        assert self.alarm_template.is_sub_dict(value, update_result.json()), \
            "更新告警模板{}比对数据失败，返回数据:{},期望数据:{}".format(data['alarm_template_name'], update_result.json(), value)

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 测试告警模板更新审计(self):
        payload = {"user_name": USERNAME, "operation_type": "update", "resource_type": "alerttemplates",
                   "resource_name": update_data_List[0]['alarm_template_name']}
        result = self.alarm_template.search_audit(payload)
        payload.update({"namespace": "", "region_name": GLOBAL_REGION_NAME})
        values = self.alarm_template.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.alarm_template.is_sub_dict(values, result.json()), "审计数据不符合预期"
