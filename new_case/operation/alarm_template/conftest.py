from time import sleep

import pytest
import base64
from common import settings
from common.check_status import checker
from common.settings import K8S_NAMESPACE, RESOURCE_PREFIX, DEFAULT_NS, PROJECT_NAME, IMAGE, REGION_NAME, NOTI_PWD, \
    NOTI_USERNAME
from new_case.operation.notification.notification_server.notification_server import Notification_Server
from new_case.operation.notification.notification_object.notification_object import Notification_Object
from new_case.operation.notification.email_sender.email_sender import Email_Sender
from new_case.operation.notification.notification_template.notification_template import Notification_Template
from new_case.operation.notification.new_notification.notification import Notification
from new_case.operation.monitoring.monitoring import Monitoring
from new_case.containerplatform.application.application import Application

create_data_List = [
    {
        "alarm_template_name": "{}-ares-region-template-1".format(settings.RESOURCE_PREFIX),
        "description": "''",
        "kind": "cluster",
        "rules": [{
            "rule_name": "{}-ares-region-1".format(settings.RESOURCE_PREFIX),
            "query_labels": [{"name": "__name__", "value": "'cluster.cpu.utilization'"}],
            "threshold": 1,
            "comparison": ">",
            "test_labels": {"aaa": "bbbbb", "cccccc": "cdcdcdcdcd", "severity": "High"}
        }]
    },
    {
        "alarm_template_name": "{}-ares-region-template-2".format(settings.RESOURCE_PREFIX),
        "kind": "cluster",
        "description": "region-description",
        "rules": [{
            "rule_name": "{}-ares-region-2".format(settings.RESOURCE_PREFIX),
            "query_labels": [{"name": "__name__", "value": "'cluster.memory.utilization'"}],
            "function": "avg",
            "_range": "60",
            "severity": "Critical",
            "comparison": ">",
            "threshold": 0.12
        }]
    },
    {
        "alarm_template_name": "{}-ares-region-template-3".format(settings.RESOURCE_PREFIX),
        "kind": "cluster",
        "description": "region-custom",
        "rules": [{
            "rule_name": "{}-ares-region-3".format(settings.RESOURCE_PREFIX),
            "query_labels": [{"name": "'__name__'", "value": "'custom'"},
                             {"name": "'expr'", "value": "'cluster_cpu_utilization'"}],
            "severity": "High",
            "comparison": "==",
            "threshold": 4.4444,
            "wait": "180"
        }]
    },
    {
        "alarm_template_name": "{}-ares-region-template-4".format(settings.RESOURCE_PREFIX),
        "kind": "cluster",
        "description": "region more rule",
        "rules": [
            {
                "rule_name": "{}-region-1".format(settings.RESOURCE_PREFIX),
                "query_labels": [{"name": "'__name__'", "value": "cluster.node.not.ready.count"}],
                "comparison": ">",
                "threshold": 10,
            },
            {
                "rule_name": "{}-region-2".format(settings.RESOURCE_PREFIX),
                "query_labels": [{"name": "'__name__'", "value": "cluster.node.not.ready.count"}],
                "comparison": ">",
                "threshold": 10,
                "function": "avg",
                "_range": "300"
            },
            {
                "rule_name": "{}-region-3".format(settings.RESOURCE_PREFIX),
                "query_labels": [{"name": "'__name__'", "value": "'custom'"},
                                 {"name": "'expr'", "value": "'custom-aadadadadadada'"}],
                "comparison": ">",
                "threshold": 1012
            }
        ]
    },
    {
        "alarm_template_name": "{}-ares-node-template-1".format(settings.RESOURCE_PREFIX),
        "kind": "node",
        "description": "''",
        "rules": [{
            "rule_name": "{}-ares-node-1".format(settings.RESOURCE_PREFIX),
            "query_labels": [{"name": "'__name__'", "value": "'node.cpu.utilization'"}],
            "severity": "Low",
            "threshold": 2,
            "comparison": "<=",
            "wait": "300"
        }]
    },
    {
        "alarm_template_name": "{}-ares-node-template-2".format(settings.RESOURCE_PREFIX),
        "kind": "node",
        "description": "node-description",
        "rules": [{
            "rule_name": "{}-ares-node-2".format(settings.RESOURCE_PREFIX),
            "query_labels": [{"name": "'__name__'", "value": "'node.memory.utilization'"}],
            "threshold": 2.22,
            "comparison": "<",
            "wait": "600",
            "function": "max",
            "_range": "120"
        }]
    },
    {
        "alarm_template_name": "{}-ares-node-template-3".format(settings.RESOURCE_PREFIX),
        "kind": "node",
        "description": "node-label",
        "rules": [{
            "rule_name": "{}-ares-node-3".format(settings.RESOURCE_PREFIX),
            "query_labels": [{"name": "'__name__'", "value": "'custom'"},
                             {"name": "'expr'", "value": "'node_cpu'"}],
            "threshold": 0,
            "comparison": "!=",
            "test_labels": {"test11": "test11", "severity": "'Low'"},
            "test_annotations": {"aaa": "bbbbb"},
            "wait": "1800"
        }]
    },
    {
        "alarm_template_name": "{}-ares-node-template-4".format(settings.RESOURCE_PREFIX),
        "kind": "node",
        "description": "node-more-rule",
        "rules": [
            {
                "rule_name": "{}-node-1".format(settings.RESOURCE_PREFIX),
                "query_labels": [{"name": "'__name__'", "value": "'node.network.transmit_bytes'"}],
                "threshold": 100,
                "comparison": ">",
            },
            {
                "rule_name": "{}-node-2".format(settings.RESOURCE_PREFIX),
                "query_labels": [{"name": "'__name__'", "value": "'node.load.1'"}],
                "threshold": 10,
                "comparison": ">",
                "function": "avg",
                "_range": "600"
            },
            {
                "rule_name": "{}-node-3".format(settings.RESOURCE_PREFIX),
                "query_labels": [{"name": "'__name__'", "value": "'custom'"},
                                 {"name": "'expr'", "value": "'test_aadadadadad'"}],
                "threshold": 10.12,
                "comparison": ">",
            }
        ]
    },
    {
        "alarm_template_name": "{}-ares-workload-template-1".format(settings.RESOURCE_PREFIX),
        "kind": "workload",
        "description": "''",
        "rules": [{
            "rule_name": "{}-ares-workload-1".format(settings.RESOURCE_PREFIX),
            "query_labels": [{"name": "'__name__'", "value": "'container.cpu.utilization'"}],
            "threshold": 0.11,
            "comparison": ">"
        }]
    },
    {
        "alarm_template_name": "{}-ares-workload-template-2".format(settings.RESOURCE_PREFIX),
        "kind": "workload",
        "description": "workload-description",
        "rules": [{
            "rule_name": "{}-ares-workload-2".format(settings.RESOURCE_PREFIX),
            "query_labels": [{"name": "'__name__'", "value": "'pod.cpu.utilization'"}],
            "function": "min",
            "_range": "180",
            "comparison": ">",
            "threshold": 6.66
        }]
    },
    {
        "alarm_template_name": "{}-ares-logalarm-template-6".format(settings.RESOURCE_PREFIX),
        "kind": "workload",
        "description": "log-alarm",
        "rules": [{
            "rule_name": "{}-ares-logalarm".format(settings.RESOURCE_PREFIX),
            "query_labels": [{"name": "'__name__'", "value": "'workload.log.keyword.count'"},
                             {"name": "'query'", "value": "'root hehe.txt'"}],
            "comparison": ">",
            "threshold": 30,
            "_range": "60",
            "query": "root hehe.txt"
        }]
    },
    {
        "alarm_template_name": "{}-ares-eventalarm-template-7".format(settings.RESOURCE_PREFIX),
        "kind": "workload",
        "description": "event-alarm",
        "rules": [{
            "rule_name": "{}-ares-eventalarm".format(settings.RESOURCE_PREFIX),
            "query_labels": [{"name": "'__name__'", "value": "'workload.event.reason.count'"},
                             {"name": "'query'", "value": "'Started'"}],
            "comparison": "<=",
            "threshold": 11,
            "_range": "120",
            "query": "Started",
        }]
    },
    {
        "alarm_template_name": "{}-ares-workload-template-5".format(settings.RESOURCE_PREFIX),
        "kind": "workload",
        "description": "alarm-action",
        "rules": [{
            "rule_name": "{}-ares-workload-5".format(settings.RESOURCE_PREFIX),
            "query_labels": [{"name": "'__name__'", "value": "'custom'"},
                             {"name": "'exp'", "value": "'container_cpu_utilization'"}],
            "comparison": ">",
            "threshold": 12,
            "wait": "600",
            "test_labels": {"cdcd": " cdcdcdc", "severity": "'Low'"},
            "test_annotations": {"cdc": "vfvf"}
        }]
    },
    {
        "alarm_template_name": "{}-ares-workload-template-6".format(settings.RESOURCE_PREFIX),
        "kind": "workload",
        "description": "workload more rule",
        "rules": [
            {
                "rule_name": "{}-workload-1".format(settings.RESOURCE_PREFIX),
                "query_labels": [{"name": "'__name__'", "value": "'container.cpu.utilization'"}],
                "comparison": ">",
                "threshold": 14,
            },
            {
                "rule_name": "{}-workload-2".format(settings.RESOURCE_PREFIX),
                "query_labels": [{"name": "'__name__'", "value": "'container.network.transmit_bytes'"}],
                "comparison": ">",
                "threshold": 56,
                "function": "avg",
                "_range": "1800"
            },
            {
                "rule_name": "{}-workload-3".format(settings.RESOURCE_PREFIX),
                "query_labels": [{"name": "'__name__'", "value": "'workload.log.keyword.count'"},
                                 {"name": "'query'", "value": "'hehe.txt'"}],
                "comparison": ">",
                "threshold": 10,
                "_range": "180",
                "query": "hehe.txt"
            },
            {
                "rule_name": "{}-workload-4".format(settings.RESOURCE_PREFIX),
                "query_labels": [{"name": "'__name__'", "value": "'workload.event.reason.count'"},
                                 {"name": "'query'", "value": "'Started'"}],
                "comparison": ">",
                "threshold": 2,
                "_range": "600",
                "query": "Started"
            },
            {
                "rule_name": "{}-workload-5".format(settings.RESOURCE_PREFIX),
                "query_labels": [{"name": "'__name__'", "value": "'custom'"},
                                 {"name": "'exp'", "value": "'workload_jvflvfvf'"}],
                "comparison": ">",
                "threshold": 35,
            }
        ]
    }

]
update_data_List = [
    {
        "alarm_template_name": "{}-ares-region-template-1".format(settings.RESOURCE_PREFIX),
        "alarm_name": "{}-ares-region-1".format(settings.RESOURCE_PREFIX),
        "description": "''",
        "kind": "cluster",
        "cluster_name": REGION_NAME,
        "resource_name": REGION_NAME,
        "rules": [{
            "rule_name": "{}-ares-region-1".format(settings.RESOURCE_PREFIX),
            "query_labels": [{"name": "__name__", "value": "'cluster.memory.utilization'"}],
            "threshold": 2,
            "comparison": ">=",
            "test_labels": {"qw": "vf", "bg": "nh", "severity": "Low"},
            "metric_name": "cluster.memory.utilization",
        }]
    },
    {
        "alarm_template_name": "{}-ares-region-template-2".format(settings.RESOURCE_PREFIX),
        "alarm_name": "{}-ares-region-2".format(settings.RESOURCE_PREFIX),
        "kind": "cluster",
        "description": "update region-description",
        "cluster_name": REGION_NAME,
        "resource_name": REGION_NAME,
        "rules": [{
            "rule_name": "{}-ares-region-2".format(settings.RESOURCE_PREFIX),
            "query_labels": [{"name": "__name__", "value": "'cluster.node.not.ready.count'"}],
            "function": "max",
            "_range": "120",
            "severity": "High",
            "comparison": "==",
            "threshold": 1.22,
            "metric_name": "cluster.node.not.ready.count"
        }]
    },
    {
        "alarm_template_name": "{}-ares-region-template-3".format(settings.RESOURCE_PREFIX),
        "alarm_name": "{}-ares-region-3".format(settings.RESOURCE_PREFIX),
        "kind": "cluster",
        "description": "update region-custom",
        "cluster_name": REGION_NAME,
        "resource_name": REGION_NAME,
        "rules": [{
            "rule_name": "{}-ares-region-3".format(settings.RESOURCE_PREFIX),
            "query_labels": [{"name": "'__name__'", "value": "'custom'"},
                             {"name": "'expr'", "value": "'cluster_resource_requests_cpu_cores'"}],
            "severity": "Low",
            "comparison": "!=",
            "threshold": 33.33,
            "wait": "300",
            "metric_name": "custom",
            "metric": "cluster_resource_requests_cpu_cores"
        }]
    },
    {
        "alarm_template_name": "{}-ares-region-template-4".format(settings.RESOURCE_PREFIX),
        "alarm_name": "{}-region-more-rule".format(settings.RESOURCE_PREFIX),
        "kind": "cluster",
        "description": "update region more rule",
        "cluster_name": REGION_NAME,
        "resource_name": REGION_NAME,
        "rules": [
            {
                "rule_name": "{}-region-1".format(settings.RESOURCE_PREFIX),
                "query_labels": [{"name": "'__name__'", "value": "cluster.resource.request.memory.utilization"}],
                "comparison": "<=",
                "threshold": 99,
                "metric_name": "cluster.resource.request.memory.utilization"
            },
            {
                "rule_name": "{}-region-2".format(settings.RESOURCE_PREFIX),
                "query_labels": [{"name": "'__name__'", "value": "cluster.node.not.ready.count"}],
                "comparison": "==",
                "threshold": 33,
                "function": "max",
                "_range": "600",
                "metric_name": "cluster.resource.request.memory.utilization"
            },
            {
                "rule_name": "{}-region-3".format(settings.RESOURCE_PREFIX),
                "query_labels": [{"name": "'__name__'", "value": "'custom'"},
                                 {"name": "'expr'", "value": "'update_vfvfvfv'"}],
                "comparison": "<=",
                "threshold": 23.44,
                "metric_name": "custom",
                "metric": "update_vfvfvfv"
            }
        ]
    },
    {
        "alarm_template_name": "{}-ares-node-template-1".format(settings.RESOURCE_PREFIX),
        "alarm_name": "{}-ares-node-1".format(settings.RESOURCE_PREFIX),
        "kind": "node",
        "description": "''",
        "cluster_name": REGION_NAME,
        "rules": [{
            "rule_name": "{}-ares-node-1".format(settings.RESOURCE_PREFIX),
            "query_labels": [{"name": "'__name__'", "value": "'node.memory.utilization'"}],
            "severity": "High",
            "threshold": 46,
            "comparison": "<",
            "wait": "180",
            "metric_name": "node.memory.utilization"
        }]
    },
    {
        "alarm_template_name": "{}-ares-node-template-2".format(settings.RESOURCE_PREFIX),
        "alarm_name": "{}-ares-node-2".format(settings.RESOURCE_PREFIX),
        "kind": "node",
        "description": "update node-description",
        "cluster_name": REGION_NAME,
        "rules": [{
            "rule_name": "{}-ares-node-2".format(settings.RESOURCE_PREFIX),
            "query_labels": [{"name": "'__name__'", "value": "'node.disk.io.time'"}],
            "threshold": 44,
            "comparison": "<=",
            "wait": "120",
            "function": "min",
            "_range": "180",
            "metric_name": "node.disk.io.time"
        }]
    },
    {
        "alarm_template_name": "{}-ares-node-template-3".format(settings.RESOURCE_PREFIX),
        "alarm_name": "{}-ares-node-3".format(settings.RESOURCE_PREFIX),
        "kind": "node",
        "description": "update node-custom",
        "cluster_name": REGION_NAME,
        "rules": [{
            "rule_name": "{}-ares-node-3".format(settings.RESOURCE_PREFIX),
            "query_labels": [{"name": "'__name__'", "value": "'custom'"},
                             {"name": "'expr'", "value": "'nodeqwqwqwq'"}],
            "threshold": 12,
            "comparison": ">=",
            "test_labels": {"vfv": "bgb", "severity": "'High'"},
            "test_annotations": {"qwqw": "vfvfvf"},
            "wait": "180",
            "metric_name": "custom",
            "metric": "nodeqwqwqwq"
        }]
    },
    {
        "alarm_template_name": "{}-ares-node-template-4".format(settings.RESOURCE_PREFIX),
        "alarm_name": "{}-node-more-rule".format(settings.RESOURCE_PREFIX),
        "kind": "node",
        "description": "update node-more-rule",
        "cluster_name": REGION_NAME,
        "rules": [
            {
                "rule_name": "{}-node-1".format(settings.RESOURCE_PREFIX),
                "query_labels": [{"name": "'__name__'", "value": "'node.load.5'"}],
                "threshold": 10,
                "comparison": ">=",
                "metric_name": "node.load.5"
            },
            {
                "rule_name": "{}-node-2".format(settings.RESOURCE_PREFIX),
                "query_labels": [{"name": "'__name__'", "value": "'node.network.receive_bytes'"}],
                "threshold": 9,
                "comparison": "<",
                "function": "min",
                "_range": "60",
                "metric_name": "node.network.receive_bytes"
            },
            {
                "rule_name": "{}-node-3".format(settings.RESOURCE_PREFIX),
                "query_labels": [{"name": "'__name__'", "value": "'custom'"},
                                 {"name": "'expr'", "value": "'node_cpu'"}],
                "threshold": 1.2,
                "comparison": "<=",
                "metric_name": "custom",
                "metric": "node_cpu"
            }
        ]
    },
    {
        "alarm_template_name": "{}-ares-workload-template-1".format(settings.RESOURCE_PREFIX),
        "alarm_name": "{}-ares-workload-1".format(settings.RESOURCE_PREFIX),
        "kind": "workload",
        "description": "''",
        "cluster_name": REGION_NAME,
        "rules": [{
            "rule_name": "{}-ares-workload-1".format(settings.RESOURCE_PREFIX),
            "query_labels": [{"name": "'__name__'", "value": "'container.memory.utilization'"}],
            "threshold": 1.1,
            "comparison": "==",
            "metric_name": "container.memory.utilization"
        }]
    },
    {
        "alarm_template_name": "{}-ares-workload-template-2".format(settings.RESOURCE_PREFIX),
        "alarm_name": "{}-ares-workload-2".format(settings.RESOURCE_PREFIX),
        "kind": "workload",
        "description": "update workload-description",
        "cluster_name": REGION_NAME,
        "rules": [{
            "rule_name": "{}-ares-workload-2".format(settings.RESOURCE_PREFIX),
            "query_labels": [{"name": "'__name__'", "value": "'workload.network.transmit_bytes'"}],
            "function": "avg",
            "_range": "60",
            "comparison": ">=",
            "threshold": 66,
            "metric_name": "workload.network.transmit_bytes"
        }]
    },
    {
        "alarm_template_name": "{}-ares-logalarm-template-6".format(settings.RESOURCE_PREFIX),
        "alarm_name": "{}-ares-logalarm".format(settings.RESOURCE_PREFIX),
        "kind": "workload",
        "description": "update log-alarm",
        "cluster_name": REGION_NAME,
        "rules": [{
            "rule_name": "{}-ares-logalarm".format(settings.RESOURCE_PREFIX),
            "query_labels": [{"name": "'__name__'", "value": "'workload.log.keyword.count'"},
                             {"name": "'query'", "value": "'root'"}],
            "comparison": ">=",
            "threshold": 4,
            "_range": "180",
            "query": "root",
            "metric_name": "workload.log.keyword.count"
        }]
    },
    {
        "alarm_template_name": "{}-ares-eventalarm-template-7".format(settings.RESOURCE_PREFIX),
        "alarm_name": "{}-ares-eventalarm".format(settings.RESOURCE_PREFIX),
        "kind": "workload",
        "description": "update event-alarm",
        "cluster_name": REGION_NAME,
        "rules": [{
            "rule_name": "{}-ares-eventalarm".format(settings.RESOURCE_PREFIX),
            "query_labels": [{"name": "'__name__'", "value": "'workload.event.reason.count'"},
                             {"name": "'query'", "value": "'Failed'"}],
            "comparison": "!=",
            "threshold": 5.6,
            "_range": "600",
            "query": "Failed",
            "metric_name": "workload.event.reason.count"
        }]
    },
    {
        "alarm_template_name": "{}-ares-workload-template-5".format(settings.RESOURCE_PREFIX),
        "alarm_name": "{}-ares-workload-5".format(settings.RESOURCE_PREFIX),
        "kind": "workload",
        "description": "alarm-action",
        "cluster_name": REGION_NAME,
        "rules": [{
            "rule_name": "{}-ares-workload-5".format(settings.RESOURCE_PREFIX),
            "query_labels": [{"name": "'__name__'", "value": "'custom'"},
                             {"name": "'expr'", "value": "'container_cpu_utilization'"}],
            "comparison": "<=",
            "threshold": 12.21,
            "wait": "120",
            "test_labels": {"vf": " bg", "severity": "'Medium'"},
            "test_annotations": {"cdc": "fvfvfvfvf"},
            "metric_name": "custom",
            "metric": "container_cpu_utilization"
        }]
    },
    {
        "alarm_template_name": "{}-ares-workload-template-6".format(settings.RESOURCE_PREFIX),
        "alarm_name": "{}-workload-more-rule".format(settings.RESOURCE_PREFIX),
        "kind": "workload",
        "description": "update workload more rule",
        "cluster_name": REGION_NAME,
        "rules": [
            {
                "rule_name": "{}-workload-1".format(settings.RESOURCE_PREFIX),
                "query_labels": [{"name": "'__name__'", "value": "'workload.network.transmit_bytes'"}],
                "comparison": ">=",
                "threshold": 41,
                "metric_name": "workload.network.transmit_bytes"
            },
            {
                "rule_name": "{}-workload-2".format(settings.RESOURCE_PREFIX),
                "query_labels": [{"name": "'__name__'", "value": "'workload.memory.utilization'"}],
                "comparison": ">=",
                "threshold": 65,
                "function": "max",
                "_range": "180",
                "metric_name": "workload.memory.utilization"
            },
            {
                "rule_name": "{}-workload-3".format(settings.RESOURCE_PREFIX),
                "query_labels": [{"name": "'__name__'", "value": "'workload.log.keyword.count'"},
                                 {"name": "'query'", "value": "'root hehe.txt'"}],
                "comparison": ">",
                "threshold": 10,
                "_range": "180",
                "metric_name": "workload.log.keyword.count",
                "query": "root hehe.txt"
            },
            {
                "rule_name": "{}-workload-4".format(settings.RESOURCE_PREFIX),
                "query_labels": [{"name": "'__name__'", "value": "'workload.event.reason.count'"},
                                 {"name": "'query'", "value": "'Started'"}],
                "comparison": "<=",
                "threshold": 20,
                "_range": "180",
                "metric_name": "workload.event.reason.count",
                "query": "Started"
            },
            {
                "rule_name": "{}-workload-5".format(settings.RESOURCE_PREFIX),
                "query_labels": [{"name": "'__name__'", "value": "'custom'"},
                                 {"name": "'exp'", "value": "'container_cpu_utilization'"}],
                "comparison": "<",
                "threshold": 350,
                "metric_name": "custom",
                "metric": "container_cpu_utilization"
            }
        ]
    }

]
create_L0_data = {
    "alarm_template_name": "{}-ares-alarm-template-name".format(RESOURCE_PREFIX),
    "kind": "cluster",
    "description": "region-L0-test",
    "rules": [{
        "rule_name": "{}-ares-rule-11".format(RESOURCE_PREFIX),
        "query_labels": [{"name": "'__name__'", "value": "'cluster.cpu.utilization'"}],
        "comparison": ">=",
        "threshold": 20,
        "function": "avg",
        "_range": "180",
        "test_labels": {"create111": "create11", "create22": "createvfvfv", "severity": "High"},
        "test_annotations": {"aa": "cc", "bb": "ff"}
    }]
}
update_L0_List = {
    "alarm_template_name": "{}-ares-alarm-template-name".format(RESOURCE_PREFIX),
    "alarm_name": "{}-ares-rule-alarm".format(RESOURCE_PREFIX),
    "kind": "cluster",
    "description": "update-template",
    "cluster_name": REGION_NAME,
    "resource_name": REGION_NAME,
    "ns_name": DEFAULT_NS,
    "rules": [{
        "rule_name": "{}-ares-rule".format(RESOURCE_PREFIX),
        "query_labels": [{"name": "'__name__'", "value": "'cluster.memory.utilization'"}],
        "metric_name": "cluster.memory.utilization",
        "comparison": "<=",
        "threshold": 200,
        "test_labels": {"update11": "uad", "ad": "dad", "severity": "Low"},
        "test_annotations": {"vvv": "ww", "vv": "ccc"}
    }]
}

L1_template_name = ["集群告警模板-数据类型原始值", "集群告警模板-数据类型聚合值", "集群告警模板-自定义规则", "集群告警模板-多个规则",
                    "主机告警模板-数据类型原始值", "主机告警模板-数据类型聚合值", "主机告警模板-自定义规则", "主机告警模板-多个规则",
                    "应用告警模板-数据类型原始值", "应用告警模板-数据类型聚合值", "应用告警模板-日志告警", "应用告警模板-事件告警",
                    "应用告警模板- 自定义规则", "应用告警模板-多个规则"]


@pytest.fixture(scope="session", autouse=True)
@pytest.mark.skipif(checker.smtp_availabel() is False, reason="邮箱服务器无法访问")
def Prepare_Template():
    notification_object = Notification_Object()
    notification_server = Notification_Server()
    notification_template = Notification_Template()
    email_sender = Email_Sender()
    notification = Notification()
    monitoring = Monitoring()
    newapp = Application()
    app_name = "{}-ares-template-deploy".format(RESOURCE_PREFIX)
    object_name = "{}-ares-template-receiver".format(settings.RESOURCE_PREFIX)
    sender_name = "{}-ares-template-sender".format(settings.RESOURCE_PREFIX)
    email_server_name = "{}-ares-template-server".format(settings.RESOURCE_PREFIX)
    template_name = "{}-ares-template-template".format(settings.RESOURCE_PREFIX)
    notification_name = "{}-ares-template-notification".format(settings.RESOURCE_PREFIX)
    ns_name = K8S_NAMESPACE
    # 获取集群主机ip
    results = monitoring.get_list_node()
    assert results.status_code == 200, "获取主机列表失败:{}".format(results.text)
    node_name = monitoring.get_value(results.json(), 'items.0.status.addresses.0.address')
    if settings.CASE_TYPE not in ("delete", "upgrade"):
        newapp.delete_app(ns_name, app_name)
        newapp.check_exists(newapp.common_app_url(ns_name, app_name), 404)
        notification_object.delete_notification_object_jinja2(object_name)
        notification_server.delete_notification_server_jinja2(email_server_name)
        notification_template.delete_notification_temolate_jinja2(template_name)
        notification.delete_notification_jinja2(notification_name)

        # 创建应用
        data = {
            'project_name': PROJECT_NAME,
            'namespace': K8S_NAMESPACE,
            'app_name': app_name,
            'deployment_name': app_name,
            'image': IMAGE,
            'replicas': 1
        }
        create_result = newapp.create_app(
            './test_data/newapp/create_app.jinja2', data
        )
        assert create_result.status_code == 200, "新版应用创建失败 {}".format(create_result.text)
        app_status = newapp.get_app_status(ns_name, app_name, 'Running')
        assert app_status, "创建应用后，验证应用状态出错：app: {} is not running".format(app_name)
        # 添加通知对象receiver
        object_data = {
            "name": object_name,
            "display_name": "this is L0 test",
            "destination": "cdjbcdjbcd@qq.com",
            "namespace": settings.DEFAULT_NS
        }
        create_result = notification_object.create_notification_object_jinja2(
            "./test_data/notification/notification_object.jinja2", object_data)
        assert create_result.status_code == 201, "添加通知使用的通知对象失败:{}".format(create_result.text)
        # 创建通知服务器
        server_data = {
            "notificationserver_name": email_server_name,
            "description": "通知使用的邮件服务器",
            "display_name": "this for email sender",
            "host": "smtp.163.com",
            "port": "25",
            "insecureSkipVerify": "true"
        }
        create_result = notification_server.create_notification_server_jinja2(
            "./test_data/notification/notification_server.jinja2", server_data)
        assert create_result.status_code == 201, "通知服务器创建失败:{}".format(create_result.text)
        sleep(3)
        # 添加通知发送人sender(前提是创建好通知服务器）
        sender_data = {
            "email_sender_name": sender_name,
            "username": str(base64.b64encode(NOTI_USERNAME[0].encode("utf-8")), "utf-8"),
            "password": str(base64.b64encode(NOTI_PWD[0].encode("utf-8")), "utf-8"),
            "notification_server_name": email_server_name,
            "namespace": DEFAULT_NS
        }
        create_result = email_sender.create_email_sender_jinja2(
            "./test_data/notification/email_sender.jinja2", sender_data)
        assert create_result.status_code == 201 or "exists" in create_result.text, "添加通知发送人失败:{}".format(
            create_result.text)
        # 创建邮件通知模板
        template_data = {
            "name": template_name,
            "display_name": "template for notification",
            "description": "template for notification"
        }
        create_result = notification_template.create_notification_temolate_jinja2(
            "./test_data/notification/notification_template.jinja2", template_data)
        assert create_result.status_code == 201, "创建通知模板失败:{}".format(create_result.text)
        # 创建通知
        notification_data = {
            "name": notification_name,
            "display_name": "create notification",
            "description": "create test",
            "receivers": object_name,
            "sender": sender_name,
            "template": template_name,
            "namespace": settings.DEFAULT_NS
        }
        create_result = notification.create_notification_jinja2(
            "./test_data/notification/notification.jinja2", notification_data)
        assert create_result.status_code == 201, "创建通知失败:{}".format(create_result.text)
    prepare_data = {
        "node_name": node_name,
        "app_name": app_name,
        "notification_name": notification_name,
        "notification_namespace": DEFAULT_NS,
        "namespace": K8S_NAMESPACE,
        "region_name": REGION_NAME

    }
    yield prepare_data
    if settings.CASE_TYPE not in ("prepare", "upgrade"):
        notification_object.delete_notification_object_jinja2(object_name)
        notification_server.delete_notification_server_jinja2(email_server_name)
        email_sender.delete_email_sender_jinja2(sender_name)
        notification_template.delete_notification_temolate_jinja2(template_name)
        notification.delete_notification_jinja2(notification_name)
        newapp.delete_app(ns_name, app_name)
