import os
import sys
import json
import time
import hashlib
import subprocess

from common import settings
from common.base_request import Common
from common.log import logger


class Alarm(Common):
    def get_create_alarm_url(self, ns_name):
        return 'kubernetes/{}/apis/monitoring.coreos.com/v1/namespaces/{}/prometheusrules'.format(settings.REGION_NAME,
                                                                                                  ns_name)

    def get_alarm_url(self, ns_name, alarm_name):
        return 'kubernetes/{}/apis/monitoring.coreos.com/v1/namespaces/{}/prometheusrules/{}'.format(
            settings.REGION_NAME, ns_name, alarm_name)

    def get_alarm_list_url(self):
        return 'kubernetes/{}/apis/monitoring.coreos.com/v1/prometheusrules'.format(settings.REGION_NAME)

    def get_alert_state_url(self):
        return 'v1/metrics/{}/prometheus/query'.format(settings.REGION_NAME)

    def get_indicators_url(self):
        return "v1/metrics/global/indicators"

    def create_alarm_jinja2(self, file, data, ns_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_create_alarm_url(ns_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=url, json=data, params={})

    def update_alarm_jinja2(self, alarm_name, file, data, ns_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_alarm_url(ns_name, alarm_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='PUT', path=url, json=data, params={})

    def get_alarm_detail(self, alarm_name, ns_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_alarm_url(ns_name, alarm_name)
        return self.send(method='GET', path=url, params={})

    def delete_alarm(self, alarm_name, ns_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_alarm_url(ns_name, alarm_name)
        return self.send(method='DELETE', path=url, params={})

    def list_alarm(self, payload):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_alarm_list_url()
        payload.update({"limit": 100})
        return self.send(method='GET', path=url, params=payload)

    def search_alarm(self, payload):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = "acp/v1/resources/search/" + self.get_alarm_list_url()
        payload.update({"limit": 20})
        return self.send(method='GET', path=url, params=payload)

    def get_alert_state(self, alertname):
        count = 0
        while count < 200:
            count += 1
            url = self.get_alert_state_url()
            params = {"query": "ALERTS{}", "time": time.time()}
            result = self.send(method='GET', path=url, params=params)
            if result.status_code == 200 and alertname in result.text:
                return True
            time.sleep(3)
        return False

    def get_resourceVersion(self, alarm_name, ns_name):
        url = self.get_alarm_url(ns_name, alarm_name)
        response = self.send(method='GET', path=url, params={})
        resourceVersion = self.get_value(response.json(), 'metadata.resourceVersion')
        return resourceVersion

    def get_metric_query(self, metric_name):
        '''
        获取监控指标对应的go模板
        :param metric_name: 要查询的监控资源名称
        :return:
        '''
        url = self.get_indicators_url()
        response = self.send(method='GET', path=url, params={})
        for items in response.json():
            if items["name"] == metric_name:
                return items["query"]
        return ""

    def parse_go_template(self, dict_data, template_data):
        '''
        监控指标对应的模板渲染
        :param dict_data: 要渲染的数据，是一个dict，key对应模板中要渲染的参数
        :param template_data: 要渲染的模板
        :return:
        '''
        base_dir = os.path.dirname(__file__)
        # go_file = os.path.join(base_dir, "parse_go_template.go")
        go_file = os.path.join(base_dir, "parse_go_template.bin")
        timestamp = int(time.time() * 1e6)
        pid = os.getpid()
        hash_value = hashlib.md5("{}_{}".format(timestamp, pid).encode("ascii")).hexdigest()
        data_path = os.path.join(base_dir, "data_%s.json" % hash_value)
        template_path = os.path.join(base_dir, "template_%s.html" % hash_value)
        with open(data_path, "w") as fp:
            json.dump(dict_data, fp=fp)
        with open(template_path, "w") as fp:
            fp.write(template_data)
        cmd_template = "\"{go_file}\"  --dataPath \"{data_path}\" --templatePath \"{template_path}\""
        cmd = cmd_template.format(
            go_file=go_file, data_path=data_path, template_path=template_path
        )

        status, message = subprocess.getstatusoutput(cmd)
        # 先删除临时文件
        if os.path.exists(data_path):
            os.remove(data_path)
        if os.path.exists(template_path):
            os.remove(template_path)

        # 在判断命令执行状态
        if status != 0:
            raise Exception("parse go template Failed(data=%s, template=%s), Run(cmd=%s, status=%s, message=%s)" % (
                json.dumps(dict_data), template_data, cmd, status, message
            ))

        # message = message.lstrip("# command-line-arguments\nloadinternal: cannot find runtime/cgo\n")
        return message

    def get_alarm_expr(self, data, Prepare_Alarm):
        '''
        获取监控指标对应的prometheus公式
        :param data: 模板中要用到的数据
        :param Prepare_Alarm: 模板中要用到的数据
        :return:
        '''
        template_data = self.get_metric_query(data["metric_name"])
        if template_data == "":
            expr = data["metric"]
        else:
            if data["resource_name"] == "0.0.0.0":
                name = ".*"
            elif data["kind"] == "Deployment":
                name = Prepare_Alarm["app_name"]
            elif data["kind"] == "Node":
                name = data["resource_name"] + ":.*"
            else:
                name = data["resource_name"]
            dict_data = {
                "Function": data.get("function", ""),
                "Namespace": Prepare_Alarm["namespace"],
                "PodNamePattern": Prepare_Alarm["app_name"] + "-[a-z0-9]{7,10}-[a-z0-9]{5}",
                "Range": data.get("_range", "0"),
                "Name": name,
                "AliasName": Prepare_Alarm["node_name"],
                "ClusterName": Prepare_Alarm["region_name"],
                "Kind": data.get("kind"),
                "Application": Prepare_Alarm["app_name"],
                "Query": data.get("query", "")
            }
            expr = self.parse_go_template(dict_data, template_data)
        return expr + data["comparison"] + str(data["threshold"])
