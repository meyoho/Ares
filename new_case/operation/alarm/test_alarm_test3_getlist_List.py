import pytest
from common.check_status import checker
from common.settings import RERUN_TIMES, K8S_NAMESPACE, DEFAULT_LABEL
from new_case.operation.alarm.alarm import Alarm
from new_case.operation.alarm.conftest import update_data_List, L1_case_name


@pytest.mark.morgans
@pytest.mark.Regression
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=0)
class TestAlarmSuiteGet():
    def setup_class(self):
        self.alarm = Alarm()

    @pytest.mark.parametrize("data", update_data_List, ids=L1_case_name)
    def 测试获取告警列表_带参数name(self, data):
        payload = {
            "keyword": data['alarm_name'],
            "field": "metadata.name",
            "labelSelector": "alert.{}/owner=System".format(DEFAULT_LABEL)
        }
        create_result = self.alarm.search_alarm(payload)
        assert create_result.status_code == 200, "获取告警列表_带参数name{}失败:{}".format(data['alarm_name'], create_result.text)
        assert data['alarm_name'] in create_result.text, "告警{}不在列表中:{}".format(data['alarm_name'], create_result.text)

    @pytest.mark.skipif(checker.smtp_availabel() is False, reason="邮箱服务器无法访问")
    def 测试获取告警列表_带参数labelSelector(self, Prepare_Alarm):
        payload = {
            "labelSelector": {
                "alert.{}/owner".format(DEFAULT_LABEL): "System",
                "alert.{}/namespace".format(DEFAULT_LABEL): K8S_NAMESPACE,
                "alert.{}/application".format(DEFAULT_LABEL): Prepare_Alarm['app_name']
            }
        }
        create_result = self.alarm.list_alarm(payload)
        assert create_result.status_code == 200, "获取告警列表_带参数labelSelector失败:{}".format(create_result.text)
