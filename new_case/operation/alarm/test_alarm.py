import sys

import pytest
from common.check_status import checker
from common.settings import RERUN_TIMES, K8S_NAMESPACE, DEFAULT_NS, DEFAULT_LABEL
from new_case.operation.alarm.alarm import Alarm
from new_case.operation.alarm.conftest import create_L0_data, update_L0_List, l0_create_case_name, l0_update_case_name


@pytest.mark.morgans
@pytest.mark.BAT
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=0)
@pytest.mark.skipif(checker.smtp_availabel() is False, reason="邮箱服务器无法访问")
class TestAlarmSuite(object):
    def setup_class(self):
        self.alarm = Alarm()

    @pytest.mark.prepare
    @pytest.mark.parametrize("data", create_L0_data, ids=l0_create_case_name)
    def 测试不同指标告警创建L0(self, data, Prepare_Alarm):
        # 删除资源
        ns_name = K8S_NAMESPACE if data['kind'] == "Deployment" else DEFAULT_NS
        self.alarm.delete_alarm(data['alarm_name'], ns_name)

        # create alarm
        if data['kind'] == "Node":
            data.update({"resource_name": data.get("resource_name", Prepare_Alarm["node_name"])})
        elif data['kind'] == "Deployment":
            data.update({"resource_name": Prepare_Alarm["app_name"], "application": Prepare_Alarm["app_name"]})
        data.update({
            "test_notifications": [{"name": Prepare_Alarm["notification_name"],
                                    "namespace": Prepare_Alarm["notification_namespace"]}],
        })
        for item in data["rules"]:
            n_data = {
                "application": data.get('application', ""),
                "resource_name": data["resource_name"],
                "cluster_name": data["cluster_name"],
                "kind": data["kind"],
                "ns_name": data["ns_name"],
                "project": data.get("project", "")
            }
            if data['kind'] != 'Deployment':
                n_data.pop('application')
            item.update(n_data)
            item.update({"expr": self.alarm.get_alarm_expr(item, Prepare_Alarm)})
        create_result = self.alarm.create_alarm_jinja2("./test_data/alarm/alarm.jinja2", data, ns_name)
        assert create_result.status_code == 201, "创建不同指标告警{}失败:{}".format(data['alarm_name'], create_result.text)
        value = self.alarm.generate_jinja_data("verify_data/alarm/verify_alarm.jinja2", data)
        assert self.alarm.is_sub_dict(value, create_result.json()), \
            "创建不同指标告警{}比对数据失败，返回数据:{},期望数据:{}".format(data['alarm_name'], create_result.json(), value)

    @pytest.mark.upgrade
    @pytest.mark.parametrize("data", create_L0_data, ids=l0_create_case_name)
    def 测试获取指标告警详情页L0(self, data, Prepare_Alarm):
        # get alarm detail
        if data['kind'] == "Node":
            data.update({"resource_name": data.get("resource_name", Prepare_Alarm["node_name"])})
        elif data['kind'] == "Deployment":
            data.update({"resource_name": Prepare_Alarm["app_name"], "application": Prepare_Alarm["app_name"]})
        data.update({
            "test_notifications": [{"name": Prepare_Alarm["notification_name"],
                                    "namespace": Prepare_Alarm["notification_namespace"]}],
        })
        ns_name = K8S_NAMESPACE if data['kind'] == "Deployment" else DEFAULT_NS
        for item in data["rules"]:
            n_data = {
                "application": data.get('application', ""),
                "resource_name": data["resource_name"],
                "cluster_name": data["cluster_name"],
                "kind": data["kind"],
                "ns_name": data["ns_name"],
                "project": data.get("project", "")
            }
            if data['kind'] != 'Deployment':
                n_data.pop('application')
            item.update(n_data)
            item.update({"expr": self.alarm.get_alarm_expr(item, Prepare_Alarm)})
        detail_result = self.alarm.get_alarm_detail(data['alarm_name'], ns_name)
        assert detail_result.status_code == 200, "获取指标告警{}详情页失败:{}".format(data['alarm_name'], detail_result.text)
        value = self.alarm.generate_jinja_data("verify_data/alarm/verify_alarm.jinja2", data)
        assert self.alarm.is_sub_dict(value, detail_result.json()), \
            "指标告警{}详情页比对数据失败，返回数据:{},期望数据:{}".format(data['alarm_name'], detail_result.json(), value)

    @pytest.mark.upgrade
    @pytest.mark.parametrize("data", create_L0_data, ids=l0_create_case_name)
    def 测试指标告警列表页L0(self, data, Prepare_Alarm):
        # get alarm list
        if data['kind'] == "Node":
            data.update({"resource_name": data.get("resource_name", Prepare_Alarm["node_name"])})
        elif data['kind'] == "Deployment":
            data.update({"resource_name": Prepare_Alarm["app_name"], "application": Prepare_Alarm["app_name"]})
        data.update({
            "test_notifications": [{"name": Prepare_Alarm["notification_name"],
                                    "namespace": Prepare_Alarm["notification_namespace"]}],
        })
        list_result = self.alarm.list_alarm(payload={"labelSelector": "alert.{}/owner=System".format(DEFAULT_LABEL)})
        for item in data["rules"]:
            n_data = {
                "application": data.get('application', ""),
                "resource_name": data["resource_name"],
                "cluster_name": data["cluster_name"],
                "kind": data["kind"],
                "ns_name": data["ns_name"],
                "project": data.get("project", "")
            }
            if data['kind'] != 'Deployment':
                n_data.pop('application')
            item.update(n_data)
            item.update({"expr": self.alarm.get_alarm_expr(item, Prepare_Alarm)})
        assert list_result.status_code == 200, "获取指标告警列表页失败:{}".format(list_result.text)
        assert data['alarm_name'] in list_result.text, "指标告警不在告警列表中"
        content = self.alarm.get_k8s_resource_data(list_result.json(), data['alarm_name'], list_key="items")
        value = self.alarm.generate_jinja_data("verify_data/alarm/verify_alarm.jinja2", data)
        assert self.alarm.is_sub_dict(value, content), \
            "获取指标告警列表比对数据失败，返回数据:{},期望数据:{}".format(content, value)

    @pytest.mark.upgrade
    @pytest.mark.monitor
    @pytest.mark.parametrize("data", update_L0_List, ids=l0_update_case_name)
    def 测试指标告警更新L0(self, data, Prepare_Alarm):
        # update alarm
        ns_name = K8S_NAMESPACE if data['kind'] == "Deployment" else DEFAULT_NS
        if data['kind'] == "Node":
            data.update({"resource_name": data.get("resource_name", Prepare_Alarm["node_name"])})
        elif data['kind'] == "Deployment":
            data.update({"resource_name": Prepare_Alarm["app_name"], "application": Prepare_Alarm["app_name"]})
        data.update({
            "resourceVersion": self.alarm.get_resourceVersion(data['alarm_name'], ns_name),
            "test_notifications": [{"name": Prepare_Alarm["notification_name"],
                                    "namespace": Prepare_Alarm["notification_namespace"]}],
        })
        for item in data["rules"]:
            n_data = {
                "application": data.get('application', ""),
                "resource_name": data["resource_name"],
                "cluster_name": data["cluster_name"],
                "kind": data["kind"],
                "ns_name": data["ns_name"],
                "project": data.get("project", "")
            }
            if data['kind'] != 'Deployment':
                n_data.pop('application')
            item.update(n_data)
            item.update({"expr": self.alarm.get_alarm_expr(item, Prepare_Alarm)})
        update_result = self.alarm.update_alarm_jinja2(data['alarm_name'],
                                                       "./test_data/alarm/alarm.jinja2", data, ns_name)
        assert update_result.status_code == 200, "更新指标告警{}失败:{}".format(data['alarm_name'], update_result.text)
        data.update({
            "resourceVersion": self.alarm.get_resourceVersion(data['alarm_name'], ns_name)
        })
        value = self.alarm.generate_jinja_data("verify_data/alarm/verify_alarm.jinja2", data)
        assert self.alarm.is_sub_dict(value, update_result.json()), \
            "更新集群指标告警比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)
        response = self.alarm.get_alert_state(data["rules"][0]['rule_name'])
        assert response, "{}:触发告警失败".format(sys._getframe().f_code.co_name)

    @pytest.mark.delete
    @pytest.mark.parametrize("data", create_L0_data, ids=l0_create_case_name)
    def 测试指标告警删除L0(self, data):
        # delete alarm
        ns_name = K8S_NAMESPACE if data['kind'] == "Deployment" else DEFAULT_NS
        delete_result = self.alarm.delete_alarm(data['alarm_name'], ns_name)
        assert delete_result.status_code == 200, "删除指标告警{}失败:{}".format(data['alarm_name'], delete_result.text)
        assert self.alarm.check_exists(self.alarm.get_alarm_url(ns_name, data['alarm_name']), 404, params="")
