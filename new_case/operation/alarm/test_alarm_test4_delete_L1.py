import pytest
from common.check_status import checker
from common.settings import RERUN_TIMES, K8S_NAMESPACE, DEFAULT_NS, USERNAME, REGION_NAME, AUDIT_UNABLED
from new_case.operation.alarm.alarm import Alarm
from new_case.operation.alarm.conftest import update_data_List, L1_case_name


@pytest.mark.morgans
@pytest.mark.Regression
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=0)
@pytest.mark.skipif(checker.smtp_availabel() is False, reason="邮箱服务器无法访问")
class TestAlarmL1SuiteDelete():
    def setup_class(self):
        self.alarm = Alarm()

    @pytest.mark.parametrize("data", update_data_List, ids=L1_case_name)
    def 测试指标告警删除(self, data):
        # delete alarm
        ns_name = K8S_NAMESPACE if data['kind'] == "Deployment" else DEFAULT_NS
        delete_result = self.alarm.delete_alarm(data['alarm_name'], ns_name)
        assert delete_result.status_code == 200, "删除指标告警{}失败:{}".format(data['alarm_name'], delete_result.text)
        assert self.alarm.check_exists(self.alarm.get_alarm_url(ns_name, data['alarm_name']), 404, params="")

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 不测试Cluster告警删除审计(self):
        payload = {"user_name": USERNAME, "operation_type": "delete", "resource_type": "prometheusrules",
                   "resource_name": update_data_List[0]['alarm_name']}
        result = self.alarm.search_audit(payload)
        payload.update({"namespace": DEFAULT_NS, "region_name": REGION_NAME})
        values = self.alarm.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.alarm.is_sub_dict(values, result.json()), "审计数据不符合预期"

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 不测试Deployment告警删除审计(self):
        payload = {"user_name": USERNAME, "operation_type": "delete", "resource_type": "prometheusrules",
                   "resource_name": update_data_List[-1]['alarm_name']}
        result = self.alarm.search_audit(payload)
        payload.update({"namespace": K8S_NAMESPACE, "region_name": REGION_NAME})
        values = self.alarm.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.alarm.is_sub_dict(values, result.json()), "审计数据不符合预期"
