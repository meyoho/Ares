import pytest
from common.check_status import checker
from common.settings import RERUN_TIMES, REGION_NAME, K8S_NAMESPACE, DEFAULT_NS, USERNAME, AUDIT_UNABLED
from new_case.operation.alarm.alarm import Alarm
from new_case.operation.alarm.conftest import update_data_List, L1_case_name


@pytest.mark.morgans
@pytest.mark.Regression
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=0)
@pytest.mark.skipif(checker.smtp_availabel() is False, reason="邮箱服务器无法访问")
class TestAlarmSuitePut():
    def setup_class(self):
        self.alarm = Alarm()

    @pytest.mark.parametrize("data", update_data_List, ids=L1_case_name)
    def 测试不同指标告警更新(self, data, Prepare_Alarm):
        # update alarm
        if data['kind'] == "Node":
            data.update({"resource_name": data.get("resource_name", Prepare_Alarm["node_name"])})
        elif data['kind'] == "Deployment":
            data.update({"resource_name": Prepare_Alarm["app_name"], "application": Prepare_Alarm["app_name"]})
        if data['description'] == "update-alarm-action":
            data.update({"test_notifications": [
                {"name": Prepare_Alarm["notification_name"], "namespace": Prepare_Alarm["notification_namespace"]}]})
        ns_name = K8S_NAMESPACE if data['kind'] == "Deployment" else DEFAULT_NS
        data.update({
            "resourceVersion": self.alarm.get_resourceVersion(data['alarm_name'], ns_name),
        })
        for item in data["rules"]:
            n_data = {
                "application": data.get('application', ""),
                "resource_name": data["resource_name"],
                "cluster_name": data["cluster_name"],
                "kind": data["kind"],
                "ns_name": data["ns_name"],
                "project": data.get("project", "")
            }
            if data['kind'] != 'Deployment':
                n_data.pop('application')
            item.update(n_data)
            item.update({"expr": self.alarm.get_alarm_expr(item, Prepare_Alarm)})
        update_result = self.alarm.update_alarm_jinja2(data['alarm_name'],
                                                       "./test_data/alarm/alarm.jinja2", data, ns_name)
        assert update_result.status_code == 200, "更新指标告警{}失败:{}".format(data['alarm_name'], update_result.text)
        data.update({
            "resourceVersion": self.alarm.get_resourceVersion(data['alarm_name'], ns_name)
        })
        value = self.alarm.generate_jinja_data("verify_data/alarm/verify_alarm.jinja2", data)
        assert self.alarm.is_sub_dict(value, update_result.json()), \
            "更新集群指标告警比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 不测试Cluster告警更新审计(self):
        payload = {"user_name": USERNAME, "operation_type": "update", "resource_type": "prometheusrules",
                   "resource_name": update_data_List[0]['alarm_name']}
        result = self.alarm.search_audit(payload)
        payload.update({"namespace": DEFAULT_NS, "region_name": REGION_NAME})
        values = self.alarm.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.alarm.is_sub_dict(values, result.json()), "审计数据不符合预期"

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 不测试Deployment告警更新审计(self):
        payload = {"user_name": USERNAME, "operation_type": "update", "resource_type": "prometheusrules",
                   "resource_name": update_data_List[-1]['alarm_name']}
        result = self.alarm.search_audit(payload)
        payload.update({"namespace": K8S_NAMESPACE, "region_name": REGION_NAME})
        values = self.alarm.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.alarm.is_sub_dict(values, result.json()), "审计数据不符合预期"
