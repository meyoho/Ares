package main
import (
 "os"
 "fmt"
 "flag"
 "io/ioutil"
 "encoding/json"
 "text/template"
)

type ApiResponseData struct {
 Function string
 Namespace  string
 PodNamePattern  string
 Range  string
 Name   string
 AliasName string
 ClusterName string
 Kind string
 Application string
 Query string
}

func main() {
 dataPath  := flag.String("dataPath", "demo.json", "data path")
 templatePath :=  flag.String("templatePath", "demo.txt", "filepath")
 flag.Parse()
 data, err := ioutil.ReadFile(*dataPath)
 if err != nil {
  fmt.Println("File reading error", err)
  os.Exit(-1)
 }
 var apiData ApiResponseData
 jsonErr := json.Unmarshal([]byte(data), &apiData)
 if jsonErr != nil {
  fmt.Println("parse json data failed", jsonErr)
  os.Exit(-1)
 }

 t, err := template.ParseFiles(*templatePath)
 if err != nil {
  fmt.Println("parse file err:", err)
  os.Exit(-1)
 }

 if err := t.Execute(os.Stdout, apiData); err != nil {
  fmt.Println("There was an error:", err.Error())
  os.Exit(-1)
 }
}