import pytest
from common.check_status import checker
from common.settings import RERUN_TIMES, REGION_NAME, K8S_NAMESPACE, DEFAULT_NS, USERNAME, AUDIT_UNABLED
from new_case.operation.alarm.alarm import Alarm
from new_case.operation.alarm.conftest import create_data_List, L1_case_name


@pytest.mark.morgans
@pytest.mark.Regression
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=0)
@pytest.mark.skipif(checker.smtp_availabel() is False, reason="邮箱服务器无法访问")
class TestAlarmSuitePost():
    def setup_class(self):
        self.alarm = Alarm()

    @pytest.mark.parametrize("data", create_data_List, ids=L1_case_name)
    def 测试不同指标告警创建(self, data, Prepare_Alarm):
        # 删除资源
        ns_name = K8S_NAMESPACE if data['kind'] == "Deployment" else DEFAULT_NS
        self.alarm.delete_alarm(data['alarm_name'], ns_name)
        if data['kind'] == "Node":
            data.update({"resource_name": data.get("resource_name", Prepare_Alarm["node_name"])})
        elif data['kind'] == "Deployment":
            data.update({"resource_name": Prepare_Alarm["app_name"], "application": Prepare_Alarm["app_name"]})
        if data['description'] == "alarm-action":
            data.update({"test_notifications": [
                {"name": Prepare_Alarm["notification_name"], "namespace": Prepare_Alarm["notification_namespace"]}]})
        for item in data["rules"]:
            n_data = {
                "application": data.get('application', ""),
                "resource_name": data["resource_name"],
                "cluster_name": data["cluster_name"],
                "kind": data["kind"],
                "ns_name": data["ns_name"],
                "project": data.get("project", "")
            }
            if data['kind'] != 'Deployment':
                n_data.pop('application')
            item.update(n_data)
            item.update({"expr": self.alarm.get_alarm_expr(item, Prepare_Alarm)})
        create_result = self.alarm.create_alarm_jinja2("./test_data/alarm/alarm.jinja2", data, ns_name)
        assert create_result.status_code == 201, "创建不同指标告警{}失败:{}".format(data['alarm_name'], create_result.text)
        value = self.alarm.generate_jinja_data("verify_data/alarm/verify_alarm.jinja2", data)
        assert self.alarm.is_sub_dict(value, create_result.json()), \
            "创建不同指标告警{}比对数据失败，返回数据:{},期望数据:{}".format(data['alarm_name'], create_result.json(), value)

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 不测试Cluster告警创建审计(self):
        payload = {"user_name": USERNAME, "operation_type": "create", "resource_type": "prometheusrules",
                   "resource_name": create_data_List[0]['alarm_name']}
        result = self.alarm.search_audit(payload)
        payload.update({"namespace": K8S_NAMESPACE, "region_name": REGION_NAME, "code": 201})
        values = self.alarm.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.alarm.is_sub_dict(values, result.json()), "审计数据不符合预期"

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 不测试Deployment告警创建审计(self):
        payload = {"user_name": USERNAME, "operation_type": "create", "resource_type": "prometheusrules",
                   "resource_name": create_data_List[-1]['alarm_name']}
        result = self.alarm.search_audit(payload)
        payload.update({"namespace": K8S_NAMESPACE, "region_name": REGION_NAME, "code": 201})
        values = self.alarm.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.alarm.is_sub_dict(values, result.json()), "审计数据不符合预期"
