import pytest
import base64
from time import sleep
from common.check_status import checker
from common.settings import K8S_NAMESPACE, RESOURCE_PREFIX, DEFAULT_NS, PROJECT_NAME, IMAGE, REGION_NAME, CASE_TYPE, \
    NOTI_USERNAME, NOTI_PWD
from new_case.operation.notification.notification_server.notification_server import Notification_Server
from new_case.operation.notification.notification_object.notification_object import Notification_Object
from new_case.operation.notification.email_sender.email_sender import Email_Sender
from new_case.operation.notification.notification_template.notification_template import Notification_Template
from new_case.operation.notification.new_notification.notification import Notification
from new_case.operation.monitoring.monitoring import Monitoring
from new_case.containerplatform.application.application import Application

create_data_List = [
    {
        "alarm_name": "{}-ares-region-alarm-1".format(RESOURCE_PREFIX),
        "ns_name": DEFAULT_NS,
        "description": "''",
        "cluster_name": REGION_NAME,
        "kind": "Cluster",
        "resource_name": REGION_NAME,
        "project": "''",
        "rules": [{
            "rule_name": "{}-ares-region-rule-1".format(RESOURCE_PREFIX),
            "severity": "Critical",
            "metric_name": "cluster.node.not.ready.count",
            "threshold": "1212",
            "comparison": ">="
        }]
    },
    {
        "alarm_name": "{}-ares-region-alarm-2".format(RESOURCE_PREFIX),
        "ns_name": DEFAULT_NS,
        "description": "region-alarm-2",
        "cluster_name": REGION_NAME,
        "kind": "Cluster",
        "resource_name": REGION_NAME,
        "project": "''",
        "rules": [
            {
                "rule_name": "{}-ares-region-rule-2".format(RESOURCE_PREFIX),
                "metric_name": "cluster.node.total.count",
                "threshold": "99",
                "comparison": ">",
                "wait": "120",
                "_range": "60",
                "function": "avg"
            }
        ]
    },
    {
        "alarm_name": "{}-ares-region-alarm-3".format(RESOURCE_PREFIX),
        "ns_name": DEFAULT_NS,
        "description": "alarm-action",
        "cluster_name": REGION_NAME,
        "kind": "Cluster",
        "resource_name": REGION_NAME,
        "project": "''",
        "rules": [{
            "rule_name": "{}-ares-region-rule-3".format(RESOURCE_PREFIX),
            "severity": "High",
            "metric_name": "custom",
            "threshold": "0.04",
            "comparison": "==",
            "wait": "180",
            "metric": "cluster_memory_MemUsed"
        }]
    },
    {
        "alarm_name": "{}-ares-region-alarm-4".format(RESOURCE_PREFIX),
        "ns_name": DEFAULT_NS,
        "description": "more cluster rule",
        "cluster_name": REGION_NAME,
        "kind": "Cluster",
        "resource_name": REGION_NAME,
        "project": "''",
        "rules": [
            {
                "rule_name": "{}-rule-1".format(RESOURCE_PREFIX),
                "metric_name": "cluster.alerts.firing",
                "threshold": "0",
                "comparison": ">"
            },
            {
                "rule_name": "{}-rule-2".format(RESOURCE_PREFIX),
                "metric_name": "cluster.resource.request.cpu.utilization",
                "threshold": "0",
                "_range": "300",
                "function": "avg",
                "comparison": ">"
            },
            {
                "rule_name": "{}-rule-3".format(RESOURCE_PREFIX),
                "metric_name": "custom",
                "metric": "cluster_cpu_utilization",
                "unit": "core",
                "threshold": "0.02",
                "comparison": ">"
            }
        ]
    },
    {
        "alarm_name": "{}-ares-node-alarm-1".format(RESOURCE_PREFIX),
        "ns_name": DEFAULT_NS,
        "description": "''",
        "cluster_name": REGION_NAME,
        "resource_name": "0.0.0.0",
        "kind": "Node",
        "project": "''",
        "rules": [{
            "rule_name": "{}-ares-node-rule-1".format(RESOURCE_PREFIX),
            "severity": "Low",
            "metric_name": "node.disk.read.bytes",
            "threshold": "0.11",
            "wait": "1800",
            "comparison": "<="
        }]
    },
    {
        "alarm_name": "{}-ares-node-alarm-2".format(RESOURCE_PREFIX),
        "ns_name": DEFAULT_NS,
        "description": "node-alarm-2",
        "cluster_name": REGION_NAME,
        "resource_name": "0.0.0.0",
        "kind": "Node",
        "project": "''",
        "rules": [{
            "rule_name": "{}-ares-node-rule-2".format(RESOURCE_PREFIX),
            "metric_name": "node.disk.utilization",
            "threshold": "88",
            "comparison": "<",
            "wait": "600",
            "_range": "120",
            "function": "max"
        }]
    },
    {
        "alarm_name": "{}-ares-node-alarm-3".format(RESOURCE_PREFIX),
        "ns_name": DEFAULT_NS,
        "description": "node-alarm-3",
        "cluster_name": REGION_NAME,
        "resource_name": "0.0.0.0",
        "kind": "Node",
        "project": "''",
        "rules": [{
            "rule_name": "{}-ares-node-rule-3".format(RESOURCE_PREFIX),
            "metric_name": "custom",
            "metric": "node_cpu",
            "threshold": "999",
            "comparison": "==",
            "wait": "300",
            "test_labels": {"ww": "cdc", "fvf": "bgbgbg"},
            "test_annotations": {"fvf": "gb", "bg": "vfvf"}
        }]

    },
    {
        "alarm_name": "{}-ares-node-alarm-4".format(RESOURCE_PREFIX),
        "ns_name": DEFAULT_NS,
        "description": "alarm-action",
        "cluster_name": REGION_NAME,
        "resource_name": "0.0.0.0",
        "kind": "Node",
        "project": "''",
        "rules": [
            {
                "rule_name": "{}-node-rule1".format(RESOURCE_PREFIX),
                "metric_name": "node.disk.space.utilization",
                "threshold": "13",
                "comparison": ">"
            },
            {
                "rule_name": "{}-node-rule2".format(RESOURCE_PREFIX),
                "metric_name": "node.load.5",
                "threshold": "44",
                "_range": "600",
                "function": "avg",
                "comparison": ">"
            },
            {
                "rule_name": "{}-node-rule3".format(RESOURCE_PREFIX),
                "metric_name": "custom",
                "metric": "node_adadad",
                "threshold": "44",
                "comparison": ">"
            }
        ]
    },
    {
        "alarm_name": "{}-ares-node-alarm-5".format(RESOURCE_PREFIX),
        "ns_name": DEFAULT_NS,
        "description": "alarm-action",
        "cluster_name": REGION_NAME,
        "kind": "Node",
        "project": "''",
        "rules": [
            {
                "rule_name": "{}-node-rule1".format(RESOURCE_PREFIX),
                "metric_name": "node.load.1",
                "threshold": "66",
                "comparison": ">"
            }
        ]
    },
    {
        "alarm_name": "{}-ares-node-alarm-6".format(RESOURCE_PREFIX),
        "ns_name": DEFAULT_NS,
        "description": "more node rule",
        "cluster_name": REGION_NAME,
        "kind": "Node",
        "project": "''",
        "rules": [
            {
                "rule_name": "{}-singlenode-rule1".format(RESOURCE_PREFIX),
                "metric_name": "node.disk.space.utilization",
                "threshold": "444",
                "_range": "180",
                "function": "min",
                "comparison": ">"
            },
            {
                "rule_name": "{}-singlenode-rule2".format(RESOURCE_PREFIX),
                "metric_name": "node.cpu.utilization",
                "threshold": "44",
                "_range": "1800",
                "function": "avg",
                "comparison": ">"
            }
        ]
    },
    {
        "alarm_name": "{}-ares-deploy-alarm-1".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "description": "''",
        "cluster_name": REGION_NAME,
        "kind": "Deployment",
        "project": PROJECT_NAME,
        "rules": [{
            "rule_name": "{}-ares-deploy-rule-1".format(RESOURCE_PREFIX),
            "metric_name": "container.cpu.utilization",
            "threshold": "1",
            "comparison": ">"
        }]
    },
    {
        "alarm_name": "{}-ares-deploy-alarm-2".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "description": "deploy-alarm-2",
        "cluster_name": REGION_NAME,
        "kind": "Deployment",
        "project": PROJECT_NAME,
        "rules": [{
            "rule_name": "{}-ares-deploy-rule-2".format(RESOURCE_PREFIX),
            "severity": "Critical",
            "metric_name": "pod.memory.utilization",
            "threshold": "0.22",
            "comparison": ">=",
            "wait": "120",
            "_range": "60",
            "function": "avg"
        }]
    },
    {
        "alarm_name": "{}-ares-deploy-alarm-3".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "description": "deploy-alarm-3",
        "cluster_name": REGION_NAME,
        "kind": "Deployment",
        "project": PROJECT_NAME,
        "rules": [{
            "rule_name": "{}-ares-deploy-rule-3".format(RESOURCE_PREFIX),
            "severity": "High",
            "metric_name": "workload.log.keyword.count",
            "threshold": "2121212",
            "comparison": "==",
            "_range": "60",
            "test_labels": {"deployaa": "cdc", "vf": "mj"},
            "unit": "items",
            "query": "root"
        }]
    },
    {
        "alarm_name": "{}-ares-deploy-alarm-4".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "description": "deploy-alarm-4",
        "cluster_name": REGION_NAME,
        "kind": "Deployment",
        "project": PROJECT_NAME,
        "rules": [{
            "rule_name": "{}-ares-deploy-rule-4".format(RESOURCE_PREFIX),
            "severity": "Low",
            "metric_name": "workload.event.reason.count",
            "threshold": "123",
            "comparison": "<=",
            "_range": "120",
            "test_annotations": {"events": "aaa", "vf": "mj"},
            "unit": "items",
            "query": "Started"
        }]
    },
    {
        "alarm_name": "{}-ares-deploy-alarm-5".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "description": "alarm-action",
        "cluster_name": REGION_NAME,
        "kind": "Deployment",
        "project": PROJECT_NAME,
        "rules": [{
            "rule_name": "{}-ares-deploy-rule-5".format(RESOURCE_PREFIX),
            "metric_name": "custom",
            "threshold": "212",
            "comparison": "<",
            "wait": "300",
            "metric": "container_cpu_load_average_10s"
        }]
    },
    {
        "alarm_name": "{}-ares-deploy-alarm-6".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "description": "more deploy rule",
        "cluster_name": REGION_NAME,
        "kind": "Deployment",
        "project": PROJECT_NAME,
        "rules": [
            {
                "rule_name": "{}deploy-rule-1".format(RESOURCE_PREFIX),
                "metric_name": "container.network.transmit_bytes",
                "threshold": "45",
                "comparison": ">"
            },
            {
                "rule_name": "{}deploy-rule-2".format(RESOURCE_PREFIX),
                "metric_name": "container.network.transmit_bytes",
                "threshold": "11",
                "_range": "300",
                "function": "max",
                "comparison": ">"
            }
        ]
    }

]
update_data_List = [
    {
        "alarm_name": "{}-ares-region-alarm-1".format(RESOURCE_PREFIX),
        "ns_name": DEFAULT_NS,
        "description": "''",
        "cluster_name": REGION_NAME,
        "resource_name": REGION_NAME,
        "kind": "Cluster",
        "project": "''",
        "rules": [{
            "rule_name": "{}-ares-region-rule-1".format(RESOURCE_PREFIX),
            "metric_name": "cluster.node.not.ready.count",
            "threshold": "3.33",
            "comparison": "=="
        }]
    },
    {
        "alarm_name": "{}-ares-region-alarm-2".format(RESOURCE_PREFIX),
        "ns_name": DEFAULT_NS,
        "description": "update region-alarm-2",
        "cluster_name": REGION_NAME,
        "resource_name": REGION_NAME,
        "kind": "Cluster",
        "project": "''",
        "rules": [
            {
                "rule_name": "{}-ares-region-rule-2".format(RESOURCE_PREFIX),
                "metric_name": "cluster.node.total.count",
                "threshold": "22",
                "comparison": ">=",
                "wait": "180",
                "_range": "120",
                "function": "max"
            }
        ]
    },
    {
        "alarm_name": "{}-ares-region-alarm-3".format(RESOURCE_PREFIX),
        "ns_name": DEFAULT_NS,
        "description": "alarm-action",
        "cluster_name": REGION_NAME,
        "resource_name": REGION_NAME,
        "kind": "Cluster",
        "project": "''",
        "rules": [{
            "rule_name": "{}-ares-region-rule-3".format(RESOURCE_PREFIX),
            "severity": "High",
            "metric_name": "custom",
            "threshold": "4555",
            "comparison": "<=",
            "wait": "300",
            "metric": "cluster_memory_MemUsed"
        }]
    },
    {
        "alarm_name": "{}-ares-region-alarm-4".format(RESOURCE_PREFIX),
        "ns_name": DEFAULT_NS,
        "description": "update more cluster rule",
        "cluster_name": REGION_NAME,
        "resource_name": REGION_NAME,
        "kind": "Cluster",
        "project": "''",
        "rules": [
            {
                "rule_name": "{}-rule-1".format(RESOURCE_PREFIX),
                "metric_name": "cluster.alerts.firing",
                "threshold": "44",
                "comparison": ">"
            },
            {
                "rule_name": "{}-rule-2".format(RESOURCE_PREFIX),
                "metric_name": "cluster.resource.request.cpu.utilization",
                "threshold": "6",
                "_range": "600",
                "function": "min",
                "comparison": ">"
            },
            {
                "rule_name": "{}-rule-3".format(RESOURCE_PREFIX),
                "metric_name": "custom",
                "metric": "cluster_cpu_utilization",
                "unit": "aaa",
                "threshold": "2",
                "comparison": ">"
            }
        ]
    },
    {
        "alarm_name": "{}-ares-node-alarm-1".format(RESOURCE_PREFIX),
        "ns_name": DEFAULT_NS,
        "description": "''",
        "cluster_name": REGION_NAME,
        "resource_name": "0.0.0.0",
        "kind": "Node",
        "project": "''",
        "rules": [{
            "rule_name": "{}-ares-node-rule-1".format(RESOURCE_PREFIX),
            "metric_name": "node.disk.read.bytes",
            "threshold": "6",
            "comparison": "<"
        }]
    },
    {
        "alarm_name": "{}-ares-node-alarm-2".format(RESOURCE_PREFIX),
        "ns_name": DEFAULT_NS,
        "description": "update node-alarm-2",
        "cluster_name": REGION_NAME,
        "resource_name": "0.0.0.0",
        "kind": "Node",
        "project": "''",
        "rules": [{
            "rule_name": "{}-ares-node-rule-2".format(RESOURCE_PREFIX),
            "metric_name": "node.disk.utilization",
            "threshold": "133",
            "comparison": "<=",
            "wait": "120",
            "_range": "1800",
            "function": "avg"
        }]
    },
    {
        "alarm_name": "{}-ares-node-alarm-3".format(RESOURCE_PREFIX),
        "ns_name": DEFAULT_NS,
        "description": "update node-alarm-3",
        "cluster_name": REGION_NAME,
        "resource_name": "0.0.0.0",
        "kind": "Node",
        "project": "''",
        "rules": [{
            "rule_name": "{}-ares-node-rule-3".format(RESOURCE_PREFIX),
            "metric_name": "custom",
            "metric": "node_cpu",
            "threshold": "4",
            "comparison": ">=",
            "wait": "120",
            "test_labels": {"as": "fv", "gb": "wew"},
            "test_annotations": {"fvfvf": "nhn", "vggb": "nhnh"}
        }]
    },
    {
        "alarm_name": "{}-ares-node-alarm-4".format(RESOURCE_PREFIX),
        "ns_name": DEFAULT_NS,
        "description": "update alarm-action",
        "cluster_name": REGION_NAME,
        "resource_name": "0.0.0.0",
        "kind": "Node",
        "project": "''",
        "rules": [
            {
                "rule_name": "{}-node-rule1".format(RESOURCE_PREFIX),
                "metric_name": "node.disk.space.utilization",
                "threshold": "31",
                "comparison": ">"
            },
            {
                "rule_name": "{}-node-rule2".format(RESOURCE_PREFIX),
                "metric_name": "node.load.5",
                "threshold": "56",
                "_range": "180",
                "function": "max",
                "comparison": ">"
            },
            {
                "rule_name": "{}-node-rule3".format(RESOURCE_PREFIX),
                "metric_name": "custom",
                "metric": "adad_jfkvjfvfv",
                "threshold": "77",
                "comparison": ">"
            }
        ]
    },
    {
        "alarm_name": "{}-ares-node-alarm-5".format(RESOURCE_PREFIX),
        "ns_name": DEFAULT_NS,
        "description": "update alarm-action",
        "cluster_name": REGION_NAME,
        "kind": "Node",
        "project": "''",
        "rules": [
            {
                "rule_name": "{}-node-rule1".format(RESOURCE_PREFIX),
                "metric_name": "node.load.1",
                "threshold": "1",
                "comparison": ">"
            }
        ]
    },
    {
        "alarm_name": "{}-ares-node-alarm-6".format(RESOURCE_PREFIX),
        "ns_name": DEFAULT_NS,
        "description": "update more node rule",
        "cluster_name": REGION_NAME,
        "kind": "Node",
        "project": "''",
        "rules": [
            {
                "rule_name": "{}-singlenode-rule1".format(RESOURCE_PREFIX),
                "metric_name": "node.disk.space.utilization",
                "threshold": "5",
                "_range": "120",
                "function": "min",
                "comparison": ">"
            },
            {
                "rule_name": "{}-singlenode-rule2".format(RESOURCE_PREFIX),
                "metric_name": "node.cpu.utilization",
                "threshold": "7",
                "_range": "120",
                "function": "min",
                "comparison": ">"
            }
        ]
    },
    {
        "alarm_name": "{}-ares-deploy-alarm-1".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "description": "''",
        "cluster_name": REGION_NAME,
        "kind": "Deployment",
        "project": PROJECT_NAME,
        "rules": [{
            "rule_name": "{}-ares-deploy-rule-1".format(RESOURCE_PREFIX),
            "metric_name": "container.cpu.utilization",
            "threshold": "3",
            "comparison": ">"
        }]
    },
    {
        "alarm_name": "{}-ares-deploy-alarm-2".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "description": "update deploy-alarm-2",
        "cluster_name": REGION_NAME,
        "kind": "Deployment",
        "project": PROJECT_NAME,
        "rules": [{
            "rule_name": "{}-ares-deploy-rule-2".format(RESOURCE_PREFIX),
            "severity": "Critical",
            "metric_name": "pod.memory.utilization",
            "threshold": "2.2",
            "wait": "180",
            "_range": "60",
            "function": "min",
            "comparison": ">"
        }]
    },
    {
        "alarm_name": "{}-ares-deploy-alarm-3".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "description": "update deploy-alarm-3",
        "cluster_name": REGION_NAME,
        "kind": "Deployment",
        "project": PROJECT_NAME,
        "rules": [{
            "rule_name": "{}-ares-deploy-rule-3".format(RESOURCE_PREFIX),
            "severity": "Low",
            "metric_name": "workload.log.keyword.count",
            "threshold": "12",
            "comparison": "!=",
            "_range": "600",
            "test_labels": {"qwqwqwqw": "cdc", "vf": "mj"},
            "unit": "items",
            "query": "root hehe.txt"
        }]
    },
    {
        "alarm_name": "{}-ares-deploy-alarm-4".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "description": "update deploy-alarm-4",
        "cluster_name": REGION_NAME,
        "kind": "Deployment",
        "project": PROJECT_NAME,
        "rules": [{
            "rule_name": "{}-ares-deploy-rule-4".format(RESOURCE_PREFIX),
            "severity": "Critical",
            "metric_name": "workload.event.reason.count",
            "threshold": "23",
            "comparison": ">=",
            "_range": "600",
            "test_annotations": {"events": "aaa", "vf": "mj"},
            "unit": "items",
            "query": "Started"
        }]
    },
    {
        "alarm_name": "{}-ares-deploy-alarm-5".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "description": "alarm-action",
        "cluster_name": REGION_NAME,
        "kind": "Deployment",
        "project": PROJECT_NAME,
        "rules": [{
            "rule_name": "{}-ares-deploy-rule-5".format(RESOURCE_PREFIX),
            "metric_name": "custom",
            "threshold": "4",
            "comparison": "<=",
            "wait": "1800",
            "metric": "container_cpu_load_average_10s"
        }]
    },
    {
        "alarm_name": "{}-ares-deploy-alarm-6".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "description": "update more deploy rule",
        "cluster_name": REGION_NAME,
        "kind": "Deployment",
        "project": PROJECT_NAME,
        "rules": [
            {
                "rule_name": "{}deploy-rule-1".format(RESOURCE_PREFIX),
                "metric_name": "container.network.transmit_bytes",
                "threshold": "54",
                "comparison": ">"
            },
            {
                "rule_name": "{}deploy-rule-2".format(RESOURCE_PREFIX),
                "metric_name": "container.network.transmit_bytes",
                "threshold": "32",
                "_range": "120",
                "function": "min",
                "comparison": ">"
            }
        ]
    }

]
create_L0_data = [
    {
        "alarm_name": "{}-ares-region-alarm-high".format(RESOURCE_PREFIX),
        "ns_name": DEFAULT_NS,
        "description": "region-L0",
        "cluster_name": REGION_NAME,
        "kind": "Cluster",
        "resource_name": REGION_NAME,
        "project": "''",
        "rules": [{
            "rule_name": "{}-ares-region-rule-high".format(RESOURCE_PREFIX),
            "test_annotations": {"aa": "bb", "cc": "dd"},
            "test_labels": {"ww": "rr", "vv": "qq"},
            "severity": "Critical",
            "metric_name": "cluster.cpu.utilization",
            "threshold": "0.01",
            "comparison": ">"
        }]
    },
    {
        "alarm_name": "{}-ares-node-alarm-high".format(RESOURCE_PREFIX),
        "ns_name": DEFAULT_NS,
        "description": "node-L0",
        "cluster_name": REGION_NAME,
        "kind": "Node",
        "project": "''",
        "rules": [{
            "rule_name": "{}-ares-node-rule-high".format(RESOURCE_PREFIX),
            "test_annotations": {"node11": "node22", "node33": "node44"},
            "test_labels": {"dd": "aa", "xx": "mmjm"},
            "severity": "Medium",
            "metric_name": "node.cpu.utilization",
            "threshold": "0.02",
            "_range": "180",
            "function": "min",
            "comparison": "<",
            "wait": "300"
        }]
    },
    {
        "alarm_name": "{}-ares-deploy-alarm-high".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "description": "deploy-L0",
        "cluster_name": REGION_NAME,
        "kind": "Deployment",
        "project": PROJECT_NAME,
        "rules": [{
            "rule_name": "{}-ares-deploy-rule-high".format(RESOURCE_PREFIX),
            "test_annotations": {"deployaa": "cdc", "vf": "mj"},
            "test_labels": {"vfvf": "bgbg", "ere": "bgbgbgb"},
            "severity": "High",
            "metric_name": "custom",
            "threshold": "189.99",
            "comparison": "==",
            "wait": "600",
            "metric": "container_fs_inodes_free"
        }]
    },
    {
        "alarm_name": "{}-ares-deploy-log-alarm".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "description": "deploy-L0-log-alarm",
        "cluster_name": REGION_NAME,
        "kind": "Deployment",
        "project": PROJECT_NAME,
        "rules": [{
            "rule_name": "{}-ares-deploy-rule-log".format(RESOURCE_PREFIX),
            "test_annotations": {"kl": "vvv", "rrr": "xxx"},
            "test_labels": {"xsxs": "vfvf", "bgbg": "ewewe"},
            "metric_name": "workload.log.keyword.count",
            "threshold": "300",
            "comparison": ">=",
            "unit": "items",
            "query": "root hehe.txt",
            "_range": "120"
        }]
    }
]
update_L0_List = [
    {
        "alarm_name": "{}-ares-region-alarm-high".format(RESOURCE_PREFIX),
        "ns_name": DEFAULT_NS,
        "description": "update-region-L0",
        "cluster_name": REGION_NAME,
        "kind": "Cluster",
        "resource_name": REGION_NAME,
        "project": "''",
        "rules": [{
            "rule_name": "{}-ares-region-rule-high".format(RESOURCE_PREFIX),
            "test_annotations": {"update111": "update222", "up11": "p33"},
            "_range": "60",
            "function": "max",
            "test_labels": {"bb": "ss", "cc": "aaa"},
            "severity": "High",
            "metric_name": "cluster.memory.utilization",
            "threshold": "0.01",
            "wait": "120",
            "comparison": ">"
        }]
    },
    {
        "alarm_name": "{}-ares-node-alarm-high".format(RESOURCE_PREFIX),
        "ns_name": DEFAULT_NS,
        "description": "update-node-L0",
        "cluster_name": REGION_NAME,
        "kind": "Node",
        "project": "''",
        "rules": [{
            "rule_name": "{}-ares-node-rule-high".format(RESOURCE_PREFIX),
            "test_annotations": {"qq": "ss", "vv": "ff"},
            "test_labels": {"z": "aa", "nn": "jj"},
            "severity": "Low",
            "metric_name": "node.disk.io.time",
            "threshold": "10000",
            "_range": "300",
            "function": "avg",
            "comparison": "<",
            "wait": "120",
            "unit": "byte/second"
        }]
    },
    {
        "alarm_name": "{}-ares-deploy-alarm-high".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "description": "update-deploy-L0",
        "cluster_name": REGION_NAME,
        "kind": "Deployment",
        "project": PROJECT_NAME,
        "rules": [{
            "rule_name": "{}-ares-deploy-rule-high".format(RESOURCE_PREFIX),
            "test_annotations": {"kl": "vvv", "rrr": "xxx"},
            "test_labels": {"xsxs": "vfvf", "bgbg": "ewewe"},
            "severity": "Critical",
            "metric_name": "workload.log.keyword.count",
            "threshold": "30",
            "comparison": "!=",
            "unit": "items",
            "query": "root hehe.txt",
            "_range": "180"
        }]
    },
    {
        "alarm_name": "{}-ares-deploy-log-alarm".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "description": "update deploy-L0-event-alarm",
        "cluster_name": REGION_NAME,
        "kind": "Deployment",
        "project": PROJECT_NAME,
        "rules": [{
            "rule_name": "{}-ares-deploy-rule-log".format(RESOURCE_PREFIX),
            "test_annotations": {"kl": "vvv", "rrr": "xxx"},
            "test_labels": {"qwqw": "wqw", "wqw": "wq"},
            "metric_name": "workload.event.reason.count",
            "threshold": "100",
            "comparison": "<=",
            "unit": "items",
            "query": "Started",
            "_range": "600"
        }]
    }
]
l0_create_case_name = ["集群指标告警", "主机指标告警", "应用自定义告警", "应用日志告警"]
l0_update_case_name = ["集群指标告警", "主机指标告警", "应用日志告警", "应用事件告警"]
L1_case_name = ["集群告警-平台指标-数据类型原始值", "集群告警-平台指标-数据类型聚合值", "集群告警-自定义指标", "集群告警-多个规则",
                "主机告警-全部主机-平台指标-数据类型原始值", "主机告警-全部主机-平台指标-数据类型聚合值", "主机告警-全部主机-自定义指标",
                "主机告警-全部主机-多个规则", "主机告警-单个主机-平台指标-数据类型原始值", "主机告警-单个主机-多个规则", "应用告警-平台指标-数据类型原始值",
                "应用告警-平台指标-数据类型聚合值", "应用告警-日志告警", "应用告警-事件告警", "应用告警-自定义指标", "应用告警-多个规则"]


@pytest.fixture(scope="session", autouse=True)
@pytest.mark.skipif(checker.smtp_availabel() is False, reason="邮箱服务器无法访问")
def Prepare_Alarm():
    notification_object = Notification_Object()
    notification_server = Notification_Server()
    notification_template = Notification_Template()
    email_sender = Email_Sender()
    notification = Notification()
    monitoring = Monitoring()
    newapp = Application()
    app_name = "{}-ares-alarm-deploy".format(RESOURCE_PREFIX)
    object_name = "{}-ares-alarm-receiver".format(RESOURCE_PREFIX)
    sender_name = "{}-ares-alarm-sender".format(RESOURCE_PREFIX)
    email_server_name = "{}-ares-alarm-server".format(RESOURCE_PREFIX)
    template_name = "{}-ares-alarm-template".format(RESOURCE_PREFIX)
    notification_name = "{}-ares-alarm-notification".format(RESOURCE_PREFIX)
    ns_name = K8S_NAMESPACE
    # 获取集群主机ip
    results = monitoring.get_list_node()
    assert results.status_code == 200, "获取主机列表失败:{}".format(results.text)
    node_name = monitoring.get_value(results.json(), 'items.0.status.addresses.0.address')
    if CASE_TYPE not in ("delete", "upgrade"):
        newapp.delete_app(ns_name, app_name)
        newapp.check_exists(newapp.common_app_url(ns_name, app_name), 404)
        notification_object.delete_notification_object_jinja2(object_name)
        notification_server.delete_notification_server_jinja2(email_server_name)
        notification_template.delete_notification_temolate_jinja2(template_name)
        notification.delete_notification_jinja2(notification_name)

        # 创建应用
        data = {
            'project_name': PROJECT_NAME,
            'namespace': K8S_NAMESPACE,
            'app_name': app_name,
            'deployment_name': app_name,
            'image': IMAGE,
            'replicas': 1
        }
        create_result = newapp.create_app(
            './test_data/newapp/create_app.jinja2', data
        )
        assert create_result.status_code == 200, "新版应用创建失败 {}".format(create_result.text)
        app_status = newapp.get_app_status(ns_name, app_name, 'Running')
        assert app_status, "创建应用后，验证应用状态出错：app: {} is not running".format(app_name)
        # 添加通知对象receiver
        object_data = {
            "name": object_name,
            "display_name": "this is L0 test",
            "destination": "cdjbcdjbcd@qq.com",
            "namespace": DEFAULT_NS
        }
        create_result = notification_object.create_notification_object_jinja2(
            "./test_data/notification/notification_object.jinja2", object_data)
        assert create_result.status_code == 201, "添加通知使用的通知对象失败:{}".format(create_result.text)
        # 创建通知服务器
        server_data = {
            "notificationserver_name": email_server_name,
            "description": "通知使用的邮件服务器",
            "display_name": "this for email sender",
            "host": "smtp.163.com",
            "port": "25",
            "insecureSkipVerify": "true"
        }
        create_result = notification_server.create_notification_server_jinja2(
            "./test_data/notification/notification_server.jinja2", server_data)
        assert create_result.status_code == 201, "通知服务器创建失败:{}".format(create_result.text)
        sleep(3)
        # 添加通知发送人sender(前提是创建好通知服务器）
        sender_data = {
            "email_sender_name": sender_name,
            "username": str(base64.b64encode(NOTI_USERNAME[0].encode("utf-8")), "utf-8"),
            "password": str(base64.b64encode(NOTI_PWD[0].encode("utf-8")), "utf-8"),
            "notification_server_name": email_server_name,
            "namespace": DEFAULT_NS
        }
        create_result = email_sender.create_email_sender_jinja2(
            "./test_data/notification/email_sender.jinja2", sender_data)
        assert create_result.status_code == 201 or "exists" in create_result.text, "添加通知发送人失败:{}".format(
            create_result.text)
        # 创建邮件通知模板
        template_data = {
            "name": template_name,
            "display_name": "template for notification",
            "description": "template for notification"
        }
        create_result = notification_template.create_notification_temolate_jinja2(
            "./test_data/notification/notification_template.jinja2", template_data)
        assert create_result.status_code == 201, "创建通知模板失败:{}".format(create_result.text)
        # 创建通知
        notification_data = {
            "name": notification_name,
            "display_name": "create notification",
            "description": "create test",
            "receivers": object_name,
            "sender": sender_name,
            "template": template_name,
            "namespace": DEFAULT_NS
        }
        create_result = notification.create_notification_jinja2(
            "./test_data/notification/notification.jinja2", notification_data)
        assert create_result.status_code == 201, "创建通知失败:{}".format(create_result.text)
    prepare_data = {
        "node_name": node_name,
        "app_name": app_name,
        "notification_name": notification_name,
        "notification_namespace": DEFAULT_NS,
        "namespace": K8S_NAMESPACE,
        "region_name": REGION_NAME

    }
    yield prepare_data
    if CASE_TYPE not in ("prepare", "upgrade"):
        notification_object.delete_notification_object_jinja2(object_name)
        notification_server.delete_notification_server_jinja2(email_server_name)
        email_sender.delete_email_sender_jinja2(sender_name)
        notification_template.delete_notification_temolate_jinja2(template_name)
        notification.delete_notification_jinja2(notification_name)
        newapp.delete_app(ns_name, app_name)
