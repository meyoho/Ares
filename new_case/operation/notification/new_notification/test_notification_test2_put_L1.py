import pytest
from common.settings import RERUN_TIMES
from common.check_status import checker
from new_case.operation.notification.new_notification.notification import Notification
from new_case.operation.notification.new_notification.conftest import update_data_list, l1_case_name


@pytest.mark.courier
@pytest.mark.Regression
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=0)
@pytest.mark.skipif(checker.smtp_availabel() is False, reason="邮箱服务器无法访问")
class TestNotificationSuite(object):
    def setup_class(self):
        self.notification = Notification()

    @pytest.mark.parametrize("data", update_data_list, ids=l1_case_name)
    def 测试更新不同类型的通知(self, data, Prepare_notification):
        # update notification
        if data["method"] == "email":
            data.update({"sender": Prepare_notification["sender"]})
        template_name = Prepare_notification[data["method"] + "_template"] if data["method"] in ["email", "sms"] else \
            Prepare_notification["web_template"]
        data.update(
            {"receivers": Prepare_notification[data["method"] + "_receivers"],
             "template": template_name,
             "resourceVersion": self.notification.get_resourceVersion(data['name'])},
        )
        update_result = self.notification.update_notification_jinja2(
            data['name'], "./test_data/notification/notification.jinja2", data)
        assert update_result.status_code == 200, "更新通知{}失败:{}".format(data['name'], update_result.text)
        data.update(
            {"resourceVersion": self.notification.get_resourceVersion(data['name'])})
        value = self.notification.generate_jinja_data(
            "./test_data/notification/notification.jinja2", data)
        assert self.notification.is_sub_dict(value, update_result.json()), \
            "更新通知{}比对数据失败，返回数据:{},期望数据:{}".format(data['name'], update_result.json(), value)
