import sys
from common.base_request import Common
from common.log import logger
from common import settings


class Notification(Common):
    def notification_url(self):
        return 'apis/aiops.alauda.io/v1beta1/namespaces/{}/notifications'.format(settings.DEFAULT_NS)

    def search_notification_url(self, notification_name):
        return 'apis/aiops.alauda.io/v1beta1/namespaces/{}/notifications?limit=20&fieldSelector=metadata.name={}&labelSelector={}/version!=1.0'.format(
            settings.DEFAULT_NS, notification_name, settings.DEFAULT_LABEL)

    def get_notification_url(self, notification_name):
        return 'apis/aiops.alauda.io/v1beta1/namespaces/{}/notifications/{}'.format(settings.DEFAULT_NS,
                                                                                    notification_name)

    def create_notification_jinja2(self, file, data):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.notification_url()
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=url, json=data, params={})

    def update_notification_jinja2(self, notification_name, file, data):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_notification_url(notification_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='PUT', path=url, json=data, params={})

    def get_notification_detail_jinja2(self, notification_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_notification_url(notification_name)
        return self.send(method='GET', path=url, params={})

    def delete_notification_jinja2(self, notification_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_notification_url(notification_name)
        return self.send(method='DELETE', path=url, params={})

    def list_notification(self):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.notification_url()
        payload = ({"limit": 20, "labelSelector": "{}/version!=1.0".format(settings.DEFAULT_LABEL)})
        return self.send(method='GET', path=url, params=payload)

    def search_notification(self, notification_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.search_notification_url(notification_name)
        return self.send(method='GET', path=url, params={})

    def get_resourceVersion(self, notification_name):
        url = self.notification_url()
        payload = {"limit": 20, "labelSelector": "{}/version!=1.0".format(settings.DEFAULT_LABEL)}
        response = self.send(method='GET', path=url, params=payload)
        for items in self.get_value(response.json(), 'items'):
            if self.get_value(items, 'metadata.name') == notification_name:
                return self.get_value(items, 'metadata.resourceVersion')
        return ""
