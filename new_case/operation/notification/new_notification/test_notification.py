import pytest
from common.settings import RERUN_TIMES, CASE_TYPE
from common.check_status import checker
from new_case.operation.notification.new_notification.notification import Notification
from new_case.operation.notification.new_notification.conftest import create_data, update_data


@pytest.mark.courier
@pytest.mark.BAT
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=0)
@pytest.mark.skipif(checker.smtp_availabel() is False, reason="邮箱服务器无法访问")
class TestNotificationSuite(object):
    def setup_class(self):
        self.notification = Notification()
        self.teardown_class(self)

    def teardown_class(self):
        if CASE_TYPE not in ("prepare", "upgrade", "delete"):
            self.notification.delete_notification_jinja2(update_data['name'])

    @pytest.mark.prepare
    def 测试创建有描述的邮件通知(self, Prepare_notification):
        # create notification
        create_data.update(
            {"receivers": Prepare_notification["email_receivers"],
             "sender": Prepare_notification["sender"],
             "template": Prepare_notification["email_template"]})
        create_result = self.notification.create_notification_jinja2(
            "./test_data/notification/notification.jinja2", create_data)
        assert create_result.status_code == 201, "创建通知失败:{}".format(create_result.text)
        value = self.notification.generate_jinja_data(
            "./test_data/notification/notification.jinja2", create_data)
        assert self.notification.is_sub_dict(value, create_result.json()), \
            "创建通知比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

    @pytest.mark.upgrade
    def 测试更新有描述的邮件通知(self, Prepare_notification):
        # update notification
        update_data.update(
            {"resourceVersion": self.notification.get_resourceVersion(update_data['name']),
             "receivers": Prepare_notification["email_receivers"],
             "sender": Prepare_notification["sender"],
             "template": Prepare_notification["email_template"]})
        update_result = self.notification.update_notification_jinja2(
            update_data['name'], "./test_data/notification/notification.jinja2", update_data)
        assert update_result.status_code == 200, "更新通知失败:{}".format(update_result.text)
        update_data.update(
            {"resourceVersion": self.notification.get_resourceVersion(update_data['name'])})
        value = self.notification.generate_jinja_data(
            "./test_data/notification/notification.jinja2", update_data)
        assert self.notification.is_sub_dict(value, update_result.json()), \
            "更新通知比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

    @pytest.mark.upgrade
    def 测试获取有描述的邮件通知详情页(self, Prepare_notification):
        # get notification detail
        update_data.update(
            {"receivers": Prepare_notification["email_receivers"],
             "sender": Prepare_notification["sender"],
             "template": Prepare_notification["email_template"]})
        detail_result = self.notification.get_notification_detail_jinja2(update_data['name'])
        assert detail_result.status_code == 200, "获取通知详情页失败:{}".format(detail_result.text)
        value = self.notification.generate_jinja_data(
            "./test_data/notification/notification.jinja2", update_data)
        assert self.notification.is_sub_dict(value, detail_result.json()), \
            "通知详情页比对数据失败，返回数据:{},期望数据:{}".format(detail_result.json(), value)

    @pytest.mark.upgrade
    def 测试通知列表页(self, Prepare_notification):
        # get notification list
        list_result = self.notification.list_notification()
        assert list_result.status_code == 200, "获取通知列表页失败:{}".format(list_result.text)
        assert update_data['name'] in list_result.text, "创建的通知不在通知列表中"
        content = self.notification.get_k8s_resource_data(list_result.json(), update_data['name'],
                                                          list_key="items")
        update_data.update(
            {"receivers": Prepare_notification["email_receivers"],
             "sender": Prepare_notification["sender"],
             "template": Prepare_notification["email_template"]})
        value = self.notification.generate_jinja_data("./test_data/notification/notification.jinja2", update_data)
        assert self.notification.is_sub_dict(value, content), "通知列表比对数据失败，返回数据:{},期望数据:{}".format(content,
                                                                                                  value)

    @pytest.mark.upgrade
    def 测试有描述的邮件通知列表页搜索(self):
        # search notification
        list_result = self.notification.search_notification(update_data['name'])
        assert list_result.status_code == 200, "通知列表页搜索失败:{}".format(list_result.text)
        assert len(self.notification.get_value(list_result.json(), 'items')) == 1, "通知列表页搜索失败:{}".format(
            list_result.text)

    @pytest.mark.delete
    def 测试有描述的邮件通知删除(self):
        # delete notification
        delete_result = self.notification.delete_notification_jinja2(update_data['name'])
        assert delete_result.status_code == 200, "删除通知失败:{}".format(delete_result.text)
        assert self.notification.check_exists(
            self.notification.get_notification_url(update_data['name']), 404, params="")
