from common import settings
import base64
import pytest
from common.check_status import checker
from new_case.operation.notification.notification_server.notification_server import Notification_Server
from new_case.operation.notification.notification_object.notification_object import Notification_Object
from new_case.operation.notification.email_sender.email_sender import Email_Sender
from new_case.operation.notification.notification_template.notification_template import Notification_Template

create_data = {
    "name": "{}-ares-notification-name".format(settings.RESOURCE_PREFIX),
    "display_name": "create notification",
    "description": "create test",
    "namespace": settings.DEFAULT_NS
}
update_data = {
    "name": "{}-ares-notification-name".format(settings.RESOURCE_PREFIX),
    "display_name": "update notification",
    "description": "update test",
    "namespace": settings.DEFAULT_NS
}
create_data_list = [
    {
        "name": "{}-ares-email1".format(settings.RESOURCE_PREFIX),
        "display_name": "create email notification",
        "method": "email",
        "namespace": settings.DEFAULT_NS
    },
    {
        "name": "{}-ares-sms1".format(settings.RESOURCE_PREFIX),
        "display_name": "create sms notification",
        "method": "sms",
        "sender": "default-sms-sender",
        "namespace": settings.DEFAULT_NS
    },
    {
        "name": "{}-ares-webhook1".format(settings.RESOURCE_PREFIX),
        "display_name": "create webhook notification",
        "method": "webhook",
        "sender": "default-webhook-sender",
        "namespace": settings.DEFAULT_NS
    },
    {
        "name": "{}-ares-dingtalk1".format(settings.RESOURCE_PREFIX),
        "display_name": "create dingtalk notification",
        "method": "dingtalk",
        "namespace": settings.DEFAULT_NS
    },
    {
        "name": "{}-ares-wechat1".format(settings.RESOURCE_PREFIX),
        "display_name": "create wechat notification",
        "method": "wechat",
        "namespace": settings.DEFAULT_NS
    },
    {
        "name": "{}-ares-sms2".format(settings.RESOURCE_PREFIX),
        "display_name": "create sms notification with description",
        "method": "sms",
        "sender": "default-sms-sender",
        "description": "创建有描述的短信通知",
        "namespace": settings.DEFAULT_NS
    },
    {
        "name": "{}-ares-webhook2".format(settings.RESOURCE_PREFIX),
        "display_name": "create webhook notification with description",
        "method": "webhook",
        "sender": "default-webhook-sender",
        "description": "创建有描述的webhook通知",
        "namespace": settings.DEFAULT_NS
    },
    {
        "name": "{}-ares-dingtalk2".format(settings.RESOURCE_PREFIX),
        "display_name": "create dingtalk notification with description",
        "method": "dingtalk",
        "description": "创建有描述的钉钉通知",
        "namespace": settings.DEFAULT_NS
    },
    {
        "name": "{}-ares-wechat2".format(settings.RESOURCE_PREFIX),
        "display_name": "create wechat notification with description",
        "method": "wechat",
        "description": "创建有描述的企业微信通知",
        "namespace": settings.DEFAULT_NS
    }
]
update_data_list = []
for data in create_data_list:
    update_l1_data = data
    update_l1_data.update({"display_name": data["display_name"].replace("create", "update")})
    if "description" in data:
        update_l1_data.update({"description": data["description"].replace("创建", "更新")})
    update_data_list.append(update_l1_data)
l1_case_name = ["无描述的邮件通知", "无描述的短信通知", "无描述的webhook通知", "无描述的钉钉通知",
                "无描述的企业微信通知", "有描述的短信通知", "有描述的webhook通知", "有描述的钉钉通知", "有描述的企业微信通知"]


@pytest.fixture(scope="session", autouse=True)
@pytest.mark.skipif(checker.smtp_availabel() is False, reason="邮箱服务器无法访问")
def Prepare_notification():
    notification_object = Notification_Object()
    notification_server = Notification_Server()
    notification_template = Notification_Template()
    email_sender = Email_Sender()
    email_object_name = "{}-email-receiver".format(settings.RESOURCE_PREFIX)
    sms_object_name = "{}-sms-receiver".format(settings.RESOURCE_PREFIX)
    webhooks_object_name = "{}-webhook-receiver".format(settings.RESOURCE_PREFIX)
    webchat_object_name = "{}-webchat-receiver".format(settings.RESOURCE_PREFIX)
    dingtalk_object_name = "{}-dingtalk-receiver".format(settings.RESOURCE_PREFIX)
    sender_name = "{}-ares-sender".format(settings.RESOURCE_PREFIX)
    email_server_name = "{}-ares-server".format(settings.RESOURCE_PREFIX)
    email_template_name = "{}-email-template".format(settings.RESOURCE_PREFIX)
    sms_template_name = "{}-sms-template".format(settings.RESOURCE_PREFIX)
    web_template_name = "{}-web-template".format(settings.RESOURCE_PREFIX)
    # 通知对象用到的数据
    object_list = [
        {
            "name": email_object_name,
            "display_name": "email object",
            "destination": "email@alauda.io",
            "namespace": settings.DEFAULT_NS
        },
        {
            "name": sms_object_name,
            "display_name": "sms object",
            "destination": "'11111111'",
            "type": "sms",
            "namespace": settings.DEFAULT_NS
        },
        {
            "name": webhooks_object_name,
            "display_name": "webhook object",
            "destination": "https://webhook",
            "type": "webhook",
            "namespace": settings.DEFAULT_NS
        },
        {
            "name": dingtalk_object_name,
            "display_name": "dingtalk object",
            "destination": "https://dingtalk",
            "type": "dingtalk",
            "namespace": settings.DEFAULT_NS
        },
        {
            "name": webchat_object_name,
            "display_name": "webchat object",
            "destination": "https://wechat",
            "type": "wechat",
            "namespace": settings.DEFAULT_NS
        }
    ]
    # 通知模板用到的数据
    template_list = [
        {
            "name": email_template_name,
            "display_name": "email template",
            "subject": "start firing"
        },
        {
            "name": sms_template_name,
            "display_name": "sms template",
            "type": "sms"
        },
        {
            "name": web_template_name,
            "display_name": "webhook/dingtalk/webhat template",
            "type": "other"
        }
    ]
    if settings.CASE_TYPE not in ("delete", "upgrade"):
        # 创建前先删除资源
        for items in object_list:
            notification_object.delete_notification_object_jinja2(items["name"])
        notification_server.delete_notification_server_jinja2(email_server_name)
        for items in template_list:
            notification_template.delete_notification_temolate_jinja2(items["name"])
        # 添加所有类型的通知对象receiver
        for items in object_list:
            create_result = notification_object.create_notification_object_jinja2(
                "./test_data/notification/notification_object.jinja2", items)
            assert create_result.status_code == 201, "添加通知使用的通知对象失败:{}".format(create_result.text)
        # 创建邮件服务器
        server_data = {
            "notificationserver_name": email_server_name,
            "description": "通知使用的邮件服务器",
            "display_name": "this for email sender",
            "host": "smtp.163.com",
            "port": "25",
            "insecureSkipVerify": "true"
        }
        create_result = notification_server.create_notification_server_jinja2(
            "./test_data/notification/notification_server.jinja2", server_data)
        assert create_result.status_code == 201, "通知服务器创建失败:{}".format(create_result.text)
        # 添加邮件发送人sender(前提是创建好邮件服务器）
        sender_data = {
            "email_sender_name": sender_name,
            "username": str(base64.b64encode(settings.NOTI_USERNAME[0].encode("utf-8")), "utf-8"),
            "password": str(base64.b64encode(settings.NOTI_PWD[0].encode("utf-8")), "utf-8"),
            "notification_server_name": email_server_name,
            "namespace": settings.DEFAULT_NS
        }
        create_result = email_sender.create_email_sender_jinja2(
            "./test_data/notification/email_sender.jinja2", sender_data)
        assert create_result.status_code == 201 or "exists" in create_result.text, "创建发送人失败:{}".format(create_result.text)
        # 创建各个类型的通知模板
        for items in template_list:
            create_result = notification_template.create_notification_temolate_jinja2(
                "./test_data/notification/notification_template.jinja2", items)
            assert create_result.status_code == 201, "创建通知模板失败:{}".format(create_result.text)
    prepare_data = {
        "email_receivers": email_object_name,
        "sms_receivers": sms_object_name,
        "webhook_receivers": webhooks_object_name,
        "dingtalk_receivers": webchat_object_name,
        "wechat_receivers": dingtalk_object_name,
        "sender": sender_name,
        "email_template": email_template_name,
        "sms_template": sms_template_name,
        "web_template": web_template_name
    }
    yield prepare_data
    if settings.CASE_TYPE not in ("prepare", "upgrade"):
        for items in object_list:
            notification_object.delete_notification_object_jinja2(items["name"])
        notification_server.delete_notification_server_jinja2(email_server_name)
        for items in template_list:
            notification_template.delete_notification_temolate_jinja2(items["name"])
