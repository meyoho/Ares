import pytest
from common.settings import RERUN_TIMES
from common.check_status import checker
from new_case.operation.notification.new_notification.notification import Notification
from new_case.operation.notification.new_notification.conftest import create_data_list, l1_case_name


@pytest.mark.courier
@pytest.mark.Regression
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=0)
@pytest.mark.skipif(checker.smtp_availabel() is False, reason="邮箱服务器无法访问")
class TestNotificationSuite(object):
    def setup_class(self):
        self.notification = Notification()

    @pytest.mark.parametrize("data", create_data_list, ids=l1_case_name)
    def 测试创建不同类型的通知(self, data, Prepare_notification):
        # 创建之前先删除所有资源
        self.notification.delete_notification_jinja2(data['name'])

        # create notification
        if data["method"] == "email":
            data.update({"sender": Prepare_notification["sender"]})
        template_name = Prepare_notification[data["method"] + "_template"] if data["method"] in ["email", "sms"] else \
            Prepare_notification["web_template"]
        data.update(
            {"receivers": Prepare_notification[data["method"] + "_receivers"],
             "template": template_name})
        create_result = self.notification.create_notification_jinja2(
            "./test_data/notification/notification.jinja2", data)
        assert create_result.status_code == 201, "创建通知{}失败:{}".format(data["name"], create_result.text)
        value = self.notification.generate_jinja_data(
            "./test_data/notification/notification.jinja2", data)
        assert self.notification.is_sub_dict(value, create_result.json()), \
            "创建通知{}比对数据失败，返回数据:{},期望数据:{}".format(data["name"], create_result.json(), value)
