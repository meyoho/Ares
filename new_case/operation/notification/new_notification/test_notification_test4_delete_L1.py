import pytest
from common.check_status import checker
from common.settings import RERUN_TIMES
from new_case.operation.notification.new_notification.notification import Notification
from new_case.operation.notification.new_notification.conftest import update_data_list, l1_case_name


@pytest.mark.courier
@pytest.mark.Regression
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=0)
@pytest.mark.skipif(checker.smtp_availabel() is False, reason="邮箱服务器无法访问")
class TestNotificationSuite(object):
    def setup_class(self):
        self.notification = Notification()

    @pytest.mark.parametrize("data", update_data_list, ids=l1_case_name)
    def 测试删除不同类型的通知(self, data):
        # delete notification
        delete_result = self.notification.delete_notification_jinja2(data['name'])
        assert delete_result.status_code == 200, "删除通知{}失败:{}".format(data['name'], delete_result.text)
        assert self.notification.check_exists(
            self.notification.get_notification_url(data['name']), 404, params="")
