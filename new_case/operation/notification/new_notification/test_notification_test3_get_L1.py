import pytest
from common.settings import RERUN_TIMES
from common.check_status import checker
from new_case.operation.notification.new_notification.notification import Notification
from new_case.operation.notification.new_notification.conftest import update_data_list, l1_case_name


@pytest.mark.courier
@pytest.mark.Regression
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=0)
@pytest.mark.skipif(checker.smtp_availabel() is False, reason="邮箱服务器无法访问")
class TestNotificationSuite(object):
    def setup_class(self):
        self.notification = Notification()

    @pytest.mark.parametrize("data", update_data_list, ids=l1_case_name)
    def 测试获取不同类型的通知详情页(self, data, Prepare_notification):
        # get notification detail
        if data["method"] == "email":
            data.update({"sender": Prepare_notification["sender"]})
        template_name = Prepare_notification[data["method"] + "_template"] if data["method"] in ["email", "sms"] else \
            Prepare_notification["web_template"]
        data.update(
            {"receivers": Prepare_notification[data["method"] + "_receivers"],
             "template": template_name},
        )
        detail_result = self.notification.get_notification_detail_jinja2(data['name'])
        assert detail_result.status_code == 200, "获取通知{}详情页失败:{}".format(data['name'], detail_result.text)
        value = self.notification.generate_jinja_data(
            "./test_data/notification/notification.jinja2", data)
        assert self.notification.is_sub_dict(value, detail_result.json()), \
            "通知{}详情页比对数据失败，返回数据:{},期望数据:{}".format(data['name'], detail_result.json(), value)

    @pytest.mark.parametrize("data", update_data_list, ids=l1_case_name)
    def 测试通知列表页不同类型通知搜索(self, data):
        # search notification
        list_result = self.notification.search_notification(data['name'])
        assert list_result.status_code == 200, "通知列表页搜索通知{}失败:{}".format(data['name'], list_result.text)
        assert len(self.notification.get_value(list_result.json(), 'items')) == 1, "通知列表页搜索通知{}失败:{}".format(
            data['name'], list_result.text)
