from common import settings

create_data_List = [
    {
        "notification_name": "{}-ares-notification-1".format(settings.RESOURCE_PREFIX),
        "display_name": "''",
        "subscriptions": [
            {
                "method": "email",
                "recipient": "'test11@alauda.io'",
                "remark": "''",
                "secret": "''"
            }
        ]
    },
    {
        "notification_name": "{}-ares-notification-2".format(settings.RESOURCE_PREFIX),
        "display_name": "email-1",
        "subscriptions": [
            {
                "method": "email",
                "recipient": "'test22@alauda.io'",
                "remark": "''",
                "secret": "''"
            }
        ]
    },
    {
        "notification_name": "{}-ares-notification-3".format(settings.RESOURCE_PREFIX),
        "display_name": "'email-2'",
        "subscriptions": [
            {
                "method": "email",
                "recipient": "'test33@alauda.io'",
                "remark": "this is email notification",
                "secret": "''"
            }
        ]
    },
    {
        "notification_name": "{}-ares-notification-4".format(settings.RESOURCE_PREFIX),
        "display_name": "''",
        "subscriptions": [
            {
                "method": "sms",
                "recipient": "'16728975623'",
                "remark": "''",
                "secret": "''"
            }
        ]
    },
    {
        "notification_name": "{}-ares-notification-5".format(settings.RESOURCE_PREFIX),
        "display_name": "phone-1",
        "subscriptions": [
            {
                "method": "sms",
                "recipient": "'16728975623'",
                "remark": "''",
                "secret": "''"
            }
        ]
    },
    {
        "notification_name": "{}-ares-notification-6".format(settings.RESOURCE_PREFIX),
        "display_name": "phone-2",
        "subscriptions": [
            {
                "method": "sms",
                "recipient": "'16728975623'",
                "remark": "this is phone notification",
                "secret": "''"
            }
        ]
    },
    {
        "notification_name": "{}-ares-notification-7".format(settings.RESOURCE_PREFIX),
        "display_name": "''",
        "subscriptions": [
            {
                "method": "webhook",
                "recipient": "'cndjfhbvf732873'",
                "remark": "''",
                "secret": "''"
            }
        ]
    },
    {
        "notification_name": "{}-ares-notification-8".format(settings.RESOURCE_PREFIX),
        "display_name": "webhook-1",
        "subscriptions": [
            {
                "method": "webhook",
                "recipient": "'cndjfhbvf732873'",
                "remark": "''",
                "secret": "''"
            }
        ]
    },
    {
        "notification_name": "{}-ares-notification-9".format(settings.RESOURCE_PREFIX),
        "display_name": "webhook-2",
        "subscriptions": [
            {
                "method": "webhook",
                "recipient": "'cndjfhbvf732873'",
                "remark": "this is webhook notification",
                "secret": "''"
            }
        ]
    },
    {
        "notification_name": "{}-ares-notification-10".format(settings.RESOURCE_PREFIX),
        "display_name": "webhook-3",
        "subscriptions": [
            {
                "method": "webhook",
                "recipient": "'cndjfhbvf732873'",
                "remark": "this is webhook notification",
                "secret": "cbdchdbcdbvfvfvfvf"
            }
        ]
    },
    {
        "notification_name": "{}-ares-notification-11".format(settings.RESOURCE_PREFIX),
        "display_name": "''",
        "subscriptions": [
            {
                "method": "dingtalk",
                "recipient": "f5764c7d87ccccb2c68131ba66b9c6546087e8a6e17fca01b1b5c9fdb489cb0c",
                "remark": "''",
                "secret": "''"
            }
        ]
    },
    {
        "notification_name": "{}-ares-notification-12".format(settings.RESOURCE_PREFIX),
        "display_name": "dingtalk-1",
        "subscriptions": [
            {
                "method": "dingtalk",
                "recipient": "'f5764c7d87ccccb2c68131ba66b9c6546087e8a6e17fca01b1b5c9fdb489cb0c'",
                "remark": "''",
                "secret": "''"
            }
        ]
    },
    {
        "notification_name": "{}-ares-notification-13".format(settings.RESOURCE_PREFIX),
        "display_name": "dingtalk-2",
        "subscriptions": [
            {
                "method": "dingtalk",
                "recipient": "'f576432d87aaccb2c68131ba66b9c6546087e8a6e17fca01b1b5c9fdb489cb0c'",
                "remark": "this is dingtalk notification",
                "secret": "''"
            }
        ]
    }
]
update_data_List = [
    {
        "notification_name": create_data_List[0]["notification_name"],
        "display_name": "''",
        "subscriptions": [
            {
                "method": "email",
                "recipient": "'notification11@alauda.io'",
                "remark": "''",
                "secret": "''"
            }
        ]
    },
    {
        "notification_name": create_data_List[1]["notification_name"],
        "display_name": "update-email-1",
        "subscriptions": [
            {
                "method": "email",
                "recipient": "'notification22@alauda.io'",
                "remark": "''",
                "secret": "''"
            }
        ]
    },
    {
        "notification_name": create_data_List[2]["notification_name"],
        "display_name": "'update-email-2'",
        "subscriptions": [
            {
                "method": "email",
                "recipient": "'notification33@alauda.io'",
                "remark": "update this  email notification",
                "secret": "''"
            }
        ]
    },
    {
        "notification_name": create_data_List[3]["notification_name"],
        "display_name": "''",
        "subscriptions": [
            {
                "method": "sms",
                "recipient": "'21789345678'",
                "remark": "''",
                "secret": "''"
            }
        ]
    },
    {
        "notification_name": create_data_List[4]["notification_name"],
        "display_name": "update-phone-1",
        "subscriptions": [
            {
                "method": "sms",
                "recipient": "'16728975623'",
                "remark": "''",
                "secret": "''"
            }
        ]
    },
    {
        "notification_name": create_data_List[5]["notification_name"],
        "display_name": "update-phone-2",
        "subscriptions": [
            {
                "method": "sms",
                "recipient": "'16728975623'",
                "remark": "update this  phone notification",
                "secret": "''"
            }
        ]
    },
    {
        "notification_name": create_data_List[6]["notification_name"],
        "display_name": "''",
        "subscriptions": [
            {
                "method": "webhook",
                "recipient": "'gshcdbjchdsbcjfbvfjvfvfvf'",
                "remark": "''",
                "secret": "''"
            }
        ]
    },
    {
        "notification_name": create_data_List[7]["notification_name"],
        "display_name": "update-webhook-1",
        "subscriptions": [
            {
                "method": "webhook",
                "recipient": "'ccbdjhvbfjdvbfdjkvfvf'",
                "remark": "''",
                "secret": "''"
            }
        ]
    },
    {
        "notification_name": create_data_List[8]["notification_name"],
        "display_name": "update-webhook-2",
        "subscriptions": [
            {
                "method": "webhook",
                "recipient": "'cjkvfdjkvnfkvdfnkbgbgbg'",
                "remark": "update this  webhook notification",
                "secret": "''"
            }
        ]
    },
    {
        "notification_name": create_data_List[9]["notification_name"],
        "display_name": "webhook-3",
        "subscriptions": [
            {
                "method": "webhook",
                "recipient": "'cndjfhbvf732873'",
                "remark": "update cndcdc this  webhook notification",
                "secret": "aaadxdcddcd"
            }
        ]
    },
    {
        "notification_name": create_data_List[10]["notification_name"],
        "display_name": "''",
        "subscriptions": [
            {
                "method": "dingtalk",
                "recipient": "'ui764c7d87ccccb2c68131ba66b9c6546087e8a6e17fca01b1b5c9fdb489cb0c'",
                "remark": "''",
                "secret": "''"
            }
        ]
    },
    {
        "notification_name": create_data_List[11]["notification_name"],
        "display_name": "update-dingtalk-1",
        "subscriptions": [
            {
                "method": "dingtalk",
                "recipient": "'f5764d3d87ccccb2c68131ba66b9c6546087e8a6e17fca01b1b5c9fdb489cb0c'",
                "remark": "''",
                "secret": "''"
            }
        ]
    },
    {
        "notification_name": create_data_List[12]["notification_name"],
        "display_name": "update-dingtalk-2",
        "subscriptions": [
            {
                "method": "dingtalk",
                "recipient": "'f576432d87ccccb2caa131ba66b9c6546087e8a6e17fca01b1b5c9fdb489cb0c'",
                "remark": "update this  dingtalk notification",
                "secret": "''"
            }
        ]
    }
]
create_data = {
    "notificationserver_name": "{}-ares-notificationserver-name".format(settings.RESOURCE_PREFIX),
    "description": "this L0 notificationserver",
    "display_name": "L0 notificationserver",
    "host": "asasasasa",
    "port": "22"
}
update_data = {
    "notificationserver_name": "{}-ares-notificationserver-name".format(settings.RESOURCE_PREFIX),
    "description": "update L0 notificationserver",
    "display_name": "update notificationserver",
    "host": "http://asaadadad",
    "port": "465",
    "ssl": "true",
    "insecureSkipVerify": "true"
}
