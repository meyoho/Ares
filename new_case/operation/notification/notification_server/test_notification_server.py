import pytest
from common.settings import RERUN_TIMES, CASE_TYPE
from new_case.operation.notification.notification_server.notification_server import Notification_Server
from new_case.operation.notification.notification_server.conftest import create_data, update_data


@pytest.mark.courier
@pytest.mark.BAT
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestNotification_ServerSuite(object):
    def setup_class(self):
        self.notification_server = Notification_Server()
        self.teardown_class(self)

    def teardown_class(self):
        if CASE_TYPE not in ("prepare", "upgrade", "delete"):
            self.notification_server.delete_notification_server_jinja2(update_data['notificationserver_name'])

    @pytest.mark.prepare
    def 测试通知服务器创建(self):
        # create notification server
        create_result = self.notification_server.create_notification_server_jinja2(
            "./test_data/notification/notification_server.jinja2", create_data)
        assert create_result.status_code == 201, "通知服务器创建失败:{}".format(create_result.text)
        value = self.notification_server.generate_jinja_data(
            "./test_data/notification/notification_server.jinja2", create_data)
        assert self.notification_server.is_sub_dict(value, create_result.json()), \
            "创建通知服务器比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

    @pytest.mark.upgrade
    def 测试通知服务器更新(self):
        # update notification server
        update_data.update(
            {"resourceVersion": self.notification_server.get_resourceVersion(update_data['notificationserver_name'])})
        update_result = self.notification_server.update_notification_server_jinja2(
            update_data['notificationserver_name'], "./test_data/notification/notification_server.jinja2", update_data)
        assert update_result.status_code == 200, "更新通知服务器失败:{}".format(update_result.text)
        update_data.update(
            {"resourceVersion": self.notification_server.get_resourceVersion(update_data['notificationserver_name'])})
        value = self.notification_server.generate_jinja_data(
            "./test_data/notification/notification_server.jinja2", update_data)
        assert self.notification_server.is_sub_dict(value, update_result.json()), \
            "更新通知服务器比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

    @pytest.mark.upgrade
    def 测试通知服务器详情页(self):
        # get notification  server detail
        detail_result = self.notification_server.get_notification_server_detail(update_data['notificationserver_name'])
        assert detail_result.status_code == 200, "获取通知服务器详情页失败:{}".format(detail_result.text)
        value = self.notification_server.generate_jinja_data(
            "./test_data/notification/notification_server.jinja2", update_data)
        assert self.notification_server.is_sub_dict(value, detail_result.json()), \
            "通知服务器详情页比对数据失败，返回数据:{},期望数据:{}".format(detail_result.json(), value)

    @pytest.mark.upgrade
    def 测试通知服务器列表页(self):
        # get notification server list
        list_result = self.notification_server.list_notification_server()
        assert list_result.status_code == 200, "获取通知服务器列表页失败:{}".format(list_result.text)
        assert update_data['notificationserver_name'] in list_result.text, "创建的通知服务器不在通知列表中"
        content = self.notification_server.get_k8s_resource_data(list_result.json(),
                                                                 update_data['notificationserver_name'],
                                                                 list_key="items")
        value = self.notification_server.generate_jinja_data("./test_data/notification/notification_server.jinja2",
                                                             update_data)
        assert self.notification_server.is_sub_dict(value, content), "通知服务器列表比对数据失败，返回数据:{},期望数据:{}".format(content,
                                                                                                            value)

    @pytest.mark.delete
    def 测试通知服务器删除(self):
        # delete notification server
        delete_result = self.notification_server.delete_notification_server_jinja2(
            update_data['notificationserver_name'])
        assert delete_result.status_code == 200, "删除通知服务器失败:{}".format(delete_result.text)
        assert self.notification_server.check_exists(
            self.notification_server.get_notification_server_url(update_data['notificationserver_name']), 404,
            params="")
