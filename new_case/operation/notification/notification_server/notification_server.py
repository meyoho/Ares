import sys
from common.base_request import Common
from common.log import logger
from common import settings


class Notification_Server(Common):
    def notification_server_url(self):
        return 'apis/aiops.alauda.io/v1beta1/notificationservers'

    def get_notification_server_url(self, notification_server_name):
        return 'apis/aiops.alauda.io/v1beta1/notificationservers/{}'.format(notification_server_name)

    def create_notification_server_jinja2(self, file, data):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.notification_server_url()
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=url, json=data, params={})

    def update_notification_server_jinja2(self, notification_server_name, file, data):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_notification_server_url(notification_server_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='PUT', path=url, json=data, params={})

    def get_notification_server_detail(self, notification_server_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_notification_server_url(notification_server_name)
        return self.send(method='GET', path=url, params={})

    def delete_notification_server_jinja2(self, notification_server_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_notification_server_url(notification_server_name)
        return self.send(method='DELETE', path=url, params={})

    def list_notification_server(self):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.notification_server_url()
        payload = {"labelSelector": "{}/type=email".format(settings.DEFAULT_LABEL)}
        return self.send(method='GET', path=url, params=payload)

    def get_resourceVersion(self, notification_server_name):
        url = self.get_notification_server_url(notification_server_name)
        response = self.send(method='GET', path=url, params={})
        resourceVersion = self.get_value(response.json(), 'metadata.resourceVersion')
        return resourceVersion
