import pytest
from common.settings import RERUN_TIMES
from new_case.operation.notification.notification_object.notification_object import Notification_Object
from new_case.operation.notification.notification_object.conftest import create_data_list, casename_list


@pytest.mark.courier
@pytest.mark.Regression
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=0)
class TestNotification_ObjectPostSuite(object):
    def setup_class(self):
        self.notification_object = Notification_Object()

    @pytest.mark.parametrize("data", create_data_list, ids=casename_list)
    def 测试添加不同类型的通知对象(self, data):
        # create notification object
        create_result = self.notification_object.create_notification_object_jinja2(
            "./test_data/notification/notification_object.jinja2", data)
        assert create_result.status_code == 201, "添加通知对象{}失败:{}".format(data["name"], create_result.text)
        value = self.notification_object.generate_jinja_data(
            "./test_data/notification/notification_object.jinja2", data)
        assert self.notification_object.is_sub_dict(value, create_result.json()), \
            "添加通知对象{}比对数据失败，返回数据:{},期望数据:{}".format(data["name"], create_result.json(), value)
