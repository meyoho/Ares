import sys
from common import settings
from common.base_request import Common
from common.log import logger


class Notification_Object(Common):
    def notification_object_url(self):
        return 'apis/aiops.alauda.io/v1beta1/namespaces/{}/notificationreceivers'.format(settings.DEFAULT_NS)

    def search_notification_object_url(self, type, search_name):
        return 'apis/aiops.alauda.io/v1beta1/namespaces/{}/notificationreceivers?limit=1000&labelSelector={}/{}={}'.format(
            settings.DEFAULT_NS, settings.DEFAULT_LABEL, type, search_name)

    def get_notification_object_url(self, notification_object_name):
        return 'apis/aiops.alauda.io/v1beta1/namespaces/{}/notificationreceivers/{}'.format(settings.DEFAULT_NS,
                                                                                            notification_object_name)

    def create_notification_object_jinja2(self, file, data):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.notification_object_url()
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=url, json=data, params={})

    def update_notification_object_jinja2(self, notification_object_name, file, data):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_notification_object_url(notification_object_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='PUT', path=url, json=data, params={})

    def delete_notification_object_jinja2(self, notification_object_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_notification_object_url(notification_object_name)
        return self.send(method='DELETE', path=url, params={})

    def list_notification_object(self):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.notification_object_url()
        payload = ({"limit": 1000})
        return self.send(method='GET', path=url, params=payload)

    def search_notification_object(self, type, search_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.search_notification_object_url(type, search_name)
        return self.send(method='GET', path=url, params={})

    def get_resourceVersion(self, notification_object_name):
        url = self.notification_object_url()
        payload = {"limit": 1000}
        response = self.send(method='GET', path=url, params=payload)
        for items in self.get_value(response.json(), 'items'):
            if self.get_value(items, 'metadata.name') == notification_object_name:
                return self.get_value(items, 'metadata.resourceVersion')
        return ""
