from common import settings

create_data = {
    "name": "{}-ares-notification-object-name".format(settings.RESOURCE_PREFIX),
    "display_name": "this is L0 test",
    "destination": "cdjbcdjbcd@qq.com",
    "namespace": settings.DEFAULT_NS
}
update_data = {
    "name": "{}-ares-notification-object-name".format(settings.RESOURCE_PREFIX),
    "display_name": "update L0 test",
    "destination": "CDVCDHCVDCD@ALAUADA.IO",
    "namespace": settings.DEFAULT_NS
}
create_data_list = [
    {
        "name": "{}-ares-notification-object-2".format(settings.RESOURCE_PREFIX),
        "display_name": "second object",
        "destination": "'11111111'",
        "type": "sms",
        "namespace": settings.DEFAULT_NS
    },
    {
        "name": "{}-ares-notification-object-3".format(settings.RESOURCE_PREFIX),
        "display_name": "third object",
        "destination": "http://webhook.test",
        "type": "webhook",
        "namespace": settings.DEFAULT_NS
    },
    {
        "name": "{}-ares-notification-object-4".format(settings.RESOURCE_PREFIX),
        "display_name": "fourth object",
        "destination": "https://dingtalk.test",
        "type": "dingtalk",
        "namespace": settings.DEFAULT_NS
    },
    {
        "name": "{}-ares-notification-object-5".format(settings.RESOURCE_PREFIX),
        "display_name": "fifth object",
        "destination": "https://wechat.test",
        "type": "wechat",
        "namespace": settings.DEFAULT_NS
    }
]
update_data_list = [
    {
        "name": create_data_list[0]["name"],
        "display_name": "update second object",
        "destination": "'222222222'",
        "type": "sms",
        "namespace": settings.DEFAULT_NS
    },
    {
        "name": create_data_list[1]["name"],
        "display_name": "update third object",
        "destination": "http://update.webhook.test",
        "type": "webhook",
        "namespace": settings.DEFAULT_NS
    },
    {
        "name": create_data_list[2]["name"],
        "display_name": "update fourth object",
        "destination": "https://update.dingtalk.test",
        "type": "dingtalk",
        "namespace": settings.DEFAULT_NS
    },
    {
        "name": create_data_list[3]["name"],
        "display_name": "update fifth object",
        "destination": "https://update.wechat.test",
        "type": "wechat",
        "namespace": settings.DEFAULT_NS
    }
]
casename_list = ["短信类型的通知对象", "webhook类型的通知对象", "钉钉类型的通知对象", "企业微信类型的通知对象"]
