import pytest
import hashlib
from common.settings import RERUN_TIMES, CASE_TYPE
from new_case.operation.notification.notification_object.notification_object import Notification_Object
from new_case.operation.notification.notification_object.conftest import create_data, update_data


@pytest.mark.courier
@pytest.mark.BAT
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=0)
class TestNotification_ObjectSuite(object):
    def setup_class(self):
        self.notification_object = Notification_Object()
        self.teardown_class(self)

    def teardown_class(self):
        if CASE_TYPE not in ("prepare", "upgrade", "delete"):
            self.notification_object.delete_notification_object_jinja2(update_data['name'])

    @pytest.mark.prepare
    def 测试添加邮件类型的通知对象(self):
        # create notification object
        create_result = self.notification_object.create_notification_object_jinja2(
            "./test_data/notification/notification_object.jinja2", create_data)
        assert create_result.status_code == 201, "添加通知对象失败:{}".format(create_result.text)
        value = self.notification_object.generate_jinja_data(
            "./test_data/notification/notification_object.jinja2", create_data)
        assert self.notification_object.is_sub_dict(value, create_result.json()), \
            "添加通知对象比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

    @pytest.mark.upgrade
    def 测试更新邮件类型的通知对象(self):
        # update notification object
        update_data.update(
            {"resourceVersion": self.notification_object.get_resourceVersion(update_data['name'])})
        update_result = self.notification_object.update_notification_object_jinja2(
            update_data['name'], "./test_data/notification/notification_object.jinja2", update_data)
        assert update_result.status_code == 200, "更新通知对象失败:{}".format(update_result.text)
        update_data.update(
            {"resourceVersion": self.notification_object.get_resourceVersion(update_data['name'])})
        value = self.notification_object.generate_jinja_data(
            "./test_data/notification/notification_object.jinja2", update_data)
        assert self.notification_object.is_sub_dict(value, update_result.json()), \
            "更新通知对象比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

    @pytest.mark.upgrade
    def 测试通知对象列表页(self):
        # get notification object list
        list_result = self.notification_object.list_notification_object()
        assert list_result.status_code == 200, "获取通知对象列表页失败:{}".format(list_result.text)
        assert update_data['name'] in list_result.text, "创建的通知对象不在通知对象列表中"
        content = self.notification_object.get_k8s_resource_data(list_result.json(), update_data['name'],
                                                                 list_key="items")
        value = self.notification_object.generate_jinja_data("./test_data/notification/notification_object.jinja2",
                                                             update_data)
        assert self.notification_object.is_sub_dict(value, content), "通知对象列表比对数据失败，返回数据:{},期望数据:{}".format(content,
                                                                                                           value)

    @pytest.mark.upgrade
    def 测试通知对象列表页用显示名称搜索(self):
        # search by display-name
        list_result = self.notification_object.search_notification_object("display-name-md5", hashlib.md5(
            update_data['display_name'].encode(encoding='UTF-8')).hexdigest())
        assert list_result.status_code == 200, "通知对象列表页用显示名称搜索失败:{}".format(list_result.text)
        assert len(self.notification_object.get_value(list_result.json(), 'items')) == 1, "搜索失败:{}".format(list_result.text)

    @pytest.mark.upgrade
    def 测试通知对象列表页用对象名称搜索(self):
        # search by destination-name
        list_result = self.notification_object.search_notification_object("destination-md5", hashlib.md5(
            update_data['destination'].encode(encoding='UTF-8')).hexdigest())
        assert list_result.status_code == 200, "通知对象列表页用对象名称搜索失败:{}".format(list_result.text)
        assert len(self.notification_object.get_value(list_result.json(), 'items')) == 1, "搜索失败:{}".format(list_result.text)

    @pytest.mark.delete
    def 测试邮件类型的通知对象删除(self):
        # delete notification object
        delete_result = self.notification_object.delete_notification_object_jinja2(update_data['name'])
        assert delete_result.status_code == 200, "删除通知对象失败:{}".format(delete_result.text)
        assert update_data['name'] not in self.notification_object.list_notification_object(), "删除的通知对象还在列表中"
