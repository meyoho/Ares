import pytest
from common.settings import RERUN_TIMES
from new_case.operation.notification.notification_object.notification_object import Notification_Object
from new_case.operation.notification.notification_object.conftest import update_data_list, casename_list


@pytest.mark.courier
@pytest.mark.Regression
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=0)
class TestNotification_ObjectPutSuite(object):
    def setup_class(self):
        self.notification_object = Notification_Object()

    @pytest.mark.parametrize("data", update_data_list, ids=casename_list)
    def 测试更新不同类型的通知对象(self, data):
        # update notification object
        data.update(
            {"resourceVersion": self.notification_object.get_resourceVersion(data['name'])})
        update_result = self.notification_object.update_notification_object_jinja2(
            data['name'], "./test_data/notification/notification_object.jinja2", data)
        assert update_result.status_code == 200, "更新通知对象{}失败:{}".format(data['name'], update_result.text)
        data.update(
            {"resourceVersion": self.notification_object.get_resourceVersion(data['name'])})
        value = self.notification_object.generate_jinja_data(
            "./test_data/notification/notification_object.jinja2", data)
        assert self.notification_object.is_sub_dict(value, update_result.json()), \
            "更新通知对象{}比对数据失败，返回数据:{},期望数据:{}".format(data['name'], update_result.json(), value)
