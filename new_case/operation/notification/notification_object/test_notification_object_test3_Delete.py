import pytest
from common.settings import RERUN_TIMES
from new_case.operation.notification.notification_object.notification_object import Notification_Object
from new_case.operation.notification.notification_object.conftest import update_data_list, casename_list


@pytest.mark.courier
@pytest.mark.Regression
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=0)
class TestNotification_ObjectDeleteSuite(object):
    def setup_class(self):
        self.notification_object = Notification_Object()

    @pytest.mark.parametrize("data", update_data_list, ids=casename_list)
    def 测试不同类型的通知对象删除(self, data):
        # delete notification object
        delete_result = self.notification_object.delete_notification_object_jinja2(data['name'])
        assert delete_result.status_code == 200, "删除通知对象{}失败:{}".format(data['name'], delete_result.text)
        assert data['name'] not in self.notification_object.list_notification_object(), "删除的通知对象还在列表中"
