import sys
from common import settings
from common.base_request import Common
from common.log import logger


class Email_Sender(Common):
    def email_sender_url(self):
        return 'api/v1/namespaces/{}/secrets'.format(settings.DEFAULT_NS)

    def get_email_sender_url(self, email_sender_name):
        return 'api/v1/namespaces/{}/secrets/{}'.format(settings.DEFAULT_NS, email_sender_name)

    def create_email_sender_jinja2(self, file, data):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.email_sender_url()
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=url, json=data, params={})

    def update_email_sender_jinja2(self, email_sender_name, file, data):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_email_sender_url(email_sender_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='PUT', path=url, json=data, params={})

    def delete_email_sender_jinja2(self, email_sender_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_email_sender_url(email_sender_name)
        return self.send(method='DELETE', path=url, params={})

    def list_email_sender(self):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.email_sender_url()
        payload = {"labelSelector": "{}/type=email".format(settings.DEFAULT_LABEL)}
        return self.send(method='GET', path=url, params=payload)

    def get_resourceVersion(self, email_sender_name):
        url = self.email_sender_url()
        payload = {"labelSelector": "{}/type=email".format(settings.DEFAULT_LABEL)}
        response = self.send(method='GET', path=url, params=payload)
        for items in self.get_value(response.json(), 'items'):
            if self.get_value(items, 'metadata.name') == email_sender_name:
                return self.get_value(items, 'metadata.resourceVersion')
        return ""
