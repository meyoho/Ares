import pytest
from common.check_status import checker
from common.settings import RERUN_TIMES, CASE_TYPE
from new_case.operation.notification.email_sender.email_sender import Email_Sender
from new_case.operation.notification.email_sender.conftest import create_data, update_data


@pytest.mark.courier
@pytest.mark.BAT
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
@pytest.mark.skipif(checker.smtp_availabel() is False, reason="邮箱服务器无法访问")
class TestEmail_SenderSuite(object):
    def setup_class(self):
        self.email_sender = Email_Sender()
        self.teardown_class(self)

    def teardown_class(self):
        if CASE_TYPE not in ("prepare", "upgrade", "delete"):
            self.email_sender.delete_email_sender_jinja2(update_data['email_sender_name'])

    @pytest.mark.prepare
    def 测试添加通知发送人(self, Prepare_email):
        # create email sender
        create_data.update({"notification_server_name": Prepare_email["notification_server_name"]})
        create_result = self.email_sender.create_email_sender_jinja2(
            "./test_data/notification/email_sender.jinja2", create_data)
        assert create_result.status_code == 201, "添加通知发送人失败:{}".format(create_result.text)
        value = self.email_sender.generate_jinja_data(
            "./test_data/notification/email_sender.jinja2", create_data)
        assert self.email_sender.is_sub_dict(value, create_result.json()), \
            "添加通知发送人比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

    @pytest.mark.upgrade
    def 测试更新通知发送人(self, Prepare_email):
        # update email sender
        update_data.update(
            {"resourceVersion": self.email_sender.get_resourceVersion(update_data['email_sender_name']),
             "notification_server_name": Prepare_email["notification_server_name"]})
        update_result = self.email_sender.update_email_sender_jinja2(
            update_data['email_sender_name'], "./test_data/notification/email_sender.jinja2", update_data)
        assert update_result.status_code == 200, "更新通知发送人失败:{}".format(update_result.text)
        update_data.update(
            {"resourceVersion": self.email_sender.get_resourceVersion(update_data['email_sender_name'])})
        value = self.email_sender.generate_jinja_data(
            "./test_data/notification/email_sender.jinja2", update_data)
        assert self.email_sender.is_sub_dict(value, update_result.json()), \
            "更新通知发送人比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

    @pytest.mark.upgrade
    def 测试通知发送人列表页(self, Prepare_email):
        # get email sender list
        list_result = self.email_sender.list_email_sender()
        assert list_result.status_code == 200, "获取通知发送人列表页失败:{}".format(list_result.text)
        assert update_data['email_sender_name'] in list_result.text, "创建的通知发送人不在通知列表中"
        content = self.email_sender.get_k8s_resource_data(list_result.json(), update_data['email_sender_name'],
                                                          list_key="items")
        update_data.update(
            {"notification_server_name": Prepare_email["notification_server_name"]})
        value = self.email_sender.generate_jinja_data("./verify_data/notification/email_sender.jinja2", update_data)
        assert self.email_sender.is_sub_dict(value, content), "通知发送人列表比对数据失败，返回数据:{},期望数据:{}".format(content,
                                                                                                     value)

    @pytest.mark.delete
    def 测试通知发送人删除(self):
        # delete email sender
        delete_result = self.email_sender.delete_email_sender_jinja2(update_data['email_sender_name'])
        assert delete_result.status_code == 200, "删除通知发送人失败:{}".format(delete_result.text)
        assert update_data['email_sender_name'] not in self.email_sender.list_email_sender(), "删除的通知发送人还在列表中"
