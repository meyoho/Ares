import base64

import pytest
from common.check_status import checker
from common.settings import (CASE_TYPE, DEFAULT_NS, NOTI_PWD, NOTI_USERNAME,
                             RESOURCE_PREFIX)
from new_case.operation.notification.notification_server.notification_server import \
    Notification_Server

create_data = {
    "email_sender_name": "{}-ares-email-sender-name".format(RESOURCE_PREFIX),
    "username": str(base64.b64encode(NOTI_USERNAME[-1].encode("utf-8")), "utf-8"),
    "password": str(base64.b64encode(NOTI_PWD[-1].encode("utf-8")), "utf-8"),
    "namespace": DEFAULT_NS
}
update_data = {
    "email_sender_name": "{}-ares-email-sender-name".format(RESOURCE_PREFIX),
    "username": str(base64.b64encode(NOTI_USERNAME[-1].encode("utf-8")), "utf-8"),
    "password": str(base64.b64encode(NOTI_PWD[-1].encode("utf-8")), "utf-8"),
    "namespace": DEFAULT_NS
}


@pytest.fixture(scope="session", autouse=True)
@pytest.mark.skipif(checker.smtp_availabel() is False, reason="邮箱服务器无法访问")
def Prepare_email():
    notification_server = Notification_Server()
    notification_server_name = "{}-ares-email-server".format(RESOURCE_PREFIX)
    if CASE_TYPE not in ("delete", "upgrade"):
        notification_server.delete_notification_server_jinja2(notification_server_name)
        create_data = {
            "notificationserver_name": notification_server_name,
            "description": "邮件发送人使用的邮件服务器",
            "display_name": "this for email sender",
            "host": "smtp.163.com",
            "port": "25",
            "insecureSkipVerify": "true"
        }
        create_result = notification_server.create_notification_server_jinja2(
            "./test_data/notification/notification_server.jinja2", create_data)
        assert create_result.status_code == 201, "通知服务器创建失败:{}".format(create_result.text)
    prepare_data = {
        "notification_server_name": notification_server_name
    }
    yield prepare_data
    if CASE_TYPE not in ("prepare", "upgrade"):
        notification_server.delete_notification_server_jinja2(notification_server_name)
