import pytest
from common.settings import RERUN_TIMES
from new_case.operation.notification.notification_template.notification_template import Notification_Template
from new_case.operation.notification.notification_template.conftest import update_data_list, casename_list


@pytest.mark.courier
@pytest.mark.Regression
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=0)
class TestNotification_TemplateSuite(object):
    def setup_class(self):
        self.notification_template = Notification_Template()

    @pytest.mark.parametrize("data", update_data_list, ids=casename_list)
    def 测试不同类型的通知模板详情页(self, data):
        # get notification  template detail
        detail_result = self.notification_template.get_notification_temolate__detail_jinja2(data['name'])
        assert detail_result.status_code == 200, "获取通知模板{}详情页失败:{}".format(data['name'], detail_result.text)
        value = self.notification_template.generate_jinja_data(
            "./test_data/notification/notification_template.jinja2", data)
        assert self.notification_template.is_sub_dict(value, detail_result.json()), \
            "通知模板{}详情页比对数据失败，返回数据:{},期望数据:{}".format(data['name'], detail_result.json(), value)
