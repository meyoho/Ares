from common import settings

create_data = {
    "name": "{}-ares-notification-template-name".format(settings.RESOURCE_PREFIX),
    "display_name": "this is notification template test",
    "description": "just test test",
    "subject": "邮件通知模板"
}
update_data = {
    "name": "{}-ares-notification-template-name".format(settings.RESOURCE_PREFIX),
    "display_name": "update notification template test",
    "description": "update test test",
    "subject": "更新邮件通知模板",
    "content": "更新邮件模板内容"
}
create_data_list = [
    {
        "name": "{}-ares-notification-template-1".format(settings.RESOURCE_PREFIX),
        "display_name": " email notification template",
        "subject": "email alarm"
    },
    {
        "name": "{}-ares-notification-template-2".format(settings.RESOURCE_PREFIX),
        "display_name": "sms notification template",
        "type": "sms"
    },
    {
        "name": "{}-ares-notification-template-3".format(settings.RESOURCE_PREFIX),
        "display_name": "webhook dingtalk webhat notification template",
        "type": "other"
    },
    {
        "name": "{}-ares-notification-template-5".format(settings.RESOURCE_PREFIX),
        "display_name": "sms notification template has description",
        "type": "sms",
        "description": "type is sms"
    },
    {
        "name": "{}-ares-notification-template-6".format(settings.RESOURCE_PREFIX),
        "display_name": "webhook dingtalk webhat notification template has description",
        "type": "other",
        "description": "type is other"
    }
]
update_data_list = [
    {
        "name": create_data_list[0]["name"],
        "display_name": "update email notification template",
        "subject": "告警触发邮件通知了",
        "content": "告警触发邮件通知了,告警了，告警了，告警了"
    },
    {
        "name": create_data_list[1]["name"],
        "display_name": "update sms notification template",
        "type": "sms",
        "content": "告警触发短信通知了,告警了，告警了，告警了"
    },
    {
        "name": create_data_list[2]["name"],
        "display_name": "update webhook dingtalk webhat notification template",
        "type": "other",
        "content": "告警触发webhook/dingding/微信通知了,告警了，告警了，告警了"
    },
    {
        "name": create_data_list[3]["name"],
        "display_name": "update sms notification template has description",
        "type": "sms",
        "description": "update type is sms",
        "content": "有描述的告警触发短信通知了,告警了，告警了，告警了"
    },
    {
        "name": create_data_list[4]["name"],
        "display_name": "update webhook dingtalk webhat notification template has description",
        "type": "other",
        "description": "update type is other",
        "content": "有描述的告警触发webhook/dingding/微信通知了,告警了，告警了，告警了"
    }
]
casename_list = ["无描述的邮件模板", "无描述的短信模板", "无描述的其他模板", "有描述的短信模板", "有描述的其他模板"]
