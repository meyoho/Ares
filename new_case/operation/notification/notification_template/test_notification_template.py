import pytest
from common.settings import RERUN_TIMES, CASE_TYPE
from new_case.operation.notification.notification_template.notification_template import Notification_Template
from new_case.operation.notification.notification_template.conftest import create_data, update_data


@pytest.mark.courier
@pytest.mark.BAT
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=0)
class TestNotification_TemplateSuite(object):
    def setup_class(self):
        self.notification_template = Notification_Template()
        self.teardown_class(self)

    def teardown_class(self):
        if CASE_TYPE not in ("prepare", "upgrade", "delete"):
            self.notification_template.delete_notification_temolate_jinja2(update_data['name'])

    @pytest.mark.prepare
    def 测试添加有描述的邮件通知模板(self):
        # create notification template
        create_result = self.notification_template.create_notification_temolate_jinja2(
            "./test_data/notification/notification_template.jinja2", create_data)
        assert create_result.status_code == 201, "添加通知模板失败:{}".format(create_result.text)
        value = self.notification_template.generate_jinja_data(
            "./test_data/notification/notification_template.jinja2", create_data)
        assert self.notification_template.is_sub_dict(value, create_result.json()), \
            "添加通知模板比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

    @pytest.mark.upgrade
    def 测试更新有描述的邮件通知模板(self):
        # update notification template
        update_data.update(
            {"resourceVersion": self.notification_template.get_resourceVersion(update_data['name'])})
        update_result = self.notification_template.update_notification_temolate_jinja2(
            update_data['name'], "./test_data/notification/notification_template.jinja2", update_data)
        assert update_result.status_code == 200, "更新通知模板失败:{}".format(update_result.text)
        update_data.update(
            {"resourceVersion": self.notification_template.get_resourceVersion(update_data['name'])})
        value = self.notification_template.generate_jinja_data(
            "./test_data/notification/notification_template.jinja2", update_data)
        assert self.notification_template.is_sub_dict(value, update_result.json()), \
            "更新通知模板比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

    @pytest.mark.upgrade
    def 测试有描述的邮件通知模板详情页(self):
        # get notification  template detail
        detail_result = self.notification_template.get_notification_temolate__detail_jinja2(update_data['name'])
        assert detail_result.status_code == 200, "获取通知模板详情页失败:{}".format(detail_result.text)
        value = self.notification_template.generate_jinja_data(
            "./test_data/notification/notification_template.jinja2", update_data)
        assert self.notification_template.is_sub_dict(value, detail_result.json()), \
            "通知模板详情页比对数据失败，返回数据:{},期望数据:{}".format(detail_result.json(), value)

    @pytest.mark.upgrade
    def 测试通知模板列表页(self):
        # get notification template list
        list_result = self.notification_template.list_notification_temolate()
        assert list_result.status_code == 200, "获取通知模板列表页失败:{}".format(list_result.text)
        assert update_data['name'] in list_result.text, "创建的通知模板不在通知对象列表中"
        content = self.notification_template.get_k8s_resource_data(list_result.json(), update_data['name'],
                                                                   list_key="items")
        value = self.notification_template.generate_jinja_data("./test_data/notification/notification_template.jinja2",
                                                               update_data)
        assert self.notification_template.is_sub_dict(value, content), "通知模板列表比对数据失败，返回数据:{},期望数据:{}".format(content,
                                                                                                             value)

    @pytest.mark.delete
    def 测试有描述的邮件通知模板删除(self):
        # delete notification template
        delete_result = self.notification_template.delete_notification_temolate_jinja2(update_data['name'])
        assert delete_result.status_code == 200, "删除通知模板失败:{}".format(delete_result.text)
        assert self.notification_template.check_exists(
            self.notification_template.get_notification_temolate_url(update_data['name']), 404, params="")
