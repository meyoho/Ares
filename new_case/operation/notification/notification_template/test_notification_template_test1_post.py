import pytest
from common.settings import RERUN_TIMES
from new_case.operation.notification.notification_template.notification_template import Notification_Template
from new_case.operation.notification.notification_template.conftest import create_data_list, casename_list


@pytest.mark.courier
@pytest.mark.Regression
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=0)
class TestNotification_TemplateSuite(object):
    def setup_class(self):
        self.notification_template = Notification_Template()

    @pytest.mark.parametrize("data", create_data_list, ids=casename_list)
    def 测试添加不同类型的通知模板(self, data):
        # create notification template
        create_result = self.notification_template.create_notification_temolate_jinja2(
            "./test_data/notification/notification_template.jinja2", data)
        assert create_result.status_code == 201, "添加通知模板{}失败:{}".format(data["name"], create_result.text)
        value = self.notification_template.generate_jinja_data(
            "./test_data/notification/notification_template.jinja2", data)
        assert self.notification_template.is_sub_dict(value, create_result.json()), \
            "添加通知模板{}比对数据失败，返回数据:{},期望数据:{}".format(data["name"], create_result.json(), value)
