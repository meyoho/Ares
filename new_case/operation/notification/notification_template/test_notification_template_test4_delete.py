import pytest
from common.settings import RERUN_TIMES
from new_case.operation.notification.notification_template.notification_template import Notification_Template
from new_case.operation.notification.notification_template.conftest import update_data_list, casename_list


@pytest.mark.courier
@pytest.mark.Regression
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=0)
class TestNotification_TemplateSuite(object):
    def setup_class(self):
        self.notification_template = Notification_Template()

    @pytest.mark.parametrize("data", update_data_list, ids=casename_list)
    def 测试不同类型的通知模板删除(self, data):
        # delete notification template
        delete_result = self.notification_template.delete_notification_temolate_jinja2(data['name'])
        assert delete_result.status_code == 200, "删除通知模板{}失败:{}".format(data['name'], delete_result.text)
        assert self.notification_template.check_exists(
            self.notification_template.get_notification_temolate_url(data['name']), 404, params="")
