import sys
from common.base_request import Common
from common.log import logger


class Notification_Template(Common):
    def notification_temolate_url(self):
        return 'apis/aiops.alauda.io/v1beta1/notificationtemplates'

    def get_notification_temolate_url(self, notification_temolate_name):
        return 'apis/aiops.alauda.io/v1beta1/notificationtemplates/{}'.format(notification_temolate_name)

    def create_notification_temolate_jinja2(self, file, data):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.notification_temolate_url()
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=url, json=data, params={})

    def update_notification_temolate_jinja2(self, notification_temolate_name, file, data):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_notification_temolate_url(notification_temolate_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='PUT', path=url, json=data, params={})

    def get_notification_temolate__detail_jinja2(self, notification_temolate_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_notification_temolate_url(notification_temolate_name)
        return self.send(method='GET', path=url, params={})

    def delete_notification_temolate_jinja2(self, notification_temolate_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_notification_temolate_url(notification_temolate_name)
        return self.send(method='DELETE', path=url, params={})

    def list_notification_temolate(self):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.notification_temolate_url()
        payload = {"limit": 20}
        return self.send(method='GET', path=url, params=payload)

    def get_resourceVersion(self, notification_temolate_name):
        url = self.notification_temolate_url()
        payload = {"limit": 20}
        response = self.send(method='GET', path=url, params=payload)
        for items in self.get_value(response.json(), 'items'):
            if self.get_value(items, 'metadata.name') == notification_temolate_name:
                return self.get_value(items, 'metadata.resourceVersion')
        return ""
