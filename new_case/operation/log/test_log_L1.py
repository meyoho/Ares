import pytest
from new_case.operation.log.log import Log
from common.settings import RERUN_TIMES, K8S_NAMESPACE, REGION_NAME, PROJECT_NAME


@pytest.mark.kibana
@pytest.mark.elasticsearch
@pytest.mark.nevermore
@pytest.mark.lanaya
@pytest.mark.acp_operation
@pytest.mark.log
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=0)
class TestLogSuiteL1(object):
    data_list = {
        "集群名": {
            "clusters": REGION_NAME
        },
        "命名空间": {
            "namespaces": K8S_NAMESPACE
        },
        "应用名称": {
            "applications": ""
        },
        "pod名称": {
            "pod": ""
        },
        "节点": {
            "nodes": ""
        },
        "查询root字符": {
            "query_string": "root"
        },
        "项目名": {
            "projects": PROJECT_NAME
        },
        "路径hehe.txt": {
            "paths": "hehe.txt"
        },
        "sources为container": {
            "sources": "container"
        },
        "sources为kubelet": {
            "sources": "kubelet"
        },
        "sources为docker": {
            "sources": "docker"
        }
    }

    def setup_class(self):
        self.log = Log()

    def 测试搜索日志查询条件_带参数projects(self):
        payload = {"projects": PROJECT_NAME}
        result = self.log.get_logs_type(payload)
        assert result.status_code == 200, "搜索日志查询条件_带参数projects失败{}".format(result.text)
        assert len(result.json()) > 0, "搜索日志查询条件_带参数projects返回数据为空"

    def 测试搜索日志查询条件_带参数projects_namespaces(self):
        payload = {"projects": PROJECT_NAME, "namespaces": K8S_NAMESPACE}
        result = self.log.get_logs_type(payload)
        assert result.status_code == 200, "搜索日志查询条件_带参数projects_namespaces失败{}".format(result.text)
        assert len(result.json()) > 0, "搜索日志查询条件_带参数projects_namespaces返回数据为空"

    @pytest.mark.parametrize("key", data_list)
    def 测试搜索日志_带参数(self, key, prepare_app):
        data = self.data_list[key]
        for key1 in data.keys():
            for key2 in prepare_app.keys():
                if key1 == key2:
                    data[key1] = prepare_app[key2]
        result = self.log.get_logs_search(payload=data)
        assert result.status_code == 200, "搜索日志_带参数{}失败{}".format(key, result.text)
        assert self.log.get_value(result.json(), 'total_items') > 0, "搜索日志_带参数{}返回数据为空".format(data.keys())
        global container_id
        container_id = self.log.get_value(result.json(), "items.0.spec.data.container_id")

    @pytest.mark.parametrize("key", data_list)
    def 测试搜索日志条数_带参数(self, key, prepare_app):
        data = self.data_list[key]
        for key1 in data.keys():
            for key2 in prepare_app.keys():
                if key1 == key2:
                    data[key1] = prepare_app[key2]
        result = self.log.get_logs_aggregations(payload=data)
        assert result.status_code == 200, "搜索日志条数_带参数{}失败{}".format(data.keys(), result.text)
        assert len(self.log.get_value(result.json(), 'items')) > 0, "搜索日志条数_带参数{}返回数据为空".format(data.keys())

    def 测试搜索日志_带参数instances(self):
        result = self.log.get_logs_search(payload={"instances": container_id[0:8]})
        assert result.status_code == 200, "搜索日志_带参数instances失败{}".format(result.text)
        assert self.log.get_value(result.json(), 'total_items') > 0, "搜索日志_带参数container_id返回数据为空"
