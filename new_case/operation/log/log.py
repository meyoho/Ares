import sys
from time import sleep, time
from common.base_request import Common
from common.log import logger
from common.settings import DEFAULT_LABEL


class Log(Common):
    def __init__(self):
        super(Log, self).__init__()
        self.genetate_global_info()
        self.default_ns = "default"

    def get_logs_types_for_platform_url(self):
        return "v4/logs/types"

    def get_logs_search_url(self):
        return "v4/logs/search"

    def get_logs_aggregations_url(self):
        return "v4/logs/aggregations"

    def get_pods_name_url(self, region_name, ns_name, pod_name):
        return "kubernetes/{}/api/v1/namespaces/{}/pods?labelSelector=app.{}/name={}".format(region_name,
                                                                                             ns_name, DEFAULT_LABEL, pod_name)

    def get_pod_node_name_url(self, region_name, ns_name, app_name):
        return "kubernetes/{}/api/v1/namespaces/{}/pods?limit=20&labelSelector=app.{}/name={}.{}".format(
            region_name, ns_name, DEFAULT_LABEL, app_name, ns_name)

    def get_logs_context_url(self, log_index, log_id):
        return "v4/logs/context?size=5&log_index={}&log_type=log&log_id={}".format(log_index, log_id)

    def get_logs_type(self, payload):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        count = 0
        while count < 40:
            count += 1
            url = self.get_logs_types_for_platform_url()
            result = self.send(method='get', path=url, params=payload)
            if result.status_code == 200 and len(result.json()) > 0:
                break
            sleep(3)
        return result

    def get_logs_search(self, payload, cnt=40):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        count = 0
        while count < cnt:
            count += 1
            url = self.get_logs_search_url()
            if "start_time" not in payload.keys():
                payload.update(Common.generate_time_params())
            else:
                payload.update({"end_time": time()})
            payload.update({"pageno": 1, "size": 50})
            result = self.send(method='get', path=url, params=payload)
            if result.status_code == 200 and self.get_value(result.json(), 'total_items') > 0:
                break
            sleep(3)
        return result

    def get_logs_aggregations(self, payload):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        count = 0
        while count < 40:
            count += 1
            url = self.get_logs_aggregations_url()
            payload.update(Common.generate_time_params())
            payload.update({"pageno": 1, "size": 50})
            result = self.send(method='get', path=url, params=payload)
            if result.status_code == 200 and len(self.get_value(result.json(), 'items')) > 0:
                break
            sleep(3)
        return result

    def query_level_key_log(self, payload):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_logs_search_url()
        return self.send(method='get', path=url, params=payload)

    def query_level_key_aggregations(self, payload):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_logs_aggregations_url()
        return self.send(method='get', path=url, params=payload)

    def get_pod_node_name(self, region_name, ns_name, app_name):
        url = self.get_pod_node_name_url(region_name, ns_name, app_name)
        result = self.send(method='get', path=url, params={})
        pod_node_name = self.get_value(result.json(), 'items.0.spec.nodeName')
        return pod_node_name

    def get_logs_context(self, log_index, log_id):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_logs_context_url(log_index, log_id)
        return self.send(method='get', path=url, params={})
