import pytest
from new_case.containerplatform.application.application import Application
from common.settings import K8S_NAMESPACE, PROJECT_NAME, IMAGE, RESOURCE_PREFIX, VM_IPS
from new_case.operation.log.log import Log


@pytest.fixture(scope="session", autouse=True)
def prepare_app():
    newapp = Application()
    log = Log()
    app_name = "{}-ares-logs-ll".format(RESOURCE_PREFIX)
    search_appname = "{}.{}".format(app_name, K8S_NAMESPACE)
    newapp.delete_app(K8S_NAMESPACE, app_name)
    newapp.check_exists(newapp.common_app_url(K8S_NAMESPACE, app_name), 404)
    # 创建应用
    data = {
        'project_name': PROJECT_NAME,
        'namespace': K8S_NAMESPACE,
        'app_name': app_name,
        'deployment_name': app_name,
        'image': IMAGE,
        "file_log_path": "/var/*.txt",
        'replicas': 1
    }
    create_result = newapp.create_app(
        './test_data/newapp/create_app.jinja2', data
    )
    assert create_result.status_code == 200, "新版应用创建失败 {}".format(create_result.text)
    app_status = newapp.get_app_status(K8S_NAMESPACE, app_name, 'Running')
    assert app_status, "创建应用后，验证应用状态出错：app: {} is not running".format(app_name)
    results = newapp.get_app_pod(K8S_NAMESPACE, app_name)
    assert results.status_code == 200, "获取容器组失败{}".format(results.text)
    pod_name = newapp.get_value(results.json(), "items.0.metadata.name")

    prepare_data = {
        "app_name": app_name,
        "search_appname": search_appname,
        "pod": pod_name
    }
    if VM_IPS:
        node_name = log.excute_script(cmd="hostname", ip=VM_IPS.split(';')[0])[1].split("\n")[-1]
        prepare_data["nodes"] = node_name
    yield prepare_data
    newapp.delete_app(K8S_NAMESPACE, app_name)
