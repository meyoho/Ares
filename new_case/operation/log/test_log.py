import pytest
from new_case.operation.log.log import Log
from new_case.containerplatform.application.application import Application


@pytest.mark.kibana
@pytest.mark.elasticsearch
@pytest.mark.nevermore
@pytest.mark.lanaya
@pytest.mark.BAT
@pytest.mark.log
@pytest.mark.flaky(reruns=2, reruns_delay=3)
class TestLogSuite(object):

    def setup_class(self):
        self.log = Log()
        self.newapp = Application()
        self.ns_name = self.log.global_info['$K8S_NAMESPACE']
        self.cluster_name = self.log.global_info['$REGION_NAME']
        self.project_name = self.log.global_info['$PROJECT_NAME']

    def 测试获取日志查询条件(self, prepare_app):
        result = {"flag": True}
        search_app_name = prepare_app["search_appname"]
        list_result = self.log.get_logs_type(payload={})
        result = self.log.update_result(result, list_result.status_code == 200, list_result.text)
        result = self.log.update_result(result, len(list_result.json()) > 0, "获取日志查询条件返回数据是空的")
        result = self.log.update_result(result,
                                        self.log.check_value_in_response(self.log.get_logs_types_for_platform_url(),
                                                                         search_app_name),
                                        "公有服务名称不在查询条件中")
        result = self.log.update_result(result, self.cluster_name in self.log.get_value(list_result.json(), 'items.0.clusters'),
                                        "集群名不在查询结果中")
        assert result['flag'], result

    def 测试获取默认三十分钟以及日志上下文(self):
        list_result = self.log.get_logs_search(payload={})
        assert list_result.status_code == 200, list_result.text
        assert self.log.get_value(list_result.json(), 'total_items') > 0, "获取默认30分钟日志返回数据是空的"
        log_index = self.log.get_value(list_result.json(), 'items.-1.spec.data.log_index')
        log_id = self.log.get_value(list_result.json(), 'items.-1.spec.data.log_id')
        context_result = self.log.get_logs_context(log_index, log_id)
        assert context_result.status_code == 200, context_result.text

    def 测试获取默认日志条数(self):
        list_result = self.log.get_logs_aggregations(payload={})
        assert list_result.status_code == 200, list_result.text
        assert len(self.log.get_value(list_result.json(), 'items')) > 0, "获取默认30分钟日志条数返回数据是空的"
