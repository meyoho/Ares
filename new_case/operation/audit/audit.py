import sys

from common.base_request import Common
from common.log import logger
from time import sleep


class Audit(Common):
    def get_audit_type_url(self):
        return 'v1/kubernetes-audits/types'

    def get_audit_search_url(self):
        return 'v1/kubernetes-audits'

    def get_audit_type(self, payload):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        count = 0
        while count < 20:
            count += 1
            url = self.get_audit_type_url()
            payload.update(self.generate_time_params())
            result = self.send(method='get', path=url, params=payload)
            if result.status_code == 200 and len(self.get_value(result.json(), 'result')) > 0:
                break
            sleep(3)
        return result

    def search_audit(self, payload):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        count = 0
        while count < 20:
            count += 1
            url = self.get_audit_search_url()
            payload.update(self.generate_time_params())
            result = self.send(method='get', path=url, params=payload)
            if result.status_code == 200 and self.get_value(result.json(), 'total_items') > 0:
                break
            sleep(3)
        return result

    def resource_audit_pagination(self, url, query="results.Items.0"):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        params = {"page_size": 1, "page": 1}
        params.update(self.generate_time_params())
        page1 = self.send(method="GET", path=url, params=params)
        if page1.status_code != 200:
            return False
        logger.info("列表1的结果:{}".format(page1.json()))
        name1 = self.get_value(page1.json(), query)
        params.update({"page_size": 1, "page": 2})
        params.update(self.generate_time_params())
        page2 = self.send(method="GET", path=url, params=params)
        if page2.status_code != 200:
            return False
        logger.info("列表2的结果:{}".format(page2.json()))
        name2 = self.get_value(page2.json(), query)
        if name1 == name2:
            return False
        return True
