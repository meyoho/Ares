import pytest
from new_case.operation.audit.audit import Audit
from common.settings import RERUN_TIMES


# @pytest.mark.BAT
@pytest.mark.nortrom
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
@pytest.mark.skip(reason="audit is alpha function")
class TestAuditSuite(object):

    def setup_class(self):
        self.audit = Audit()

    def 测试获取审计查询条件_带参数field_user_name(self):
        payload = {"field": "user_name"}
        result = self.audit.get_audit_type(payload)
        assert result.status_code == 200, "获取审计查询条件_带参数field_user_name失败{}".format(result.text)
        assert len(self.audit.get_value(result.json(), 'result')) > 0, "获取审计查询条件_带参数field返回数据是空"

    def 测试获取审计查询条件_带参数field_resource_name(self):
        payload = {"field": "resource_name"}
        result = self.audit.get_audit_type(payload)
        assert result.status_code == 200, "获取审计查询条件_带参数field_resource_name失败{}".format(result.text)
        assert len(self.audit.get_value(result.json(), 'result')) > 0, "获取审计查询条件_带参数field返回数据是空"

    def 测试获取审计面板(self):
        payload = {"page": 1, "page_size": 20}
        result = self.audit.search_audit(payload)
        assert result.status_code == 200, "获取审计面板失败{}".format(result.text)
        assert self.audit.get_value(result.json(), 'total_items') > 0, "获取审计面板返回数据是空"
