import pytest
from new_case.operation.audit.audit import Audit
from common.settings import RERUN_TIMES, RESOURCE_PREFIX, USERNAME
from new_case.operation.alarm_template.alarm_template import Alarm_Template


@pytest.mark.nortrom
@pytest.mark.Regression
@pytest.mark.acp_operation
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
@pytest.mark.skip(reason="audit is alpha function")
class TestAuditSuite(object):

    def setup_class(self):
        self.audit = Audit()
        self.alarm_template = Alarm_Template()
        self.alarm_template_name = "{}-ares-audit-template".format(RESOURCE_PREFIX)
        self.teardown_class(self)
        # create alarm template
        data = {
            "alarm_template_name": self.alarm_template_name,
            "alarm_name": "test-rule",
            "kind": "cluster",
            "description": "test audit",
            "query_labels": [{"name": "'__name__'", "value": "'cluster.cpu.utilization'"}],
            "comparison": ">=",
            "threshold": 20
        }
        create_result = self.alarm_template.create_alarm_template_jinja2(
            "./test_data/alarm_template/alarm_template.jinja2", data)
        assert create_result.status_code == 201, "创建告警模板失败:{}".format(create_result.text)

    def teardown_class(self):
        self.alarm_template.delete_alarm_template(self.alarm_template_name)

    def 测试获取审计_带参数操作人(self):
        payload = {"user_name": USERNAME, "page": 1, "page_size": 20}
        result = self.audit.search_audit(payload)
        assert result.status_code == 200, "获取审计_带参数操作人失败{}".format(result.text)
        assert self.audit.get_value(result.json(), 'total_items') > 0, "获取审计_带参数操作人返回数据是空"

    def 测试获取审计_带参数操作类型(self):
        payload = {"operation_type": "create", "page": 1, "page_size": 20}
        result = self.audit.search_audit(payload)
        assert result.status_code == 200, "获取审计_带参数操作类型失败{}".format(result.text)
        assert self.audit.get_value(result.json(), 'total_items') > 0, "获取审计_带参数操作类型返回数据是空"

    def 测试获取审计_带参数对象类型(self):
        payload = {"resource_type": "alerttemplates", "page": 1, "page_size": 20}
        result = self.audit.search_audit(payload)
        assert result.status_code == 200, "获取审计_带参数对象类型失败{}".format(result.text)
        assert self.audit.get_value(result.json(), 'total_items') > 0, "获取审计_带参数对象类型返回数据是空"

    def 测试获取审计_带参数操作对象(self):
        payload = {"resource_name": self.alarm_template_name, "page": 1, "page_size": 20}
        result = self.audit.search_audit(payload)
        assert result.status_code == 200, "获取审计_带参数操作对象失败{}".format(result.text)
        assert self.audit.get_value(result.json(), 'total_items') > 0, "获取审计_带参数操作对象返回数据是空"

    def 测试获取审计_带参数操作人_操作类型_对象类型_操作对象(self):
        payload = {"user_name": USERNAME, "operation_type": "create", "resource_type": "alerttemplates",
                   "resource_name": self.alarm_template_name, "page": 1, "page_size": 20}
        result = self.audit.search_audit(payload)
        assert result.status_code == 200, "获取审计_带参数操作人_操作类型_对象类型_操作对象失败{}".format(result.text)
        assert self.audit.get_value(result.json(), 'total_items') > 0, "获取审计_带参数操作人_操作类型_对象类型_操作对象返回数据是空"

    def 测试审计面板分页(self):
        url = self.audit.get_audit_search_url()
        get_result = self.audit.resource_audit_pagination(url)
        assert get_result, "审计面板分页测试失败"
