import pytest
import base64
from common import settings
from new_case.operation.notification.email_sender.email_sender import Email_Sender


@pytest.fixture(scope="session", autouse=True)
def Prepare_Alarm():
    email_sender = Email_Sender()
    ret = email_sender.list_email_sender()
    emails = ret.json()["items"]
    for email in emails:
        username = email["data"]["username"]
        email_name = base64.b64decode(username).decode("utf-8")
        if email_name in settings.NOTI_USERNAME:
            email_sender.delete_email_sender_jinja2(email["metadata"]["name"])
