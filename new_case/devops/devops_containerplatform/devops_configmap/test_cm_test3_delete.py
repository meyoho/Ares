import pytest
from common.settings import RERUN_TIMES
from new_case.devops.devops_containerplatform.containerplatform import GenericApi


@pytest.mark.devopsApiServer
@pytest.mark.devops_app
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestCMDelete(object):

    def setup_class(self):
        self.configmap = GenericApi('configmap')

    def 测试删除多组数据的配置字典(self):
        data = GenericApi.data_cm2_create[0]
        ret = self.configmap.delete_namespace_resource(data["cm_name"])
        assert ret.status_code == 200, "删除{}失败:{}".format(data["cm_name"], ret.text)
