import pytest
from common.settings import RERUN_TIMES
from new_case.devops.devops_containerplatform.containerplatform import GenericApi


@pytest.mark.devopsApiServer
@pytest.mark.devops_app
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestCMGet(object):

    def setup_class(self):
        self.configmap = GenericApi('configmap')

    def 测试搜索多组数据的配置字典(self):
        data = GenericApi.data_cm2_search[0]
        ret = self.configmap.serach_namespace_resource(resource_name=data["cm_name"])
        assert ret.status_code == 200, "没有搜索到{}，{}".format(data["cm_name"], ret.text)
        value = self.configmap.generate_jinja_data("./verify_data/devops_containerplatform/search_cm.jinja2", data=data)
        assert self.configmap.is_sub_dict(value, ret.json()), \
            "搜索多组数据的配置字典对比预期数据失败，实际数据:{},期望数据{}".format(ret.json(), value)
