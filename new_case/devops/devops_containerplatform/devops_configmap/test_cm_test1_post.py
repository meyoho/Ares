import pytest
from common.settings import RERUN_TIMES
from new_case.devops.devops_containerplatform.containerplatform import GenericApi


@pytest.mark.devopsApiServer
@pytest.mark.devops_app
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestCMPost(object):

    def setup_class(self):
        self.configmap = GenericApi('configmap')

    def 测试创建多组数据的配置字典(self):
        # 清空资源
        data = GenericApi.data_cm2_create[0]
        self.configmap.delete_namespace_resource(data["cm_name"])

        ret = self.configmap.create_namespace_resource("test_data/devops_containerplatform/configmap.jinja2", data=data)
        assert ret.status_code == 200, "创建配置字典失败:{}".format(ret.text)

        value = self.configmap.generate_jinja_data("./verify_data/devops_containerplatform/create_cm.jinja2", data=data)
        assert self.configmap.is_sub_dict(value, ret.json()), \
            "创建多组数据的配置字典对比预期数据失败，实际数据:{},期望数据{}".format(ret.json(), value)
