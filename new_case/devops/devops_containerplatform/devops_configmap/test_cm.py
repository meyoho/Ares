import pytest
from common.settings import RERUN_TIMES, K8S_NAMESPACE, RESOURCE_PREFIX, CASE_TYPE
from new_case.devops.devops_containerplatform.containerplatform import GenericApi


@pytest.mark.devopsApiServer
@pytest.mark.BAT
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestCMSuite(object):
    def setup_class(self):
        self.configmap = GenericApi('configmap')
        self.configmap_name = '{}-ares-devops-configmap'.format(RESOURCE_PREFIX)
        self.configmap_ns = K8S_NAMESPACE
        self.data = {"key": "value"}
        self.updata = {"updatecm": "updatecm"}
        self.teardown_class(self)

    def teardown_class(self):
        if CASE_TYPE not in ("prepare", "upgrade", "delete"):
            self.configmap.delete_namespace_resource(self.configmap_name)

    @pytest.mark.prepare
    def 测试devops配置字典增加(self):
        # create configmap
        createconfigmap_result = self.configmap.create_namespace_resource("test_data/devops_containerplatform/configmap.jinja2",
                                                                          {"namespace": self.configmap_ns,
                                                                           "cm_name": self.configmap_name,
                                                                           "data": self.data})
        assert createconfigmap_result.status_code == 200, "创建配置字典失败:{}".format(createconfigmap_result.text)

    @pytest.mark.upgrade
    def 测试devops配置字典列表(self):
        # list configmap
        list_result = self.configmap.list_namespace_resource()
        assert list_result.status_code == 200, list_result.text
        assert self.configmap_name in list_result.text, "列表：新建配置字典不在列表中"

    @pytest.mark.upgrade
    def 测试devops配置字典更新(self):
        # update configmap
        update_result = self.configmap.update_namespace_resource(self.configmap_name,
                                                                 "test_data/devops_containerplatform/configmap.jinja2",
                                                                 {"namespace": self.configmap_ns,
                                                                  "cm_name": self.configmap_name,
                                                                  "data": self.updata})
        assert update_result.status_code == 200, "更新配置字典出错:{}".format(update_result.text)

    @pytest.mark.upgrade
    def 测试devops配置字典详情(self):
        # detail configmap
        detail_result = self.configmap.detail_namespace_resource(self.configmap_name)
        assert detail_result.status_code == 200, detail_result.text
        assert self.configmap.get_value(detail_result.json(),
                                        "data.updatecm") == "updatecm", detail_result.text

    @pytest.mark.delete
    def 测试devops配置字典删除(self):
        # delete configmap
        delete_result = self.configmap.delete_namespace_resource(self.configmap_name)
        assert delete_result.status_code == 200, "删除配置字典失败：{}".format(delete_result.text)
        assert self.configmap.check_exists(
            self.configmap.get_namespace_resource_url(resource_name=self.configmap_name), 404)
