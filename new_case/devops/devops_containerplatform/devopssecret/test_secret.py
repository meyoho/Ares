import pytest
import base64
from new_case.devops.devops_containerplatform.containerplatform import GenericApi
from common import settings
from common.utils import dockerjson


@pytest.mark.devopsApiServer
@pytest.mark.BAT
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class Testsecret(object):
    opaque_value = str(base64.b64encode("opaque_value".encode('utf-8')), 'utf8')
    data_list = [
        {
            "secret_name": "{}-ares-devops-tls-secret".format(settings.RESOURCE_PREFIX),
            "secret_type": "kubernetes.io/tls",
            "tls_crt": str(base64.b64encode("tlscrt".encode('utf-8')), 'utf8'),
            "tls_key": str(base64.b64encode("tlskey".encode('utf-8')), 'utf8')
        },
        {
            "secret_name": "{}-ares-devops-opaque-secret".format(settings.RESOURCE_PREFIX),
            "secret_type": "Opaque",
            "opaque_data": {"opaque_key": opaque_value}
        },
        {
            "secret_name": "{}-ares-devops-ssh-secret".format(settings.RESOURCE_PREFIX),
            "secret_type": "kubernetes.io/ssh-auth",
            "ssh_privatevalue": str(base64.b64encode("value".encode('utf-8')), 'utf8'),
        },
        {
            "secret_name": "{}-ares-devops-base-secret".format(settings.RESOURCE_PREFIX),
            "secret_type": "kubernetes.io/basic-auth",
            "username": str(base64.b64encode("username".encode('utf-8')), 'utf8'),
            "password": str(base64.b64encode("password".encode('utf-8')), 'utf8')
        },
        {
            "secret_name": "{}-ares-devops-docker-secret".format(settings.RESOURCE_PREFIX),
            "secret_type": "kubernetes.io/dockerconfigjson",
            "dockerconfigjson": dockerjson("index.alauda.cn", "alauda", "alauda", "hchan@alauda.io"),
        }
    ]
    casename_list = ["TLS类型", "Opaque类型", "SSH认证类型", "用户名-密码类型", "镜像服务类型"]

    def setup_class(self):
        self.secret_tool = GenericApi("secret")
        self.teardown_class(self)

    def teardown_class(self):
        if settings.CASE_TYPE not in ("prepare", "upgrade", "delete"):
            for data in self.data_list:
                self.secret_tool.delete_namespace_resource(data["secret_name"])

    @pytest.mark.prepare
    @pytest.mark.parametrize("data", data_list, ids=casename_list)
    def 测试devops命名空间下保密字典创建(self, data):
        data.update({"K8S_NAMESPACE": settings.K8S_NAMESPACE})
        # create manager_secret_basic_auth
        ret = self.secret_tool.create_namespace_resource('./test_data/devops_containerplatform/secret.jinja2', data=data)

        assert ret.status_code == 200, "创建{}类型保密字典失败:{}".format(data['secret_type'], ret.text)

    @pytest.mark.upgrade
    def 测试devops命名空间下保密字典分页(self):
        # check pagination
        path = "devops/api/v1/appsecret/{}?cluster={}&sortBy=a,name".format(settings.K8S_NAMESPACE, settings.REGION_NAME)
        pagination_flag = self.secret_tool.resource_pagination(path,
                                                               query="secrets.0.objectMeta.name",
                                                               page_size_key="itemsPerPage")
        assert pagination_flag, "验证平台保密字典列表分页出错，请手动验证"

    @pytest.mark.upgrade
    def 测试devops命名空间下获取保密字典列表(self):
        # get manger_secret list
        ret = self.secret_tool.list_secret()
        assert ret.status_code == 200, "获取保密字典列表失败:{}".format(ret.text)

    @pytest.mark.upgrade
    @pytest.mark.parametrize("data", data_list, ids=casename_list)
    def 测试devops命名空间下更新保密字典(self, data):
        data.update({"K8S_NAMESPACE": settings.K8S_NAMESPACE, "description": "update secret"})
        # update manager_secret_basic_auth
        ret = self.secret_tool.update_namespace_resource(data['secret_name'], './test_data/devops_containerplatform/secret.jinja2',
                                                         data=data)
        assert ret.status_code == 200, "更新{}类型保密字典失败:{}".format(data["secret_type"], ret.text)

    @pytest.mark.upgrade
    @pytest.mark.parametrize("data", data_list, ids=casename_list)
    def 测试devops命名空间下获取保密字典详情(self, data):
        ret = self.secret_tool.detail_namespace_resource(data["secret_name"])
        assert ret.status_code == 200, "获取{}类型的保密字典详情失败{}".format(data["secret_type"], ret.text)
        assert data["secret_name"] in ret.text and "update secret" in ret.text, "{}类型的保密字典更新失败:{}".format(data["secret_type"], ret.text)

    @pytest.mark.delete
    @pytest.mark.parametrize("data", data_list, ids=casename_list)
    def 测试devops命名空间下删除保密字典(self, data):
        ret = self.secret_tool.delete_namespace_resource(data["secret_name"])
        assert ret.status_code == 200, "删除{}类型的保密字典失败:{}".format(data["secret_type"], ret.text)

        assert self.secret_tool.check_exists(
            self.secret_tool.get_namespace_resource_url(resource_name=data["secret_name"]), 404), "删除失败"
