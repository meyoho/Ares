import pytest
from common.settings import RERUN_TIMES
from new_case.devops.devops_containerplatform.containerplatform import GenericApi


@pytest.mark.devopsApiServer
@pytest.mark.devops_app
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestCMGet(object):
    def setup_class(self):
        self.secret = GenericApi('secret')

    def 测试搜索多个key的Qpauqe类型的secret(self):
        data = self.secret.data_secret2_search
        ret = self.secret.search_secret(resource_name=data["cm_name"])
        assert ret.status_code == 200, "没有搜索到{}，{}".format(data["cm_name"], ret.text)
        value = self.secret.generate_jinja_data("./verify_data/devops_containerplatform/search_cm.jinja2", data=data)
        assert self.secret.is_sub_dict(value, ret.json()), \
            "搜索多个key的Qpauqe类型的secret对比预期数据失败，实际数据:{},期望数据{}".format(ret.json(), value)
