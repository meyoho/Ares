import pytest
from common import settings
from new_case.devops.devops_containerplatform.containerplatform import GenericApi


@pytest.mark.devopsApiServer
@pytest.mark.devops_app
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestSecretPost(object):

    def setup_class(self):
        self.secret = GenericApi('secret')

    def 测试创建多个key的Qpauqe类型的secret(self):
        data = self.secret.data_secret2_create

        # 清理资源
        self.secret.delete_namespace_resource(data["secret_name"])
        ret = self.secret.create_namespace_resource("./test_data/devops_containerplatform/secret.jinja2", data=data)
        assert ret.status_code == 200, "创建{}失败：{}".format(data["secret_name"], ret.text)

        data2 = self.secret.data_secret2_check
        value = self.secret.generate_jinja_data("./verify_data/devops_containerplatform/create_secret.jinja2", data=data2)
        assert self.secret.is_sub_dict(value, ret.json()), \
            "创建多个key的Qpauqe类型的secret的返回值和预期结果不一致，期望数据:{},实际数据{}".format(value, ret.json())
