import pytest
from common.settings import RERUN_TIMES
from new_case.devops.devops_containerplatform.containerplatform import GenericApi


@pytest.mark.devopsApiServer
@pytest.mark.devops_app
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestSecretDelete(object):
    def setup_class(self):
        self.secret = GenericApi('secret')

    def 测试删除多个key的Qpauqe类型的secret(self):
        data = self.secret.data_secret2_create

        ret = self.secret.delete_namespace_resource(data["secret_name"])
        assert ret.status_code == 200, "删除{}失败:{}".format(data["secret_name"], ret.text)
