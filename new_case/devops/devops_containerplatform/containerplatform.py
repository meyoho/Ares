import sys
import base64
from common import settings
from common.base_request import Common
from common.log import logger
from common.settings import K8S_NAMESPACE, RESOURCE_PREFIX


class GenericApi(Common):
    def __init__(self, resource_type, region_name=settings.REGION_NAME):
        super(GenericApi, self).__init__()
        self.resource_type = resource_type
        self.region_name = region_name

    # 集群级资源
    def get_region_resource_url(self, resource_name=None):
        return resource_name \
               and 'devops/api/v1/{}/{}?cluster={}'.format(self.resource_type, resource_name, self.region_name) \
               or 'devops/api/v1/{}?cluster={}'.format(self.resource_type, self.region_name)

    # 命名空间级资源
    def get_namespace_resource_url(self, namespace_name=settings.K8S_NAMESPACE, resource_name=None):
        return resource_name and \
               'devops/api/v1/{}/{}/{}?cluster={}'.format(self.resource_type, namespace_name,
                                                          resource_name, self.region_name) \
               or 'devops/api/v1/{}/{}?cluster={}'.format(self.resource_type, namespace_name, self.region_name)

    def search_namespace_resource_url(self, namespace_name=settings.K8S_NAMESPACE, resource_name=None):
        return '/devops/api/v1/{}/{}?cluster={}&filterBy=name,{}'.format(self.resource_type, namespace_name, self.region_name, resource_name)

    def create_namespace_resource(self, file, data, namespace_name=settings.K8S_NAMESPACE):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_namespace_resource_url(namespace_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=url, json=data)

    def list_namespace_resource(self, namespace_name=settings.K8S_NAMESPACE):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_namespace_resource_url(namespace_name)
        return self.send(method='get', path=url)

    def list_secret(self, namespace_name=settings.K8S_NAMESPACE):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        path = 'devops/api/v1/appsecret/{}?cluster={}'.format(namespace_name, self.region_name)
        return self.send(method='get', path=path)

    def detail_namespace_resource(self, resource_name, namespace_name=settings.K8S_NAMESPACE):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_namespace_resource_url(namespace_name, resource_name)
        return self.send(method='get', path=url)

    def update_namespace_resource(self, resource_name, file, data, namespace_name=settings.K8S_NAMESPACE):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_namespace_resource_url(namespace_name, resource_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='put', path=url, json=data)

    def delete_namespace_resource(self, resource_name, namespace_name=settings.K8S_NAMESPACE):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_namespace_resource_url(namespace_name, resource_name)
        return self.send(method='delete', path=url)

    def list_region_resource(self):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_region_resource_url()
        return self.send(method='get', path=url)

    def serach_namespace_resource(self, namespace_name=settings.K8S_NAMESPACE, resource_name=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.search_namespace_resource_url(namespace_name, resource_name)
        return self.send(method='get', path=url)

    def search_secret(self, namespace_name=settings.K8S_NAMESPACE, resource_name=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = 'devops/api/v1/appsecret/{}?cluster={}&filterBy=name,{}'.format(namespace_name, self.region_name, resource_name)
        return self.send(method='get', path=url)

    def start_stop_app(self, resource_name, action, data={}, namespace_name=settings.K8S_NAMESPACE):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        path = "devops/api/v1/applications/{}/{}/{}?cluster={}".format(namespace_name,
                                                                       resource_name, action, self.region_name)
        return self.send(method='put', path=path, json=data)

    def delete_app(self, resource_name, namespace_name=settings.K8S_NAMESPACE):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        path = "devops/api/v1/applications/{}/{}?cluster={}".format(namespace_name,
                                                                    resource_name, self.region_name)
        return self.send(method='delete', path=path)

    def amount_volume(self, deploy_name, container_name, file, data, namespace=settings.K8S_NAMESPACE):
        path = 'devops/api/v1/{}/{}/{}/container/{}/volumeMount?cluster={}'. \
            format(self.resource_type, namespace, deploy_name, container_name, self.region_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=path, json=data)

    def update_container(self, deploy_name, container_name, data, namespace=settings.K8S_NAMESPACE):
        path = 'devops/api/v1/{}/{}/{}/container/{}/resources?cluster={}'. \
            format(self.resource_type, namespace, deploy_name, container_name, self.region_name)
        return self.send(method='put', path=path, json=data)

    def update_app_env(self, deploy_name, container_name, data, namespace=settings.K8S_NAMESPACE):
        path = 'devops/api/v1/{}/{}/{}/container/{}/env?cluster={}'. \
            format(self.resource_type, namespace, deploy_name, container_name, self.region_name)
        return self.send(method='put', path=path, json=data)

    def update_app_network(self, deploy_name, data, namespace=settings.K8S_NAMESPACE):
        path = 'devops/api/v1/{}/{}/{}/network?cluster={}'. \
            format(self.resource_type, namespace, deploy_name, self.region_name)
        return self.send(method='put', path=path, json=data)

    def get_app_version(self, deploy_name, namespace=settings.K8S_NAMESPACE):
        path = 'devops/api/v1/{}/{}/{}/oldreplicaset?cluster={}'. \
            format(self.resource_type, namespace, deploy_name, self.region_name)
        return self.send(method='get', path=path)

    def rollback_app(self, deploy_name, data, namespace=settings.K8S_NAMESPACE):
        path = 'devops/api/v1/{}/{}/{}/actions/rollback?cluster={}'. \
            format(self.resource_type, namespace, deploy_name, self.region_name)
        return self.send(method='post', path=path, json=data)

    def update_app_image(self, deploy_name, container_name, data, namespace=settings.K8S_NAMESPACE):
        path = 'devops/api/v1/{}/{}/{}/container/{}/image?cluster={}'. \
            format(self.resource_type, namespace, deploy_name, container_name, self.region_name)
        return self.send(method='put', path=path, json=data)

    def get_app_yaml(self, app_name, namespace=settings.K8S_NAMESPACE):
        path = 'devops/api/v1/{}/{}/{}/yaml?cluster={}'. \
            format(self.resource_type, namespace, app_name, self.region_name)
        return self.send(method='get', path=path)

    def get_deploy_resourceversion(self, deploy_name):
        ret = self.detail_namespace_resource(deploy_name)
        resourceVersion = self.get_value(ret.json(), 'data.metadata.resourceVersion')
        return resourceVersion

    def get_status(self, url, key, expect_value, delimiter='.', params={}, expect_cnt=30):
        return super(GenericApi, self).get_status(url, key, expect_value, delimiter, params, expect_cnt)

    # configmap data
    data_cm2_create = [
        {
            "namespace": K8S_NAMESPACE,
            "cm_name": '{}-ares-devops-configmap1'.format(RESOURCE_PREFIX),
            "data": {"key": "value", "key1": "value1"}
        }
    ]
    data_cm2_search = [
        {
            "items": "items",
            "namespace": K8S_NAMESPACE,
            "cm_name": '{}-ares-devops-configmap1'.format(RESOURCE_PREFIX),
            "kind": "ConfigMap",
            "key": ["key", "key1"]
        }
    ]
    # secret data
    opaque_value = str(base64.b64encode("value".encode('utf-8')), 'utf8')
    opaque_value1 = str(base64.b64encode("value1".encode('utf-8')), 'utf8')
    data_secret2_create = {
        "secret_name": "{}-ares-devops-opaque-secret1".format(settings.RESOURCE_PREFIX),
        "description": "{}-ares-devops-opaque-secret1".format(settings.RESOURCE_PREFIX),
        "secret_type": "Opaque",
        "opaque_data": {"key": opaque_value, "key1": opaque_value1}
    }
    data_secret2_check = {
        "secret_name": "{}-ares-devops-opaque-secret1".format(settings.RESOURCE_PREFIX),
        "description": "{}-ares-devops-opaque-secret1".format(settings.RESOURCE_PREFIX),
        "namespace": K8S_NAMESPACE,
        "key": ["key", "key1"]
    }
    data_secret2_search = {
        "items": "secrets",
        "namespace": K8S_NAMESPACE,
        "cm_name": "{}-ares-devops-opaque-secret1".format(settings.RESOURCE_PREFIX),
        "kind": "Secret",
        "key": ["key", "key1"]
    }
    # pvc data
    data_pvc_create = [
        {
            "type": "ReadOnlyMany",
            "namespace": K8S_NAMESPACE,
            "pvc_name": "{}-ares-devops-readonly-pvc".format(settings.RESOURCE_PREFIX)
        },
        {
            "type": "ReadWriteMany",
            "namespace": K8S_NAMESPACE,
            "pvc_name": "{}-ares-devops-readwritemany-pvc".format(settings.RESOURCE_PREFIX)
        }
    ]
    pvc_casename_list = ["只读类型", "共享类型"]
