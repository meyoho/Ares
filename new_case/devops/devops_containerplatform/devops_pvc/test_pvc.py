import pytest
from common.settings import RERUN_TIMES, K8S_NAMESPACE, RESOURCE_PREFIX, SC_NAME, CASE_TYPE
from new_case.devops.devops_containerplatform.containerplatform import GenericApi


@pytest.mark.devopsApiServer
@pytest.mark.devops_app
@pytest.mark.BAT
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestPVCSuite(object):
    def setup_class(self):
        self.pvc = GenericApi('persistentvolumeclaim')
        self.pvc_name = '{}-ares-devops-pvc'.format(RESOURCE_PREFIX)
        self.pvc_ns = K8S_NAMESPACE
        self.pvc_type = "ReadWriteOnce"
        self.teardown_class(self)

    def teardown_class(self):
        if CASE_TYPE not in ("prepare", "upgrade", "delete"):
            self.pvc.delete_namespace_resource(self.pvc_name)

    @pytest.mark.prepare
    def 测试devops读写类型pvc增加(self):
        # create pvc
        createpvc_result = self.pvc.create_namespace_resource("test_data/devops_containerplatform/pvc.jinja2",
                                                              {"namespace": self.pvc_ns,
                                                               "pvc_name": self.pvc_name,
                                                               "type": self.pvc_type,
                                                               "scs_name": SC_NAME})
        assert createpvc_result.status_code == 200, "创建pvc失败:{}".format(createpvc_result.text)

    @pytest.mark.upgrade
    def 测试devops_pvc列表(self):
        # list pvc
        list_result = self.pvc.list_namespace_resource()
        assert list_result.status_code == 200, list_result.text
        assert self.pvc_name in list_result.text, "列表：新建配置字典不在列表中"

    @pytest.mark.upgrade
    def 测试devops读写类型pvc详情(self):
        # detail pvc
        detail_result = self.pvc.detail_namespace_resource(self.pvc_name)
        assert detail_result.status_code == 200, detail_result.text

    @pytest.mark.delete
    def 测试devops读写类型pvc删除(self):
        # delete pvc
        delete_result = self.pvc.delete_namespace_resource(self.pvc_name)
        assert delete_result.status_code == 200, "删除配置字典失败：{}".format(delete_result.text)
        assert self.pvc.check_exists(
            self.pvc.get_namespace_resource_url(resource_name=self.pvc_name), 404)
