import pytest
from common.settings import RERUN_TIMES, SC_NAME
from new_case.devops.devops_containerplatform.containerplatform import GenericApi


@pytest.mark.devopsApiServer
@pytest.mark.devops_app
@pytest.mark.devops_api
@pytest.mark.skipif(not SC_NAME, reason="没有存储类，不运行当前用例")
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestPVCGet(object):
    data_list = GenericApi.data_pvc_create

    def setup_class(self):
        self.pvc = GenericApi("persistentvolumeclaim")
        self.scs = GenericApi("storageclass")
        ret = self.scs.list_region_resource()
        self.scs_name = self.scs.get_value(ret.json(), "storageClasses.0.objectMeta.name")

    @pytest.mark.parametrize("data", data_list, ids=GenericApi.pvc_casename_list)
    def 测试搜索存储(self, data):
        data.update({"scs_name": SC_NAME})
        ret = self.pvc.serach_namespace_resource(resource_name=data["pvc_name"])
        assert ret.status_code == 200, "没有搜索到{}，{}".format(data["pvc_name"], ret.text)
        value = self.pvc.generate_jinja_data("./verify_data/devops_containerplatform/search_pvc.jinja2", data=data)
        assert self.pvc.is_sub_dict(value, ret.json()), \
            "搜索存储对比预期数据失败，实际数据:{},期望数据{}".format(ret.json(), value)
