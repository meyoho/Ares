import pytest
from common.settings import RERUN_TIMES, SC_NAME
from new_case.devops.devops_containerplatform.containerplatform import GenericApi


@pytest.mark.devopsApiServer
@pytest.mark.devops_app
@pytest.mark.devops_api
@pytest.mark.skipif(not SC_NAME, reason="没有存储类，不运行当前用例")
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestPVCDelete(object):
    data_list = GenericApi.data_pvc_create

    def setup_class(self):
        self.pvc = GenericApi("persistentvolumeclaim")

    @pytest.mark.parametrize("data", data_list, ids=GenericApi.pvc_casename_list)
    def 测试删除存储(self, data):
        ret = self.pvc.delete_namespace_resource(data["pvc_name"])
        assert ret.status_code == 200, "删除{}失败:{}".format(data["pvc_name"], ret.text)
