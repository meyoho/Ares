import pytest
from common.settings import RERUN_TIMES, SC_NAME
from new_case.devops.devops_containerplatform.containerplatform import GenericApi


@pytest.mark.devopsApiServer
@pytest.mark.devops_app
@pytest.mark.devops_api
@pytest.mark.skipif(not SC_NAME, reason="没有存储类，不运行当前用例")
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestPVCPost(object):
    data_list = GenericApi.data_pvc_create

    def setup_class(self):
        self.pvc = GenericApi("persistentvolumeclaim")

    @pytest.mark.parametrize("data", data_list, ids=GenericApi.pvc_casename_list)
    def 测试添加存储(self, data):
        self.pvc.delete_namespace_resource(data["pvc_name"])
        data.update({"scs_name": SC_NAME})

        ret = self.pvc.create_namespace_resource("./test_data/devops_containerplatform/pvc.jinja2", data=data)
        assert ret.status_code == 200, "创建pvc失败:{}".format(ret.text)
        value = self.pvc.generate_jinja_data("./verify_data/devops_containerplatform/create_pvc.jinja2", data=data)
        assert self.pvc.is_sub_dict(value, ret.json()), \
            "添加存储比对数据失败，期望值:{},实际值;{}".format(value, ret.json())

        pvc_status = self.pvc.get_status(self.pvc.get_namespace_resource_url(resource_name=data["pvc_name"]),
                                         "status", "Bound")
        assert pvc_status, "创建存储状态错误:{}的状态没有被绑定".format(data["pvc_name"])
