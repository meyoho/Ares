import pytest
from common.settings import RERUN_TIMES, K8S_NAMESPACE, IMAGE
from new_case.devops.devops_containerplatform.containerplatform import GenericApi
from new_case.devops.devops_containerplatform.devops_application.conftest import app_create_data


@pytest.mark.devopsApiServer
@pytest.mark.devops_app
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestL1GetDevopsAppSuite(object):
    def setup_class(self):
        self.newapp = GenericApi("applications")
        self.data = app_create_data[0]
        self.app_name = self.data["app_name"]
        self.deploy_name = self.data["deployments"][0]["deploy_name"]
        self.container_name = self.data["deployments"][0]["containers"][0]["container_name"]

    def 测试获取devops应用yaml(self, prepare):
        yaml_ret = self.newapp.get_app_yaml(self.app_name)
        assert yaml_ret.status_code == 200, "获取devops应用YAML失败:{}".format(yaml_ret.text)
        verify_data = {
            "deploy_name": self.deploy_name,
            "container_name": self.container_name,
            "namespace": K8S_NAMESPACE,
            "labels": self.data["deployments"][0]["labels"],
            "envs": self.data["deployments"][0]["containers"][0]["envs"],
            "cm_name": prepare["configmap_name"],
            "secret_name": prepare["secret_name"],
            "devops_secret": prepare["devops_secret_name"],
            "image": IMAGE
        }
        value = self.newapp.generate_jinja_data("./verify_data/devops_containerplatform/get_yaml.jinja2", verify_data)
        assert self.newapp.is_sub_dict(value, yaml_ret.json()), \
            "获取devops应用yaml对比预期数据失败，期望数据:{},实际数据{}".format(value, yaml_ret.json())
