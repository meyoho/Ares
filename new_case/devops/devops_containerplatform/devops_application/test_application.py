import pytest
from common.settings import RERUN_TIMES, K8S_NAMESPACE, IMAGE, RESOURCE_PREFIX, CASE_TYPE
from new_case.devops.devops_containerplatform.containerplatform import GenericApi


@pytest.mark.devopsApiServer
@pytest.mark.devops_app
@pytest.mark.BAT
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=30)
class TestNewApplicationSuite(object):

    def setup_class(self):
        self.newapp = GenericApi("applications")
        self.deploy = GenericApi("deployment")
        self.k8s_namespace = K8S_NAMESPACE
        self.newapp_name = '{}-ares-devops-newapp'.format(RESOURCE_PREFIX)

    def teardown_class(self):
        if CASE_TYPE not in ("prepare", "upgrade", "delete"):
            self.newapp.delete_namespace_resource(self.newapp_name)

    @pytest.mark.prepare
    def 测试创建devops应用(self):
        # 创建前删除一下资源
        self.newapp.delete_namespace_resource(self.newapp_name)

        create_result = self.newapp.create_namespace_resource('test_data/devops_containerplatform/app.jinja2',
                                                              {'app_name': self.newapp_name, "image": IMAGE})
        assert create_result.status_code == 200, "devops应用创建失败 {}".format(create_result.text)
        value = self.newapp.generate_jinja_data("./verify_data/devops_containerplatform/update_pod_num.jinja2",
                                                {"app_name": self.newapp_name, "pod_num": 1,
                                                 "namespace": K8S_NAMESPACE, "image": IMAGE, "action": "Create"})
        assert self.newapp.is_sub_dict(value, create_result.json()), \
            "创建devops应用比对数据失败，期望数据:{},实际数据:{}".format(value, create_result.text)
        # self.newapp.check_value_in_response(self.newapp.get_namespace_resource_url(resource_name=self.newapp_name), "containers")
        app_status = self.newapp.get_status(self.newapp.get_namespace_resource_url(resource_name=self.newapp_name),
                                            "deployments.0.status", 'Succeeded')
        assert app_status, "创建应用后，验证应用状态出错：app: {} is not running".format(self.newapp_name)

    @pytest.mark.upgrade
    def 测试获取devops应用详情(self):
        detail_result = self.newapp.detail_namespace_resource(self.newapp_name)
        assert detail_result.status_code == 200, "获取devops应用详情失败 :{}".format(detail_result.text)
        value = self.newapp.generate_jinja_data("./verify_data/devops_containerplatform/get_app.jinja2",
                                                {"app_name": self.newapp_name, "pod_num": 1,
                                                 "namespace": K8S_NAMESPACE, "image": IMAGE})
        assert self.newapp.is_sub_dict(value, detail_result.json()), \
            "获取devops应用详情比对数据失败，期望数据:{},实际数据:{}".format(value, detail_result.text)

    @pytest.mark.upgrade
    def 测试停止devops应用(self):
        # 停止应用
        stop_result = self.newapp.start_stop_app(self.newapp_name, "stop")
        assert stop_result.status_code == 200, "停止应用失败 {}".format(stop_result.text)

        app_status = self.newapp.get_status(self.newapp.get_namespace_resource_url(resource_name=self.newapp_name),
                                            "deployments.0.status", 'Unknown')
        assert app_status, "停止应用后，验证应用状态出错：app: {} is not Unknown".format(self.newapp_name)

    @pytest.mark.upgrade
    def 测试开始devops应用(self):
        # 开始应用
        start_result = self.newapp.start_stop_app(self.newapp_name, "start")
        assert start_result.status_code == 200, "启动应用失败 {}".format(start_result.text)

        app_status = self.newapp.get_status(self.newapp.get_namespace_resource_url(resource_name=self.newapp_name),
                                            "deployments.0.status", 'Succeeded')
        assert app_status, "启动应用后，验证应用状态出错：app: {} is not running".format(self.newapp_name)

    @pytest.mark.delete
    def 测试删除devops应用(self):
        # 删除应用
        delete_result = self.newapp.delete_app(self.newapp_name)
        assert delete_result.status_code == 200, "删除应用失败 {}".format(delete_result.text)
        value = self.newapp.generate_jinja_data("./verify_data/devops_containerplatform/delete_app.jinja2",
                                                {"deploy_name": self.newapp_name, "app_name": self.newapp_name})
        assert self.newapp.is_sub_list(value, delete_result.json()), \
            "删除应用比对数据失败，期望数据:{},实际数据:{}".format(value, delete_result.text)
        delete_flag = self.newapp.check_exists(self.newapp.get_namespace_resource_url(resource_name=self.newapp_name),
                                               500)
        assert delete_flag, "删除devops应用失败"
