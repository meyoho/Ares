import pytest
from common.settings import RERUN_TIMES, IMAGE
from new_case.devops.devops_containerplatform.containerplatform import GenericApi
from new_case.devops.devops_containerplatform.devops_application.conftest import app_create_data


@pytest.mark.devopsApiServer
@pytest.mark.devops_app
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestL1RollbackDevopsAppSuite(object):
    def setup_class(self):
        self.deploy = GenericApi("deployment")
        self.data = app_create_data[0]
        self.deploy_name = self.data["deployments"][0]["deploy_name"]
        self.container_name = self.data["deployments"][0]["containers"][0]["container_name"]

    def 测试获取devops应用历史版本(self):
        version_ret = self.deploy.get_app_version(self.deploy_name)
        assert version_ret.status_code == 200, "获取应用历史版本失败:{}".format(version_ret.text)
        # verify_data = {
        #     "image": IMAGE,
        #     "deploy_name": self.deploy_name,
        #     "namespace": K8S_NAMESPACE
        # }
        # value = self.deploy.generate_jinja_data("./verify_data/devops_containerplatform/history_version.jinja2", verify_data)
        # assert self.deploy.is_sub_dict(value, version_ret.json()), \
        #     "获取devops应用历史版本对比预期数据失败，期望数据:{},实际数据{}".format(value, version_ret.json())

    def 测试回滚devops应用(self):
        roll_data = {"revision": 1}
        roll_ret = self.deploy.rollback_app(self.deploy_name, roll_data)
        assert roll_ret.status_code == 200, "回滚devops应用失败:{}".format(roll_ret.text)
        get_ret = self.deploy.detail_namespace_resource(self.deploy_name)
        assert get_ret.status_code == 200, "回滚devops应用后获取详情失败:{}".format(get_ret.text)
        image = self.deploy.get_value(get_ret.json(), "data.spec.template.spec.containers.0.image")
        assert image == IMAGE, "期望更新后的镜像是{},实际是{}".format(IMAGE, image)
