import pytest
from common.settings import K8S_NAMESPACE, IMAGE
from new_case.devops.devops_containerplatform.containerplatform import GenericApi
from new_case.devops.devops_containerplatform.devops_application.conftest import app_create_data
import copy
from common.log import logger
from time import sleep


@pytest.mark.devopsApiServer
@pytest.mark.devops_app
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=0, reruns_delay=3)
class notTestL1UpdateDevopsAppSuite(object):
    def setup_class(self):
        self.newapp = GenericApi("applications")
        self.deploy = GenericApi("deployment")
        self.data = copy.deepcopy(app_create_data[0])
        self.deploy_name = self.data["deployments"][0]["deploy_name"]
        self.container_name = self.data["deployments"][0]["containers"][0]["container_name"]

    def 测试更新devops应用增加pod数量(self):
        sleep(30)
        resourceVersion = self.deploy.get_deploy_resourceversion(self.deploy_name)
        update_result = self.newapp.update_namespace_resource(self.deploy_name,
                                                              'test_data/devops_containerplatform/app.jinja2',
                                                              {'app_name': self.deploy_name, "replicas": 2,
                                                               "namespace": K8S_NAMESPACE, "image": IMAGE,
                                                               "resourceVersion": resourceVersion})
        assert update_result.status_code == 200, "更新devops应用失败 {}".format(update_result.text)
        logger.info("update app response: {}".format(update_result.json()))
        value = self.newapp.generate_jinja_data("./verify_data/devops_containerplatform/update_pod_num.jinja2",
                                                {"app_name": self.deploy_name, "pod_num": 2,
                                                 "namespace": K8S_NAMESPACE, "image": IMAGE})
        assert self.newapp.is_sub_dict(value, update_result.json()), \
            "更新devops应用增加pod数量比对数据失败，期望数据:{},实际数据:{}".format(value, update_result.text)

        app_status = self.newapp.get_status(self.newapp.get_namespace_resource_url(resource_name=self.deploy_name),
                                            "deployments.0.status", 'Succeeded')
        assert app_status, "更新应用后，验证应用状态出错：app: {} is not running".format(self.deploy_name)
        sleep(30)

    def 测试增加pod数量后获取devops应用详情(self):
        detail_result = self.newapp.detail_namespace_resource(self.deploy_name)
        assert detail_result.status_code == 200, "获取devops应用详情失败 :{}".format(detail_result.text)
        value = self.newapp.generate_jinja_data("./verify_data/devops_containerplatform/get_app.jinja2",
                                                {"app_name": self.deploy_name, "pod_num": 2,
                                                 "namespace": K8S_NAMESPACE, "image": IMAGE})
        assert self.newapp.is_sub_dict(value, detail_result.json()), \
            "增加pod数量后获取devops应用详情比对数据失败，期望数据:{},实际数据:{}".format(value, detail_result.text)

    def 测试更新devops应用减少pod数量(self):
        resourceVersion = self.deploy.get_deploy_resourceversion(self.deploy_name)
        update_result = self.newapp.update_namespace_resource(self.deploy_name,
                                                              'test_data/devops_containerplatform/app.jinja2',
                                                              {'app_name': self.deploy_name, "replicas": 1,
                                                               "namespace": K8S_NAMESPACE, "image": IMAGE,
                                                               "resourceVersion": resourceVersion})
        assert update_result.status_code == 200, "更新devops应用失败 {}".format(update_result.text)
        logger.info("update app response: {}".format(update_result.json()))
        value = self.newapp.generate_jinja_data("./verify_data/devops_containerplatform/update_pod_num.jinja2",
                                                {"app_name": self.deploy_name, "pod_num": 1,
                                                 "namespace": K8S_NAMESPACE, "image": IMAGE})
        assert self.newapp.is_sub_dict(value, update_result.json()), \
            "更新devops应用减少pod数量比对数据失败，期望数据:{},实际数据:{}".format(value, update_result.text)

        app_status = self.newapp.get_status(self.newapp.get_namespace_resource_url(resource_name=self.deploy_name),
                                            "deployments.0.status", 'Succeeded')
        assert app_status, "更新应用后，验证应用状态出错：app: {} is not running".format(self.deploy_name)
        sleep(30)

    def 测试更新devops应用大小和环境变量(self):
        data = self.data
        resourceVersion = self.deploy.get_deploy_resourceversion(self.deploy_name)
        data.update({"request_cpu": "30m", "request_mem": "50Mi", "resourceVersion": resourceVersion})
        data["deployments"][0]["containers"][0].update({"envs": {"key1": "value1", "key2": "value2"}})
        update_ret = self.newapp.update_namespace_resource(data["app_name"], 'test_data/devops_containerplatform/app_all_para.jinja2',
                                                           data=data)
        assert update_ret.status_code == 200, "devops应用更新失败 {}".format(update_ret.text)
        value = self.newapp.generate_jinja_data("./verify_data/devops_containerplatform/update_app.jinja2", data=data)
        assert self.newapp.is_sub_dict(value, update_ret.json()), \
            "更新devops应用大小和环境变量对比预期数据失败，期望数据:{},实际数据{}".format(value, update_ret.json())
        app_status = self.newapp.get_status(self.newapp.get_namespace_resource_url(resource_name=data["app_name"]),
                                            "deployments.0.status", 'Succeeded')
        assert app_status, "更新devops应用大小和环境变量后，验证应用状态出错：app: {} is not running".format(data["app_name"])
        sleep(30)

    def 测试更新容器的request大小(self, prepare):
        data = self.data
        size_data = {"requests": {"memory": "30Mi", "cpu": "30m"}}
        update_ret = self.deploy.update_container(self.deploy_name, self.container_name, size_data)
        assert update_ret.status_code == 200, "devops容器大小更新失败 {}".format(update_ret.text)
        verify_data = {
            "deploy_name": self.deploy_name,
            "container_name": self.container_name,
            "namespace": K8S_NAMESPACE,
            "labels": data["deployments"][0]["labels"],
            "envs": data["deployments"][0]["containers"][0]["envs"],
            "cm_name": prepare["configmap_name"],
            "secret_name": prepare["secret_name"],
            "devops_secret": prepare["devops_secret_name"],
            "image": IMAGE
        }
        verify_data.update(size_data)
        value = self.newapp.generate_jinja_data("./verify_data/devops_containerplatform/update_container.jinja2", verify_data)
        assert self.newapp.is_sub_dict(value, update_ret.json()), \
            "更新容器的request大小对比预期数据失败，期望数据:{},实际数据{}".format(value, update_ret.json())

        app_status = self.newapp.get_status(self.newapp.get_namespace_resource_url(resource_name=data["app_name"]),
                                            "deployments.0.status", 'Succeeded')
        assert app_status, "更新容器的request大小后，验证应用状态出错：app: {} is not running".format(data["app_name"])
        sleep(30)

    def 测试更新容器的limit大小(self, prepare):
        data = self.data
        size_data = {"limits": {"memory": "30Mi", "cpu": "30m"}}
        update_ret = self.deploy.update_container(self.deploy_name, self.container_name, size_data)
        assert update_ret.status_code == 200, "更新容器的limit大小更新失败 {}".format(update_ret.text)
        verify_data = {
            "deploy_name": self.deploy_name,
            "container_name": self.container_name,
            "namespace": K8S_NAMESPACE,
            "labels": data["deployments"][0]["labels"],
            "envs": data["deployments"][0]["containers"][0]["envs"],
            "cm_name": prepare["configmap_name"],
            "secret_name": prepare["secret_name"],
            "devops_secret": prepare["devops_secret_name"],
            "image": IMAGE,
            "requests": {"memory": "30Mi", "cpu": "30m"}
        }
        verify_data.update(size_data)
        value = self.newapp.generate_jinja_data("./verify_data/devops_containerplatform/update_container.jinja2", verify_data)
        assert self.newapp.is_sub_dict(value, update_ret.json()), \
            "更新容器的limit大小对比预期数据失败，期望数据:{},实际数据{}".format(value, update_ret.json())

        app_status = self.newapp.get_status(self.newapp.get_namespace_resource_url(resource_name=data["app_name"]),
                                            "deployments.0.status", 'Succeeded')
        assert app_status, "更新容器的limit大小后，验证应用状态出错：app: {} is not running".format(data["app_name"])
        sleep(30)

    def 测试更新容器的环境变量(self, prepare):
        data = self.data
        env_data = {"env": [{"name": "key1", "value": "value1"}],
                    "envFrom": [{"secretRef": {"name": prepare["secret_name"]}}, {"configMapRef": {"name": prepare["configmap_name"]}}]}
        update_ret = self.deploy.update_app_env(self.deploy_name, self.container_name, env_data)
        assert update_ret.status_code == 200, "更新容器的环境变量失败 {}".format(update_ret.text)
        verify_data = {
            "deploy_name": self.deploy_name,
            "container_name": self.container_name,
            "namespace": K8S_NAMESPACE,
            "labels": data["deployments"][0]["labels"],
            "envs": {"key1": "value1"},
            "cm_name": prepare["configmap_name"],
            "secret_name": prepare["secret_name"],
            "devops_secret": prepare["devops_secret_name"],
            "image": IMAGE,
            "limits": {"memory": "30Mi", "cpu": "30m"},
            "requests": {"memory": "30Mi", "cpu": "30m"}
        }
        value = self.newapp.generate_jinja_data("./verify_data/devops_containerplatform/update_container.jinja2", verify_data)
        assert self.newapp.is_sub_dict(value, update_ret.json()), \
            "更新容器的环境变量对比预期数据失败，期望数据:{},实际数据{}".format(value, update_ret.json())

        app_status = self.newapp.get_status(self.newapp.get_namespace_resource_url(resource_name=data["app_name"]),
                                            "deployments.0.status", 'Succeeded')
        assert app_status, "更新容器的环境变量后，验证应用状态出错：app: {} is not running".format(data["app_name"])
        sleep(30)

    def 测试更新容器的配置字典(self, prepare):
        data = self.data
        env_data = {"env": [{"name": "key1", "value": "value1"}],
                    "envFrom": [{"configMapRef": {"name": prepare["configmap_name"]}}]}
        update_ret = self.deploy.update_app_env(self.deploy_name, self.container_name, env_data)
        assert update_ret.status_code == 200, "更新容器的配置字典失败 {}".format(update_ret.text)
        verify_data = {
            "deploy_name": self.deploy_name,
            "container_name": self.container_name,
            "namespace": K8S_NAMESPACE,
            "labels": data["deployments"][0]["labels"],
            "envs": {"key1": "value1"},
            "cm_name": prepare["configmap_name"],
            "devops_secret": prepare["devops_secret_name"],
            "image": IMAGE,
            "limits": {"memory": "30Mi", "cpu": "30m"},
            "requests": {"memory": "30Mi", "cpu": "30m"}
        }
        value = self.newapp.generate_jinja_data("./verify_data/devops_containerplatform/update_container.jinja2", verify_data)
        assert self.newapp.is_sub_dict(value, update_ret.json()), \
            "更新容器的配置字典对比预期数据失败，期望数据:{},实际数据{}".format(value, update_ret.json())

        app_status = self.newapp.get_status(self.newapp.get_namespace_resource_url(resource_name=data["app_name"]),
                                            "deployments.0.status", 'Succeeded')
        assert app_status, "更新容器的配置字典后，验证应用状态出错：app: {} is not running".format(data["app_name"])
        sleep(30)

    def 测试修改应用网络(self):
        detail_ret = self.deploy.detail_namespace_resource(self.deploy_name)
        assert detail_ret.status_code == 200, '获取deploy详情失败:{}'.format(detail_ret.text)
        network_info = self.deploy.get_value(detail_ret.json(), "networkInfo.externalNetworkInfos.0")
        update_data = {
            "action": "delete",
            "type": "external",
            "oldExternalNetworkInfo": network_info
        }
        update_ret = self.deploy.update_app_network(self.deploy_name, update_data)
        assert update_ret.status_code == 200, "修改应用网络失败:{}".format(update_ret.text)

    def 测试修改应用镜像版本(self):
        update_data = {"image": IMAGE + "222"}
        update_ret = self.deploy.update_app_image(self.deploy_name, self.container_name, update_data)
        assert update_ret.status_code == 200, "修改应用镜像失败:{}".format(update_ret.text)
        image = self.deploy.get_value(update_ret.json(), "spec.template.spec.containers.0.image")
        assert image == IMAGE + "222", "期望更新后的镜像是{},实际是{}".format(IMAGE + "222", image)
