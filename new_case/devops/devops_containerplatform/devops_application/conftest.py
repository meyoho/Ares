import pytest
from new_case.devops.devops_containerplatform.containerplatform import GenericApi
from new_case.devops.devops_secret.secret import Secret
from common.settings import K8S_NAMESPACE, IMAGE, PROJECT_NAME, SC_NAME, RESOURCE_PREFIX
from new_case.containerplatform.service.service import Service
import base64

prepare_data = {
    "configmap_name": "{}-devops-app-cm".format(RESOURCE_PREFIX),
    "pvc_name": "{}-devops-app-pvc".format(RESOURCE_PREFIX),
    "secret_name": "{}-devops-app-secret".format(RESOURCE_PREFIX),
    "devops_secret_name": "{}-devops-app-devops-secret".format(RESOURCE_PREFIX)
}
app_create_data = [
    {
        "app_name": "{}-ares-devops-appwith-all-para".format(RESOURCE_PREFIX),
        "deployments": [{
            "containers": [{
                "container_name": "ares-devops-appwith-all-para",
                "image_name": IMAGE,
                "envs": {
                    "key": "value"
                },
                "secret_name": prepare_data["secret_name"],
                "configmap_name": prepare_data["configmap_name"],
                "volume_name": prepare_data["configmap_name"],
                "volume_type": "ConfigMap",
            }],
            "devops_secret_name": prepare_data["devops_secret_name"],
            "labels": {
                "key": "value"
            },
            "deploy_name": "{}-ares-devops-appwith-all-para".format(RESOURCE_PREFIX)
        }
        ],
        "networkInfo": True,
    },
    {
        "app_name": "{}-ares-devops-appwith-two-deploy".format(RESOURCE_PREFIX),
        "deployments": [
            {
                "containers": [{
                    "container_name": "ares-devops-deploy1",
                    "image_name": IMAGE,
                    "envs": {
                        "key": "value"
                    },
                    "secret_name": prepare_data["secret_name"],
                    "configmap_name": prepare_data["configmap_name"],
                    "volume_name": prepare_data["configmap_name"],
                    "volume_type": "ConfigMap"

                }],
                "devops_secret_name": prepare_data["devops_secret_name"],
                "labels": {
                    "key": "value"
                },
                "deploy_name": "{}-ares-devops-deploy1".format(RESOURCE_PREFIX)
            },
            {
                "containers": [{
                    "container_name": "ares-devops-deploy2",
                    "image_name": IMAGE,
                    "envs": {
                        "key": "value"
                    },
                    "secret_name": prepare_data["secret_name"],
                    "configmap_name": prepare_data["configmap_name"]

                }],
                "volume_name": prepare_data["configmap_name"],
                "volume_type": "ConfigMap",
                "devops_secret_name": prepare_data["devops_secret_name"],
                "labels": {
                    "key": "value"
                },
                "deploy_name": "{}-ares-devops-deploy2".format(RESOURCE_PREFIX)
            }
        ]
    },
    {
        "app_name": "{}-ares-devops-appwith-two-container".format(RESOURCE_PREFIX),
        "deployments": [
            {
                "containers": [
                    {
                        "container_name": "ares-devops-container1",
                        "image_name": IMAGE,
                        "envs": {
                            "key": "value"
                        },
                        "secret_name": prepare_data["secret_name"],
                        "configmap_name": prepare_data["configmap_name"],
                        "volume_name": prepare_data["configmap_name"],
                        "volume_type": "ConfigMap"
                    },
                    {
                        "container_name": "ares-devops-container2",
                        "image_name": IMAGE,
                        "envs": {
                            "key": "value"
                        }
                    }
                ],
                "devops_secret_name": prepare_data["devops_secret_name"],
                "labels": {
                    "key": "value"
                },
                "deploy_name": "{}-ares-devops-containers".format(RESOURCE_PREFIX)
            }
        ]
    }
]
applicationcase_list = ["devops应用包含所有参数", "devops应用包含两个deployment", "devops应用pod包含两个容器"]


@pytest.fixture(scope="session", autouse=True)
def prepare():
    service_client = Service()
    service_client.delete_service(K8S_NAMESPACE, "test")
    configmap = GenericApi('configmap')
    pvc = GenericApi('persistentvolumeclaim')
    secret = GenericApi("secret")
    devops_secret = Secret()
    cm_ret = configmap.create_namespace_resource("test_data/devops_containerplatform/configmap.jinja2",
                                                 {"namespace": K8S_NAMESPACE,
                                                  "cm_name": prepare_data["configmap_name"],
                                                  "data": {"key": "value"}})
    assert cm_ret.status_code in (409, 200), "准备数据,创建配置字典失败:{}".format(cm_ret.text)
    secret_data = {
        "secret_name": prepare_data["secret_name"],
        "secret_type": "kubernetes.io/basic-auth",
        "username": str(base64.b64encode("username".encode('utf-8')), 'utf8'),
        "password": str(base64.b64encode("password".encode('utf-8')), 'utf8'),
        "K8S_NAMESPACE": K8S_NAMESPACE
    }
    secret_ret = secret.create_namespace_resource('./test_data/devops_containerplatform/secret.jinja2',
                                                  data=secret_data)

    assert secret_ret.status_code in (409, 200), "准备数据创建secret失败:{}".format(secret_ret.text)

    pvc_ret = pvc.create_namespace_resource("test_data/devops_containerplatform/pvc.jinja2",
                                            {"namespace": K8S_NAMESPACE,
                                             "pvc_name": prepare_data["pvc_name"],
                                             "scs_name": SC_NAME})
    assert pvc_ret.status_code in (409, 200), "准备数据，创建pvc失败:{}".format(pvc_ret.text)

    # pvc_status = pvc.get_status(pvc.get_namespace_resource_url(resource_name=prepare_data["pvc_name"]),
    #                             "status", "Bound")
    # assert pvc_status, "准备数据，pvc存储状态错误:{}的状态没有被绑定".format(prepare_data["pvc_name"])

    secret_data.update({"secret_name": prepare_data["devops_secret_name"], "k8s_namespace": PROJECT_NAME})
    secret_data.pop("K8S_NAMESPACE")
    ret = devops_secret.create_secret(scope=PROJECT_NAME, file='./test_data/devops_secret/create_secret.jinja2',
                                      data=secret_data)

    assert ret.status_code in (409, 200), "准备创建凭据失败:{}".format(ret.text)
    yield prepare_data
    configmap.delete_namespace_resource(prepare_data["configmap_name"])
    pvc.delete_namespace_resource(prepare_data["pvc_name"])
    secret.delete_namespace_resource(prepare_data["secret_name"])
    devops_secret.delete_secret(PROJECT_NAME, prepare_data["devops_secret_name"])
    newapp = GenericApi("applications")
    for data in app_create_data:
        newapp.delete_app(data["app_name"])
