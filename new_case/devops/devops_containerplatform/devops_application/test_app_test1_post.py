import pytest
from common.settings import RERUN_TIMES, K8S_NAMESPACE, IMAGE, SC_NAME
from new_case.devops.devops_containerplatform.containerplatform import GenericApi
from new_case.devops.devops_containerplatform.devops_application.conftest import app_create_data, applicationcase_list


@pytest.mark.devopsApiServer
@pytest.mark.devops_app
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestL1CreateDevopsAppSuite(object):
    def setup_class(self):
        self.newapp = GenericApi("applications")
        self.deploy = GenericApi("deployment")

    @pytest.mark.parametrize("data", app_create_data, ids=applicationcase_list)
    def 测试创建devops应用l1用例(self, data):
        if not self.newapp.check_exists(self.newapp.get_namespace_resource_url(K8S_NAMESPACE, data["app_name"]), 404, expect_cnt=1):
            self.newapp.delete_app(data["app_name"])
        create_result = self.newapp.create_namespace_resource('test_data/devops_containerplatform/app_all_para.jinja2',
                                                              data=data)
        assert create_result.status_code == 200, "devops应用创建失败 {}".format(create_result.text)
        value = self.newapp.generate_jinja_data("./verify_data/devops_containerplatform/create_app.jinja2", data=data)
        assert self.newapp.is_sub_dict(value, create_result.json()), \
            "创建devops应用对比预期数据失败，期望数据:{},实际数据{}".format(value, create_result.json())
        app_status = self.newapp.get_status(self.newapp.get_namespace_resource_url(resource_name=data["app_name"]),
                                            "deployments.0.status", 'Succeeded')
        assert app_status, "创建应用后，验证应用状态出错：app: {} is not running".format(data["app_name"])

    @pytest.mark.skipif(not SC_NAME, reason="没有存储类，不跑当前用例")
    def 测试挂载存储卷(self, prepare):
        data = app_create_data[0]
        deploy_name = data["deployments"][0]["deploy_name"]
        container_name = data["deployments"][0]["containers"][0]["container_name"]
        mout_ret = self.deploy.amount_volume(deploy_name, container_name,
                                             "test_data/devops_containerplatform/amount_volume.jinja2", {"pvc_name": prepare["pvc_name"]})
        assert mout_ret.status_code == 200, "挂在存储卷失败:{}".format(mout_ret.text)
        verify_data = {
            "deploy_name": deploy_name,
            "container_name": container_name,
            "namespace": K8S_NAMESPACE,
            "labels": data["deployments"][0]["labels"],
            "envs": data["deployments"][0]["containers"][0]["envs"],
            "cm_name": prepare["configmap_name"],
            "pvc_name": prepare["pvc_name"],
            "secret_name": prepare["secret_name"],
            "devops_secret": prepare["devops_secret_name"],
            "image": IMAGE
        }
        value = self.deploy.generate_jinja_data("./verify_data/devops_containerplatform/amount_volume.jinja2", data=verify_data)
        assert self.newapp.is_sub_dict(value, mout_ret.json()), \
            "挂载存储卷对比预期数据失败，期望数据:{},实际数据{}".format(value, mout_ret.json())

        @pytest.mark.skip(reason="not ready")
        def 测试通过workload创建应用():
            # 需测试daemonset、deployment、statefulsat 3种
            assert True
