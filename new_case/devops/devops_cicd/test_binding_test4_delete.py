import pytest
from new_case.devops.tool_chain.tool_chain import ToolChain
from common import settings
from new_case.devops.devops_cicd.conftest import bind_info, tool_casename_list
import copy
from common.check_status import checker


@pytest.mark.skipif(not checker.harbor_available(), reason='Harbor不可用')
@pytest.mark.skipif(not checker.harbor_available(), reason='Gitlab不可用')
@pytest.mark.skipif(not checker.jenkins_available(), reason='Jenkins不可用')
@pytest.mark.tool
@pytest.mark.jenkins
@pytest.mark.devopsApiServer
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestDeleteBinding(object):
    def setup_class(self):
        self.toolchain = ToolChain()
        global data_list
        data_list = copy.deepcopy(bind_info)
        for dict in data_list:
            name = dict["bind_name"]
            dict.update({"bind_name": name + "binding"})

    def 测试解除绑定工具链(self):
        for data in data_list:
            ret = self.toolchain.unbind_chain(type=data["api_type"], name=data["bind_name"])
            assert ret.status_code == 200, "解除绑定工具链{}失败".format(data["bind_name"])

    @pytest.mark.skipif(settings.AUDIT_UNABLED, reason="do not have audit")
    @pytest.mark.parametrize("data", bind_info, ids=tool_casename_list)
    def 测试工具链解绑审计(self, data):
        payload = {"user_name": settings.USERNAME, "operation_type": "delete", "resource_type": data["audit_type"],
                   "resource_name": data["bind_name"] + "binding"}
        result = self.toolchain.search_audit(payload)
        payload.update({"namespace": settings.PROJECT_NAME, "region_name": settings.GLOBAL_REGION_NAME})
        values = self.toolchain.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.toolchain.is_sub_dict(values, result.json()), "审计数据不符合预期"
