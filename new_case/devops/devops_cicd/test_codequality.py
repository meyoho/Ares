import pytest
from common.settings import RERUN_TIMES
from new_case.devops.devops_cicd.cicd import CicdApi


@pytest.mark.tool
@pytest.mark.devopsApiServer
@pytest.mark.BAT
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestCodeQualitySuite(object):
    def setup_class(self):
        self.codequality = CicdApi('codequalityproject')

    def 测试获取devops代码检查列表(self):
        # list code quality
        list_result = self.codequality.list_pipeline_config()
        assert list_result.status_code == 200, list_result.text
        data = {"listMeta": {}, "codequalityprojects": [], "errors": []}
        assert self.codequality.is_sub_dict(data, list_result.json()), "获取代码扫描列表返回数据格式不对"
