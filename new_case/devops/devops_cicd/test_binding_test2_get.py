import pytest
from new_case.devops.devops_cicd.cicd import CicdApi
from new_case.devops.devops_cicd.conftest import bind_info
import copy
from common.check_status import checker


@pytest.mark.skipif(not checker.harbor_available(), reason="Harbor不可用")
@pytest.mark.skipif(not checker.harbor_available(), reason="Gitlab不可用")
@pytest.mark.skipif(not checker.jenkins_available(), reason="Jenkins不可用")
@pytest.mark.tool
@pytest.mark.jenkins
@pytest.mark.devopsApiServer
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=2, reruns_delay=3)
class TestGetBinding(object):
    def setup_class(self):
        self.jira_tool = CicdApi("")
        data_list = copy.deepcopy(bind_info)
        for i in range(len(data_list)):
            if data_list[i]["kind"] == "ProjectManagementBinding":
                data = data_list[i]
        self.jira_bind_name = data["bind_name"]+"binding"

    @pytest.mark.skip(reason="not ready")
    def 测试获取issue列表和详情页(self):
        ret = self.jira_tool.list_binding("projectManagement")
        assert ret.status_code == 200, "获取jira绑定失败"
        ret = self.jira_tool.list_jira_issueoptions(self.jira_bind_name)
        assert ret.status_code == 200, "获取jira选择项失败"
        ret = self.jira_tool.list_jira_issuelist(self.jira_bind_name)
        assert ret.status_code == 200, "获取jira列表失败"
        issue_num = self.jira_tool.get_value(ret.json(), "total")
        if issue_num > 0:
            issue_key = self.jira_tool.get_value(ret.json(), "items.0.key")
            issue_detail = self.jira_tool.deatil_jira_issue(self.jira_bind_name, issue_key)
            assert issue_detail.status_code == 200, "获取issue详情失败"
