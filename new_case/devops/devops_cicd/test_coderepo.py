import pytest
from common.settings import RERUN_TIMES, GITLAB_REPO, PROJECT_NAME, GITLAB_URL, GITLAB_USERNAME
from new_case.devops.devops_cicd.cicd import CicdApi
from new_case.devops.devops_cicd.conftest import bind_info


@pytest.mark.devops_controller
@pytest.mark.devopsApiServer
@pytest.mark.BAT
@pytest.mark.tool
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestCodeRepoSuite(object):
    def setup_class(self):
        self.coderepo = CicdApi('coderepository')
        self.code_info = self.coderepo.get_code_repo_info(GITLAB_REPO, get_name=True)

    def 测试获取devops代码仓库列表(self):
        # list code repo
        list_result = self.coderepo.list_code_repo()
        assert list_result.status_code == 200, list_result.text
        data = {
            "name": GITLAB_REPO,
            "code_repo_name": self.code_info["code_repo_name"],
            "namespace": PROJECT_NAME,
            "gitlab_tool_name": bind_info[3]["tool_name"],
            "gitlab_bind": bind_info[3]["bind_name"],
            "gitlab_url": GITLAB_URL,
            "gitlab_user": GITLAB_USERNAME,
            "secret_name": bind_info[3]["secret_name"]
        }
        value = self.coderepo.generate_jinja_data("./verify_data/devops_pipeline_config/list_code_repo.jinja2",
                                                  data=data)
        assert self.coderepo.is_sub_dict(value, list_result.json()), \
            "gitlab列表对比预期数据失败，实际数据:{},期望数据{}".format(list_result.json(), value)

    def 测试项目代码仓库分页(self):
        # check pagination
        name_flag = self.coderepo.resource_pagination(self.coderepo.cicd_resource_url(),
                                                      query="coderepositories.0.metadata.name",
                                                      page_size_key="itemsPerPage", params={"sortBy": "a,name"})

        assert name_flag, "验证平台凭据列表分页出错，请手动验证"
