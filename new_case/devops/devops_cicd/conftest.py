import pytest
from common import settings
from new_case.devops.devops_secret.secret import Secret
from new_case.devops.tool_chain.tool_chain import ToolChain
import base64
import copy
from common.log import logger, color
from new_case.devops.devops_containerplatform.containerplatform import GenericApi
from time import sleep, time

app_name = "{}-config-hello-world".format(settings.RESOURCE_PREFIX)
pipeline_config = [
    {"name": "{}-config-input-code-repo".format(settings.RESOURCE_PREFIX)},
    {"name": "{}-config-multiple-build-command".format(settings.RESOURCE_PREFIX)},
    {"name": "{}-config-multiple-image-tag".format(settings.RESOURCE_PREFIX)},
    {"name": "{}-config-new-image-repo".format(settings.RESOURCE_PREFIX)},
    {"name": "{}-config-input-image-repo".format(settings.RESOURCE_PREFIX)},
    {"name": "{}-config-code-change".format(settings.RESOURCE_PREFIX)},
    {"name": "{}-config-cron".format(settings.RESOURCE_PREFIX)},
    {"name": "{}-sync-image".format(settings.RESOURCE_PREFIX)},
    {"name": "{}-update-application".format(settings.RESOURCE_PREFIX)},
    {"name": "{}-script-input".format(settings.RESOURCE_PREFIX)},
    {"name": "{}-config-multiple-gitlab".format(settings.RESOURCE_PREFIX)},
    {"name": "{}-gobuild-deploy-deploy".format(settings.RESOURCE_PREFIX)},
    {"name": "{}-gobuild-deploy-app".format(settings.RESOURCE_PREFIX)},
    {"name": "{}-javabuild-juit-success".format(settings.RESOURCE_PREFIX)},
    {"name": "{}-javabuild-juit-failed".format(settings.RESOURCE_PREFIX)},
    {"name": "{}-sync-image-cron".format(settings.RESOURCE_PREFIX)}
]

tool_info = [
    {
        "tool_name": settings.JENKINS_TOOL_NAME,
        "host_url": settings.JENKINS_URL,
        "accessUrl": settings.JENKINS_URL,
        "api_type": "jenkinses"
    },

    {
        "tool_name": "{}-ares-harbortool".format(settings.RESOURCE_PREFIX),
        "host_url": settings.HARBOR_URL,
        "accessUrl": settings.HARBOR_URL,
        "type": "Harbor",
        "kind": "ImageRegistry",
        "api_type": "imageregistry"
    },
    {
        "tool_name": "{}-ares-sonartool".format(settings.RESOURCE_PREFIX),
        "host_url": settings.SONAR_URL,
        "accessUrl": settings.SONAR_URL,
        "type": "Sonarqube",
        "kind": "CodeQualityTool",
        "api_type": "codequalitytool"
    },
    {
        "tool_name": "{}-ares-gitlabtool".format(settings.RESOURCE_PREFIX),
        "host_url": settings.GITLAB_URL,
        "accessUrl": settings.GITLAB_URL,
        "type": "Gitlab",
        "api_type": "codereposervice"
    },
    {
        "tool_name": "{}-ares-dockertool".format(settings.RESOURCE_PREFIX),
        "host_url": settings.DOCKER_REGISTRY_URL,
        "accessUrl": settings.DOCKER_REGISTRY_URL,
        "kind": "ImageRegistry",
        "type": "Docker",
        "api_type": "imageregistry"
    },
    {
        "tool_name": "{}-ares-jira".format(settings.RESOURCE_PREFIX),
        "host_url": settings.JIRA_URL,
        "accessUrl": settings.JIRA_URL,
        "kind": "ProjectManagement",
        "type": "Jira",
        "api_type": "projectmanagement"
    }
]
secret_info = [
    {
        "secret_name": "{}-ares-jenkins".format(settings.RESOURCE_PREFIX),
        "secret_type": "kubernetes.io/basic-auth",
        "username": str(base64.b64encode(settings.JENKINS_USERNAME.encode('utf-8')), 'utf8'),
        "password": str(base64.b64encode(settings.JENKINS_PASSWORD.encode('utf-8')), 'utf8')
    },
    {
        "secret_name": "{}-ares-harbor".format(settings.RESOURCE_PREFIX),
        "secret_type": "kubernetes.io/basic-auth",
        "username": str(base64.b64encode(settings.HARBOR_USERNAME.encode('utf-8')), 'utf8'),
        "password": str(base64.b64encode(settings.HARBOR_PASSWORD.encode('utf-8')), 'utf8')
    },
    {
        "secret_name": "{}-ares-sonar".format(settings.RESOURCE_PREFIX),
        "secret_type": "kubernetes.io/basic-auth",
        "username": str(base64.b64encode(settings.SONAR_USERNAME.encode('utf-8')), 'utf8'),
        "password": str(base64.b64encode(settings.SONAR_PASSWORD.encode('utf-8')), 'utf8')
    },
    {
        "secret_name": "{}-ares-gitlab".format(settings.RESOURCE_PREFIX),
        "secret_type": "kubernetes.io/basic-auth",
        "username": str(base64.b64encode(settings.GITLAB_USERNAME.encode('utf-8')), 'utf8'),
        "password": str(base64.b64encode(settings.GITLAB_PASSWORD.encode('utf-8')), 'utf8')
    },
    {
        "secret_name": "{}-ares-docker".format(settings.RESOURCE_PREFIX),
        "secret_type": "kubernetes.io/basic-auth",
        "username": str(base64.b64encode(settings.GITLAB_USERNAME.encode('utf-8')), 'utf8'),
        "password": str(base64.b64encode(settings.GITLAB_PASSWORD.encode('utf-8')), 'utf8')
    },
    {
        "secret_name": "{}-ares-jira".format(settings.RESOURCE_PREFIX),
        "secret_type": "kubernetes.io/basic-auth",
        "username": str(base64.b64encode(settings.JIRA_USERNAME.encode('utf-8')), 'utf8'),
        "password": str(base64.b64encode(settings.JIRA_PASSWORD.encode('utf-8')), 'utf8')
    },
    {
        "secret_name": "{}-ares-nexus".format(settings.RESOURCE_PREFIX),
        "secret_type": "kubernetes.io/basic-auth",
        "username": str(base64.b64encode(settings.NEXUS_USERNAME.encode('utf-8')), 'utf8'),
        "password": str(base64.b64encode(settings.NEXUS_PASSWORD.encode('utf-8')), 'utf8')
    }
]
bind_info = [
    {
        "bind_name": "{}-ares-jenkins-cicd".format(settings.RESOURCE_PREFIX),
        "kind": "JenkinsBinding",
        "api_type": "jenkinsbinding",
        "audit_type": "jenkinsbindings"
    },
    {
        "bind_name": "{}-ares-harbor-cicd".format(settings.RESOURCE_PREFIX),
        "kind": "ImageRegistryBinding",
        "api_type": "imageregistrybinding",
        "audit_type": "imageregistrybindings"
    },
    {
        "bind_name": "{}-ares-sonar-cicd".format(settings.RESOURCE_PREFIX),
        "kind": "CodeQualityBinding",
        "api_type": "codequalitybinding",
        "audit_type": "codequalitybindings"
    },
    {
        "bind_name": "{}-ares-gitlab-cicd".format(settings.RESOURCE_PREFIX),
        "kind": "codeRepoService",
        "api_type": "coderepobinding",
        "audit_type": "coderepobindings"
    },
    {
        "bind_name": "{}-ares-docker-cicd".format(settings.RESOURCE_PREFIX),
        "kind": "DockerBinding",
        "api_type": "imageregistrybinding"
    },
    {
        "bind_name": "{}-ares-jira-cicd".format(settings.RESOURCE_PREFIX),
        "kind": "ProjectManagementBinding",
        "api_type": "projectmanagementbinding"
    }
]
tool_casename_list = ["jenkins工具链", "harbor工具链", "sonarqube工具链", "gitlab工具链", "docker工具链", "JIRA工具链"]
tool_name = {"none": "none"}


@pytest.fixture(scope="session", autouse=True)
def prepare_tool():
    create_jenkins_flag = False
    chain_tool = ToolChain()
    secret = Secret()
    newapp = GenericApi("applications")
    try:
        if settings.CASE_TYPE not in ("delete", "upgrade"):
            newapp.create_namespace_resource('test_data/devops_containerplatform/app.jinja2',
                                             {'app_name': app_name, "image": settings.IMAGE})
            app_status = newapp.get_status(newapp.get_namespace_resource_url(resource_name=app_name),
                                           "deployments.0.status", 'Succeeded')
            # 如果Running状态后直接去更新有概率会报错 http://jira.alaudatech.com/browse/DEVOPS-1442
            assert app_status, "创建应用后，验证应用状态出错：hellworld is not running"
            # clear
            for data in bind_info:
                chain_tool.unbind_chain(type=data["api_type"], name=data["bind_name"])
            for data in secret_info:
                if "gitlab" in data['secret_name']:
                    secret.delete_secret(scope=settings.PROJECT_NAME, name=data["secret_name"])
                elif settings.DEFAULT_NS == "alauda-system":
                    secret.delete_secret(scope="global-credentials", name=data["secret_name"])
                else:
                    secret.delete_secret(scope="{}-global-credentials".format(settings.DEFAULT_NS),
                                         name=data["secret_name"])
            for data in tool_info:
                if data["api_type"] != "jenkinses":
                    chain_tool.delete_chain(data["api_type"], data["tool_name"])

            # create tool chain
            for data in tool_info:
                create_data = copy.deepcopy(data)
                api_type = create_data.pop("api_type")
                result = chain_tool.create_chain(api_type, './test_data/tool_chain/create_chain.jinja2',
                                                 data=create_data)
                logger.info(color.red(result.text))
                if data["api_type"] == "jenkinses" and result.status_code == 201:
                    create_jenkins_flag = True
        # get tool name
        get_tool_name()
        # create secret
        for data in secret_info:
            if "gitlab" in data['secret_name']:
                data.update({"k8s_namespace": settings.PROJECT_NAME, "global_flag": '"false"'})
                secret.create_secret(scope=settings.PROJECT_NAME,
                                     file='./test_data/devops_secret/create_secret.jinja2', data=data)
            else:
                secret.create_secret(scope="", file='./test_data/devops_secret/create_secret.jinja2', data=data)
        # bind to project
        for i in range(len(bind_info)):
            if i < len(secret_info):
                secret_name = secret_info[i]['secret_name']
            else:
                secret_name = ""
            kind = bind_info[i]["kind"]
            if bind_info[i]["kind"] not in tool_name:
                kind = "none"
            bind_data = {
                "kind": kind,
                "description": "description",
                "bind_name": bind_info[i]["bind_name"],
                "namespace": settings.PROJECT_NAME,
                "secret_name": secret_name,
                "tool_name": tool_name[kind],
                "secret_namespace": "global-credentials" if settings.DEFAULT_NS == "alauda-system" else "{}-global-credentials".format(
                    settings.DEFAULT_NS)
            }
            bind_info[i].update(bind_data)
            if settings.CASE_TYPE not in ("delete", "upgrade"):
                if bind_info[i]["kind"] == "JenkinsBinding":
                    ret = chain_tool.bind_chain("jenkinsbinding", "test_data/tool_chain/create_jenkins_bind.jinja2",
                                                bind_data)
                    assert ret.status_code == 200, "初始化绑定jenkins工具链失败"
                elif bind_info[i]["kind"] == "CodeQualityBinding":
                    ret = chain_tool.bind_chain("codequalitybinding",
                                                "test_data/tool_chain/create_sonarqube_bind.jinja2", bind_data)
                    assert ret.status_code == 200, "初始化绑定sonaqube工具链失败"
                elif bind_info[i]["kind"] == "codeRepoService":
                    bind_data.update({"secret_namespace": settings.PROJECT_NAME})
                    ret = chain_tool.bind_chain("coderepobinding", "test_data/tool_chain/create_coderepo_bind.jinja2",
                                                bind_data)
                    assert ret.status_code == 200, "初始化绑定gitlab工具链失败"
                    bind_data.update(
                        {"method": "put", "account": settings.GITLAB_USERNAME, "gitlab_url": settings.GITLAB_URL,
                         "secret_namespace": settings.PROJECT_NAME})
                    sleep(10)
                    ret = chain_tool.update_bind_chain("coderepobinding", bind_info[i]["bind_name"],
                                                       "test_data/tool_chain/update_coderepo_bind.jinja2", bind_data)
                    assert ret.status_code == 200, "初始化分配代码仓库失败"
                elif bind_info[i]["kind"] == "ImageRegistryBinding":
                    ret = chain_tool.bind_chain("imageregistrybinding",
                                                "test_data/tool_chain/create_imagerepo_bind.jinja2",
                                                bind_data)
                    assert ret.status_code == 200, "初始化绑定harbor工具链失败"
                    resourceVersion = chain_tool.get_value(ret.json(), "metadata.resourceVersion")
                    bind_data.update(
                        {"method": "put", "remote_repo": settings.HARBOR_PROJECT, "resourceVersion": resourceVersion})
                    sleep(5)
                    ret = chain_tool.update_bind_chain("imageregistrybinding", bind_info[i]["bind_name"],
                                                       "test_data/tool_chain/update_imagerepo_bind.jinja2", bind_data)
                    assert ret.status_code == 200, "初始化分配镜像仓库失败"
                else:
                    pass
    except AssertionError as e:
        logger.error(e)
        for data in bind_info:
            chain_tool.unbind_chain(type=data["api_type"], name=data["bind_name"])
        for data in secret_info:
            if "gitlab" in data['secret_name']:
                secret.delete_secret(scope=settings.PROJECT_NAME, name=data["secret_name"])
            elif settings.DEFAULT_NS == "alauda-system":
                secret.delete_secret(scope="global-credentials", name=data["secret_name"])
            else:
                secret.delete_secret(scope="{}-global-credentials".format(settings.DEFAULT_NS),
                                     name=data["secret_name"])
        for data in tool_info:
            if data["api_type"] == "jenkinses" and not create_jenkins_flag:
                pass
            else:
                chain_tool.delete_chain(data["api_type"], data["tool_name"])

        newapp.delete_app(app_name)
        raise AssertionError(e)

    yield bind_info
    if settings.CASE_TYPE not in ("prepare", "upgrade"):
        for data in bind_info:
            chain_tool.unbind_chain(type=data["api_type"], name=data["bind_name"])
        for data in secret_info:
            if "gitlab" in data['secret_name']:
                secret.delete_secret(scope=settings.PROJECT_NAME, name=data["secret_name"])
            elif settings.DEFAULT_NS == "alauda-system":
                secret.delete_secret(scope="global-credentials", name=data["secret_name"])
            else:
                secret.delete_secret(scope="{}-global-credentials".format(settings.DEFAULT_NS),
                                     name=data["secret_name"])
        for data in tool_info:
            if data["api_type"] == "jenkinses" and not create_jenkins_flag:
                pass
            else:
                chain_tool.delete_chain(data["api_type"], data["tool_name"])

        newapp.delete_app(app_name)


def get_tool_name():
    tool_list = get_chain_list()
    for key, value in tool_list.items():
        if value.split("/")[2] == settings.JENKINS_URL.split("/")[2]:
            tool_name["JenkinsBinding"] = key
        elif value.split("/")[2] == settings.HARBOR_URL.split("/")[2]:
            tool_name["ImageRegistryBinding"] = key
        elif value.split("/")[2] == settings.GITLAB_URL.split("/")[2]:
            tool_name["codeRepoService"] = key
        elif value.split("/")[2] == settings.SONAR_URL.split("/")[2]:
            tool_name["CodeQualityBinding"] = key
        elif value.split("/")[2] == settings.DOCKER_REGISTRY_URL.split("/")[2]:
            tool_name["DockerBinding"] = key
        elif value.split("/")[2] == settings.JIRA_URL.split("/")[2]:
            tool_name["ProjectManagementBinding"] = key
        else:
            pass


def get_chain_list():
    chain_tool = ToolChain()
    # get exist data
    ret = chain_tool.get_chain_list()
    assert ret.status_code == 200, "获取全部工具链列表失败:{}".format(ret.text)
    data_dict = dict()
    contents = ret.json().get("items", [])
    for content in contents:
        name = chain_tool.get_value(content, "metadata.name")
        url = chain_tool.get_value(content, "spec.http.host")
        data_dict.update({name: url})
    logger.info("tool chain list data is {}".format(data_dict))
    return data_dict


@pytest.fixture(scope="session")
def create_nexus():
    chain_tool = ToolChain()
    nexus_name = "{}-ares-nexus".format(settings.RESOURCE_PREFIX)
    repo_name = "{}-nexus-repo-{}".format(settings.RESOURCE_PREFIX, int(time()))
    secret_name = secret_info[6]["secret_name"]
    bind_name = "{}-ares-nexus-cicd".format(settings.RESOURCE_PREFIX)
    bind_data = {
        "description": "description",
        "bind_name": bind_name,
        "namespace": settings.PROJECT_NAME,
        "secret_name": secret_name,
        "repo_name": repo_name,
        "secret_namespace": "global-credentials" if settings.DEFAULT_NS == "alauda-system" else "{}-global-credentials".format(
            settings.DEFAULT_NS)
    }
    data = {
        "name": nexus_name,
        "secret_name": secret_name,
        "nexus_url": settings.NEXUS_URL,
        "secret_namespace": "global-credentials" if settings.DEFAULT_NS == "alauda-system" else "{}-global-credentials".format(
            settings.DEFAULT_NS)
    }
    chain_tool.create_chain("artifactregistrymanagers", "./test_data/tool_chain/create_nexus.jinja2", data=data)

    # get nexus name
    nexus_list = chain_tool.send("get", chain_tool.get_common_chain_url("artifactregistrymanagers"))
    assert nexus_list.status_code == 200, "获取nexus列表失败:{}".format(nexus_list.text)
    contents = nexus_list.json()["artifactregistrymanagers"]
    for content in contents:
        url = chain_tool.get_value(content, "spec.http.host")
        if url == settings.NEXUS_URL:
            nexus_name = chain_tool.get_value(content, "metadata.name")
            break

    if settings.CASE_TYPE not in ("delete", "upgrade"):
        data = {
            "repo_name": repo_name,
            "secret_name": secret_name,
            "secret_namespace": "global-credentials" if settings.DEFAULT_NS == "alauda-system" else "{}-global-credentials".format(
                settings.DEFAULT_NS)
        }
        chain_tool.create_nexus_repo(nexus_name, "./test_data/tool_chain/create_nexus_repo.jinja2", data)
        sleep(3)
        chain_tool.bind_chain("artifactregistrybindings",
                              "test_data/tool_chain/create_nexus_bind.jinja2",
                              bind_data)
    yield bind_data
    if settings.CASE_TYPE not in ("prepare", "upgrade"):
        chain_tool.unbind_chain("artifactregistrybindings", bind_name)
        chain_tool.delete_chain("artifactregistries", repo_name)
