import pytest
import json
from common.settings import RERUN_TIMES, RESOURCE_PREFIX, PROJECT_NAME
from new_case.devops.devops_cicd.conftest import bind_info
from new_case.devops.devops_cicd.cicd import CicdApi
from common import settings
from common.log import logger
from time import sleep


@pytest.mark.jenkins
@pytest.mark.devopsApiServer
@pytest.mark.BAT
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestPipelineSuite(object):
    def setup_class(self):
        self.config_client = CicdApi('pipelineconfig')
        self.pipeline = CicdApi('pipeline')
        self.image_repo_client = CicdApi('imagerepository')
        self.code_repo_client = CicdApi('coderepository')
        self.config_name = "{}-ares-golangbuild-l0".format(RESOURCE_PREFIX)
        self.codeRepository = {"url": "", "credentialId": "", "kind": "select", "bindingRepositoryName": "",
                               "sourceType": "GIT"}
        self.CodeQualityBinding = {"namespace": self.config_client.project_name, "name": ""}

        self.imageRepository = {"type": "select", "repositoryPath": "", "tag": "e2etest", "secretName": "",
                                "secretNamespace": "", "credentialId": ""}
        self.values = {"PlatformCodeRepository": self.codeRepository, "Branch": "master",
                       "RelativeDirectory": "src",
                       "buildCommand": "cd go;go build", "UseSonarQube": "true",
                       "CodeQualityBinding": self.CodeQualityBinding,
                       "EnableBranchAnalysis": "false",
                       "AnalysisParameters": "sonar.sources=./go\nsonar.sourceEncoding=UTF-8\n",
                       "FailedIfNotPassQualityGate": "false", "imageRepository": self.imageRepository,
                       "context": "./go", "buildArguments": "", "dockerfile": "Dockerfile", "retry": "3", "UseNotification": "false",
                       "PlatformNotification": "\"\""}
        logger.info('测试创建golang构建配置')
        image_repo = self.image_repo_client.get_image_repo_info(settings.HARBOR_REPO)
        code_repo = self.code_repo_client.get_code_repo_info(settings.GITLAB_REPO)
        logger.info("镜像信息 {}".format(json.dumps(image_repo)))
        logger.info("代码仓库信息 {}".format(json.dumps(code_repo)))
        assert code_repo["url"], "获取到的代码地址为空"
        self.imageRepository.update(image_repo)
        self.codeRepository.update(code_repo)
        self.values['PlatformCodeRepository'] = json.dumps(self.codeRepository)
        self.values['imageRepository'] = json.dumps(self.imageRepository)

    @pytest.mark.prepare
    def 测试创建golang构建配置(self):
        self.CodeQualityBinding.update({"name": bind_info[2]["bind_name"]})
        self.values['CodeQualityBinding'] = json.dumps(self.CodeQualityBinding)
        data = {
            "pipeline_name": self.config_name,
            "display_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": bind_info[0]["bind_name"],
            "pipeline_template_name": "GoLangBuilder",
            "values": self.values
        }
        body_data = self.config_client.generate_template_data(data=data)
        create_result = self.config_client.create_pipeline_config(body_data)
        assert create_result.status_code == 200, create_result.text
        expect_data = {
            "pipeline_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": bind_info[0]["bind_name"],
            "runPolicy": "Serial"
        }
        value = self.config_client.generate_jinja_data(
            'verify_data/devops_pipeline_config/create_pipeline_config.jinja2',
            data=expect_data)
        assert self.config_client.is_sub_dict(value, create_result.json()), \
            "创建流水线失败，期望数据:{},实际数据:{}".format(value, create_result.json())

    @pytest.mark.upgrade
    def 测试获取流水线列表(self):
        list_ret = self.config_client.list_pipeline_config()
        assert list_ret.status_code == 200, "获取流水线列表失败{}".format(list_ret.text)

    @pytest.mark.upgrade
    def 测试获取流水线详情(self):
        detail = self.config_client.detail_pipeline_config(self.config_name)
        assert detail.status_code == 200, "获取流水线详情失败{}".format(detail.text)
        url = self.config_client.cicd_resource_url(self.config_name)
        phase = self.config_client.get_status(url, 'status.phase', 'Ready')
        assert phase, "流水线同步失败"

    @pytest.mark.prepare
    def 测试串行执行golang构建配置(self):
        logger.info('测试串行执行golang构建配置')
        trigger1 = self.config_client.trigger_pipeline_config(self.config_name)
        assert trigger1.status_code == 200, "执行流水线失败{}".format(trigger1.text)
        expect_data = {
            "pipeline_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": bind_info[0]["bind_name"]
        }
        value = self.config_client.generate_jinja_data(
            'verify_data/devops_pipeline_config/trigger_pipeline_config.jinja2', data=expect_data)
        assert self.config_client.is_sub_dict(value, trigger1.json()), \
            "创建流水线失败，期望数据:{},实际数据:{}".format(value, trigger1.json())
        execution_id1 = self.config_client.get_value(trigger1.json(), 'metadata.name')
        trigger2 = self.config_client.trigger_pipeline_config(self.config_name)
        assert trigger2.status_code == 200, "执行流水线失败{}".format(trigger2.text)
        execution_id2 = self.config_client.get_value(trigger2.json(), 'metadata.name')
        i = 20
        while i > 0:  # 两条流水线是随机执行
            ret1 = self.pipeline.detail_pipeline(execution_id1)
            status1 = self.pipeline.get_value(ret1.json(), 'status.phase')
            ret2 = self.pipeline.detail_pipeline(execution_id2)
            status2 = self.pipeline.get_value(ret2.json(), 'status.phase')
            if status1 == 'Running' or status2 == 'Running':
                if status1 == 'Running':
                    assert status2 != "Running"
                    delete = self.pipeline.delete_pipeline(execution_id1)
                    assert delete.status_code == 200, delete.text
                    delete = self.pipeline.delete_pipeline(execution_id2)
                    assert delete.status_code == 200, delete.text
                    break
                else:
                    assert status1 != "Running"
                    delete = self.pipeline.delete_pipeline(execution_id1)
                    assert delete.status_code == 200, delete.text
                    delete = self.pipeline.delete_pipeline(execution_id2)
                    assert delete.status_code == 200, delete.text
                    break
            else:
                i = i - 1
                sleep(1)

    @pytest.mark.upgrade
    def 测试更新golang流水线(self):
        self.CodeQualityBinding.update({"name": bind_info[2]["bind_name"]})
        self.values['CodeQualityBinding'] = json.dumps(self.CodeQualityBinding)
        data = {
            "pipeline_name": self.config_name,
            "display_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": bind_info[0]["bind_name"],
            "pipeline_template_name": "GoLangBuilder",
            "values": self.values
        }
        data.update({"display_name": "update_golang", "runpolicy": "Parallel"})
        update_result = self.config_client.update_pipeline_config(self.config_name,
                                                                  "test_data/devops_pipeline/pipelineconfig-template.jinja2",
                                                                  data)
        assert update_result.status_code == 200, "更新流水线失败:{}".format(update_result.text)
        expect_data = {
            "pipeline_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": bind_info[0]["bind_name"],
            "runPolicy": "Parallel"
        }
        value = self.config_client.generate_jinja_data(
            'verify_data/devops_pipeline_config/update_pipeline_config.jinja2',
            data=expect_data)
        assert self.config_client.is_sub_dict(value, update_result.json()), \
            "创建流水线失败，期望数据:{},实际数据:{}".format(value, update_result.json())

    @pytest.mark.upgrade
    def 测试并行执行golang构建配置(self):
        logger.info("测试并行执行golang构建配置")
        trigger = self.config_client.trigger_pipeline_config(self.config_name)
        assert trigger.status_code == 200, "执行流水线失败{}".format(trigger.text)
        execution_id1 = self.config_client.get_value(trigger.json(), 'metadata.name')
        trigger = self.config_client.trigger_pipeline_config(self.config_name)
        assert trigger.status_code == 200, "执行流水线失败{}".format(trigger.text)
        execution_id2 = self.config_client.get_value(trigger.json(), 'metadata.name')
        status1 = self.pipeline.get_status(self.pipeline.detail_pipeline_url(execution_id1), 'status.phase', 'Running||Complete')
        status2 = self.pipeline.get_status(self.pipeline.detail_pipeline_url(execution_id2), 'status.phase', 'Running||Complete')
        assert status1 and status2, "并行执行失败"
        delete = self.pipeline.delete_pipeline(execution_id1)
        assert delete.status_code == 200, delete.text
        delete = self.pipeline.delete_pipeline(execution_id2)
        assert delete.status_code == 200, delete.text

    @pytest.mark.upgrade
    def 测试执行golang构建配置完成和再次执行(self):
        logger.info("测试执行golang构建配置完成")
        trigger = self.config_client.trigger_pipeline_config(self.config_name)
        assert trigger.status_code == 200, "执行流水线失败{}".format(trigger.text)
        execution_id = self.config_client.get_value(trigger.json(), 'metadata.name')
        completed = self.pipeline.get_status(url=self.pipeline.detail_pipeline_url(execution_id), key='status.phase',
                                             expect_value='Complete', expect_cnt=120)
        assert completed, "流水线执行失败{} 执行记录{}".format(self.config_name, execution_id)
        detail = self.pipeline.detail_pipeline(execution_id)
        success = self.pipeline.get_value(detail.json(), 'status.jenkins.result')
        assert success == "SUCCESS", "流水线执行失败{} 执行记录{}".format(self.config_name, execution_id)
        trigger = self.pipeline.retry_pipeline(execution_id)
        assert trigger.status_code == 200, trigger.text
        execution_id1 = self.config_client.get_value(trigger.json(), 'metadata.name')
        delete = self.pipeline.delete_pipeline(execution_id)
        assert delete.status_code == 200, delete.text
        delete = self.pipeline.delete_pipeline(execution_id1)
        assert delete.status_code == 200, delete.text

    @pytest.mark.upgrade
    def 测试取消执行golang构建配置(self):
        logger.info("测试取消执行golang构建配置")
        trigger = self.config_client.trigger_pipeline_config(self.config_name)
        assert trigger.status_code == 200, "执行流水线失败{}".format(trigger.text)
        execution_id = self.config_client.get_value(trigger.json(), 'metadata.name')
        running = self.pipeline.get_status(self.pipeline.detail_pipeline_url(execution_id), 'status.phase', 'Running')
        assert running, "流水线执行失败{} 执行记录{}".format(self.config_name, execution_id)
        abort = self.pipeline.abort_pipeline(execution_id)
        assert abort.status_code == 200, "取消流水线失败{}".format(abort.text)
        cancelled = self.pipeline.get_status(self.pipeline.detail_pipeline_url(execution_id), 'status.phase', 'Cancelled')
        ret = self.pipeline.detail_pipeline(execution_id)
        status = self.pipeline.get_value(ret.json(), 'status.phase')
        assert cancelled, "取消流水线失败,状态是{}".format(status)
        delete = self.pipeline.delete_pipeline(execution_id)
        assert delete.status_code == 200, delete.text

    @pytest.mark.delete
    def 测试删除golang构建配置记录(self):
        logger.info("测试删除golang构建配置记录")
        trigger = self.config_client.trigger_pipeline_config(self.config_name)
        assert trigger.status_code == 200, "执行流水线失败{}".format(trigger.text)
        execution_id = self.config_client.get_value(trigger.json(), 'metadata.name')
        delete = self.pipeline.delete_pipeline(execution_id)
        assert delete.status_code == 200, delete.text

    @pytest.mark.delete
    def 测试删除流水线(self):
        logger.info("测试删除流水线")
        del_ret = self.config_client.delete_pipeline_config(self.config_name)
        assert del_ret.status_code == 200, "删除流水线失败:{}".format(del_ret.text)
