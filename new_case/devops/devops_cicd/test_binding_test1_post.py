import pytest
from new_case.devops.tool_chain.tool_chain import ToolChain
from common import settings
from new_case.devops.devops_cicd.conftest import bind_info, tool_casename_list
import copy
from time import sleep
from common.check_status import checker


@pytest.mark.skipif(not checker.harbor_available(), reason="Harbor不可用")
@pytest.mark.skipif(not checker.harbor_available(), reason="Gitlab不可用")
@pytest.mark.skipif(not checker.jenkins_available(), reason="Jenkins不可用")
@pytest.mark.tool
@pytest.mark.jenkins
@pytest.mark.devopsApiServer
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestPostBinding(object):
    def setup_class(self):
        self.toolchain = ToolChain()
        global data_list
        data_list = copy.deepcopy(bind_info)
        for dict in data_list:
            name = dict["bind_name"]
            dict.update({"bind_name": name + "binding"})

    def 测试绑定工具链jenkins(self):
        data = dict()
        for i in range(len(data_list)):
            if data_list[i]["kind"] == "JenkinsBinding":
                data = data_list[i]
        if not self.toolchain.check_exists(self.toolchain.bind_project_url(type=data["api_type"], name=data["bind_name"]), 404, expect_cnt=1):
            self.toolchain.unbind_chain(type=data["api_type"], name=data["bind_name"])
        # 绑定jenkins工具
        ret = self.toolchain.bind_chain("jenkinsbinding", "test_data/tool_chain/create_jenkins_bind.jinja2", data=data)
        assert ret.status_code == 200, "绑定工具链jenkins失败:{}".format(ret.text)
        verify_data = data
        verify_data.update({"host_url": settings.JENKINS_URL})
        value = self.toolchain.generate_jinja_data("./verify_data/tool_chain/create_jenkins_bind.jinja2", data=verify_data)
        assert self.toolchain.is_sub_dict(value, ret.json()), \
            "绑定工具链jenkins失败，预期{},实际{}".format(value, ret.json())

    def 测试绑定工具harbor(self):
        data = dict()
        for i in range(len(data_list)):
            if data_list[i]["kind"] == "ImageRegistryBinding":
                data = data_list[i]
        if not self.toolchain.check_exists(self.toolchain.bind_project_url(type=data["api_type"], name=data["bind_name"]), 404, expect_cnt=1):
            self.toolchain.unbind_chain(type=data["api_type"], name=data["bind_name"])

        # 绑定harbor工具
        ret = self.toolchain.bind_chain("imageregistrybinding", "test_data/tool_chain/create_imagerepo_bind.jinja2", data=data)
        assert ret.status_code == 200, "绑定工具链harbor失败:{}".format(ret.text)
        verify_data = data
        verify_data.update({"host_url": settings.HARBOR_URL})
        value = self.toolchain.generate_jinja_data("./verify_data/tool_chain/create_harbor_bind.jinja2", data=verify_data)
        assert self.toolchain.is_sub_dict(value, ret.json()), \
            "绑定工具链harbor失败，预期{},实际{}".format(value, ret.json())

        # 获取harbor仓库和分配harbor仓库
        ret = self.toolchain.get_registry_repo(type="imageregistrybinding", name=data["bind_name"])
        assert ret.status_code == 200, "获取harbor镜像仓库失败"
        repo = settings.HARBOR_REPO
        repo_verify_data = {
            "items": [repo]
        }
        assert self.toolchain.is_sub_dict(repo_verify_data, ret.json()), "harbor没有镜像仓库:{}".format(ret.text)
        data.update({"remote_repo": repo})
        sleep(5)
        ret = self.toolchain.update_bind_chain("imageregistrybinding", data["bind_name"],
                                               "test_data/tool_chain/update_imagerepo_bind.jinja2", data)
        assert ret.status_code == 200, "分配镜像仓库失败"
        verify_data.update({"repos": repo_verify_data["items"]})
        value = self.toolchain.generate_jinja_data("./verify_data/tool_chain/create_harbor_bind.jinja2", data=verify_data)
        assert self.toolchain.is_sub_dict(value, ret.json()), \
            "Harbor分配仓库比对数据失败，预期{},实际{}".format(value, ret.json())

    def 测试绑定工具sonarqube(self):
        data = dict()
        for i in range(len(data_list)):
            if data_list[i]["kind"] == "CodeQualityBinding":
                data = data_list[i]
        if not self.toolchain.check_exists(self.toolchain.bind_project_url(type=data["api_type"], name=data["bind_name"]), 404, expect_cnt=1):
            self.toolchain.unbind_chain(type=data["api_type"], name=data["bind_name"])
        # 绑定代码扫描工具
        ret = self.toolchain.bind_chain("codequalitybinding", "test_data/tool_chain/create_sonarqube_bind.jinja2", data=data)
        assert ret.status_code == 200, "绑定工具链sonarqube失败:{}".format(ret.text)
        verify_data = data
        verify_data.update({"host_url": settings.SONAR_URL})
        value = self.toolchain.generate_jinja_data("./verify_data/tool_chain/create_sonar_bind.jinja2", data=verify_data)
        assert self.toolchain.is_sub_dict(value, ret.json()), \
            "绑定工具链sonar失败，预期{},实际{}".format(value, ret.json())

    def 测试绑定工具gitlab(self):
        data = dict()
        for i in range(len(data_list)):
            if data_list[i]["kind"] == "codeRepoService":
                data = data_list[i]
        if not self.toolchain.check_exists(self.toolchain.bind_project_url(type=data["api_type"], name=data["bind_name"]), 404, expect_cnt=1):
            self.toolchain.unbind_chain(type=data["api_type"], name=data["bind_name"])

        # 绑定gitlab工具
        data.update({"secret_namespace": settings.PROJECT_NAME})
        ret = self.toolchain.bind_chain("coderepobinding", "test_data/tool_chain/create_coderepo_bind.jinja2", data=data)
        assert ret.status_code == 200, "绑定工具链gitlab失败:{}".format(ret.text)
        verify_data = data
        verify_data.update({"host_url": settings.GITLAB_URL})
        value = self.toolchain.generate_jinja_data("./verify_data/tool_chain/create_gitlab_bind.jinja2", data=verify_data)
        assert self.toolchain.is_sub_dict(value, ret.json()), \
            "绑定工具链gitlab失败，预期{},实际{}".format(value, ret.json())

        # 获取gitlab代码仓库和分配代码仓库
        ret = self.toolchain.get_gitlab_remote_repo(type="coderepobinding", name=data["bind_name"])
        assert ret.status_code == 200, "获取代码仓库失败"
        gitlab_data = {
            "gitlab_url": settings.GITLAB_URL,
            "gitlab_user": settings.GITLAB_USERNAME,
            "repo_name": settings.GITLAB_REPO
        }
        value = self.toolchain.generate_jinja_data("./verify_data/tool_chain/gitlab_remote_repo.jinja2", data=gitlab_data)
        assert self.toolchain.is_sub_dict(value, ret.json()), \
            "获取gitlab远程仓库比对数据失败，预期{},实际{}".format(value, ret.json())
        owner_type = self.toolchain.get_value(value, "owners.0.repositories.0.owner.type")
        owner_name = self.toolchain.get_value(value, "owners.0.repositories.0.owner.name")
        repo_name = self.toolchain.get_value(value, "owners.0.repositories.0.name")
        data.update({"secret_namespace": settings.PROJECT_NAME, "ower_type": owner_type, "account": owner_name,
                     "repo_name": repo_name})
        sleep(5)
        ret = self.toolchain.update_bind_chain("coderepobinding", data["bind_name"],
                                               "test_data/tool_chain/update_coderepo_bind.jinja2", data)
        assert ret.status_code == 200, "分配代码仓库失败"
        verify_data = {"items": None, "errors": None}
        assert self.toolchain.is_sub_dict(verify_data, ret.json()), \
            "分配gitlab仓库失败，期望值:{}, 实际值:{}".format(verify_data, ret.json())

    def 测试绑定工具docker_registry(self):
        data = dict()
        for i in range(len(data_list)):
            if data_list[i]["kind"] == "DockerBinding":
                data = data_list[i]
        if not self.toolchain.check_exists(self.toolchain.bind_project_url(type=data["api_type"], name=data["bind_name"]), 404, expect_cnt=1):
            self.toolchain.unbind_chain(type=data["api_type"], name=data["bind_name"])

        # 绑定docker_registry工具
        ret = self.toolchain.bind_chain("imageregistrybinding", "test_data/tool_chain/create_imagerepo_bind.jinja2",
                                        data=data)
        assert ret.status_code == 200, "绑定工具链docker_registry失败:{}".format(ret.text)
        verify_data = data
        verify_data.update({"host_url": settings.DOCKER_REGISTRY_URL})
        value = self.toolchain.generate_jinja_data("./verify_data/tool_chain/create_docker_bind.jinja2", data=verify_data)
        assert self.toolchain.is_sub_dict(value, ret.json()), \
            "绑定docker_registry失败，期望值:{}, 实际值:{}".format(value, ret.json())

        sleep(3)
        # 获取docker_registry仓库和分配docker_registry仓库
        ret = self.toolchain.get_registry_repo(type="imageregistrybinding", name=data["bind_name"])
        assert ret.status_code == 200, "获取docker_registry仓库失败"
        if len(ret.json()["items"]) > 0:
            repo = ret.json()["items"][0]
        else:
            assert False, "docker_registry没有镜像仓库"
        data.update({"remote_repo": repo})
        sleep(5)
        ret = self.toolchain.update_bind_chain("imageregistrybinding", data["bind_name"],
                                               "test_data/tool_chain/update_docker_registry_bind.jinja2", data)
        assert ret.status_code == 200, "分配docker_registry镜像仓库失败"
        verify_data.update({"repos": [repo]})
        value = self.toolchain.generate_jinja_data("./verify_data/tool_chain/create_docker_bind.jinja2", data=verify_data)
        assert self.toolchain.is_sub_dict(value, ret.json()), \
            "Docker registry分配仓库比对数据失败，预期{},实际{}".format(value, ret.json())

    def 测试绑定工具JIRA(self):
        data = dict()
        for i in range(len(data_list)):
            if data_list[i]["kind"] == "ProjectManagementBinding":
                data = data_list[i]
        if not self.toolchain.check_exists(self.toolchain.bind_project_url(type=data["api_type"], name=data["bind_name"]), 404, expect_cnt=1):
            self.toolchain.unbind_chain(type=data["api_type"], name=data["bind_name"])

        # 绑定JIRA工具
        ret = self.toolchain.bind_chain("projectmanagementbinding", "test_data/tool_chain/create_projectmanger.jinja2",
                                        data=data)
        assert ret.status_code == 200, "绑定JIRA工具链失败:{}".format(ret.text)
        verify_data = data
        verify_data.update({"host_url": settings.JIRA_URL})
        value = self.toolchain.generate_jinja_data("./verify_data/tool_chain/create_jira_bind.jinja2", data=verify_data)
        assert self.toolchain.is_sub_dict(value, ret.json()), \
            "绑定JIRA失败，期望值:{}, 实际值:{}".format(value, ret.json())

        # 获取JIRA和分配JIRA项目
        ret = self.toolchain.get_jira_remote_repo(type="projectmanagement", secret_name=data["secret_name"], namespace=data["secret_namespace"],
                                                  tool_name=data["tool_name"])
        assert ret.status_code == 200, "获取jira项目失败"
        if len(ret.json()["items"]) > 0:
            jira_id = self.toolchain.get_value(ret.json(), "items.0.metadata.annotations.id")
            jira_key = self.toolchain.get_value(ret.json(), "items.0.metadata.annotations.key")
            jira_name = self.toolchain.get_value(ret.json(), "items.0.metadata.name")
        else:
            assert False, "JIRA项目为空"
        data.update({"jira_id": jira_id, "jira_key": jira_key, "jira_name": jira_name})
        sleep(5)
        ret = self.toolchain.update_bind_chain("projectmanagementbinding", data["bind_name"],
                                               "test_data/tool_chain/create_projectmanger.jinja2", data)
        assert ret.status_code == 200, "分配JIRA项目失败"
        value = self.toolchain.generate_jinja_data("./verify_data/tool_chain/create_jira_bind.jinja2", data=data)
        assert self.toolchain.is_sub_dict(value, ret.json()), \
            "分配JIRA项目失败，期望值:{}, 实际值:{}".format(value, ret.json())
        ret = self.toolchain.get_jiraproject_status(name=data["bind_name"])
        assert ret, "jira项目同步没成功"

    @pytest.mark.skipif(settings.AUDIT_UNABLED, reason="do not have audit")
    @pytest.mark.parametrize("data", bind_info, ids=tool_casename_list)
    def 测试工具链绑定审计(self, data):
        payload = {"user_name": settings.USERNAME, "operation_type": "create", "resource_type": data["audit_type"],
                   "resource_name": ""}
        result = self.toolchain.search_audit(payload)
        payload.update({"namespace": "", "resource_name": " ", "code": 201})
        values = self.toolchain.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.toolchain.is_sub_dict(values, result.json()), "审计数据不符合预期"
