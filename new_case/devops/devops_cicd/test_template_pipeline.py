import pytest
import json
from common.settings import RERUN_TIMES, PROJECT_NAME
from new_case.devops.devops_cicd.cicd import CicdApi
from common import settings
from common.log import logger
from new_case.devops.devops_cicd.conftest import app_name, bind_info
from new_case.containerplatform.application.application import Application
from new_case.containerplatform.deployment.deployment import Deployment


@pytest.mark.jenkins
@pytest.mark.devopsApiServer
@pytest.mark.devops_api
@pytest.mark.prepare
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestPipelineSuite(object):
    build_data = {
        "python2.7构建": {
            'pipeline_name': "{}-python2-7build".format(settings.RESOURCE_PREFIX),
            'pipeline_template_name': "Python2.7Builder",
        },
        "python3.6构建": {
            'pipeline_name': "{}-python3-6build".format(settings.RESOURCE_PREFIX),
            'pipeline_template_name': "Python3.6Builder",
        },
        "nodejs10构建": {
            'pipeline_name': "{}-nodejsbuild".format(settings.RESOURCE_PREFIX),
            'pipeline_template_name': "NodejsBuilder",
        }
    }
    build_update_data = {
        "python2.7构建并更新应用": {
            'pipeline_name': "{}-python2-7build-update".format(settings.RESOURCE_PREFIX),
            'pipeline_template_name': "Python2.7BuilderAndDeployService",
        },
        "python3.6构建并更新应用": {
            'pipeline_name': "{}-python3-6build-update".format(settings.RESOURCE_PREFIX),
            'pipeline_template_name': "Python3.6BuilderAndDeployService",
        },
        "nodejs10构建并更新应用": {
            'pipeline_name': "{}-nodejsbuild-update".format(settings.RESOURCE_PREFIX),
            'pipeline_template_name': "NodejsBuilderAndDeployService",
        }
    }

    def setup_class(self):
        self.config_client = CicdApi('pipelineconfig')
        self.app_client = Application()
        self.deploy_client = Deployment()
        self.pipeline = CicdApi('pipeline')
        self.image_repo_client = CicdApi('imagerepository')
        self.code_repo_client = CicdApi('coderepository')
        self.config_name = ''
        code_info = self.code_repo_client.get_code_repo_info(settings.GITLAB_REPO)
        self.codeRepository = {"url": "", "credentialId": "", "kind": "select", "bindingRepositoryName": "",
                               "sourceType": "GIT"}
        self.codeRepository.update(code_info)
        self.CodeQualityBinding = {"namespace": self.config_client.project_name, "name": bind_info[2]["bind_name"]}
        self.image_info = self.image_repo_client.get_image_repo_info(settings.HARBOR_REPO)
        self.imageRepository = {"type": "select", "repositoryPath": "", "tag": "e2etest", "secretName": "",
                                "secretNamespace": "", "credentialId": ""}
        self.imageRepository.update(self.image_info)
        self.values = {"PlatformCodeRepository": json.dumps(self.codeRepository), "Branch": "master",
                       "RelativeDirectory": "src",
                       "buildCommand": "cd go;go build", "UseSonarQube": "true",
                       "CodeQualityBinding": json.dumps(self.CodeQualityBinding),
                       "EnableBranchAnalysis": "false",
                       "AnalysisParameters": "sonar.sources=./go\nsonar.sourceEncoding=UTF-8\n",
                       "FailedIfNotPassQualityGate": "false", "imageRepository": json.dumps(self.imageRepository),
                       "context": "./go", "buildArguments": "", "dockerfile": "Dockerfile", "retry": "3", "UseNotification": "false",
                       "PlatformNotification": "\"\""}

    def teardown_method(self):
        if settings.CASE_TYPE not in ("prepare", "upgrade"):
            self.config_client.delete_pipeline_config(self.config_name)

    @pytest.mark.parametrize("key", build_update_data)
    def 测试模版构建并更新应用(self, key):
        data = self.build_update_data[key]
        logger.info('测试模版构建并更新应用{}'.format(key))
        self.config_name = data["pipeline_name"]
        self.config_client.delete_pipeline_config(self.config_name)
        application = {"clusterName": settings.REGION_NAME, "namespace": settings.K8S_NAMESPACE,
                       "createApp": "false",
                       "serviceName": "{}:deployment:{}".format(app_name, app_name),
                       "containerName": app_name, "rollback": "false", "command": "", "args": "", "timeout": "300"}
        self.values.update(application)
        self.values.update({"buildCommand": "ls"})
        values = {
            "pipeline_name": self.config_name,
            "display_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": bind_info[0]["bind_name"],
            "pipeline_template_name": data["pipeline_template_name"],
            "values": self.values
        }
        body_data = self.config_client.generate_template_data(data=values)
        create_result = self.config_client.create_pipeline_config(body_data)
        assert create_result.status_code == 200, create_result.text
        trigger = self.config_client.trigger_pipeline_config(self.config_name)
        assert trigger.status_code == 200, "执行流水线失败{}".format(trigger.text)
        execution_id = self.config_client.get_value(trigger.json(), 'metadata.name')
        completed = self.pipeline.get_status(self.pipeline.detail_pipeline_url(execution_id), 'status.phase',
                                             'Complete', expect_cnt=120)
        assert completed, "流水线执行失败{} 执行记录{}".format(self.config_name, execution_id)

    @pytest.mark.parametrize("key", build_data)
    def 测试模版构建(self, key):
        data = self.build_data[key]
        logger.info('测试模版构建{}'.format(key))
        self.config_name = data["pipeline_name"]
        self.config_client.delete_pipeline_config(self.config_name)
        self.values['buildCommand'] = "ls"
        values = {
            "pipeline_name": self.config_name,
            "display_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": bind_info[0]["bind_name"],
            "pipeline_template_name": data["pipeline_template_name"],
            "values": self.values
        }
        body_data = self.config_client.generate_template_data(data=values)
        create_result = self.config_client.create_pipeline_config(body_data)
        assert create_result.status_code == 200, create_result.text
        trigger = self.config_client.trigger_pipeline_config(self.config_name)
        assert trigger.status_code == 200, "执行流水线失败{}".format(trigger.text)
        execution_id = self.config_client.get_value(trigger.json(), 'metadata.name')
        status = self.pipeline.get_status(self.pipeline.detail_pipeline_url(execution_id), 'status.phase',
                                          'Complete', expect_cnt=120)
        assert status, "流水线执行失败{} 执行记录{}".format(self.config_name, execution_id)

    def 测试镜像构建模版(self):
        logger.info('测试镜像构建模板')
        self.config_name = "{}-imagebuild".format(settings.RESOURCE_PREFIX)
        self.config_client.delete_pipeline_config(self.config_name)
        value = {
            "PlatformCodeRepository": self.values["PlatformCodeRepository"],
            "Branch": self.values["Branch"],
            "RelativeDirectory": self.values["RelativeDirectory"],
            "imageRepository": self.values["imageRepository"],
            "context": self.values["context"],
            "buildArguments": self.values["buildArguments"],
            "dockerfile": self.values["dockerfile"],
            "retry": self.values["retry"],
            "UseNotification": self.values['UseNotification'],
            "PlatformNotification": self.values['PlatformNotification']
        }
        values = {
            "pipeline_name": self.config_name,
            "display_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": bind_info[0]["bind_name"],
            "pipeline_template_name": "ImageBuild",
            "values": value
        }
        body_data = self.config_client.generate_template_data(data=values)
        create_result = self.config_client.create_pipeline_config(body_data)
        assert create_result.status_code == 200, create_result.text
        trigger = self.config_client.trigger_pipeline_config(self.config_name)
        assert trigger.status_code == 200, "执行流水线失败{}".format(trigger.text)
        execution_id = self.config_client.get_value(trigger.json(), 'metadata.name')
        status = self.pipeline.get_status(self.pipeline.detail_pipeline_url(execution_id), 'status.phase',
                                          'Complete', expect_cnt=120)
        assert status, "流水线执行失败{} 执行记录{}".format(self.config_name, execution_id)

    def 测试更新应用模版(self):
        logger.info('测试更新应用模板')
        self.config_name = "{}-updateservice".format(settings.RESOURCE_PREFIX)
        self.config_client.delete_pipeline_config(self.config_name)
        application = {"clusterName": settings.REGION_NAME, "namespace": settings.K8S_NAMESPACE,
                       "serviceName": "{}:deployment:{}".format(app_name, app_name),
                       "containerName": app_name, "rollback": "false", "command": "", "args": "", "timeout": "300", "UseNotification": "false",
                       "PlatformNotification": "\"\""}
        self.imageRepository.update({"tag": "${imageTag}"})
        value = {"imageRepositoryDeploy": json.dumps(self.imageRepository)}
        value.update(application)
        values = {
            "pipeline_name": self.config_name,
            "display_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": bind_info[0]["bind_name"],
            "pipeline_template_name": "alaudaDeployService",
            "values": value
        }
        body_data = self.config_client.generate_template_data(data=values)
        create_result = self.config_client.create_pipeline_config(body_data)
        assert create_result.status_code == 200, create_result.text
        trigger = self.config_client.trigger_pipeline_config(self.config_name,
                                                             data={"params": [{"name": "imageTag", "type": "StringParameterDefinition", "value": "e2etest"}]})
        assert trigger.status_code == 200, "执行流水线失败{}".format(trigger.text)
        execution_id = self.config_client.get_value(trigger.json(), 'metadata.name')
        completed = self.pipeline.get_status(self.pipeline.detail_pipeline_url(execution_id), 'status.phase',
                                             'Complete', expect_cnt=120)
        assert completed, "流水线执行失败{} 执行记录{}".format(self.config_name, execution_id)
        # app_detail = self.app_client.detail_app(settings.K8S_NAMESPACE, app_name)
        # assert app_detail.status_code == 200, "获取流水线更新的应用失败:{}".format(app_detail.text)
        # image = "{}/{}:{}".format(settings.HARBOR_URL.split("//")[-1], settings.HARBOR_REPO, "e2etest")
        # assert image in app_detail.text, "更新后镜像版本不对:{}".format(app_detail.text)

    def 测试脚本创建流水线(self):
        self.config_name = "{}-script-repo".format(settings.RESOURCE_PREFIX)
        self.config_client.delete_pipeline_config(self.config_name)
        logger.info('测试创建执行脚本代码方式')
        source = {
            "sourceType": self.codeRepository["sourceType"],
            "codeRepository": {
                "name": self.codeRepository["bindingRepositoryName"],
                "ref": "master"
            },
            "secret": {
                "name": bind_info[3]["secret_name"],
                "namespace": bind_info[3]["namespace"]
            }
        }
        values = {
            "pipeline_name": self.config_name,
            "display_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": bind_info[0]["bind_name"],
            "source": source
        }
        body_data = self.config_client.generate_script_data(data=values)
        create_result = self.config_client.create_pipeline_config(body_data)
        assert create_result.status_code == 200, create_result.text
        trigger = self.config_client.trigger_pipeline_config(self.config_name)
        assert trigger.status_code == 200, "执行流水线失败{}".format(trigger.text)
        execution_id = self.config_client.get_value(trigger.json(), 'metadata.name')
        completed = self.pipeline.get_status(self.pipeline.detail_pipeline_url(execution_id), 'status.phase',
                                             'Complete', expect_cnt=120)
        assert completed, "流水线执行失败{} 执行记录{}".format(self.config_name, execution_id)
        detail = self.pipeline.detail_pipeline(execution_id)
        success = self.pipeline.get_value(detail.json(), 'status.jenkins.result')
        assert success == "SUCCESS", "流水线执行失败{} 执行记录{}".format(self.config_name, execution_id)

    @pytest.mark.skip(reason="not ready")
    def 测试项目下自定义模版(self):
        # 到管理视图-项目下-流水线模版-配置模版仓库-输入方式-代码仓库地址 https://gitlab.alauda.cn/root/template-public.git
        # 分支 test 然后确定等待流水线模版同步完成
        # 到业务视图创建流水线-模版创建-选择构建应用AAA-按照流水线其他步骤-输入相关必填写信息
        # 检查流水线状态可以ready并能运行
        # 有必要可以运行成功
        assert True
