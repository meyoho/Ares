import pytest
from common.settings import RERUN_TIMES, HARBOR_REPO, PROJECT_NAME, HARBOR_URL
from new_case.devops.devops_cicd.cicd import CicdApi
from new_case.devops.devops_cicd.conftest import bind_info


@pytest.mark.devopsApiServer
@pytest.mark.BAT
@pytest.mark.tool
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestImageRepoSuite(object):
    def setup_class(self):
        self.imagerepo = CicdApi('imagerepository')
        self.image_info = self.imagerepo.get_image_repo_info(HARBOR_REPO, get_name=True)

    def 测试获取devops制品仓库列表(self):
        # list image repo
        list_result = self.imagerepo.list_image_repo()
        assert list_result.status_code == 200, list_result.text
        data = {
            "image_repo_name": self.image_info["repo_name"],
            "namespace": PROJECT_NAME,
            "harbor_tool_name": bind_info[1]["tool_name"],
            "harbor_bind": bind_info[1]["bind_name"],
            "harbor_url": HARBOR_URL.split("//")[-1],
            "harbor_repo": HARBOR_REPO,
            "secret_name": bind_info[1]["secret_name"]
        }
        value = self.imagerepo.generate_jinja_data("./verify_data/devops_pipeline_config/list_image_repo.jinja2",
                                                   data=data)
        assert self.imagerepo.is_sub_dict(value, list_result.json()), "harbor列表对比预期数据失败，实际数据:{},期望数据{}".format(list_result.json(), value)
