import pytest
from common.settings import RERUN_TIMES, PROJECT_NAME, CASE_TYPE, DEFAULT_LABEL, GITLAB_REPO
from new_case.devops.devops_cicd.cicd import CicdApi
from new_case.devops.devops_cicd.conftest import bind_info
from common.log import logger
from new_case.devops.devops_cicd.conftest import pipeline_config
from time import sleep


@pytest.mark.jenkins
@pytest.mark.devopsApiServer
@pytest.mark.BAT
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestPipelineSuite(object):
    def setup_class(self):
        self.config_client = CicdApi('pipelineconfig')
        self.pipeline_client = CicdApi("pipeline")
        self.code_repo_client = CicdApi('coderepository')
        self.config_name = pipeline_config[10]['name']
        source = self.code_repo_client.get_code_repo_source(GITLAB_REPO)
        self.value = {
            "pipeline_name": self.config_name,
            "display_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": bind_info[0]["bind_name"],
            "cron": "true",
            "rule": "H/2 * * * *",
            "source": source,
        }
        self.body_data = self.config_client.generate_multi_data(data=self.value)

    def teardown_class(self):
        if CASE_TYPE not in ("prepare", "upgrade"):
            self.config_client.delete_pipeline_config(self.config_name)

    @pytest.mark.prepare
    def 测试创建GitLab多分支流水线(self):
        logger.info('测试创建GitLab多分支流水线')
        self.config_client.delete_pipeline_config(self.config_name)
        create_result = self.config_client.create_pipeline_config(self.body_data)
        assert create_result.status_code == 200, create_result.text
        scan = self.config_client.scan_pipeline_config(self.config_name)
        assert scan.status_code == 200, "多分支扫描失败{}".format(scan.text)
        assert self.config_client.scan_find_PR(self.config_name), "无法发现多分支流水线的PR"
        assert self.config_client.multi_pipeline_complate(self.config_name), "没有通过全部的流水线"

    def 测试多分支详情页(self):
        # 验证项 1、执行记录有数据、能扫出master 和非master分支 2、点击详情按钮接口正常
        params = {
            "filterBy": "labels,pipelineConfig:" + self.config_name,
            "sortBy": "d,creationTimestamp",
            "itemsPerPage": 10,
            "page": 1
        }
        ret = self.pipeline_client.list_pipeline_config(params=params)
        assert ret.status_code == 200, "获取流水线执行记录失败"
        assert ret.json()["listMeta"]["totalItems"] >= 2, "获取的流水线只有一条:{}".format(ret.text)
        branches = self.pipeline_client.get_value_list(ret.json()["pipelines"],
                                                       "metadata#annotations#{}/multiBranchName".format(DEFAULT_LABEL),
                                                       delimiter="#")
        assert "master" in branches and len(branches) >= 2, "执行记录没有master或者记录少于2条:{}".format(ret.text)
        for name in self.pipeline_client.get_value_list(ret.json()["pipelines"], "metadata.name"):
            ret = self.pipeline_client.detail_pipeline(name)
            assert ret.status_code == 200, "获取多分流水线执行记录详情失败"

    def 测试更新Gitlab多分支流水线(self):
        # 更新 分支名称过滤，更新为：master|PR-.*|MR-.*|muti-branch 。然后执行，muti-branch 分支是不是被扫出来
        self.value["source"]['codeRepository']['ref'] = "master|PR-.*|MR-.*|muti-branch"
        self.value.update({"filterExpression": "master|PR-.*|MR-.*|muti-branch"})
        update_ret = self.config_client.update_pipeline_config(self.config_name,
                                                               "test_data/devops_pipeline/pipelineconfig-multi.jinja2",
                                                               self.value)
        assert update_ret.status_code == 200, update_ret.text
        assert "muti-branch" in update_ret.text, "新增的分支不在响应体:{}".format(update_ret.text)
        scan = self.config_client.scan_pipeline_config(self.config_name)
        assert scan.status_code == 200, "多分支扫描失败{}".format(scan.text)
        sleep(60)
        assert self.config_client.multi_pipeline_complate(self.config_name), "没有通过全部的流水线"

        params = {
            "filterBy": "labels,pipelineConfig:" + self.config_name,
            "sortBy": "d,creationTimestamp",
            "itemsPerPage": 10,
            "page": 1
        }
        ret = self.pipeline_client.list_pipeline_config(params=params)
        branches = self.pipeline_client.get_value_list(ret.json()["pipelines"],
                                                       "metadata#annotations#{}/multiBranchName".format(DEFAULT_LABEL),
                                                       delimiter="#")
        assert "muti-branch" in branches and len(branches) >= 3, "执行记录没有muti-branch或者记录少于3条:{}".format(ret.text)
