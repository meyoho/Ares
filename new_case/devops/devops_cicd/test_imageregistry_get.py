import pytest
from common.settings import RERUN_TIMES
from common import settings
from new_case.devops.devops_cicd.cicd import CicdApi
from new_case.devops.devops_cicd.conftest import bind_info


@pytest.mark.devopsApiServer
@pytest.mark.devops_api
@pytest.mark.tool
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestImageRepoSuite(object):
    def setup_class(self):
        self.imagerepo = CicdApi('imagerepository')
        image_info = self.imagerepo.get_image_repo_info(settings.HARBOR_REPO, get_name=True)
        self.image_repo_name = image_info["repo_name"]

    def 测试获得Harbor仓库详情(self):
        image_repo_detail = self.imagerepo.detail_image_repo(self.image_repo_name)
        assert image_repo_detail.status_code == 200, image_repo_detail.text

        data = {"name": self.image_repo_name, "namespace": settings.PROJECT_NAME,
                "harbor_tool_name": bind_info[1]['tool_name'], "harbor_bind": bind_info[1]['bind_name']}
        value = self.imagerepo.generate_jinja_data("./verify_data/devops_pipeline_config/detail_image_repo.jinja2",
                                                   data=data)
        assert self.imagerepo.is_sub_dict(value, image_repo_detail.json()), \
            "harbor镜像详情对比预期数据失败，实际数据:{},期望数据{}".format(image_repo_detail.json(), value)

    def 测试获得Harbor仓库tags(self):
        tags = self.imagerepo.tags_image_repo(self.image_repo_name)
        assert tags.status_code == 200
        tags_length = len(self.imagerepo.get_value(tags.json(), 'tags'))
        assert tags_length >= 1, "镜像tag列表信息失败,实际镜像tags {}个".format(tags_length)
