import pytest
import json
from common.settings import RERUN_TIMES, PROJECT_NAME
from new_case.devops.devops_cicd.cicd import CicdApi
from common import settings
from common.log import logger


@pytest.mark.jenkins
@pytest.mark.devopsApiServer
@pytest.mark.BAT
@pytest.mark.tool
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestPipelineSuite(object):
    def setup_class(self):
        self.config_client = CicdApi('pipelineconfig')
        self.pipeline = CicdApi('pipeline')
        self.image_repo_client = CicdApi('imagerepository')
        self.code_repo_client = CicdApi('coderepository')
        self.config_name = "{}-ares-pipeline-config-test-general-view".format(settings.RESOURCE_PREFIX)
        self.codeRepository = {"url": "", "credentialId": "", "kind": "select", "bindingRepositoryName": "",
                               "sourceType": "GIT"}
        self.CodeQualityBinding = {"namespace": self.config_client.project_name, "name": ""}

        self.imageRepository = {"type": "select", "repositoryPath": "", "tag": "e2etest", "secretName": "",
                                "secretNamespace": "", "credentialId": ""}
        self.values = {"PlatformCodeRepository": self.codeRepository, "Branch": "master",
                       "RelativeDirectory": "src",
                       "buildCommand": "cd go;go build", "UseSonarQube": "true",
                       "CodeQualityBinding": self.CodeQualityBinding,
                       "EnableBranchAnalysis": "false",
                       "AnalysisParameters": "sonar.sources=.\nsonar.sourceEncoding=UTF-8\n",
                       "FailedIfNotPassQualityGate": "false", "imageRepository": self.imageRepository,
                       "context": ".", "buildArguments": "", "dockerfile": "Dockerfile", "retry": "3"}

    def teardown_method(self):
        self.config_client.delete_pipeline_config(self.config_name)

    def 测试概览页(self, prepare_tool):
        logger.info('测试概览页统计')
        image_repo = self.image_repo_client.get_image_repo_info(settings.HARBOR_REPO)
        code_repo = self.code_repo_client.get_code_repo_info(settings.GITLAB_REPO)
        logger.info("镜像信息 {}".format(json.dumps(image_repo)))
        logger.info("代码仓库信息 {}".format(json.dumps(code_repo)))
        self.imageRepository.update(image_repo)
        self.codeRepository.update(code_repo)
        self.CodeQualityBinding.update({"name": prepare_tool[2]["bind_name"]})
        global data
        self.values['PlatformCodeRepository'] = json.dumps(self.codeRepository)
        self.values['CodeQualityBinding'] = json.dumps(self.CodeQualityBinding)
        self.values['imageRepository'] = json.dumps(self.imageRepository)
        data = {
            "pipeline_name": self.config_name,
            "display_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": prepare_tool[0]["bind_name"],
            "pipeline_template_name": "GoLangBuilder",
            "values": self.values
        }
        body_data = self.config_client.generate_template_data(data=data)
        create_result = self.config_client.create_pipeline_config(body_data)
        assert create_result.status_code == 200, create_result.text
        expect_data = {
            "pipeline_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": prepare_tool[0]["bind_name"],
            "runPolicy": "Serial"
        }
        value = self.config_client.generate_jinja_data(
            'verify_data/devops_pipeline_config/create_pipeline_config.jinja2',
            data=expect_data)
        assert self.config_client.is_sub_dict(value, create_result.json()), \
            "创建流水线失败，期望数据:{},实际数据:{}".format(value, create_result.json())
        trigger = self.config_client.trigger_pipeline_config(self.config_name)
        assert trigger.status_code == 200, "执行流水线失败{}".format(trigger.text)
        statistics = self.config_client.statistics('codequality')
        assert statistics.status_code == 200, "获得代码质量概览失败:{}".format(statistics.text)
        params = {"period": "-25h"}
        statistics = self.config_client.statistics('pipeline', params=params)
        assert statistics.status_code == 200, "获得流水线执行概览失败:{}".format(statistics.text)
        params = {"period": "-25h"}
        stage = self.config_client.statistics('stage', params=params)
        assert stage.status_code == 200, "获得流水线stage失败:{}".format(stage.text)
