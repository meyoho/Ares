import pytest
from common.settings import RERUN_TIMES, HARBOR_REPO
from new_case.devops.devops_cicd.cicd import CicdApi


@pytest.mark.devopsApiServer
@pytest.mark.devops_api
@pytest.mark.tool
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestImageRepoSuite(object):
    def setup_class(self):
        self.imagerepo = CicdApi('imagerepository')

    def 测试扫描harbor镜像仓库(self):
        image_info = self.imagerepo.get_image_repo_info(HARBOR_REPO, get_name=True)
        image_repo_name = image_info["repo_name"]
        scan = self.imagerepo.scan_image_repo(image_repo_name)
        assert scan.status_code == 200, "扫描harbor镜像任务失败"
        message = self.imagerepo.get_value(scan.json(), 'message')
        assert "postRepositoriesRepoNameTagsTagScanAccepted" in message
