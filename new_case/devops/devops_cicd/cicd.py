import sys
from common import settings
from common.base_request import Common
from common.log import logger
import yaml
from time import sleep


class CicdApi(Common):
    def __init__(self, resource_type, project=settings.PROJECT_NAME):
        super(CicdApi, self).__init__()
        self.resource_type = resource_type
        self.project_name = project
        self.template_body = self.generate_jinja_template('test_data/devops_pipeline/pipelineconfig-template.jinja2')
        self.script_body = self.generate_jinja_template('test_data/devops_pipeline/pipelineconfig-script.jinja2')
        self.multi_body = self.generate_jinja_template('test_data/devops_pipeline/pipelineconfig-multi.jinja2')

    def generate_template_data(self, data={}):
        data.update({"default_label": settings.DEFAULT_LABEL})
        data = yaml.safe_load(self.template_body.render(data))
        return data

    def generate_script_data(self, data={}):
        data.update({"default_label": settings.DEFAULT_LABEL})
        data = yaml.safe_load(self.script_body.render(data))
        return data

    def generate_multi_data(self, data={}):
        data.update({"default_label": settings.DEFAULT_LABEL})
        data = yaml.safe_load(self.multi_body.render(data))
        return data

    def cicd_resource_url(self, resource_name=""):
        return resource_name and \
               'devops/api/v1/{}/{}/{}'.format(self.resource_type, self.project_name, resource_name) \
               or 'devops/api/v1/{}/{}'.format(self.resource_type, self.project_name)

    def pipeline_log_url(self, id):
        return "devops/api/v1/pipeline/{}/{}/logs?start=0".format(settings.PROJECT_NAME, id)

    def binding_url(self, tool_type=""):
        return 'devops/api/v1/toolchain/bindings/{}?tool_type={}'.format(self.project_name, tool_type)

    def jira_issueoptions_url(self, jira_bind_name=""):
        return 'devops/api/v1/projectmanagementbinding/{}/{}/issueoptions?type=all'.format(self.project_name,
                                                                                           jira_bind_name)

    def jira_issuelist_url(self, jira_bind_name=""):
        return 'devops/api/v1/projectmanagementbinding/{}/{}/issueslist?page=1&itemsPerPage=20&orderby=updated'.format(
            self.project_name, jira_bind_name)

    def jira_issuedetail_url(self, jira_bind_name="", jira_key=""):
        return 'devops/api/v1/projectmanagementbinding/{}/{}/issue?key={}'.format(self.project_name, jira_bind_name,
                                                                                  jira_key)

    def create_pipeline_config(self, data):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.cicd_resource_url()
        return self.send(method='post', path=url, json=data)

    def list_pipeline_config(self, params={}):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.cicd_resource_url()
        return self.send(method='get', path=url, params=params)

    def detail_pipeline_config(self, resource_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.cicd_resource_url(resource_name)
        return self.send(method='get', path=url)

    def update_pipeline_config(self, resource_name, file, data):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.cicd_resource_url(resource_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='put', path=url, json=data)

    def delete_pipeline_config(self, resource_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.cicd_resource_url(resource_name)
        return self.send(method='delete', path=url)

    def get_pipelinetemplate(self):
        path = "devops/api/v1/clusterpipelinetemplate"
        return self.send(method='get', path=path)

    def trigger_pipeline_config(self, resource_name, data={}):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.cicd_resource_url(resource_name) + '/trigger'
        return self.send(method='post', path=url, json=data)

    def detail_pipeline_url(self, execution_id):
        return "{}/{}".format(self.cicd_resource_url(), execution_id)

    def detail_pipeline(self, execution_id):
        return self.send(method='get', path=self.detail_pipeline_url(execution_id))

    def abort_pipeline(self, execution_id):
        url = "{}/{}/abort".format(self.cicd_resource_url(), execution_id)
        return self.send(method='put', path=url)

    def retry_pipeline(self, execution_id):
        url = "{}/{}/retry".format(self.cicd_resource_url(), execution_id)
        return self.send(method='post', path=url)

    def delete_pipeline(self, execution_id):
        url = "{}/{}".format(self.cicd_resource_url(), execution_id)
        return self.send(method='delete', path=url)

    def statistics(self, resource_name, **params):
        url = "devops/api/v1/statistics/{}/{}".format(resource_name, settings.PROJECT_NAME)
        return self.send(method='get', path=url, **params)

    def get_status(self, url, key, expect_value, delimiter='.', params={}, expect_cnt=30):
        sleep(1)
        status = super(CicdApi, self).get_status(url, key, expect_value, delimiter, params, expect_cnt)
        if not status:
            id = url.split("/")[-1]
            log = self.send(method="get", path=self.pipeline_log_url(id))
            logger.error("获取状态失败,相关日志:{}".format(log.content))
        return status

    def list_code_repo(self):
        url = self.cicd_resource_url()
        return self.send(method='get', path=url)

    def list_image_repo(self):
        url = self.cicd_resource_url()
        return self.send(method='get', path=url)

    def list_binding(self, tool_type):
        url = self.binding_url(tool_type)
        return self.send(method='get', path=url)

    def list_jira_issueoptions(self, jira_bind_name):
        url = self.jira_issueoptions_url(jira_bind_name)
        return self.send(method='get', path=url)

    def list_jira_issuelist(self, jira_bind_name):
        url = self.jira_issuelist_url(jira_bind_name)
        return self.send(method='get', path=url)

    def deatil_jira_issue(self, jira_bind_name, jira_key):
        url = self.jira_issuedetail_url(jira_bind_name, jira_key)
        return self.send(method='get', path=url)

    def detail_image_repo(self, name):
        url = self.cicd_resource_url(name)
        return self.send(method='get', path=url)

    def tags_image_repo(self, name):
        url = "{}/tags".format(self.cicd_resource_url(name))
        return self.send(method='get', path=url)

    def scan_image_repo(self, name, tag='latest'):
        url = "{}/security".format(self.cicd_resource_url(name))
        params = {'tag': tag}
        return self.send(method='post', path=url, params=params)

    def get_report(self, execution_id):
        url = "{}/{}/testreports".format(self.cicd_resource_url(), execution_id)
        params = {'start': 0, "limit": 50}
        return self.send(method='get', path=url, params=params)

    def get_image_repo_info(self, image_name, get_name=False):
        '''
        :return: image code info
        '''
        i = 20
        while i > 0:  # 两条流水线是随机执行
            code_list = self.list_image_repo()
            number = self.get_value(code_list.json(), 'listMeta.totalItems')
            if number > 0:
                break
            else:
                i = i - 1
                sleep(2)
        assert i, "镜像仓库同步失败，请手工检查"
        image_list = self.list_image_repo()
        assert image_list.status_code == 200, '获取镜像仓库列表失败:{}'.format(image_list.text)
        image_repos = image_list.json()["imagerepositories"]
        image_endpont = image = secret_name = secret_ns = repo_name = ""
        for temp_image in image_repos:
            if self.get_value(temp_image, "spec.image") == image_name:
                image_endpont = self.get_value(temp_image, "metadata.annotations.imageRegistryEndpoint")
                image = self.get_value(temp_image, "spec.image")
                secret_name = self.get_value(temp_image, "metadata.annotations.secretName")
                secret_ns = self.get_value(temp_image, "metadata.annotations.secretNamespace")
                repo_name = self.get_value(temp_image, "metadata.name")
                break

        image_info = {
            "repositoryPath": "{}/{}".format(image_endpont, image),
            "secretName": secret_name,
            "secretNamespace": secret_ns,
            "credentialId": "{}-{}".format(secret_ns, secret_name)
        }
        if get_name:
            image_info.update({"repo_name": repo_name})
        return image_info

    def get_code_repo_info(self, code_name, get_name=False):
        '''
        :return: image code info
        '''
        global code_list
        i = 20
        while i > 0:
            code_list = self.list_code_repo()
            number = self.get_value(code_list.json(), 'listMeta.totalItems')
            code_names = self.get_value_list(code_list.json(), "coderepositories||spec.repository.name")
            if number > 0 and code_name in code_names:
                break
            else:
                sleep(2)
                i = i - 1
        assert i, "同步代码仓库失败，请手工验证"
        code_repos = code_list.json()["coderepositories"]
        repo_url = binding_repo_name = secret_name = secret_ns = code_repo_name = ""
        for code_repo in code_repos:
            if self.get_value(code_repo, "spec.repository.name") == code_name:
                repo_url = self.get_value(code_repo, "spec.repository.htmlURL")
                binding_repo_name = self.get_value(code_repo, "metadata.name")
                secret_name = self.get_value(code_repo, "metadata.annotations.secretName")
                secret_ns = self.get_value(code_repo, "metadata.annotations.secretNamespace")
                code_repo_name = self.get_value(code_repo, "metadata.name")
                break

        repo_info = {
            "url": repo_url,
            "credentialId": "{}-{}".format(secret_ns, secret_name),
            "bindingRepositoryName": binding_repo_name,
        }
        if get_name:
            repo_info.update({"code_repo_name": code_repo_name})
        return repo_info

    def scan_pipeline_config(self, resource_name, data={}, retry_num=60, delay_time=5):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        for i in range(retry_num):
            pipeconfig_status = self.send(method='get', path=self.cicd_resource_url(resource_name), params={})
            assert pipeconfig_status.status_code == 200, "没有创建pipelineconfig"
            pipeconfig_info = pipeconfig_status.json()
            if str(self.get_value(pipeconfig_info, "status.phase")).lower() == 'ready':
                break
            else:
                sleep(delay_time)
        url = self.cicd_resource_url(resource_name) + '/scan'
        return self.send(method='post', path=url, json=data)

    def scan_find_PR(self, resource_name, retry_num=60, delay_time=5):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        for i in range(retry_num):
            multi_info = self.send(method='get', path=self.cicd_resource_url(resource_name), params={})
            assert multi_info.status_code == 200, '无法找到多分支流水线'
            all_pipeline = multi_info.json()['mulitiBranchPipelines']
            for pipeline in all_pipeline.values():
                for info in pipeline:
                    if 'metadata' in info and 'annotations' in info['metadata'] and \
                            info['metadata']['annotations'][
                                '{}/multiBranchCategory'.format(settings.DEFAULT_LABEL)] == 'pr':
                        return True
            sleep(delay_time)
        return False

    def multi_pipeline_complate(self, resource_name, until_all_complete=True, retry_num=60, delay_time=5):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        all_pipelines = {}
        for i in range(retry_num):
            multi_info = self.send(method='get', path=self.cicd_resource_url(resource_name), params={})
            assert multi_info.status_code == 200, '无法找到多分支流水线'
            for pipeline in multi_info.json()['mulitiBranchPipelines'].values():
                for p in pipeline:
                    all_pipelines[self.get_value(p, 'metadata.name')] = p
            complate_num = len(
                [val for val in all_pipelines.values() if 'status' in val and val['status']['phase'] == 'Complete'])
            if complate_num > 0 and not until_all_complete:
                return True
            if complate_num == len(all_pipelines):
                return True
            sleep(delay_time)
        return False

    def get_code_repo_source(self, code_name, retry_num=60, delay_time=5):
        '''
        :return: image code info
        '''
        # 等待controller同步代码仓库
        while retry_num > 0:
            code_list = self.list_code_repo()
            number = self.get_value(code_list.json(), 'listMeta.totalItems')
            if number > 0:
                code_repos = code_list.json()["coderepositories"]
                if len([repo for repo in code_repos if self.get_value(repo, "spec.repository.name") == code_name]) != 0:
                    break
            retry_num = retry_num - 1
            sleep(delay_time)
        assert retry_num != 0, "同步代码仓库失败，请手工验证"
        code_list = self.list_code_repo()
        code_repos = code_list.json()["coderepositories"]
        fullname = secret_name = secret_ns = ""
        for code_repo in code_repos:
            if self.get_value(code_repo, "spec.repository.name") == code_name:
                fullname = self.get_value(code_repo, "metadata.name")
                secret_name = self.get_value(code_repo, "metadata.annotations.secretName")
                secret_ns = self.get_value(code_repo, "metadata.annotations.secretNamespace")
                break

        repo_info = {
            "codeRepository": {
                "name": fullname,
                "ref": "master|PR-.*|MR-.*"
            },
            "secret": {
                "name": secret_name,
                "namespace": secret_ns
            },
            "soureceType": "GIT"
        }
        return repo_info
