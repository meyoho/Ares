import pytest
import json
from common.settings import RERUN_TIMES, PROJECT_NAME
from new_case.devops.devops_cicd.cicd import CicdApi
from common import settings
from common.log import logger
from new_case.devops.devops_cicd.conftest import app_name, bind_info, pipeline_config, secret_info
from new_case.containerplatform.application.application import Application
from new_case.containerplatform.deployment.deployment import Deployment
from time import sleep
import copy


@pytest.mark.jenkins
@pytest.mark.devopsApiServer
@pytest.mark.devops_api
@pytest.mark.prepare
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestPipelineSuite(object):
    def setup_class(self):
        self.config_client = CicdApi('pipelineconfig')
        self.app_client = Application()
        self.deploy_client = Deployment()
        self.pipeline = CicdApi('pipeline')
        self.image_repo_client = CicdApi('imagerepository')
        self.code_repo_client = CicdApi('coderepository')
        self.config_name = ''
        code_info = self.code_repo_client.get_code_repo_info(settings.GITLAB_REPO)
        self.codeRepository = {"url": "", "credentialId": "", "kind": "select", "bindingRepositoryName": "",
                               "sourceType": "GIT"}
        self.codeRepository.update(code_info)
        self.CodeQualityBinding = {"namespace": self.config_client.project_name, "name": bind_info[2]["bind_name"]}
        self.image_info = self.image_repo_client.get_image_repo_info(settings.HARBOR_REPO)
        self.imageRepository = {"type": "select", "repositoryPath": "", "tag": "e2etest", "secretName": "",
                                "secretNamespace": "", "credentialId": ""}
        self.imageRepository.update(self.image_info)
        self.values = {"PlatformCodeRepository": json.dumps(self.codeRepository), "Branch": "master",
                       "RelativeDirectory": "src",
                       "buildCommand": "cd go;go build", "UseSonarQube": "true",
                       "CodeQualityBinding": json.dumps(self.CodeQualityBinding),
                       "EnableBranchAnalysis": "false",
                       "AnalysisParameters": "sonar.sources=./go\nsonar.sourceEncoding=UTF-8\n",
                       "FailedIfNotPassQualityGate": "false", "imageRepository": json.dumps(self.imageRepository),
                       "context": "./go", "buildArguments": "", "dockerfile": "Dockerfile", "retry": "3",
                       "UseNotification": "false",
                       "PlatformNotification": "\"\""}
        self.region_name = settings.REGION_NAME

    def teardown_method(self):
        if settings.CASE_TYPE not in ("prepare", "upgrade"):
            self.config_client.delete_pipeline_config(self.config_name)

    def teardown_class(self):
        if settings.CASE_TYPE not in ("prepare", "upgrade"):
            self.app_client.delete_app(settings.K8S_NAMESPACE, "app")
            self.app_client.delete_app(settings.K8S_NAMESPACE, pipeline_config[11]['name'])
            self.deploy_client.delete_deplyment(settings.K8S_NAMESPACE, "test-deployment", region_name=self.region_name)

    def 测试创建golang构建配置代码仓库input方式(self):
        logger.info('测试创建golang构建配置代码仓库input-git方式')
        self.config_name = pipeline_config[0]['name']
        self.config_client.delete_pipeline_config(self.config_name)

        code_repo = {
            "url": "{}/{}/{}.git".format(settings.GITLAB_URL, settings.GITLAB_USERNAME, settings.GITLAB_REPO),
            "credentialId": "{}-{}".format(settings.PROJECT_NAME, secret_info[3]['secret_name']),
            "kind": "input",
            "sourceType": "GIT"}
        self.values['PlatformCodeRepository'] = json.dumps(code_repo)
        values = {
            "pipeline_name": self.config_name,
            "display_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": bind_info[0]["bind_name"],
            "pipeline_template_name": "GoLangBuilder",
            "values": self.values
        }
        body_data = self.config_client.generate_template_data(data=values)
        create_result = self.config_client.create_pipeline_config(body_data)
        assert create_result.status_code == 200, create_result.text
        expect_data = {
            "pipeline_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": bind_info[0]["bind_name"],
            "runPolicy": "Serial"
        }
        value = self.config_client.generate_jinja_data(
            'verify_data/devops_pipeline_config/create_pipeline_config.jinja2',
            data=expect_data)
        assert self.config_client.is_sub_dict(value, create_result.json()), \
            "创建流水线失败，期望数据:{},实际数据:{}".format(value, create_result.json())
        trigger = self.config_client.trigger_pipeline_config(self.config_name)
        assert trigger.status_code == 200, "执行流水线失败{}".format(trigger.text)
        execution_id = self.config_client.get_value(trigger.json(), 'metadata.name')
        status = self.pipeline.get_status(self.pipeline.detail_pipeline_url(execution_id), 'status.phase',
                                          'Running||Complete')
        assert status, "流水线{}执行失败, 执行记录{}".format(self.config_name, execution_id)
        delete = self.pipeline.delete_pipeline(execution_id)
        assert delete.status_code == 200, delete.text

    def 测试创建执行golang多个构建命令(self):
        logger.info('测试创建golang-构建多个命令')
        self.config_name = pipeline_config[1]['name']
        self.config_client.delete_pipeline_config(self.config_name)
        self.codeRepository = {
            "url": "{}/{}/{}.git".format(settings.GITLAB_URL, settings.GITLAB_USERNAME, settings.GITLAB_REPO),
            "credentialId": "{}-{}".format(settings.PROJECT_NAME,
                                           secret_info[3]['secret_name']),
            "kind": "input",
            "sourceType": "GIT"}
        self.values['PlatformCodeRepository'] = json.dumps(self.codeRepository)
        self.values['buildCommand'] = "cd go;go build;pwd"
        values = {
            "pipeline_name": self.config_name,
            "display_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": bind_info[0]["bind_name"],
            "pipeline_template_name": "GoLangBuilder",
            "values": self.values
        }
        body_data = self.config_client.generate_template_data(data=values)
        create_result = self.config_client.create_pipeline_config(body_data)
        assert create_result.status_code == 200, create_result.text
        trigger = self.config_client.trigger_pipeline_config(self.config_name)
        assert trigger.status_code == 200, "执行流水线失败{}".format(trigger.text)
        execution_id = self.config_client.get_value(trigger.json(), 'metadata.name')
        status = self.pipeline.get_status(self.pipeline.detail_pipeline_url(execution_id), 'status.phase',
                                          'Running||Complete')
        assert status, "流水线执行失败{} 执行记录{}".format(self.config_name, execution_id)
        delete = self.pipeline.delete_pipeline(execution_id)
        assert delete.status_code == 200, delete.text

    def 测试创建执行golang生成多个tag镜像(self):
        logger.info('测试创建golang-构建多个tag镜像')
        self.config_name = pipeline_config[2]['name']
        self.config_client.delete_pipeline_config(self.config_name)

        self.imageRepository.update({'tag': 'latest1,latest2'})
        self.values['PlatformCodeRepository'] = json.dumps(self.codeRepository)
        self.values['imageRepository'] = json.dumps(self.imageRepository)

        value = {
            "pipeline_name": self.config_name,
            "display_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": bind_info[0]["bind_name"],
            "pipeline_template_name": "GoLangBuilder",
            "values": self.values
        }
        body_data = self.config_client.generate_template_data(data=value)
        create_result = self.config_client.create_pipeline_config(body_data)
        assert create_result.status_code == 200, create_result.text
        trigger = self.config_client.trigger_pipeline_config(self.config_name)
        assert trigger.status_code == 200, "执行流水线失败{}".format(trigger.text)
        execution_id = self.config_client.get_value(trigger.json(), 'metadata.name')
        status = self.pipeline.get_status(self.pipeline.detail_pipeline_url(execution_id), 'status.phase',
                                          'Running||Complete')
        assert status, "流水线执行失败{} 执行记录{}".format(self.config_name, execution_id)
        delete = self.pipeline.delete_pipeline(execution_id)
        assert delete.status_code == 200, delete.text

    def 测试创建执行golang镜像仓库新建(self):
        logger.info('测试创建golang-构建新建镜像仓库')
        self.config_name = pipeline_config[3]['name']
        self.config_client.delete_pipeline_config(self.config_name)
        repo_path = self.image_info['repositoryPath'] + "create"
        self.imageRepository.update(
            {'type': 'create', "repositoryPath": repo_path,
             'tag': 'latest1,latest2'})
        self.values['imageRepository'] = json.dumps(self.imageRepository)

        value = {
            "pipeline_name": self.config_name,
            "display_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": bind_info[0]["bind_name"],
            "pipeline_template_name": "GoLangBuilder",
            "values": self.values
        }
        body_data = self.config_client.generate_template_data(data=value)
        create_result = self.config_client.create_pipeline_config(body_data)
        assert create_result.status_code == 200, create_result.text
        trigger = self.config_client.trigger_pipeline_config(self.config_name)
        assert trigger.status_code == 200, "执行流水线失败{}".format(trigger.text)
        execution_id = self.config_client.get_value(trigger.json(), 'metadata.name')
        status = self.pipeline.get_status(self.pipeline.detail_pipeline_url(execution_id), 'status.phase',
                                          'Running||Complete')
        assert status, "流水线执行失败{} 执行记录{}".format(self.config_name, execution_id)
        delete = self.pipeline.delete_pipeline(execution_id)
        assert delete.status_code == 200, delete.text

    def 测试创建执行golang镜像仓库输入(self):
        logger.info('测试创建执行golang镜像仓库输入')
        self.config_name = pipeline_config[4]['name']
        self.config_client.delete_pipeline_config(self.config_name)
        repo_path = self.image_info['repositoryPath'] + "create2"
        self.imageRepository.update(
            {'type': 'input', "repositoryPath": repo_path,
             'tag': 'latest1,latest2'})
        self.values['imageRepository'] = json.dumps(self.imageRepository)
        value = {
            "pipeline_name": self.config_name,
            "display_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": bind_info[0]["bind_name"],
            "pipeline_template_name": "GoLangBuilder",
            "values": self.values
        }
        body_data = self.config_client.generate_template_data(data=value)
        create_result = self.config_client.create_pipeline_config(body_data)
        assert create_result.status_code == 200, create_result.text
        trigger = self.config_client.trigger_pipeline_config(self.config_name)
        assert trigger.status_code == 200, "执行流水线失败{}".format(trigger.text)
        execution_id = self.config_client.get_value(trigger.json(), 'metadata.name')
        status = self.pipeline.get_status(self.pipeline.detail_pipeline_url(execution_id), 'status.phase',
                                          'Running||Complete')
        assert status, "流水线执行失败{} 执行记录{}".format(self.config_name, execution_id)
        delete = self.pipeline.delete_pipeline(execution_id)
        assert delete.status_code == 200, delete.text

    def 测试创建执行golang构建代码触发(self):
        logger.info('测试创建golang-构建代码触发')
        self.config_name = pipeline_config[5]['name']
        self.config_client.delete_pipeline_config(self.config_name)
        image_repo = self.image_repo_client.get_image_repo_info(settings.HARBOR_REPO)
        self.imageRepository.update(image_repo)
        self.imageRepository.update({'tag': 'latest1,latest2'})
        self.values['imageRepository'] = json.dumps(self.imageRepository)
        value = {
            "pipeline_name": self.config_name,
            "display_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": bind_info[0]["bind_name"],
            "pipeline_template_name": "GoLangBuilder",
            "values": self.values,
            "codeChange": "true",
            "periodicCheck": "H/2 * * * *"
        }
        body_data = self.config_client.generate_template_data(data=value)
        create_result = self.config_client.create_pipeline_config(body_data)
        assert create_result.status_code == 200, create_result.text
        trigger = self.config_client.trigger_pipeline_config(self.config_name)
        assert trigger.status_code == 200, "执行流水线失败{}".format(trigger.text)
        execution_id = self.config_client.get_value(trigger.json(), 'metadata.name')
        status = self.pipeline.get_status(self.pipeline.detail_pipeline_url(execution_id), 'status.phase',
                                          'Running||Complete')
        assert status, "流水线执行失败{} 执行记录{}".format(self.config_name, execution_id)
        delete = self.pipeline.delete_pipeline(execution_id)
        assert delete.status_code == 200, delete.text

    def 测试创建执行golang构建添加定时规则(self):
        logger.info('测试创建golang-构建添加定时规则')
        self.config_name = pipeline_config[6]['name']
        self.config_client.delete_pipeline_config(self.config_name)
        value = {
            "pipeline_name": self.config_name,
            "display_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": bind_info[0]["bind_name"],
            "pipeline_template_name": "GoLangBuilder",
            "values": self.values,
            "cron": "true",
            "rule": "H/2 * * * *"
        }
        body_data = self.config_client.generate_template_data(data=value)
        create_result = self.config_client.create_pipeline_config(body_data)
        assert create_result.status_code == 200, create_result.text
        trigger = self.config_client.trigger_pipeline_config(self.config_name)
        assert trigger.status_code == 200, "执行流水线失败{}".format(trigger.text)
        execution_id = self.config_client.get_value(trigger.json(), 'metadata.name')
        status = self.pipeline.get_status(self.pipeline.detail_pipeline_url(execution_id), 'status.phase',
                                          'Running||Complete')
        assert status, "流水线执行失败{} 执行记录{}".format(self.config_name, execution_id)
        delete = self.pipeline.delete_pipeline(execution_id)
        assert delete.status_code == 200, delete.text

    def 测试创建执行同步镜像(self):
        logger.info('测试创建执行同步镜像')
        self.config_name = pipeline_config[7]['name']
        self.config_client.delete_pipeline_config(self.config_name)
        repo_path = self.image_info['repositoryPath'] + "sync"
        self.values_syc = {"sourceCredentialsId": self.image_info['credentialId'],
                           "sourceImageRepository": self.image_info['repositoryPath'],
                           "sourceImageTag": "latest",
                           "targetCredentialsId": self.image_info['credentialId'],
                           "targetImageRepository": repo_path,
                           "targetImageTag": "${imageTag}",
                           "UseNotification": "false",
                           "PlatformNotification": "\"\""
                           }

        value = {
            "pipeline_name": self.config_name,
            "display_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": bind_info[0]["bind_name"],
            "pipeline_template_name": "alaudaSyncImage",
            "values": self.values_syc
        }
        body_data = self.config_client.generate_template_data(data=value)
        create_result = self.config_client.create_pipeline_config(body_data)
        assert create_result.status_code == 200, create_result.text
        image_data = {"params": [{"name": "imageTag", "type": "StringParameterDefinition", "value": "latest1"}]}
        trigger = self.config_client.trigger_pipeline_config(self.config_name, data=image_data)
        assert trigger.status_code == 200, "执行流水线失败{}".format(trigger.text)
        execution_id = self.config_client.get_value(trigger.json(), 'metadata.name')
        completed = self.pipeline.get_status(self.pipeline.detail_pipeline_url(execution_id), 'status.phase',
                                             'Complete', expect_cnt=60)
        assert completed, "流水线执行失败{} 执行记录{}".format(self.config_name, execution_id)
        detail = self.pipeline.detail_pipeline(execution_id)
        success = self.pipeline.get_value(detail.json(), 'status.jenkins.result')
        assert success == "SUCCESS", "流水线执行失败{} 执行记录{}".format(self.config_name, execution_id)

    def 测试创建定时触发同步镜像(self):
        logger.info('测试创建定时触发同步镜像')
        self.config_name = pipeline_config[15]['name']
        self.config_client.delete_pipeline_config(self.config_name)
        repo_path = self.image_info['repositoryPath'] + "sync"
        self.values_syc = {"sourceCredentialsId": self.image_info['credentialId'],
                           "sourceImageRepository": self.image_info['repositoryPath'],
                           "sourceImageTag": "${imageTag}",
                           "targetCredentialsId": self.image_info['credentialId'],
                           "targetImageRepository": repo_path,
                           "targetImageTag": "${imageTag}",
                           "UseNotification": "false",
                           "PlatformNotification": "\"\""
                           }

        value = {
            "pipeline_name": self.config_name,
            "display_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": bind_info[0]["bind_name"],
            "pipeline_template_name": "alaudaSyncImage",
            "values": self.values_syc,
            "cron": "true",
            "rule": "*/1 * * * *"
        }
        body_data = self.config_client.generate_template_data(data=value)
        create_result = self.config_client.create_pipeline_config(body_data)
        assert create_result.status_code == 200, create_result.text
        sleep(70)
        ret = self.config_client.detail_pipeline_config(self.config_name)
        assert ret.status_code == 200, "获取{}流水线详情失败".format(self.config_name)
        pipelines = self.config_client.get_value(ret.json(), 'pipelines')
        logger.info("流水线执行记录{}".format(pipelines))
        assert pipelines != [], "定时触发没有生效，没有流水线执行记录{}".format(pipelines)

    @pytest.mark.erebus
    def 测试创建执行golang构建并更新应用(self):
        logger.info('测试创建golang构建并且更新应用')
        self.config_name = pipeline_config[8]['name']
        self.config_client.delete_pipeline_config(self.config_name)
        application = {"clusterName": settings.REGION_NAME, "namespace": settings.K8S_NAMESPACE,
                       "createApp": "false",
                       "serviceName": "{}:deployment:{}".format(app_name, app_name),
                       "containerName": app_name, "rollback": "false", "command": "", "args": "", "timeout": "300"}
        self.codeRepository = {
            "url": "{}/{}/{}.git".format(settings.GITLAB_URL, settings.GITLAB_USERNAME, settings.GITLAB_REPO),
            "credentialId": "{}-{}".format(settings.PROJECT_NAME, secret_info[3]['secret_name']),
            "kind": "input",
            "sourceType": "GIT"}
        self.values['PlatformCodeRepository'] = json.dumps(self.codeRepository)
        self.values.update(application)
        values = {
            "pipeline_name": self.config_name,
            "display_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": bind_info[0]["bind_name"],
            "pipeline_template_name": "GoLangAndDeployService",
            "values": self.values
        }
        body_data = self.config_client.generate_template_data(data=values)
        create_result = self.config_client.create_pipeline_config(body_data)
        assert create_result.status_code == 200, create_result.text
        trigger = self.config_client.trigger_pipeline_config(self.config_name)
        assert trigger.status_code == 200, "执行流水线失败{}".format(trigger.text)
        execution_id = self.config_client.get_value(trigger.json(), 'metadata.name')
        completed = self.pipeline.get_status(self.pipeline.detail_pipeline_url(execution_id), 'status.phase',
                                             'Complete')
        assert completed, "流水线执行失败{} 执行记录{}".format(self.config_name, execution_id)
        detail = self.pipeline.detail_pipeline(execution_id)
        success = self.pipeline.get_value(detail.json(), 'status.jenkins.result')
        assert success == "SUCCESS", "流水线执行失败{} 执行记录{}".format(self.config_name, execution_id)

    def 测试创建执行脚本输入方式(self):
        self.config_name = pipeline_config[9]['name']
        self.config_client.delete_pipeline_config(self.config_name)
        logger.info('测试创建执行脚本输入方式')
        values = {
            "pipeline_name": self.config_name,
            "display_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": bind_info[0]["bind_name"],
            "jenkinsfile": "echo \"helloworld\" "
        }
        body_data = self.config_client.generate_script_data(data=values)
        create_result = self.config_client.create_pipeline_config(body_data)
        assert create_result.status_code == 200, create_result.text
        trigger = self.config_client.trigger_pipeline_config(self.config_name)
        assert trigger.status_code == 200, "执行流水线失败{}".format(trigger.text)
        execution_id = self.config_client.get_value(trigger.json(), 'metadata.name')
        completed = self.pipeline.get_status(self.pipeline.detail_pipeline_url(execution_id), 'status.phase',
                                             'Complete')
        assert completed, "流水线执行失败{} 执行记录{}".format(self.config_name, execution_id)
        detail = self.pipeline.detail_pipeline(execution_id)
        success = self.pipeline.get_value(detail.json(), 'status.jenkins.result')
        assert success == "SUCCESS", "流水线执行失败{} 执行记录{}".format(self.config_name, execution_id)

    def 测试创建执行go构建部署应用模版deployment(self):
        # 构建的生成的tag 用${GIT_COOMIT}
        # 1、打开创建应用开关 2、选择集群、命名空间 3、填写容器名称和路径（该路径下有一个deployment） 4、执行成功  5验证应用创建成功-应用名称是流水线名称
        logger.info("测试创建执行go构建部署应用模板deploy")
        self.config_name = pipeline_config[11]['name']
        self.config_client.delete_pipeline_config(self.config_name)
        application = {"clusterName": settings.REGION_NAME, "namespace": settings.K8S_NAMESPACE,
                       "createApp": "true",
                       "command": "", "args": "", "timeout": "300",
                       "deployConfigFolder": "go/dep", "container": "ares-test"}
        self.values.update(application)
        values = {
            "pipeline_name": self.config_name,
            "display_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": bind_info[0]["bind_name"],
            "pipeline_template_name": "GoLangAndDeployService",
            "values": self.values,
            "jenkinsfile": "",
            "jenkinsfilePath": ""
        }
        body_data = self.config_client.generate_template_data(data=values)
        create_result = self.config_client.create_pipeline_config(body_data)
        assert create_result.status_code == 200, create_result.text
        sleep(5)
        trigger = self.config_client.trigger_pipeline_config(self.config_name)
        assert trigger.status_code == 200, "执行流水线失败{}".format(trigger.text)
        execution_id = self.config_client.get_value(trigger.json(), 'metadata.name')
        completed = self.pipeline.get_status(self.pipeline.detail_pipeline_url(execution_id), 'status.phase',
                                             'Complete')
        assert completed, "流水线执行失败{} 执行记录{}".format(self.config_name, execution_id)
        detail = self.pipeline.detail_pipeline(execution_id)
        success = self.pipeline.get_value(detail.json(), 'status.jenkins.result')
        assert success == "SUCCESS", "流水线执行失败{} 执行记录{}".format(self.config_name, execution_id)
        ret = self.app_client.get_app_status(settings.K8S_NAMESPACE, self.config_name, 'Running')
        assert ret, '创建应用后，应用状态不是运行中'
        app_detail = self.app_client.detail_app(settings.K8S_NAMESPACE, self.config_name)
        assert app_detail.status_code == 200, "获取流水线创建的应用失败:{}".format(app_detail.text)
        verify_data = {
            "namespace": settings.K8S_NAMESPACE,
            "app_name": self.config_name,
            "project_name": settings.PROJECT_NAME,
            "image": settings.HARBOR_REPO,
            "deployment_name": self.app_client.get_value(app_detail.json(), "1.metadata.name")
        }
        values = self.app_client.generate_jinja_data("verify_data/application/detail_response.jinja2", verify_data)
        assert self.app_client.is_sub_list(values, app_detail.json()), \
            "获取应用详情比对数据失败，返回数据{}，期望数据{}".format(app_detail.json(), values)
        # 防止teardown吧流水线删除
        self.config_name = ""

    def 测试再次执行go构建部署应用模版deployment(self):
        # 应用应该被更新 。需验证pod是否重启
        self.config_name = pipeline_config[11]['name']
        pod_detail = self.app_client.get_app_pod(settings.K8S_NAMESPACE, self.config_name)
        assert pod_detail.status_code == 200, "获取流水线创建的应用失败:{}".format(pod_detail.text)
        pod_name1 = self.app_client.get_value(pod_detail.json(), "items.0.metadata.name")
        trigger = self.config_client.trigger_pipeline_config(self.config_name)
        assert trigger.status_code == 200, "执行流水线失败{}".format(trigger.text)
        execution_id = self.config_client.get_value(trigger.json(), 'metadata.name')
        completed = self.pipeline.get_status(self.pipeline.detail_pipeline_url(execution_id), 'status.phase',
                                             'Complete', expect_cnt=60)
        assert completed, "流水线执行失败{} 执行记录{}".format(self.config_name, execution_id)
        ret = self.app_client.get_app_status(settings.K8S_NAMESPACE, self.config_name, 'Running')
        assert ret, '创建应用后，应用状态不是运行中'
        pods = self.app_client.get_app_pod(settings.K8S_NAMESPACE, self.config_name)
        assert pods.status_code == 200, "获取流水线创建的应用失败:{}".format(pods.text)
        pod_name2 = self.app_client.get_value(pods.json(), "items.0.metadata.name")
        pod_name3 = self.app_client.get_value(pods.json(), "items.-1.metadata.name")
        assert not (pod_name1 == pod_name2 and pod_name1 == pod_name3), "重新执行流水线更新应用后pod没有变化,原来是{},更新后是{}".format(
            pod_name1, pod_name2)
        self.app_client.delete_app(settings.K8S_NAMESPACE, self.config_name)

    def 测试创建执行go构建部署应用模版applcation(self):
        # 1、打开创建应用开关 2、选择集群、命名空间 3、填写容器名称和路径（该路径下有一个application，deployment） 4、执行成功  5验证应用创建成功-应用名称是代码仓库里的application
        logger.info("测试创建执行go构建部署应用模板application")
        self.config_name = pipeline_config[12]['name']
        self.config_client.delete_pipeline_config(self.config_name)
        application = {"clusterName": settings.REGION_NAME, "namespace": settings.K8S_NAMESPACE,
                       "createApp": "true",
                       "command": "", "args": "", "timeout": "300",
                       "deployConfigFolder": "go/app", "container": "ares-test"}
        self.values.update(application)
        values = {
            "pipeline_name": self.config_name,
            "display_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": bind_info[0]["bind_name"],
            "pipeline_template_name": "GoLangAndDeployService",
            "values": self.values,
            "jenkinsfile": "",
            "jenkinsfilePath": ""
        }
        body_data = self.config_client.generate_template_data(data=values)
        create_result = self.config_client.create_pipeline_config(body_data)
        assert create_result.status_code == 200, create_result.text
        sleep(5)
        trigger = self.config_client.trigger_pipeline_config(self.config_name)
        assert trigger.status_code == 200, "执行流水线失败{}".format(trigger.text)
        execution_id = self.config_client.get_value(trigger.json(), 'metadata.name')
        completed = self.pipeline.get_status(self.pipeline.detail_pipeline_url(execution_id), 'status.phase',
                                             'Complete')
        assert completed, "流水线执行失败{} 执行记录{}".format(self.config_name, execution_id)
        detail = self.pipeline.detail_pipeline(execution_id)
        success = self.pipeline.get_value(detail.json(), 'status.jenkins.result')
        assert success == "SUCCESS", "流水线执行失败{} 执行记录{}".format(self.config_name, execution_id)
        # 这里应用名字是在代码里yaml写死的
        ret = self.app_client.get_app_status(settings.K8S_NAMESPACE, "app", 'Running')
        assert ret, '创建应用后，应用状态不是运行中'
        app_detail = self.app_client.detail_app(settings.K8S_NAMESPACE, "app")
        assert app_detail.status_code == 200, "获取流水线创建的应用失败:{}".format(app_detail.text)
        verify_data = {
            "namespace": settings.K8S_NAMESPACE,
            "app_name": "app",
            "project_name": settings.PROJECT_NAME,
            "image": settings.HARBOR_REPO,
            "deployment_name": self.app_client.get_value(app_detail.json(), "1.metadata.name")
        }
        values = self.app_client.generate_jinja_data("verify_data/application/detail_response.jinja2", verify_data)
        assert self.app_client.is_sub_list(values, app_detail.json()), \
            "获取应用详情比对数据失败，返回数据{}，期望数据{}".format(app_detail.json(), values)
        self.app_client.delete_app(settings.K8S_NAMESPACE, "app")

    @pytest.mark.skip(reason="not ready")
    def 测试自动触发(self):
        # 提交代码 查看流水线是否自动触发
        assert True

    @pytest.mark.skip(reason="maven构建耗时太长，先不跑")
    def 测试创建mvn构建分发仓库且单元测试全部通过(self, create_nexus):
        # 打开测试报告，验证测试报告
        logger.info("测试创建执行mvn构建分发仓库和单元测试")
        self.config_name = pipeline_config[13]['name']
        self.config_client.delete_pipeline_config(self.config_name)
        self.values['buildCommand'] = "cd success;mvn clean deploy"
        self.values["showTestResult"] = "true"
        self.values["RelativeDirectory"] = "."
        self.values["testResultLocation"] = "success/target/surefire-reports/*.xml"
        self.values["UseSonarQube"] = "false"
        values = copy.deepcopy(self.values)

        mvn_data = {
            "pom": "./success/pom.xml",
            "dependArtifactRegistry": "[]",
            "deployArtifactRegistry": json.dumps(
                {"name": create_nexus["bind_name"], "namespace": create_nexus["namespace"]})
        }
        values.update(mvn_data)
        data = {
            "pipeline_name": self.config_name,
            "display_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": bind_info[0]["bind_name"],
            "pipeline_template_name": "MavenBuildAndDeployService",
            "values": values,
            "jenkinsfile": "",
            "jenkinsfilePath": ""
        }
        body_data = self.config_client.generate_template_data(data=data)
        create_result = self.config_client.create_pipeline_config(body_data)
        assert create_result.status_code == 200, create_result.text
        sleep(5)
        trigger = self.config_client.trigger_pipeline_config(self.config_name)
        assert trigger.status_code == 200, "执行流水线失败{}".format(trigger.text)
        execution_id = self.config_client.get_value(trigger.json(), 'metadata.name')
        completed = self.pipeline.get_status(self.pipeline.detail_pipeline_url(execution_id), 'status.phase',
                                             'Complete', expect_cnt=240)
        assert completed, "流水线执行失败{} 执行记录{}".format(self.config_name, execution_id)
        detail = self.pipeline.detail_pipeline(execution_id)
        success = self.pipeline.get_value(detail.json(), 'status.jenkins.result')
        assert success == "SUCCESS", "流水线执行失败{} 执行记录{}".format(self.config_name, execution_id)
        ret_report = self.pipeline.get_report(execution_id)
        assert ret_report.status_code == 200, "获取测试报告失败:{}".format(ret_report.text)
        # 验证case 全部通过
        failed = self.pipeline.get_value(ret_report.json(), "Summary.Failed")
        exsit_failed = self.pipeline.get_value(ret_report.json(), "Summary.ExistingFailed")
        assert failed + exsit_failed == 0, "有失败的测试用例{}".format(ret_report.text)

    @pytest.mark.skip(reason="maven构建耗时太长，先不跑")
    def 测试创建mvn构建依赖仓库且单元测试失败(self, create_nexus):
        # 打开测试报告 执行mvn依赖分发仓库，检查日志有从nexus下载jar包
        logger.info("测试创建执行mvn构建依赖分发仓库")
        self.config_name = pipeline_config[14]['name']
        self.config_client.delete_pipeline_config(self.config_name)
        self.values['buildCommand'] = "cd failed;mvn clean package"
        self.values["showTestResult"] = "true"
        self.values["testResultLocation"] = "failed/target/surefire-reports/*.xml"
        values = copy.deepcopy(self.values)
        values.update({"dependArtifactRegistry": json.dumps(
            [{"name": create_nexus["bind_name"], "namespace": create_nexus["namespace"]}])})
        data = {
            "pipeline_name": self.config_name,
            "display_name": self.config_name,
            "project_name": PROJECT_NAME,
            "jenkins_bind_name": bind_info[0]["bind_name"],
            "pipeline_template_name": "MaveBuildService",
            "values": values,
            "jenkinsfile": "",
            "jenkinsfilePath": ""
        }
        body_data = self.config_client.generate_template_data(data=data)
        create_result = self.config_client.create_pipeline_config(body_data)
        assert create_result.status_code == 200, create_result.text
        sleep(5)
        trigger = self.config_client.trigger_pipeline_config(self.config_name)
        assert trigger.status_code == 200, "执行流水线失败{}".format(trigger.text)
        execution_id = self.config_client.get_value(trigger.json(), 'metadata.name')
        completed = self.pipeline.get_status(self.pipeline.detail_pipeline_url(execution_id), 'status.phase',
                                             'Running')
        assert completed, "流水线执行成功{} 执行记录{}".format(self.config_name, execution_id)
        check_log = self.pipeline.get_logs(self.pipeline.pipeline_log_url(execution_id),
                                           "Downloaded from {}: {}/repo".format(create_nexus["repo_name"],
                                                                                settings.NEXUS_URL), times=60)
        assert check_log, "期望从{}下载包，但是日志里没有体现".format(settings.NEXUS_URL)
