from common.base_request import Common
from common import settings
from common.log import logger
from time import sleep


class ToolChain(Common):
    def __init__(self):
        super(ToolChain, self).__init__()
        self.genetate_global_info()

    def get_common_chain_url(self, type, name=''):
        return name and "devops/api/v1/{type}/{name}".format(type=type, name=name) \
               or "devops/api/v1/{type}".format(type=type)

    def bind_project_url(self, type, project_name=settings.PROJECT_NAME, name=''):
        return name and "devops/api/v1/{type}/{project_name}/{name}".format(type=type, project_name=project_name,
                                                                            name=name) \
               or "devops/api/v1/{type}/{project_name}".format(type=type, project_name=project_name)

    def create_chain(self, type, file, data):
        path = self.get_common_chain_url(type)
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=path, json=data)

    def create_nexus_repo(self, nexus_name, file, data):
        url = "devops/api/v1/common/artifactregistrymanagers/{name}/sub/project".format(name=nexus_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=url, json=data)

    def get_chain_list(self, type=""):
        path = "devops/api/v1/toolchain"
        params = {"tool_type": type}
        return self.send(method='get', path=path, params=params)

    def get_chain_detail(self, type, name):
        path = self.get_common_chain_url(type, name)
        return self.send(method='get', path=path)

    def delete_chain(self, type, name):
        path = self.get_common_chain_url(type, name)
        return self.send(method='delete', path=path)

    def update_chain(self, type, name, file, data):
        path = self.get_common_chain_url(type, name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='put', path=path, json=data)

    def get_surpport_chain(self):
        path = "devops/api/v1/settings/devops"
        return self.send(method='get', path=path)

    def bind_chain(self, type, file, data, project_name=settings.PROJECT_NAME):
        path = self.bind_project_url(type, project_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=path, json=data)

    def get_chain_binding_list(self, project_name=settings.PROJECT_NAME):
        path = "devops/api/v1/toolchain/bindings/{}?tool_type=".format(project_name)
        return self.send(method='get', path=path)

    def get_chain_binding_detail(self, type, name, project_name=settings.PROJECT_NAME):
        path = self.bind_project_url(type, project_name=project_name, name=name)
        return self.send(method='get', path=path)

    def unbind_chain(self, type, name, project_name=settings.PROJECT_NAME):
        path = self.bind_project_url(type, project_name=project_name, name=name)
        return self.send(method='delete', path=path)

    def update_bind_chain(self, type, name, file, data, project_name=settings.PROJECT_NAME):
        path = self.bind_project_url(type, project_name=project_name, name=name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='put', path=path, json=data)

    def get_gitlab_remote_repo(self, name, type, project_name=settings.PROJECT_NAME):
        path = "devops/api/v1/{}/{}/{}/remote-repositories".format(type, project_name, name)
        return self.send(method='get', path=path)

    def get_jira_remote_repo(self, type, secret_name, namespace, tool_name):
        path = "devops/api/v1/{}/{}/remoteprojects".format(type, tool_name)
        params = {"secretName": secret_name, "namespace": namespace}
        return self.send(method='get', path=path, params=params)

    def get_registry_repo(self, name, type, project_name=settings.PROJECT_NAME):
        path = "devops/api/v1/{}/{}/{}/remote-repositories-project".format(type, project_name, name)
        return self.send(method='get', path=path)

    def get_tool_name(self):
        tool_list = self.get_chain_listdict()
        for key, value in tool_list.items():
            if value.split("/")[2] == settings.DOCKER_REGISTRY_URL.split("/")[2]:
                return {"docker-registry": key}
            elif value.split("/")[2] == settings.HARBOR_URL.split("/")[2]:
                return {"harbor": key}
            elif value.split("/")[2] == settings.JENKINS_URL.split("/")[2]:
                return {"jenkins": key}
            elif value.split("/")[2] == settings.GITLAB_URL.split("/")[2]:
                return {"gitlab": key}
            elif value.split("/")[2] == settings.SONAR_URL.split("/")[2]:
                return {"sonar": key}
            else:
                pass

    def get_chain_listdict(self):
        # get exist data
        ret = self.get_chain_list()
        assert ret.status_code == 200, "获取全部工具链列表失败:{}".format(ret.text)
        data_dict = dict()
        contents = ret.json().get("items", [])
        for content in contents:
            name = Common.get_value(content, "metadata.name")
            url = Common.get_value(content, "spec.http.host")
            data_dict.update({name: url})
        logger.info("tool chain list data is {}".format(data_dict))
        return data_dict

    def get_chain_verifydata(self, data):
        if data["api_type"] == "jenkinses":
            data.update({"tool_type": "continuousIntegration", "tool_kind": "Jenkins", "type": "Jenkins"})
        elif data["type"] == "Gitlab":
            data.update({"tool_type": "codeRepository", "tool_kind": "Gitlab"})
        elif data["type"] == "Harbor":
            data.update({"tool_type": "artifactRepository", "tool_kind": "Harbor"})
        elif data["type"] == "Sonarqube":
            data.update({"tool_type": "codeQualityTool", "tool_kind": "Sonarqube"})
        elif data["type"] == "Docker":
            data.update({"tool_type": "artifactRepository", "tool_kind": "Docker"})
        elif data["type"] == "nexus":
            data.update({"tool_type": "artifactRegistryManager"})
        elif data["type"] == "Gitee":
            data.update({"tool_type": "codeRepository", "tool_kind": "Gitee"})
        elif data["type"] == "Jira":
            data.update({"tool_type": "projectManagement", "tool_kind": "Jira"})
        return data

    def get_jiraproject_status(self, name, expect_cnt=5):
        cnt = 0
        flag = False
        message = ""
        while cnt < expect_cnt:
            ret = self.get_chain_binding_detail(type="projectmanagementbinding", name=name)
            assert ret.status_code == 200, "查看jira绑定详情页失败"
            conditions = self.get_value(ret.json(), "status.conditions")
            for i in range(len(conditions)):
                if conditions[i]["name"] == "ProjectsInfo":
                    message = conditions[i]["message"]
            if "name" in message:
                flag = True
                break
            else:
                sleep(2)
                cnt += 1
        return flag

    def get_secret_tool_name(self, tooltype_name):
        ret = self.get_chain_list()
        assert ret.status_code == 200, "获取全部工具链列表失败:{}".format(ret.text)
        contents = ret.json().get("items", [])
        for content in contents:
            tool_name = Common.get_value(content, "metadata.name")
            tooltypename = Common.get_value(content, "metadata#labels#{}/toolTypeName".format(settings.DEFAULT_LABEL), delimiter='#')
            if tooltypename == tooltype_name:
                return tool_name
        else:
            return None


create_data = {
    "docker_registry": {
        "tool_name": "{}-ares-docker-registry-tool".format(settings.RESOURCE_PREFIX),
        "host_url": "http://1.1.1.5",
        "accessUrl": "http://1.1.1.5",
        "type": "Docker",
        "kind": "ImageRegistry",
        "api_type": "imageregistry",
        "public_flag": False
    },
    "nexus": {
        "tool_name": "{}-ares-nexus-tool".format(settings.RESOURCE_PREFIX),
        "host_url": "http://1.1.1.6",
        "accessUrl": "http://1.1.1.6",
        "type": "nexus",
        "api_type": "artifactregistrymanagers",
        "secret_name": "",
        "secret_namespace": ""
    },
    "Gitee企业版": {
        "tool_name": "{}-ares-gitee-tool".format(settings.RESOURCE_PREFIX),
        "host_url": "http://1.1.1.7",
        "accessUrl": "http://1.1.1.7",
        "type": "Gitee",
        "kind": "Gitee",
        "api_type": "codereposervice",
        "public_flag": False
    },
    "JIRA": {
        "tool_name": "{}-ares-jira-tool".format(settings.RESOURCE_PREFIX),
        "host_url": "http://1.1.1.8",
        "accessUrl": "http://1.1.1.8",
        "type": "Jira",
        "kind": "ProjectManagement",
        "api_type": "projectmanagement",
        "public_flag": False
    }
}
