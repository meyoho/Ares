import pytest
import copy
from new_case.devops.tool_chain.tool_chain import ToolChain
from common import settings


@pytest.mark.devops_controller
@pytest.mark.devopsApiServer
@pytest.mark.tool
@pytest.mark.BAT
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestSuiteChain(object):
    data_list = [
        {
            "tool_name": "{}-ares-jenkins-tool".format(settings.RESOURCE_PREFIX),
            "host_url": "http://1.1.1.1",
            "accessUrl": "http://1.1.1.1",
            "api_type": "jenkinses"
        },
        {
            "tool_name": "{}-ares-gitlab-tool".format(settings.RESOURCE_PREFIX),
            "host_url": "http://1.1.1.2",
            "accessUrl": "http://1.1.1.2",
            "type": "Gitlab",
            "api_type": "codereposervice"
        },
        {
            "tool_name": "{}-ares-harbor-tool".format(settings.RESOURCE_PREFIX),
            "host_url": "http://1.1.1.3",
            "accessUrl": "http://1.1.1.3",
            "type": "Harbor",
            "kind": "ImageRegistry",
            "api_type": "imageregistry"
        },
        {
            "tool_name": "{}-ares-sonar-tool".format(settings.RESOURCE_PREFIX),
            "host_url": "http://1.1.1.4",
            "accessUrl": "http://1.1.1.4",
            "type": "Sonarqube",
            "kind": "CodeQualityTool",
            "api_type": "codequalitytool"
        }
    ]
    casename_list = ["jenkins工具链", "gitlab工具链", "harbor工具链", "sonarqube工具链"]
    update_url = "http://2.2.2.2"

    def setup_class(self):
        self.chain_tool = ToolChain()
        self.teardown_class(self)

    def teardown_class(self):
        if settings.CASE_TYPE not in ("prepare", "upgrade", "delete"):
            for data in self.data_list:
                self.chain_tool.delete_chain(data["api_type"], data["tool_name"])

    @pytest.mark.prepare
    @pytest.mark.parametrize("data", data_list, ids=casename_list)
    def 测试工具链创建(self, data, get_chain_list):
        create_data = copy.deepcopy(data)
        if create_data["host_url"] in get_chain_list.values():
            create_data["host_url"] = "https://1.1.1.1"
        # create manager_secret_basic_auth
        verify_data = self.chain_tool.get_chain_verifydata(create_data)
        api_type = create_data.pop("api_type")
        ret = self.chain_tool.create_chain(api_type, './test_data/tool_chain/create_chain.jinja2', data=create_data)

        assert ret.status_code in (200, 201), "创建{}类型工具链失败:{}".format(api_type, ret.text)

        value = self.chain_tool.generate_jinja_data("./verify_data/tool_chain/create_chain.jinja2", data=verify_data)
        assert self.chain_tool.is_sub_dict(value, ret.json()), \
            "创建{}工具链返回值和预期结果不一致，期望数据:{},实际数据:{}".format(data["api_type"], value, ret.json())

    @pytest.mark.upgrade
    @pytest.mark.parametrize("data", data_list, ids=casename_list)
    def 测试获取工具链列表(self, data):
        # get maanger_secret list
        ret = self.chain_tool.get_chain_list()
        assert ret.status_code == 200, "获取全部工具链列表失败:{}".format(ret.text)
        assert data["tool_name"] in ret.text, "工具链:{}，不在列表内".format(data["tool_name"])

    @pytest.mark.upgrade
    @pytest.mark.parametrize("data", data_list, ids=casename_list)
    def 测试更新工具链(self, data):
        update_data = copy.deepcopy(data)
        update_data.update({"host_url": self.update_url})
        api_type = update_data.pop("api_type")
        # update manager_secret_basic_auth
        ret = self.chain_tool.update_chain(api_type, update_data['tool_name'],
                                           './test_data/tool_chain/create_chain.jinja2', data=update_data)
        assert ret.status_code == 200, "更新{}类型工具链失败:{}".format(api_type, ret.text)

        verify_data = self.chain_tool.get_chain_verifydata(data)
        verify_data.update({"host_url": self.update_url})

        value = self.chain_tool.generate_jinja_data("./verify_data/tool_chain/create_chain.jinja2", data=verify_data)
        assert self.chain_tool.is_sub_dict(value, ret.json()), \
            "更新{}工具链返回值和预期结果不一致，期望数据:{},实际数据:{}".format(data["api_type"], value, ret.json())

    @pytest.mark.upgrade
    @pytest.mark.parametrize("data", data_list, ids=casename_list)
    def 测试获取工具链详情(self, data):
        ret = self.chain_tool.get_chain_detail(data["api_type"], data["tool_name"])
        assert ret.status_code == 200, "获取{}类型的工具链详情失败{}".format(data["api_type"], ret.text)
        assert self.update_url in ret.text, "获取{}类型的工具链详情失败:{}".format(data["api_type"], ret.text)

        verify_data = self.chain_tool.get_chain_verifydata(data)
        verify_data.update({"host_url": self.update_url})

        value = self.chain_tool.generate_jinja_data("./verify_data/tool_chain/create_chain.jinja2", data=verify_data)
        assert self.chain_tool.is_sub_dict(value, ret.json()), \
            "获取{}工具链详情返回值和预期结果不一致，期望数据:{},实际数据:{}".format(data["api_type"], value, ret.json())

    @pytest.mark.delete
    @pytest.mark.parametrize("data", data_list, ids=casename_list)
    def 测试删除工具链(self, data):
        ret = self.chain_tool.delete_chain(data["api_type"], data["tool_name"])
        assert ret.status_code == 200, "删除{}类型的工具链失败:{}".format(data["api_type"], ret.text)

        assert self.chain_tool.check_exists(self.chain_tool.get_common_chain_url(data["api_type"], data["tool_name"]),
                                            404), "删除失败"

    @pytest.mark.skip(reason="有功能开关，目前不好判断")
    def 测试获取目前支持的工具链类型(self):
        ret = self.chain_tool.get_surpport_chain()
        assert ret.status_code == 200, "获取目前支持的工具链类型失败:{}".format(ret.text)
        value = self.chain_tool.generate_jinja_data("./verify_data/tool_chain/tool_type.jinja2")
        assert self.chain_tool.is_sub_dict(value, ret.json()), \
            "获取目前支持的工具链类型返回值和预期结果不一致，期望数据:{},实际数据:{}".format(value, ret.json())
