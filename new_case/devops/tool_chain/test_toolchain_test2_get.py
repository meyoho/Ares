import pytest
from new_case.devops.tool_chain.tool_chain import ToolChain, create_data
from common import settings


@pytest.mark.devops_controller
@pytest.mark.tool
@pytest.mark.devopsApiServer
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestGetChain(object):
    def setup_class(self):
        self.toolchain = ToolChain()

    @pytest.mark.parametrize("key", create_data)
    def 测试获取工具链详情(self, key):
        data = create_data[key]
        too_name = data["tool_name"]
        ret = self.toolchain.get_chain_detail(data["api_type"], too_name)
        assert ret.status_code == 200, "获取{}工具链详情失败:{}".format(key, ret.text)
        host_url = self.toolchain.get_value(ret.json(), "spec.http.host")
        verify_url = data["host_url"]
        assert host_url == verify_url, \
            "详情页URL不一致，预期:{},实际{}".format(verify_url, host_url)
