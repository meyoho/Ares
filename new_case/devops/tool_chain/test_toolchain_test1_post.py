import pytest
from new_case.devops.tool_chain.tool_chain import ToolChain, create_data
from common import settings


@pytest.mark.devops_controller
@pytest.mark.tool
@pytest.mark.devopsApiServer
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestPostChain(object):
    def setup_class(self):
        self.toolchain = ToolChain()

    @pytest.mark.parametrize("key", create_data)
    def 测试工具链创建(self, key):
        data = create_data[key]

        # 删除同名已存在的工具链
        self.toolchain.delete_chain(data["api_type"], data["tool_name"])

        # 创建工具链
        ret = self.toolchain.create_chain(data["api_type"], './test_data/tool_chain/create_chain.jinja2', data)
        assert ret.status_code == 200, "创建{}工具链失败".format(key)

        data = self.toolchain.get_chain_verifydata(data)
        value = self.toolchain.generate_jinja_data("./verify_data/tool_chain/create_chain.jinja2", data=data)
        assert self.toolchain.is_sub_dict(value, ret.json()), \
            "创建{}工具链返回值和预期结果不一致，期望数据:{},实际数据:{}".format(key, value, ret.json())
