import pytest
from common import settings
from new_case.devops.tool_chain.tool_chain import ToolChain


@pytest.mark.devops_controller
@pytest.mark.tool
@pytest.mark.devopsApiServer
@pytest.mark.devops_api
@pytest.mark.skipif(settings.AUDIT_UNABLED, reason="do not have audit")
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestToolChainAuditL1Suite(object):
    data_list = [
        {
            "tool_name": "{}-ares-jenkins-tool".format(settings.RESOURCE_PREFIX),
            "type": "jenkinses"
        },
        {
            "tool_name": "{}-ares-gitlab-tool".format(settings.RESOURCE_PREFIX),
            "type": "codereposervices"
        },
        {
            "tool_name": "{}-ares-harbor-tool".format(settings.RESOURCE_PREFIX),
            "type": "imageregistries"
        },
        {
            "tool_name": "{}-ares-sonar-tool".format(settings.RESOURCE_PREFIX),
            "type": "codequalitytools"
        }
    ]
    casename_list = ["jenkins工具链", "gitlab工具链", "harbor工具链", "sonarqube工具链"]

    def setup_class(self):
        self.toolchain = ToolChain()

    @pytest.mark.parametrize("data", data_list, ids=casename_list)
    def 测试工具链创建审计(self, data):
        payload = {"user_name": settings.USERNAME, "operation_type": "create", "resource_type": data["type"],
                   "resource_name": ""}
        result = self.toolchain.search_audit(payload)
        payload.update({"namespace": "", "region_name": settings.GLOBAL_REGION_NAME, "resource_name": " ", "code": 201})
        values = self.toolchain.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.toolchain.is_sub_dict(values, result.json()), "审计数据不符合预期"

    @pytest.mark.parametrize("data", data_list, ids=casename_list)
    def 测试工具链更新审计(self, data):
        payload = {"user_name": settings.USERNAME, "operation_type": "update", "resource_type": data["type"],
                   "resource_name": data["tool_name"]}
        result = self.toolchain.search_audit(payload)
        payload.update({"namespace": "", "region_name": settings.GLOBAL_REGION_NAME})
        values = self.toolchain.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.toolchain.is_sub_dict(values, result.json()), "审计数据不符合预期"

    @pytest.mark.parametrize("data", data_list, ids=casename_list)
    def 测试工具链删除审计(self, data):
        payload = {"user_name": settings.USERNAME, "operation_type": "delete", "resource_type": data["type"],
                   "resource_name": data["tool_name"]}
        result = self.toolchain.search_audit(payload)
        payload.update({"namespace": "", "region_name": settings.GLOBAL_REGION_NAME})
        values = self.toolchain.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.toolchain.is_sub_dict(values, result.json()), "审计数据不符合预期"
