import pytest
from new_case.devops.tool_chain.tool_chain import ToolChain, create_data
from common import settings


@pytest.mark.devops_controller
@pytest.mark.tool
@pytest.mark.devopsApiServer
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestDeleteChain(object):
    def setup_class(self):
        self.toolchain = ToolChain()

    @pytest.mark.parametrize("key", create_data)
    def 测试删除工具链(self, key):
        data = create_data[key]
        ret = self.toolchain.delete_chain(data["api_type"], data["tool_name"])
        assert ret.status_code == 200, "删除{}工具链失败:{}".format(key, ret.text)
