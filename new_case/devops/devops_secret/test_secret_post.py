import pytest
import base64
from new_case.devops.devops_secret.secret import Secret
from new_case.devops.tool_chain.tool_chain import ToolChain
from common import settings


@pytest.mark.devopsApiServer
@pytest.mark.BAT
@pytest.mark.devops_secret
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class Testsecret(object):
    data_list = [
        {
            "secret_name": "{}-ares-jenkins-secret".format(settings.RESOURCE_PREFIX),
            "tool_type": "Jenkins",
            "tooltype_name": "jenkins",
        },
        {
            "secret_name": "{}-ares-sonar-secret".format(settings.RESOURCE_PREFIX),
            "tool_type": "Sonar",
            "tooltype_name": "sonar"
        },
        {
            "secret_name": "{}-ares-docker-registry-secret".format(settings.RESOURCE_PREFIX),
            "tool_type": "Docker",
            "tooltype_name": "docker-registry"
        },
        {
            "secret_name": "{}-ares-jira-secret".format(settings.RESOURCE_PREFIX),
            "tool_type": "Jira",
            "tooltype_name": "jira"
        },
        {
            "secret_name": "{}-ares-maven-secret".format(settings.RESOURCE_PREFIX),
            "tool_type": "Maven2",
            "tooltype_name": "maven2"
        },
        {
            "secret_name": "{}-ares-bitbucket-secret".format(settings.RESOURCE_PREFIX),
            "tool_type": "Bitbucket",
            "tooltype_name": "bitbucket"
        },
        {
            "secret_name": "{}-ares-gitea-secret".format(settings.RESOURCE_PREFIX),
            "tool_type": "Gitea",
            "tooltype_name": "gitea"
        },
        {
            "secret_name": "{}-ares-gitee-secret".format(settings.RESOURCE_PREFIX),
            "tool_type": "Gitee",
            "tooltype_name": "gitee"
        },
        {
            "secret_name": "{}-ares-gitee-enterprise-secret".format(settings.RESOURCE_PREFIX),
            "tool_type": "Gitee",
            "tooltype_name": "gitee-enterprise"
        },
        {
            "secret_name": "{}-ares-github-secret".format(settings.RESOURCE_PREFIX),
            "tool_type": "Github",
            "tooltype_name": "github"
        },
        {
            "secret_name": "{}-ares-gitlab-secret".format(settings.RESOURCE_PREFIX),
            "tool_type": "Gitlab",
            "tooltype_name": "gitlab"
        },
        {
            "secret_name": "{}-ares-gitlab-enterprise-secret".format(settings.RESOURCE_PREFIX),
            "tool_type": "Gitlab",
            "tooltype_name": "gitlab-enterprise"
        },
        {
            "secret_name": "{}-ares-gogs-secret".format(settings.RESOURCE_PREFIX),
            "tool_type": "Gogs",
            "tooltype_name": "gogs"
        },
        {
            "secret_name": "{}-ares-dockerhub-registry-secret".format(settings.RESOURCE_PREFIX),
            "tool_type": "DockerHub",
            "tooltype_name": "dockerhub-registry"
        },
        {
            "secret_name": "{}-ares-harbor-registry-secret".format(settings.RESOURCE_PREFIX),
            "tool_type": "Harbor",
            "tooltype_name": "harbor-registry"
        },
        {
            "secret_name": "{}-ares-nexus-secret".format(settings.RESOURCE_PREFIX),
            "tool_type": "nexus",
            "tooltype_name": "nexus"
        }
    ]
    casename_list = ["创建jenkins凭据", "创建sonar凭据", "创建docker-registry凭据", "创建jira凭据", "创建maven凭据",
                     "创建bitbuket凭据", "创建gitea凭据", "创建gitee凭据", "创建gitee-enterprisep凭据", "创建github凭据",
                     "创建gitlab凭据", "创建gitlab-enterpris凭据", "创建gogs凭据", "创建dockerhub-registry凭据",
                     "创建harbor凭据", "创建nexus凭据"]

    def setup_class(self):
        self.secret_tool = Secret()
        self.chain_tool = ToolChain()

        self.teardown_class(self)

    def teardown_class(self):
        if settings.CASE_TYPE not in ("prepare", "upgrade", "delete"):
            for data in self.data_list:
                if settings.DEFAULT_NS == "alauda-system":
                    scope = "global-credentials"
                else:
                    scope = "{}-global-credentials".format(settings.DEFAULT_NS)
                self.secret_tool.delete_secret(scope=scope, name=data["secret_name"])

    @pytest.mark.prepare
    @pytest.mark.parametrize("data", data_list, ids=casename_list)
    def 测试devops工具链类型凭据创建(self, data):
        tool_name = self.chain_tool.get_secret_tool_name(data["tooltype_name"])
        update_date = {
            "username": str(base64.b64encode("username".encode('utf-8')), 'utf8'),
            "password": str(base64.b64encode("password".encode('utf-8')), 'utf8'),
            "secret_type": "kubernetes.io/basic-auth",
            "tool_name": tool_name
        }
        data.update(update_date)
        ret = self.secret_tool.create_secret(scope="", file='./test_data/devops_secret/create_secret.jinja2', data=data)
        assert ret.status_code == 200, "创建{}凭据失败:{}".format(data['secret_name'], ret.text)
