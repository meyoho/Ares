import pytest
import base64
from new_case.devops.devops_secret.secret import Secret
from common import settings
from common.utils import dockerjson
import copy


@pytest.mark.devopsApiServer
@pytest.mark.BAT
@pytest.mark.devops_secret
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class Testsecret(object):
    data_list = [
        {
            "secret_name": "{}-ares-sshsecret".format(settings.RESOURCE_PREFIX),
            "secret_type": "kubernetes.io/ssh-auth",
            "ssh_privatevalue": str(base64.b64encode("value".encode('utf-8')), 'utf8'),
        },
        {
            "secret_name": "{}-ares-oauth".format(settings.RESOURCE_PREFIX),
            "secret_type": "devops.alauda.io/oauth2",
            "client_id": str(base64.b64encode("clientid".encode('utf-8')), 'utf8'),
            "client_key": str(base64.b64encode("clientkey".encode('utf-8')), 'utf8'),
        },
        {
            "secret_name": "{}-ares-basesecret".format(settings.RESOURCE_PREFIX),
            "secret_type": "kubernetes.io/basic-auth",
            "username": str(base64.b64encode("username".encode('utf-8')), 'utf8'),
            "password": str(base64.b64encode("password".encode('utf-8')), 'utf8')
        },
        {
            "secret_name": "{}-ares-dockersecret".format(settings.RESOURCE_PREFIX),
            "secret_type": "kubernetes.io/dockerconfigjson",
            "dockerconfigjson": dockerjson("index.alauda.cn", "alauda", "alauda", "hchan@alauda.io"),
        },
        {
            "secret_name": "{}-ares-project-sshsecret-scope".format(settings.RESOURCE_PREFIX),
            "secret_type": "kubernetes.io/ssh-auth",
            "ssh_privatevalue": str(base64.b64encode("value".encode('utf-8')), 'utf8'),
            "k8s_namespace": settings.PROJECT_NAME,
            "global_flag": '"false"'
        },
        {
            "secret_name": "{}-ares-project-oauth-scope".format(settings.RESOURCE_PREFIX),
            "secret_type": "devops.alauda.io/oauth2",
            "client_id": str(base64.b64encode("clientid".encode('utf-8')), 'utf8'),
            "client_key": str(base64.b64encode("clientkey".encode('utf-8')), 'utf8'),
            "k8s_namespace": settings.PROJECT_NAME,
            "global_flag": '"false"'
        },
        {
            "secret_name": "{}-ares-project-basesecret-scope".format(settings.RESOURCE_PREFIX),
            "secret_type": "kubernetes.io/basic-auth",
            "username": str(base64.b64encode("username".encode('utf-8')), 'utf8'),
            "password": str(base64.b64encode("password".encode('utf-8')), 'utf8'),
            "k8s_namespace": settings.PROJECT_NAME,
            "global_flag": '"false"'
        },
        {
            "secret_name": "{}-ares-project-dockersecret-scope".format(settings.RESOURCE_PREFIX),
            "secret_type": "kubernetes.io/dockerconfigjson",
            "dockerconfigjson": dockerjson("index.alauda.cn", "alauda", "alauda", "hchan@alauda.io"),
            "k8s_namespace": settings.PROJECT_NAME,
            "global_flag": '"false"'
        }
    ]
    casename_list = ["平台公共的ssh凭据", "平台公共的oAuth2凭据", "平台公共的用户名-密码凭据", "平台公共的镜像服务凭据",
                     "项目私有的ssh凭据", "项目私有的oAuth2凭据", "项目私有的用户名-密码凭据", "项目私有的镜像服务凭据"]

    def setup_class(self):
        self.secret_tool = Secret()

        self.teardown_class(self)

    def teardown_class(self):
        if settings.CASE_TYPE not in ("prepare", "upgrade", "delete"):
            for data in self.data_list:
                if "k8s_namespace" in data.keys():
                    scope = data["k8s_namespace"]
                elif settings.DEFAULT_NS == "alauda-system":
                    scope = "global-credentials"
                else:
                    scope = "{}-global-credentials".format(settings.DEFAULT_NS)
                self.secret_tool.delete_secret(scope=scope, name=data["secret_name"])

    @pytest.mark.prepare
    @pytest.mark.parametrize("data", data_list, ids=casename_list)
    def 测试devops凭据创建(self, data):
        scope = ""
        if "k8s_namespace" in data.keys():
            scope = data["k8s_namespace"]
        ret = self.secret_tool.create_secret(scope=scope, file='./test_data/devops_secret/create_secret.jinja2',
                                             data=data)

        assert ret.status_code == 200, "创建{}类型凭据失败:{}".format(data['secret_type'], ret.text)
        verify_data = copy.deepcopy(data)
        verify_data.update({"k8s_namespace": scope})
        value = self.secret_tool.generate_jinja_data(filename='./verify_data/devops_secret/create_secret.jinja2',
                                                     data=verify_data)
        assert self.secret_tool.is_sub_dict(value, ret.json()), "创建{}类型凭据比对数据失败,期望数据:{},实际数据{}".format(data['secret_type'], value, ret.text)

    @pytest.mark.upgrade
    def 测试devops平台级别凭据分页(self):
        # check pagination
        name_flag = self.secret_tool.resource_pagination("devops/api/v1/secret", query="secrets.0.objectMeta.name",
                                                         page_size_key="itemsPerPage", params={"sortBy": "a,name"})
        namespace_flag = self.secret_tool.resource_pagination("devops/api/v1/secret",
                                                              query="secrets.0.objectMeta.namespace",
                                                              page_size_key="itemsPerPage", params={"sortBy": "a,name"})
        assert name_flag or namespace_flag, "验证平台凭据列表分页出错，请手动验证"

    @pytest.mark.upgrade
    def 测试devops项目级别凭据分页(self):
        # check pagination
        name_flag = self.secret_tool.resource_pagination("devops/api/v1/secret/{}".format(settings.PROJECT_NAME),
                                                         query="secrets.0.objectMeta.name",
                                                         page_size_key="itemsPerPage", params={"sortBy": "a,name"})
        namespace_flag = self.secret_tool.resource_pagination("devops/api/v1/secret/{}".format(settings.PROJECT_NAME),
                                                              query="secrets.0.objectMeta.namespace",
                                                              page_size_key="itemsPerPage", params={"sortBy": "a,name"})
        assert name_flag or namespace_flag, "验证项目凭据列表分页出错，请手动验证"

    @pytest.mark.upgrade
    def 测试获取devops平台级别凭据列表(self):
        # get maanger_secret list
        ret = self.secret_tool.get_secret_list()
        assert ret.status_code == 200, "获取平台凭据列表失败:{}".format(ret.text)
        secret_names = self.secret_tool.get_value_list(ret.json(), "secrets||objectMeta.name")
        for data in self.data_list:
            assert data["secret_name"] in secret_names, "凭据:{}不在列表内".format(data["secret_name"])

    @pytest.mark.upgrade
    def 测试获取devops项目级别凭据列表(self):
        # get maanger_secret list
        ret = self.secret_tool.get_secret_list(scope=settings.PROJECT_NAME)
        assert ret.status_code == 200, "获取项目凭据列表失败:{}".format(ret.text)
        secret_names = self.secret_tool.get_value_list(ret.json(), "secrets||objectMeta.name")
        for data in self.data_list:
            if "k8s_namespace" in data.keys():
                assert data["secret_name"] in secret_names, "凭据:{}不在列表内".format(data["secret_name"])

    @pytest.mark.upgrade
    def 测试平台凭据搜索(self):
        name = self.data_list[0]['secret_name']
        ret = self.secret_tool.get_secret_list(params={"filterBy": "name,{}".format(name)})
        assert ret.status_code == 200, "平台凭据搜索失败:{}".format(ret.text)
        secrets = self.secret_tool.get_value(ret.json(), "secrets")
        assert len(secrets) > 0, "未搜索到对应的结果:{}".format(ret.text)
        for secret in secrets:
            ret_name = self.secret_tool.get_value(secret, "objectMeta.name")
            assert ret_name == name, "搜索到的结果是{},期望匹配的是:{}".format(ret_name, name)

    @pytest.mark.upgrade
    def 测试项目内凭据搜索(self):
        name = self.data_list[-1]['secret_name']
        scope = self.data_list[-1]['k8s_namespace']
        ret = self.secret_tool.get_secret_list(scope=scope, params={"filterBy": "name,{}".format(name)})
        assert ret.status_code == 200, "平台凭据搜索失败:{}".format(ret.text)
        secrets = self.secret_tool.get_value(ret.json(), "secrets")
        assert len(secrets) > 0, "未搜索到对应的结果:{}".format(ret.text)
        for secret in secrets:
            ret_name = self.secret_tool.get_value(secret, "objectMeta.name")
            assert ret_name == name, "搜索到的结果是{},期望匹配的是:{}".format(ret_name, name)

    @pytest.mark.upgrade
    @pytest.mark.parametrize("data", data_list, ids=casename_list)
    def 测试更新devops凭据(self, data):
        data.update({"display_name": "display_name"})
        if "k8s_namespace" in data.keys():
            scope = data["k8s_namespace"]
        elif settings.DEFAULT_NS == "alauda-system":
            scope = "global-credentials"
        else:
            scope = "{}-global-credentials".format(settings.DEFAULT_NS)
        # update manager_secret_basic_auth
        ret = self.secret_tool.update_secret(scope, data['secret_name'],
                                             './test_data/devops_secret/create_secret.jinja2', data=data)
        assert ret.status_code == 200, "更新{}类型凭据失败:{}".format(data["secret_type"], ret.text)
        verify_data = copy.deepcopy(data)
        verify_data.update({"k8s_namespace": scope})
        value = self.secret_tool.generate_jinja_data(filename='./verify_data/devops_secret/create_secret.jinja2',
                                                     data=verify_data)
        assert self.secret_tool.is_sub_dict(value, ret.json()), "创建{}类型凭据比对数据失败,期望数据:{},实际数据{}".format(data['secret_type'], value, ret.text)

    @pytest.mark.upgrade
    @pytest.mark.parametrize("data", data_list, ids=casename_list)
    def 测试获取devops凭据详情(self, data):
        if "k8s_namespace" in data.keys():
            scope = data["k8s_namespace"]
        elif settings.DEFAULT_NS == "alauda-system":
            scope = "global-credentials"
        else:
            scope = "{}-global-credentials".format(settings.DEFAULT_NS)
        ret = self.secret_tool.get_secret_detail(scope, data["secret_name"])
        assert ret.status_code == 200, "获取{}类型的凭据详情失败{}".format(data["secret_type"], ret.text)
        assert data["secret_name"] in ret.text and "display_name" in ret.text, "{}类型的凭据更新失败:{}".format(
            data["secret_type"], ret.text)

    @pytest.mark.delete
    @pytest.mark.parametrize("data", data_list, ids=casename_list)
    def 测试删除凭据(self, data):
        if "k8s_namespace" in data.keys():
            scope = data["k8s_namespace"]
        elif settings.DEFAULT_NS == "alauda-system":
            scope = "global-credentials"
        else:
            scope = "{}-global-credentials".format(settings.DEFAULT_NS)
        ret = self.secret_tool.delete_secret(scope, data["secret_name"])
        assert ret.status_code == 200, "删除{}类型的凭据失败:{}".format(data["secret_type"], ret.text)
        assert self.secret_tool.check_exists(
            self.secret_tool.get_common_secret_url(scope=scope, name=data["secret_name"]), 404), "删除失败"
