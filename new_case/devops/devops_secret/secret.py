from common.base_request import Common


class Secret(Common):
    def __init__(self):
        super(Secret, self).__init__()
        self.genetate_global_info()

    def get_common_secret_url(self, scope="", name=""):
        url = scope and "devops/api/v1/secret/{}".format(scope) or "devops/api/v1/secret"
        return name and "{}/{}".format(url, name) or url

    def create_secret(self, scope, file, data):
        path = self.get_common_secret_url(scope)
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=path, json=data, params={})

    def get_secret_list(self, scope="", params={}):
        path = self.get_common_secret_url(scope=scope)
        return self.send(method='get', path=path, params=params)

    def get_secret_detail(self, scope, name):
        path = self.get_common_secret_url(scope, name)
        return self.send(method='get', path=path, params={})

    def delete_secret(self, scope, name):
        path = self.get_common_secret_url(scope, name)
        return self.send(method='delete', path=path, params={})

    def update_secret(self, scope, name, file, data):
        path = self.get_common_secret_url(scope, name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='put', path=path, json=data, params={})
