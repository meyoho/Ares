from common.base_request import Common


class Template(Common):
    def get_cluster_template_url(self):
        return "devops/api/v1/clusterpipelinetemplatesync/TemplateSync"

    def get_cluster_template_list_url(self):
        return "devops/api/v1/clusterpipelinetemplate?" \
               "filterBy=labels,latest:true,labels,source:customer&sortBy=a,name&itemsPerPage=10&page=1"

    def post_cluster_template_url(self):
        return "devops/api/v1/clusterpipelinetemplatesync"

    def cluster_template_sync(self, file, data):
        path = self.get_cluster_template_url()
        data = self.generate_jinja_data(file, data)
        return self.send(method='put', path=path, json=data)

    def get_cluster_template_sync_detail(self):
        path = self.get_cluster_template_url()
        return self.send(method='get', path=path)

    def get_cluster_template_list(self):
        path = self.get_cluster_template_list_url()
        return self.send(method='get', path=path)

    def post_template_sync(self, file, data):
        path = self.post_cluster_template_url()
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=path, json=data)
