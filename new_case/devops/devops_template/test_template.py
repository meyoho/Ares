import pytest
from common import settings
from new_case.devops.devops_template.template import Template
from new_case.devops.devops_secret.secret import Secret
import base64


@pytest.mark.devops_controller
@pytest.mark.devopsApiServer
@pytest.mark.BAT
@pytest.mark.devops_api
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestTemplateSync(object):
    secret_data = {
        "secret_name": "{}-ares-gitlab-template".format(settings.RESOURCE_PREFIX),
        "secret_type": "kubernetes.io/basic-auth",
        "username": str(base64.b64encode(settings.GITLAB_USERNAME.encode('utf-8')), 'utf8'),
        "password": str(base64.b64encode(settings.GITLAB_PASSWORD.encode('utf-8')), 'utf8')
    }

    template_data = {
        "template_url": "{}/{}/{}".format(settings.GITLAB_URL, settings.GITLAB_USERNAME, settings.GITLAB_TEMPLATE_REPO),
        "secret_name": "{}-ares-gitlab-template".format(settings.RESOURCE_PREFIX),
        "secret_namespace": "global-credentials" if settings.DEFAULT_NS == "alauda-system" else "{}-global-credentials".format(
                    settings.DEFAULT_NS)
    }

    def setup_class(self):
        self.template = Template()
        self.secret = Secret()
        self.teardown_class(self)

        ret = self.secret.create_secret(scope="", file='./test_data/devops_secret/create_secret.jinja2', data=self.secret_data)
        assert ret.status_code in (200, 409), "创建平台模版同步凭据失败{}".format(ret.text)

    def teardown_class(self):
        if settings.CASE_TYPE not in ("prepare", "upgrade", "delete"):
            self.secret.delete_secret(scope="global-credentials", name=self.secret_data["secret_name"])

    @pytest.mark.prepare
    def 测试代码仓库输入私有仓库master分支搜素并选择凭据模版导入成功(self):
        datail_request = self.template.get_cluster_template_sync_detail()
        if datail_request.status_code == 404:
            ret = self.template.post_template_sync("./test_data/devops_template/cluster_template_sync.jinja2", data=self.template_data)
            assert ret.status_code == 200, "同步全局自定义模版失败{}".format(ret.text)
            sync_status = self.template.get_status(self.template.get_cluster_template_url(),
                                                   "status.phase", "Ready", params={}, expect_cnt=5)
            assert sync_status, "全局自定义模版同步失败,状态不对"
        elif datail_request.status_code == 200:
            assert True
        else:
            assert False, "获取模版详情页状态不对"

    def 测试模版同步后同步模版按钮可点击再次同步(self):
        ret = self.template.cluster_template_sync("./test_data/devops_template/cluster_template_sync.jinja2", data=self.template_data)
        assert ret.status_code == 200, "更新同步全局自定义模版失败{}".format(ret.text)
        sync_status = self.template.get_status(self.template.get_cluster_template_url(),
                                               "status.phase", "Ready", params={}, expect_cnt=5)
        assert sync_status, "更新全局自定义模版同步失败,状态不对"

    def 测试全局自定义模版同步详情(self):
        ret = self.template.get_cluster_template_sync_detail()
        assert ret.status_code == 200, "获取全局自定义模版同步详情失败{}".format(ret.text)
        value = self.template.generate_jinja_data("./verify_data/devops_template/cluster_template_sync_detail.jinja2",
                                                  data=self.template_data)
        assert self.template.is_sub_dict(value, ret.json()), "模版同步详情页数据校验错误，期望数据{}, 实际数据{}".format(value, ret.text)

    def 测试全局自定义模版列表(self):
        ret = self.template.get_cluster_template_list()
        assert ret.status_code == 200, "获取全局自定义模版失败{}".format(ret.text)
        template_name = self.template.get_value(ret.json(), "clusterpipelinetemplates.0.metadata.name")
        assert template_name == "Helloworld", "同步的全局自定义模版列表中不存在"
