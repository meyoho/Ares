import pytest
from new_case.devops.tool_chain.tool_chain import ToolChain
from common.log import logger


@pytest.fixture(scope="session")
def get_chain_list():
    chain_tool = ToolChain()
    # get exist data
    ret = chain_tool.get_chain_list()
    assert ret.status_code == 200, "获取全部工具链列表失败:{}".format(ret.text)
    data_dict = dict()
    contents = ret.json().get("items", [])
    for content in contents:
        name = chain_tool.get_value(content, "metadata.name")
        url = chain_tool.get_value(content, "spec.http.host")
        data_dict.update({name: url})
    logger.info("tool chain list data is {}".format(data_dict))
    return data_dict
