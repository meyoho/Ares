import pytest

from common import settings
from common.settings import get_featuregate_status, RESOURCE_PREFIX, FED_NAME
from common.utils import dockerjson
from new_case.containerplatform.application.application import Application
from new_case.containerplatform.fedapp.conftest import namespace_name, project_name
from new_case.containerplatform.fedapp.fedapp import FedApp
from new_case.containerplatform.ingress.ingress import Ingress


@pytest.mark.BAT
@pytest.mark.acp_containerplatform
@pytest.mark.skipif(not get_featuregate_status(None, 'kubefed'), reason="kubefed功能开关未开启，默认不跑对应用例")
@pytest.mark.skipif(not FED_NAME, reason="没有传联邦集群名称，默认不跑对应用例")
class TestAppFedSuite(object):
    def setup_class(self):
        self.app_client = Application()
        self.fedapp = FedApp()
        self.name = "{}-forfed".format(RESOURCE_PREFIX)
        self.data = {"app_name": self.name,
                     "ns_name": namespace_name,
                     "resources": [
                         {
                             "secret_name": self.name,
                             "K8S_NAMESPACE": namespace_name,
                             "data": {
                                 ".dockerconfigjson": dockerjson("index.alauda.cn", "alauda", "alauda",
                                                                 "hchan@alauda.io")
                             }
                         },
                         {
                             "configmap_name": self.name,
                             "namespace": namespace_name,
                             "configmap_key": "key",
                             "configmap_value": "value"
                         },
                         {
                             "deploy_name": self.name,
                             "kind": 'Deployment',
                             "ns_name": namespace_name,
                             "project": project_name,
                             "app_name": self.name,
                             "containers": [
                                 {
                                     "image": settings.IMAGE,
                                 }
                             ],
                         }, {
                             "deploy_name": self.name,
                             "kind": 'StatefulSet',
                             "ns_name": namespace_name,
                             "project": project_name,
                             "app_name": self.name,
                             "containers": [
                                 {
                                     "image": settings.IMAGE,
                                 }
                             ],
                         }, {
                             "service_name": self.name,
                             "ns_name": namespace_name,
                             "project": project_name,
                             "app_name": self.name,
                             "deploy_name": self.name,
                             "deploy_kind": 'StatefulSet',
                             "ports": [
                                 {
                                     "protocol": "TCP",
                                     "port": 80,
                                     "targetPort": 80
                                 }
                             ]
                         },
                         {
                             "ingress_name": self.name,
                             "ns_name": namespace_name,
                             "host": '.'.join([self.name, namespace_name]),
                             "url_path": '/',
                             "service_name": self.name,
                             "service_port": 80
                         }
                     ]}
        self.fedapp_data = {"app_name": self.name,
                            "ns_name": namespace_name,
                            "default_label": settings.DEFAULT_LABEL,
                            "resources": [{
                                "name": self.name,
                                "kind": 'Secret',
                                "ns_name": namespace_name,
                                "data": {
                                    ".dockerconfigjson": dockerjson("index.alauda.cn", "alauda", "alauda",
                                                                    "hchan@alauda.io")
                                }
                            }, {
                                "name": self.name,
                                "kind": 'ConfigMap',
                                "ns_name": namespace_name,
                                "data": {'key': 'value'}
                            }, {
                                "name": self.name,
                                "kind": 'Deployment',
                                "ns_name": namespace_name,
                                "project": project_name,
                                "app_name": self.name,
                                "containers": [
                                    {
                                        "image": settings.IMAGE,
                                    }
                                ],
                            }, {
                                "name": self.name,
                                "kind": 'StatefulSet',
                                "ns_name": namespace_name,
                                "project": project_name,
                                "app_name": self.name,
                                "containers": [
                                    {
                                        "image": settings.IMAGE,
                                    }
                                ],
                            }, {
                                "name": self.name,
                                "kind": 'Service',
                                "ns_name": namespace_name,
                                "project": project_name,
                                "app_name": self.name,
                                "workload_name": self.name,
                                "workload_kind": 'StatefulSet',
                                "ports": [
                                    {
                                        "protocol": "TCP",
                                        "port": 80,
                                        "targetPort": 80
                                    }
                                ]
                            }, {"name": self.name,
                                "kind": 'Ingress',
                                "ns_name": namespace_name, }]}
        self.name = self.data['app_name']
        self.fedapp.delete(namespace_name, self.name)
        self.app_client.delete_app(namespace_name, self.name, self.fedapp.host_cluster_name)
        self.ingress_client = Ingress()
        self.ingress_client.delete_ingress(namespace_name, self.name, self.fedapp.host_cluster_name)

    def teardown_class(self):
        self.fedapp.delete(namespace_name, self.name)
        self.app_client.delete_app(namespace_name, self.name, self.fedapp.host_cluster_name)
        self.ingress_client.delete_ingress(namespace_name, self.name, self.fedapp.host_cluster_name)

    def 测试应用联邦化(self):
        response = self.app_client.create_app_by_template(self.data, namespace_name, self.fedapp.host_cluster_name)
        assert response.status_code == 200, "应用创建失败:{}".format(response.text)
        ret = self.fedapp.federate(namespace_name, self.name)
        assert ret.status_code == 200, "应用联邦化失败:{}".format(ret.text)
        values = self.fedapp.render_app_template(self.fedapp_data)
        assert self.fedapp.is_sub_dict(values, ret.json()), "应用联邦化比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)
        assert self.fedapp.check_exists(self.fedapp.common_url(namespace_name, self.name), 200), "联邦化后，联邦应用未创建"
        for resource in self.fedapp_data['resources']:
            if resource['kind'] in ['Deployment', 'StatefulSet', 'Service', 'ConfigMap', 'Secret']:
                for cluster in self.fedapp.all_cluster_name:
                    assert self.fedapp.check_exists(
                        self.fedapp.resource_url(namespace_name, resource['kind'], resource['name'], cluster),
                        200), "应该联邦的资源没有被创建呀! {}集群{}类型的{}未创建".format(cluster, resource['kind'], resource['name'])
            else:
                for cluster in self.fedapp.member_cluster_name:
                    assert self.fedapp.check_exists(
                        self.fedapp.resource_url(namespace_name, resource['kind'], resource['name'], cluster),
                        404), "不应该联邦的资源被创建啦！{}集群{}类型的{}已创建".format(cluster, resource['kind'], resource['name'])
