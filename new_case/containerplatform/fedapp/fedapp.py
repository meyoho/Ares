import copy

import yaml

from common.base_request import Common
from common import settings


class FedApp(Common):
    def __init__(self):
        super(FedApp, self).__init__()
        self.genetate_global_info()
        self.fedapp_tpl = self.generate_jinja_template("./test_data/fedapp/fedapp.jinja2")
        self.workload_tpl = self.generate_jinja_template("./test_data/fedapp/workload.jinja2")
        self.container_tpl = self.generate_jinja_template("./test_data/fedapp/container.jinja2")
        self.service_tpl = self.generate_jinja_template("./test_data/fedapp/service.jinja2")
        self.configmap_tpl = self.generate_jinja_template("./test_data/fedapp/configmap.jinja2")
        self.secret_tpl = self.generate_jinja_template("./test_data/fedapp/secret.jinja2")
        self.host_cluster_name = ''
        self.member_cluster_name = []
        self.all_cluster_name = []
        url = 'apis/infrastructure.alauda.io/v1alpha1/clusterfeds/{}'.format(settings.FED_NAME)
        response = self.send(method='get', path=url)
        if response.status_code == 200:
            for cluster in response.json()['spec']['clusters']:
                self.all_cluster_name.append(cluster['name'])
                if cluster['type'] == 'Host':
                    self.host_cluster_name = cluster['name']
                else:
                    self.member_cluster_name.append(cluster['name'])

    def render_app_template(self, data):
        _data = copy.deepcopy(data)
        rs = []
        if 'resources' in _data:
            for index, resource in enumerate(_data['resources']):
                resource.update({"default_label": settings.DEFAULT_LABEL})
                if resource['kind'] == 'ConfigMap':
                    rs.append({"resource": self.configmap_tpl.render(**resource)})
                elif resource['kind'] == 'Secret':
                    rs.append({"resource": self.secret_tpl.render(**resource)})
                elif resource['kind'] == 'Service':
                    rs.append({"resource": self.service_tpl.render(**resource)})
                elif resource['kind'] in ['Deployment', 'StatefulSet']:
                    cts = []
                    for container in resource['containers']:
                        cts.append(self.container_tpl.render(**container))
                    resource['containers'] = cts
                    rs.append({"resource": self.workload_tpl.render(**resource)})
                if 'overrides' in resource and len(resource["overrides"]) and rs[index]:
                    rs[index].update({"overrides": resource["overrides"]})
        _data['resources'] = rs
        return yaml.safe_load(self.fedapp_tpl.render(_data))

    def render_resource_template(self, data):
        resource = copy.deepcopy(data)
        resource.update({"default_label": settings.DEFAULT_LABEL})
        if resource['kind'] == 'ConfigMap':
            return yaml.safe_load(self.configmap_tpl.render(**resource))
        elif resource['kind'] == 'Secret':
            return yaml.safe_load(self.secret_tpl.render(**resource))
        elif resource['kind'] == 'Service':
            return yaml.safe_load(self.service_tpl.render(**resource))
        elif resource['kind'] in ['Deployment', 'StatefulSet']:
            cts = []
            for container in resource['containers']:
                cts.append(self.container_tpl.render(**container))
            resource['containers'] = cts
            return yaml.safe_load(self.workload_tpl.render(**resource))

    def create(self, namespace_name, data):
        path = 'acp/v1/kubernetes/{}/namespaces/{}/federatedapplications'.format(self.host_cluster_name,
                                                                                 namespace_name)
        data = self.render_app_template(data)
        return self.send(method='post', path=path, json=data)

    def federate(self, namespace_name, name):
        path = 'acp/v1/kubernetes/{}/namespaces/{}/applications/{}/federate'.format(self.host_cluster_name,
                                                                                    namespace_name, name)
        return self.send(method='post', path=path)

    def list(self, namespace_name):
        path = 'kubernetes/{}/apis/types.kubefed.io/v1beta1/namespaces/{}/federatedapplications'.format(
            self.host_cluster_name, namespace_name)
        return self.send(method='get', path=path)

    def detail(self, namespace_name, name):
        path = 'kubernetes/{}/apis/types.kubefed.io/v1beta1/namespaces/{}/federatedapplications/{}'.format(
            self.host_cluster_name, namespace_name, name)
        return self.send(method='get', path=path)

    def components(self, namespace_name, name):
        path = 'acp/v1/kubernetes/{}/namespaces/{}/federatedapplications/{}/components'.format(
            self.host_cluster_name, namespace_name, name)
        return self.send(method='get', path=path)

    def delete(self, namespace_name, name):
        path = 'kubernetes/{}/apis/types.kubefed.io/v1beta1/namespaces/{}/federatedapplications/{}'.format(
            self.host_cluster_name, namespace_name, name)
        return self.send(method='delete', path=path)

    def force_delete(self, namespace_name, name):
        path = 'kubernetes/{}/apis/types.kubefed.io/v1beta1/namespaces/{}/federatedapplications/{}'.format(
            self.host_cluster_name, namespace_name, name)
        data = [{"op": "replace", "path": "/metadata/finalizers", "value": []}]
        return self.send(method='PATCH', path=path, json=data, headers={"Content-Type": "application/merge-patch+json"})

    def update(self, namespace_name, name, data):
        path = 'acp/v1/kubernetes/{}/namespaces/{}/federatedapplications/{}'.format(self.host_cluster_name,
                                                                                    namespace_name, name)
        data = self.render_app_template(data)
        return self.send(method='put', path=path, json=data)

    def common_url(self, namespace_name, name):
        return 'kubernetes/{}/apis/types.kubefed.io/v1beta1/namespaces/{}/federatedapplications/{}'.format(
            self.host_cluster_name, namespace_name, name)

    def resource_url(self, namespace_name, kind, name, region):
        kinds = {'Deployment': 'deployments', 'StatefulSet': 'statefulsets', 'Service': 'services',
                 'ConfigMap': 'configmaps', 'Secret': 'secrets', 'Ingress': 'ingresses'}
        if kind in ['Deployment', 'StatefulSet']:
            return 'kubernetes/{}/apis/apps/v1/namespaces/{}/{}/{}'.format(
                region, namespace_name, kinds.get(kind), name)
        elif kind == 'Ingress':
            return 'kubernetes/{}/apis/extensions/v1beta1/namespaces/{}/{}/{}'.format(
                region, namespace_name, kinds.get(kind), name)
        else:
            return 'kubernetes/{}/api/v1/namespaces/{}/{}/{}'.format(
                region, namespace_name, kinds.get(kind), name)

    def detail_resource(self, namespace_name, kind, name, region):
        path = self.resource_url(namespace_name, kind, name, region)
        return self.send(method='get', path=path)
