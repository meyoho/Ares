import pytest

from common import settings
from common.log import logger
from common.settings import CASE_TYPE, get_featuregate_status, RESOURCE_PREFIX, FED_NAME
from new_case.containerplatform.fedapp.fedapp import FedApp
from new_case.containerplatform.fedapp.conftest import project_name


@pytest.mark.BAT
@pytest.mark.acp_containerplatform
@pytest.mark.skipif(not get_featuregate_status(None, 'kubefed'), reason="kubefed功能开关未开启，默认不跑对应用例")
@pytest.mark.skipif(not FED_NAME, reason="没有传联邦集群名称，默认不跑对应用例")
class TestFedAppSuite(object):
    def setup_class(self):
        self.fedapp = FedApp()
        self.project_name = project_name
        self.namespace_name = "{}-fedapp".format(project_name)
        self.name = "{}-l0".format(RESOURCE_PREFIX)
        self.data = {
            "app_name": self.name,
            "ns_name": self.namespace_name,
            "default_label": settings.DEFAULT_LABEL,
            "resources": []
        }

    def teardown_class(self):
        if CASE_TYPE not in ("prepare", "upgrade", "delete"):
            self.fedapp.delete(self.namespace_name, self.name)

    @pytest.mark.prepare
    def 测试创建联邦应用L0_无资源(self):
        self.fedapp.delete(self.namespace_name, self.name)
        self.fedapp.check_exists(self.fedapp.common_url(self.namespace_name, self.name), 404)
        ret = self.fedapp.create(self.namespace_name, self.data)
        assert ret.status_code == 200, "创建联邦应用失败:{}".format(ret.text)
        logger.info(ret.json())
        values = self.fedapp.render_app_template(self.data)
        assert self.fedapp.is_sub_dict(values, ret.json()), \
            "创建联邦应用比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)

    def 测试联邦应用下的组件L0(self):
        ret = self.fedapp.components(self.namespace_name, self.name)
        assert ret.status_code == 200, "联邦应用组件失败:{}".format(ret.text)
        values = self.fedapp.generate_jinja_data("verify_data/fedapp/components_response.jinja2", self.data)
        assert self.fedapp.is_sub_dict(values, ret.json()), \
            "联邦应用组件比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)

    @pytest.mark.upgrade
    def 测试联邦应用详情L0(self):
        ret = self.fedapp.detail(self.namespace_name, self.name)
        assert ret.status_code == 200, "联邦应用详情失败:{}".format(ret.text)
        values = self.fedapp.render_app_template(self.data)
        assert self.fedapp.is_sub_dict(values, ret.json()), \
            "联邦应用详情比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)

    def 测试联邦应用列表L0(self):
        ret = self.fedapp.list(self.namespace_name)
        assert ret.status_code == 200, "联邦应用列表失败:{}".format(ret.text)
        content = self.fedapp.get_k8s_resource_data(ret.json(), self.name, list_key="items")
        values = self.fedapp.render_app_template(self.data)
        assert self.fedapp.is_sub_dict(values, content), \
            "联邦应用列表比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)

    @pytest.mark.upgrade
    def 测试更新联邦应用L0_添加statefulset(self):
        self.data.update({'resources': [
            {
                "name": "{}-sts".format(self.name),
                "kind": 'StatefulSet',
                "ns_name": self.namespace_name,
                "project": self.project_name,
                "app_name": self.name,
                "containers": [
                    {
                        "image": settings.IMAGE,
                    }
                ],
            }
        ]})
        ret = self.fedapp.update(self.namespace_name, self.name, self.data)
        assert ret.status_code == 200, "更新联邦应用失败:{}".format(ret.text)
        logger.info(ret.json())
        values = self.fedapp.render_app_template(self.data)
        assert self.fedapp.is_sub_dict(values, ret.json()), \
            "更新联邦应用比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)
        for cluster in self.fedapp.all_cluster_name:
            for resource in self.data['resources']:
                assert self.fedapp.check_exists(
                    self.fedapp.resource_url(self.namespace_name, resource['kind'], resource['name'], cluster),
                    200), "{}集群{}类型的{}未创建".format(cluster, resource['kind'], resource['name'])

    @pytest.mark.delete
    def 测试删除联邦应用L0(self):
        ret1 = self.fedapp.delete(self.namespace_name, self.name)
        assert ret1.status_code == 200, "删除联邦应用失败:{}".format(ret1.text)
        assert self.fedapp.check_exists(
            self.fedapp.common_url(self.namespace_name, self.name), 404)
