import pytest

from common.settings import get_featuregate_status, FED_NAME
from new_case.containerplatform.fedapp.fedapp import FedApp
from new_case.containerplatform.fedapp.conftest import project_name, namespace_name, l1_same_list, l1_diff_list


@pytest.mark.acp_containerplatform
@pytest.mark.skipif(not get_featuregate_status(None, 'kubefed'), reason="kubefed功能开关未开启，默认不跑对应用例")
@pytest.mark.skipif(not FED_NAME, reason="没有传联邦集群名称，默认不跑对应用例")
class TestFedAppSuite(object):
    def setup_class(self):
        self.fedapp = FedApp()
        self.project_name = project_name
        self.namespace_name = namespace_name

    @pytest.mark.delete
    @pytest.mark.parametrize("key", {**l1_same_list, **l1_diff_list})
    def 测试删除联邦应用(self, key):
        data = {**l1_same_list, **l1_diff_list}[key]
        self.name = data['app_name']
        ret1 = self.fedapp.delete(self.namespace_name, self.name)
        assert ret1.status_code == 200, "删除联邦应用失败:{}".format(ret1.text)
        flag = self.fedapp.check_exists(self.fedapp.common_url(self.namespace_name, self.name), 404)
        if flag is False:
            self.fedapp.force_delete(self.namespace_name, self.name)
        assert flag
