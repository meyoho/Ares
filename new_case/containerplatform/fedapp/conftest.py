import base64

import pytest

from common import settings
from common.utils import dockerjson
from new_case.containerplatform.fedns.fedns import FedNs
from new_case.platform.project.project import Project

project_name = 'e2enamespace-kubefed'
namespace_name = "{}-fedapp".format(project_name)
fedns = FedNs()
all_cluster_name = fedns.all_cluster_name
host_cluster_name = fedns.host_cluster_name
prefix = settings.RESOURCE_PREFIX
l1_same_list = {
    '联邦应用包含多个deployment': {
        "app_name": "{}-l1-deploys".format(prefix),
        "ns_name": namespace_name,
        "default_label": settings.DEFAULT_LABEL,
        "resources": [{
            "name": "{}-l1-deploy1".format(prefix),
            "kind": 'Deployment',
            "ns_name": namespace_name,
            "project": project_name,
            "app_name": "{}-l1-deploys".format(prefix),
            "containers": [
                {
                    "image": settings.IMAGE,
                }
            ],
        }, {
            "name": "{}-l1-deploy2".format(prefix),
            "kind": 'Deployment',
            "ns_name": namespace_name,
            "project": project_name,
            "app_name": "{}-l1-deploys".format(prefix),
            "containers": [
                {
                    "image": settings.IMAGE,
                }
            ],
        }]
    },
    '联邦应用包含deployment和service': {
        "app_name": "{}-l1-service".format(prefix),
        "ns_name": namespace_name,
        "default_label": settings.DEFAULT_LABEL,
        "resources": [{
            "name": "{}-l1-service".format(prefix),
            "kind": 'Deployment',
            "ns_name": namespace_name,
            "project": project_name,
            "app_name": "{}-l1-service".format(prefix),
            "containers": [
                {
                    "image": settings.IMAGE,
                }
            ],
        },
            {
                "name": "{}-l1-clusterip".format(prefix),
                "kind": 'Service',
                "ns_name": namespace_name,
                "project": project_name,
                "app_name": "{}-l1-service".format(prefix),
                "workload_name": "{}-l1-service".format(prefix),
                "workload_kind": 'Deployment',
                "ports": [
                    {
                        "protocol": "TCP",
                        "port": 80,
                        "targetPort": 80
                    }
                ]
            },
            {
                "name": "{}-l1-nodeport".format(prefix),
                "kind": 'Service',
                "ns_name": namespace_name,
                "project": project_name,
                "app_name": "{}-l1-service".format(prefix),
                "workload_name": "{}-l1-service".format(prefix),
                "workload_kind": 'Deployment',
                "service_type": "NodePort",
                "sessionAffinity": "ClientIP",
                "ports": [
                    {
                        "name": "tcp-91-80",
                        "protocol": "TCP",
                        "port": 91,
                        "targetPort": 80
                    },
                    {
                        "name": "udp-92-80",
                        "protocol": "UDP",
                        "port": 92,
                        "targetPort": 80
                    },
                    {
                        "name": "http-93-80",
                        "protocol": "TCP",
                        "port": 93,
                        "targetPort": 80
                    },
                    {
                        "name": "https-94-80",
                        "protocol": "TCP",
                        "port": 94,
                        "targetPort": 80
                    },
                    {
                        "name": "http2-95-80",
                        "protocol": "TCP",
                        "port": 95,
                        "targetPort": 80
                    },
                    {
                        "name": "grpc-96-80",
                        "protocol": "TCP",
                        "port": 96,
                        "targetPort": 80
                    }
                ]
            }
        ]
    },
    '联邦应用包含configmap': {
        "app_name": "{}-l1-config".format(prefix),
        "ns_name": namespace_name,
        "default_label": settings.DEFAULT_LABEL,
        "resources": [{
            "name": "{}-l1-cm-empty".format(prefix),
            "kind": 'ConfigMap',
            "ns_name": namespace_name,
        }, {
            "name": "{}-l1-cm-many".format(prefix),
            "kind": 'ConfigMap',
            "ns_name": namespace_name,
            "data": {'key1': 'value1', 'key2': 'value2', 'key3': 'value3'}
        }]},
    '联邦应用包含secret': {
        "app_name": "{}-l1-secret".format(prefix),
        "ns_name": namespace_name,
        "default_label": settings.DEFAULT_LABEL,
        "resources": [{
            "name": "{}-l1-opaque".format(prefix),
            "kind": 'Secret',
            "ns_name": namespace_name,
            "data": {
                "opaque_key": str(base64.b64encode("opaque_value".encode('utf-8')), 'utf8')
            }
        }, {
            "name": "{}-l1-tls".format(prefix),
            "kind": 'Secret',
            "ns_name": namespace_name,
            "data": {
                "tls.crt": str(base64.b64encode("tls.crt".encode('utf-8')), 'utf8'),
                "tls.key": str(base64.b64encode("tls.key".encode('utf-8')), 'utf8')
            }
        }, {
            "name": "{}-l1-ssh".format(prefix),
            "kind": 'Secret',
            "ns_name": namespace_name,
            "data": {
                "ssh-privatekey": str(base64.b64encode("value".encode('utf-8')), 'utf8')
            }
        }, {
            "name": "{}-l1-user".format(prefix),
            "kind": 'Secret',
            "ns_name": namespace_name,
            "data": {
                "username": str(base64.b64encode("username".encode('utf-8')), 'utf8'),
                "password": str(base64.b64encode("password".encode('utf-8')), 'utf8')
            }
        }, {
            "name": "{}-l1-image".format(prefix),
            "kind": 'Secret',
            "ns_name": namespace_name,
            "data": {
                ".dockerconfigjson": dockerjson("index.alauda.cn", "alauda", "alauda", "hchan@alauda.io")
            }
        }]
    }
}
l1_diff_list = {
    '联邦应用包含应用差异化': {
        "app_name": "{}-app-diff".format(prefix),
        "ns_name": namespace_name,
        "default_label": settings.DEFAULT_LABEL,
        "app_overrides": [{'clustername': host_cluster_name, 'op': 'add', 'value': 'e2eadd'}]
    },
    '联邦应用包含部署差异化': {
        "app_name": "{}-deploy-diff".format(prefix),
        "ns_name": namespace_name,
        "default_label": settings.DEFAULT_LABEL,
        "resources": [{
            "name": "{}-deploy-diff".format(prefix),
            "kind": 'Deployment',
            "ns_name": namespace_name,
            "project": project_name,
            "app_name": "{}-deploy-diff".format(prefix),
            "containers": [
                {
                    "image": settings.IMAGE,
                }
            ],
            "overrides": [
                {'clustername': host_cluster_name,
                 'clusteroverrides': [
                     {'path': '/spec/replicas', 'value': 0},
                     {'path': '/spec/template/spec/containers/0/resources/limits/cpu', 'value': '40m'},
                     {'path': '/spec/template/spec/containers/0/resources/limits/memory', 'value': '40Mi'}]
                 }]
        }]
    },
    '联邦应用包含有状态副本集差异化': {
        "app_name": "{}-sts-diff".format(prefix),
        "ns_name": namespace_name,
        "default_label": settings.DEFAULT_LABEL,
        "resources": [{
            "name": "{}-sts-diff".format(prefix),
            "kind": 'StatefulSet',
            "ns_name": namespace_name,
            "project": project_name,
            "app_name": "{}-sts-diff".format(prefix),
            "containers": [
                {
                    "image": settings.IMAGE,
                }
            ],
            "overrides": [
                {'clustername': host_cluster_name,
                 'clusteroverrides': [
                     {'path': '/spec/replicas', 'value': 0},
                     {'path': '/spec/template/spec/containers/0/resources/limits/memory', 'value': '40Mi'}]
                 }]
        }]
    }
}

l1_same_update_list = {
    '联邦应用包含多个deployment_更新资源': {
        "app_name": "{}-l1-deploys".format(prefix),
        "ns_name": namespace_name,
        "default_label": settings.DEFAULT_LABEL,
        "resources": [{
            "name": "{}-l1-deploy1".format(prefix),
            "kind": 'Deployment',
            "ns_name": namespace_name,
            "project": project_name,
            "app_name": "{}-l1-deploys".format(prefix),
            "containers": [
                {
                    "image": settings.IMAGE,
                }
            ],
        }, {
            "name": "{}-l1-deploy2".format(prefix),
            "kind": 'Deployment',
            "maxSurge": 1,
            "maxUnavailable": 1,
            "ns_name": namespace_name,
            "project": project_name,
            "app_name": "{}-l1-deploys".format(prefix),
            "containers": [
                {
                    "image": settings.IMAGE,
                    "commands": ["/run.sh"],
                    "args": ["/bin/sh"],
                    "envs": [
                        {
                            "name": "ares-env1",
                            "value": "test"
                        },
                        {
                            "name": "__FILE_LOG_PATH__",
                            "value": "/var/*.txt"
                        },
                        {
                            "name": "__EXCLUDE_LOG_PATH__",
                            "value": "/var/hehe.txt"
                        }]
                }
            ],
        }]
    },
    '联邦应用包含deployment和service_删除资源': {
        "app_name": "{}-l1-service".format(prefix),
        "ns_name": namespace_name,
        "default_label": settings.DEFAULT_LABEL,
        "resources": [{
            "name": "{}-l1-service".format(prefix),
            "kind": 'Deployment',
            "ns_name": namespace_name,
            "project": project_name,
            "app_name": "{}-l1-service".format(prefix),
            "containers": [
                {
                    "image": settings.IMAGE,
                }
            ],
        },
            {
                "name": "{}-l1-clusterip".format(prefix),
                "kind": 'Service',
                "ns_name": namespace_name,
                "project": project_name,
                "app_name": "{}-l1-service".format(prefix),
                "workload_name": "{}-l1-service".format(prefix),
                "workload_kind": 'Deployment',
                "ports": [
                    {
                        "protocol": "TCP",
                        "port": 80,
                        "targetPort": 80
                    }
                ]
            }
        ]
    },
    '联邦应用包含configmap_添加资源和删除资源': {
        "app_name": "{}-l1-config".format(prefix),
        "ns_name": namespace_name,
        "default_label": settings.DEFAULT_LABEL,
        "resources": [{
            "name": "{}-l1-cm-empty".format(prefix),
            "kind": 'ConfigMap',
            "ns_name": namespace_name,
        }, {
            "name": "{}-l1-cm-many-add".format(prefix),
            "kind": 'ConfigMap',
            "ns_name": namespace_name,
            "data": {'key1': 'value1', 'key2': 'value2', 'key3': 'value3'}
        }]},
    '联邦应用包含secret_更新资源和删除资源': {
        "app_name": "{}-l1-secret".format(prefix),
        "ns_name": namespace_name,
        "default_label": settings.DEFAULT_LABEL,
        "resources": [{
            "name": "{}-l1-opaque".format(prefix),
            "kind": 'Secret',
            "ns_name": namespace_name,
            "data": {
                "opaque_key": str(base64.b64encode("opaque_value_update".encode('utf-8')), 'utf8')
            }
        }]
    }
}
l1_diff_update_list = {
    '联邦应用包含应用差异化_更新差异化': {
        "app_name": "{}-app-diff".format(prefix),
        "ns_name": namespace_name,
        "default_label": settings.DEFAULT_LABEL,
        "app_overrides": [{'clustername': host_cluster_name, 'op': 'add', 'value': 'e2eupdate'}]
    },
    '联邦应用包含部署差异化_删除差异化': {
        "app_name": "{}-deploy-diff".format(prefix),
        "ns_name": namespace_name,
        "default_label": settings.DEFAULT_LABEL,
        "resources": [{
            "name": "{}-deploy-diff".format(prefix),
            "kind": 'Deployment',
            "ns_name": namespace_name,
            "project": project_name,
            "app_name": "{}-deploy-diff".format(prefix),
            "containers": [
                {
                    "image": settings.IMAGE,
                }
            ]
        }]
    },
    '联邦应用包含有状态副本集差异化_添加差异化': {
        "app_name": "{}-sts-diff".format(prefix),
        "ns_name": namespace_name,
        "default_label": settings.DEFAULT_LABEL,
        "resources": [{
            "name": "{}-sts-diff".format(prefix),
            "kind": 'StatefulSet',
            "ns_name": namespace_name,
            "project": project_name,
            "app_name": "{}-sts-diff".format(prefix),
            "containers": [
                {
                    "image": settings.IMAGE,
                }
            ],
            "overrides": [
                {'clustername': host_cluster_name,
                 'clusteroverrides': [
                     {'path': '/spec/replicas', 'value': 2},
                     {'path': '/spec/template/spec/containers/0/resources/limits/cpu', 'value': '40m'},
                     {'path': '/spec/template/spec/containers/0/resources/limits/memory', 'value': '40Mi'}]
                 }]
        }]
    }
}


@pytest.fixture(scope="session", autouse=True)
@pytest.mark.skipif(not settings.get_featuregate_status(None, 'kubefed'), reason="kubefed功能开关未开启，默认不跑对应用例")
def prepare():
    project = Project()
    data = {
        "project_name": project_name,
        'fed_region': settings.FED_NAME,
        "regions": all_cluster_name,
        "display_name": 'e2e 联邦测试',
        "description": "e2e test project",
        "cpu": "10",
        "memory": "10Gi"
    }
    create_project_result = project.create_project("./test_data/project/create_project.jinja2", data)
    assert create_project_result.status_code in (201, 409), "创建项目失败"
    fedns_data = {
        "name": "{}-fedapp".format(project_name),
        "hostname": host_cluster_name,
        "fed_name": settings.FED_NAME,
        "project_name": project_name,
        "override": 'false',
        "pod": 50
    }
    ret = fedns.create(project_name, fedns_data)
    assert ret.status_code in (200, 409), "创建联邦命名空间失败:{}".format(ret.text)
