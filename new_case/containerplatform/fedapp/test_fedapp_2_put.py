import pytest

from common.settings import get_featuregate_status, FED_NAME
from new_case.containerplatform.application.application import Application
from new_case.containerplatform.fedapp.fedapp import FedApp
from new_case.containerplatform.fedapp.conftest import project_name, namespace_name, l1_diff_update_list, \
    l1_same_update_list


@pytest.mark.acp_containerplatform
@pytest.mark.skipif(not get_featuregate_status(None, 'kubefed'), reason="kubefed功能开关未开启，默认不跑对应用例")
@pytest.mark.skipif(not FED_NAME, reason="没有传联邦集群名称，默认不跑对应用例")
class TestFedAppSuite(object):
    def setup_class(self):
        self.fedapp = FedApp()
        self.project_name = project_name
        self.namespace_name = namespace_name
        self.app_client = Application()

    @pytest.mark.upgrade
    @pytest.mark.parametrize("key", l1_same_update_list)
    def 测试更新联邦应用_无差异化(self, key):
        data = l1_same_update_list[key]
        data.update({'upgrade': 'true'})
        name = data['app_name']
        ret = self.fedapp.update(self.namespace_name, name, data)
        assert ret.status_code == 200, "更新联邦应用失败:{}".format(ret.text)
        values = self.fedapp.render_app_template(data)
        assert self.fedapp.is_sub_dict(values, ret.json()), \
            "更新联邦应用比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)
        for cluster in self.fedapp.all_cluster_name:
            for resource in data['resources']:
                self.app_client.wait_app_status(self.namespace_name, name, cluster)
                assert self.fedapp.check_exists(
                    self.fedapp.resource_url(self.namespace_name, resource['kind'], resource['name'], cluster),
                    200), "{}集群{}类型的{}未创建".format(cluster, resource['kind'], resource['name'])
                ret = self.fedapp.detail_resource(self.namespace_name, resource['kind'], resource['name'], cluster)
                values = self.fedapp.render_resource_template(resource)
                assert self.fedapp.is_sub_dict(values, ret.json()), \
                    "{}集群{}类型的{} 比对数据失败，返回数据{}，期望数据{}".format(cluster, resource['kind'], resource['name'], ret.text,
                                                              values)
        # 验证更新后的组件个数
        if 'resources' in data:
            ret = self.fedapp.components(self.namespace_name, name)
            assert ret.status_code == 200, "获取联邦应用组件失败:{}".format(ret.text)
            assert len(data['resources']) == len(self.fedapp.get_value(ret.json(), 'spec.components')) - 1, \
                "联邦应用组件个数比对数据失败，返回数据{}，期望数据{}".format(ret.text, data)

    @pytest.mark.upgrade
    @pytest.mark.parametrize("key", l1_diff_update_list)
    def 测试更新联邦应用_有差异化(self, key):
        data = l1_diff_update_list[key]
        data.update({'upgrade': 'true'})
        name = data['app_name']
        ret = self.fedapp.update(self.namespace_name, name, data)
        assert ret.status_code == 200, "更新联邦应用失败:{}".format(ret.text)
        values = self.fedapp.render_app_template(data)
        assert self.fedapp.is_sub_dict(values, ret.json()), \
            "更新联邦应用比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)
        # 验证成员集群中的资源创建
        for cluster in self.fedapp.member_cluster_name:
            if 'resources' in data:
                for resource in data['resources']:
                    self.app_client.wait_app_status(self.namespace_name, name, cluster)
                    assert self.fedapp.check_exists(
                        self.fedapp.resource_url(self.namespace_name, resource['kind'], resource['name'], cluster),
                        200), "{}集群{}类型的{}未创建".format(cluster, resource['kind'], resource['name'])
                    ret = self.fedapp.detail_resource(self.namespace_name, resource['kind'], resource['name'], cluster)
                    values = self.fedapp.render_resource_template(resource)
                    assert self.fedapp.is_sub_dict(values, ret.json()), \
                        "{}集群{}类型的{} 比对数据失败，返回数据{}，期望数据{}".format(cluster, resource['kind'], resource['name'], ret.text,
                                                                  values)
        # 验证应用差异化
        if 'app_overrides' in data:
            for app_override in data['app_overrides']:
                self.app_client.wait_app_status(self.namespace_name, name, app_override['clustername'])
                data.update({'display_name': app_override['value']})
                ret = self.app_client.detail_app(self.namespace_name, name, app_override['clustername'])
                assert ret.status_code == 200, "验证应用差异化 {}集群 获取应用详情失败:{}".format(app_override['clustername'], ret.text)
                values = self.app_client.generate_jinja_data("verify_data/fedapp/app_response.jinja2",
                                                             data)
                assert self.app_client.is_sub_list(values, ret.json()), \
                    "验证应用差异化{}集群详情比对数据失败，返回数据{}，期望数据{}".format(app_override['clustername'], ret.json(), values)
        # 验证部署、有状态副本集差异化
        if 'resources' in data and 'overrides' in str(data['resources']):
            for resource in data['resources']:
                for override in resource['overrides']:
                    for clusteroverride in override['clusteroverrides']:
                        if clusteroverride['path'] == '/spec/replicas':
                            resource.update({'replicas': clusteroverride['value']})
                        elif clusteroverride['path'] == '/spec/template/spec/containers/0/resources/limits/cpu':
                            resource['containers'][0].update({'cpu_limits': clusteroverride['value']})
                        elif clusteroverride['path'] == '/spec/template/spec/containers/0/resources/limits/memory':
                            resource['containers'][0].update({'memory_limits': clusteroverride['value']})
                    values = self.fedapp.render_resource_template(resource)
                    assert self.fedapp.check_exists(
                        self.fedapp.resource_url(self.namespace_name, resource['kind'], resource['name'],
                                                 override['clustername']),
                        200), "{}集群{}类型的{}未创建".format(override['clustername'], resource['kind'], resource['name'])
                    ret = self.fedapp.detail_resource(self.namespace_name, resource['kind'], resource['name'],
                                                      override['clustername'])
                    assert self.fedapp.is_sub_dict(values, ret.json()), \
                        "验证workload差异化失败 {}集群{}类型的{} 比对数据失败，返回数据{}，期望数据{}".format(override['clustername'],
                                                                                  resource['kind'], resource['name'],
                                                                                  ret.text, values)
