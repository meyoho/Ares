import pytest
import yaml
from common.settings import K8S_NAMESPACE, RERUN_TIMES, USERNAME, REGION_NAME, AUDIT_UNABLED
from new_case.containerplatform.application.application import Application
from new_case.containerplatform.secret.secret import Secret
from new_case.containerplatform.persistentvolumeclaims.pvc import Pvc
from new_case.containerplatform.configmap.configmap import Configmap
from new_case.containerplatform.application.conftest import l1_update_data, update_application_casename


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestPutApp(object):

    def setup_class(self):
        self.app_client = Application()
        self.k8s_namespace = K8S_NAMESPACE
        self.secret_tool = Secret()
        self.cm_tool = Configmap()
        self.pvc = Pvc()

    @pytest.mark.metis
    @pytest.mark.parametrize("data", l1_update_data, ids=update_application_casename)
    def 测试更新应用L1(self, data):
        ret = self.app_client.update_app_by_template(data["app_name"], data)
        assert ret.status_code == 200, "更新应用失败:{}".format(ret.text)
        values = yaml.safe_load(self.app_client.render_app_template(data))
        if "'resources': []" in str(data):
            values['spec']['componentTemplates'] = None
        assert self.app_client.is_sub_dict(values, ret.json()), \
            "更新应用比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)
        if 'deploy_name' not in str(data):
            ret = self.app_client.get_app_status(self.k8s_namespace, data['app_name'], 'Empty')
            assert ret, '更新应用后，应用状态不是无计算组件'
        elif "'replicas': 0" in str(data):
            ret = self.app_client.get_app_status(self.k8s_namespace, data['app_name'], 'PartialRunning')
            assert ret, '更新应用后，应用状态不是部分运行'
        else:
            ret = self.app_client.get_app_status(self.k8s_namespace, data['app_name'], 'Running', expect_cnt=100)
            assert ret, '更新应用后，应用状态不是运行中'

    @pytest.mark.skip
    def 测试更新应用_全部参数_信息正确(self):
        pass

    def 测试更新应用事件(self):
        payload = {"cluster": REGION_NAME,
                   "kind": "Deployment,Application,Pod,EndPoint,Service,HorizontalPodAutoscaler,ReplicaSet,DaemonSet,StatefulSet",
                   "name": l1_update_data[0]["app_name"], "namespace": K8S_NAMESPACE, "page": 1, "page_size": 100}
        kevents_results = self.app_client.get_kevents(payload=payload, expect_value="SuccessfulCreate", times=2)
        assert kevents_results, "获取更新应用事件失败"

    @pytest.mark.metis
    def 测试停止应用(self):
        ret = self.app_client.stop_app(self.k8s_namespace, l1_update_data[0]["app_name"])
        assert ret.status_code == 204, "停止应用失败:{}".format(ret.text)
        ret = self.app_client.get_app_status(self.k8s_namespace, l1_update_data[0]["app_name"], 'Stopped')
        assert ret, '停止应用后，应用状态不是已停止'

    def 测试停止应用事件(self):
        payload = {"cluster": REGION_NAME,
                   "kind": "Deployment,Application,Pod,EndPoint,Service,HorizontalPodAutoscaler,ReplicaSet,DaemonSet,StatefulSet",
                   "name": l1_update_data[0]["app_name"], "namespace": K8S_NAMESPACE, "page": 1, "page_size": 100}
        kevents_results = self.app_client.get_kevents(payload=payload, expect_value="SuccessfulDelete", times=2)
        assert kevents_results, "获取停止应用事件失败"

    @pytest.mark.metis
    def 测试开始应用(self):
        ret = self.app_client.start_app(self.k8s_namespace, l1_update_data[0]["app_name"])
        assert ret.status_code == 204, "停止应用失败:{}".format(ret.text)
        ret = self.app_client.get_app_status(self.k8s_namespace, l1_update_data[0]["app_name"], 'Running')
        assert ret, '开始应用后，应用状态不是运行中'

    def 测试开始应用事件(self):
        payload = {"cluster": REGION_NAME,
                   "kind": "Deployment,Application,Pod,EndPoint,Service,HorizontalPodAutoscaler,ReplicaSet,DaemonSet,StatefulSet",
                   "name": l1_update_data[0]["app_name"], "namespace": K8S_NAMESPACE, "page": 1, "page_size": 100}
        kevents_results = self.app_client.get_kevents(payload=payload, expect_value="SuccessfulCreate", times=2)
        assert kevents_results, "获取开始应用事件失败"

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 测试应用更新审计(self):
        payload = {"user_name": USERNAME, "operation_type": "update", "resource_type": "applications",
                   "resource_name": l1_update_data[0]['app_name']}
        result = self.app_client.search_audit(payload)
        payload.update({"namespace": self.k8s_namespace, "region_name": REGION_NAME})
        values = self.app_client.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.app_client.is_sub_dict(values, result.json()), "审计数据不符合预期"

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 测试应用打补丁审计(self):
        payload = {"user_name": USERNAME, "operation_type": "patch", "resource_type": "applications",
                   "resource_name": l1_update_data[0]['app_name']}
        result = self.app_client.search_audit(payload)
        payload.update({"namespace": self.k8s_namespace, "region_name": REGION_NAME})
        values = self.app_client.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.app_client.is_sub_dict(values, result.json()), "审计数据不符合预期"
