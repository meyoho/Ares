from common.settings import RESOURCE_PREFIX, IMAGE, PROJECT_NAME, K8S_NAMESPACE, SC_NAME, CASE_TYPE
import pytest
import base64
from new_case.containerplatform.secret.secret import Secret
from new_case.containerplatform.configmap.configmap import Configmap
from new_case.containerplatform.persistentvolumeclaims.pvc import Pvc

l0_data = {
    'project_name': PROJECT_NAME,
    'namespace': K8S_NAMESPACE,
    'app_name': "{}-ares-acp-application".format(RESOURCE_PREFIX),
    'deployment_name': "{}-ares-acp-application-dep".format(RESOURCE_PREFIX),
    'image': IMAGE,
    'replicas': 1
}
l0_data_update = {
    'project_name': PROJECT_NAME,
    'namespace': K8S_NAMESPACE,
    'app_name': l0_data['app_name'],
    'display_name': "{}-ares-acp-app".format(RESOURCE_PREFIX),
    'deployment_name': l0_data['deployment_name'],
    'image': IMAGE,
    'replicas': 1,
    'file_log_path': "/var/*.*",
    'exclude_log_path': "/var/hehe.txt",
    'env': True,
    'livenessProbe': True,
    'readinessProbe': True,
    'service_name': "{}-ares-acp-app-svc".format(RESOURCE_PREFIX),
}
l0_hpa = {
    'project_name': PROJECT_NAME,
    'namespace': K8S_NAMESPACE,
    'app_name': l0_data_update['app_name'],
    'deployment_name': l0_data_update['deployment_name'],
    'service_name': l0_data_update['service_name'],
    'image': IMAGE,
    'replicas': 1,
    'max': 2,
    'min': 1,
    'percent': 10
}
l0_hpa_update = {
    'project_name': PROJECT_NAME,
    'namespace': K8S_NAMESPACE,
    'app_name': l0_data_update['app_name'],
    'deployment_name': l0_data_update['deployment_name'],
    'service_name': l0_data_update['service_name'],
    'image': IMAGE,
    'replicas': 1,
    'max': 1,
    'min': 1,
    'percent': 100
}
l0_workload_update = {
    'project_name': PROJECT_NAME,
    'namespace': K8S_NAMESPACE,
    'app_name': l0_data_update['app_name'],
    'deployment_name': l0_data_update['deployment_name'],
    'image': IMAGE,
    'replicas': 2
}
l0_workload_delete = {
    'project_name': PROJECT_NAME,
    'namespace': K8S_NAMESPACE,
    'app_name': l0_data_update['app_name'],
}
cm_info = {
    "configmap_name": "{}-ares-app-cm".format(RESOURCE_PREFIX),
    "description": "key-value",
    "namespace": K8S_NAMESPACE,
    "configmap_key": "key",
    "configmap_value": "value"
}
secret_info = {
    "secret_name": "{}-ares-app-secret".format(RESOURCE_PREFIX),
    "secret_type": "Opaque",
    "opaque_key": "opaque_key",
    "opaque_value": str(base64.b64encode("opaque_value".encode('utf-8')), 'utf8'),
    "K8S_NAMESPACE": K8S_NAMESPACE
}
pvc_name = "{}-ares-app-pvc".format(RESOURCE_PREFIX)
pvc_info = {
    "pvc_name": pvc_name,
    "namespace": K8S_NAMESPACE,
    "storageclassname": SC_NAME
}
create_application_casename = ["daemonset类型挂headless内部路由", "statefulset类型挂nodeport内部路由和访问规则", "空应用",
                               "包含两个deployment", "必填参数-可选参数只有1个(容器组标签)-信息正确", "必填参数-可选参数只有1个(配置引用)-信息正确",
                               "必填参数-可选参数只有1个(主机选择器)-信息正确", "必填参数-可选参数只有1个(亲和性)-信息正确"]
l1_data = [
    {
        "app_name": "{}-ares-acp-app-ds".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "resources": [
            {
                "deploy_name": "{}-ares-acp-app-ds-name".format(RESOURCE_PREFIX),
                "kind": 'DaemonSet',
                "ns_name": K8S_NAMESPACE,
                "project": PROJECT_NAME,
                "app_name": "{}-ares-acp-app-ds".format(RESOURCE_PREFIX),
                "containers": [
                    {
                        "image": IMAGE,
                    }
                ],
            },
            {
                "service_name": "{}-ares-acp-app-ds-svc".format(RESOURCE_PREFIX),
                "ns_name": K8S_NAMESPACE,
                "app_name": "{}-ares-acp-app-ds".format(RESOURCE_PREFIX),
                "deploy_name": "{}-ares-acp-app-ds-name".format(RESOURCE_PREFIX),
                "headless": True,
                "sessionAffinity": "None",
                "ports": [
                    {
                        "protocol": "TCP",
                        "port": 80,
                        "targetPort": 80
                    }
                ]
            },
        ]
    },
    {
        "app_name": "{}-ares-acp-app-st".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "resources": [
            {
                "deploy_name": "{}-ares-acp-app-st-name".format(RESOURCE_PREFIX),
                "kind": 'StatefulSet',
                "ns_name": K8S_NAMESPACE,
                "project": PROJECT_NAME,
                "app_name": "{}-ares-acp-app-st".format(RESOURCE_PREFIX),
                "containers": [
                    {
                        "image": IMAGE,
                    }
                ],
            },
            {
                "service_name": "{}-ares-acp-app-st-svc".format(RESOURCE_PREFIX),
                "ns_name": K8S_NAMESPACE,
                "app_name": "{}-ares-acp-app-st".format(RESOURCE_PREFIX),
                "deploy_name": "{}-ares-acp-app-st-name".format(RESOURCE_PREFIX),
                "service_type": "NodePort",
                "sessionAffinity": "ClientIP",
                "ports": [
                    {
                        "protocol": "TCP",
                        "port": 80,
                        "targetPort": 80
                    }
                ]
            },
            {
                "ingress_name": "{}-ares-acp-app-st-ingress".format(RESOURCE_PREFIX),
                "ns_name": K8S_NAMESPACE,
                "host": '.'.join(["{}-ares-acp-app-st".format(RESOURCE_PREFIX), K8S_NAMESPACE]),
                "url_path": '/',
                "service_name": "{}-ares-acp-app-st-svc".format(RESOURCE_PREFIX),
                "service_port": 80
            }
        ]
    },
    {
        "app_name": "{}-ares-acp-app-none".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "resources": [
        ]
    },
    {
        "app_name": "{}-ares-acp-app-many".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "resources": [
            {
                "deploy_name": "{}-ares-acp-app-many-st".format(RESOURCE_PREFIX),
                "kind": 'StatefulSet',
                "ns_name": K8S_NAMESPACE,
                "project": PROJECT_NAME,
                "app_name": "{}-ares-acp-app-many".format(RESOURCE_PREFIX),
                "containers": [
                    {
                        "image": IMAGE,
                    }
                ]
            },
            {
                "deploy_name": "{}-ares-acp-app-many-dep".format(RESOURCE_PREFIX),
                "kind": 'Deployment',
                "replicas": 0,
                "ns_name": K8S_NAMESPACE,
                "project": PROJECT_NAME,
                "labels": {"ares": "test"},
                "app_name": "{}-ares-acp-app-many".format(RESOURCE_PREFIX),
                "containers": [
                    {
                        "image": IMAGE
                    }
                ]
            }
        ]
    },
    {
        "app_name": "{}-ares-acp-app-lb".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "resources": [
            {
                "deploy_name": "{}-ares-acp-app-lb-d".format(RESOURCE_PREFIX),
                "ns_name": K8S_NAMESPACE,
                "project": PROJECT_NAME,
                "app_name": "{}-ares-acp-app-lb".format(RESOURCE_PREFIX),
                "containers": [
                    {
                        "image": IMAGE,
                    }
                ],
                "labels": {
                    "test1": "value1"
                }
            }
        ]
    },
    {
        "app_name": "{}-ares-acp-app-envf".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "resources": [
            {
                "deploy_name": "{}-ares-acp-app-envf-d".format(RESOURCE_PREFIX),
                "ns_name": K8S_NAMESPACE,
                "project": PROJECT_NAME,
                "app_name": "{}-ares-acp-app-envf".format(RESOURCE_PREFIX),
                "containers": [
                    {
                        "image": IMAGE,
                        "envFroms": [{
                            "configMapRef": {
                                "name": cm_info["configmap_name"]
                            }
                        }]
                    }
                ],

            }
        ]
    },
    {
        "app_name": "{}-ares-acp-app-nose".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "resources": [
            {
                "deploy_name": "{}-ares-acp-app-nose-d".format(RESOURCE_PREFIX),
                "ns_name": K8S_NAMESPACE,
                "project": PROJECT_NAME,
                "app_name": "{}-ares-acp-app-nose".format(RESOURCE_PREFIX),
                "containers": [
                    {
                        "image": IMAGE,
                    }
                ],
                "nodeSelector": {
                    "beta.kubernetes.io/os": "linux"
                }
            }
        ]
    },
    {
        "app_name": "{}-ares-acp-app-aff".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "resources": [
            {
                "deploy_name": "{}-ares-acp-app-aff-d".format(RESOURCE_PREFIX),
                "ns_name": K8S_NAMESPACE,
                "project": PROJECT_NAME,
                "app_name": "{}-ares-acp-app-aff".format(RESOURCE_PREFIX),
                "containers": [
                    {
                        "image": IMAGE,
                    }
                ],
                "affinity_match_labels": {
                    "app": "{}-{}-ares-acp-app-nose-d".format("deployment", RESOURCE_PREFIX)
                }
            }
        ]
    }
]
use_pvc_data = [
    {
        "app_name": "{}-ares-acp-app-all-para".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "resources": [
            {
                "deploy_name": "{}-ares-acp-app-all-para-dep".format(RESOURCE_PREFIX),
                "kind": 'Deployment',
                "replicas": 2,
                "ns_name": K8S_NAMESPACE,
                "project": PROJECT_NAME,
                "labels": {"ares": "test"},
                "maxSurge": 2,
                "maxUnavailable": 0,
                "volumes": [
                    {
                        "name": "pvc-{}".format(pvc_name),
                        "persistentVolumeClaim": {
                            "claimName": pvc_name
                        }
                    }
                ],
                "app_name": "{}-ares-acp-app-all-para".format(RESOURCE_PREFIX),
                "nodeSelector": {"beta.kubernetes.io/arch": "amd64"},
                "containers": [
                    {
                        "image": IMAGE,
                        "commands": ["/run.sh"],
                        "args": ["/bin/sh"],
                        "envs": [
                            {
                                "name": "ares-env1",
                                "value": "test"
                            },
                            {
                                "name": "__FILE_LOG_PATH__",
                                "value": "/var/*.txt"
                            },
                            {
                                "name": "__EXCLUDE_LOG_PATH__",
                                "value": "/var/hehe.txt"
                            },
                            {
                                "name": "ares-env2",
                                "valueFrom": {
                                    "configMapKeyRef": {
                                        "name": cm_info["configmap_name"],
                                        "key": cm_info["configmap_key"]
                                    }
                                }
                            },
                            {
                                "name": "ares-env3",
                                "valueFrom": {
                                    "secretKeyRef": {
                                        "name": secret_info["secret_name"],
                                        "key": secret_info["opaque_key"]
                                    }
                                }
                            }
                        ],
                        "volumeMounts": [
                            {
                                "name": "pvc-{}".format(pvc_name),
                                "mountPath": "/tmp/"
                            }
                        ],
                        "liveness_protocol": "exec",
                        "liveness_exec_commands": ["ls", "/"],
                        "readiness_protocol": "httpGet",
                        "readiness_http_headers": {"a": "b"}
                    }
                ]
            }
        ]
    }
]
use_pvc_data_name = ["deployment类型-两个实例-挂载pvc存储-指定启动命令-添加环境变量-添加日志文件和排除日志文件-挂载configmap和secret-添加健康检查"]
# if SC_NAME:
#     l1_data.extend(use_pvc_data)
update_application_casename = ["更新statefulset类型-添加日志文件和排除日志文件-指定启动命令-添加健康检查-添加nodeport内部路由和ingress",
                               "更新deployment类型两个实例", "必填参数-可选参数只有1个(配置引用)-信息正确",
                               "必填参数-可选参数只有1个(主机选择器)-信息正确", "必填参数-可选参数只有1个(亲和性)-信息正确"]
l1_update_data = [
    {
        "app_name": "{}-ares-acp-app-st".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "resources": [
            {
                "deploy_name": "{}-ares-acp-app-st-name".format(RESOURCE_PREFIX),
                "kind": 'StatefulSet',
                "ns_name": K8S_NAMESPACE,
                "project": PROJECT_NAME,
                "app_name": "{}-ares-acp-app-st".format(RESOURCE_PREFIX),
                "containers": [
                    {
                        "image": IMAGE,
                        "envs": [
                            {
                                "name": "__FILE_LOG_PATH__",
                                "value": "/var/*.txt"
                            },
                            {
                                "name": "__EXCLUDE_LOG_PATH__",
                                "value": "/var/hehe.txt"
                            }
                        ],
                        "commands": ["/run.sh"],
                        "args": ["/bin/sh"],
                        "liveness_protocol": "exec",
                        "liveness_exec_commands": ["ls", "/"],
                        "readiness_protocol": "httpGet",
                        "readiness_http_headers": {"a": "b"}
                    }
                ],
            },
            {
                "service_name": "{}-ares-acp-app-st-svc".format(RESOURCE_PREFIX),
                "ns_name": K8S_NAMESPACE,
                "app_name": "{}-ares-acp-app-st".format(RESOURCE_PREFIX),
                "deploy_name": "{}-ares-acp-app-st-name".format(RESOURCE_PREFIX),
                "service_type": "NodePort",
                "sessionAffinity": "ClientIP",
                "ports": [
                    {
                        "protocol": "TCP",
                        "port": 80,
                        "targetPort": 80
                    }
                ]
            },
            {
                "ingress_name": "{}-ares-acp-app-st-ingress".format(RESOURCE_PREFIX),
                "ns_name": K8S_NAMESPACE,
                "host": '.'.join(["{}-ares-acp-app-st".format(RESOURCE_PREFIX), K8S_NAMESPACE]),
                "url_path": '/',
                "service_name": "{}-ares-acp-app-st-svc".format(RESOURCE_PREFIX),
                "service_port": 80
            }
        ]
    },
    {
        "app_name": "{}-ares-acp-app-lb".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "resources": [
            {
                "deploy_name": "{}-ares-acp-app-lb-d".format(RESOURCE_PREFIX),
                "ns_name": K8S_NAMESPACE,
                "project": PROJECT_NAME,
                "app_name": "{}-ares-acp-app-lb".format(RESOURCE_PREFIX),
                "containers": [
                    {
                        "image": IMAGE,
                    }
                ],
                "labels": {
                    "test2": "value2"
                }
            }
        ]
    },
    {
        "app_name": "{}-ares-acp-app-envf".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "resources": [
            {
                "deploy_name": "{}-ares-acp-app-envf-d".format(RESOURCE_PREFIX),
                "ns_name": K8S_NAMESPACE,
                "project": PROJECT_NAME,
                "app_name": "{}-ares-acp-app-envf".format(RESOURCE_PREFIX),
                "containers": [
                    {
                        "image": IMAGE,
                        "envFroms": [{
                            "secretRef": {
                                "name": secret_info["secret_name"]
                            }
                        }]
                    }
                ],

            }
        ]
    },
    {
        "app_name": "{}-ares-acp-app-nose".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "resources": [
            {
                "deploy_name": "{}-ares-acp-app-nose-d".format(RESOURCE_PREFIX),
                "ns_name": K8S_NAMESPACE,
                "project": PROJECT_NAME,
                "app_name": "{}-ares-acp-app-nose".format(RESOURCE_PREFIX),
                "containers": [
                    {
                        "image": IMAGE,
                    }
                ],
                "nodeSelector": {
                    "node-role.kubernetes.io/node": ""
                }
            }
        ]
    },
    {
        "app_name": "{}-ares-acp-app-aff".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "resources": [
            {
                "deploy_name": "{}-ares-acp-app-aff-d".format(RESOURCE_PREFIX),
                "ns_name": K8S_NAMESPACE,
                "project": PROJECT_NAME,
                "app_name": "{}-ares-acp-app-aff".format(RESOURCE_PREFIX),
                "containers": [
                    {
                        "image": IMAGE,
                    }
                ],
                "affinity_match_labels": {
                    "app": "{}-{}-ares-acp-app-nose-d".format("deployment", RESOURCE_PREFIX)
                },
                "affinity": "Anti",
                "affinity_type": "preferred"
            }
        ]
    }
]
use_pvc_update_data = [
    {
        "app_name": "{}-ares-acp-app-all-para".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "resources": [
            {
                "deploy_name": "{}-ares-acp-app-all-para-dep".format(RESOURCE_PREFIX),
                "kind": 'Deployment',
                "replicas": 2,
                "ns_name": K8S_NAMESPACE,
                "labels": {"ares": "test"},
                "project": PROJECT_NAME,
                "maxSurge": 2,
                "maxUnavailable": 0,
                "app_name": "{}-ares-acp-app-all-para".format(RESOURCE_PREFIX),
                "nodeSelector": {"node-role.kubernetes.io/master": ""},
                "containers": [
                    {
                        "image": IMAGE,
                    }
                ]
            }
        ]
    },
    {
        "app_name": "{}-ares-acp-app-ds".format(RESOURCE_PREFIX),
        "ns_name": K8S_NAMESPACE,
        "resources": [
            {
                "deploy_name": "{}-ares-acp-app-ds-name".format(RESOURCE_PREFIX),
                "kind": 'DaemonSet',
                "ns_name": K8S_NAMESPACE,
                "project": PROJECT_NAME,
                "maxUnavailable": 5,
                "volumes": [
                    {
                        "name": "pvc-{}".format(pvc_name),
                        "persistentVolumeClaim": {
                            "claimName": pvc_name
                        }
                    }
                ],
                "app_name": "{}-ares-acp-app-ds".format(RESOURCE_PREFIX),
                "containers": [
                    {
                        "image": IMAGE,
                        "volumeMounts": [
                            {
                                "name": "pvc-{}".format(pvc_name),
                                "mountPath": "/tmp/"
                            }
                        ],
                        "envs": [
                            {
                                "name": "ares-env1",
                                "value": "test"
                            },
                            {
                                "name": "ares-env2",
                                "valueFrom": {
                                    "configMapKeyRef": {
                                        "name": cm_info["configmap_name"],
                                        "key": cm_info["configmap_key"]
                                    }
                                }
                            },
                            {
                                "name": "ares-env3",
                                "valueFrom": {
                                    "secretKeyRef": {
                                        "name": secret_info["secret_name"],
                                        "key": secret_info["opaque_key"]
                                    }
                                }
                            }
                        ]
                    }
                ]
            }
        ]
    },
]
use_pvc_update_data_name = ["必填参数-可选参数只有1个(容器组标签)-信息正确", "更新daemonset类型-最多不可用数五个-挂载pvc存储-添加环境变量-挂载configmap和secret"]

if SC_NAME:
    l1_data.extend(use_pvc_data)
    create_application_casename.extend(use_pvc_data_name)
    l1_update_data.extend(use_pvc_update_data)
    update_application_casename.extend(use_pvc_update_data_name)


@pytest.fixture(scope="session", autouse=True)
def prepare():
    pvc = Pvc()
    secret_tool = Secret()
    cm_tool = Configmap()
    if CASE_TYPE not in ("delete", "upgrade"):
        try:
            ret = secret_tool.create_secret('./test_data/secret/create_secret.jinja2', data=secret_info)
            assert ret.status_code in (201, 409), "创建{}类型保密字典失败:{}".format(secret_info['secret_type'], ret.text)
            ret = secret_tool.check_exists(
                secret_tool.get_secret_url(secret_tool.k8s_namespace, secret_info['secret_name']), 200, expect_cnt=2)
            assert ret, "创建保密字典后检查不到该保密字典"

            ret = cm_tool.create_configmap(data=cm_info)
            assert ret.status_code in (201, 409), "创建configmap失败:{}".format(ret.text)
            ret = cm_tool.check_exists(
                cm_tool.get_common_configmap_url(cm_info['configmap_name'], secret_tool.k8s_namespace), 200,
                expect_cnt=2)
            assert ret, "创建cm后检查不到该cm"
            create_result = pvc.create_pvc_jinja2("./test_data/pvc/pvc.jinja2", data=pvc_info)
            assert create_result.status_code in (201, 409), "创建pvc:{}失败{}".format(pvc_name, create_result.text)
            pvc.get_status(pvc.get_common_pvc_url_v1(pvc_name=pvc_info["pvc_name"]), "status.phase", "Bound")
        except Exception as e:
            secret_tool.delete_secret(secret_info["secret_name"])
            cm_tool.delete_configmap(cm_info["configmap_name"])
            pvc.delete_pvc(K8S_NAMESPACE, pvc_name)
            raise AssertionError(e)
    yield
    if CASE_TYPE not in ("prepare", "upgrade"):
        secret_tool.delete_secret(secret_info["secret_name"])
        cm_tool.delete_configmap(cm_info["configmap_name"])
        pvc.delete_pvc(K8S_NAMESPACE, pvc_name)
