import pytest

from common.settings import RERUN_TIMES, K8S_NAMESPACE
from new_case.containerplatform.application.application import Application
from new_case.containerplatform.application.conftest import l0_data, l0_data_update, l0_workload_update, l0_hpa_update, \
    l0_hpa, l0_workload_delete
from new_case.containerplatform.application.workload import Wordload
from new_case.containerplatform.resourcemanage.resource import Resource


@pytest.mark.archon
@pytest.mark.BAT
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestApp(object):

    def setup_class(self):
        self.app_client = Application()
        self.k8s_namespace = K8S_NAMESPACE
        self.app_name = l0_data['app_name']
        self.resource_client = Resource()
        self.deployment_client = Wordload()

    @pytest.mark.metis
    @pytest.mark.prepare
    def 测试创建应用(self):
        # 偶现 archon版本不对 导致 删除应用时旗下资源未删除，先删掉
        self.resource_client.delete_resource(self.k8s_namespace, 'deployments', l0_data_update['deployment_name'])
        self.deployment_client.check_exists(
            self.deployment_client.common_workload_url(self.k8s_namespace, l0_hpa['deployment_name']), 404)
        self.resource_client.delete_resource(self.k8s_namespace, 'services', l0_data_update['service_name'])
        self.app_client.check_exists(
            self.resource_client.common_resource_in_namespace_url(self.k8s_namespace, 'services',
                                                                  l0_hpa['service_name']), 404)
        self.app_client.delete_app(self.k8s_namespace, self.app_name)
        self.app_client.check_exists(self.app_client.common_app_url(self.k8s_namespace, self.app_name), 404)

        ret = self.app_client.create_app('./test_data/application/create_app.jinja2', l0_data)
        assert ret.status_code == 200, "创建应用失败:{}".format(ret.text)
        values = self.app_client.generate_jinja_data("verify_data/application/create_response.jinja2", l0_data)
        assert self.app_client.is_sub_dict(values, ret.json()), \
            "创建应用比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)
        ret = self.app_client.get_app_status(self.k8s_namespace, self.app_name, 'Running')
        assert ret, '创建应用后，应用状态不是运行中'

    @pytest.mark.upgrade
    def 测试应用列表(self):
        ret = self.app_client.list_app(self.k8s_namespace)
        assert ret.status_code == 200, "获取应用列表失败:{}".format(ret.text)
        content = self.app_client.get_k8s_resource_data(ret.json(), self.app_name, list_key="items")
        values = self.app_client.generate_jinja_data("verify_data/application/detail_response.jinja2", l0_data)
        assert self.app_client.is_sub_dict(values[0], content), \
            "获取应用列表比对数据失败，返回数据{}，期望数据{}".format(content, values)

    @pytest.mark.upgrade
    def 测试获取访问地址(self):
        ret = self.app_client.get_app_address(self.k8s_namespace, self.app_name)
        assert ret.status_code == 200, "获取访问地址失败:{}".format(ret.text)
        values = self.app_client.generate_jinja_data("verify_data/application/address_response.jinja2", l0_data)
        assert self.app_client.is_sub_dict(values, ret.json()), \
            "获取访问地址比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)

    @pytest.mark.upgrade
    def 测试获取容器组(self):
        ret = self.app_client.get_app_pod(self.k8s_namespace, self.app_name)
        assert ret.status_code == 200, "获取容器组失败:{}".format(ret.text)
        global pod_name
        pod_name = self.app_client.get_value(ret.json(), 'items.0.metadata.name')
        global container_name
        container_name = self.app_client.get_value(ret.json(), 'items.0.spec.containers.0.name')
        l0_data.update({'pod_name': pod_name})
        values = self.app_client.generate_jinja_data("verify_data/application/pod_response.jinja2", l0_data)
        assert self.app_client.is_sub_dict(values, ret.json()), \
            "获取容器组比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)

    def not测试获取拓扑(self):
        # http://jira.alaudatech.com/browse/ACP-434
        ret = self.app_client.get_app_topology(self.k8s_namespace, self.app_name)
        assert ret.status_code == 200, "获取拓扑失败:{}".format(ret.text)
        length = len(ret.json()['nodes'])
        assert length == 2, "获取拓扑失败,应该只有 2 个节点，返回数据{}".format(ret.json())
        content = list(ret.json()['nodes'].values())
        values = self.app_client.generate_jinja_data("verify_data/application/topolopy_response.jinja2", l0_data)
        assert self.app_client.is_sub_list(values, content), \
            "获取拓扑比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)

    @pytest.mark.upgrade
    def 测试获取日志(self):
        ret = self.app_client.get_app_logs(self.k8s_namespace, pod_name, container_name)
        assert ret.status_code == 200, "获取日志失败:{}".format(ret.text)
        l0_data.update({'pod_name': pod_name, 'container_name': container_name})
        values = self.app_client.generate_jinja_data("verify_data/application/log_response.jinja2", l0_data)
        assert self.app_client.is_sub_dict(values, ret.json()), \
            "获取日志比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)
        assert len(self.app_client.get_value(ret.json(), 'logs')) != 0, "获取日志为空 {}".format(ret.json())

    @pytest.mark.upgrade
    def 测试容器重建(self):
        ret = self.app_client.delete_pod(self.k8s_namespace, pod_name)
        assert ret.status_code == 200, "容器重建失败:{}".format(ret.text)
        l0_data.update({'pod_name': pod_name})
        values = self.app_client.generate_jinja_data("verify_data/application/delete_pod_response.jinja2", l0_data)
        assert self.app_client.is_sub_dict(values, ret.json()), \
            "重建容器比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)

        ret = self.app_client.get_app_status(self.k8s_namespace, self.app_name, 'Running')
        assert ret, '创建应用后，应用状态不是运行中'

    @pytest.mark.metis
    @pytest.mark.upgrade
    def 测试更新应用(self):
        ret = self.app_client.detail_app(self.k8s_namespace, self.app_name)
        assert ret.status_code == 200, "获取应用详情失败:{}".format(ret.text)
        resourceVersion = self.app_client.get_value(ret.json()[0], "metadata.resourceVersion")
        l0_data_update.update({'resourceVersion': resourceVersion})
        ret = self.app_client.update_app(self.k8s_namespace, self.app_name, 'test_data/application/create_app.jinja2',
                                         l0_data_update)
        assert ret.status_code == 200, "更新应用失败:{}".format(ret.text)
        values = self.app_client.generate_jinja_data("verify_data/application/create_response.jinja2", l0_data_update)
        assert self.app_client.is_sub_dict(values, ret.json()), \
            "更新应用比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)
        ret = self.app_client.get_app_status(self.k8s_namespace, self.app_name, 'Running')
        assert ret, '更新应用后，应用状态不是运行中'

    @pytest.mark.upgrade
    def 测试应用详情(self):
        ret = self.app_client.detail_app(self.k8s_namespace, self.app_name)
        assert ret.status_code == 200, "获取应用详情失败:{}".format(ret.text)
        values = self.app_client.generate_jinja_data("verify_data/application/detail_response.jinja2", l0_data_update)
        assert self.app_client.is_sub_list(values, ret.json()), \
            "获取应用详情比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)

    @pytest.mark.upgrade
    def 测试回滚应用(self):
        ret = self.app_client.rollback_app(self.k8s_namespace, self.app_name, 1, 'ares')
        assert ret.status_code == 204, "回滚应用失败:{}".format(ret.text)
        ret = self.app_client.get_app_status(self.k8s_namespace, self.app_name, 'Running')
        assert ret, '回滚应用后，应用状态不是运行中'
        ret = self.app_client.detail_app(self.k8s_namespace, self.app_name)
        assert ret.status_code == 200, "获取应用详情失败:{}".format(ret.text)
        values = self.app_client.generate_jinja_data("verify_data/application/detail_response.jinja2", l0_data)
        assert self.app_client.is_sub_dict(values, ret.json()), \
            "回滚应用后，获取应用详情比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)

    @pytest.mark.upgrade
    def 测试历史版本列表(self):
        ret = self.app_client.get_app_histories(self.k8s_namespace, self.app_name)
        assert ret.status_code == 200, "获取应用历史版本失败:{}".format(ret.text)
        assert len(ret.json()['items']) == 3, "获取应用历史版本失败,应该三个,实际:{}".format(len(ret.json()['items']))
        values = self.app_client.generate_jinja_data("verify_data/application/history_response.jinja2", l0_data_update)
        assert self.app_client.is_sub_list(values, ret.json()['items']), \
            "获取历史版本比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)

    @pytest.mark.upgrade
    def 测试历史版本详情(self):
        ret = self.app_client.detail_histories(self.k8s_namespace, self.app_name + '-1')
        assert ret.status_code == 200, "获取应用历史版本详情失败:{}".format(ret.text)
        values = self.app_client.generate_jinja_data("verify_data/application/history_response.jinja2", l0_data_update)
        assert self.app_client.is_sub_dict(values[0], ret.json()), \
            "获取历史版本详情比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)

    @pytest.mark.upgrade
    def 测试告警列表(self):
        ret = self.app_client.list_alert(self.k8s_namespace, self.app_name)
        assert ret.status_code == 200, "获取应用告警列表失败:{}".format(ret.text)
        values = self.app_client.generate_jinja_data("verify_data/application/alert_response.jinja2", l0_data_update)
        assert self.app_client.is_sub_dict(values, ret.json()), \
            "获取告警列表比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)

    def not测试部署拓扑(self):
        # http://jira.alaudatech.com/browse/ACP-434
        ret = self.deployment_client.get_workload_topology(self.k8s_namespace, l0_hpa['deployment_name'])
        assert ret.status_code == 200, "获取部署拓扑失败:{}".format(ret.text)
        length = len(ret.json()['nodes'])
        assert length == 3, "获取拓扑失败,应该只有 3 个节点，返回数据{}".format(ret.json())
        content = list(ret.json()['nodes'].values())
        values = self.deployment_client.generate_jinja_data("verify_data/application/topolopy_response.jinja2",
                                                            l0_hpa)
        assert self.deployment_client.is_sub_list(values, content), \
            "获取部署拓扑比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)

    @pytest.mark.prepare
    def 测试创建自动扩缩容(self):
        ret = self.deployment_client.create_hpa(self.k8s_namespace, './test_data/application/create_hpa.jinja2', l0_hpa)
        assert ret.status_code == 201, "创建自动扩缩容失败:{}".format(ret.text)
        values = self.deployment_client.generate_jinja_data("verify_data/application/hpa_response.jinja2", l0_hpa)
        assert self.deployment_client.is_sub_dict(values, ret.json()), \
            "创建自动扩缩容比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)

    @pytest.mark.upgrade
    def 测试自动扩缩容详情(self):
        ret = self.deployment_client.get_hpa(self.k8s_namespace, l0_hpa['deployment_name'])
        assert ret.status_code == 200, "自动扩缩容详情失败:{}".format(ret.text)
        values = self.deployment_client.generate_jinja_data("verify_data/application/hpa_response.jinja2", l0_hpa)
        assert self.deployment_client.is_sub_dict(values, ret.json()), \
            "自动扩缩容详情比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)

    @pytest.mark.upgrade
    def 测试更新自动扩缩容(self):
        ret = self.deployment_client.update_hpa(self.k8s_namespace, l0_hpa['deployment_name'],
                                                './test_data/application/create_hpa.jinja2', l0_hpa_update)

        assert ret.status_code == 200, "更新自动扩缩容失败:{}".format(ret.text)
        values = self.deployment_client.generate_jinja_data("verify_data/application/hpa_response.jinja2",
                                                            l0_hpa_update)
        assert self.deployment_client.is_sub_dict(values, ret.json()), \
            "更新自动扩缩容比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)

    @pytest.mark.delete
    def 测试删除自动扩缩容(self):
        ret = self.deployment_client.delete_hpa(self.k8s_namespace, l0_hpa['deployment_name'])
        assert ret.status_code == 200, "删除自动扩缩容失败:{}".format(ret.text)
        values = self.deployment_client.generate_jinja_data("verify_data/application/delete_hpa_response.jinja2",
                                                            l0_hpa_update)
        assert self.deployment_client.is_sub_dict(values, ret.json()), \
            "删除自动扩缩容比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)
        delete_flag = self.deployment_client.check_exists(
            self.deployment_client.common_hpa_url(self.k8s_namespace, l0_hpa['deployment_name']), 404)
        assert delete_flag, "删除自动扩缩容失败"

    @pytest.mark.upgrade
    def 测试更新部署(self):
        ret = self.app_client.detail_app(self.k8s_namespace, self.app_name)
        assert ret.status_code == 200, "获取应用详情失败:{}".format(ret.text)
        resourceVersion = self.app_client.get_value(ret.json()[0], "metadata.resourceVersion")
        l0_workload_update.update({'resourceVersion': resourceVersion})
        ret = self.app_client.update_app(self.k8s_namespace, l0_workload_update['app_name'],
                                         './test_data/application/create_app.jinja2', l0_workload_update)

        assert ret.status_code == 200, "更新应用失败:{}".format(ret.text)
        values = self.app_client.generate_jinja_data("verify_data/application/create_response.jinja2",
                                                     l0_workload_update)
        assert self.app_client.is_sub_dict(values, ret.json()), \
            "更新部署比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)
        ret = self.app_client.get_app_status(self.k8s_namespace, self.app_name, 'Running')
        assert ret, '更新部署后，应用状态不是运行中'
        delete_flag = self.app_client.check_exists(
            self.resource_client.common_resource_in_namespace_url(self.k8s_namespace, 'services',
                                                                  l0_hpa['service_name']), 404)
        assert delete_flag, "更新部署后，关联的service没有删除"

    @pytest.mark.upgrade
    def 测试创建版本快照有版本注释(self):
        app_history = self.app_client.get_app_histories(self.k8s_namespace, self.app_name)
        ret = self.app_client.add_snapshot(self.app_name, '测试创建版本快照')
        assert ret.status_code == 204, "创建应用版本快照失败: {}".format(ret.text)
        app_history_1 = self.app_client.get_app_histories(self.k8s_namespace, self.app_name)
        assert len(app_history_1.json().get("items")) == len(app_history.json().get("items")) + 1, '创建应用版本快照失败'

    @pytest.mark.upgrade
    def 测试创建版本快照版本注释为空(self):
        app_history = self.app_client.get_app_histories(self.k8s_namespace, self.app_name)
        ret = self.app_client.add_snapshot(self.app_name)
        assert ret.status_code == 204, "创建应用版本快照失败: {}".format(ret.text)
        app_history_1 = self.app_client.get_app_histories(self.k8s_namespace, self.app_name)
        assert len(app_history_1.json().get("items")) == len(app_history.json().get("items")) + 1, '创建应用版本快照失败'

    @pytest.mark.delete
    def 测试删除部署(self):
        ret = self.app_client.detail_app(self.k8s_namespace, self.app_name)
        assert ret.status_code == 200, "获取应用详情失败:{}".format(ret.text)
        resourceVersion = self.app_client.get_value(ret.json()[0], "metadata.resourceVersion")
        l0_workload_update.update({'resourceVersion': resourceVersion})
        ret = self.app_client.update_app(self.k8s_namespace, l0_workload_update['app_name'],
                                         './test_data/application/create_app.jinja2', l0_workload_delete)

        assert ret.status_code == 200, "更新应用失败:{}".format(ret.text)
        values = self.app_client.generate_jinja_data("verify_data/application/create_response.jinja2",
                                                     l0_workload_delete)
        assert self.app_client.is_sub_dict(values, ret.json()), \
            "删除部署比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)
        ret = self.app_client.get_app_status(self.k8s_namespace, self.app_name, 0, '0.status.totalComponents')
        assert ret, '删除部署后，应用状态中的totalComponents不是 0'
        # ret = self.app_client.get_app_status(self.k8s_namespace, self.app_name, 'Empty')
        # assert ret, '删除部署后，应用状态不是无计算组件,检查charon版本大于v2.2-b.3'

        delete_flag = self.deployment_client.check_exists(
            self.deployment_client.common_workload_url(self.k8s_namespace, l0_hpa['deployment_name']), 404)
        assert delete_flag, "删除部署失败，关联的deployment没有删除"

    @pytest.mark.metis
    @pytest.mark.delete
    def 测试删除应用(self):
        ret = self.app_client.delete_app(self.k8s_namespace, self.app_name)
        assert ret.status_code == 200, "删除应用失败:{}".format(ret.text)
        delete_flag = self.app_client.check_exists(self.app_client.common_app_url(self.k8s_namespace, self.app_name),
                                                   404)
        assert delete_flag, "删除应用失败"

    @pytest.mark.delete
    def 测试删除告警(self):
        ret = self.app_client.delete_alert(self.k8s_namespace, self.app_name)
        assert ret.status_code == 204, "删除告警失败:{}".format(ret.text)
