import pytest
from common.settings import K8S_NAMESPACE, RERUN_TIMES, USERNAME, REGION_NAME, AUDIT_UNABLED
from new_case.containerplatform.application.application import Application
from new_case.containerplatform.application.conftest import l1_data, create_application_casename


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestPostApp(object):

    def setup_class(self):
        self.app_client = Application()
        self.k8s_namespace = K8S_NAMESPACE

    @pytest.mark.metis
    @pytest.mark.parametrize("data", l1_data, ids=create_application_casename)
    def 测试删除应用L1(self, data):
        ret = self.app_client.delete_app(self.k8s_namespace, data['app_name'])
        assert ret.status_code == 200, "删除应用失败:{}".format(ret.text)
        delete_flag = self.app_client.check_exists(self.app_client.common_app_url(self.k8s_namespace, data['app_name']),
                                                   404)
        assert delete_flag, "删除应用失败"

    def 测试删除应用事件(self):
        payload = {"cluster": REGION_NAME,
                   "kind": "Deployment,Application,Pod,EndPoint,Service,HorizontalPodAutoscaler,ReplicaSet,DaemonSet,StatefulSet",
                   "name": l1_data[-2]["resources"][0]["deploy_name"], "namespace": K8S_NAMESPACE, "page": 1, "page_size": 100}
        kevents_results = self.app_client.get_kevents(payload=payload, expect_value="SuccessfulDelete", times=2)
        assert kevents_results, "获取删除应用事件失败"

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 测试应用删除审计(self):
        payload = {"user_name": USERNAME, "operation_type": "delete", "resource_type": "applications",
                   "resource_name": l1_data[0]['app_name']}
        result = self.app_client.search_audit(payload)
        payload.update({"namespace": self.k8s_namespace, "region_name": REGION_NAME})
        values = self.app_client.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.app_client.is_sub_dict(values, result.json()), "审计数据不符合预期"
