import pytest
import yaml
from common.settings import K8S_NAMESPACE, RERUN_TIMES, REGION_NAME, USERNAME, AUDIT_UNABLED
from new_case.containerplatform.application.application import Application
from new_case.containerplatform.secret.secret import Secret
from new_case.containerplatform.persistentvolumeclaims.pvc import Pvc
from new_case.containerplatform.configmap.configmap import Configmap
from new_case.containerplatform.application.conftest import l1_data, create_application_casename


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestPostApp(object):

    def setup_class(self):
        self.app_client = Application()
        self.k8s_namespace = K8S_NAMESPACE
        self.secret_tool = Secret()
        self.cm_tool = Configmap()
        self.pvc = Pvc()

    @pytest.mark.metis
    @pytest.mark.parametrize("data", l1_data, ids=create_application_casename)
    def 测试创建应用L1(self, data):
        if not self.app_client.check_exists(self.app_client.common_app_url(self.k8s_namespace, data['app_name']), 404, expect_cnt=1):
            self.app_client.delete_app(self.k8s_namespace, data['app_name'])
            self.app_client.check_exists(self.app_client.common_app_url(self.k8s_namespace, data['app_name']), 404)

        ret = self.app_client.create_app_by_template(data)
        assert ret.status_code == 200, "创建应用失败:{}".format(ret.text)
        values = yaml.safe_load(self.app_client.render_app_template(data))
        if "'resources': []" in str(data):
            values['spec']['componentTemplates'] = None
        assert self.app_client.is_sub_dict(values, ret.json()), \
            "创建应用比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)
        if 'deploy_name' not in str(data):
            ret = self.app_client.get_app_status(self.k8s_namespace, data['app_name'], 'Empty', expect_cnt=50)
            assert ret, '创建应用后，应用状态不是无计算组件'
        elif "'replicas': 0" in str(data):
            ret = self.app_client.get_app_status(self.k8s_namespace, data['app_name'], 'PartialRunning', expect_cnt=50)
            assert ret, '创建应用后，应用状态不是部分运行'
        else:
            ret = self.app_client.get_app_status(self.k8s_namespace, data['app_name'], 'Running')
            assert ret, '创建应用后，应用状态不是运行中'

    def 测试创建应用事件(self):
        payload = {"cluster": REGION_NAME,
                   "kind": "Deployment,Application,Pod,EndPoint,Service,HorizontalPodAutoscaler,ReplicaSet,DaemonSet,StatefulSet",
                   "name": l1_data[-2]["app_name"], "namespace": K8S_NAMESPACE, "page": 1, "page_size": 20}
        kevents_results = self.app_client.get_kevents(payload=payload, expect_value="SuccessfulCreate", times=2)
        assert kevents_results, "创建应用获取事件失败失败"

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 不测试应用创建审计(self):
        payload = {"user_name": USERNAME, "operation_type": "create", "resource_type": "applications",
                   "resource_name": l1_data[0]['app_name']}
        result = self.app_client.search_audit(payload)
        payload.update({"namespace": self.k8s_namespace, "region_name": REGION_NAME})
        values = self.app_client.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.app_client.is_sub_dict(values, result.json()), "审计数据不符合预期"
