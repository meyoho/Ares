import pytest
import copy
from common.settings import K8S_NAMESPACE, RERUN_TIMES, IMAGE, RESOURCE_PREFIX, PROJECT_NAME
from new_case.containerplatform.application.application import Application
from new_case.containerplatform.deployment.deployment import Deployment
from common.log import logger
from common import settings


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestRsMgt(object):
    def setup_class(self):
        self.k8s_namespace = K8S_NAMESPACE
        self.app_name = '{}-rs-mgt'.format(RESOURCE_PREFIX)
        self.app_client = Application()
        self.deploy_client = Deployment()
        self.region_name = settings.REGION_NAME

        self.teardown_class(self)

        self.base_body = {
            "kind": "ApplicationRollback",
            "apiVersion": "app.k8s.io/v1beta1",
            "metadata": {
                "name": self.app_name,
                "namespace": self.k8s_namespace
            },
            "spec": {
            }
        }

        self.app_data = {
            "app_name": self.app_name,
            "ns_name": self.k8s_namespace,
            "resources": [
                {
                    "deploy_name": self.app_name+"-"+IMAGE.split("/")[-1].split(":")[0],
                    "ns_name": self.k8s_namespace,
                    "project": PROJECT_NAME,
                    "app_name": self.app_name,
                    "containers": [
                        {
                            "image": IMAGE,
                        }
                    ],
                }
            ]
        }
        ret = self.app_client.create_app_by_template(self.app_data)
        assert ret.status_code == 200, '应用创建失败: {}'.format(self.app_name)

        self.app_ownerReferences = [
            {
                "kind": "Application",
                "apiVersion": "app.k8s.io/v1beta1",
                "name": self.app_name,
                "blockOwnerDeletion": True,
                "controller": True,
                "uid": ret.json()["metadata"]["uid"]
            }
        ]
        self.app_match_label = ret.json()["spec"]["selector"]["matchLabels"]
        self.deploy_data = {
            "deployment_name": self.app_name,
            "namespace": self.k8s_namespace,
            "image": IMAGE+"s"
        }
        ret = self.deploy_client.create_deployment_jinja2("./test_data/deployment/deployment.jinja2", self.deploy_data,
                                                          region_name=self.region_name, ns_name=self.k8s_namespace)
        assert ret.status_code == 201, "创建deployment失败"

    @pytest.mark.metis
    def 测试导入导出deployment资源后的应用状态(self):
        # 等待running
        app_status = self.app_client.get_app_status(self.k8s_namespace, self.app_name, "Running", expect_cnt=120)
        assert app_status, '创建应用后，应用状态不是运行中'
        # 导入Pending状态的deployment
        rs = self.deploy_client.detail_deplyment_jinja2(self.k8s_namespace, self.app_name, region_name=self.region_name)
        body = copy.deepcopy(self.base_body)
        body["spec"]["components"] = [rs.json()]
        ret = self.app_client.import_resource(self.app_name, body)
        assert ret.status_code == 204, "导入deployment资源失败"
        # 断言是否进入Pending状态
        app_status = self.app_client.get_app_status(self.k8s_namespace, self.app_name, "Pending", expect_cnt=30)
        assert app_status, '应用导入pending状态资源后，应用状态不是Pending'
        # 移除Pending状态的deployment
        rs = self.deploy_client.detail_deplyment_jinja2(self.k8s_namespace, self.app_name, region_name=self.region_name)
        body = copy.deepcopy(self.base_body)
        body["spec"]["components"] = [rs.json()]
        ret = self.app_client.export_resource(self.app_name, body)
        assert ret.status_code == 204, "导出deployment资源失败"
        # 断言是否进入Running状态
        app_status = self.app_client.get_app_status(self.k8s_namespace, self.app_name, "Running", expect_cnt=30)
        assert app_status, '应用移除pending状态资源后，应用状态不是Running'

    def teardown_class(self):
        logger.info("delete rs")
        self.deploy_client.delete_deplyment(self.k8s_namespace, self.app_name, self.region_name)
        self.app_client.delete_app(self.k8s_namespace, self.app_name)
