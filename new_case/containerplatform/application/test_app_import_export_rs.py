import pytest
import copy
import yaml
from common.settings import K8S_NAMESPACE, RERUN_TIMES, IMAGE, RESOURCE_PREFIX, SC_NAME, DEFAULT_LABEL
from new_case.containerplatform.application.application import Application
from new_case.containerplatform.resourcemanage.resource import Resource
from new_case.containerplatform.configmap.configmap import Configmap
from new_case.containerplatform.secret.secret import Secret
from new_case.containerplatform.service.service import Service
from new_case.containerplatform.ingress.ingress import Ingress
from new_case.containerplatform.jobs import Cronjobs
from new_case.containerplatform.daemonset.daemonset import Daemonset
from new_case.containerplatform.deployment.deployment import Deployment
from new_case.containerplatform.persistentvolumeclaims.pvc import Pvc
from common.log import logger
from common import settings


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestImportRs(object):
    def setup_class(self):
        self.k8s_namespace = K8S_NAMESPACE
        self.app_name = '{}-app-import'.format(RESOURCE_PREFIX)
        self.app_client = Application()
        self.resource_client = Resource()
        self.cm_client = Configmap()
        self.secret_client = Secret()
        self.service_client = Service()
        self.ingress_client = Ingress()
        self.cronjob_client = Cronjobs()
        self.deploy_client = Deployment()
        self.daemonset_client = Daemonset()
        self.pvc_client = Pvc()
        self.region_name = settings.REGION_NAME

        self.teardown_class(self)

        self.base_body = {
            "kind": "ApplicationRollback",
            "apiVersion": "app.k8s.io/v1beta1",
            "metadata": {
                "name": self.app_name,
                "namespace": self.k8s_namespace
            },
            "spec": {
            }
        }

        self.app_data = {
            "app_name": self.app_name,
            "ns_name": self.k8s_namespace,
            "resources": []
        }
        ret = self.app_client.create_app_by_template(self.app_data)
        assert ret.status_code == 200, '应用创建失败: {}'.format(self.app_name)

        self.app_ownerReferences = [
            {
                "kind": "Application",
                "apiVersion": "app.k8s.io/v1beta1",
                "name": self.app_name,
                "blockOwnerDeletion": True,
                "controller": True,
                "uid": ret.json()["metadata"]["uid"]
            }
        ]
        self.app_match_label = ret.json()["spec"]["selector"]["matchLabels"]

    def 测试导入deployment资源(self):
        self.deploy_data = {
            "deployment_name": self.app_name,
            "namespace": self.k8s_namespace,
            "image": IMAGE
        }
        ret = self.deploy_client.create_deployment_jinja2("./test_data/deployment/deployment.jinja2", self.deploy_data,
                                                          region_name=self.region_name, ns_name=self.k8s_namespace)
        assert ret.status_code == 201, "创建deployment失败"
        rs = self.deploy_client.detail_deplyment_jinja2(self.k8s_namespace, self.app_name, self.region_name)
        body = copy.deepcopy(self.base_body)
        body["spec"]["components"] = [rs.json()]
        ret = self.app_client.import_resource(self.app_name, body)
        assert ret.status_code == 204, "导入deployment资源失败"
        detail = self.deploy_client.detail_deplyment_jinja2(self.k8s_namespace, self.app_name, self.region_name).json()
        assert self.app_client.is_sub_dict(self.app_match_label, detail["metadata"]["labels"]), "没有加入label"
        assert self.app_client.is_sub_dict(self.app_ownerReferences,
                                           detail["metadata"]["ownerReferences"]), "没有加入ownerReferences"

    def 测试导出deployment资源(self):
        rs = self.deploy_client.detail_deplyment_jinja2(self.k8s_namespace, self.app_name, self.region_name)
        body = copy.deepcopy(self.base_body)
        body["spec"]["components"] = [rs.json()]
        ret = self.app_client.export_resource(self.app_name, body)
        assert ret.status_code == 204, "导出deployment资源失败"
        detail = self.deploy_client.detail_deplyment_jinja2(self.k8s_namespace, self.app_name, self.region_name).json()
        assert "ownerReferences" not in detail["metadata"], "ownerReferences 字段没有移除"
        for k in self.app_match_label:
            if "labels" in detail["metadata"]:
                assert k not in detail["metadata"]["labels"], "标签没有移除: {}".format(k)

    def 测试导入daemonset资源(self):
        self.daemonset_data = {
            "daemonset_name": self.app_name,
            "namespace": self.k8s_namespace,
            "image": IMAGE
        }
        ret = self.daemonset_client.create_daemonset_jinja2("./test_data/daemonset/daemonset.jinja2",
                                                            data=self.daemonset_data)
        assert ret.status_code == 201, "创建daemonset失败"
        rs = self.daemonset_client.detail_daemonset_jinja2(self.k8s_namespace, self.app_name)
        body = copy.deepcopy(self.base_body)
        body["spec"]["components"] = [rs.json()]
        ret = self.app_client.import_resource(self.app_name, body)
        assert ret.status_code == 204, "导入daemonset资源失败"
        detail = self.daemonset_client.detail_daemonset_jinja2(self.k8s_namespace, self.app_name).json()
        assert self.app_client.is_sub_dict(self.app_match_label, detail["metadata"]["labels"]), "没有加入label"
        assert self.app_client.is_sub_dict(self.app_ownerReferences,
                                           detail["metadata"]["ownerReferences"]), "没有加入ownerReferences"

    def 测试导出daemonset资源(self):
        rs = self.daemonset_client.detail_daemonset_jinja2(self.k8s_namespace, self.app_name)
        body = copy.deepcopy(self.base_body)
        body["spec"]["components"] = [rs.json()]
        ret = self.app_client.export_resource(self.app_name, body)
        assert ret.status_code == 204, "导出daemonset资源失败"
        detail = self.daemonset_client.detail_daemonset_jinja2(self.k8s_namespace, self.app_name).json()
        assert "ownerReferences" not in detail["metadata"], "ownerReferences 字段没有移除"
        for k in self.app_match_label:
            if "labels" in detail["metadata"]:
                assert k not in detail["metadata"]["labels"], "标签没有移除: {}".format(k)

    def 测试导入service资源(self):
        self.service_data = {
            'service_name': self.app_name,
            'namespace': self.k8s_namespace,
            'type': 'ClusterIP',
            'sessionAffinity': 'None',
            'portdata': '一组数据'
        }
        ret = self.service_client.create_service(self.k8s_namespace, "./test_data/service/service.jinja2",
                                                 self.service_data)
        assert ret.status_code == 201, "创建service失败"
        rs = self.service_client.get_service(self.k8s_namespace, self.app_name)
        body = copy.deepcopy(self.base_body)
        body["spec"]["components"] = [rs.json()]
        ret = self.app_client.import_resource(self.app_name, body)
        assert ret.status_code == 204, "导入service资源失败"
        detail = self.service_client.get_service(self.k8s_namespace, self.app_name).json()
        assert self.app_client.is_sub_dict(self.app_match_label, detail["metadata"]["labels"]), "没有加入label"
        assert self.app_client.is_sub_dict(self.app_ownerReferences,
                                           detail["metadata"]["ownerReferences"]), "没有加入ownerReferences"

    def 测试导出service资源(self):
        rs = self.service_client.get_service(self.k8s_namespace, self.app_name)
        body = copy.deepcopy(self.base_body)
        body["spec"]["components"] = [rs.json()]
        ret = self.app_client.export_resource(self.app_name, body)
        assert ret.status_code == 204, "导出service资源失败"
        detail = self.service_client.get_service(self.k8s_namespace, self.app_name).json()
        assert "ownerReferences" not in detail["metadata"], "ownerReferences 字段没有移除"
        for k in self.app_match_label:
            if "labels" in detail["metadata"]:
                assert k not in detail["metadata"]["labels"], "标签没有移除: {}".format(k)

    def 测试导入ingress资源(self):
        self.ingress_data = {
            'ingress_name': self.app_name,
            'namespace': self.k8s_namespace,
            'host': "{}.ares.acp.ingress.domain".format(self.app_name),
            'service_name': self.app_name,
            'ruledata': '一组数据'
        }
        ret = self.ingress_client.create_ingress(self.k8s_namespace, './test_data/ingress/ingress.jinja2',
                                                 self.ingress_data)
        assert ret.status_code == 201, "创建ingress失败"
        rs = self.ingress_client.get_ingress(self.k8s_namespace, self.app_name)
        body = copy.deepcopy(self.base_body)
        body["spec"]["components"] = [rs.json()]
        ret = self.app_client.import_resource(self.app_name, body)
        assert ret.status_code == 204, "导入ingress资源失败"
        detail = self.ingress_client.get_ingress(self.k8s_namespace, self.app_name).json()
        assert self.app_client.is_sub_dict(self.app_match_label, detail["metadata"]["labels"]), "没有加入label"
        assert self.app_client.is_sub_dict(self.app_ownerReferences,
                                           detail["metadata"]["ownerReferences"]), "没有加入ownerReferences"

    def 测试导出ingress资源(self):
        rs = self.ingress_client.get_ingress(self.k8s_namespace, self.app_name)
        body = copy.deepcopy(self.base_body)
        body["spec"]["components"] = [rs.json()]
        ret = self.app_client.export_resource(self.app_name, body)
        assert ret.status_code == 204, "导出ingress资源失败"
        detail = self.ingress_client.get_ingress(self.k8s_namespace, self.app_name).json()
        assert "ownerReferences" not in detail["metadata"], "ownerReferences 字段没有移除"
        for k in self.app_match_label:
            if "labels" in detail["metadata"]:
                assert k not in detail["metadata"]["labels"], "标签没有移除: {}".format(k)

    def 测试导入configmap资源(self):
        self.cm_data = {
            "namespace": self.k8s_namespace,
            "configmap_name": self.app_name,
            "configmap_key": "key",
            "configmap_value": "value"

        }
        ret = self.cm_client.create_configmap(self.cm_data)
        assert ret.status_code == 201, "创建configmap失败"
        rs = self.cm_client.get_configmap_detail(self.app_name)
        body = copy.deepcopy(self.base_body)
        body["spec"]["components"] = [rs.json()]
        ret = self.app_client.import_resource(self.app_name, body)
        assert ret.status_code == 204, "导入configmap资源失败"
        detail = self.cm_client.get_configmap_detail(self.app_name).json()
        assert self.app_client.is_sub_dict(self.app_match_label, detail["metadata"]["labels"]), "没有加入label"
        assert self.app_client.is_sub_dict(self.app_ownerReferences,
                                           detail["metadata"]["ownerReferences"]), "没有加入ownerReferences"

    def 测试导出configmap资源(self):
        rs = self.cm_client.get_configmap_detail(self.app_name)
        body = copy.deepcopy(self.base_body)
        body["spec"]["components"] = [rs.json()]
        ret = self.app_client.export_resource(self.app_name, body)
        assert ret.status_code == 204, "导出configmap资源失败"
        detail = self.cm_client.get_configmap_detail(self.app_name).json()
        assert "ownerReferences" not in detail["metadata"], "ownerReferences 字段没有移除"
        for k in self.app_match_label:
            if "labels" in detail["metadata"]:
                assert k not in detail["metadata"]["labels"], "标签没有移除: {}".format(k)

    def 测试导入secret资源(self):
        self.secret_data = {
            "secret_name": self.app_name,
            "K8S_NAMESPACE": self.k8s_namespace,
        }
        ret = self.secret_client.create_secret("./test_data/secret/secret.jinja2", self.secret_data)
        assert ret.status_code == 201, "创建保密字典失败"
        rs = self.secret_client.get_secret_detail(self.app_name)
        body = copy.deepcopy(self.base_body)
        body["spec"]["components"] = [rs.json()]
        ret = self.app_client.import_resource(self.app_name, body)
        assert ret.status_code == 204, "导入secret资源失败"
        detail = self.secret_client.get_secret_detail(self.app_name).json()
        assert self.app_client.is_sub_dict(self.app_match_label, detail["metadata"]["labels"]), "没有加入label"
        assert self.app_client.is_sub_dict(self.app_ownerReferences,
                                           detail["metadata"]["ownerReferences"]), "没有加入ownerReferences"

    def 测试导出secret资源(self):
        rs = self.secret_client.get_secret_detail(self.app_name)
        body = copy.deepcopy(self.base_body)
        body["spec"]["components"] = [rs.json()]
        ret = self.app_client.export_resource(self.app_name, body)
        assert ret.status_code == 204, "导出secret资源失败"
        detail = self.secret_client.get_secret_detail(self.app_name).json()
        assert "ownerReferences" not in detail["metadata"], "ownerReferences 字段没有移除"
        for k in self.app_match_label:
            if "labels" in detail["metadata"]:
                assert k not in detail["metadata"]["labels"], "标签没有移除: {}".format(k)

    def 测试导入cronjob资源(self):
        self.cronjob_data = {
            "cronjob_name": self.app_name,
            "namespace": self.k8s_namespace,
            "default_label": DEFAULT_LABEL
        }
        ret = self.cronjob_client.create_cronjob(**self.cronjob_data)
        assert ret.status_code == 201, "创建定时任务失败"
        rs = self.cronjob_client.get_cronjob_detail(self.app_name)
        body = copy.deepcopy(self.base_body)
        body["spec"]["components"] = [rs.json()]
        ret = self.app_client.import_resource(self.app_name, body)
        assert ret.status_code == 204, "导入cronjob资源失败"
        detail = self.cronjob_client.get_cronjob_detail(self.app_name).json()
        assert self.app_client.is_sub_dict(self.app_match_label, detail["metadata"]["labels"]), "没有加入label"
        assert self.app_client.is_sub_dict(self.app_ownerReferences,
                                           detail["metadata"]["ownerReferences"]), "没有加入ownerReferences"

    def 测试导出cronjob资源(self):
        rs = self.cronjob_client.get_cronjob_detail(self.app_name)
        body = copy.deepcopy(self.base_body)
        body["spec"]["components"] = [rs.json()]
        ret = self.app_client.export_resource(self.app_name, body)
        assert ret.status_code == 204, "导出cronjob资源失败"
        detail = self.cronjob_client.get_cronjob_detail(self.app_name).json()
        assert "ownerReferences" not in detail["metadata"], "ownerReferences 字段没有移除"
        for k in self.app_match_label:
            if "labels" in detail["metadata"]:
                assert k not in detail["metadata"]["labels"], "标签没有移除: {}".format(k)

    def 测试导入job资源(self):
        self.job_data = {
            "job_name": self.app_name,
            "namespace": self.k8s_namespace,
            "image": IMAGE,
            "default_label": DEFAULT_LABEL
        }
        ret = self.resource_client._create_job(
            yaml.safe_load(
                self.resource_client.generate_jinja_template("./test_data/jobs/job.jinja2").render(self.job_data)
            )
        )
        assert ret.status_code == 204, "创建任务失败"
        rs = self.cronjob_client.get_jobhistory_detail(self.app_name)
        body = copy.deepcopy(self.base_body)
        body["spec"]["components"] = [rs.json()]
        ret = self.app_client.import_resource(self.app_name, body)
        assert ret.status_code == 204, "导入job资源失败"
        detail = self.cronjob_client.get_jobhistory_detail(self.app_name).json()
        assert self.app_client.is_sub_dict(self.app_match_label, detail["metadata"]["labels"]), "没有加入label"
        assert self.app_client.is_sub_dict(self.app_ownerReferences,
                                           detail["metadata"]["ownerReferences"]), "没有加入ownerReferences"

    def 测试导出job资源(self):
        rs = self.cronjob_client.get_jobhistory_detail(self.app_name)
        body = copy.deepcopy(self.base_body)
        body["spec"]["components"] = [rs.json()]
        ret = self.app_client.export_resource(self.app_name, body)
        assert ret.status_code == 204, "导出job资源失败"
        detail = self.cronjob_client.get_jobhistory_detail(self.app_name).json()
        assert "ownerReferences" not in detail["metadata"], "ownerReferences 字段没有移除"
        for k in self.app_match_label:
            if "labels" in detail["metadata"]:
                assert k not in detail["metadata"]["labels"], "标签没有移除: {}".format(k)

    def 测试导入pvc资源(self):
        self.pvc_data = {
            "pvc_name": self.app_name,
            "namespace": self.k8s_namespace,
            "storageclassname": SC_NAME
        }
        ret = self.pvc_client.create_pvc_jinja2("./test_data/pvc/pvc.jinja2", self.pvc_data)
        assert ret.status_code == 201, "创建pvc失败"
        rs = self.pvc_client.get_pvc_detail(self.k8s_namespace, self.app_name)
        body = copy.deepcopy(self.base_body)
        body["spec"]["components"] = [rs.json()]
        ret = self.app_client.import_resource(self.app_name, body)
        assert ret.status_code == 204, "导入pvc资源失败"
        detail = self.pvc_client.get_pvc_detail(self.k8s_namespace, self.app_name).json()
        assert self.app_client.is_sub_dict(self.app_match_label, detail["metadata"]["labels"]), "没有加入label"
        assert self.app_client.is_sub_dict(self.app_ownerReferences,
                                           detail["metadata"]["ownerReferences"]), "没有加入ownerReferences"

    def 测试导出pvc资源(self):
        rs = self.pvc_client.get_pvc_detail(self.k8s_namespace, self.app_name)
        body = copy.deepcopy(self.base_body)
        body["spec"]["components"] = [rs.json()]
        ret = self.app_client.export_resource(self.app_name, body)
        assert ret.status_code == 204, "导出pvc资源失败"
        detail = self.pvc_client.get_pvc_detail(self.k8s_namespace, self.app_name).json()
        assert "ownerReferences" not in detail["metadata"], "ownerReferences 字段没有移除"
        for k in self.app_match_label:
            if "labels" in detail["metadata"]:
                assert k not in detail["metadata"]["labels"], "标签没有移除: {}".format(k)

    def teardown_class(self):
        logger.info("delete rs")
        self.cronjob_client.delete_jobhistory(self.app_name)
        self.cm_client.delete_configmap(self.app_name)
        self.secret_client.delete_secret(self.app_name, self.k8s_namespace)
        self.service_client.delete_service(self.k8s_namespace, self.app_name)
        self.ingress_client.delete_ingress(self.k8s_namespace, self.app_name)
        self.cronjob_client.delete_cronjob(self.app_name)
        self.deploy_client.delete_deplyment(self.k8s_namespace, self.app_name, region_name=self.region_name)
        self.daemonset_client.delete_daemonset(self.k8s_namespace, self.app_name)
        self.pvc_client.delete_pvc(self.k8s_namespace, self.app_name)
        self.app_client.delete_app(self.k8s_namespace, self.app_name)
