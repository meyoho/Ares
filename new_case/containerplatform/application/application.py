import sys
from time import sleep
import yaml
import copy
from common import settings
from common.base_request import Common
from common.log import logger
from new_case.operation.events.events import Events


class Application(Common):
    def __init__(self):
        super(Application, self).__init__()
        # self.genetate_global_info()
        self.events = Events()
        self.app_tpl = self.generate_jinja_template("./test_data/newapp/app.jinja2")
        self.deploy_tpl = self.generate_jinja_template("./test_data/newapp/deploy.jinja2")
        self.container_tpl = self.generate_jinja_template("./test_data/newapp/container.jinja2")
        self.ingress_tpl = self.generate_jinja_template("./test_data/newapp/ingress.jinja2")
        self.service_tpl = self.generate_jinja_template("./test_data/newapp/service.jinja2")
        self.secret_tpl = self.generate_jinja_template("./test_data/secret/secret.jinja2")
        self.configmap_tpl = self.generate_jinja_template("./test_data/configmap/create_configmap.jinja2")

    def create_app_url(self, namespace=settings.K8S_NAMESPACE, region_name=settings.REGION_NAME):
        return "acp/v1/kubernetes/{}/namespaces/{}/applications".format(region_name, namespace)

    def get_app_pod_url(self, namespace, name):
        return "kubernetes/{}/api/v1/namespaces/{}/pods?labelSelector=app.{}/name={}.{}". \
            format(self.region_name, namespace, settings.DEFAULT_LABEL, name, namespace)

    def create_app(self, file, data, namespace=settings.K8S_NAMESPACE, region_name=settings.REGION_NAME):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.create_app_url(namespace, region_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=url, json=data)

    def render_app_template(self, data):
        _data = copy.deepcopy(data)
        rs = []
        if 'resources' in _data:
            for resource in _data['resources']:
                resource.update({"default_label": settings.DEFAULT_LABEL})
                if 'deploy_name' in resource and 'service_name' not in resource and 'ingress_name' not in resource:
                    cts = []
                    for container in resource['containers']:
                        cts.append(self.container_tpl.render(**container))
                    resource['containers'] = cts
                    rs.append(self.deploy_tpl.render(**resource))
                elif 'deploy_name' not in resource and 'service_name' in resource and 'ingress_name' in resource:
                    rs.append(self.ingress_tpl.render(**resource))
                elif 'deploy_name' in resource and 'service_name' in resource and 'ingress_name' not in resource:
                    rs.append(self.service_tpl.render(**resource))
                elif 'deploy_name' not in resource and 'configmap_name' in resource:
                    rs.append(self.configmap_tpl.render(**resource))
                elif 'deploy_name' not in resource and 'secret_name' in resource:
                    rs.append(self.secret_tpl.render(**resource))
        _data['resources'] = rs
        return self.app_tpl.render(_data)

    def create_app_by_template(self, data, namespace=settings.K8S_NAMESPACE, region_name=settings.REGION_NAME):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.create_app_url(namespace, region_name)
        payload = yaml.safe_load(self.render_app_template(copy.deepcopy(data)))
        return self.send(method='post', path=url, json=payload)

    def update_app_by_template(self, app_name, data, namespace=settings.K8S_NAMESPACE):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.create_app_url(namespace) + "/" + app_name
        payload = yaml.safe_load(self.render_app_template(copy.deepcopy(data)))
        return self.send(method='put', path=url, json=payload)

    def delete_app(self, namespace, name, region_name=settings.REGION_NAME):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = "kubernetes/{}/apis/app.k8s.io/v1beta1/namespaces/{}/applications/{}".format(region_name, namespace,
                                                                                           name)
        return self.send(method='delete', path=url)

    def get_app_status(self, namespace, name, expect_value, expect_key="0.status.state", expect_cnt=30,
                       region_name=settings.REGION_NAME):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        cnt = 0
        flag = False
        while cnt < expect_cnt and not flag:
            cnt += 1
            sleep(6)
            url = "acp/v1/kubernetes/{}/namespaces/{}/applications/{}". \
                format(region_name, namespace, name)
            response = self.send(method="GET", path=url)
            assert response.status_code == 200, "get app status failed"
            value = self.get_value(response.json(), expect_key)
            logger.info("应用状态：{}".format(value))
            if value == expect_value:
                flag = True
                break
        return flag

    def kubectl_exec(self, app_name, namespace, cmd, expect_value, public_ip=''):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        public_ip = public_ip or self.global_info['public_ips'][0]
        pod_result = self.get_app_pod(namespace, app_name)
        pod_name = self.get_value(pod_result.json(), 'items.0.metadata.name')
        command = "kubectl exec -it {} -n {} -- {}".format(pod_name, namespace, cmd)
        ret_excute = self.excute_script(command, public_ip)
        return expect_value in ret_excute[1], ret_excute[1]

    def stop_app(self, namespace, app_name):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        url = "acp/v1/kubernetes/{}/namespaces/{}/applications/{}/stop". \
            format(self.region_name, namespace, app_name)
        return self.send(method="post", path=url)

    def start_app(self, namespace, app_name):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        url = "acp/v1/kubernetes/{}/namespaces/{}/applications/{}/start". \
            format(self.region_name, namespace, app_name)
        return self.send(method="post", path=url)

    def common_app_url(self, namespace, app_name, region_name=settings.REGION_NAME):
        return "acp/v1/kubernetes/{}/namespaces/{}/applications/{}".format(region_name, namespace, app_name)

    def update_app(self, namespace, app_name, file, data):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        url = "acp/v1/kubernetes/{}/namespaces/{}/applications/{}".format(self.region_name, namespace, app_name)
        data = self.generate_jinja_data(file, data)
        return self.send('put', url, json=data)

    def list_alert(self, namespace, app_name):
        url = "v1/alerts/{region}/{ns}?labelSelector=alert.{label}/namespace={ns},alert.{label}/application={app_name}" \
              "&page=1&page_size=10".format(region=self.region_name, ns=namespace, app_name=app_name, label=settings.DEFAULT_LABEL)
        return self.send('get', url)

    def delete_alert(self, namespace, app_name):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        url = "v1/alerts/{region}/{ns}?labelSelector=alert.{label}/namespace={ns},alert.{label}/application={app_name}" \
              "&page=1&page_size=10".format(region=self.region_name, ns=namespace, app_name=app_name, label=settings.DEFAULT_LABEL)
        return self.send(method="delete", path=url)

    def list_app(self, namespace, limit=20):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        url = "kubernetes/{}/apis/app.k8s.io/v1beta1/namespaces/{}/applications?limit={}". \
            format(self.region_name, namespace, limit)
        return self.send(method="get", path=url)

    def detail_app(self, namespace, app_name, region=settings.REGION_NAME):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        url = "acp/v1/kubernetes/{}/namespaces/{}/applications/{}".format(region, namespace, app_name)
        return self.send(method="get", path=url)

    def get_app_address(self, namespace, app_name):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        url = "acp/v1/kubernetes/{}/namespaces/{}/applications/{}/address".format(self.region_name, namespace, app_name)
        return self.send(method="get", path=url)

    def get_app_pod(self, namespace, app_name, region_name=settings.REGION_NAME):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        url = "kubernetes/{region}/api/v1/namespaces/{ns}/pods?labelSelector=app.{label}/name={app_name}.{ns}". \
            format(region=region_name, ns=namespace, app_name=app_name, label=settings.DEFAULT_LABEL)
        return self.send(method="get", path=url)

    def delete_pod(self, namespace, pod_name):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        url = "kubernetes/{}/api/v1/namespaces/{}/pods/{}".format(self.region_name, namespace, pod_name)
        return self.send(method="delete", path=url)

    def export_resource(self, app_name, data):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        url = "acp/v1/kubernetes/{cluster}/namespaces/{namespace}/applications/{app_name}/export". \
            format(cluster=self.region_name, namespace=self.k8s_namespace, app_name=app_name)
        return self.send(method='post', path=url, json=data)

    def add_snapshot(self, app_name, change_cause=''):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        url = 'acp/v1/kubernetes/{cluster}/namespaces/{namespace}/applications/{app_name}/snapshot'. \
            format(cluster=self.region_name, namespace=self.k8s_namespace, app_name=app_name)
        data = {
            "kind": "ApplicationRollback",
            "apiVersion": "app.k8s.io/v1beta1",
            "metadata": {
                "name": app_name,
                "namespace": self.k8s_namespace
            },
            "spec": {
                "changeCause": change_cause
            }
        }
        return self.send(method='post', path=url, json=data)

    def import_resource(self, app_name, data):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        url = "acp/v1/kubernetes/{cluster}/namespaces/{namespace}/applications/{app_name}/import". \
            format(cluster=self.region_name, namespace=self.k8s_namespace, app_name=app_name)
        return self.send(method='post', path=url, json=data)

    def get_app_topology(self, namespace, app_name):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        url = "acp/v1/kubernetes/{}/topology/{}/{}/{}".format(self.region_name, namespace, 'application', app_name)
        return self.send(method="get", path=url)

    def get_app_histories(self, namespace, app_name):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        url = "kubernetes/{}/apis/app.k8s.io/v1beta1/namespaces/{}/applicationhistories?" \
              "labelSelector=app.{}/name={}.{}".format(self.region_name, namespace, settings.DEFAULT_LABEL, app_name, namespace)
        return self.send(method="get", path=url)

    def detail_histories(self, namespace, revision):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        url = "kubernetes/{}/apis/app.k8s.io/v1beta1/namespaces/{}/applicationhistories/{}".format(self.region_name,
                                                                                                   namespace, revision)
        return self.send(method="get", path=url)

    def rollback_app(self, namespace, app_name, revision, user='admin'):

        url = "acp/v1/kubernetes/{}/namespaces/{}/applications/{}/rollback".format(self.region_name, namespace,
                                                                                   app_name)
        data = {"kind": "ApplicationRollback", "apiVersion": "app.k8s.io/v1beta1",
                "metadata": {"name": app_name, "namespace": namespace}, "spec": {"revision": revision, "user": user}}
        return self.send('post', url, json=data)

    def get_app_logs(self, namespace, pod_name, container_name):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        url = "acp/v1/kubernetes/{}/namespaces/{}/pods/{}/{}/log?previous=false&logFilePosition=end" \
              "&referenceTimestamp=newest&offsetFrom=2000000000&offsetTo=2000000100". \
            format(self.region_name, namespace, pod_name, container_name)
        return self.send(method="get", path=url)

    def get_app_kevents(self, namespace, app_name, workload_name):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        times = self.generate_time_params()
        payload = {
            "start_time": times["start_time"],
            "end_time": times["end_time"],
            "cluster": self.region_name,
            "name": ','.join([app_name, workload_name]),
            "fuzzy": 'true',
            "kind": "Deployment,Application,Pod,EndPoint,Service,HorizontalPodAutoscaler,ReplicaSet,DaemonSet,StatefulSet",
            "namespace": namespace,
            "page": 1,
            "page_size": 20

        }
        return self.send(method="get", path=self.events.get_kevents_url(), params=payload)

    def wait_app_status(self, namespace, name, region_name=settings.REGION_NAME, expect_cnt=10):
        cnt = 0
        flag = False
        sleep(10)
        while cnt < expect_cnt and not flag:
            cnt += 1
            sleep(5)
            url = "acp/v1/kubernetes/{}/namespaces/{}/applications/{}".format(region_name, namespace, name)
            response = self.send(method="GET", path=url)
            assert response.status_code == 200, "get app status failed"
            value = self.get_value(response.json(), "0.status.state")
            logger.info("应用状态：{}".format(value))
            if value in ['Running', 'Empty', 'PartialRunning']:
                flag = True
                break
        return flag
