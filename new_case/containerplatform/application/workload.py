import sys

from common.base_request import Common
from common.log import logger


class Wordload(Common):
    def common_workload_url(self, namespace, workload_name, workload_kind='deployments'):
        return "kubernetes/{}/apis/extensions/v1beta1/namespaces/{}/{}/{}".format(self.region_name, namespace,
                                                                                  workload_kind, workload_name)

    def update_workload(self, namespace, workload_name, file, data, workload_kind='deployments'):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.common_workload_url(namespace, workload_name, workload_kind)
        data = self.generate_jinja_data(file, data)
        return self.send(method='put', path=url, json=data)

    def get_workload(self, namespace, workload_name, workload_kind='deployments'):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.common_workload_url(namespace, workload_name, workload_kind)
        return self.send(method='get', path=url)

    def get_workload_topology(self, namespace, workload_name, workload_kind='deployment'):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = "acp/v1/kubernetes/{}/topology/{}/{}/{}?depth=2". \
            format(self.region_name, namespace, workload_kind, workload_name)
        return self.send(method='get', path=url)

    def common_hpa_url(self, namespace, name):
        return "kubernetes/{}/apis/autoscaling/v1/namespaces/{}/horizontalpodautoscalers/{}".format(self.region_name,
                                                                                                    namespace, name)

    def get_hpa(self, namespace, workload_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.common_hpa_url(namespace, workload_name)
        return self.send(method='get', path=url)

    def create_hpa(self, namespace, file, data):
        url = "kubernetes/{}/apis/autoscaling/v1/namespaces/{}/horizontalpodautoscalers".format(self.region_name,
                                                                                                namespace)

        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=url, json=data)

    def update_hpa(self, namespace, name, file, data):
        url = self.common_hpa_url(namespace, name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='put', path=url, json=data)

    def delete_hpa(self, namespace, name):
        url = self.common_hpa_url(namespace, name)
        return self.send(method='delete', path=url)
