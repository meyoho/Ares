import sys
from common.base_request import Common
from common.log import logger
from common import settings


class Pv(Common):
    def __init__(self):
        super(Pv, self).__init__()
        self.genetate_global_info()

    def get_common_pv_url_v1(self, pv_name=''):
        return 'kubernetes/{}/api/v1/persistentvolumes/{}'.format(self.region_name, pv_name)

    def get_common_pv_list_url(self, pv_name='', limit=1, cnt=""):
        return limit and "kubernetes/{}/api/v1/persistentvolumes/{}?limit={}&continue={}".format(self.region_name,
                                                                                                 pv_name, limit, cnt) \
               or "kubernetes/{}/api/v1/persistentvolumes/{}".format(self.region_name, pv_name)

    def list_pv(self, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_pv_url_v1()
        return self.send(method='get', path=url, params={}, auth=auth)

    def create_pv(self, file, data, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_pv_url_v1()
        data = self.generate_data(file, data)
        return self.send(method='post', path=url, data=data, params={}, auth=auth)

    def create_pv_jinja2(self, file, data):
        path = self.get_common_pv_url_v1()
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=path, json=data, params={})

    def update_pv(self, pv_name, file, data, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_pv_url_v1(pv_name)
        data = self.generate_data(file, data)
        return self.send(method='put', path=url, data=data, params={}, auth=auth)

    def update_pv_jinja2(self, pv_name, file, data):
        path = self.get_common_pv_url_v1(pv_name=pv_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='put', path=path, json=data, params={})

    def get_pv_detail(self, pv_name, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_pv_url_v1(pv_name)
        return self.send(method='get', path=url, params={}, auth=auth)

    def delete_pv(self, pv_name, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_pv_url_v1(pv_name)
        return self.send(method='delete', path=url, params={}, auth=auth)

    def search_pv(self, pv_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = "acp/v1/resources/search/kubernetes/{}/api/v1/persistentvolumes".format(settings.REGION_NAME)
        params = {"keyword": pv_name, "field": "metadata.name", "limit": 20}
        return self.send(method='get', path=url, params=params)

    def get_pv_list(self, limit=None, cnt=""):
        path = self.get_common_pv_list_url(limit=limit, cnt=cnt)
        return self.send(method='get', path=path, params={})

    data_list = [
        {
            "pv_name": '{}-ares-alauda-pv-nfs'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
            "nfs_server": settings.NFS_IP,
            "nfs_path": settings.NFS_PATH
        },
        {
            "pv_name": '{}-ares-alauda-pv-hostpath'.format(settings.RESOURCE_PREFIX).replace('_', '-')
        },
        {
            "metadata_labels": {"key1": "value1", "key2": "value2"},
            "pv_name": '{}-ares-alauda-pv-nfs-1'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
            "nfs_server": settings.NFS_IP,
            "nfs_path": settings.NFS_PATH
        },
        {
            "metadata_labels": {"key1": "value1", "key2": "value2"},
            "pv_name": '{}-ares-alauda-pv-hostpath-1'.format(settings.RESOURCE_PREFIX).replace('_', '-')
        },
        {
            "metadata_annotations": {"a": "c", "b": "d"},
            "pv_name": '{}-ares-alauda-pv-nfs-2'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
            "nfs_server": settings.NFS_IP,
            "nfs_path": settings.NFS_PATH
        },
        {
            "metadata_annotations": {"a": "c", "b": "d"},
            "pv_name": '{}-ares-alauda-pv-hostpath-2'.format(settings.RESOURCE_PREFIX).replace('_', '-')
        }
    ]
    caename_list = ["nfs类型必填参数", "hostPath类型必填参数", "nfs类型多组标签", "hostPath类型多组标签", "nfs类型多组注解", "hostPath类型多组注解"]
