import pytest
from common import settings
from common.delayed_assert import assert_expectations, expect
# from test_case.newapp.newapp import Newapplication
from new_case.containerplatform.persistentvolumeclaims.pvc import Pvc
from new_case.containerplatform.persistentvolumes.pv import Pv
from new_case.containerplatform.storageclass.scs import Scs


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestPvSuite(object):
    def setup_class(self):
        self.pv = Pv()
        self.pv_name = '{}-ares-alauda-pv-{}'.format(settings.RESOURCE_PREFIX, self.pv.region_name).replace('_', '-')
        self.pv_name2 = '{}-ares-alauda-pv2-{}'.format(settings.RESOURCE_PREFIX, self.pv.region_name).replace('_', '-')
        self.k8s_namespace = self.pv.global_info["$K8S_NAMESPACE"]
        self.scs = Scs()
        self.nfs_ip = settings.NFS_IP
        self.nfs_path = settings.NFS_PATH
        self.pvc = Pvc()
        self.pvc_name_nfs = '{}-ares-alauda-pvcforapp-nfs'.format(settings.RESOURCE_PREFIX)
        self.teardown(self)

    def teardown(self):
        # self.newapp.delete_newapp(self.k8s_namespace, self.appwithpvc_name)
        self.pvc.delete_pvc(self.k8s_namespace, self.pvc_name_nfs)
        self.pv.delete_pv(self.pv_name)
        self.pv.check_exists(self.pv.get_common_pv_url_v1(self.pv_name), 404)
        self.pv.delete_pv(self.pv_name2)

    @pytest.mark.BAT
    def 测试pv基于本地存储的增删改查(self):
        '''
        持久卷hostpath测试:创建pv-验证分页-获取pv列表-更新pv-获取pv详情-删除pv
        '''
        self.pv.delete_pv(self.pv_name)
        result = {"flag": True}
        # create pv
        create_result = self.pv.create_pv("./test_data/pv/pv.json",
                                          {"$pv_name": self.pv_name, "$size": "1",
                                           })
        assert create_result.status_code == 201, "创建pv失败{}".format(create_result.text)
        self.pv.check_value_in_response(self.pv.get_common_pv_url_v1(), self.pv_name, params={})

        # list pv
        list_result = self.pv.list_pv()
        result = self.pv.update_result(result, list_result.status_code == 200, list_result.text)
        result = self.pv.update_result(result, self.pv_name in list_result.text, "获取持久卷列表：新建pv不在列表中")

        # update pv
        update_result = self.pv.update_pv(self.pv_name, "./test_data/pv/pv.json",
                                          {"$pv_name": self.pv_name, "$size": "2",
                                           })
        assert update_result.status_code == 200, "更新pv失败{}".format(update_result.text)

        # get pv detail
        detail_result = self.pv.get_pv_detail(self.pv_name)
        result = self.pv.update_result(result, detail_result.status_code == 200, detail_result.text)
        result = self.pv.update_result(result,
                                       self.pv.get_value(detail_result.json(),
                                                         "status.phase") == "Available",
                                       "获取持久卷详情失败：状态不是可用")
        result = self.pv.update_result(result,
                                       self.pv.get_value(detail_result.json(),
                                                         "spec.capacity.storage") == "2Gi",
                                       "获取持久卷详情失败：大小不是2Gi")

        # delete pv
        delete_result = self.pv.delete_pv(self.pv_name)
        assert delete_result.status_code == 200, "删除pv失败{}".format(delete_result.text)

        exists_result = self.pv.check_exists(self.pv.get_common_pv_url_v1(self.pv_name), 404)
        assert exists_result, "删除持久卷失败"
        expect(result['flag'], result)
        assert_expectations()

    @pytest.mark.Regression
    @pytest.mark.skipif(settings.NFS_IP == "", reason="没有提供变量NFS_IP，无法测试")
    def test_pv_nfs(self):
        '''
        持久卷nfs测试:创建pv-获取pv列表-更新pv-获取pv详情-创建pvc-创建应用-删除应用-删除pvc-删除pv
        '''
        self.pvc.delete_pvc(self.k8s_namespace, self.pvc_name_nfs)
        self.pv.delete_pv(self.pv_name2)
        result = {"flag": True}
        # create pv
        create_result = self.pv.create_pv("./test_data/pv/pv-nfs.json",
                                          {"$pv_name": self.pv_name, "$size": "2", "$nfs_ip": self.nfs_ip,
                                           "$nfs_path": self.nfs_path
                                           })
        assert create_result.status_code == 201, "创建pv-nfs失败{}".format(create_result.text)
        self.pv.check_value_in_response(self.pv.get_common_pv_url_v1(), self.pv_name, params={})
        # list pv
        list_result = self.pv.list_pv()
        result = self.pv.update_result(result, list_result.status_code == 200, list_result.text)
        result = self.pv.update_result(result, self.pv_name in list_result.text, "获取持久卷列表：新建pv-nfs不在列表中")
        # update pv
        update_result = self.pv.update_pv(self.pv_name, "./test_data/pv/pv-nfs.json",
                                          {"$pv_name": self.pv_name, "$size": "1", "$nfs_ip": self.nfs_ip,
                                           "$nfs_path": self.nfs_path
                                           })
        assert update_result.status_code == 204, "更新pv失败{}".format(update_result.text)
        # get pv detail
        detail_result = self.pv.get_pv_detail(self.pv_name)
        result = self.pv.update_result(result, detail_result.status_code == 200, detail_result.text)
        result = self.pv.update_result(result,
                                       self.pv.get_value(detail_result.json(),
                                                         "status.phase") == "Available",
                                       "获取持久卷详情失败：状态不是可用")
        result = self.pv.update_result(result,
                                       self.pv.get_value(detail_result.json(),
                                                         "spec.capacity.storage") == "1Gi",
                                       "获取持久卷详情失败：大小不是1Gi")
        # create pvc
        createpvc_result = self.pvc.create_pvc("./test_data/pvc/pvc-nfs.json",
                                               {"$pvc_name": self.pvc_name_nfs, "$size": "1"})
        assert createpvc_result.status_code == 201, "创建pvc失败{}".format(createpvc_result.text)
        self.pvc.get_status(self.pvc.get_common_pvc_url_v1(self.pvc.global_info["$K8S_NAMESPACE"], self.pvc_name_nfs),
                            "status.phase", "Bound")
