import pytest
from common import settings
from new_case.containerplatform.persistentvolumes.pv import Pv


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestPvSuitePost(object):

    def setup_class(self):
        self.pv = Pv()

    @pytest.mark.prepare
    @pytest.mark.parametrize("data", Pv.data_list, ids=Pv.caename_list)
    def 测试_创建pv(self, data):
        create_result = self.pv.create_pv_jinja2("./test_data/pv/pv.jinja2", data=data)
        assert create_result.status_code == 201, "创建pv:{}失败{}".format(data['pv_name'], create_result.text)

        value = self.pv.generate_jinja_data("./verify_data/pv/create-pv.jinja2", data)
        assert self.pv.is_sub_dict(value, create_result.json()), \
            "创建pv_必填参数_可选参数为空比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

    @pytest.mark.skipif(settings.AUDIT_UNABLED, reason="do not have audit")
    def 不测试pv创建审计(self):
        payload = {"user_name": settings.USERNAME, "operation_type": "create", "resource_type": "persistentvolumes",
                   "resource_name": self.data_list[0]['pv_name']}
        result = self.pv.search_audit(payload)
        payload.update({"namespace": settings.K8S_NAMESPACE, "region_name": settings.REGION_NAME, "code": 201})
        values = self.pv.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.pv.is_sub_dict(values, result.json()), "审计数据不符合预期"
