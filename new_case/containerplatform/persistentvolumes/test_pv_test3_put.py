import pytest
from common import settings
from new_case.containerplatform.persistentvolumes.pv import Pv


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestPvSuitePut(object):
    def setup_class(self):
        self.pv = Pv()

    @pytest.mark.upgrade
    @pytest.mark.parametrize("data", Pv.data_list, ids=Pv.caename_list)
    def 测试_更新pv(self, data):
        data.update({"storage": "2Gi"})
        update_result = self.pv.update_pv_jinja2(data['pv_name'], './test_data/pv/pv.jinja2', data=data)
        assert update_result.status_code == 200, "更新pv:{}失败{}".format(data['pv_name'], update_result.text)

        value = self.pv.generate_jinja_data("./verify_data/pv/create-pv.jinja2", data)
        assert self.pv.is_sub_dict(value, update_result.json()), \
            "更新pv_必填参数_可选参数为空比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

    @pytest.mark.skipif(settings.AUDIT_UNABLED, reason="do not have audit")
    def 测试pv更新审计(self):
        payload = {"user_name": settings.USERNAME, "operation_type": "update", "resource_type": "persistentvolumes",
                   "resource_name": self.data_list[0]['pv_name']}
        result = self.pv.search_audit(payload)
        payload.update({"namespace": "", "region_name": settings.REGION_NAME})
        values = self.pv.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.pv.is_sub_dict(values, result.json()), "审计数据不符合预期"
