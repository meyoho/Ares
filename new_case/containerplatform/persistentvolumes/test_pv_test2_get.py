import pytest
from common import settings
from new_case.containerplatform.persistentvolumes.pv import Pv


@pytest.mark.archon
@pytest.mark.upgrade
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestPvSuiteGet(object):
    data_list = Pv.data_list

    def setup_class(self):
        self.pv = Pv()

    def 测试_搜索pv(self):
        data = self.data_list[0]
        ser = self.pv.search_pv(pv_name=data["pv_name"])
        assert ser.status_code == 200, "没有搜索到{}，{}".format(data["pv_name"], ser.text)

        value = self.pv.generate_jinja_data("./verify_data/pv/pv_list.jinja2", data)
        assert self.pv.is_sub_dict(value, ser.json()), "搜索pv比对数据失败，返回数据:{},期望数据:{}".format(
            ser.json(), value)

    def 测试_模糊搜索pv(self):
        pv_name = '{}-ares-alauda-pv'.format(settings.RESOURCE_PREFIX).replace('_', '-')
        ser = self.pv.search_pv(pv_name=pv_name)
        assert ser.status_code == 200, "没有搜索到{}，{}".format(pv_name, ser.text)
        assert len(ser.json()["items"]) == 6, "模糊搜索期望搜索到6条数据，结果却是{}".format(len(ser.json()["items"]))

    def 测试_获取pv列表_无limit参数(self):
        ret = self.pv.get_pv_list()
        assert ret.status_code == 200, "获取pv列表失败:{}".format(ret.text)
        pv_num = len(ret.json()["items"])
        assert pv_num >= len(self.data_list), "获取pv列表,不传limit时返回失败,预期至少返回{}个,实际返回{}个".format(len(self.data_list), pv_num)

    def 测试_获取pv列表_有limit参数(self):
        ret = self.pv.get_pv_list(limit=20)
        assert ret.status_code == 200, "获取pv列表失败:{}".format(ret.text)
        ret2 = self.pv.get_pv_list(limit=1)
        pv_num = len(ret2.json()["items"])
        assert pv_num == 1, "获取pv列表,limit=1时失败,预期返回1个pv,实际返回{}个".format(pv_num)

        global name
        name = Pv.get_value(ret2.json(), "items.0.metadata.name")

        global cnt
        cnt = Pv.get_value(ret2.json(), "metadata.continue")

    def 测试_获取pv列表_有limit参数和continue参数(self):
        ret = self.pv.get_pv_list(limit=1, cnt=cnt)
        assert ret.status_code == 200, "获取pv列表_有limit和continue参数:{}".format(ret.text)
        name_continue = Pv.get_value(ret.json(), "items.0.metadata.name")
        assert name_continue != name, "分页数据相同，pv第一页数据:{},pv第二页数据:{}".format(name, name_continue)
