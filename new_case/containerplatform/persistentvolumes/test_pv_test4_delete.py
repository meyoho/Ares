import pytest
from common import settings
from new_case.containerplatform.persistentvolumes.pv import Pv


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestPvSuiteDelete(object):
    def setup_class(self):
        self.pv = Pv()

    @pytest.mark.delete
    @pytest.mark.parametrize("data", Pv.data_list, ids=Pv.caename_list)
    def 测试_删除pv(self, data):
        delete_result = self.pv.delete_pv(data['pv_name'])
        assert delete_result.status_code == 200, "删除pv:{}失败{}".format(data['pv_name'], delete_result.text)

    @pytest.mark.skipif(settings.AUDIT_UNABLED, reason="do not have audit")
    def 测试pv删除审计(self):
        payload = {"user_name": settings.USERNAME, "operation_type": "delete", "resource_type": "persistentvolumes",
                   "resource_name": self.data_list[0]['pv_name']}
        result = self.pv.search_audit(payload)
        payload.update({"namespace": "", "region_name": settings.REGION_NAME})
        values = self.pv.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.pv.is_sub_dict(values, result.json()), "审计数据不符合预期"
