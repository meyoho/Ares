import pytest
import json
from common.settings import RERUN_TIMES, CASE_TYPE
from new_case.containerplatform.namespace.namespace import Namespace
from new_case.containerplatform.namespace.conftest import L0_data
from common.base_request import Common


@pytest.mark.archon
@pytest.mark.BAT
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestNamespaceSuite(object):
    def setup_class(self):
        self.namespace = Namespace()
        self.teardown_class(self)

    def teardown_class(self):
        if CASE_TYPE not in ("prepare", "upgrade", "delete"):
            self.namespace.delete_namespace(L0_data["namespace_name"])

    @pytest.mark.prepare
    def 测试_创建命名空间_参数都正确(self, data=L0_data):
        '''
        创建命名空间-参数都正确
        :return:
        '''
        verify_template = Common.generate_jinja_template(self, './verify_data/namespace/create_response.jinja2')
        create_ns_result = self.namespace.create_namespace('./test_data/namespace/create_namespace.jinja2', data)
        assert create_ns_result.status_code == 200, "创建命名空间,参数都正确时失败 {}".format(create_ns_result.text)
        value = verify_template.render(data)
        assert self.namespace.is_sub_dict(json.loads(value), create_ns_result.json()), \
            "创建命名空间比对数据失败，返回数据:{},期望数据:{}".format(create_ns_result.json(), json.loads(value))

    @pytest.mark.upgrade
    def 测试_获取指定命名空间_不带任何参数(self, data=L0_data):
        '''
        获取指定命名空间_不带任何参数
        :return:
        '''
        verify_template = Common.generate_jinja_template(self, './verify_data/namespace/get_response.jinja2')
        get_ns_result = self.namespace.get_namespaces(data["namespace_name"])
        assert get_ns_result.status_code == 200, "获取指定命名空间,不带任何参数时失败 {}".format(get_ns_result.text)
        value = verify_template.render(data)
        assert self.namespace.is_sub_dict(json.loads(value), get_ns_result.json()), \
            "获取指定命名空间{}比对数据失败，返回数据:{},期望数据:{}".format(data["namespace_name"], get_ns_result.json(), json.loads(value))

    @pytest.mark.upgrade
    def 测试_更新命名空间资源配额的存储配额(self):
        '''
        测试_更新命名空间资源配额的存储配额
        :return:
        '''
        verify_template = Common.generate_jinja_template(self,
                                                         './verify_data/namespace/patch_resourcequota_response.jinja2')
        L0_data.update({"requests_storage": "10"})

        update_ns_result = self.namespace.update_resourcequota(L0_data["namespace_name"],
                                                               './test_data/namespace/update_resourcequota.jinja2',
                                                               data=L0_data)
        assert update_ns_result.status_code == 200, "更新命名空间资源配额的所有配额时失败 {}".format(update_ns_result.text)
        self.namespace.wait_resourcequota_status(L0_data["namespace_name"])
        get_resourcequota_result = self.namespace.detail_resourcequota(L0_data["namespace_name"])
        value = verify_template.render(L0_data)
        assert self.namespace.is_sub_dict(json.loads(value), get_resourcequota_result.json()), \
            "更新命名空间比对数据失败，返回数据:{},期望数据:{}".format(update_ns_result.json(), json.loads(value))

    @pytest.mark.upgrade
    def 测试_更新命名空间容器限额的最大内存限制(self):
        '''
        更新命名空间容器限额的最大内存限制
        :return:
        '''
        verify_template = Common.generate_jinja_template(self,
                                                         './verify_data/namespace/patch_limitRange_response.jinja2')
        L0_data.update({"max_mem": "2Gi"})
        update_ns_result = self.namespace.update_limitrange(L0_data["namespace_name"],
                                                            './test_data/namespace/update_limitRange.jinja2',
                                                            data=L0_data)
        assert update_ns_result.status_code == 200, "更新命名空间容器限额时失败 {}".format(update_ns_result.text)
        value = verify_template.render(L0_data)
        assert self.namespace.is_sub_dict(json.loads(value), update_ns_result.json()), \
            "更新命名空间limitRange比对数据失败，返回数据:{},期望数据:{}".format(update_ns_result.json(), json.loads(value))

    @pytest.mark.delete
    def 测试_删除指定命名空间(self, data=L0_data):
        '''
        删除指定命名空间
        :return:
        '''
        delete_ns_result = self.namespace.delete_namespace(data["namespace_name"])
        assert delete_ns_result.status_code == 200, "删除命名空间时失败 {}".format(delete_ns_result.text)
        value = self.namespace.generate_jinja_data("verify_data/namespace/delete_response.jinja2", data)
        assert self.namespace.is_sub_dict(value, delete_ns_result.json()), "删除命名空间比对数据失败，返回数据:{},期望数据:{}".format(
            delete_ns_result.json(), value)
