import pytest
import json
from common.settings import RERUN_TIMES
from common.base_request import Common
from new_case.containerplatform.namespace.namespace import Namespace
from new_case.containerplatform.namespace.conftest import setup_ns_data, link_ns_data


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestNamespaceJoinSuite(object):
    def setup_class(self):
        self.namespace = Namespace()
        self.verify_template = Common.generate_jinja_template(self,
                                                              './verify_data/namespace/link_namespace_response.jinja2')
        for data in setup_ns_data:
            create_ns_result = self.namespace.create_namespace(
                './test_data/namespace/create_namespace_without_project.jinja2', data)
            if create_ns_result.status_code == 409:
                pass
            else:
                assert create_ns_result.status_code == 200, "创建不关联项目的命名空间{}失败 {}".format(data["namespace_name"],
                                                                                         create_ns_result.text)

    def teardown_class(self):
        for data in setup_ns_data:
            self.namespace.delete_namespace(data.get("namespace_name"))

    def 测试_获取没有关联项目的命名空间列表(self):
        '''
        获取没有关联项目的命名空间列表
        :return:
        '''
        exp_namelist = [link_ns_data[0].get("namespace_name"), link_ns_data[1].get("namespace_name"),
                        link_ns_data[2].get("namespace_name"), link_ns_data[3].get("namespace_name")]
        get_ns_result = self.namespace.list_namespace_not_link_project()
        assert get_ns_result.status_code == 200, "获取没有关联项目的命名空间列表失败 {}".format(get_ns_result.text)
        namelist = self.namespace.get_namelist_of_namespace(get_ns_result.json())
        assert self.namespace.is_sub_list(exp_namelist, namelist), "期望返回的命名空间名字列表是:{},实际返回的是{}".format(exp_namelist,
                                                                                                       namelist)

    def 测试_导入有ResourceQuota和LimitRange的命名空间(self, data=link_ns_data[0],
                                            namespace=link_ns_data[0].get("namespace_name")):
        '''
        测试_导入有ResourceQuota和LimitRange的命名空间
        :return:
        '''
        exp_namelist = [link_ns_data[0].get("namespace_name")]
        link_ns_result = self.namespace.link_namespace_with_project(
            './test_data/namespace/create_namespace_without_project.jinja2', data, namespace)
        assert link_ns_result.status_code == 200, "导入有ResourceQuota和LimitRange的命名空间,参数都正确时失败 {}".format(
            link_ns_result.text)
        value = self.verify_template.render(data)
        assert self.namespace.is_sub_dict(json.loads(value), link_ns_result.json()), \
            "导入有ResourceQuota和LimitRange的命名空间比对数据失败，返回数据:{},期望数据:{}".format(link_ns_result.json(), json.loads(value))
        get_ns_result = self.namespace.list_namespace_not_link_project()
        namelist = self.namespace.get_namelist_of_namespace(get_ns_result.json())
        assert not self.namespace.is_sub_list(exp_namelist, namelist), "导入命名空间后,还能在没有关联项目的命名空间列表:{}中查到该命名空间:{}".format(
            exp_namelist, namelist)

    def 测试_导入没有ResourceQuota有LimitRange的命名空间(self, data=link_ns_data[1],
                                             namespace=link_ns_data[1].get("namespace_name")):
        '''
        测试_导入没有ResourceQuota有LimitRange的命名空间
        :return:
        '''
        exp_namelist = [link_ns_data[1].get("namespace_name")]
        link_ns_result = self.namespace.link_namespace_with_project(
            './test_data/namespace/create_namespace_without_project.jinja2', data, namespace)
        assert link_ns_result.status_code == 200, "导入没有ResourceQuota,但是有LimitRange的命名空间,参数都正确时失败 {}".format(
            link_ns_result.text)
        value = self.verify_template.render(data)
        assert self.namespace.is_sub_dict(json.loads(value), link_ns_result.json()), \
            "导入没有ResourceQuota,但是有LimitRange的命名空间比对数据失败，返回数据:{},期望数据:{}".format(link_ns_result.json(),
                                                                                json.loads(value))
        get_ns_result = self.namespace.list_namespace_not_link_project()
        namelist = self.namespace.get_namelist_of_namespace(get_ns_result.json())
        assert not self.namespace.is_sub_list(exp_namelist, namelist), "导入命名空间后,还能在没有关联项目的命名空间列表:{}中查到该命名空间:{}".format(
            exp_namelist, namelist)

    def 测试_导入有ResourceQuota没有LimitRange的命名空间(self, data=link_ns_data[2],
                                             namespace=link_ns_data[2].get("namespace_name")):
        '''
        测试_导入没有ResourceQuota有LimitRange的命名空间
        :return:
        '''
        exp_namelist = [link_ns_data[2].get("namespace_name")]
        link_ns_result = self.namespace.link_namespace_with_project(
            './test_data/namespace/create_namespace_without_project.jinja2', data, namespace)
        assert link_ns_result.status_code == 200, "导入有ResourceQuota,但是没有LimitRange的命名空间,参数都正确时失败 {}".format(
            link_ns_result.text)
        value = self.verify_template.render(data)
        assert self.namespace.is_sub_dict(json.loads(value), link_ns_result.json()), \
            "导入有ResourceQuota,但是没有LimitRange的命名空间比对数据失败，返回数据:{},期望数据:{}".format(link_ns_result.json(),
                                                                                json.loads(value))
        get_ns_result = self.namespace.list_namespace_not_link_project()
        namelist = self.namespace.get_namelist_of_namespace(get_ns_result.json())
        assert not self.namespace.is_sub_list(exp_namelist, namelist), "导入命名空间后,还能在没有关联项目的命名空间列表:{}中查到该命名空间:{}".format(
            exp_namelist, namelist)

    def 测试_导入ResourceQuota和LimitRange都没有的命名空间(self, data=link_ns_data[3],
                                              namespace=link_ns_data[3].get("namespace_name")):
        '''
        测试_导入没有ResourceQuota有LimitRange的命名空间
        :return:
        '''
        exp_namelist = [link_ns_data[3].get("namespace_name")]
        link_ns_result = self.namespace.link_namespace_with_project(
            './test_data/namespace/create_namespace_without_project.jinja2', data, namespace)
        assert link_ns_result.status_code == 200, "导入ResourceQuota和LimitRange都没有的命名空间,参数都正确时失败 {}".format(
            link_ns_result.text)
        value = self.verify_template.render(data)
        assert self.namespace.is_sub_dict(json.loads(value), link_ns_result.json()), \
            "导入ResourceQuota和LimitRange都没有的命名空间比对数据失败，返回数据:{},期望数据:{}".format(link_ns_result.json(), json.loads(value))
        get_ns_result = self.namespace.list_namespace_not_link_project()
        namelist = self.namespace.get_namelist_of_namespace(get_ns_result.json())
        assert not self.namespace.is_sub_list(exp_namelist, namelist), "导入命名空间后,还能在没有关联项目的命名空间列表:{}中查到该命名空间:{}".format(
            exp_namelist, namelist)

    def 测试_从项目中移除有ResourceQuota和LimitRange的命名空间(self, namespace=link_ns_data[0].get("namespace_name")):
        '''
        测试_从项目中移除有ResourceQuota和LimitRange的命名空间
        :return:
        '''
        exp_namelist = [link_ns_data[0].get("namespace_name")]
        remove_ns_result = self.namespace.remove_namespace_from_project(namespace)
        assert remove_ns_result.status_code == 200, "从项目中移除有ResourceQuota和LimitRange的命名空间失败 {}".format(
            remove_ns_result.text)
        get_ns_result = self.namespace.list_namespace_not_link_project()
        namelist = self.namespace.get_namelist_of_namespace(get_ns_result.json())
        assert self.namespace.is_sub_list(exp_namelist, namelist), "移除命名空间后,在没有关联项目的命名空间列表:{}中查不到该命名空间:{}".format(
            exp_namelist, namelist)

    def 测试_从项目中移除没有ResourceQuota有LimitRange的命名空间(self, namespace=link_ns_data[1].get("namespace_name")):
        '''
        测试_从项目中移除没有ResourceQuota有LimitRange的命名空间
        :return:
        '''
        exp_namelist = [link_ns_data[1].get("namespace_name")]
        remove_ns_result = self.namespace.remove_namespace_from_project(namespace)
        assert remove_ns_result.status_code == 200, "从项目中移除没有ResourceQuota但有LimitRange的命名空间失败 {}".format(
            remove_ns_result.text)
        get_ns_result = self.namespace.list_namespace_not_link_project()
        namelist = self.namespace.get_namelist_of_namespace(get_ns_result.json())
        assert self.namespace.is_sub_list(exp_namelist, namelist), "移除命名空间后,在没有关联项目的命名空间列表:{}中查不到该命名空间:{}".format(
            exp_namelist, namelist)

    def 测试_从项目中移除有ResourceQuota没有LimitRange的命名空间(self, namespace=link_ns_data[2].get("namespace_name")):
        '''
        测试_从项目中移除没有ResourceQuota有LimitRange的命名空间
        :return:
        '''
        exp_namelist = [link_ns_data[2].get("namespace_name")]
        remove_ns_result = self.namespace.remove_namespace_from_project(namespace)
        assert remove_ns_result.status_code == 200, "从项目中移除有ResourceQuota但没有LimitRange的命名空间失败 {}".format(
            remove_ns_result.text)
        get_ns_result = self.namespace.list_namespace_not_link_project()
        namelist = self.namespace.get_namelist_of_namespace(get_ns_result.json())
        assert self.namespace.is_sub_list(exp_namelist, namelist), "移除命名空间后,在没有关联项目的命名空间列表:{}中查不到该命名空间:{}".format(
            exp_namelist, namelist)

    def 测试_从项目中移除ResourceQuota和LimitRange都没有的命名空间(self, namespace=link_ns_data[3].get("namespace_name")):
        '''
        测试_从项目中移除ResourceQuota和LimitRange都没有的命名空间
        :return:
        '''
        exp_namelist = [link_ns_data[1].get("namespace_name")]
        remove_ns_result = self.namespace.remove_namespace_from_project(namespace)
        assert remove_ns_result.status_code == 200, "从项目中移除ResourceQuota和LimitRange都没有的命名空间失败 {}".format(
            remove_ns_result.text)
        get_ns_result = self.namespace.list_namespace_not_link_project()
        namelist = self.namespace.get_namelist_of_namespace(get_ns_result.json())
        assert self.namespace.is_sub_list(exp_namelist, namelist), "移除命名空间后,在没有关联项目的命名空间列表:{}中查不到该命名空间:{}".format(
            exp_namelist, namelist)
