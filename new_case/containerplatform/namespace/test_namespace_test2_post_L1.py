import pytest
import json
from common.settings import RERUN_TIMES, USERNAME, REGION_NAME, AUDIT_UNABLED
from new_case.containerplatform.namespace.namespace import Namespace
from new_case.containerplatform.namespace.conftest import data_list
from common.base_request import Common


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestPostNamespaceL1Suite(object):
    def setup_class(self):
        self.namespace = Namespace()
        self.verify_template = Common.generate_jinja_template(self, './verify_data/namespace/create_response.jinja2')
        self.namespace.delete_namespace(data_list[0]["namespace_name"])
        self.namespace.delete_namespace(data_list[1]["namespace_name"])
        self.namespace.delete_namespace(data_list[2]["namespace_name"])

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 不测试命名空间创建审计(self):
        payload = {"user_name": USERNAME, "operation_type": "create", "resource_type": "namespaces",
                   "resource_name": data_list[-1]['namespace_name']}
        result = self.namespace.search_audit(payload)
        payload.update({"namespace": "", "region_name": REGION_NAME})
        values = self.namespace.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.namespace.is_sub_dict(values, result.json()), "审计数据不符合预期"

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 不测试资源配额创建审计(self):
        payload = {"user_name": USERNAME, "operation_type": "create", "resource_type": "resourcequotas",
                   "resource_name": "default"}
        result = self.namespace.search_audit(payload)
        payload.update({"namespace": "", "region_name": REGION_NAME})
        values = self.namespace.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.namespace.is_sub_dict(values, result.json()), "审计数据不符合预期"

    def 测试_创建命名空间_指定中文显示名称(self, data=data_list[0]):
        '''
        创建命名空间-指定命名空间的中文显示名称
        :return:
        '''
        create_ns_result = self.namespace.create_namespace('./test_data/namespace/create_namespace.jinja2', data)
        assert create_ns_result.status_code == 200, "创建命名空间,传入中文显示名称时失败 {}".format(create_ns_result.text)
        value = self.verify_template.render(data)
        assert self.namespace.is_sub_dict(json.loads(value), create_ns_result.json()), \
            "创建命名空间比对数据失败，返回数据:{},期望数据:{}".format(create_ns_result.json(), json.loads(value))

    def 测试_创建命名空间_不指定资源配额(self, data=data_list[1]):
        '''
        创建命名空间-不指定资源配额
        :return:
        '''
        create_ns_result = self.namespace.create_namespace(
            './test_data/namespace/create_namespace.jinja2', data)
        assert create_ns_result.status_code == 200, "创建命名空间,不指定资源配额时失败 {}".format(create_ns_result.text)
        value = self.verify_template.render(data)
        assert self.namespace.is_sub_dict(json.loads(value), create_ns_result.json()), \
            "创建命名空间比对数据失败，返回数据:{},期望数据:{}".format(create_ns_result.json(), json.loads(value))

    def 测试_创建命名空间_额外指定一个标签(self, data=data_list[2]):
        '''
        创建命名空间-额外指定一个标签
        :return:
        '''
        create_ns_result = self.namespace.create_namespace('./test_data/namespace/create_namespace.jinja2', data)
        assert create_ns_result.status_code == 200, "创建命名空间,额外指定一个标签时失败 {}".format(create_ns_result.text)
        value = self.verify_template.render(data)
        assert self.namespace.is_sub_dict(json.loads(value), create_ns_result.json()), \
            "创建命名空间比对数据失败，返回数据:{},期望数据:{}".format(create_ns_result.json(), json.loads(value))
