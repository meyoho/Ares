import pytest
import json
from common.settings import RERUN_TIMES
from common.base_request import Common
from new_case.containerplatform.namespace.namespace import Namespace
from new_case.containerplatform.namespace.conftest import data_list


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestGetNamespaceL1Suite(object):
    def setup_class(self):
        self.namespace = Namespace()
        self.verify_template = Common.generate_jinja_template(self, './verify_data/namespace/get_response.jinja2')

    def 测试_获取命名空间列表_传入limit参数(self):
        '''
        测试_获取命名空间列表_传入limit参数
        :return:
        '''
        get_ns_result = self.namespace.list_namespaces(limit=1)
        assert get_ns_result.status_code == 200, "获取命名空间列表,同时带limit参数时失败 {}".format(get_ns_result.text)
        ns_num = len(get_ns_result.json()["items"])
        assert ns_num == 1, "获取命名空间列表,传limit=1时返回失败,预期返回1个,实际返回{}个".format(ns_num)

    def 测试_获取命名空间列表_不带limit参数(self):
        '''
        获取命名空间列表_不带limit参数
        :return:
        '''
        get_ns_result = self.namespace.list_namespaces()
        assert get_ns_result.status_code == 200, "获取命名空间列表,不带任何参数时失败 {}".format(get_ns_result.text)
        ns_num = len(get_ns_result.json()["items"])
        assert ns_num >= len(data_list), "获取命名空间列表,不传limit时返回失败,预期返回{}个,实际返回{}个".format(
            len(data_list), ns_num)

    def 测试_获取命名空间列表_传入limit和continue参数(self):
        '''
        测试_获取命名空间列表_传入limit和continue参数
        :return:
        '''
        get_ns_result = self.namespace.list_namespaces(limit=1)
        assert get_ns_result.status_code == 200, "获取命名空间列表,传入limit=1参数时失败 {}".format(get_ns_result.text)
        continues = self.namespace.get_value(get_ns_result.json(), "metadata.continue")
        get_ns_result2 = self.namespace.list_namespaces(limit=1, continues=continues)
        assert get_ns_result.status_code == 200, "获取命名空间列表,同时带limit=1和continues参数时失败 {}".format(get_ns_result.text)
        assert get_ns_result.json() != get_ns_result2.json(), "获取命名空间列表,分页数据相同，第一页数据:{},第二页数据:{}".format(
            get_ns_result.json(), get_ns_result2.json())

    @pytest.mark.parametrize("data", data_list, ids=["指定中文显示名称", "不指定资源配额", "额外指定一个标签"])
    def 测试_获取指定命名空间_不带任何参数(self, data):
        '''
        获取指定命名空间_不带任何参数
        :return:
        '''
        get_ns_result = self.namespace.get_namespaces(data["namespace_name"])
        assert get_ns_result.status_code == 200, "获取指定命名空间,不带任何参数时失败 {}".format(get_ns_result.text)
        value = self.verify_template.render(data)
        assert self.namespace.is_sub_dict(json.loads(value), get_ns_result.json()), \
            "获取指定命名空间{}比对数据失败，返回数据:{},期望数据:{}".format(data["namespace_name"], get_ns_result.json(), json.loads(value))
