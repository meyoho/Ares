from common.settings import RESOURCE_PREFIX, REGION_NAME, PROJECT_NAME, USERNAME, DEFAULT_LABEL

L0_data = {
    "namespace_name": "{}-level0-general".format(RESOURCE_PREFIX).replace('_', '-'),
    "display_name": "{}-level0-general".format(RESOURCE_PREFIX).replace('_', '-'),
    "cluster": "{}".format(REGION_NAME),
    "project": "{}".format(PROJECT_NAME),
    "ResourceQuota": "True",
    "morelabel": "False",
    "default_label": DEFAULT_LABEL
}

data_list = [
    {
        "namespace_name": "{}-level1-general2".format(RESOURCE_PREFIX).replace('_', '-'),
        "display_name": "显示名称",
        "cluster": "{}".format(REGION_NAME),
        "project": "{}".format(PROJECT_NAME),
        "ResourceQuota": "True",
        "morelabel": "False",
        "default_label": DEFAULT_LABEL
    },
    {
        "namespace_name": "{}-level1-general3".format(RESOURCE_PREFIX).replace('_', '-'),
        "display_name": "{}-level1-general".format(RESOURCE_PREFIX).replace('_', '-'),
        "cluster": "{}".format(REGION_NAME),
        "project": "{}".format(PROJECT_NAME),
        "ResourceQuota": "False",
        "morelabel": "False",
        "default_label": DEFAULT_LABEL
    },
    {
        "namespace_name": "{}-level1-general4".format(RESOURCE_PREFIX).replace('_', '-'),
        "display_name": "{}-level1-general".format(RESOURCE_PREFIX).replace('_', '-'),
        "cluster": "{}".format(REGION_NAME),
        "project": "{}".format(PROJECT_NAME),
        "ResourceQuota": "True",
        "morelabel": "True",
        "default_label": DEFAULT_LABEL
    }
]

setup_ns_data = [
    {
        "namespace_name": "{}-ns1-no-project".format(RESOURCE_PREFIX).replace('_', '-'),
        "ResourceQuota": "True",
        "LimitRange": "True",
        "default_label": DEFAULT_LABEL
    },
    {
        "namespace_name": "{}-ns2-no-project".format(RESOURCE_PREFIX).replace('_', '-'),
        "ResourceQuota": "False",
        "LimitRange": "True",
        "default_label": DEFAULT_LABEL
    },
    {
        "namespace_name": "{}-ns3-no-project".format(RESOURCE_PREFIX).replace('_', '-'),
        "ResourceQuota": "True",
        "LimitRange": "False",
        "default_label": DEFAULT_LABEL
    },
    {
        "namespace_name": "{}-ns4-no-project".format(RESOURCE_PREFIX).replace('_', '-'),
        "ResourceQuota": "False",
        "LimitRange": "False",
        "default_label": DEFAULT_LABEL
    }
]

link_ns_data = [
    {
        "namespace_name": "{}-ns1-no-project".format(RESOURCE_PREFIX).replace('_', '-'),
        "username": "{}".format(USERNAME),
        "ResourceQuota": "True",
        "LimitRange": "True",
        "LinkPro": "True",
        "cluster": "{}".format(REGION_NAME),
        "project": "{}".format(PROJECT_NAME),
        "default_label": DEFAULT_LABEL
    },
    {
        "namespace_name": "{}-ns2-no-project".format(RESOURCE_PREFIX).replace('_', '-'),
        "username": "{}".format(USERNAME),
        "ResourceQuota": "False",
        "LimitRange": "True",
        "LinkPro": "True",
        "cluster": "{}".format(REGION_NAME),
        "project": "{}".format(PROJECT_NAME),
        "default_label": DEFAULT_LABEL
    },
    {
        "namespace_name": "{}-ns3-no-project".format(RESOURCE_PREFIX).replace('_', '-'),
        "username": "{}".format(USERNAME),
        "ResourceQuota": "True",
        "LimitRange": "False",
        "LinkPro": "True",
        "cluster": "{}".format(REGION_NAME),
        "project": "{}".format(PROJECT_NAME),
        "default_label": DEFAULT_LABEL
    },
    {
        "namespace_name": "{}-ns4-no-project".format(RESOURCE_PREFIX).replace('_', '-'),
        "username": "{}".format(USERNAME),
        "ResourceQuota": "False",
        "LimitRange": "False",
        "LinkPro": "True",
        "cluster": "{}".format(REGION_NAME),
        "project": "{}".format(PROJECT_NAME),
        "default_label": DEFAULT_LABEL
    }
]
