import pytest
from common.settings import RERUN_TIMES
from common.base_request import Common
from new_case.containerplatform.namespace.namespace import Namespace
from new_case.containerplatform.namespace.conftest import data_list
import json


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestPatchNamespaceL1Suite(object):
    def setup_class(self):
        self.namespace = Namespace()
        self.verify_template = Common.generate_jinja_template(self,
                                                              './verify_data/namespace/patch_limitRange_response.jinja2')

    def 测试_更新命名空间容器限额(self):
        '''
        更新命名空间容器限额
        :return:
        '''
        data_list[1].update(
            {"limitRange_default_cpu": "200m", "limitRange_default_mem": "200Mi", "limitRange_request_cpu": "200m",
             "limitRange_request_mem": "200Mi", "max_cpu": "2", "max_mem": "2Gi"})
        update_ns_result = self.namespace.update_limitrange(data_list[1]["namespace_name"],
                                                            './test_data/namespace/update_limitRange.jinja2',
                                                            data=data_list[1])
        assert update_ns_result.status_code == 200, "更新命名空间容器限额时失败 {}".format(update_ns_result.text)
        value = self.verify_template.render(data_list[1])
        assert self.namespace.is_sub_dict(json.loads(value), update_ns_result.json()), \
            "更新命名空间limitRange比对数据失败，返回数据:{},期望数据:{}".format(update_ns_result.json(), json.loads(value))
