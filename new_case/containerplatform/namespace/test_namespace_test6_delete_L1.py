import pytest
from common.settings import RERUN_TIMES, USERNAME, REGION_NAME, AUDIT_UNABLED
from new_case.containerplatform.namespace.conftest import data_list
from new_case.containerplatform.namespace.namespace import Namespace
from common.base_request import Common


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestDeleteNamespaceL1Suite(object):
    def setup_class(self):
        self.namespace = Namespace()
        self.verify_template = Common.generate_jinja_template(self, './verify_data/namespace/delete_response.jinja2')

    def 测试_删除显示名称是中文的命名空间(self):
        '''
        测试_删除显示名称是中文的命名空间
        :return:
        '''
        delete_ns_result = self.namespace.delete_namespace(data_list[0]["namespace_name"])
        assert delete_ns_result.status_code == 200, "删除显示名称是中文的命名空间时失败 {}".format(delete_ns_result.text)
        value = self.namespace.generate_jinja_data("verify_data/namespace/delete_response.jinja2", data_list[0])
        assert self.namespace.is_sub_dict(value,
                                          delete_ns_result.json()), "删除显示名称是中文的命名空间比对数据失败，返回数据:{},期望数据:{}".format(
            delete_ns_result.json(), value)

    def 测试_删除没有设置资源配额的命名空间(self):
        '''
        测试_删除没有设置资源配额的命名空间
        :return:
        '''
        delete_ns_result = self.namespace.delete_namespace(data_list[1]["namespace_name"])
        assert delete_ns_result.status_code == 200, "删除没有设置资源配额的命名空间时失败 {}".format(delete_ns_result.text)
        value = self.namespace.generate_jinja_data("verify_data/namespace/delete_response.jinja2", data_list[1])
        assert self.namespace.is_sub_dict(value,
                                          delete_ns_result.json()), "删除没有设置资源配额的命名空间比对数据失败，返回数据:{},期望数据:{}".format(
            delete_ns_result.json(), value)

    def 测试_删除包含额外标签和注解的命名空间(self):
        '''
        测试_删除包含额外标签和注解的命名空间
        :return:
        '''
        delete_ns_result = self.namespace.delete_namespace(data_list[2]["namespace_name"])
        assert delete_ns_result.status_code == 200, "删除包含额外标签和注解的命名空间时失败 {}".format(delete_ns_result.text)
        value = self.namespace.generate_jinja_data("verify_data/namespace/delete_response.jinja2", data_list[2])
        assert self.namespace.is_sub_dict(value,
                                          delete_ns_result.json()), "删除包含额外标签和注解的命名空间比对数据失败，返回数据:{},期望数据:{}".format(
            delete_ns_result.json(), value)

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 测试命名空间删除审计(self):
        payload = {"user_name": USERNAME, "operation_type": "delete", "resource_type": "namespaces",
                   "resource_name": data_list[3]['namespace_name']}
        result = self.namespace.search_audit(payload)
        payload.update({"namespace": data_list[3]['namespace_name'], "region_name": REGION_NAME})
        values = self.namespace.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.namespace.is_sub_dict(values, result.json()), "审计数据不符合预期"
