import pytest
from common.settings import RERUN_TIMES, USERNAME, AUDIT_UNABLED
from common.base_request import Common
from new_case.containerplatform.namespace.namespace import Namespace
from new_case.containerplatform.namespace.conftest import data_list
import json


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestPatchNamespaceL1Suite(object):
    def setup_class(self):
        self.namespace = Namespace()
        self.verify_template = Common.generate_jinja_template(self, './verify_data/namespace/patch_resourcequota_response.jinja2')

    def 测试_更新命名空间资源配额的所有配额(self):
        '''
        测试_更新命名空间资源配额的所有配额
        :return:
        '''
        data_list[0].update(
            {"limits_cpu": "10", "request_cpu": "10", "limits_mem": "10Gi", "requests_mem": "10Gi", "pvc": "10",
             "pod": "10", "requests_storage": "10"})

        update_ns_result = self.namespace.update_resourcequota(data_list[0]["namespace_name"],
                                                               './test_data/namespace/update_resourcequota.jinja2',
                                                               data=data_list[0])
        assert update_ns_result.status_code == 200, "更新命名空间资源配额的所有配额时失败 {}".format(update_ns_result.text)
        self.namespace.wait_resourcequota_status(data_list[0]["namespace_name"])
        get_resourcequota_result = self.namespace.detail_resourcequota(data_list[0]["namespace_name"])
        value = self.verify_template.render(data_list[0])
        assert self.namespace.is_sub_dict(json.loads(value), get_resourcequota_result.json()), \
            "更新命名空间比对数据失败，返回数据:{},期望数据:{}".format(update_ns_result.json(), json.loads(value))

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 测试资源配额更新审计(self):
        payload = {"user_name": USERNAME, "operation_type": "patch", "resource_type": "resourcequotas",
                   "resource_name": "default"}
        result = self.namespace.search_audit(payload)
        payload.update({"namespace": "", "region_name": ""})
        values = self.namespace.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.namespace.is_sub_dict(values, result.json()), "审计数据不符合预期"
