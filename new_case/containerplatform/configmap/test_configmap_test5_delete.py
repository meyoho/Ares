import pytest

from common import settings
from new_case.containerplatform.configmap.configmap import Configmap


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestDeleteConfigmap(object):
    data_list = [
        {
            "configmap_name": "{}-ares-configmap-2".format(settings.RESOURCE_PREFIX),
            "description": "conf文件2",
            "namespace": settings.K8S_NAMESPACE
        },
        {
            "configmap_name": "{}-ares-configmap-3".format(settings.RESOURCE_PREFIX),
            "description": "key-value",
            "namespace": settings.K8S_NAMESPACE,
            "configmap_key": "key",
            "configmap_value": "value"
        },
        {
            "configmap_name": "{}-ares-configmap-4".format(settings.RESOURCE_PREFIX),
            "description": "多个配置项",
            "namespace": settings.K8S_NAMESPACE,
            "configmap_key": "new_key",
            "configmap_value": "new_value"
        }
    ]
    casename = ["导入数据", "无描述", "多个配置项(导入数据和key-value模式)"]

    def setup_class(self):
        self.configmap_tool = Configmap()
        self.namespace = settings.K8S_NAMESPACE

    def teardown_class(self):
        for data in self.data_list:
            self.configmap_tool.delete_configmap(data["configmap_name"])

    @pytest.mark.parametrize("data", data_list, ids=casename)
    def 测试删除不同类型的配置字典(self, data):
        # 分别删除不同类型的配置字典: key-value, conf文件, 没有描述信息和多配置项的配置字典
        ret = self.configmap_tool.delete_configmap(configmap_name=data['configmap_name'])

        assert ret.status_code == 200, "删除{}的配置字典{}失败:{}".format(data["description"], data['configmap_name'], ret.text)

        value = self.configmap_tool.generate_jinja_data('verify_data/configmap/delete_response.jinja2', data)
        assert self.configmap_tool.is_sub_dict(value, ret.json()), \
            "删除configmap比对数据失败，返回数据:{},期望数据:{}".format(ret.json(), value)

    @pytest.mark.skipif(settings.AUDIT_UNABLED, reason="do not have audit")
    def 测试配置字典删除审计(self):
        payload = {"user_name": settings.USERNAME, "operation_type": "delete", "resource_type": "configmaps",
                   "resource_name": self.data_list[0]['configmap_name']}
        result = self.configmap_tool.search_audit(payload)
        payload.update({"namespace": self.namespace, "region_name": settings.REGION_NAME})
        values = self.configmap_tool.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.configmap_tool.is_sub_dict(values, result.json()), "审计数据不符合预期"
