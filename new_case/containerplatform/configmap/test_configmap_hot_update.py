import pytest
import json
import copy
from new_case.containerplatform.configmap.configmap import Configmap
from new_case.containerplatform.application.application import Application
from common import settings
from common.log import logger
import time


@pytest.mark.erebus
@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestConfigmapHostUpdate(object):
    def setup_class(self):
        self.configmap_tool = Configmap()
        self.application = Application()
        self.namespace = settings.K8S_NAMESPACE
        self.verify_template = self.application.generate_jinja_template(
            './verify_data/configmap/create_response.jinja2'
        )
        self.app_name = "{}-configmap-hot-update".format(settings.RESOURCE_PREFIX)
        self.configmap_name = "{}-ares-configmap-hot-update".format(settings.RESOURCE_PREFIX)

    @pytest.mark.metis
    def 测试配置字典热更新(self):
        self.configmap_tool.delete_configmap(self.configmap_name)
        self.application.delete_app(self.application.k8s_namespace, self.app_name)
        cm_data = {
            "configmap_name": self.configmap_name,
            "description": "hot-update",
            "namespace": self.application.k8s_namespace,
            "configmap_key": "hot-update.conf",
            "configmap_value": "config=abcdef"
        }
        ret = self.configmap_tool.create_configmap(data=cm_data)

        assert ret.status_code == 201, "创建类配置字典失败:{}".format(ret.text)
        value = self.verify_template.render(cm_data)
        assert self.configmap_tool.is_sub_dict(json.loads(value), ret.json()), "创建configmap比对数据失败，返回数据:{},期望数据:{}". \
            format(ret.json(), json.loads(value))

        app_data = {
            "app_name": self.app_name,
            "ns_name": self.configmap_tool.k8s_namespace,
            "resources": [
                {
                    "deploy_name": self.app_name,
                    "ns_name": self.configmap_tool.k8s_namespace,
                    "project": self.configmap_tool.project_name,
                    "app_name": self.app_name,
                    "containers": [
                        {
                            "image": settings.IMAGE,
                            "volumeMounts": [
                                {
                                    "name": "configmap-%s" % self.configmap_name,
                                    "mountPath": "/home/"
                                }
                            ]
                        }
                    ],
                    "volumes": [
                        {
                            "name": "configmap-%s" % self.configmap_name,
                            "configMap": {
                                "name": self.configmap_name
                            }
                        }
                    ]
                },
                {
                    "service_name": self.app_name,
                    "ns_name": self.configmap_tool.k8s_namespace,
                    "app_name": self.app_name,
                    "deploy_name": self.app_name,
                    "ports": [
                        {
                            "protocol": "TCP",
                            "port": 80,
                            "targetPort": 80
                        }
                    ]
                },
                {
                    "ingress_name": self.app_name,
                    "ns_name": self.configmap_tool.k8s_namespace,
                    "host": '.'.join([self.app_name, self.configmap_tool.k8s_namespace]),
                    "url_path": '/',
                    "service_name": self.app_name,
                    "service_port": 80
                }
            ]
        }
        ret = self.application.create_app_by_template(app_data)
        # logger.info(app_data)
        logger.info("create configmap hot update application: \n" + self.application.render_app_template(app_data))
        assert ret.status_code == 200, "configmap热更新应用创建失败 {}".format(ret.text)
        app_status = self.application.get_app_status(self.application.k8s_namespace, self.app_name, 'Running')
        assert app_status, "创建应用后，验证应用状态出错：app: {} is not running".format(self.app_name)
        if not settings.VM_IPS:
            pytest.skip("没有传虚拟机IP，不需要验证configmap是否生效")
        ret = self.application.kubectl_exec(
            self.app_name,
            self.application.k8s_namespace,
            "cat /home/hot-update.conf",
            "config=abcdef",
            settings.VM_IPS.split(";")[0]
        )
        assert ret[0], "应用挂载configmap失败。预期值:%s 实际值:%s" % ("config=abcdef", ret[1])
        update_data = copy.copy(cm_data)
        update_data["configmap_value"] = "config=123456"
        update_result = self.configmap_tool.update_configmap(self.configmap_name, update_data)
        assert update_result.status_code == 200, "更新配置字典出错:{}".format(update_result.text)
        logger.warning(update_result.text)
        assert update_result.json()["data"]["hot-update.conf"] == "config=123456", \
            "更新configmap比对数据失败，返回数据:{},期望数据:{}".format(
                update_result.json()["data"]["hot-update.conf"],
                "config=123456"
            )
        s_time = time.time()
        while time.time() < s_time + 120:
            ret = self.application.kubectl_exec(
                self.app_name,
                self.application.k8s_namespace,
                "cat /home/hot-update.conf",
                "config=123456",
                settings.VM_IPS.split(";")[0]
            )
            logger.info(ret)
            if ret[0] is True:
                break
            time.sleep(10)

        assert ret[0], "configmap热更新失败。预期值:%s 实际值:%s" % ("config=123456", ret[1])

    def teardown_class(self):
        self.application.delete_app(self.application.k8s_namespace, self.app_name)
        self.configmap_tool.delete_configmap(self.configmap_name)
