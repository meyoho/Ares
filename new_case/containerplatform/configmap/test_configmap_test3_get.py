import pytest
import json
from new_case.containerplatform.configmap.configmap import Configmap
from new_case.containerplatform.deployment.deployment import Deployment
from common import settings
from common.base_request import Common


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestGetConfigmap(object):
    data_list = [
        {
            "configmap_name": "{}-ares-configmap-2".format(settings.RESOURCE_PREFIX),
            "description": "conf文件1",
            "namespace": settings.K8S_NAMESPACE,
            "default_label": settings.DEFAULT_LABEL
        },
        {
            "configmap_name": "{}-ares-configmap-3".format(settings.RESOURCE_PREFIX),
            "description": "",
            "namespace": settings.K8S_NAMESPACE,
            "configmap_key": "key",
            "configmap_value": "value",
            "default_label": settings.DEFAULT_LABEL
        },
        {
            "configmap_name": "{}-ares-configmap-4".format(settings.RESOURCE_PREFIX),
            "description": "多个配置项",
            "namespace": settings.K8S_NAMESPACE,
            "configmap_key": "key",
            "configmap_value": "value",
            "default_label": settings.DEFAULT_LABEL
        }
    ]
    casename = ["导入数据", "无描述", "多个配置项(导入数据和key-value模式)"]

    def setup_class(self):
        self.configmap_tool = Configmap()
        self.deployment = Deployment()
        self.namespace = settings.K8S_NAMESPACE
        self.verify_template = Common.generate_jinja_template(self, './verify_data/configmap/get_response.jinja2')
        self.configmap_name = "{}-ares-deployment-configmap".format(settings.RESOURCE_PREFIX)

        self.region_name = settings.REGION_NAME
        self.k8s_namespace = settings.K8S_NAMESPACE

    @pytest.mark.parametrize("data", data_list, ids=casename)
    def 测试获取不同类型的配置字典详情(self, data):
        # get key_value, conf文件, 描述信息为空和包含多个配置项类型的配置字典
        ret = self.configmap_tool.get_configmap_detail(configmap_name=data['configmap_name'])

        assert ret.status_code == 200, "获取配置字典{}详情失败:{}".format(data['configmap_name'], ret.text)

        value = self.verify_template.render(data)

        assert self.configmap_tool.is_sub_dict(json.loads(value), ret.json()), \
            "创建configmap比对数据失败，返回数据:{},期望数据:{}".format(ret.json(), value)

    def 测试获取配置字典列表_有limit参数(self):
        # 传limit参数,get配置字典列表
        ret = self.configmap_tool.get_configmap_list(params={"limit": 20})

        assert ret.status_code == 200, "获取配置字典列表失败:{}".format(ret.text)
        for data in self.data_list:
            assert data["configmap_name"] in ret.text, "配置字典{}不在配置字典列表中".format(data["configmap_name"])

    def 测试获取配置字典列表_有limit和continue参数(self):
        ret2 = self.configmap_tool.get_configmap_list(params={"limit": 1})
        cm_num = len(ret2.json()["items"])
        assert cm_num == 1, "获取配置字典列表,limit=1时失败,预期返回1个配置字典,实际返回{}个".format(cm_num)
        cnt = self.configmap_tool.get_value(ret2.json(), "metadata.continue")
        ret = self.configmap_tool.get_configmap_list(params={"limit": 20, "continue": cnt})
        assert ret.status_code == 200, "获取配置字典列表失败:{}".format(ret.text)
        cm_num = len(ret.json()["items"])
        assert cm_num >= len(self.data_list) - 1, "获取配置字典列表,不传limit时返回失败,预期至少返回{}个,实际返回{}个". \
            format(len(self.data_list), cm_num)

    def 测试获取配置字典详情_计算组件(self):
        result = {"flag": True}
        # 创建configmap
        cm_data = {
            "configmap_name": self.configmap_name,
            "description": "key-value",
            "namespace": settings.K8S_NAMESPACE,
            "configmap_key": "key",
            "configmap_value": "value"
        }
        self.configmap_tool.delete_configmap(self.configmap_name)
        ret = self.configmap_tool.create_configmap(data=cm_data)
        assert ret.status_code == 201, "创建配置字典失败:{}".format(ret.text)

        value = self.verify_template.render(cm_data)
        assert self.configmap_tool.is_sub_dict(json.loads(value), ret.json()), \
            "创建configmap比对数据失败，返回数据:{},期望数据:{}".format(ret.json(), value)

        # 创建deployment
        data_deployment = {
            "deployment_name": '{}-deployment-configmap'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
            "namespace": settings.K8S_NAMESPACE,
            "image": settings.IMAGE,
            "envFrom": " ",
            "configMapRef": " ",
            "configmap_name": self.configmap_name
        }
        self.deployment.delete_deplyment(ns_name=data_deployment['namespace'],
                                         deployment_name=data_deployment['deployment_name'],
                                         region_name=self.region_name)
        create_result = self.deployment.create_deployment_jinja2("./test_data/deployment/deployment.jinja2",
                                                                 data=data_deployment, region_name=self.region_name,
                                                                 ns_name=self.k8s_namespace)
        assert create_result.status_code == 201, "创建{}部署失败{}".format(data_deployment['deployment_name'],
                                                                     create_result.text)

        value = self.deployment.generate_jinja_data("./verify_data/deployment/create_deployment.jinja2",
                                                    data_deployment)
        assert self.deployment.is_sub_dict(value, create_result.json()), \
            "创建Deployment_有一个配置引用configmap比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

        ret = self.configmap_tool.get_configmap_workload(configmap_name=cm_data['configmap_name'])
        assert ret.status_code == 200, "获取配置字典详情_计算组件信息失败:{}".format(ret.text)
        length = len(ret.json()['nodes'])
        assert length == 2, "获取数据失败,应该只有 2 个节点，返回数据{}".format(ret.json())

        # 删除configmap
        del_config_ret = self.configmap_tool.delete_configmap(configmap_name=cm_data['configmap_name'])
        assert del_config_ret.status_code == 200, "删除配置字典{}失败:{}".format(cm_data['configmap_name'], del_config_ret.text)
        # 删除deployment
        del_deploy_ret = self.deployment.delete_deplyment(ns_name=data_deployment['namespace'],
                                                          deployment_name=data_deployment['deployment_name'],
                                                          region_name=self.region_name)
        assert del_deploy_ret.status_code == 200, "删除部署:{}失败{}".format(data_deployment['deployment_name'],
                                                                       del_deploy_ret.text)
        assert result['flag'], result

    def 测试_搜索configmap(self):
        data = self.data_list[0]
        ser = self.configmap_tool.search_configmap_jinja2_v1(ns_name=data["namespace"], limit=20,
                                                             configmap_name=data["configmap_name"])
        assert ser.status_code == 200, "没有搜索到{}，{}".format(data["configmap_name"], ser.text)

        value = self.configmap_tool.generate_jinja_data("./verify_data/configmap/search_configmap.jinja2", data)
        assert self.configmap_tool.is_sub_dict(value, ser.json()), "搜索configmap比对数据失败，返回数据:{},期望数据:{}". \
            format(ser.json(), value)
