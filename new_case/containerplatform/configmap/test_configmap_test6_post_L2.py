import pytest
from new_case.containerplatform.configmap.configmap import Configmap
from common import settings


@pytest.mark.archon
@pytest.mark.Low
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestPostConfigmap(object):
    # 正向测试用例
    l2_data_list_positive = [
        {
            "configmap_name": "a123456789012345678901234567890123456789012345678901234567890123",
            "description": "名称为64个英文字符",
            "namespace": settings.K8S_NAMESPACE,
            "configmap_key": "key",
            "configmap_value": "value",
            "status_code": 201
        },
        {
            "configmap_name": "{}-ares-configmap-253chars".format(settings.RESOURCE_PREFIX),
            "description": "Key带有253个英文字符",
            "namespace": settings.K8S_NAMESPACE,
            "configmap_key": "a123456789012345678901234567890123456789012345678901234567890"
                             "123456789012345678901234567890123456789012345678901234567890"
                             "123456789012345678901234567890123456789012345678901234567890"
                             "123456789012345678901234567890123456789012345678901234567890"
                             "123456789012",
            "configmap_value": "value",
            "status_code": 201
        },
        {
            "configmap_name": "{}-ares-configmap-data-null".format(settings.RESOURCE_PREFIX),
            "description": "配置字典data字段为空",
            "namespace": settings.K8S_NAMESPACE,
            "data": "null",
            "status_code": 201
        },
        {
            "configmap_name": "{}-ares-configmap-nullfile".format(settings.RESOURCE_PREFIX),
            "description": "创建配置字典导入文件且文件内容为空",
            "namespace": settings.K8S_NAMESPACE,
            "configmap_key": "null.conf",
            "configmap_value": "",
            "status_code": 201
        }
    ]
    l2_casename_positive = ["名称为64个英文字符",
                            "Key带有253个英文字符",
                            "创建配置字典data字段为空",
                            "创建配置字典导入文件且文件内容为空"]
    # 逆向测试用例
    l2_data_list_negative = [
        {
            "configmap_name": "",
            "description": "创建配置字典名称为空",
            "namespace": settings.K8S_NAMESPACE,
            "configmap_key": "key",
            "configmap_value": "value",
            "message": r"ConfigMap \"\" is invalid: metadata.name: Required value: name or generateName is required",
            "details": True,
            "causes": True,
            "reason_2": "FieldValueRequired",
            "message_2": "Required value: name or generateName is required",
            "field": "metadata.name",
            "status_code": 422
        },
        {
            "configmap_name": "a1234567890123456789012345678901234567890123456789012345678"
                              "901234567890123456789012345678901234567890123456789012345678"
                              "901234567890123456789012345678901234567890123456789012345678"
                              "901234567890123456789012345678901234567890123456789012345678"
                              "901234567890123",
            "description": "创建配置字典名称为254个英文字符",
            "namespace": settings.K8S_NAMESPACE,
            "configmap_key": "key",
            "configmap_value": "value",
            "messasge": r"ConfigMap \"a12345678901234567890123456789012345678901234567890123456789012345678901234567890"
                        r"123456789012345678901234567890123456789012345678901234567890123456789012345678901234567"
                        r"89012345678901234567890123456789012345678901234567890123456789012345678901234567890123\" "
                        r"is invalid: metadata.name: Invalid value: \"a1234567890123456789012345678901234567890"
                        r"123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890"
                        r"123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890"
                        r"123456789012345678901234567890123\": must be no more than 253 characters",
            "details": True,
            "causes": True,
            "reason_2": "FieldValueInvalid",
            "message_2": r"Invalid value: \"a1234567890123456789012345678901234567890123456789012345678901234567890123"
                         r"456789012345678901234567890123456789012345678901234567890123456789012345678901"
                         r"234567890123456789012345678901234567890123456789012345678901234567890123456789012"
                         r"345678901234567890123\": must be no more than 253 characters",
            "field": "metadata.name",
            "status_code": 422
        },
        {
            "configmap_name": "{}-ares-configmap-duplicate-name".format(settings.RESOURCE_PREFIX),
            "description": "创建配置字典与已存在的同名",
            "namespace": settings.K8S_NAMESPACE,
            "configmap_key": "key",
            "configmap_value": "value",
            "message": r"configmaps \"{}-ares-configmap-duplicate-name\" already exists".format(
                settings.RESOURCE_PREFIX),
            "details": True,
            "kind_2": "configmaps",
            "reason": "AlreadyExists",
            "status_code": 409,
            "duplicate_name": True
        },
        {
            "configmap_name": "A",
            "description": "创建配置字典名称包含非法字符",
            "namespace": settings.K8S_NAMESPACE,
            "configmap_key": "key",
            "configmap_value": "value",
            "message": r"ConfigMap \"A\" is invalid: metadata.name: Invalid value: \"A\": a DNS-1123 subdomain must "
                       r"consist of lower case alphanumeric characters, '-' or '.', and must start and end with an "
                       r"alphanumeric character (e.g. 'example.com', regex used for validation is '[a-z0-9](["
                       r"-a-z0-9]*[a-z0-9])?(\\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*')",
            "details": True,
            "causes": True,
            "reason_2": "FieldValueInvalid",
            "message_2": r"Invalid value: \"A\": a DNS-1123 subdomain must consist of lower case alphanumeric "
                         r"characters, '-' or '.', and must start and end with an alphanumeric character (e.g. "
                         r"'example.com', regex used for validation is '[a-z0-9]([-a-z0-9]*[a-z0-9])?(\\.[a-z0-9](["
                         r"-a-z0-9]*[a-z0-9])?)*')",
            "field": "metadata.name",
            "status_code": 422
        },
        {
            "configmap_name": "{}-ares-configmap-invalidkey".format(settings.RESOURCE_PREFIX),
            "description": "创建配置字典key含有非法字符",
            "namespace": settings.K8S_NAMESPACE,
            "configmap_key": r'"*"',
            "configmap_value": "value",
            "message": r"ConfigMap \"{}-ares-configmap-invalidkey\" is invalid: data[*]: Invalid value: \"*\": a "
                       r"valid config key must consist of alphanumeric characters, '-', '_' or '.' (e.g. 'key.name',  "
                       r"or 'KEY_NAME',  or 'key-name', regex used for validation is"
                       r" '[-._a-zA-Z0-9]+')".format(settings.RESOURCE_PREFIX),
            "details": True,
            "causes": True,
            "reason_2": "FieldValueInvalid",
            "message_2": r"Invalid value: \"*\": a valid config key must consist of alphanumeric characters, '-', "
                         r"'_' or '.' (e.g. 'key.name',  or 'KEY_NAME',  or 'key-name', regex used for validation is "
                         r"'[-._a-zA-Z0-9]+')",
            "field": "data[*]",
            "status_code": 422
        },
        {
            "configmap_name": "{}-ares-configmap-254chars".format(settings.RESOURCE_PREFIX),
            "description": "创建配置字典的key具有254个英文字符",
            "namespace": settings.K8S_NAMESPACE,
            "configmap_key": "a1234567890123456789012345678901234567890123456789012345678"
                             "901234567890123456789012345678901234567890123456789012345678"
                             "901234567890123456789012345678901234567890123456789012345678"
                             "901234567890123456789012345678901234567890123456789012345678"
                             "901234567890123",
            "configmap_value": "value",
            "message": r"ConfigMap \"{}-ares-configmap-254chars\" is invalid: data["
                       r"a1234567890123456789012345678901234567890123456789012345678901234"
                       r"567890123456789012345678901234567890123456789012345678901234567890"
                       r"1234567890123456789012345678901234567890123456789012345678901234567"
                       r"89012345678901234567890123456789012345678901234567890123]: Invalid "
                       r"value: \"a1234567890123456789012345678901234567890123456789012345678"
                       r"901234567890123456789012345678901234567890123456789012345678901234567890"
                       r"12345678901234567890123456789012345678901234567890123456789012345678901234"
                       r"5678901234567890123456789012345678901234567890123\": must be no more "
                       r"than 253 characters".format(settings.RESOURCE_PREFIX),
            "details": True,
            "causes": True,
            "reason_2": "FieldValueInvalid",
            "message_2": r"Invalid value: "
                         r"\"a1234567890123456789012345678901234567890123456789012345678901234567890"
                         r"12345678901234567890123456789012345678901234567890123456789012345678901234"
                         r"56789012345678901234567890123456789012345678901234567890123456789012345678"
                         r"90123456789012345678901234567890123\": must be no more than 253 characters",
            "field": r"data[a1234567890123456789012345678901234567890123456789012345678901234567890123"
                     r"45678901234567890123456789012345678901234567890123456789012345678901234567890123"
                     r"45678901234567890123456789012345678901234567890123456789012345678901234567890123"
                     r"45678901234567890123]",
            "status_code": 422
        },
        {
            "configmap_name": "{}-ares-configmap-nullkey".format(settings.RESOURCE_PREFIX),
            "description": "创建配置字典key为空",
            "namespace": settings.K8S_NAMESPACE,
            "configmap_key": "null",
            "configmap_value": "value",
            "message": r"ConfigMap \"{}-ares-configmap-nullkey\" is invalid: data[]: Invalid value: \"\": a valid "
                       r"config key must consist of alphanumeric characters, '-', '_' or '.' (e.g. 'key.name',  "
                       r"or 'KEY_NAME',  or 'key-name', regex used for validation is"
                       r" '[-._a-zA-Z0-9]+')".format(settings.RESOURCE_PREFIX),
            "details": True,
            "causes": True,
            "reason_2": "FieldValueInvalid",
            "message_2": r"Invalid value: \"\": a valid config key must consist of alphanumeric characters, '-', "
                         r"'_' or '.' (e.g. 'key.name',  or 'KEY_NAME',  or 'key-name', regex used for validation is "
                         r"'[-._a-zA-Z0-9]+')",
            "field": "data[]",
            "status_code": 422
        }]
    l2_casename_negative = ["创建配置字典名称为空",
                            "创建配置字典名称为254个英文字符",
                            "创建配置字典与已存在的同名",
                            "创建配置字典名称包含非法字符",
                            "创建配置字典key含有非法字符",
                            "创建配置字典的key具有254个英文字符",
                            "创建配置字典key为空"
                            ]
    l2_data_list = l2_data_list_positive + l2_data_list_negative
    l2_casename = l2_casename_positive + l2_casename_negative

    def setup_class(self):
        self.configmap_tool = Configmap()
        self.namespace = settings.K8S_NAMESPACE
        self.teardown_class(self)

    def teardown_class(self):
        for item in self.l2_data_list:
            if item["configmap_name"]:
                self.configmap_tool.delete_configmap(item['configmap_name'])

    @pytest.mark.parametrize("data", l2_data_list, ids=l2_casename)
    def 测试不同类型的配置字典的创建L2(self, data):
        if data.get("duplicate_name", ""):
            self.configmap_tool.create_configmap(data=data)
        ret = self.configmap_tool.create_configmap(data=data)
        expected_code = data['status_code']
        assert ret.status_code == expected_code, "创建类配置字典,描述信息为空时失败:{}".format(ret.text)
        verify_template_name = 'create_response.jinja2' if expected_code < 300 else 'create_response_failure.jinja2'
        verify_template_path = './verify_data/configmap/{}'.format(verify_template_name)
        value = self.configmap_tool.generate_jinja_data(verify_template_path, data)
        assert self.configmap_tool.is_sub_dict(value, ret.json()), \
            "创建configmap比对数据失败，返回数据:{},期望数据:{}".format(ret.json(), value)
