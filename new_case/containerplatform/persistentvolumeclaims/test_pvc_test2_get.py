import pytest
from common import settings
from new_case.containerplatform.persistentvolumeclaims.pvc import Pvc


@pytest.mark.storage
@pytest.mark.acp_containerplatform
@pytest.mark.skipif(not settings.SC_NAME, reason="没有存储类，不运行当前用例")
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestPvcSuiteGet(object):
    data_list_2 = Pvc.data_list_labels

    def setup_class(self):
        self.pvc = Pvc()

    def 测试_获取pvc列表_有limit参数(self):
        ret = self.pvc.get_pvc_list(limit=20)
        assert ret.status_code == 200, "获取pvc列表失败:{}".format(ret.text)
        ret2 = self.pvc.get_pvc_list(limit=1)
        pvc_num = len(ret2.json()["items"])
        assert pvc_num == 1, "获取pvc列表,limit=1时失败,预期返回1个pvc,实际返回{}个".format(pvc_num)

        global name
        name = Pvc.get_value(ret2.json(), "items.0.metadata.name")

        global cnt
        cnt = Pvc.get_value(ret2.json(), "metadata.continue")

    def 测试_获取pvc列表_有limit参数和continue参数(self):
        ret = self.pvc.get_pvc_list(limit=1, cnt=cnt)
        assert ret.status_code == 200, "获取pvc列表_有limit和continue参数:{}".format(ret.text)
        name_continue = Pvc.get_value(ret.json(), "items.0.metadata.name")
        assert name_continue != name, "分页数据相同，pvc第一页数据:{},pvc第二页数据:{}".format(name, name_continue)

    def 测试_搜索pvc(self):
        data = self.data_list_2[0]
        ser = self.pvc.search_pvc_jinja2_v1(ns_name=data["namespace"], limit=20, pvc_name=data["pvc_name"])
        assert ser.status_code == 200, "没有搜索到{}，{}".format(data["pvc_name"], ser.text)

        value = self.pvc.generate_jinja_data("./verify_data/pvc/search_pvc.jinja2", data)
        assert self.pvc.is_sub_dict(value, ser.json()), "搜索pvc比对数据失败，返回数据:{},期望数据:{}".format(ser.json(), value)
