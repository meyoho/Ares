import sys
from common.base_request import Common
from common.log import logger
from common import settings

project_name = settings.PROJECT_NAME


class Pvc(Common):
    def get_common_pvc_url_v1(self, ns_name=settings.K8S_NAMESPACE, pvc_name=''):
        return 'kubernetes/{}/api/v1/namespaces/{}/persistentvolumeclaims/{}'.format(self.region_name, ns_name,
                                                                                     pvc_name)

    def get_common_pvc_list_url(self, ns_name=settings.K8S_NAMESPACE, pvc_name='', limit=1,
                                project=settings.PROJECT_NAME, cnt=""):
        return limit and "kubernetes/{}/api/v1/namespaces/{}/persistentvolumeclaims/{}?" \
                         "limit={}&project={}&fieldSelector=&continue={}" \
            .format(self.region_name, ns_name, pvc_name, limit, project, cnt)

    def get_common_pvc_search_url_v1(self, ns_name=settings.K8S_NAMESPACE, limit=1, project=settings.PROJECT_NAME,
                                     pvc_name=''):
        return limit and "acp/v1/resources/search/kubernetes/{}/api/v1/namespaces/{}/persistentvolumeclaims?" \
                         "limit={}&project={}&keyword={}&field=metadata.name".format(self.region_name, ns_name, limit,
                                                                                     project, pvc_name)

    def list_pvc(self, ns_name, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_pvc_url_v1(ns_name)
        return self.send(method='get', path=url, auth=auth)

    def create_pvc(self, file, data, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_pvc_url_v1()
        data = self.generate_data(file, data)
        return self.send(method='post', path=url, data=data, auth=auth)

    def create_pvc_jinja2(self, file, data):
        path = self.get_common_pvc_url_v1()
        data = self.generate_jinja_data(file, data)
        logger.info(data)
        return self.send(method='post', path=path, json=data, params={})

    def get_pvc_detail(self, ns_name, pvc_name, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_pvc_url_v1(ns_name, pvc_name)
        return self.send(method='get', path=url, auth=auth)

    def get_pvc_detail_jinja2(self, ns_name, pvc_name):
        path = self.get_common_pvc_url_v1(ns_name=ns_name, pvc_name=pvc_name)
        return self.send(method='get', path=path, params={})

    def delete_pvc(self, ns_name, pvc_name, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_pvc_url_v1(ns_name, pvc_name)
        return self.send(method='delete', path=url, auth=auth)

    def update_pvc(self, ns_name, pvc_name, file, data, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_pvc_url_v1(ns_name, pvc_name)
        data = self.generate_data(file, data)
        return self.send(method='put', path=url, data=data, auth=auth)

    def update_pvc_jinja2(self, pvc_name, file, data):
        path = self.get_common_pvc_url_v1(pvc_name=pvc_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='put', path=path, json=data, params={})

    def get_pvc_list(self, limit=None, cnt=""):
        path = self.get_common_pvc_list_url(limit=limit, cnt=cnt)
        return self.send(method='get', path=path, params={})

    def search_pvc_jinja2_v1(self, ns_name="", limit=None, pvc_name=""):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_pvc_search_url_v1(ns_name=ns_name, limit=limit, pvc_name=pvc_name)
        return self.send(method='get', path=url)

    # 测试必填参数_可选参数为空_状态为绑定
    data_list_null = [
        {
            "pvc_name": '{}-ares-alauda-pvc'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
            "namespace": settings.K8S_NAMESPACE,
            "storageclassname": settings.SC_NAME
        }
    ]

    # 测试必填参数_可选参数为空_状态为匹配中
    data_list_null_pipei = [
        {
            "pvc_name": '{}-ares-alauda-pvc-pipei'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
            "namespace": settings.K8S_NAMESPACE,
            "storageclassname": "",
            "storage": "27Gi"
        }
    ]

    # 必填参数_可选参数只有1个_标签_状态为绑定
    data_list_labels = [
        {
            "pvc_name": '{}-ares-alauda-pvc-labels-1'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
            "namespace": settings.K8S_NAMESPACE,
            "storageclassname": settings.SC_NAME,
            "metadata_labels": {"key1": "value1", "key2": "value2"}
        }
    ]

    # 必填参数_可选参数只有1个_标签_状态为匹配中
    data_list_labels_pipei = [
        {
            "pvc_name": '{}-ares-alauda-pvc-labels-pipei'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
            "namespace": settings.K8S_NAMESPACE,
            "storageclassname": "",
            "metadata_labels": {"key1": "value1", "key2": "value2"},
            "storage": "27Gi"
        }
    ]

    # 必填参数_可选参数只有1个_注解_状态为绑定
    data_list_annotations = [
        {
            "pvc_name": '{}-ares-alauda-pvc-annotations'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
            "namespace": settings.K8S_NAMESPACE,
            "storageclassname": settings.SC_NAME,
            "metadata_annotations": {"a": "c", "b": "d"}
        }
    ]

    # 必填参数_可选参数只有1个_注解_状态为匹配中
    data_list_annotations_pipei = [
        {
            "pvc_name": '{}-ares-alauda-pvc-annotations-pipei'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
            "namespace": settings.K8S_NAMESPACE,
            "storageclassname": "",
            "metadata_annotations": {"a": "c", "b": "d"},
            "storage": "27Gi"
        }
    ]

    # 必填参数_可选参数只有1个_选择器
    data_list_selector = [
        {
            "pvc_name": '{}-ares-alauda-pvc-selector'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
            "namespace": settings.K8S_NAMESPACE,
            "storageclassname": "",
            "selector_matchLabels": {"q": "w", "e": "r"}
        }
    ]
