import pytest
from new_case.containerplatform.persistentvolumeclaims.pvc import Pvc
from common.settings import RERUN_TIMES, RESOURCE_PREFIX, K8S_NAMESPACE, SC_NAME
from new_case.containerplatform.storageclass.scs import Scs


@pytest.mark.BAT
@pytest.mark.storage
@pytest.mark.acp_containerplatform
@pytest.mark.skipif(not SC_NAME, reason="没有存储类，不运行当前用例")
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestPvcSuite(object):
    def setup_class(self):
        self.pvc = Pvc()
        self.k8s_namespace = K8S_NAMESPACE
        self.scs = Scs()
        self.pvcusescs_name = '{}-ares-alauda-pvcusescs-{}'.format(RESOURCE_PREFIX, self.pvc.region_name).replace('_', '-')
        self.default_size = self.scs.get_default_size()

        self.teardown(self)

    def teardown(self):
        self.pvc.delete_pvc(self.k8s_namespace, self.pvcusescs_name)

    def 测试pvc使用默认存储类(self):
        result = {"flag": True}
        # create pvc
        createpvc_result = self.pvc.create_pvc("./test_data/pvc/pvc_usedefault.json",
                                               {"$pvc_name": self.pvcusescs_name, "$pvc_mode": "ReadWriteOnce",
                                                "$size": "1", "$K8S_NAMESPACE": self.k8s_namespace})
        assert createpvc_result.status_code == 201, "创建pvc失败{}".format(createpvc_result.text)
        self.pvc.get_status(self.pvc.get_common_pvc_url_v1(self.k8s_namespace, self.pvcusescs_name),
                            "status.phase", "Bound")
        # list pvc
        list_result = self.pvc.list_pvc(self.k8s_namespace)
        result = self.pvc.update_result(result, list_result.status_code == 200, list_result.text)
        result = self.pvc.update_result(result, self.pvcusescs_name in list_result.text, "获取持久卷声明列表：新建pvc不在列表中")
        # get pvc detail
        detail_result = self.pvc.get_pvc_detail(self.k8s_namespace, self.pvcusescs_name)
        result = self.pvc.update_result(result, detail_result.status_code == 200, detail_result.text)
        result = self.pvc.update_result(result, self.pvc.get_value(detail_result.json(), "status.phase") == "Bound",
                                        "pvc详情状态不是绑定{}".format(detail_result.text))
        volumeName = self.pvc.get_value(detail_result.json(), "spec.volumeName")
        storageName = self.pvc.get_value(detail_result.json(), "spec.storageClassName")
        result = self.pvc.update_result(result, volumeName.startswith("pvc"),
                                        "pvc详情关联持久卷不是以pvc开头的{}".format(detail_result.text))

        # update pvc
        update_result = self.pvc.update_pvc(self.k8s_namespace, self.pvcusescs_name, "./test_data/pvc/pvc-update.json",
                                            {"$pvc_name": self.pvcusescs_name, "$pvc_mode": "ReadWriteOnce",
                                             "$scs_name": storageName, "$size": "1", "$vol_name": volumeName})
        result = self.pvc.update_result(result, update_result.status_code == 200, update_result.text)
        # get pvc detail
        detail_result = self.pvc.get_pvc_detail(self.k8s_namespace, self.pvcusescs_name)
        result = self.pvc.update_result(result, detail_result.status_code == 200, detail_result.text)
        result = self.pvc.update_result(result,
                                        set({"e2e": "pvc"}).issubset(self.pvc.get_value(detail_result.json(),
                                                                                        "metadata.labels")),
                                        "pvc详情的标签没有更新{}".format(detail_result.json()))
        # delete pvc
        delete_result = self.pvc.delete_pvc(self.k8s_namespace, self.pvcusescs_name)
        assert delete_result.status_code == 200, "删除pvc失败 {}".format(delete_result.text)
        assert self.pvc.check_exists(
            self.pvc.get_common_pvc_url_v1(self.k8s_namespace, self.pvcusescs_name),
            404)
        assert result['flag'], result
