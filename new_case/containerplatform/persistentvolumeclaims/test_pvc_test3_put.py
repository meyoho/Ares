import pytest
from common import settings
from new_case.containerplatform.persistentvolumeclaims.pvc import Pvc


@pytest.mark.storage
@pytest.mark.acp_containerplatform
@pytest.mark.skipif(not settings.SC_NAME, reason="没有存储类，不运行当前用例")
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestPvcSuitePut(object):
    data_list = Pvc.data_list_null
    data_list_1 = Pvc.data_list_null_pipei
    data_list_2 = Pvc.data_list_labels
    data_list_3 = Pvc.data_list_labels_pipei
    data_list_4 = Pvc.data_list_annotations
    data_list_5 = Pvc.data_list_annotations_pipei
    data_list_6 = Pvc.data_list_selector

    def setup_class(self):
        self.pvc = Pvc()

    @pytest.mark.upgrade
    def 测试_更新pvc_必填参数_可选参数为空_状态为绑定(self):
        data = self.data_list[0]
        detail_result = self.pvc.get_pvc_detail_jinja2(ns_name=data['namespace'], pvc_name=data['pvc_name'])
        assert detail_result.status_code == 200, "获取pvc详情页{}失败:{}".format(data['pvc_name'], detail_result.text)

        volumename = self.pvc.get_value(detail_result.json(), "spec.volumeName")
        data.update({"metadata_labels": {"key1-a": "value1", "key2-b": "value2"},
                     "spec_volumeName": {"volumeName": ""}, "volumename": volumename})

        update_result = self.pvc.update_pvc_jinja2(data['pvc_name'], './test_data/pvc/pvc.jinja2', data=data)
        assert update_result.status_code == 200, "更新pvc{}失败:{}".format(data['pvc_name'], update_result.text)

        value = self.pvc.generate_jinja_data("./verify_data/pvc/create-pvc.jinja2", data)
        assert self.pvc.is_sub_dict(value, update_result.json()), \
            "更新pvc_必填参数_可选参数为空比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

    def 测试_更新pvc_必填参数_可选参数为空_状态为匹配中(self):
        data = self.data_list_1[0]
        data.update({"metadata_labels": {"key1-a": "value1", "key2-b": "value2"}})
        update_result = self.pvc.update_pvc_jinja2(data['pvc_name'], './test_data/pvc/pvc.jinja2', data=data)
        assert update_result.status_code == 200, "更新pvc{}失败:{}".format(data['pvc_name'], update_result.text)

        value = self.pvc.generate_jinja_data("./verify_data/pvc/create-pvc.jinja2", data)
        assert self.pvc.is_sub_dict(value, update_result.json()), \
            "更新pvc_必填参数_可选参数为空比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

    @pytest.mark.skipif(settings.AUDIT_UNABLED, reason="do not have audit")
    def 测试pvc更新审计(self):
        payload = {"user_name": settings.USERNAME, "operation_type": "update", "resource_type": "persistentvolumeclaims",
                   "resource_name": self.data_list_1[0]['pvc_name']}
        result = self.pvc.search_audit(payload)
        payload.update({"namespace": settings.K8S_NAMESPACE, "region_name": settings.REGION_NAME})
        values = self.pvc.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.pvc.is_sub_dict(values, result.json()), "审计数据不符合预期"

    def 测试_更新pvc_必填参数_可选参数只有1个_标签_状态为绑定(self):
        data = self.data_list_2[0]
        detail_result = self.pvc.get_pvc_detail_jinja2(ns_name=data['namespace'], pvc_name=data['pvc_name'])
        assert detail_result.status_code == 200, "获取pvc详情页{}失败:{}".format(data['pvc_name'], detail_result.text)

        volumename = self.pvc.get_value(detail_result.json(), "spec.volumeName")
        data.update({"metadata_labels": {"key1-a": "value1", "key2-b": "value2"},
                     "spec_volumeName": {"volumeName": ""}, "volumename": volumename})
        update_result = self.pvc.update_pvc_jinja2(data['pvc_name'], './test_data/pvc/pvc.jinja2', data=data)
        assert update_result.status_code == 200, "更新pvc{}失败:{}".format(data['pvc_name'], update_result.text)

        value = self.pvc.generate_jinja_data("./verify_data/pvc/create-pvc.jinja2", data)
        assert self.pvc.is_sub_dict(value, update_result.json()), \
            "更新pvc_必填参数_可选参数只有1个_标签比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

    def 测试_更新pvc_必填参数_可选参数只有1个_标签_状态为匹配中(self):
        data = self.data_list_3[0]
        data.update({"metadata_labels": {"key1-a": "value1", "key2-b": "value2"}})
        update_result = self.pvc.update_pvc_jinja2(data['pvc_name'], './test_data/pvc/pvc.jinja2', data=data)
        assert update_result.status_code == 200, "更新pvc{}失败:{}".format(data['pvc_name'], update_result.text)

        value = self.pvc.generate_jinja_data("./verify_data/pvc/create-pvc.jinja2", data)
        assert self.pvc.is_sub_dict(value, update_result.json()), \
            "更新pvc_必填参数_可选参数只有1个_标签比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

    def 测试_更新pvc_必填参数_可选参数只有1个_注解_状态为绑定(self):
        data = self.data_list_4[0]
        detail_result = self.pvc.get_pvc_detail_jinja2(ns_name=data['namespace'], pvc_name=data['pvc_name'])
        assert detail_result.status_code == 200, "获取pvc详情页{}失败:{}".format(data['pvc_name'], detail_result.text)

        volumename = self.pvc.get_value(detail_result.json(), "spec.volumeName")
        data.update({"metadata_annotations": {"a001": "c", "b001": "d"}, "spec_volumeName": {"volumeName": ""},
                     "volumename": volumename})
        update_result = self.pvc.update_pvc_jinja2(data['pvc_name'], './test_data/pvc/pvc.jinja2', data=data)
        assert update_result.status_code == 200, "更新pvc{}失败:{}".format(data['pvc_name'], update_result.text)

        value = self.pvc.generate_jinja_data("./verify_data/pvc/create-pvc.jinja2", data)
        assert self.pvc.is_sub_dict(value, update_result.json()), \
            "更新pvc_必填参数_可选参数只有1个_注解比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

    def 测试_更新pvc_必填参数_可选参数只有1个_注解_状态为匹配中(self):
        data = self.data_list_5[0]
        data.update({"metadata_annotations": {"a001": "c", "b001": "d"}})
        update_result = self.pvc.update_pvc_jinja2(data['pvc_name'], './test_data/pvc/pvc.jinja2', data=data)
        assert update_result.status_code == 200, "更新pvc{}失败:{}".format(data['pvc_name'], update_result.text)

        value = self.pvc.generate_jinja_data("./verify_data/pvc/create-pvc.jinja2", data)
        assert self.pvc.is_sub_dict(value, update_result.json()), \
            "更新pvc_必填参数_可选参数只有1个_注解比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

    def 测试_更新pvc_必填参数_可选参数只有1个_选择器(self):
        data = self.data_list_6[0]
        data.update({"metadata_labels": {"key1-a": "value1", "key2-b": "value2"}})
        update_result = self.pvc.update_pvc_jinja2(data['pvc_name'], './test_data/pvc/pvc.jinja2', data=data)
        assert update_result.status_code == 200, "更新pvc{}失败:{}".format(data['pvc_name'], update_result.text)

        value = self.pvc.generate_jinja_data("./verify_data/pvc/pvc-selector.jinja2", data)
        assert self.pvc.is_sub_dict(value, update_result.json()), \
            "更新pvc_必填参数_可选参数只有1个_选择器比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)
