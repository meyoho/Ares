import pytest
from common import settings
from new_case.containerplatform.persistentvolumeclaims.pvc import Pvc


@pytest.mark.storage
@pytest.mark.acp_containerplatform
@pytest.mark.skipif(not settings.SC_NAME, reason="没有存储类，不运行当前用例")
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestPvcSuiteDelete(object):
    data_list = Pvc.data_list_null
    data_list_1 = Pvc.data_list_null_pipei
    data_list_2 = Pvc.data_list_labels
    data_list_3 = Pvc.data_list_labels_pipei
    data_list_4 = Pvc.data_list_annotations
    data_list_5 = Pvc.data_list_annotations_pipei
    data_list_6 = Pvc.data_list_selector

    def setup_class(self):
        self.pvc = Pvc()

    @pytest.mark.delete
    def 测试_删除pvc_必填参数_可选参数为空_状态为绑定(self):
        data = self.data_list[0]
        delete_result = self.pvc.delete_pvc(ns_name=data['namespace'], pvc_name=data['pvc_name'])
        assert delete_result.status_code == 200, "删除pvc:{}失败{}".format(data['pvc_name'], delete_result.text)

    def 测试_删除pvc_必填参数_可选参数为空_状态为匹配中(self):
        data = self.data_list_1[0]
        delete_result = self.pvc.delete_pvc(ns_name=data['namespace'], pvc_name=data['pvc_name'])
        assert delete_result.status_code == 200, "删除pvc:{}失败{}".format(data['pvc_name'], delete_result.text)

    @pytest.mark.skipif(settings.AUDIT_UNABLED, reason="do not have audit")
    def 测试pvc删除审计(self):
        payload = {"user_name": settings.USERNAME, "operation_type": "delete", "resource_type": "persistentvolumeclaims",
                   "resource_name": self.data_list_1[0]['pvc_name']}
        result = self.pvc.search_audit(payload)
        payload.update({"namespace": settings.K8S_NAMESPACE, "region_name": settings.REGION_NAME})
        values = self.pvc.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.pvc.is_sub_dict(values, result.json()), "审计数据不符合预期"

    def 测试_删除pvc_必填参数_可选参数只有1个_标签_状态为绑定(self):
        data = self.data_list_2[0]
        delete_result = self.pvc.delete_pvc(ns_name=data['namespace'], pvc_name=data['pvc_name'])
        assert delete_result.status_code == 200, "删除pvc{}失败:{}".format(data['pvc_name'], delete_result.text)

    def 测试_删除pvc_必填参数_可选参数只有1个_标签_状态为匹配中(self):
        data = self.data_list_3[0]
        delete_result = self.pvc.delete_pvc(ns_name=data['namespace'], pvc_name=data['pvc_name'])
        assert delete_result.status_code == 200, "删除pvc{}失败:{}".format(data['pvc_name'], delete_result.text)

    def 测试_删除pvc_必填参数_可选参数只有1个_注解_状态为绑定(self):
        data = self.data_list_4[0]
        delete_result = self.pvc.delete_pvc(ns_name=data['namespace'], pvc_name=data['pvc_name'])
        assert delete_result.status_code == 200, "删除pvc{}失败:{}".format(data['pvc_name'], delete_result.text)

    def 测试_删除pvc_必填参数_可选参数只有1个_注解_状态为匹配中(self):
        data = self.data_list_5[0]
        delete_result = self.pvc.delete_pvc(ns_name=data['namespace'], pvc_name=data['pvc_name'])
        assert delete_result.status_code == 200, "删除pvc{}失败:{}".format(data['pvc_name'], delete_result.text)

    def 测试_删除pvc_必填参数_可选参数只有1个_注解_选择器(self):
        data = self.data_list_6[0]
        delete_result = self.pvc.delete_pvc(ns_name=data['namespace'], pvc_name=data['pvc_name'])
        assert delete_result.status_code == 200, "删除pvc{}失败:{}".format(data['pvc_name'], delete_result.text)
