import pytest
from common import settings
from new_case.containerplatform.persistentvolumeclaims.pvc import Pvc


@pytest.mark.storage
@pytest.mark.acp_containerplatform
@pytest.mark.skipif(not settings.SC_NAME, reason="没有存储类，不运行当前用例")
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestPvcSuitePost(object):
    data_list = Pvc.data_list_null
    data_list_1 = Pvc.data_list_null_pipei
    data_list_2 = Pvc.data_list_labels
    data_list_3 = Pvc.data_list_labels_pipei
    data_list_4 = Pvc.data_list_annotations
    data_list_5 = Pvc.data_list_annotations_pipei
    data_list_6 = Pvc.data_list_selector

    def setup_class(self):
        self.pvc = Pvc()

    @pytest.mark.prepare
    def 测试_创建pvc_必填参数_可选参数为空_状态为绑定(self):
        data = self.data_list[0]
        self.pvc.delete_pvc(ns_name=data['namespace'], pvc_name=data['pvc_name'])
        create_result = self.pvc.create_pvc_jinja2("./test_data/pvc/pvc.jinja2", data=data)
        assert create_result.status_code == 201, "创建pvc{}失败:{}".format(data['pvc_name'], create_result.text)

        pvc_status = self.pvc.get_status(self.pvc.get_common_pvc_url_v1(pvc_name=data["pvc_name"]), "status.phase", "Bound")
        assert pvc_status, "创建pvc状态错误:{}的状态没有被绑定".format(data["pvc_name"])

        value = self.pvc.generate_jinja_data("./verify_data/pvc/create-pvc.jinja2", data)
        assert self.pvc.is_sub_dict(value, create_result.json()), \
            "创建pvc_必填参数_可选参数为空比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

    def 测试pvc创建的事件(self):
        payload = {"cluster": settings.REGION_NAME,
                   "kind": "PersistentVolumeClaim",
                   "name": self.data_list[0]["pvc_name"], "namespace": settings.K8S_NAMESPACE, "page": 1, "page_size": 20}
        kevents_results = self.pvc.get_kevents(payload=payload, expect_value="ProvisioningSucceeded", times=1)
        assert kevents_results, "创建pvc获取事件失败失败"

    @pytest.mark.skipif(settings.AUDIT_UNABLED, reason="do not have audit")
    def 不测试pvc创建审计(self):
        payload = {"user_name": settings.USERNAME, "operation_type": "create", "resource_type": "persistentvolumeclaims",
                   "resource_name": self.data_list[0]['pvc_name']}
        result = self.pvc.search_audit(payload)
        payload.update({"namespace": settings.K8S_NAMESPACE, "region_name": settings.REGION_NAME, "code": 201})
        values = self.pvc.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.pvc.is_sub_dict(values, result.json()), "审计数据不符合预期"

    def 测试_创建pvc_必填参数_可选参数为空_状态为匹配中(self):
        data = self.data_list_1[0]
        self.pvc.delete_pvc(ns_name=data['namespace'], pvc_name=data['pvc_name'])
        create_result = self.pvc.create_pvc_jinja2("./test_data/pvc/pvc.jinja2", data=data)
        assert create_result.status_code == 201, "创建pvc{}失败:{}".format(data['pvc_name'], create_result.text)

        value = self.pvc.generate_jinja_data("./verify_data/pvc/create-pvc.jinja2", data)
        assert self.pvc.is_sub_dict(value, create_result.json()), \
            "创建pvc_必填参数_可选参数为空比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

    def 测试_创建pvc_必填参数_可选参数只有1个_标签_状态为绑定(self):
        data = self.data_list_2[0]
        self.pvc.delete_pvc(ns_name=data['namespace'], pvc_name=data['pvc_name'])
        create_result = self.pvc.create_pvc_jinja2("./test_data/pvc/pvc.jinja2", data=data)
        assert create_result.status_code == 201, "创建pvc{}失败:{}".format(data['pvc_name'], create_result.text)

        pvc_status = self.pvc.get_status(self.pvc.get_common_pvc_url_v1(pvc_name=data["pvc_name"]), "status.phase", "Bound")
        assert pvc_status, "创建pvc状态错误:{}的状态没有被绑定".format(data["pvc_name"])

        value = self.pvc.generate_jinja_data("./verify_data/pvc/create-pvc.jinja2", data)
        assert self.pvc.is_sub_dict(value, create_result.json()), \
            "创建pvc_必填参数_可选参数只有1个_标签比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

    def 测试_创建pvc_必填参数_可选参数只有1个_标签_状态为匹配中(self):
        data = self.data_list_3[0]
        self.pvc.delete_pvc(ns_name=data['namespace'], pvc_name=data['pvc_name'])
        create_result = self.pvc.create_pvc_jinja2("./test_data/pvc/pvc.jinja2", data=data)
        assert create_result.status_code == 201, "创建pvc{}失败:{}".format(data['pvc_name'], create_result.text)

        value = self.pvc.generate_jinja_data("./verify_data/pvc/create-pvc.jinja2", data)
        assert self.pvc.is_sub_dict(value, create_result.json()), \
            "创建pvc_必填参数_可选参数只有1个_标签比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

    def 测试_创建pvc_必填参数_可选参数只有1个_注解_状态为绑定(self):
        data = self.data_list_4[0]
        self.pvc.delete_pvc(ns_name=data['namespace'], pvc_name=data['pvc_name'])
        create_result = self.pvc.create_pvc_jinja2("./test_data/pvc/pvc.jinja2", data=data)
        assert create_result.status_code == 201, "创建pvc{}失败:{}".format(data['pvc_name'], create_result.text)

        pvc_status = self.pvc.get_status(self.pvc.get_common_pvc_url_v1(pvc_name=data["pvc_name"]), "status.phase", "Bound")
        assert pvc_status, "创建pvc状态错误:{}的状态没有被绑定".format(data["pvc_name"])

        value = self.pvc.generate_jinja_data("./verify_data/pvc/create-pvc.jinja2", data)
        assert self.pvc.is_sub_dict(value, create_result.json()), \
            "创建pvc_必填参数_可选参数只有1个_注解比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

    def 测试_创建pvc_必填参数_可选参数只有1个_注解_状态为匹配中(self):
        data = self.data_list_5[0]
        self.pvc.delete_pvc(ns_name=data['namespace'], pvc_name=data['pvc_name'])
        create_result = self.pvc.create_pvc_jinja2("./test_data/pvc/pvc.jinja2", data=data)
        assert create_result.status_code == 201, "创建pvc{}失败:{}".format(data['pvc_name'], create_result.text)

        value = self.pvc.generate_jinja_data("./verify_data/pvc/create-pvc.jinja2", data)
        assert self.pvc.is_sub_dict(value, create_result.json()), \
            "创建pvc_必填参数_可选参数只有1个_注解比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

    def 测试_创建pvc_必填参数_可选参数只有1个_选择器(self):
        data = self.data_list_6[0]
        self.pvc.delete_pvc(ns_name=data['namespace'], pvc_name=data['pvc_name'])
        create_result = self.pvc.create_pvc_jinja2("./test_data/pvc/pvc.jinja2", data=data)
        assert create_result.status_code == 201, "创建pvc{}失败:{}".format(data['pvc_name'], create_result.text)

        value = self.pvc.generate_jinja_data("./verify_data/pvc/pvc-selector.jinja2", data)
        assert self.pvc.is_sub_dict(value, create_result.json()), \
            "创建pvc_必填参数_可选参数只有1个_选择器比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)
