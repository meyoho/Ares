from common.base_request import Common


class Ratio(Common):

    def get_feature(self):
        url = "kubernetes/{}/apis/infrastructure.alauda.io/v1alpha1/features/resourceratio".format(
                             self.region_name)
        return self.send('get', path=url)

    def update_ratio(self, file, data):
        data = self.generate_jinja_data(file, data)
        url = "apis/platform.tkestack.io/v1/clusters/{}".format(self.region_name)
        return self.send('patch', path=url, json=data,
                         headers={"Content-Type": "application/merge-patch+json"})

    def create_pod(self, namespace, file, data):
        url = "kubernetes/{}/api/v1/namespaces/{}/pods".format(self.region_name, namespace)
        data = self.generate_jinja_data(file, data)
        return self.send('post', path=url, json=data)

    def common_pod_url(self, namespace, podname):
        return "kubernetes/{}/api/v1/namespaces/{}/pods/{}".format(self.region_name, namespace, podname)

    def delete_pod(self, namespace, podname):
        return self.send('delete', path=self.common_pod_url(namespace, podname))
