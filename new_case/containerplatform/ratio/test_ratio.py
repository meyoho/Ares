from time import sleep

import pytest

from common.exceptions import ParseResponseError
from common.settings import RERUN_TIMES, K8S_NAMESPACE, get_featuregate_status, REGION_NAME
from new_case.containerplatform.ratio.conftest import pod_data, update_data
from new_case.containerplatform.ratio.ratio import Ratio


@pytest.mark.metis
@pytest.mark.BAT
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
@pytest.mark.skipif(not get_featuregate_status(REGION_NAME, 'resource-ratio'), reason="resource-ratio功能开关未开启，默认不跑对应用例")
class TestRatio(object):
    def setup_class(self):
        self.ratio_client = Ratio()
        self.k8s_namespace = K8S_NAMESPACE
        setup = self.ratio_client.get_feature()
        global code, cpu, memory
        code = setup.status_code
        if code == 200:
            try:
                cpu = self.ratio_client.get_value(setup.json(), "spec.deployInfo.cpu")
            except ParseResponseError:
                cpu = 'null'
            try:
                memory = self.ratio_client.get_value(setup.json(), "spec.deployInfo.memory")
            except ParseResponseError:
                memory = 'null'

    def teardown_class(self):
        if code != 404:
            data = {"cpu": cpu, "memory": memory}
            self.ratio_client.update_ratio('test_data/ratio/update_ratio.jinja2', data)

    @pytest.mark.parametrize("key", update_data)
    def 测试更新超售比(self, key):
        data = update_data[key]
        ret = self.ratio_client.update_ratio('test_data/ratio/update_ratio.jinja2', data)
        assert ret.status_code == 200, "更新超售比失败 {}".format(ret.text)
        values = self.ratio_client.generate_jinja_data('verify_data/ratio/resourceratio.jinja2', data)
        assert self.ratio_client.is_sub_dict(values, ret.json()), \
            "更新超售比比对数据失败 期望数据 {} 返回数据 {}".format(values, ret.text)
        # 验证同步到feature
        sleep(2)
        feature = self.ratio_client.get_feature()
        assert feature.status_code == 200, "获取feature失败:{}".format(feature.text)
        feature_info = self.ratio_client.get_value(feature.json(), "spec.deployInfo")
        assert self.ratio_client.is_sub_dict(feature_info, data), \
            "feature比对数据失败 期望数据 {} 返回数据 {}".format(data, feature_info)
        # 创建pod验证超售比
        cnt = 0
        flag = False
        while cnt < 12 and not flag:
            cnt += 1
            sleep(5)
            self.ratio_client.delete_pod(self.k8s_namespace, pod_data['pod_name'])
            self.ratio_client.check_exists(
                self.ratio_client.common_pod_url(self.k8s_namespace, pod_data['pod_name']), 404)
            ret = self.ratio_client.create_pod(self.k8s_namespace, './test_data/ratio/create_pod.jinja2', pod_data)
            assert ret.status_code == 201, "创建pod失败:{}".format(ret.text)
            pod_data.update(data)
            values = self.ratio_client.generate_jinja_data('verify_data/ratio/pod.jinja2', pod_data)
            if self.ratio_client.is_sub_dict(values, ret.json()):
                flag = True
        assert flag, "创建pod验证超售比 比对数据失败"
        ret = self.ratio_client.delete_pod(self.k8s_namespace, pod_data['pod_name'])
        assert ret.status_code == 200, "删除pod失败 {}".format(ret.text)
        delete_flag = self.ratio_client.check_exists(
            self.ratio_client.common_pod_url(self.k8s_namespace, pod_data['pod_name']), 404)
        assert delete_flag, "删除pod失败 未删除"
