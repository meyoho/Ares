import pytest
from common.settings import RERUN_TIMES, USERNAME, REGION_NAME, AUDIT_UNABLED, get_featuregate_status
from new_case.containerplatform.ratio.ratio import Ratio


@pytest.mark.metis
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
@pytest.mark.skipif(not get_featuregate_status(REGION_NAME, 'resource-ratio'), reason="resource-ratio功能开关未开启，默认不跑对应用例")
class TestRatioAuditL1Suite(object):
    def setup_class(self):
        self.ratio = Ratio()

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 测试超售比更新审计(self):
        payload = {"user_name": USERNAME, "operation_type": "patch", "resource_type": "features",
                   "resource_name": "resourceratio"}
        result = self.ratio.search_audit(payload)
        payload.update({"namespace": "", "region_name": REGION_NAME})
        values = self.ratio.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.ratio.is_sub_dict(values, result.json()), "审计数据不符合预期"
