from common.settings import K8S_NAMESPACE, RESOURCE_PREFIX, IMAGE

pod_data = {
    'namespace': K8S_NAMESPACE,
    'pod_name': "{}-ares-acp-ratio".format(RESOURCE_PREFIX),
    'image': IMAGE,
    'replicas': 1
}

update_data = {
    "cpu为2,内存为2": {"cpu": 2, "memory": 2},
    "cpu为null,内存为5": {"cpu": 'null', "memory": 5},
    "cpu为2,内存为null": {"cpu": 2, "memory": 'null'},
    "cpu为null,内存为null": {"cpu": 'null', "memory": 'null'}
}
