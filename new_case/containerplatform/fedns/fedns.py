import time

from common.base_request import Common
from common import settings


class FedNs(Common):
    def __init__(self):
        super(FedNs, self).__init__()
        # self.genetate_global_info()
        self.template = './test_data/fedns/fedns.jinja2'
        self.host_cluster_name = ''
        self.member_cluster_name = []
        self.all_cluster_name = []
        url = 'apis/infrastructure.alauda.io/v1alpha1/clusterfeds/{}'.format(settings.FED_NAME)
        response = self.send(method='get', path=url)
        if response.status_code == 200:
            for cluster in response.json()['spec']['clusters']:
                self.all_cluster_name.append(cluster['name'])
                if cluster['type'] == 'Host':
                    self.host_cluster_name = cluster['name']
                else:
                    self.member_cluster_name.append(cluster['name'])

    def create(self, project_name, data):
        path = 'acp/v1/kubernetes/{}/federatednamespaces?project_name={}'.format(self.host_cluster_name, project_name)
        data = self.generate_jinja_data(self.template, data)
        return self.send(method='post', path=path, json=data)

    def federate(self, name):
        path = 'acp/v1/kubernetes/{}/namespaces/{}/federate'.format(self.host_cluster_name, name)
        return self.send(method='post', path=path)

    def list(self):
        path = 'kubernetes/{}/apis/types.kubefed.io/v1beta1/federatednamespaces'.format(self.host_cluster_name)
        return self.send(method='get', path=path)

    def detail(self, name, timeout=15):
        path = 'kubernetes/{}/apis/types.kubefed.io/v1beta1/namespaces/{}/federatednamespaces/{}'.format(
            self.host_cluster_name, name, name)
        end_time = time.time() + timeout
        while time.time() < end_time:
            ret = self.send(method='get', path=path)
            if ret.status_code == 200 and 'finalizers' in ret.json():
                return ret
            else:
                time.sleep(1)
        return self.send(method='get', path=path)

    def detail_resourequota(self, region_name, namespace, expect_value='1', query="spec#hard#limits.cpu", timeout=15):
        path = "kubernetes/{}/api/v1/namespaces/{}/resourcequotas/default".format(region_name, namespace)
        end_time = time.time() + timeout
        while time.time() < end_time:
            ret = self.send(method='get', path=path)
            if ret.status_code == 200 and self.get_value(ret.json(), query, "#") == expect_value:
                return ret
            else:
                time.sleep(1)
        return self.send(method='get', path=path)

    def detail_limitrange(self, region_name, namespace, expect_value='50m', query="spec.limits.0.default.cpu",
                          timeout=15):
        path = "kubernetes/{}/api/v1/namespaces/{}/limitranges/default".format(region_name, namespace)
        end_time = time.time() + timeout
        while time.time() < end_time:
            ret = self.send(method='get', path=path)
            if ret.status_code == 200 and self.get_value(ret.json(), query) == expect_value:
                return ret
            else:
                time.sleep(1)
        return self.send(method='get', path=path)

    def delete_fedns(self, name):
        path = 'kubernetes/{}/apis/types.kubefed.io/v1beta1/namespaces/{}/federatednamespaces/{}'.format(
            self.host_cluster_name, name, name)
        return self.send(method='delete', path=path)

    def delete_ns(self, name):
        path = 'kubernetes/{}/api/v1/namespaces/{}'.format(self.host_cluster_name, name)
        return self.send(method='delete', path=path)

    def update(self, name, data):
        path = 'acp/v1/kubernetes/{}/federatednamespaces/{}'.format(self.host_cluster_name, name)
        data = self.generate_jinja_data(self.template, data)
        return self.send(method='put', path=path, json=data)

    def patch(self, name, displayname):
        path = 'kubernetes/{}/apis/types.kubefed.io/v1beta1/namespaces/{}/federatednamespaces/{}'.format(
            self.host_cluster_name, name, name)
        data = {"metadata": {"annotations": {"{}/display-name".format(settings.DEFAULT_LABEL): displayname}}}
        return self.send(method='patch', path=path, json=data, headers={"Content-Type": "application/merge-patch+json"})

    def common_url(self, name):
        return 'kubernetes/{}/api/v1/namespaces/{}'.format(self.host_cluster_name, name)
