import pytest

from common.log import logger
from common.settings import CASE_TYPE, get_featuregate_status, RESOURCE_PREFIX, FED_NAME
from new_case.containerplatform.fedns.conftest import project_name
from new_case.containerplatform.fedns.fedns import FedNs


@pytest.mark.BAT
@pytest.mark.acp_containerplatform
@pytest.mark.skipif(not get_featuregate_status(None, 'kubefed'), reason="kubefed功能开关未开启，默认不跑对应用例")
@pytest.mark.skipif(not FED_NAME, reason="没有传联邦集群名称，默认不跑对应用例")
class TestFedNsSuite(object):
    def setup_class(self):
        self.fedns = FedNs()
        self.project_name = project_name
        self.data = {
            "name": "{}-ares-fedns-l0".format(RESOURCE_PREFIX),
            "hostname": self.fedns.host_cluster_name,
            "fed_name": FED_NAME,
            "project_name": project_name,
            "override": 'false',
            "cpu": '2',
            'default_cpu': '40m',
            'max_cpu': '80m'
        }
        self.name = self.data['name']

    def teardown_class(self):
        if CASE_TYPE not in ("prepare", "upgrade", "delete"):
            self.fedns.delete_fedns(self.name)
            self.fedns.delete_ns(self.name)

    @pytest.mark.prepare
    def 测试创建联邦命名空间_无差异化(self):
        self.fedns.delete_fedns(self.name)
        self.fedns.check_exists(self.fedns.common_url(self.name), 404)
        ret = self.fedns.create(self.project_name, self.data)
        assert ret.status_code == 200, "创建联邦命名空间失败:{}".format(ret.text)
        logger.info(ret.json())
        values = self.fedns.generate_jinja_data("verify_data/fedns/create_response.jinja2", self.data)
        assert self.fedns.is_sub_dict(values, ret.json()), \
            "创建联邦命名空间比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)

    @pytest.mark.upgrade
    def 测试联邦命名空间详情_无差异化(self):
        ret = self.fedns.detail(self.name)
        assert ret.status_code == 200, "联邦命名空间详情失败:{}".format(ret.text)
        values = self.fedns.generate_jinja_data("verify_data/fedns/detail_response.jinja2", self.data)
        assert self.fedns.is_sub_dict(values, ret.json()), \
            "联邦命名空间详情比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)

    def 测试联邦命名空间列表(self):
        ret = self.fedns.list()
        assert ret.status_code == 200, "联邦命名空间列表失败:{}".format(ret.text)
        content = self.fedns.get_k8s_resource_data(ret.json(), self.name, list_key="items")
        values = self.fedns.generate_jinja_data("verify_data/fedns/detail_response.jinja2", self.data)
        assert self.fedns.is_sub_dict(values, content), \
            "联邦命名空间列表比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)

    def 测试更新联邦命名空间显示名称(self):
        ret = self.fedns.patch(self.name, 'updatedisplayname')
        self.data.update({'display_name': 'updatedisplayname'})
        assert ret.status_code == 200, "更新联邦命名空间显示名称失败:{}".format(ret.text)
        values = self.fedns.generate_jinja_data("verify_data/fedns/detail_response.jinja2", self.data)
        assert self.fedns.is_sub_dict(values, ret.json()), \
            "更新联邦命名空间显示名称 比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)

    @pytest.mark.upgrade
    def 测试更新联邦命名空间_无差异化(self):
        self.data.update({'cpu': '3'})
        self.data.update({'default_cpu': '30m'})
        ret = self.fedns.update(self.name, self.data)
        assert ret.status_code == 200, "更新联邦命名空间失败:{}".format(ret.text)
        logger.info(ret.json())
        values = self.fedns.generate_jinja_data("verify_data/fedns/create_response.jinja2", self.data)
        assert self.fedns.is_sub_dict(values, ret.json()), \
            "更新联邦命名空间比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)

    @pytest.mark.upgrade
    def 测试更新联邦命名空间_无差异化_验证所有子集群的容器配额(self):
        for cluster in self.fedns.all_cluster_name:
            limitrange_ret = self.fedns.detail_limitrange(cluster, self.name, self.data['default_cpu'])
            assert limitrange_ret.status_code == 200, "获取容器配额失败:{}".format(limitrange_ret.text)
            values = self.fedns.generate_jinja_data("verify_data/fedns/limitrange_response.jinja2", self.data)
            assert self.fedns.is_sub_dict(values, limitrange_ret.json()), \
                "更新联邦命名空间后，容器配额比对数据失败，返回数据{}，期望数据{}".format(limitrange_ret.text, values)

    @pytest.mark.upgrade
    def 测试更新联邦命名空间_无差异化_验证所有子集群的资源配额(self):
        for cluster in self.fedns.all_cluster_name:
            resourcequota_ret = self.fedns.detail_resourequota(cluster, self.name, self.data['cpu'])
            assert resourcequota_ret.status_code == 200, "获取资源限额失败:{}".format(resourcequota_ret.text)
            values = self.fedns.generate_jinja_data("verify_data/fedns/resourcequota_response.jinja2", self.data)
            assert self.fedns.is_sub_dict(values, resourcequota_ret.json()), \
                "更新联邦命名空间后，资源配额比对数据失败，返回数据{}，期望数据{}".format(resourcequota_ret.text, values)

    @pytest.mark.delete
    def 测试删除联邦命名空间_无差异化(self):
        ret1 = self.fedns.delete_fedns(self.name)
        assert ret1.status_code == 200, "删除联邦命名空间失败:{}".format(ret1.text)
        ret2 = self.fedns.delete_ns(self.name)
        assert ret2.status_code == 200, "删除联邦命名空间所在命名空间失败:{}".format(ret2.text)
        assert self.fedns.check_exists(
            self.fedns.common_url(self.name), 404)
