import pytest

from common.log import logger
from common.settings import get_featuregate_status, RESOURCE_PREFIX, FED_NAME, DEFAULT_LABEL
from new_case.containerplatform.fedns.conftest import project_name
from new_case.containerplatform.fedns.fedns import FedNs
from new_case.containerplatform.namespace.namespace import Namespace


@pytest.mark.BAT
@pytest.mark.acp_containerplatform
@pytest.mark.skipif(not get_featuregate_status(None, 'kubefed'), reason="kubefed功能开关未开启，默认不跑对应用例")
@pytest.mark.skipif(not FED_NAME, reason="没有传联邦集群名称，默认不跑对应用例")
class TestNsFedSuite(object):
    def setup_class(self):
        self.project_name = project_name
        self.namespace = Namespace()
        self.fedns = FedNs()
        self.data = {
            "namespace_name": "{}-federate".format(RESOURCE_PREFIX),
            "cluster": self.fedns.host_cluster_name,
            "project": project_name,
            "ResourceQuota": "True",
            "requests_cpu": 2,
            "limits_cpu": 2,
            "requests_mem": "2Gi",
            "limits_mem": "2Gi",
            "requests_storage": "2Gi",
            "pvc": "2",
            "pod": "2",
            "LimitRange": "True",
            "default_label": DEFAULT_LABEL
        }
        self.fedns_data = {
            "name": "{}-federate".format(RESOURCE_PREFIX),
            "hostname": self.fedns.host_cluster_name,
            "fed_name": FED_NAME,
            "project_name": project_name,
            "override": 'false',
            "cpu": '2',
            "memory": "2Gi",
            "storage": "2Gi",
            "pvc": '2',
            "pod": "2",
            'default_cpu': '100m',
            'default_mem': '100Mi',
            'max_cpu': '1',
            'max_mem': '1Gi',
        }
        self.name = self.fedns_data['name']
        self.fedns.delete_fedns(self.name)
        self.fedns.delete_ns(self.name)

    def teardown_class(self):
        self.fedns.delete_fedns(self.name)
        self.fedns.delete_ns(self.name)

    def 测试命名空间联邦化(self):
        response = self.namespace.create_namespace('./test_data/namespace/create_namespace.jinja2', self.data,
                                                   self.fedns.host_cluster_name)
        assert response.status_code == 200, "命名空间创建失败:{}".format(response.text)
        ret = self.fedns.federate(self.name)
        assert ret.status_code == 200, "命名空间联邦化失败:{}".format(ret.text)
        logger.info(ret.json())
        values = self.fedns.generate_jinja_data("verify_data/fedns/create_response.jinja2", self.fedns_data)
        assert self.fedns.is_sub_dict(values, ret.json()), \
            "命名空间联邦化比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)

        for cluster in self.fedns.all_cluster_name:
            limitrange_ret = self.fedns.detail_limitrange(cluster, self.name, self.fedns_data['default_cpu'])
            assert limitrange_ret.status_code == 200, "获取容器配额失败:{}".format(limitrange_ret.text)
            values = self.fedns.generate_jinja_data("verify_data/fedns/limitrange_response.jinja2", self.fedns_data)
            assert self.fedns.is_sub_dict(values, limitrange_ret.json()), \
                "命名空间联邦化后，容器配额比对数据失败，返回数据{}，期望数据{}".format(limitrange_ret.text, values)

            resourcequota_ret = self.fedns.detail_resourequota(cluster, self.name, self.fedns_data['cpu'])
            assert resourcequota_ret.status_code == 200, "获取资源限额失败:{}".format(resourcequota_ret.text)
            values = self.fedns.generate_jinja_data("verify_data/fedns/resourcequota_response.jinja2", self.fedns_data)
            assert self.fedns.is_sub_dict(values, resourcequota_ret.json()), \
                "命名空间联邦化后，资源配额比对数据失败，返回数据{}，期望数据{}".format(resourcequota_ret.text, values)
