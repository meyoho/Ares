import pytest

from common import settings
from new_case.containerplatform.fedns.fedns import FedNs
from new_case.platform.project.project import Project

project_name = 'e2enamespace-kubefed'


@pytest.fixture(scope="session", autouse=True)
@pytest.mark.skipif(not settings.get_featuregate_status(None, 'kubefed'), reason="kubefed功能开关未开启，默认不跑对应用例")
def prepare():
    project = Project()
    data = {
        "project_name": project_name,
        'fed_region': settings.FED_NAME,
        "regions": FedNs().all_cluster_name,
        "display_name": 'e2e 联邦测试',
        "description": "e2e test project",
        "cpu": "10",
        "memory": "10Gi"
    }
    create_project_result = project.create_project("./test_data/project/create_project.jinja2", data)
    assert create_project_result.status_code in (201, 409), "创建项目失败"
