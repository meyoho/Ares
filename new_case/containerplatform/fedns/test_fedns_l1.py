import pytest

from common.settings import CASE_TYPE, get_featuregate_status, RESOURCE_PREFIX, FED_NAME
from new_case.containerplatform.fedns.conftest import project_name
from new_case.containerplatform.fedns.fedns import FedNs


@pytest.mark.acp_containerplatform
@pytest.mark.skipif(not get_featuregate_status(None, 'kubefed'), reason="kubefed功能开关未开启，默认不跑对应用例")
@pytest.mark.skipif(not FED_NAME, reason="没有传联邦集群名称，默认不跑对应用例")
class TestFedNsSuite(object):
    def setup_class(self):
        self.fedns = FedNs()
        self.project_name = project_name
        self.data = {"name": "{}-ares-fedns-diff".format(RESOURCE_PREFIX),
                     "hostname": self.fedns.host_cluster_name,
                     "fed_name": FED_NAME,
                     "project_name": project_name,
                     "override": 'true',
                     "rq_overrides": [],
                     'lr_overrides': []
                     }
        self.name = self.data['name']
        self.data['rq_overrides'].append(
            {'clustername': self.fedns.host_cluster_name,
             'cpu': '2', 'memory': '2Gi', 'pvc': 2, 'pods': 2, 'storage': '2Gi'})
        self.data['lr_overrides'].append({'clustername': self.fedns.host_cluster_name, 'default_cpu': '20m'})
        for cluster in self.fedns.member_cluster_name:
            self.data['rq_overrides'].append(
                {'clustername': cluster, 'cpu': '3', 'memory': '3Gi', 'pvc': 3, 'pods': 3, 'storage': '3Gi'})
            self.data['lr_overrides'].append({'clustername': cluster, 'default_cpu': '30m'})

    def teardown_class(self):
        if CASE_TYPE not in ("prepare", "upgrade", "delete"):
            self.fedns.delete_fedns(self.name)
            self.fedns.delete_ns(self.name)

    @pytest.mark.prepare
    def 测试创建联邦命名空间_有差异化(self):
        self.fedns.delete_fedns(self.name)
        self.fedns.delete_ns(self.name)
        self.fedns.check_exists(self.fedns.common_url(self.name), 404)

        ret = self.fedns.create(self.project_name, self.data)
        assert ret.status_code == 200, "创建联邦命名空间失败:{}".format(ret.text)
        values = self.fedns.generate_jinja_data("verify_data/fedns/create_response.jinja2", self.data)
        assert self.fedns.is_sub_dict(values, ret.json()), \
            "创建联邦命名空间比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)

    @pytest.mark.upgrade
    def 测试创建联邦命名空间_有差异化_验证所有子集群的容器配额(self):
        for limitrange_data in self.data['lr_overrides']:
            limitrange_data.update({"name": self.name})
            limitrange_ret = self.fedns.detail_limitrange(limitrange_data['clustername'], self.name,
                                                          limitrange_data['default_cpu'])
            assert limitrange_ret.status_code == 200, "获取容器配额失败:{}".format(limitrange_ret.text)
            values = self.fedns.generate_jinja_data("verify_data/fedns/limitrange_response.jinja2", limitrange_data)
            assert self.fedns.is_sub_dict(values, limitrange_ret.json()), \
                "更新联邦命名空间后，容器配额比对数据失败，返回数据{}，期望数据{}".format(limitrange_ret.text, values)

    @pytest.mark.upgrade
    def 测试创建联邦命名空间_有差异化_验证所有子集群的资源配额(self):
        for resourcequota_data in self.data['rq_overrides']:
            resourcequota_data.update({"name": self.name})
            resourcequota_ret = self.fedns.detail_resourequota(resourcequota_data['clustername'], self.name,
                                                               resourcequota_data['cpu'])
            assert resourcequota_ret.status_code == 200, "获取资源限额失败:{}".format(resourcequota_ret.text)
            values = self.fedns.generate_jinja_data("verify_data/fedns/resourcequota_response.jinja2",
                                                    resourcequota_data)
            assert self.fedns.is_sub_dict(values, resourcequota_ret.json()), \
                "更新联邦命名空间后，资源配额比对数据失败，返回数据{}，期望数据{}".format(resourcequota_ret.text, values)

    @pytest.mark.upgrade
    def 测试更新联邦命名空间单个集群_有差异化(self):
        for rq_override in self.data['rq_overrides']:
            if rq_override['clustername'] == self.fedns.host_cluster_name:
                rq_override.update({'cpu': '1', 'memory': '1Gi', 'pvc': 1, 'pods': 1, 'storage': '1Gi'})
        for lr_override in self.data['lr_overrides']:
            if lr_override['clustername'] == self.fedns.host_cluster_name:
                lr_override.update({'default_cpu': '10m', 'default_mem': '10Mi', 'max_cpu': '20m', 'max_mem': '20Mi'})
        ret = self.fedns.update(self.name, self.data)
        assert ret.status_code == 200, "更新联邦命名空间失败:{}".format(ret.text)
        values = self.fedns.generate_jinja_data("verify_data/fedns/create_response.jinja2", self.data)
        assert self.fedns.is_sub_dict(values, ret.json()), \
            "更新联邦命名空间比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)

    @pytest.mark.upgrade
    def 测试更新联邦命名空间单个集群_有差异化_验证所有子集群的容器配额(self):
        for limitrange_data in self.data['lr_overrides']:
            if limitrange_data['clustername'] == self.fedns.host_cluster_name:
                limitrange_data.update(
                    {'default_cpu': '10m', 'default_mem': '10Mi', 'max_cpu': '20m', 'max_mem': '20Mi'})
            limitrange_data.update({"name": self.name})
            limitrange_ret = self.fedns.detail_limitrange(limitrange_data['clustername'], self.name,
                                                          limitrange_data['default_cpu'])
            assert limitrange_ret.status_code == 200, "获取容器配额失败:{}".format(limitrange_ret.text)
            values = self.fedns.generate_jinja_data("verify_data/fedns/limitrange_response.jinja2", limitrange_data)
            assert self.fedns.is_sub_dict(values, limitrange_ret.json()), \
                "更新联邦命名空间后，容器配额比对数据失败，返回数据{}，期望数据{}".format(limitrange_ret.text, values)

    @pytest.mark.upgrade
    def 测试更新联邦命名空间单个集群_有差异化_验证所有子集群的资源配额(self):
        for resourcequota_data in self.data['rq_overrides']:
            if resourcequota_data['clustername'] == self.fedns.host_cluster_name:
                resourcequota_data.update({'cpu': '1', 'memory': '1Gi', 'pvc': 1, 'pods': 1, 'storage': '1Gi'})
            resourcequota_data.update({"name": self.name})
            resourcequota_ret = self.fedns.detail_resourequota(resourcequota_data['clustername'], self.name,
                                                               resourcequota_data['cpu'])
            assert resourcequota_ret.status_code == 200, "获取资源限额失败:{}".format(resourcequota_ret.text)
            values = self.fedns.generate_jinja_data("verify_data/fedns/resourcequota_response.jinja2",
                                                    resourcequota_data)
            assert self.fedns.is_sub_dict(values, resourcequota_ret.json()), \
                "更新联邦命名空间后，资源配额比对数据失败，返回数据{}，期望数据{}".format(resourcequota_ret.text, values)

    @pytest.mark.upgrade
    def 测试更新联邦命名空间_差异化从有到无(self):
        self.data.update({'override': 'false'})
        ret = self.fedns.update(self.name, self.data)
        assert ret.status_code == 200, "更新联邦命名空间失败:{}".format(ret.text)
        values = self.fedns.generate_jinja_data("verify_data/fedns/create_response.jinja2", self.data)
        assert self.fedns.is_sub_dict(values, ret.json()), \
            "更新联邦命名空间比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)

    @pytest.mark.upgrade
    def 测试更新联邦命名空间_差异化从有到无_验证所有子集群的容器配额(self):
        for cluster in self.fedns.all_cluster_name:
            limitrange_ret = self.fedns.detail_limitrange(cluster, self.name)
            assert limitrange_ret.status_code == 200, "获取容器配额失败:{}".format(limitrange_ret.text)
            values = self.fedns.generate_jinja_data("verify_data/fedns/limitrange_response.jinja2", self.data)
            assert self.fedns.is_sub_dict(values, limitrange_ret.json()), \
                "更新联邦命名空间后，容器配额比对数据失败，返回数据{}，期望数据{}".format(limitrange_ret.text, values)

    @pytest.mark.upgrade
    def 测试更新联邦命名空间_差异化从有到无_验证所有子集群的资源配额(self):
        for cluster in self.fedns.all_cluster_name:
            resourcequota_ret = self.fedns.detail_resourequota(cluster, self.name)
            assert resourcequota_ret.status_code == 200, "获取资源限额失败:{}".format(resourcequota_ret.text)
            values = self.fedns.generate_jinja_data("verify_data/fedns/resourcequota_response.jinja2", self.data)
            assert self.fedns.is_sub_dict(values, resourcequota_ret.json()), \
                "更新联邦命名空间后，资源配额比对数据失败，返回数据{}，期望数据{}".format(resourcequota_ret.text, values)

    @pytest.mark.delete
    def 测试删除联邦命名空间_有差异化(self):
        ret1 = self.fedns.delete_fedns(self.name)
        assert ret1.status_code == 200, "删除联邦命名空间失败:{}".format(ret1.text)
        ret2 = self.fedns.delete_ns(self.name)
        assert ret2.status_code == 200, "删除联邦命名空间所在命名空间失败:{}".format(ret2.text)
        assert self.fedns.check_exists(
            self.fedns.common_url(self.name), 404)
