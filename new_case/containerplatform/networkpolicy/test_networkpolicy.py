import pytest
from common.settings import RERUN_TIMES, RESOURCE_PREFIX, K8S_NAMESPACE, CALICO_REGION
from new_case.containerplatform.networkpolicy.networkpolicy import Networkpolicy
import json


@pytest.mark.skipif(not CALICO_REGION, reason="没有传calico集群名称，默认不跑对应用例")
@pytest.mark.raven
@pytest.mark.BAT
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=1)
class TestNetworkpolicySuit(object):
    def setup_class(self):
        self.networkpolicy = Networkpolicy()
        self.networkpolicy_name = "{}-ares-networkpolicy-0".format(RESOURCE_PREFIX)
        self.calico_cidr = "88.88.0.0/24"
        self.namespace = K8S_NAMESPACE

    @pytest.mark.prepare
    def 测试网络策略增加(self):
        createnetworkpolicy_result = self.networkpolicy.create_networkpolicy(
            './test_data/networkpolicy/network.json',
            {'$networkpolicy_name': self.networkpolicy_name,
             '$NAMESPACE': self.namespace,
             "$subnet_cidr": self.calico_cidr})
        assert createnetworkpolicy_result.status_code == 201, "创建网络策略失败:{}".format(createnetworkpolicy_result.text)

        value = self.networkpolicy.generate_data("verify_data/networkpolicy/create_response.json",
                                                 {"$networkpolicy_name": self.networkpolicy_name,
                                                  "$K8S_NAMESPACE": self.namespace,
                                                  "$subnet_cidr": self.calico_cidr})
        assert self.networkpolicy.is_sub_dict(json.loads(value), createnetworkpolicy_result.json()), \
            "创建网络策略比对数据失败，返回数据:{},期望数据:{}".format(createnetworkpolicy_result.json(), value)

    @pytest.mark.upgrade
    def 测试网络策略列表(self):
        list_result = self.networkpolicy.list_networkpolicy(ns_name=self.namespace, limit=20)
        assert list_result.status_code == 200, "获取网络策略列表失败：{}".format(list_result.text)
        assert self.networkpolicy_name in list_result.text, "列表：新建网络策略不在列表中"

        value = self.networkpolicy.generate_data("verify_data/networkpolicy/networkpolicy_list.json",
                                                 {"$networkpolicy_name": self.networkpolicy_name,
                                                  "$K8S_NAMESPACE": self.namespace,
                                                  "$subnet_cidr": self.calico_cidr})
        assert self.networkpolicy.is_sub_dict(json.loads(value), list_result.json()), \
            "获取网络策略列表比对数据失败，返回数据:{},期望数据:{}".format(list_result.json(), value)

    @pytest.mark.upgrade
    def 测试网络策略更新(self):
        update_result = self.networkpolicy.update_networkpolicy(self.namespace, self.networkpolicy_name,
                                                                "./test_data/networkpolicy/update-networkpolicy.json",
                                                                {"$networkpolicy_name": self.networkpolicy_name,
                                                                 "$subnet_cidr": self.calico_cidr})
        assert update_result.status_code == 200, "更新网络策略出错:{}".format(update_result.text)

        value = self.networkpolicy.generate_data("verify_data/networkpolicy/networkpolicy_update.json",
                                                 {"$networkpolicy_name": self.networkpolicy_name,
                                                  "$K8S_NAMESPACE": self.namespace,
                                                  "$subnet_cidr": self.calico_cidr})
        assert self.networkpolicy.is_sub_dict(json.loads(value), update_result.json()), \
            "更新网络策略比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

    @pytest.mark.delete
    def 测试网络策略删除(self):
        delete_result = self.networkpolicy.delete_networkpolicy(self.namespace, self.networkpolicy_name)
        assert delete_result.status_code == 200, "删除网络策略失败:{}".format(delete_result.text)

        value = self.networkpolicy.generate_data("verify_data/networkpolicy/networkpolicy_delete.json",
                                                 {"$networkpolicy_name": self.networkpolicy_name})
        assert self.networkpolicy.is_sub_dict(json.loads(value), delete_result.json()), \
            "删除网络策略比对数据失败，返回数据:{},期望数据:{}".format(delete_result.json(), value)
