import pytest
from common import settings
from new_case.containerplatform.networkpolicy.networkpolicy import Networkpolicy


@pytest.mark.skipif(not settings.CALICO_REGION, reason="没有传calico集群名称，默认不跑对应用例")
@pytest.mark.raven
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestNetworkplkicySuitePut(object):

    def setup_class(self):
        self.network = Networkpolicy()

    def 测试_更新网络策略_必填参数_name_namespace_可选参数只有1个podSelector(self):
        data = self.network.data_list_podSelector
        data.update({"role": "dbtest"})
        update_result = self.network.update_networkpolicy_jinja2(data['network_name'],
                                                                 './test_data/networkpolicy/networkpolicy.jinja2', data=data)
        assert update_result.status_code == 200, "更新{}网络策略失败{}".format(data['network_name'], update_result.text)

        value = self.network.generate_jinja_data("./verify_data/networkpolicy/create_networkpolicy.jinja2", data)
        assert self.network.is_sub_dict(value, update_result.json()), \
            "更新网络策略_必填参数_name_namespace_可选参数只有1个podSelector比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

    @pytest.mark.skipif(settings.AUDIT_UNABLED, reason="do not have audit")
    def 测试网络策略更新审计(self):
        payload = {"user_name": settings.USERNAME, "operation_type": "update", "resource_type": "networkpolicies",
                   "resource_name": self.network.data_list_podSelector['network_name']}
        result = self.network.search_audit(payload)
        payload.update({"namespace": settings.K8S_NAMESPACE, "region_name": settings.CALICO_REGION})
        values = self.network.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.network.is_sub_dict(values, result.json()), "审计数据不符合预期"

    def 测试_更新网络策略_必填参数_name_namespace_可选参数只有1个ingress(self):
        data = self.network.data_list_ingress
        data.update({"subnet_cidr": "10.10.0.0/24"})
        update_result = self.network.update_networkpolicy_jinja2(data['network_name'],
                                                                 './test_data/networkpolicy/networkpolicy.jinja2', data=data)
        assert update_result.status_code == 200, "更新{}网络策略失败{}".format(data['network_name'], update_result.text)

        value = self.network.generate_jinja_data("./verify_data/networkpolicy/create_networkpolicy.jinja2", data)
        assert self.network.is_sub_dict(value, update_result.json()), \
            "更新网络策略_必填参数_name_namespace_可选参数只有1个ingress比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)
