import sys
from common.base_request import Common
from common.log import logger
from common import settings


class Networkpolicy(Common):

    def get_common_networkpolicy_url_v1(self, ns_name=settings.K8S_NAMESPACE, networkpolicy_name=''):
        return "kubernetes/{}/apis/networking.k8s.io/v1/namespaces/{}/networkpolicies/{}"\
            .format(self.calico_region, ns_name, networkpolicy_name)

    def get_common_networkpolicy_search_url(self, ns_name=settings.K8S_NAMESPACE, limit=1, cnt="", networkpolicy_name=''):
        return limit and "kubernetes/{}/apis/networking.k8s.io/v1/namespaces/{}/networkpolicies?" \
                         "limit={}&continue={}&fieldSelector=metadata.name={}"\
            .format(self.calico_region, ns_name, limit, cnt, networkpolicy_name)

    def get_common_networkpolicy_list_url(self, ns_name=settings.K8S_NAMESPACE, limit=1, cnt=""):
        return limit and "kubernetes/{}/apis/networking.k8s.io/v1/namespaces/{}/networkpolicies?" \
                         "limit={}&continue={}"\
            .format(self.calico_region, ns_name, limit, cnt)

    def get_networkpolicy_template_url(self, ns_name=settings.K8S_NAMESPACE, networkpolicy_name=''):
        return "kubernetes/{}/apis/networking.k8s.io/v1/namespaces/{}/networkpolicies/{}"\
            .format(self.calico_region, ns_name, networkpolicy_name)

    def create_networkpolicy_template(self, file, data):
        path = self.get_networkpolicy_template_url()
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=path, json=data, params={})

    def delete_networkpolicy_template(self, ns_name, networkpolicy_name, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_networkpolicy_template_url(ns_name, networkpolicy_name)
        return self.send(method='delete', path=url, params={}, auth=auth)

    def create_networkpolicy(self, file, data, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_networkpolicy_url_v1()
        data = self.generate_data(file, data)
        return self.send(method='post', path=url, data=data, params={}, auth=auth)

    def list_networkpolicy(self, ns_name="", limit=None, cnt=""):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_networkpolicy_list_url(ns_name=ns_name, limit=limit, cnt=cnt)
        return self.send(method='get', path=url, params={})

    def update_networkpolicy(self, ns_name, networkpolicy_name, file, data):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_networkpolicy_url_v1(ns_name=ns_name, networkpolicy_name=networkpolicy_name)
        data = self.generate_data(file, data)
        return self.send(method='put', path=url, data=data, params={})

    def delete_networkpolicy(self, ns_name,  networkpolicy_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_networkpolicy_url_v1(ns_name, networkpolicy_name)
        return self.send(method='delete', path=url, params={})

    def create_networkpolicy_jinja2(self, file, data):
        path = self.get_common_networkpolicy_url_v1()
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=path, json=data, params={})

    def get_networkpolicy_list(self, limit=None, cnt=""):
        path = self.get_common_networkpolicy_list_url(limit=limit, cnt=cnt)
        return self.send(method='get', path=path, params={})

    def search_networkpolicy(self, ns_name="", limit=None, cnt="", networkpolicy_name=""):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_networkpolicy_search_url(ns_name=ns_name, limit=limit, cnt=cnt, networkpolicy_name=networkpolicy_name)
        return self.send(method='get', path=url)

    def update_networkpolicy_jinja2(self, networkpolicy_name, file, data):
        path = self.get_common_networkpolicy_url_v1(networkpolicy_name=networkpolicy_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='put', path=path, json=data, params={})

    def delete_networkpolicy_jinja2(self, ns_name, networkpolicy_name, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_networkpolicy_url_v1(ns_name, networkpolicy_name)
        return self.send(method='delete', path=url, params={}, auth=auth)

    # 必填参数_name_namespace_可选参数为空
    data_list_null = {
             "network_name": '{}-ares-networkpolicy'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
             "namespace": settings.K8S_NAMESPACE
        }

    # 必填参数_name_namespace_可选参数只有1个podSelector
    data_list_podSelector = {
             "network_name": '{}-ares-networkpolicy-1'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
             "namespace": settings.K8S_NAMESPACE,
             "matchLabels_role": " ",
             "role": "db"
        }

    # 必填参数_name_namespace_可选参数只有1个ingress
    data_list_ingress = {
            "network_name": '{}-ares-networkpolicy-2'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
            "namespace": settings.K8S_NAMESPACE,
            "from_podSelector": "matchLabels_role",
            "matchLabels_role": " ",
            "podrole": "frontend",
            "from_ipBlock": " ",
            "subnet_cidr": "88.88.0.0/24"
    }

    # 网络策略_模板_1
    data_list_template_1 = {
        "name": "default-deny-all-1".replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "policyTypes": " ",
        "policytypes": "Ingress"
    }

    # 网络策略_模板_2
    data_list_template_2 = {
        "name": "deny-other-namespaces-2".replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "ingress": " ",
        "podSelector": " "
    }

    # 网络策略_模板_3
    data_list_template_3 = {
        "name": "allow-other-namespaces-3".replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "ingress": " ",
        "namespaceSelector": " "
    }

    # 网络策略_模板_4
    data_list_template_4 = {
        "name": "allow-ip-4".replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "ingress": " ",
        "ipBlock": " ",
        "cidr": "10.16.0.0/16",
        "except_1": "10.16.8.0/24",
        "except_2": "10.16.0.2/32"
    }

    # 网络策略_模板_5
    data_list_template_5 = {
        "name": "default-deny-all-5".replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "policyTypes": " ",
        "policytypes": "Ingress",
        "podSelector_matchLabels": " "
    }

    # 网络策略_模板_6
    data_list_template_6 = {
        "name": "allow-some-same-namespace-6".replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "podSelector_matchLabels": " ",
        "ingress": " ",
        "podSelector": " ",
        "matchLabels": " "
    }

    # 网络策略_模板_7
    data_list_template_7 = {
        "name": "allow-same-namespace-7".replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "podSelector_matchLabels": " ",
        "ingress": " ",
        "podSelector": " "
    }

    # 网络策略_模板_8
    data_list_template_8 = {
        "name": "allow-some-other-namespace-8".replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "podSelector_matchLabels": " ",
        "ingress": " ",
        "namespaceSelector": " ",
        "podSelector": " ",
        "matchLabels": " "
    }

    # 网络策略_模板_9
    data_list_template_9 = {
        "name": "allow-other-namespace-9".replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "podSelector_matchLabels": " ",
        "ingress": " ",
        "namespaceSelector": " "
    }

    # 网络策略_模板_10
    data_list_template_10 = {
        "name": "allow-ip-10".replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "podSelector_matchLabels": " ",
        "ingress": " ",
        "ipBlock": " ",
        "cidr": "10.16.0.0/16",
        "except_1": "10.16.8.0/24",
        "except_2": "10.16.0.2/32"
    }

    # 网络策略_模板_11
    data_list_template_11 = {
        "name": "default-deny-all-11".replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "policyTypes": " ",
        "policytypes": "Egress",
        "egress": " ",
        "podSelector": " "
    }

    # 网络策略_模板_12
    data_list_template_12 = {
        "name": "allow-ip-12".replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "podSelector_matchLabels": " ",
        "policyTypes": " ",
        "policytypes": "Egress",
        "egress": " ",
        "ipBlock": " ",
        "cidr": "10.16.0.0/16",
        "except_1": "10.16.8.0/24",
        "except_2": "10.16.0.2/32",

    }
    # 在e2enamespace里创建 deployment_app:foo
    data_list_deployment_foo = {
        "deployment_name": '{}-networkpolicy-template-foo'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "metadata_labels": {"app": "foo"}
    }

    # 在e2enamespace里创建 deployment_app:web
    data_list_deployment_web = {
        "deployment_name": '{}-networkpolicy-template-web'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "metadata_labels": {"app": "web"}
    }

    # 创建namespace-a,加标签env: production
    ns_data = {
        "namespace_name": "namespace-a",
        "display_name": settings.CALICO_REGION,
        "cluster": settings.CALICO_REGION,
        "project": settings.CALICO_PROJECT_NAME,
        "ResourceQuota": "True",
        "morelabel": "True",
        "key1": "env",
        "value1": "production"
    }

    # 在namespace-a里创建 deployment_app:mail
    data_list_deployment_a_mail = {
        "deployment_name": '{}-networkpolicy-template-a-mail'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": "namespace-a",
        "image": settings.IMAGE,
        "metadata_labels": {"app": "mail"}
    }

    # 在namespace-a里创建 deployment_app:b
    data_list_deployment_a_b = {
        "deployment_name": '{}-networkpolicy-template-a-b'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": "namespace-a",
        "image": settings.IMAGE,
        "metadata_labels": {"app": "b"}
    }

    # 创建子网 10.16.0.0/16
    data_list_subnet = {
        'subnet_name': "{}-networkpolicy-calico-subnet".format(settings.RESOURCE_PREFIX).replace('_', '-'),
        'calico_cidr': "10.16.0.0/16",
        'nat_outgoing': "false"
    }

    # 在e2enamespace里创建deployment_A,子网：10.16.4.1
    data_list_deployment_a = {
        "deployment_name": '{}-networkpolicy-template-a'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "annotations": " ",
        "ipaddrs": "10.16.4.1"
    }

    # 在e2enamespace里创建deployment_B,子网：10.16.7.1
    data_list_deployment_b = {
        "deployment_name": '{}-networkpolicy-template-b'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "annotations": " ",
        "ipaddrs": "10.16.7.1"
    }

    # 在e2enamespace里创建deployment_C,子网：10.16.8.2
    data_list_deployment_c = {
        "deployment_name": '{}-networkpolicy-template-c'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "annotations": " ",
        "ipaddrs": "10.16.8.2"
    }

    # 在e2enamespace里创建 deployment_role:db
    data_list_deployment_db = {
        "deployment_name": '{}-networkpolicy-template-db'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "metadata_labels": {"role": "db"}
    }

    # 在e2enamespace里创建 deployment_app_mail
    data_list_deployment_mail = {
        "deployment_name": '{}-networkpolicy-template-mail'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "metadata_labels": {"app": "mail"}
    }
