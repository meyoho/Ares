import pytest
from common import settings
from new_case.containerplatform.networkpolicy.networkpolicy import Networkpolicy


@pytest.mark.skipif(not settings.CALICO_REGION, reason="没有传calico集群名称，默认不跑对应用例")
@pytest.mark.raven
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestNetworkplkicySuiteDelete(object):

    def setup_class(self):
        self.network = Networkpolicy()

    def 测试_删除创建网络策略_必填参数_name_namespace_可选参数为空(self):
        data = self.network.data_list_null
        delete_result = self.network.delete_networkpolicy_jinja2(ns_name=data["namespace"], networkpolicy_name=data['network_name'])
        assert delete_result.status_code == 200, "删除{}网络策略失败{}".format(data['network_name'], delete_result.text)

    def 测试_删除创建网络策略_必填参数_name_namespace_可选参数只有1个podSelector(self):
        data = self.network.data_list_podSelector
        delete_result = self.network.delete_networkpolicy_jinja2(ns_name=data["namespace"], networkpolicy_name=data['network_name'])
        assert delete_result.status_code == 200, "删除{}网络策略失败{}".format(data['network_name'], delete_result.text)

    def 测试_删除创建网络策略_必填参数_name_namespace_可选参数只有1个ingress(self):
        data = self.network.data_list_ingress
        delete_result = self.network.delete_networkpolicy_jinja2(ns_name=data["namespace"], networkpolicy_name=data['network_name'])
        assert delete_result.status_code == 200, "删除{}网络策略失败{}".format(data['network_name'], delete_result.text)

    @pytest.mark.skipif(settings.AUDIT_UNABLED, reason="do not have audit")
    def 测试网络策略删除审计(self):
        payload = {"user_name": settings.USERNAME, "operation_type": "delete", "resource_type": "networkpolicies",
                   "resource_name": self.network.data_list_ingress['network_name']}
        result = self.network.search_audit(payload)
        payload.update({"namespace": settings.K8S_NAMESPACE, "region_name": settings.CALICO_REGION})
        values = self.network.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.network.is_sub_dict(values, result.json()), "审计数据不符合预期"
