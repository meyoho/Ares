# coding=utf-8
import pytest
from common import settings
from new_case.platform.project.project import Project
from new_case.containerplatform.namespace.namespace import Namespace


@pytest.fixture(scope="session", autouse=True)
def prepare_and_clear2():
    project = Project()
    namespace = Namespace()
    data = {
        "project_name": settings.CALICO_PROJECT_NAME,
        "regions": [settings.CALICO_REGION],
        "display_name": settings.CALICO_PROJECT_NAME,
        "description": "e2e test project",
    }
    create_project_result = project.create_project("./test_data/project/create_project.jinja2", data)
    assert create_project_result.status_code in (201, 409), "创建项目失败"
    ns_data = {
        "namespace_name": settings.K8S_NAMESPACE,
        "display_name": settings.CALICO_REGION,
        "cluster": settings.CALICO_REGION,
        "project": settings.CALICO_PROJECT_NAME,
        "ResourceQuota": "True",
        "morelabel": "False"
    }
    create_ns_result = namespace.create_namespace('./test_data/namespace/create_namespace.jinja2', ns_data,
                                                  region_name=settings.CALICO_REGION)
    assert create_ns_result.status_code in (200, 409, 500), "创建新命名空间失败 {}".format(create_ns_result.text)
