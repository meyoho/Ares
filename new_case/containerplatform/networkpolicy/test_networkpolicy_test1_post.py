import pytest
from common import settings
from common.settings import USERNAME, CALICO_REGION, K8S_NAMESPACE, AUDIT_UNABLED
from new_case.containerplatform.networkpolicy.networkpolicy import Networkpolicy


@pytest.mark.skipif(not CALICO_REGION, reason="没有传calico集群名称，默认不跑对应用例")
@pytest.mark.raven
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestNetworkplkicySuitePost(object):

    def setup_class(self):
        self.network = Networkpolicy()

    def 测试_创建网络策略_必填参数_name_namespace_可选参数为空(self):
        data = self.network.data_list_null
        create_result = self.network.create_networkpolicy_jinja2("./test_data/networkpolicy/networkpolicy.jinja2", data=data)
        assert create_result.status_code == 201, "创建{}网络策略失败{}".format(data['network_name'], create_result.text)

        value = self.network.generate_jinja_data("./verify_data/networkpolicy/create_networkpolicy.jinja2", data)
        assert self.network.is_sub_dict(value, create_result.json()), \
            "创建网络策略_必填参数_name_namespace_可选参数为空比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 不测试网络策略创建审计(self):
        payload = {"user_name": USERNAME, "operation_type": "create", "resource_type": "networkpolicies",
                   "resource_name": self.network.data_list_null['network_name']}
        result = self.network.search_audit(payload)
        payload.update({"namespace": K8S_NAMESPACE, "region_name": CALICO_REGION, "code": 201})
        values = self.network.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.network.is_sub_dict(values, result.json()), "审计数据不符合预期"

    def 测试_创建网络策略_必填参数_name_namespace_可选参数只有1个podSelector(self):
        data = self.network.data_list_podSelector
        create_result = self.network.create_networkpolicy_jinja2("./test_data/networkpolicy/networkpolicy.jinja2", data=data)
        assert create_result.status_code == 201, "创建{}网络策略失败{}".format(data['network_name'], create_result.text)

        value = self.network.generate_jinja_data("./verify_data/networkpolicy/create_networkpolicy.jinja2", data)
        assert self.network.is_sub_dict(value, create_result.json()), \
            "创建网络策略_必填参数_name_namespace_可选参数只有1个podSelector对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

    def 测试_创建网络策略_必填参数_name_namespace_可选参数只有1个ingress(self):
        data = self.network.data_list_ingress
        create_result = self.network.create_networkpolicy_jinja2("./test_data/networkpolicy/networkpolicy.jinja2",
                                                                 data=data)
        assert create_result.status_code == 201, "创建{}网络策略失败{}".format(data['network_name'], create_result.text)

        value = self.network.generate_jinja_data("./verify_data/networkpolicy/create_networkpolicy.jinja2", data)
        assert self.network.is_sub_dict(value, create_result.json()), \
            "创建网络策略_必填参数_name_namespace_可选参数只有1个podSelector对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)
