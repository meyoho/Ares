import pytest
from common import settings
from new_case.containerplatform.networkpolicy.networkpolicy import Networkpolicy
from new_case.containerplatform.deployment.deployment import Deployment
from new_case.containerplatform.namespace.namespace import Namespace
from new_case.containerplatform.calico.subnet import Subnet
# from common.log import logger


@pytest.mark.skipif(not settings.CALICO_REGION, reason="没有传calico集群名称，默认不跑对应用例")
@pytest.mark.raven
@pytest.mark.metis
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestNetworkplkicySuiteTemplate(object):

    def setup_class(self):
        self.network = Networkpolicy()
        self.deployment = Deployment()
        self.namespace = Namespace()
        self.subnet_client = Subnet()
        self. calico_VM_IPS = "10.0.128.238"
        self.region_name = settings.CALICO_REGION
        self.calico_ns_name = "namespace-a"
        self.ns_name = settings.K8S_NAMESPACE
    '''
        global data_foo
        global pod_ip_foo
        global data_web
        global pod_ip_web
        global data_a_mail
        global pod_ip_a_mail
        global data_a_b
        global pod_ip_a_b
        global data_a
        global pod_ip_a
        global data_b
        global pod_ip_b
        global data_c
        global pod_ip_c
        global data_db
        global pod_ip_db
        global data_mail
        global pod_ip_mail

        # 在e2enamespace里创建 deployment_app:foo
        data_foo = self.network.data_list_deployment_foo
        create_result_foo = self.deployment.create_deployment_jinja2("./test_data/deployment/deployment.jinja2",
                                                                     data=data_foo, region_name=self.region_name, ns_name=self.ns_name)
        assert create_result_foo.status_code == 201, "创建{}部署app:foo失败{}".format(data_foo['deployment_name'],
                                                                                create_result_foo.text)
        detail_result_foo = self.deployment.detail_deplyment_jinja2(ns_name=data_foo['namespace'],
                                                                    deployment_name=data_foo['deployment_name'],
                                                                    region_name=self.region_name)
        assert detail_result_foo.status_code == 200, "获取部署app:foo{}详情页失败,{}".format(data_foo['deployment_name'],
                                                                                    detail_result_foo.text)
        deployment_status_foo = self.deployment.get_deployment_status(ns_name=data_foo['namespace'],
                                                                      deployment_name=data_foo["deployment_name"],
                                                                      region_name=self.region_name)
        assert deployment_status_foo, "创建部署后，验证部署app:foo状态出错: 部署：{} is not running".format(data_foo["deployment_name"])
        pod_result_foo = self.deployment.get_deplyment_pods(ns_name=data_foo['namespace'],
                                                            deployment_name=data_foo['deployment_name'],
                                                            region_name=self.region_name)
        pod_ip_foo = self.deployment.get_value(pod_result_foo.json(), 'items.0.status.podIP')

        # 在e2enamespace里创建 deployment_app:web
        data_web = self.network.data_list_deployment_web
        create_result_web = self.deployment.create_deployment_jinja2("./test_data/deployment/deployment.jinja2",
                                                                     data=data_web, region_name=self.region_name, ns_name=self.ns_name)
        assert create_result_web.status_code == 201, "创建{}部署app:web失败{}".format(data_web['deployment_name'],
                                                                                create_result_web.text)
        detail_result_web = self.deployment.detail_deplyment_jinja2(ns_name=data_web['namespace'],
                                                                    deployment_name=data_web['deployment_name'],
                                                                    region_name=self.region_name)
        assert detail_result_web.status_code == 200, "获取部署app:web{}详情页失败,{}".format(data_web['deployment_name'],
                                                                                    detail_result_web.text)
        deployment_status_web = self.deployment.get_deployment_status(ns_name=data_web['namespace'],
                                                                      deployment_name=data_web["deployment_name"],
                                                                      region_name=self.region_name)
        assert deployment_status_web, "创建部署后，验证部署app:web状态出错: 部署：{} is not running".format(data_web["deployment_name"])
        pod_result_web = self.deployment.get_deplyment_pods(ns_name=data_web['namespace'],
                                                            deployment_name=data_web['deployment_name'],
                                                            region_name=self.region_name)
        pod_ip_web = self.deployment.get_value(pod_result_web.json(), 'items.0.status.podIP')

        # 创建namespace-a,加标签env: production
        ns_data = self.network.ns_data
        create_ns_result = self.namespace.create_namespace('./test_data/namespace/create_namespace.jinja2', ns_data,
                                                           region_name=self.region_name)
        assert create_ns_result.status_code in (200, 409, 500), "创建新命名空间失败 {}".format(create_ns_result.text)

        # 在namespace-a 里创建 deployment_app:mail
        data_a_mail = self.network.data_list_deployment_a_mail
        create_result_a_mail = self.deployment.create_deployment_jinja2("./test_data/deployment/deployment.jinja2",
                                                                        data=data_a_mail, region_name=self.region_name,
                                                                        ns_name=self.calico_ns_name)
        assert create_result_a_mail.status_code == 201, "创建namespace-a 里{}部署app:mail失败{}".\
            format(data_a_mail['deployment_name'], create_result_a_mail.text)
        detail_result_a_mail = self.deployment.detail_deplyment_jinja2(ns_name=data_a_mail['namespace'],
                                                                       deployment_name=data_a_mail['deployment_name'],
                                                                       region_name=self.region_name)
        assert detail_result_a_mail.status_code == 200, "获取部署app:mail{}详情页失败,{}".format(data_a_mail['deployment_name'],
                                                                                        detail_result_a_mail.text)
        deployment_status_a_mail = self.deployment.get_deployment_status(ns_name=data_a_mail['namespace'],
                                                                         deployment_name=data_a_mail["deployment_name"],
                                                                         region_name=self.region_name)
        assert deployment_status_a_mail, "创建部署后，验证部署app:mail状态出错: 部署：{} is not running".format(data_a_mail["deployment_name"])
        pod_result_a_mail = self.deployment.get_deplyment_pods(ns_name=data_a_mail['namespace'],
                                                               deployment_name=data_a_mail['deployment_name'],
                                                               region_name=self.region_name)
        pod_ip_a_mail = self.deployment.get_value(pod_result_a_mail.json(), 'items.0.status.podIP')

        # 在namespace-a里创建 deployment_app:b
        data_a_b = self.network.data_list_deployment_a_b
        create_result_a_b = self.deployment.create_deployment_jinja2("./test_data/deployment/deployment.jinja2",
                                                                     data=data_a_b, region_name=self.region_name,
                                                                     ns_name=self.calico_ns_name)
        assert create_result_a_b.status_code == 201, "创建namespace-a 里{}部署app:b失败{}".format(data_a_b['deployment_name'],
                                                                                           create_result_a_b.text)
        detail_result_a_b = self.deployment.detail_deplyment_jinja2(ns_name=data_a_b['namespace'],
                                                                    deployment_name=data_a_b['deployment_name'],
                                                                    region_name=self.region_name)
        assert detail_result_a_b.status_code == 200, "获取部署app:mail{}详情页失败,{}".format(data_a_b['deployment_name'],
                                                                                     detail_result_a_b.text)
        deployment_status_a_b = self.deployment.get_deployment_status(ns_name=data_a_b['namespace'],
                                                                      deployment_name=data_a_b["deployment_name"],
                                                                      region_name=self.region_name)
        assert deployment_status_a_b, "创建部署后，验证部署app:mail状态出错: 部署：{} is not running".format(
            data_a_b["deployment_name"])
        pod_result_a_b = self.deployment.get_deplyment_pods(ns_name=data_a_b['namespace'],
                                                            deployment_name=data_a_b['deployment_name'],
                                                            region_name=self.region_name)
        pod_ip_a_b = self.deployment.get_value(pod_result_a_b.json(), 'items.0.status.podIP')

        # 创建子网 10.16.0.0/16
        data_subnet = self.network.data_list_subnet
        createsubnet_result = self.subnet_client.create_subnet("./test_data/calico/calico-subnet.jinja2", data_subnet)
        assert createsubnet_result.status_code == 201, "创建子网失败".format(createsubnet_result.text)

        # 在e2enamespace里创建deployment_A,子网：10.16.4.1
        data_a = self.network.data_list_deployment_a
        create_result_a = self.deployment.create_deployment_jinja2("./test_data/deployment/deployment.jinja2",
                                                                   data=data_a, region_name=self.region_name, ns_name=self.ns_name)
        assert create_result_a.status_code == 201, "创建{}部署a失败{}".format(data_a['deployment_name'], create_result_a.text)
        detail_result_a = self.deployment.detail_deplyment_jinja2(ns_name=data_a['namespace'],
                                                                  deployment_name=data_a['deployment_name'],
                                                                  region_name=self.region_name)
        assert detail_result_a.status_code == 200, "获取部署app:mail{}详情页失败,{}".format(data_a['deployment_name'],
                                                                                   detail_result_a.text)
        deployment_status_a = self.deployment.get_deployment_status(ns_name=data_a['namespace'],
                                                                    deployment_name=data_a["deployment_name"],
                                                                    region_name=self.region_name)
        assert deployment_status_a, "创建部署后，验证部署app:mail状态出错: 部署：{} is not running".format(data_a["deployment_name"])

        pod_result_a = self.deployment.get_deplyment_pods(ns_name=data_a['namespace'],
                                                          deployment_name=data_a['deployment_name'],
                                                          region_name=self.region_name)
        pod_ip_a = self.deployment.get_value(pod_result_a.json(), 'items.0.status.podIP')

        # 在e2enamespace里创建deployment_B,子网：10.16.7.1
        data_b = self.network.data_list_deployment_b
        create_result_b = self.deployment.create_deployment_jinja2("./test_data/deployment/deployment.jinja2",
                                                                   data=data_b, region_name=self.region_name,
                                                                   ns_name=self.ns_name)
        assert create_result_b.status_code == 201, "创建{}部署b失败{}".format(data_b['deployment_name'],
                                                                        create_result_b.text)
        detail_result_b = self.deployment.detail_deplyment_jinja2(ns_name=data_b['namespace'],
                                                                  deployment_name=data_b['deployment_name'],
                                                                  region_name=self.region_name)
        assert detail_result_b.status_code == 200, "获取部署b{}详情页失败,{}".format(data_b['deployment_name'],
                                                                            detail_result_b.text)
        deployment_status_b = self.deployment.get_deployment_status(ns_name=data_b['namespace'],
                                                                    deployment_name=data_b["deployment_name"],
                                                                    region_name=self.region_name)
        assert deployment_status_b, "创建部署后，验证部署b状态出错: 部署：{} is not running".format(data_b["deployment_name"])

        pod_result_b = self.deployment.get_deplyment_pods(ns_name=data_b['namespace'],
                                                          deployment_name=data_b['deployment_name'],
                                                          region_name=self.region_name)
        pod_ip_b = self.deployment.get_value(pod_result_b.json(), 'items.0.status.podIP')

        # 在e2enamespace里创建deployment_C,子网：10.16.8.2
        data_c = self.network.data_list_deployment_c
        create_result_c = self.deployment.create_deployment_jinja2("./test_data/deployment/deployment.jinja2",
                                                                   data=data_c, region_name=self.region_name,
                                                                   ns_name=self.ns_name)
        assert create_result_c.status_code == 201, "创建{}部署c失败{}".format(data_c['deployment_name'], create_result_c.text)
        detail_result_c = self.deployment.detail_deplyment_jinja2(ns_name=data_c['namespace'],
                                                                  deployment_name=data_c['deployment_name'],
                                                                  region_name=self.region_name)
        assert detail_result_c.status_code == 200, "获取部署c{}详情页失败,{}".format(data_c['deployment_name'],
                                                                            detail_result_c.text)
        deployment_status_c = self.deployment.get_deployment_status(ns_name=data_c['namespace'],
                                                                    deployment_name=data_c["deployment_name"],
                                                                    region_name=self.region_name)
        assert deployment_status_c, "创建部署后，验证部署c状态出错: 部署：{} is not running".format(data_c["deployment_name"])

        pod_result_c = self.deployment.get_deplyment_pods(ns_name=data_c['namespace'],
                                                          deployment_name=data_c['deployment_name'],
                                                          region_name=self.region_name)
        pod_ip_c = self.deployment.get_value(pod_result_c.json(), 'items.0.status.podIP')

        # 在e2enamespace里创建 deployment_role:db
        data_db = self.network.data_list_deployment_db
        create_result_db = self.deployment.create_deployment_jinja2("./test_data/deployment/deployment.jinja2",
                                                                    data=data_db, region_name=self.region_name,
                                                                    ns_name=self.ns_name)
        assert create_result_db.status_code == 201, "创建{}部署db失败{}".format(data_db['deployment_name'], create_result_db.text)
        detail_result_db = self.deployment.detail_deplyment_jinja2(ns_name=data_db['namespace'],
                                                                   deployment_name=data_db['deployment_name'],
                                                                   region_name=self.region_name)
        assert detail_result_db.status_code == 200, "获取部署db{}详情页失败,{}".format(data_db['deployment_name'],
                                                                              detail_result_db.text)
        deployment_status_db = self.deployment.get_deployment_status(ns_name=data_db['namespace'],
                                                                     deployment_name=data_db["deployment_name"],
                                                                     region_name=self.region_name)
        assert deployment_status_db, "创建部署后，验证部署db状态出错: 部署：{} is not running".format(data_db["deployment_name"])

        pod_result_db = self.deployment.get_deplyment_pods(ns_name=data_db['namespace'],
                                                           deployment_name=data_db['deployment_name'],
                                                           region_name=self.region_name)
        pod_ip_db = self.deployment.get_value(pod_result_db.json(), 'items.0.status.podIP')

        # 在e2enamespace里创建 deployment_app_mail
        data_mail = self.network.data_list_deployment_mail
        create_result_mail = self.deployment.create_deployment_jinja2("./test_data/deployment/deployment.jinja2",
                                                                      data=data_mail, region_name=self.region_name,
                                                                      ns_name=self.ns_name)
        assert create_result_mail.status_code == 201, "创建e2enamespace里{}部署mail失败{}".format(data_mail['deployment_name'],
                                                                                           create_result_mail.text)
        detail_result_mail = self.deployment.detail_deplyment_jinja2(ns_name=data_mail['namespace'],
                                                                     deployment_name=data_mail['deployment_name'],
                                                                     region_name=self.region_name)
        assert detail_result_mail.status_code == 200, "获取部署db{}详情页失败,{}".format(data_mail['deployment_name'],
                                                                                detail_result_mail.text)
        deployment_status_mail = self.deployment.get_deployment_status(ns_name=data_mail['namespace'],
                                                                       deployment_name=data_mail["deployment_name"],
                                                                       region_name=self.region_name)
        assert deployment_status_mail, "创建部署后，验证部署db状态出错: 部署：{} is not running".format(data_mail["deployment_name"])

        pod_result_mail = self.deployment.get_deplyment_pods(ns_name=data_mail['namespace'],
                                                             deployment_name=data_mail['deployment_name'],
                                                             region_name=self.region_name)
        pod_ip_mail = self.deployment.get_value(pod_result_mail.json(), 'items.0.status.podIP')
    '''

    def 测试_创建网络策略_模板1_全隔离namespace内pod相互无法访问也不允许外部访问(self):
        data = self.network.data_list_template_1
        create_result = self.network.create_networkpolicy_template("./test_data/networkpolicy/networkpolicy_template.jinja2", data=data)
        assert create_result.status_code == 201, "创建网络策略_模板1失败{}".format(create_result.text)

        value = self.network.generate_jinja_data("./verify_data/networkpolicy/networkpolicy_template_verify.jinja2", data)
        assert self.network.is_sub_dict(value, create_result.json()), \
            "创建网络策略_模板1比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)
        '''
        # 验证app:foo访问不了app:web
        ret_1 = self.deployment.kubectl_exec(deployment_name=data_foo['deployment_name'],
                                             ns_name=data_foo['namespace'],
                                             region_name=settings.CALICO_REGION,
                                             cmd="curl {}".format(pod_ip_web),
                                             expect_value="Failed to connect to {} port 80: Operation timed out"
                                             .format(pod_ip_web),
                                             public_ip=self.calico_VM_IPS)
        logger.info(ret_1)
        assert ret_1[0], "app:foo访问不了app:web,出现错误{}".format(ret_1[1])

        # 验证app:web访问不了app:foo
        ret_2 = self.deployment.kubectl_exec(deployment_name=data_web['deployment_name'],
                                             ns_name=data_web['namespace'],
                                             region_name=settings.CALICO_REGION,
                                             cmd="curl {}".format(pod_ip_foo),
                                             expect_value="Failed to connect to {} port 80: Operation timed out"
                                             .format(pod_ip_foo),
                                             public_ip=self.calico_VM_IPS)
        assert ret_2[0], "app:web访问不了app:foo,出现错误{}".format(ret_2[1])
        '''
        # 删除模板
        delete_result = self.network.delete_networkpolicy_template(ns_name=data["namespace"],
                                                                   networkpolicy_name=data['name'])
        assert delete_result.status_code == 200, "删除{}网络策略_模板1失败{}".format(data['network_name'], delete_result.text)

    def 测试_创建网络策略_模板2_namespace内pod互通_namespace外pod访问拒绝(self):
        data = self.network.data_list_template_2
        create_result = self.network.\
            create_networkpolicy_template("./test_data/networkpolicy/networkpolicy_template.jinja2", data=data)
        assert create_result.status_code == 201, "创建网络策略_模板2失败{}".format(create_result.text)

        value = self.network.generate_jinja_data("./verify_data/networkpolicy/networkpolicy_template_verify.jinja2", data)
        assert self.network.is_sub_dict(value, create_result.json()), "创建网络策略_模板2比对数据失败，返回数据:{},期望数据:{}".format(
            create_result.json(), value)
        '''
        # 验证app:foo访问app:web
        ret_1 = self.deployment.kubectl_exec(deployment_name=data_foo['deployment_name'],
                                             ns_name=data_foo['namespace'],
                                             region_name=settings.CALICO_REGION,
                                             cmd="curl {}".format(pod_ip_web),
                                             expect_value="Hello World!",
                                             public_ip=self.calico_VM_IPS)
        logger.info(ret_1)
        assert ret_1[0], "app:foo访问app:web,出现错误{}".format(ret_1[1])

        # 验证app:web访问app:foo
        ret_2 = self.deployment.kubectl_exec(deployment_name=data_web['deployment_name'],
                                             ns_name=data_web['namespace'],
                                             region_name=settings.CALICO_REGION,
                                             cmd="curl {}".format(pod_ip_foo),
                                             expect_value="Hello World!",
                                             public_ip=self.calico_VM_IPS)
        assert ret_2[0], "app:web访问app:foo,出现错误{}".format(ret_2[1])
        '''
        # 删除模板
        delete_result = self.network.delete_networkpolicy_template(ns_name=data["namespace"],
                                                                   networkpolicy_name=data['name'])
        assert delete_result.status_code == 200, "删除{}网络策略_模板2失败{}".format(data['network_name'], delete_result.text)

    def 测试_创建网络策略_模板3_允许某个namespace访问当前namespace(self):
        data = self.network.data_list_template_3
        create_result = self.network.\
            create_networkpolicy_template("./test_data/networkpolicy/networkpolicy_template.jinja2", data=data)
        assert create_result.status_code == 201, "创建网络策略_模板3失败{}".format(create_result.text)

        value = self.network.generate_jinja_data("./verify_data/networkpolicy/networkpolicy_template_verify.jinja2", data)
        assert self.network.is_sub_dict(value, create_result.json()), "创建网络策略_模板3比对数据失败，返回数据:{},期望数据:{}".format(
            create_result.json(), value)
        '''
        # 验证namespace-a里的app:mail访问app:foo
        ret_1 = self.deployment.kubectl_exec(deployment_name=data_a_mail['deployment_name'],
                                             ns_name=data_a_mail['namespace'],
                                             region_name=settings.CALICO_REGION,
                                             cmd="curl {}".format(pod_ip_foo),
                                             expect_value="Hello World!",
                                             public_ip=self.calico_VM_IPS)
        assert ret_1[0], "app:mail访问app:foo,出现错误{}".format(ret_1[1])

        # 验证app:foo访问不了app:web
        ret_2 = self.deployment.kubectl_exec(deployment_name=data_foo['deployment_name'],
                                             ns_name=data_foo['namespace'],
                                             region_name=settings.CALICO_REGION,
                                             cmd="curl {}".format(pod_ip_web),
                                             expect_value="Failed to connect to {} port 80: Operation timed out"
                                             .format(pod_ip_web),
                                             public_ip=self.calico_VM_IPS)
        assert ret_2[0], "app:foo访问不了app:web,出现错误{}".format(ret_2[1])
        '''
        # 删除模板
        delete_result = self.network.delete_networkpolicy_template(ns_name=data["namespace"],
                                                                   networkpolicy_name=data['name'])
        assert delete_result.status_code == 200, "删除{}网络策略_模板3失败{}".format(data['network_name'], delete_result.text)

    def 测试_创建网络策略_模板4_允许某个IP访问当前namespace(self):
        data = self.network.data_list_template_4
        create_result = self.network.\
            create_networkpolicy_template("./test_data/networkpolicy/networkpolicy_template.jinja2", data=data)
        assert create_result.status_code == 201, "创建网络策略_模板4失败{}".format(create_result.text)

        value = self.network.generate_jinja_data("./verify_data/networkpolicy/networkpolicy_template_verify.jinja2", data)
        assert self.network.is_sub_dict(value, create_result.json()), "创建网络策略_模板4比对数据失败，返回数据:{},期望数据:{}".format(
            create_result.json(), value)
        '''
        # 验证deployment_B,子网：10.16.7.1访问 deployment_A,子网：10.16.4.1
        ret_1 = self.deployment.kubectl_exec(deployment_name=data_b['deployment_name'],
                                             ns_name=data_b['namespace'],
                                             region_name=settings.CALICO_REGION,
                                             cmd="curl {}".format(pod_ip_a),
                                             expect_value="Hello World!",
                                             public_ip=self.calico_VM_IPS)
        assert ret_1[0], "deployment_B,子网：10.16.7.1访问 deployment_A,子网：10.16.4.1,出现错误{}".format(ret_1[1])

        # 验证deployment_C,子网：10.16.8.2访问不了 deployment_A,子网：10.16.4.1
        ret_2 = self.deployment.kubectl_exec(deployment_name=data_c['deployment_name'],
                                             ns_name=data_c['namespace'],
                                             region_name=settings.CALICO_REGION,
                                             cmd="curl {}".format(pod_ip_a),
                                             expect_value="Failed to connect to {} port 80: Operation timed out",
                                             public_ip=self.calico_VM_IPS)
        assert ret_2[0], "deployment_C,子网：10.16.8.2访问不了 deployment_A,子网：10.16.4.1,出现错误{}".format(ret_2[1])
        '''
        # 删除模板
        delete_result = self.network.delete_networkpolicy_template(ns_name=data["namespace"],
                                                                   networkpolicy_name=data['name'])
        assert delete_result.status_code == 200, "删除{}网络策略_模板4失败{}".format(data['network_name'], delete_result.text)

    def 测试_创建网络策略_模板5_禁止对指定pod的访问(self):
        data = self.network.data_list_template_5
        create_result = self.network.create_networkpolicy_template(
            "./test_data/networkpolicy/networkpolicy_template.jinja2", data=data)
        assert create_result.status_code == 201, "创建网络策略_模板5失败{}".format(create_result.text)

        value = self.network.generate_jinja_data("./verify_data/networkpolicy/networkpolicy_template_verify.jinja2", data)
        assert self.network.is_sub_dict(value, create_result.json()), "创建网络策略_模板5比对数据失败，返回数据:{},期望数据:{}".format(
            create_result.json(), value)
        '''
        # 验证app:foo 访问不了 role:db
        ret_1 = self.deployment.kubectl_exec(deployment_name=data_foo['deployment_name'],
                                             ns_name=data_foo['namespace'],
                                             region_name=settings.CALICO_REGION,
                                             cmd="curl {}".format(pod_ip_db),
                                             expect_value="Failed to connect to {} port 80: Operation timed out",
                                             public_ip=self.calico_VM_IPS)
        assert ret_1[0], "app:foo 访问不了 role:db,出现错误{}".format(ret_1[1])

        # 验证namespace-a里的app:mail 访问不了 role:db
        ret_2 = self.deployment.kubectl_exec(deployment_name=data_a_mail['deployment_name'],
                                             ns_name=data_a_mail['namespace'],
                                             region_name=settings.CALICO_REGION,
                                             cmd="curl {}".format(pod_ip_db),
                                             expect_value="Failed to connect to {} port 80: Operation timed out",
                                             public_ip=self.calico_VM_IPS)
        assert ret_2[0], "app:mail 访问不了 role:db,出现错误{}".format(ret_2[1])
        '''
        # 删除模板
        delete_result = self.network.delete_networkpolicy_template(ns_name=data["namespace"], networkpolicy_name=data['name'])
        assert delete_result.status_code == 200, "删除{}网络策略_模板5失败{}".format(data['network_name'], delete_result.text)

    def 测试_创建网络策略_模板6_namespace内的某些pod可访问指定pod(self):
        data = self.network.data_list_template_6
        create_result = self.network.create_networkpolicy_template(
            "./test_data/networkpolicy/networkpolicy_template.jinja2", data=data)
        assert create_result.status_code == 201, "创建网络策略_模板6失败{}".format(create_result.text)

        value = self.network.generate_jinja_data("./verify_data/networkpolicy/networkpolicy_template_verify.jinja2", data)
        assert self.network.is_sub_dict(value, create_result.json()), "创建网络策略_模板6比对数据失败，返回数据:{},期望数据:{}".format(
            create_result.json(), value)
        '''
        # 验证e2enamespace里的app:mail 访问role:db
        ret_1 = self.deployment.kubectl_exec(deployment_name=data_mail['deployment_name'],
                                             ns_name=data_mail['namespace'],
                                             region_name=settings.CALICO_REGION,
                                             cmd="curl {}".format(pod_ip_db),
                                             expect_value="Hello World!",
                                             public_ip=self.calico_VM_IPS)
        assert ret_1[0], "e2enamespace里的app:mail 访问role:db,出现错误{}".format(ret_1[1])

        # 验证app:web 访问不了 role:db
        ret_1 = self.deployment.kubectl_exec(deployment_name=data_web['deployment_name'],
                                             ns_name=data_web['namespace'],
                                             region_name=settings.CALICO_REGION,
                                             cmd="curl {}".format(pod_ip_db),
                                             expect_value="Failed to connect to {} port 80: Operation timed out",
                                             public_ip=self.calico_VM_IPS)
        assert ret_1[0], "app:web 访问不了 role:db,出现错误{}".format(ret_1[1])
        '''
        # 删除模板
        delete_result = self.network.delete_networkpolicy_template(ns_name=data["namespace"],
                                                                   networkpolicy_name=data['name'])
        assert delete_result.status_code == 200, "删除{}网络策略_模板6失败{}".format(data['network_name'], delete_result.text)

    def 测试_创建网络策略_模板7_namespace内都可访问指定pod(self):
        data = self.network.data_list_template_7
        create_result = self.network.create_networkpolicy_template(
            "./test_data/networkpolicy/networkpolicy_template.jinja2", data=data)
        assert create_result.status_code == 201, "创建网络策略_模板7失败{}".format(create_result.text)

        value = self.network.generate_jinja_data("./verify_data/networkpolicy/networkpolicy_template_verify.jinja2", data)
        assert self.network.is_sub_dict(value, create_result.json()), "创建网络策略_模板7比对数据失败，返回数据:{},期望数据:{}".format(
            create_result.json(), value)
        '''
        # 验证app:foo 访问 role:db
        ret_1 = self.deployment.kubectl_exec(deployment_name=data_foo['deployment_name'],
                                             ns_name=data_foo['namespace'],
                                             region_name=settings.CALICO_REGION,
                                             cmd="curl {}".format(pod_ip_db),
                                             expect_value="Hellocd  World!",
                                             public_ip=self.calico_VM_IPS)
        assert ret_1[0], "app:foo 访问不了 role:db,出现错误{}".format(ret_1[1])

        # 验证namespace-a里的app:mail 访问不了 role:db
        ret_2 = self.deployment.kubectl_exec(deployment_name=data_a_mail['deployment_name'],
                                             ns_name=data_a_mail['namespace'],
                                             region_name=settings.CALICO_REGION,
                                             cmd="curl {}".format(pod_ip_db),
                                             expect_value="Failed to connect to {} port 80: Operation timed out",
                                             public_ip=self.calico_VM_IPS)
        assert ret_2[0], "app:mail 访问不了 role:db,出现错误{}".format(ret_2[1])
        '''
        # 删除模板
        delete_result = self.network.delete_networkpolicy_template(ns_name=data["namespace"],
                                                                   networkpolicy_name=data['name'])
        assert delete_result.status_code == 200, "删除{}网络策略_模板7失败{}".format(data['network_name'], delete_result.text)

    def 测试_创建网络策略_模板8_某些namespace的某些pod可访问指定pod(self):
        data = self.network.data_list_template_8
        create_result = self.network.create_networkpolicy_template(
            "./test_data/networkpolicy/networkpolicy_template.jinja2", data=data)
        assert create_result.status_code == 201, "创建网络策略_模板8失败{}".format(create_result.text)

        value = self.network.generate_jinja_data("./verify_data/networkpolicy/networkpolicy_template_verify.jinja2", data)
        assert self.network.is_sub_dict(value, create_result.json()), "创建网络策略_模板8比对数据失败，返回数据:{},期望数据:{}".format(
            create_result.json(), value)
        '''
        # 验证namespace-a里的app:mail 访问 role:db
        ret_1 = self.deployment.kubectl_exec(deployment_name=data_a_mail['deployment_name'],
                                             ns_name=data_a_mail['namespace'],
                                             region_name=settings.CALICO_REGION,
                                             cmd="curl {}".format(pod_ip_db),
                                             expect_value="Hello World!",
                                             public_ip=self.calico_VM_IPS)
        assert ret_1[0], "app:mail 访问 role:db,出现错误{}".format(ret_1[1])

        # 验证namespace-a里的app:b 访问不了 role:db
        ret_2 = self.deployment.kubectl_exec(deployment_name=data_a_b['deployment_name'],
                                             ns_name=data_a_b['namespace'],
                                             region_name=settings.CALICO_REGION,
                                             cmd="curl {}".format(pod_ip_db),
                                             expect_value="Failed to connect to {} port 80: Operation timed out",
                                             public_ip=self.calico_VM_IPS)
        assert ret_2[0], "app:b 访问 role:db,出现错误{}".format(ret_2[1])
        '''
        # 删除模板
        delete_result = self.network.delete_networkpolicy_template(ns_name=data["namespace"],
                                                                   networkpolicy_name=data['name'])
        assert delete_result.status_code == 200, "删除{}网络策略_模板8失败{}".format(data['network_name'], delete_result.text)

    def 测试_创建网络策略_模板9_某些namespace内所有pod可访问指定pod(self):
        data = self.network.data_list_template_9
        create_result = self.network.create_networkpolicy_template(
            "./test_data/networkpolicy/networkpolicy_template.jinja2", data=data)
        assert create_result.status_code == 201, "创建网络策略_模板9失败{}".format(create_result.text)

        value = self.network.generate_jinja_data("./verify_data/networkpolicy/networkpolicy_template_verify.jinja2", data)
        assert self.network.is_sub_dict(value, create_result.json()), "创建网络策略_模板9比对数据失败，返回数据:{},期望数据:{}".format(
            create_result.json(), value)
        '''
        # 验证namespace-a里的app:mail 访问 role:db
        ret_1 = self.deployment.kubectl_exec(deployment_name=data_a_mail['deployment_name'],
                                             ns_name=data_a_mail['namespace'],
                                             region_name=settings.CALICO_REGION,
                                             cmd="curl {}".format(pod_ip_db),
                                             expect_value="Hello World!",
                                             public_ip=self.calico_VM_IPS)
        assert ret_1[0], "app:mail 访问 role:db,出现错误{}".format(ret_1[1])

        # 验证namespace-a里的app:b 访问 role:db
        ret_2 = self.deployment.kubectl_exec(deployment_name=data_a_b['deployment_name'],
                                             ns_name=data_a_b['namespace'],
                                             region_name=settings.CALICO_REGION,
                                             cmd="curl {}".format(pod_ip_db),
                                             expect_value="Hello World!",
                                             public_ip=self.calico_VM_IPS)
        assert ret_2[0], "app:b 访问 role:db,出现错误{}".format(ret_2[1])
        '''
        # 删除模板
        delete_result = self.network.delete_networkpolicy_template(ns_name=data["namespace"],
                                                                   networkpolicy_name=data['name'])
        assert delete_result.status_code == 200, "删除{}网络策略_模板9失败{}".format(data['network_name'], delete_result.text)

    def 测试_创建网络策略_模板10_某些IP可访问指定pod(self):
        data = self.network.data_list_template_10
        create_result = self.network.create_networkpolicy_template(
            "./test_data/networkpolicy/networkpolicy_template.jinja2", data=data)
        assert create_result.status_code == 201, "创建网络策略_模板10失败{}".format(create_result.text)

        value = self.network.generate_jinja_data("./verify_data/networkpolicy/networkpolicy_template_verify.jinja2", data)
        assert self.network.is_sub_dict(value, create_result.json()), "创建网络策略_模板10比对数据失败，返回数据:{},期望数据:{}".format(
            create_result.json(), value)
        '''
        # 验证deployment_B,子网：10.16.7.1访问  role:db
        ret_1 = self.deployment.kubectl_exec(deployment_name=data_b['deployment_name'],
                                             ns_name=data_b['namespace'],
                                             region_name=settings.CALICO_REGION,
                                             cmd="curl {}".format(pod_ip_db),
                                             expect_value="Hello World!",
                                             public_ip=self.calico_VM_IPS)
        assert ret_1[0], "deployment_B,子网：10.16.7.1访问 role:db,出现错误{}".format(ret_1[1])

        # 验证deployment_C,子网：10.16.8.2访问不了 role:db
        ret_2 = self.deployment.kubectl_exec(deployment_name=data_c['deployment_name'],
                                             ns_name=data_c['namespace'],
                                             region_name=settings.CALICO_REGION,
                                             cmd="curl {}".format(pod_ip_db),
                                             expect_value="Failed to connect to {} port 80: Operation timed out",
                                             public_ip=self.calico_VM_IPS)
        assert ret_2[0], "deployment_C,子网：10.16.8.2访问不了 role:db,出现错误{}".format(ret_2[1])
        '''
        # 删除模板
        delete_result = self.network.delete_networkpolicy_template(ns_name=data["namespace"],
                                                                   networkpolicy_name=data['name'])
        assert delete_result.status_code == 200, "删除{}网络策略_模板10失败{}".format(data['network_name'], delete_result.text)

    def 测试_创建网络策略_模板11_namespace里所有pod都不允许对外访问(self):
        data = self.network.data_list_template_11
        create_result = self.network.create_networkpolicy_template(
            "./test_data/networkpolicy/networkpolicy_template.jinja2", data=data)
        assert create_result.status_code == 201, "创建网络策略_模板11失败{}".format(create_result.text)

        value = self.network.generate_jinja_data("./verify_data/networkpolicy/networkpolicy_template_verify.jinja2", data)
        assert self.network.is_sub_dict(value, create_result.json()), "创建网络策略_模板11比对数据失败，返回数据:{},期望数据:{}".format(
            create_result.json(), value)
        '''
        # 验证app:foo访问app:web
        ret_1 = self.deployment.kubectl_exec(deployment_name=data_foo['deployment_name'],
                                             ns_name=data_foo['namespace'],
                                             region_name=settings.CALICO_REGION,
                                             cmd="curl {}".format(pod_ip_web),
                                             expect_value="Hello World!",
                                             public_ip=self.calico_VM_IPS)
        logger.info(ret_1)
        assert ret_1[0], "app:foo访问app:web,出现错误{}".format(ret_1[1])

        # 验证app:web 访问不了 namespace-a里的app:b
        ret_2 = self.deployment.kubectl_exec(deployment_name=data_web['deployment_name'],
                                             ns_name=data_web['namespace'],
                                             region_name=settings.CALICO_REGION,
                                             cmd="curl {}".format(pod_ip_a_b),
                                             expect_value="Failed to connect to {} port 80: Operation timed out",
                                             public_ip=self.calico_VM_IPS)
        assert ret_2[0], "app:web访问不了 namespace-a里的app:b,出现错误{}".format(ret_2[1])
        '''
        # 删除模板
        delete_result = self.network.delete_networkpolicy_template(ns_name=data["namespace"],
                                                                   networkpolicy_name=data['name'])
        assert delete_result.status_code == 200, "删除{}网络策略_模板11失败{}".format(data['network_name'], delete_result.text)

    def 测试_创建网络策略_模板12_指定pod可访问某些IP(self):
        data = self.network.data_list_template_12
        create_result = self.network.create_networkpolicy_template(
            "./test_data/networkpolicy/networkpolicy_template.jinja2", data=data)
        assert create_result.status_code == 201, "创建网络策略_模板12失败{}".format(create_result.text)

        value = self.network.generate_jinja_data("./verify_data/networkpolicy/networkpolicy_template_verify.jinja2", data)
        assert self.network.is_sub_dict(value, create_result.json()), "创建网络策略_模板12比对数据失败，返回数据:{},期望数据:{}".format(
            create_result.json(), value)
        '''
        # 验证role:db 访问  deployment_B,子网：10.16.7.1
        ret_1 = self.deployment.kubectl_exec(deployment_name=data_db['deployment_name'],
                                             ns_name=data_db['namespace'],
                                             region_name=settings.CALICO_REGION,
                                             cmd="curl {}".format(pod_ip_b),
                                             expect_value="Hello World!",
                                             public_ip=self.calico_VM_IPS)
        assert ret_1[0], "role:db 访问  deployment_B,子网：10.16.7.1,出现错误{}".format(ret_1[1])

        # 验证role:db 访问不了 deployment_C,子网：10.16.8.2
        ret_2 = self.deployment.kubectl_exec(deployment_name=data_db['deployment_name'],
                                             ns_name=data_db['namespace'],
                                             region_name=settings.CALICO_REGION,
                                             cmd="curl {}".format(pod_ip_c),
                                             expect_value="Failed to connect to {} port 80: Operation timed out",
                                             public_ip=self.calico_VM_IPS)
        assert ret_2[0], "role:db 访问不了 deployment_C,子网：10.16.8.2,出现错误{}".format(ret_2[1])
        '''
        # 删除模板
        delete_result = self.network.delete_networkpolicy_template(ns_name=data["namespace"],
                                                                   networkpolicy_name=data['name'])
        assert delete_result.status_code == 200, "删除{}网络策略_模板12失败{}".format(data['network_name'], delete_result.text)

    '''
    def 测试_删除deployment和namespace(self):
        # 在e2enamespace里删除 deployment_app:foo
        data_foo = self.network.data_list_deployment_foo
        delete_result_foo = self.deployment.delete_deplyment(ns_name=data_foo['namespace'],
                                                             deployment_name=data_foo['deployment_name'],
                                                             region_name=self.region_name)
        assert delete_result_foo.status_code == 200, "删除部署app:foo失败{}".format(data_foo['deployment_name'],
                                                                              delete_result_foo.text)
        # 在e2enamespace里删除 deployment_app:web
        data_web = self.network.data_list_deployment_web
        delete_result_web = self.deployment.delete_deplyment(ns_name=data_web['namespace'],
                                                             deployment_name=data_web['deployment_name'],
                                                             region_name=self.region_name)
        assert delete_result_web.status_code == 200, "删除部署app:web失败{}".format(data_web['deployment_name'],
                                                                              delete_result_web.text)
        # 在namespace-a 里删除 deployment_app:mail
        data_a_mail = self.network.data_list_deployment_a_mail
        delete_result_a_mail = self.deployment.delete_deplyment(ns_name=data_a_mail['namespace'],
                                                                deployment_name=data_a_mail['deployment_name'],
                                                                region_name=self.region_name)
        assert delete_result_a_mail.status_code == 200, "删除namespace-a 里部署app:mail失败{}".\
            format(data_a_mail['deployment_name'], delete_result_a_mail.text)

        # 在namespace-a里创建 deployment_app:b
        data_a_b = self.network.data_list_deployment_a_b
        delete_result_a_b = self.deployment.delete_deplyment(ns_name=data_a_b['namespace'],
                                                             deployment_name=data_a_b['deployment_name'],
                                                             region_name=self.region_name)
        assert delete_result_a_b.status_code == 200, "删除namespace-a 里部署app:b失败{}".\
            format(data_a_b['deployment_name'], delete_result_a_b.text)
        # 在e2enamespace里删除deployment_A,子网：10.16.4.1
        data_a = self.network.data_list_deployment_a
        delete_result_a = self.deployment.delete_deplyment(ns_name=data_a['namespace'],
                                                           deployment_name=data_a['deployment_name'],
                                                           region_name=self.region_name)
        assert delete_result_a.status_code == 200, "删除部署app:mail失败{}".format(data_a['deployment_name'], delete_result_a.text)
        # 在e2enamespace里删除deployment_B,子网：10.16.7.1
        data_b = self.network.data_list_deployment_b
        delete_result_b = self.deployment.delete_deplyment(ns_name=data_b['namespace'],
                                                           deployment_name=data_b['deployment_name'],
                                                           region_name=self.region_name)
        assert delete_result_b.status_code == 200, "删除部署app:mail失败{}".format(data_b['deployment_name'],
                                                                             delete_result_b.text)

        # 在e2enamespace里创建deployment_C,子网：10.16.8.2
        data_c = self.network.data_list_deployment_c
        delete_result_c = self.deployment.delete_deplyment(ns_name=data_c['namespace'],
                                                           deployment_name=data_c['deployment_name'],
                                                           region_name=self.region_name)
        assert delete_result_c.status_code == 200, "删除部署app:mail失败{}".format(data_c['deployment_name'], delete_result_c.text)

        # 在e2enamespace里创建 deployment_role:db
        data_db = self.network.data_list_deployment_db
        delete_result_db = self.deployment.delete_deplyment(ns_name=data_db['namespace'],
                                                            deployment_name=data_db['deployment_name'],
                                                            region_name=self.region_name)
        assert delete_result_db.status_code == 200, "删除部署app:mail失败{}".format(data_db['deployment_name'],
                                                                              delete_result_db.text)
        # 在e2enamespace里删除 deployment_app_mail
        data_mail = self.network.data_list_deployment_mail
        delete_result_mail = self.deployment.delete_deplyment(ns_name=data_mail['namespace'],
                                                              deployment_name=data_mail['deployment_name'],
                                                              region_name=self.region_name)
        assert delete_result_mail.status_code == 200, "删除部署app:mail失败{}".format(data_mail['deployment_name'],
                                                                                delete_result_mail.text)
        # 删除子网10.16.0.0/16
        data_subnet = self.network.data_list_subnet
        delete_result = self.subnet_client.delete_subnet(data_subnet['subnet_name'])
        assert delete_result.status_code == 200, "删除子网失败:{}".format(delete_result.text)

        # 创建namespace-a,加标签env: production
        ns_data = self.network.ns_data
        delete_ns_result = self.namespace.delete_namespace(ns_data["namespace_name"])
        assert delete_ns_result.status_code == 200, "删除命名空间时失败 {}".format(delete_ns_result.text)
    '''
