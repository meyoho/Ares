import pytest
from common import settings
from new_case.containerplatform.networkpolicy.networkpolicy import Networkpolicy


@pytest.mark.skipif(not settings.CALICO_REGION, reason="没有传calico集群名称，默认不跑对应用例")
@pytest.mark.raven
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestNetworkplkicySuiteGet(object):

    def setup_class(self):
        self.network = Networkpolicy()

    def 测试_搜索网络策略(self):
        data = self.network.data_list_null
        ser = self.network.search_networkpolicy(ns_name=data["namespace"], limit=20, networkpolicy_name=data["network_name"])
        assert ser.status_code == 200, "没有搜索到{}，{}".format(data["network_name"], ser.text)

        value = self.network.generate_jinja_data("./verify_data/networkpolicy/search_networkpolicy.jinja2", data)
        assert self.network.is_sub_dict(value, ser.json()), "搜索网络策略比对数据失败，返回数据:{},期望数据:{}".format(
            ser.json(), value)

    def 测试_获取网络策略列表_有limit参数(self):
        ret = self.network.get_networkpolicy_list(limit=20)
        assert ret.status_code == 200, "获取网络策略列表失败:{}".format(ret.text)
        ret2 = self.network.get_networkpolicy_list(limit=1)
        net_num = len(ret2.json()["items"])
        assert net_num == 1, "获取网络策略列表,limit=1时失败,预期返回1个网络策略,实际返回{}个".format(net_num)

        global name
        name = Networkpolicy.get_value(ret2.json(), "items.0.metadata.name")

        global cnt
        cnt = Networkpolicy.get_value(ret2.json(), "metadata.continue")

    def 测试_获取网络策略列表_有limit参数和continue参数(self):
        ret = self.network.get_networkpolicy_list(limit=1, cnt=cnt)
        assert ret.status_code == 200, "获取网络策略列表_有limit和continue参数:{}".format(ret.text)
        name_continue = Networkpolicy.get_value(ret.json(), "items.0.metadata.name")
        assert name_continue != name, "分页数据相同，网络策略第一页数据:{},网络策略第二页数据:{}".format(name, name_continue)
