import pytest
import json
from common.settings import RERUN_TIMES, REGION_NAME, K8S_NAMESPACE, DEFAULT_NS, CHARTREPO_URL
from common.base_request import Common
from new_case.containerplatform.app_catalog.catalog import Catalog
from new_case.containerplatform.app_catalog.conftest import data_list, update_data, alb2_data, alb2_update_data


@pytest.mark.metis
@pytest.mark.juno
@pytest.mark.captain
@pytest.mark.BAT
@pytest.mark.acp_containerplatform
@pytest.mark.skipif(not CHARTREPO_URL, reason="没有chart地址，catalog测试不跑")
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestAppCatalogSuite(object):
    def setup_class(self):
        self.catalog = Catalog()

    @pytest.mark.prepare
    @pytest.mark.parametrize("key", data_list)
    def 测试_添加模板仓库(self, key):
        '''
        测试_添加模板仓库
        :return:
        '''
        data = data_list[key]
        self.catalog.del_chartrepos(data.get("ctr_name"))
        verify_template = Common.generate_jinja_template(self, './verify_data/app_catalog/create_ctr_response.jinja2')
        create_ctr_result = self.catalog.create_chartrepos('./test_data/app_catalog/create_chartrepo.jinja2', data=data)
        assert create_ctr_result.status_code == 200, "创建{}失败 {}".format(key, create_ctr_result.text)
        value = verify_template.render(data)
        assert self.catalog.is_sub_dict(json.loads(value), create_ctr_result.json()), \
            "创建{}对比数据失败，返回数据:{},期望数据:{}".format(key, create_ctr_result.json(), json.loads(value))

    @pytest.mark.upgrade
    @pytest.mark.parametrize("key", data_list)
    def 测试_查询模板仓库列表页(self, key):
        '''
        测试_查询模板仓库列表页
        :return:
        '''
        data = data_list[key]
        exp_ctr_list = [data.get("ctr_name")]
        list_ctr_result = self.catalog.list_chartrepos()
        assert list_ctr_result.status_code == 200, "查询模板仓库列表页失败 {}".format(list_ctr_result.text)
        ctr_list = self.catalog.get_ctrlist_of_ctr(list_ctr_result.json())
        assert self.catalog.is_sub_list(exp_ctr_list, ctr_list), "查询{}列表页时,对比数据失败,返回数据:{},期望数据:{}".format(key, ctr_list,
                                                                                                          exp_ctr_list)

    @pytest.mark.upgrade
    @pytest.mark.parametrize("key", data_list)
    def 测试_同步模板仓库(self, key):
        '''
        测试_同步模板仓库
        :return:
        '''
        data = data_list[key]
        verify_template = Common.generate_jinja_template(self, './verify_data/app_catalog/create_ctr_response.jinja2')
        resync_ctr_result = self.catalog.resync_chartrepos(data.get("ctr_name"))
        assert resync_ctr_result.status_code == 200, "同步{}失败 {}".format(key, resync_ctr_result.text)
        value = verify_template.render(data)
        assert self.catalog.is_sub_dict(json.loads(value), resync_ctr_result.json()), \
            "同步{}对比数据失败，返回数据:{},期望数据:{}".format(key, resync_ctr_result.json(), json.loads(value))
        assert self.catalog.get_status(
            url="apis/app.alauda.io/v1alpha1/namespaces/{}/chartrepos/{}".format(DEFAULT_NS, data.get("ctr_name")),
            key="status.phase", expect_value="Synced", expect_cnt=160), "同步仓库后,状态无法同步成功"

    @pytest.mark.upgrade
    @pytest.mark.parametrize("key", data_list)
    def 测试_查询模板仓库详情页(self, key):
        '''
        测试_查询模板仓库详情页
        :return:
        '''
        data = data_list[key]
        verify_template = Common.generate_jinja_template(self, './verify_data/app_catalog/get_ctr_response.jinja2')
        get_ctr_result = self.catalog.get_chartrepos(data.get("ctr_name"))
        assert get_ctr_result.status_code == 200, "查询{}详情页失败 {}".format(key, get_ctr_result.text)
        value = verify_template.render(data)
        assert self.catalog.is_sub_dict(json.loads(value), get_ctr_result.json()), \
            "查询{}对比数据失败，返回数据:{},期望数据:{}".format(key, get_ctr_result.json(), json.loads(value))

    @pytest.mark.upgrade
    @pytest.mark.parametrize("key", data_list)
    def 测试_搜索模板仓库(self, key):
        '''
        测试_搜索模板仓库
        :return:
        '''
        data = data_list[key]
        exp_ctr_list = [data.get("ctr_name")]
        search_ctr_result = self.catalog.search_chartrepos(data.get("ctr_name"))
        assert search_ctr_result.status_code == 200, "精确搜索{}失败 {}".format(key, search_ctr_result.text)
        ctr_list = self.catalog.get_ctrlist_of_ctr(search_ctr_result.json())
        assert exp_ctr_list == ctr_list, "搜索{}时,对比数据失败,返回数据:{},期望数据:{}".format(key, ctr_list, exp_ctr_list)

    @pytest.mark.upgrade
    @pytest.mark.parametrize("key", data_list)
    def 测试_查询应用目录列表(self, key):
        data = data_list[key]
        ctr = data.get("ctr_name")
        list_chart_result = self.catalog.list_chart(ctr)
        assert list_chart_result.status_code == 200, "查询{}下的chart列表时失败 {}".format(key, list_chart_result.text)
        chart_name_list = self.catalog.get_ctrlist_of_ctr(list_chart_result.json())
        for chart in chart_name_list:
            get_chart_result = self.catalog.get_chart(chart)
            assert get_chart_result.status_code == 200, "查看{}下的chart:{}详情时失败 {}".format(key, chart,
                                                                                        get_chart_result.text)

    @pytest.mark.prepare
    def 测试_通过应用模板创建应用_参数填写正确(self, alb_name=alb2_data.get("alb_name"), data=alb2_data):
        self.catalog.delete_hr(hr=alb_name)
        verify_template = self.catalog.generate_jinja_template('./verify_data/app_catalog/create_alb2_response.jinja2')
        create_alb2_result = self.catalog.create_template_app('./test_data/app_catalog/create_alb2.jinja2', data=data)
        assert create_alb2_result.status_code == 201, "通过应用模板创建alb2应用_参数填写正确时失败: {}".format(create_alb2_result.text)
        value = verify_template.render(data)
        assert self.catalog.is_sub_dict(json.loads(value),
                                        create_alb2_result.json()), "通过应用模板创建应用_参数填写正确,对比数据失败,返回数据:{},期望数据:{}".format(
            create_alb2_result.json(), json.loads(value))

    @pytest.mark.upgrade
    def 测试_查看模板应用详情页(self, alb_name=alb2_data.get("alb_name"), data=alb2_data):
        verify_template = self.catalog.generate_jinja_template("./verify_data/app_catalog/get_alb2_response.jinja2")
        get_hr_result = self.catalog.get_hr(hr=alb_name)
        assert get_hr_result.status_code == 200, "查看模板应用详情页失败:{}".format(get_hr_result.text)
        value = verify_template.render(data)
        assert self.catalog.is_sub_dict(json.loads(value),
                                        get_hr_result.json()), "查看模板应用详情页,对比数据失败,返回数据:{},期望数据:{}".format(
            get_hr_result.json(), json.loads(value))
        assert self.catalog.get_status(
            url="kubernetes/{}/apis/app.alauda.io/v1alpha1/namespaces/{}/helmrequests/{}".format(
                REGION_NAME, K8S_NAMESPACE, alb_name),
            key="status.phase", expect_value="Synced", expect_cnt=60), "模板应用的状态,一直无法同步成功"
        list_resource_result = self.catalog.list_resources_of_hr(hr=alb_name)
        assert list_resource_result.status_code == 200, "查询模板应用下的资源列表失败:{}".format(list_resource_result.text)

    @pytest.mark.upgrade
    def 测试_更新模板应用(self, alb_name=alb2_data.get("alb_name"), data=alb2_update_data):
        verify_template = self.catalog.generate_jinja_template("./verify_data/app_catalog/create_alb2_response.jinja2")
        get_hr_result = self.catalog.get_hr(hr=alb_name)
        metadata = get_hr_result.json().get('metadata')
        resourceVersion = metadata.get('resourceVersion')
        resourceVersion_data = {"resourceVersion": "{}".format(resourceVersion)}
        data.update(resourceVersion_data)
        update_hr_result = self.catalog.update_hr(alb_name, './test_data/app_catalog/update_alb2.jinja2', data=data)
        assert update_hr_result.status_code == 200, "更新模板应用失败:{}".format(update_hr_result.text)
        value = verify_template.render(data)
        assert self.catalog.is_sub_dict(json.loads(value),
                                        update_hr_result.json()), "更新模板应用对比数据失败,返回数据:{},期望数据:{}".format(
            update_hr_result.json(), json.loads(value))
        assert self.catalog.get_status(
            url="kubernetes/{}/apis/app.alauda.io/v1alpha1/namespaces/{}/helmrequests/{}".format(
                REGION_NAME, K8S_NAMESPACE, alb_name),
            key="status.phase", expect_value="Synced", expect_cnt=60), "模板应用的状态,一直无法同步成功"

    @pytest.mark.upgrade
    def 测试_删除模板应用(self, alb_name=alb2_data.get("alb_name")):
        # verify_template = self.catalog.generate_jinja_template("./verify_data/app_catalog/delete_alb2_response.jinja2")
        delete_hr_result = self.catalog.delete_hr(hr=alb_name)
        assert delete_hr_result.status_code == 200, "删除模板应用失败:{}".format(delete_hr_result.text)
        # data = {"alb_name": "{}".format(alb_name)}
        '''
        value = verify_template.render(data)
        assert self.catalog.is_sub_dict(json.loads(value),
                                        delete_hr_result.json()), "删除模板应用对比数据失败,返回数据:{},期望数据:{}".format(
            delete_hr_result.json(), json.loads(value))
        '''
        assert self.catalog.check_exists(
            url="kubernetes/{}/apis/apps/v1/namespaces/{}/deployments/{}".format(REGION_NAME, K8S_NAMESPACE,
                                                                                 alb_name),
            expect_status=404), "删除模板应用后,hr的关联资源deployment仍然存在"
        assert self.catalog.check_exists(
            url="kubernetes/{}/api/v1/namespaces/{}/configmaps/{}".format(REGION_NAME, K8S_NAMESPACE, alb_name),
            expect_status=404), "删除模板应用后,hr的关联资源configmap仍然存在"

    @pytest.mark.upgrade
    @pytest.mark.parametrize("key", update_data)
    def 测试_更新模板仓库(self, key):
        '''
        测试_更新模板仓库
        :return:
        '''
        data = update_data[key]
        verify_template = Common.generate_jinja_template(self, './verify_data/app_catalog/create_ctr_response.jinja2')
        update_ctr_result = self.catalog.update_chartrepos(data.get("ctr_name"),
                                                           './test_data/app_catalog/create_chartrepo.jinja2', data=data)
        assert update_ctr_result.status_code == 200, "更新{}时失败 {}".format(key, update_ctr_result.text)
        value = verify_template.render(data)
        assert self.catalog.is_sub_dict(json.loads(value), update_ctr_result.json()), \
            "更新{}时,对比数据失败，返回数据:{},期望数据:{}".format(key, update_ctr_result.json(), json.loads(value))

    @pytest.mark.delete
    @pytest.mark.parametrize("key", data_list)
    def 测试_删除模板仓库(self, key):
        '''
        测试_删除模板仓库
        :return:
        '''
        data = data_list[key]
        exp_ctr_list = [data.get("ctr_name")]
        del_ctr_result = self.catalog.del_chartrepos(data.get("ctr_name"))
        assert del_ctr_result.status_code == 200, "删除{}失败 {}".format(key, del_ctr_result.text)
        list_ctr_result = self.catalog.list_chartrepos()
        ctr_list = self.catalog.get_ctrlist_of_ctr(list_ctr_result.json())
        assert not self.catalog.is_sub_list(exp_ctr_list, ctr_list), "删除{}失败,对比数据失败,返回数据:{},期望数据:{}".format(
            key, ctr_list, exp_ctr_list)
