from common import settings
from common.base_request import Common


class Catalog(Common):
    # 创建模板仓库
    def create_chartrepos(self, file, data):
        path = "catalog/v1/chartrepos"
        data = self.generate_jinja_data(file, data)
        return self.send(method='POST', path=path, json=data)

    # 查询模板仓库列表
    def list_chartrepos(self):
        path = "apis/app.alauda.io/v1alpha1/namespaces/{}/chartrepos?fieldSelector=".format(settings.DEFAULT_NS)
        return self.send(method='GET', path=path)

    # 从响应json中获取ctrlist的name
    def get_ctrlist_of_ctr(self, rep_json):
        ctrlist = []
        items_list = rep_json.get("items")
        for data in items_list:
            ctrlist.append(data.get("metadata").get("name"))
        return ctrlist

    # 同步模板仓库
    def resync_chartrepos(self, ctr):
        path = "catalog/v1/chartrepos/{}/{}/resync".format(settings.DEFAULT_NS, ctr)
        # data = self.generate_jinja_data(file, data)
        return self.send(method='PUT', path=path)

    # 删除模板仓库
    def del_chartrepos(self, ctr):
        path = "apis/app.alauda.io/v1alpha1/namespaces/{}/chartrepos/{}".format(settings.DEFAULT_NS, ctr)
        return self.send(method='DELETE', path=path)

    # 查看模板仓库详情
    def get_chartrepos(self, ctr):
        path = "apis/app.alauda.io/v1alpha1/namespaces/{}/chartrepos/{}".format(settings.DEFAULT_NS, ctr)
        return self.send(method='GET', path=path)

    # 搜索模板仓库
    def search_chartrepos(self, searchkey):
        path = "apis/app.alauda.io/v1alpha1/namespaces/{}/chartrepos?fieldSelector=metadata.name={}".format(
            settings.DEFAULT_NS, searchkey)
        return self.send(method='GET', path=path)

    # 更新模板仓库
    def update_chartrepos(self, ctr, file, data):
        path = "catalog/v1/chartrepos/{}/{}".format(settings.DEFAULT_NS, ctr)
        data = self.generate_jinja_data(file, data)
        return self.send(method='PUT', path=path, json=data)

    # 查询模板仓库下的chart列表
    def list_chart(self, ctr):
        path = "catalog/v1/charts/{}?appCluster={}&appNamespace={}&labelSelector=repo={}".format(
            settings.DEFAULT_NS, settings.REGION_NAME, settings.K8S_NAMESPACE, ctr)
        return self.send(method='GET', path=path)

    # 查询模板仓库下的chart详情
    def get_chart(self, ctr):
        path = "catalog/v1/charts/{}?appCluster={}&appNamespace={}&fieldSelector=metadata.name={}".format(
            settings.DEFAULT_NS, settings.REGION_NAME, settings.K8S_NAMESPACE, ctr)
        return self.send(method='GET', path=path)

    # 创建模板应用
    def create_template_app(self, file, data):
        path = "kubernetes/{}/apis/app.alauda.io/v1alpha1/namespaces/{}/helmrequests".format(settings.REGION_NAME,
                                                                                             settings.K8S_NAMESPACE)
        data = self.generate_jinja_data(file, data)
        return self.send(method='POST', path=path, json=data)

    # 查询模板应用详情
    def get_hr(self, hr):
        path = "kubernetes/{}/apis/app.alauda.io/v1alpha1/namespaces/{}/helmrequests/{}".format(
            settings.REGION_NAME, settings.K8S_NAMESPACE, hr)
        return self.send(method='GET', path=path)

    # 查询模板应用下的资源列表
    def list_resources_of_hr(self, hr):
        path = "catalog/v1/clusters/{}/helmrequests/{}/{}/resources".format(
            settings.REGION_NAME, settings.K8S_NAMESPACE, hr)
        return self.send(method='GET', path=path)

    # 更新模板应用
    def update_hr(self, hr, file, data):
        path = "kubernetes/{}/apis/app.alauda.io/v1alpha1/namespaces/{}/helmrequests/{}".format(settings.REGION_NAME,
                                                                                                settings.K8S_NAMESPACE,
                                                                                                hr)
        data = self.generate_jinja_data(file, data)
        return self.send(method='PUT', path=path, json=data)

    # 删除模板应用
    def delete_hr(self, hr):
        path = "kubernetes/{}/apis/app.alauda.io/v1alpha1/namespaces/{}/helmrequests/{}".format(settings.REGION_NAME,
                                                                                                settings.K8S_NAMESPACE,
                                                                                                hr)
        return self.send(method='DELETE', path=path)
