import base64
from common.settings import RESOURCE_PREFIX, PROJECT_NAME, CHARTREPO_TYPE, CHARTREPO_URL, CHARTREPO_USER, \
    CHARTREPO_PASSWORD, K8S_NAMESPACE, REGION_NAME, REGISTRY, DEFAULT_NS, DEFAULT_LABEL, GITLAB_URL, GITLAB_PASSWORD, \
    GITLAB_USERNAME, GITURL_HTTPS, SVNURL_HTTP, SVNURL_HTTP_USER, SVNURL_HTTP_PASSWORD, SVNURL_HTTPS, CATALOG_REPO

data_list = {
    "Chart_有用户名和密码的模板仓库": {
        "ctr_name": "{}-e2etest-chart-repo1".format(RESOURCE_PREFIX).replace('_', '-'),
        "project_name": "{}".format(PROJECT_NAME),
        "description": "{}-chart-repo-description".format(RESOURCE_PREFIX).replace('_', '-'),
        "type": "Chart",
        "chartrepo_url": "{}://{}".format(CHARTREPO_TYPE, CHARTREPO_URL),
        "username": str(base64.b64encode("{}".format(CHARTREPO_USER).encode('utf-8')), 'utf8'),
        "password": str(base64.b64encode("{}".format(CHARTREPO_PASSWORD).encode('utf-8')), 'utf8'),
        "namespace": DEFAULT_NS,
        "default_label": DEFAULT_LABEL
    },
    "Chart_没有用户名和密码的模板仓库": {
        "ctr_name": "{}-e2etest-chart-repo2".format(RESOURCE_PREFIX).replace('_', '-'),
        "project_name": "{}".format(PROJECT_NAME),
        "type": "Chart",
        "chartrepo_url": "{}://{}:{}@{}".format(CHARTREPO_TYPE, CHARTREPO_USER, CHARTREPO_PASSWORD, CHARTREPO_URL),
        "namespace": DEFAULT_NS,
        "default_label": DEFAULT_LABEL
    },
    "Git_有用户名和密码的模板仓库": {
        "ctr_name": "{}-e2etest-git-1".format(RESOURCE_PREFIX).replace('_', '-'),
        "project_name": "{}".format(PROJECT_NAME),
        "description": "{}-chart-git-description".format(RESOURCE_PREFIX).replace('_', '-'),
        "type": "Git",
        "source": " ",
        "chart_url": "{}/{}/{}.git".format(GITLAB_URL, GITLAB_USERNAME, CATALOG_REPO),
        "path": "/",
        "chartrepo_url": '',
        "username": str(base64.b64encode("{}".format(GITLAB_USERNAME).encode('utf-8')), 'utf8'),
        "password": str(base64.b64encode("{}".format(GITLAB_PASSWORD).encode('utf-8')), 'utf8'),
        "namespace": DEFAULT_NS,
        "default_label": DEFAULT_LABEL
    },
    "SVN_有用户名和密码的模板仓库": {
        "ctr_name": "{}-e2etest-svn-1".format(RESOURCE_PREFIX).replace('_', '-'),
        "project_name": "{}".format(PROJECT_NAME),
        "description": "{}-chart-svn-description".format(RESOURCE_PREFIX).replace('_', '-'),
        "type": "SVN",
        "source": " ",
        "chart_url": SVNURL_HTTP,
        "path": "/",
        "chartrepo_url": '',
        "username": str(base64.b64encode("{}".format(SVNURL_HTTP_USER).encode('utf-8')), 'utf8'),
        "password": str(base64.b64encode("{}".format(SVNURL_HTTP_PASSWORD).encode('utf-8')), 'utf8'),
        "namespace": DEFAULT_NS,
        "default_label": DEFAULT_LABEL
    },
    "SVN_没有用户名和密码的模板仓库": {
        "ctr_name": "{}-e2etest-svn-2".format(RESOURCE_PREFIX).replace('_', '-'),
        "project_name": "{}".format(PROJECT_NAME),
        "type": "SVN",
        "source": " ",
        "chart_url": SVNURL_HTTPS,
        "path": "/",
        "chartrepo_url": '',
        "namespace": DEFAULT_NS,
        "default_label": DEFAULT_LABEL
    },
    "Git_没有用户名和密码的模板仓库": {
        "ctr_name": "{}-e2etest-git-2".format(RESOURCE_PREFIX).replace('_', '-'),
        "project_name": "{}".format(PROJECT_NAME),
        "type": "Git",
        "source": " ",
        "chart_url": GITURL_HTTPS,
        "path": "/",
        "chartrepo_url": '',
        "namespace": DEFAULT_NS,
        "default_label": DEFAULT_LABEL
    }
}
# 如果设置了https的变量就测试，否则不测
if not SVNURL_HTTPS:
    data_list.pop("SVN_没有用户名和密码的模板仓库")
if not GITURL_HTTPS:
    data_list.pop("Git_没有用户名和密码的模板仓库")
if not SVNURL_HTTP:
    data_list.pop("SVN_有用户名和密码的模板仓库")
if not CATALOG_REPO:
    data_list.pop("Git_有用户名和密码的模板仓库")
update_data = {
    "有用户名和密码的模板仓库": {
        "ctr_name": "{}-e2etest-chart-repo1".format(RESOURCE_PREFIX).replace('_', '-'),
        "project_name": "{}".format(PROJECT_NAME),
        "description": "测试修改描述信息{}".format(RESOURCE_PREFIX).replace('_', '-'),
        "chartrepo_url": "{}://{}".format(CHARTREPO_TYPE, CHARTREPO_URL),
        "username": str(base64.b64encode("{}".format(CHARTREPO_USER).encode('utf-8')), 'utf8'),
        "password": str(base64.b64encode("{}".format(CHARTREPO_PASSWORD).encode('utf-8')), 'utf8'),
        "namespace": DEFAULT_NS,
        "default_label": DEFAULT_LABEL
    },
    "没有用户名和密码的模板仓库": {
        "ctr_name": "{}-e2etest-chart-repo2".format(RESOURCE_PREFIX).replace('_', '-'),
        "project_name": "{}".format(PROJECT_NAME),
        "chartrepo_url": "{}://{}:{}@{}".format(CHARTREPO_TYPE, CHARTREPO_USER, CHARTREPO_PASSWORD, CHARTREPO_URL),
        "namespace": DEFAULT_NS,
        "default_label": DEFAULT_LABEL
    }
}

alb2_data = {
    "alb_name": "alb2-create-by-app-tempalte",
    "ctr_name": "{}-e2etest-chart-repo1".format(RESOURCE_PREFIX).replace('_', '-'),
    "namespace": "{}".format(K8S_NAMESPACE),
    "cluster_name": "{}".format(REGION_NAME),
    "registry": "{}".format(REGISTRY),
    "default_label": DEFAULT_LABEL
}

alb2_update_data = {
    "alb_name": "alb2-create-by-app-tempalte",
    "ctr_name": "{}-e2etest-chart-repo1".format(RESOURCE_PREFIX).replace('_', '-'),
    "namespace": "{}".format(K8S_NAMESPACE),
    "cluster_name": "{}".format(REGION_NAME),
    "registry": "{}".format(REGISTRY),
    "replicas": 2,
    "default_label": DEFAULT_LABEL
}
