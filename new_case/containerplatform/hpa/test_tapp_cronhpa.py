from . import HPA
from new_case.containerplatform.resourcemanage.resource import Resource
import pytest
from common.settings import RERUN_TIMES, IMAGE, RESOURCE_PREFIX, K8S_NAMESPACE, CASE_TYPE, REGION_NAME, \
    get_featuregate_status
import copy
import yaml
from new_case.containerplatform.tapp.tapp import TApp
from common.log import logger


@pytest.mark.archon
@pytest.mark.BAT
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
@pytest.mark.skipif(not get_featuregate_status(REGION_NAME, 'tapp'), reason="tapp功能开关未开启，默认不跑对应用例")
@pytest.mark.skipif(not get_featuregate_status(REGION_NAME, 'cron-hpa'), reason="cron-hpa功能开关未开启，默认不跑对应用例")
class TestTAppCronHPA(object):
    def setup_class(self):
        self.k8s_namespace = K8S_NAMESPACE
        self.resource_client = Resource()
        self.hpa_client = HPA()
        self.tapp_client = TApp()
        self.tapp_name = '{}-tapp-cronhpa'.format(RESOURCE_PREFIX)

        tapp_data = {
            "tapp_name": self.tapp_name,
            "namespace": self.k8s_namespace,
            "key": "tapp/name",
            "value": self.tapp_name,
            "image": IMAGE
        }
        self.tapp_client.delete_tapp_jinja2(self.k8s_namespace, self.tapp_name)
        ret = self.tapp_client.create_tapp_jinja2("test_data/tapp/tapp.jinja2", tapp_data)
        assert ret.status_code in (201, 409), "创建%stapp失败%s" % (self.tapp_name, ret.text)

        self.cronhpa_data = {
            "cronhpa_name": "tapp-" + self.tapp_name,
            "workload_name": self.tapp_name,
            "kind": "TApp",
            "ns_name": self.k8s_namespace
        }

    @pytest.mark.prepare
    def 测试创建tapp_CronHPA(self):
        data = copy.deepcopy(self.cronhpa_data)
        data["cron_rules"] = [
            {
                "schedule": "0 0 * * *",
                "targetReplicas": 3
            }
        ]
        hpa_data = {
            "hpa": None,
            "cronHpa": yaml.safe_load(self.hpa_client.cronhpa_tpl.render(data))
        }
        ret = self.hpa_client.add_hpa(hpa_data, self.tapp_name, "tapp")
        logger.info(ret.text)
        assert ret.status_code == 200, "创建cronhpa失败: %s" % ret.text
        data["ownerReferences"] = True
        v_data = {
            "hpa": None,
            "cronHpa": yaml.safe_load(self.hpa_client.cronhpa_tpl.render(data))
        }
        assert self.hpa_client.is_sub_dict(v_data, ret.json()), "创建cronhpa失败"

    @pytest.mark.upgrade
    def 测试更新tapp_CronHPA(self):
        data = copy.deepcopy(self.cronhpa_data)
        data["cron_rules"] = [
            {
                "schedule": "0 2 * * *",
                "targetReplicas": 4
            }
        ]
        hpa_data = {
            "hpa": None,
            "cronHpa": yaml.safe_load(self.hpa_client.cronhpa_tpl.render(data))
        }
        ret = self.hpa_client.update_hpa(hpa_data, self.tapp_name, "tapp")
        logger.info(ret.text)
        assert ret.status_code == 200, "更新cronhpa失败: %s" % ret.text
        data["ownerReferences"] = True
        v_data = {
            "hpa": None,
            "cronHpa": yaml.safe_load(self.hpa_client.cronhpa_tpl.render(data))
        }
        assert self.hpa_client.is_sub_dict(v_data, ret.json()), "更新cronhpa失败"

    @pytest.mark.upgrade
    def 测试获取tapp_CronHPA详情(self):
        ret = self.hpa_client.get_hpa_detail(self.tapp_name, "tapp")
        logger.info(ret.text)
        assert ret.status_code == 200, "获取cronhpa详情失败: %s" % ret.text
        data = copy.deepcopy(self.cronhpa_data)
        data["cron_rules"] = [
            {
                "schedule": "0 2 * * *",
                "targetReplicas": 4
            }
        ]
        data["ownerReferences"] = True
        v_data = {
            "hpa": None,
            "cronHpa": yaml.safe_load(self.hpa_client.cronhpa_tpl.render(data))
        }
        assert self.hpa_client.is_sub_dict(v_data, ret.json()), "获取cronhpa详情失败"

    @pytest.mark.delete
    def 测试删除tapp_CronHPA(self):
        ret = self.hpa_client.delete_hpa(self.tapp_name, "tapp")
        logger.info(ret.text)
        assert ret.status_code == 200, "删除cronhpa失败: %s" % ret.text
        v_data = {
            "cronHpa": None,
            "hpa": None
        }
        assert self.hpa_client.is_sub_dict(v_data, ret.json()), "删除cronhpa失败"

    def teardown_class(self):
        if CASE_TYPE not in ("prepare", "upgrade", "delete"):
            self.hpa_client.delete_hpa(self.tapp_name, "tapp")
            self.tapp_client.delete_tapp_jinja2(self.k8s_namespace, self.tapp_name)
