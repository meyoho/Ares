from common.base_request import Common
from common.log import logger
import sys


class HPA(Common):
    def __init__(self):
        self.hpa_tpl = self.generate_jinja_template("test_data/hpa/hpa.jinja2")
        self.cronhpa_tpl = self.generate_jinja_template("test_data/hpa/cronhpa.jinja2")
        self.hpav2_tpl = self.generate_jinja_template("test_data/hpa/hpav2.jinja2")
        super(HPA, self).__init__()
        self.genetate_global_info()

    def hpa_url(self, workload_name, workload_kind="deployment"):
        return 'acp/v1/kubernetes/{cluster}/hpa/{namespace}/{kind}/{name}'.format(
            cluster=self.region_name,
            namespace=self.k8s_namespace,
            kind=workload_kind,
            name=workload_name
        )

    def cronhpa_resource_mgt_url(self, limit=None):
        return 'kubernetes/{cluster}/apis/tkestack.io/v1/namespaces/{namespace}/cronhpas'.format(
                cluster=self.region_name,
                namespace=self.k8s_namespace) if not limit else \
            'kubernetes/{cluster}/apis/tkestack.io/v1/namespaces/{namespace}/cronhpas?limit={limit}'.format(
                cluster=self.region_name,
                namespace=self.k8s_namespace,
                limit=limit)

    def hpa_resource_mgt_url(self, limit=None):
        return 'kubernetes/{cluster}/apis/autoscaling/v1/namespaces/{namespace}/horizontalpodautoscalers'.format(
                cluster=self.region_name,
                namespace=self.k8s_namespace) if not limit else \
            'kubernetes/{cluster}/apis/autoscaling/v1/namespaces/{namespace}/horizontalpodautoscalers?limit={limit}'.format(
                cluster=self.region_name,
                namespace=self.k8s_namespace,
                limit=limit)

    def get_hpa_detail(self, workload_name, workload_kind="deployment"):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        return self.send('get', self.hpa_url(workload_name, workload_kind))

    def update_hpa(self, data, workload_name, workload_kind="deployment"):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        return self.send('put', self.hpa_url(workload_name, workload_kind), json=data)

    def delete_hpa(self, workload_name, workload_kind="deployment"):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        data = {
            "hpa": None,
            "cronHpa": None,
        }
        return self.send('put', self.hpa_url(workload_name, workload_kind), json=data)

    def add_hpa(self, data, workload_name, workload_kind="deployment"):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        return self.update_hpa(data, workload_name, workload_kind)

    def get_hpa_list(self, limit=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        return self.send("get", self.hpa_resource_mgt_url(limit))

    def get_cronhpa_list(self, limit=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        return self.send("get", self.cronhpa_resource_mgt_url(limit))
