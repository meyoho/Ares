from . import HPA
from new_case.containerplatform.deployment.deployment import Deployment
from new_case.containerplatform.resourcemanage.resource import Resource
import pytest
from common.settings import RERUN_TIMES, IMAGE, RESOURCE_PREFIX, K8S_NAMESPACE, CASE_TYPE
import copy
import yaml
from common.log import logger
from common import settings

data_list = [{
    "maxReplicas": 4,
    "metrics": [{"type": "memory", "value": '3'}]
}, {
    "minReplicas": 2,
    "metrics": [{"type": "pod_network_receive_bytes_per_second", "value": '2Ki'}]
}, {
    "metrics": [{"type": "pod_network_transmit_bytes_per_second", "value": '3Ki'}]
}, {
    "metrics": [{"type": "pod_fs_read_bytes_per_second", "value": '1Ki'}]
}, {
    "metrics": [{"type": "pod_fs_write_bytes_per_second", "value": '2Ki'}]
}, {
    "metrics": [
        {"type": "cpu", "value": '4'},
        {"type": "pod_network_receive_bytes_per_second", "value": '4Ki'},
        {"type": "pod_fs_write_bytes_per_second", "value": '4Ki'}]
}]
case_name = ["内存利用率", "网络入流量", "网络出流量", "存储读流量", "存储写流量", "cpu利用率多个指标"]


@pytest.mark.archon
@pytest.mark.BAT
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
@pytest.mark.skipif(not settings.get_featuregate_status(settings.REGION_NAME, 'hpav2'), reason="hpav2功能开关开启，默认不跑对应用例")
class TestDeploymentHPAV2(object):
    def setup_class(self):
        self.k8s_namespace = K8S_NAMESPACE
        self.resource_client = Resource()
        self.deploy_client = Deployment()
        self.hpa_client = HPA()
        self.deploy_name = '{}-hpav2-deploy'.format(RESOURCE_PREFIX)
        self.region_name = settings.REGION_NAME
        self.k8s_namespace = settings.K8S_NAMESPACE

        self.deploy_data = {
            "deployment_name": self.deploy_name,
            "namespace": self.k8s_namespace,
            "image": IMAGE
        }
        self.deploy_client.delete_deplyment(self.k8s_namespace, self.deploy_name, self.region_name)
        ret = self.deploy_client.create_deployment_jinja2("./test_data/deployment/deployment.jinja2", self.deploy_data,
                                                          self.region_name, self.k8s_namespace)
        assert ret.status_code in (201, 409), "创建deployment失败"

        self.hpa_data = {
            "workload_name": self.deploy_name,
            "ns_name": self.k8s_namespace,
            "metrics": [{"type": "cpu", "value": '2'}]
        }

    @pytest.mark.prepare
    def 测试创建deploy_HPAV2(self):
        data = {
            "hpa": yaml.safe_load(self.hpa_client.hpav2_tpl.render(self.hpa_data)),
            "cronHpa": None
        }
        ret = self.hpa_client.add_hpa(data, self.deploy_name)
        logger.info(ret.text)
        assert ret.status_code == 200, "创建hpa失败: %s" % ret.text
        hpa_data = copy.deepcopy(self.hpa_data)
        hpa_data["ownerReferences"] = True
        v_data = {
            "hpa": yaml.safe_load(self.hpa_client.hpav2_tpl.render(hpa_data)),
            "cronHpa": None
        }
        assert self.hpa_client.is_sub_dict(v_data, ret.json()), "创建hpa失败"

    @pytest.mark.upgrade
    def 测试获取deploy_HPAV2详情(self):
        hpa_data = copy.deepcopy(self.hpa_data)
        hpa_data["ownerReferences"] = True
        ret = self.hpa_client.get_hpa_detail(self.deploy_name)
        assert ret.status_code == 200, "获取hpa详情失败: %s" % ret.text
        v_data = {
            "hpa": yaml.safe_load(self.hpa_client.hpav2_tpl.render(hpa_data)),
            "cronHpa": None
        }
        assert self.hpa_client.is_sub_dict(v_data, ret.json()), "获取hpa详情失败"

    @pytest.mark.parametrize("hpa_data", data_list, ids=case_name)
    @pytest.mark.upgrade
    def 测试更新deploy_HPAV2(self, hpa_data):
        hpa_data["workload_name"] = self.deploy_name
        hpa_data["ns_name"] = self.k8s_namespace
        data = {
            "hpa": yaml.safe_load(self.hpa_client.hpav2_tpl.render(hpa_data)),
            "cronHpa": None
        }
        logger.info(data)
        ret = self.hpa_client.update_hpa(data, self.deploy_name)
        assert ret.status_code == 200, "更新hpa失败: %s" % ret.text
        hpa_data["ownerReferences"] = True
        v_data = {
            "hpa": yaml.safe_load(self.hpa_client.hpav2_tpl.render(hpa_data)),
            "cronHpa": None
        }
        assert self.hpa_client.is_sub_dict(v_data, ret.json()), "更新hpa失败"

    @pytest.mark.delete
    def 测试删除deploy_HPAV2(self):
        ret = self.hpa_client.delete_hpa(self.deploy_name)
        logger.info(ret.text)
        assert ret.status_code == 200, "删除hpa失败: %s" % ret.text
        v_data = {
            "cronHpa": None,
            "hpa": None
        }
        assert self.hpa_client.is_sub_dict(v_data, ret.json()), "删除hpa失败"

    def teardown_class(self):
        if CASE_TYPE not in ("prepare", "upgrade", "delete"):
            self.hpa_client.delete_hpa(self.deploy_name)
            self.deploy_client.delete_deplyment(self.k8s_namespace, self.deploy_name, self.region_name)
