from . import HPA
from new_case.containerplatform.deployment.deployment import Deployment
from new_case.containerplatform.resourcemanage.resource import Resource
import pytest
from common.settings import RERUN_TIMES, IMAGE, RESOURCE_PREFIX, K8S_NAMESPACE
import copy
import yaml
from common.log import logger
from common import settings


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
@pytest.mark.skipif(not settings.get_featuregate_status(settings.REGION_NAME, 'cron-hpa'),
                    reason="cron-hpa功能开关未开启，默认不跑对应用例")
@pytest.mark.skipif(settings.get_featuregate_status(settings.REGION_NAME, 'hpav2'), reason="hpav2功能开关开启，默认不跑对应用例")
class TestDeploymentHPA(object):
    def setup_class(self):
        self.k8s_namespace = K8S_NAMESPACE
        self.resource_client = Resource()
        self.deploy_client = Deployment()
        self.hpa_client = HPA()
        self.deploy_name = '{}-hpas-deploy'.format(RESOURCE_PREFIX)
        self.region_name = settings.REGION_NAME
        self.k8s_namespace = settings.K8S_NAMESPACE

        self.deploy_data = {
            "deployment_name": self.deploy_name,
            "namespace": self.k8s_namespace,
            "image": IMAGE
        }
        self.deploy_client.delete_deplyment(self.k8s_namespace, self.deploy_name, self.region_name)
        ret = self.deploy_client.create_deployment_jinja2("./test_data/deployment/deployment.jinja2", self.deploy_data,
                                                          self.region_name, self.k8s_namespace)
        assert ret.status_code in (201, 409), "创建deployment失败"

        self.hpa_data = {
            "workload_name": self.deploy_name,
            "ns_name": self.k8s_namespace
        }
        self.cronhpa_data = {
            "workload_name": self.deploy_name,
            "ns_name": self.k8s_namespace
        }

    def 测试创建deploy_HPA(self):
        data = {
            "hpa": yaml.safe_load(self.hpa_client.hpa_tpl.render(self.hpa_data)),
            "cronHpa": None
        }
        ret = self.hpa_client.add_hpa(data, self.deploy_name)
        logger.info(ret.text)
        assert ret.status_code == 200, "创建hpa失败: %s" % ret.text
        hpa_data = copy.deepcopy(self.hpa_data)
        hpa_data["ownerReferences"] = True
        v_data = {
            "hpa": yaml.safe_load(self.hpa_client.hpa_tpl.render(hpa_data)),
            "cronHpa": None
        }
        assert self.hpa_client.is_sub_dict(v_data, ret.json()), "创建hpa失败"

    def 测试更新deploy_HPA(self):
        hpa_data = copy.deepcopy(self.hpa_data)
        hpa_data["maxReplicas"] = 4
        hpa_data["cpu_use"] = 60
        data = {
            "hpa": yaml.safe_load(self.hpa_client.hpa_tpl.render(hpa_data)),
            "cronHpa": None
        }
        ret = self.hpa_client.update_hpa(data, self.deploy_name)
        logger.info(ret.text)
        assert ret.status_code == 200, "更新hpa失败: %s" % ret.text
        hpa_data["ownerReferences"] = True
        v_data = {
            "hpa": yaml.safe_load(self.hpa_client.hpa_tpl.render(hpa_data)),
            "cronHpa": None
        }
        assert self.hpa_client.is_sub_dict(v_data, ret.json()), "更新hpa失败"

    def 测试获取deploy_HPA详情(self):
        hpa_data = copy.deepcopy(self.hpa_data)
        hpa_data["maxReplicas"] = 4
        hpa_data["cpu_use"] = 60
        hpa_data["ownerReferences"] = True
        ret = self.hpa_client.get_hpa_detail(self.deploy_name)
        logger.info(ret.text)
        assert ret.status_code == 200, "获取hpa详情失败: %s" % ret.text
        v_data = {
            "hpa": yaml.safe_load(self.hpa_client.hpa_tpl.render(hpa_data)),
            "cronHpa": None
        }
        assert self.hpa_client.is_sub_dict(v_data, ret.json()), "获取hpa详情失败"

    def 测试删除deploy_HPA(self):
        ret = self.hpa_client.delete_hpa(self.deploy_name)
        logger.info(ret.text)
        assert ret.status_code == 200, "删除hpa失败: %s" % ret.text
        v_data = {
            "cronHpa": None,
            "hpa": None
        }
        assert self.hpa_client.is_sub_dict(v_data, ret.json()), "删除hpa失败"

    def 测试创建deploy_CronHPA(self):
        data = copy.deepcopy(self.cronhpa_data)
        data["cron_rules"] = [
            {
                "schedule": "0 0 * * *",
                "targetReplicas": 3
            }
        ]
        hpa_data = {
            "hpa": None,
            "cronHpa": yaml.safe_load(self.hpa_client.cronhpa_tpl.render(data))
        }
        ret = self.hpa_client.add_hpa(hpa_data, self.deploy_name)
        logger.info(ret.text)
        assert ret.status_code == 200, "创建cronhpa失败: %s" % ret.text
        data["ownerReferences"] = True
        v_data = {
            "hpa": None,
            "cronHpa": yaml.safe_load(self.hpa_client.cronhpa_tpl.render(data))
        }
        assert self.hpa_client.is_sub_dict(v_data, ret.json()), "创建cronhpa失败"

    def 测试更新deploy_CronHPA(self):
        data = copy.deepcopy(self.cronhpa_data)
        data["cron_rules"] = [
            {
                "schedule": "0 2 * * *",
                "targetReplicas": 4
            }
        ]
        hpa_data = {
            "hpa": None,
            "cronHpa": yaml.safe_load(self.hpa_client.cronhpa_tpl.render(data))
        }
        ret = self.hpa_client.update_hpa(hpa_data, self.deploy_name)
        logger.info(ret.text)
        assert ret.status_code == 200, "更新cronhpa失败: %s" % ret.text
        data["ownerReferences"] = True
        v_data = {
            "hpa": None,
            "cronHpa": yaml.safe_load(self.hpa_client.cronhpa_tpl.render(data))
        }
        assert self.hpa_client.is_sub_dict(v_data, ret.json()), "更新cronhpa失败"

    def 测试获取deploy_CronHPA详情(self):
        ret = self.hpa_client.get_hpa_detail(self.deploy_name)
        logger.info(ret.text)
        assert ret.status_code == 200, "获取cronhpa详情失败: %s" % ret.text
        data = copy.deepcopy(self.cronhpa_data)
        data["cron_rules"] = [
            {
                "schedule": "0 2 * * *",
                "targetReplicas": 4
            }
        ]
        data["ownerReferences"] = True
        v_data = {
            "hpa": None,
            "cronHpa": yaml.safe_load(self.hpa_client.cronhpa_tpl.render(data))
        }
        assert self.hpa_client.is_sub_dict(v_data, ret.json()), "获取cronhpa详情失败"

    def 测试删除deploy_CronHPA(self):
        ret = self.hpa_client.delete_hpa(self.deploy_name)
        logger.info(ret.text)
        assert ret.status_code == 200, "删除cronhpa失败: %s" % ret.text
        v_data = {
            "cronHpa": None,
            "hpa": None
        }
        assert self.hpa_client.is_sub_dict(v_data, ret.json()), "删除cronhpa失败"

    def 测试同时创建deploy_HPA和deploy_CronHPA(self):
        cron_hpa_data = copy.deepcopy(self.cronhpa_data)
        cron_hpa_data["cron_rules"] = [
            {
                "schedule": "0 0 * * *",
                "targetReplicas": 3
            }
        ]
        data = {
            "hpa": yaml.safe_load(self.hpa_client.hpa_tpl.render(self.hpa_data)),
            "cronHpa": yaml.safe_load(self.hpa_client.cronhpa_tpl.render(cron_hpa_data))
        }
        ret = self.hpa_client.add_hpa(data, self.deploy_name)
        logger.info(ret.text)
        assert ret.status_code == 200, "同时创建cronhpa和hpa失败: %s" % ret.text
        hpa_data = copy.deepcopy(self.hpa_data)
        hpa_data["ownerReferences"] = True
        v_data = {
            "hpa": yaml.safe_load(self.hpa_client.hpa_tpl.render(hpa_data)),
            "cronHpa": None
        }
        assert self.hpa_client.is_sub_dict(v_data, ret.json()), "测试同时创建cronhpa和hpa失败"

    def 测试创建HPA后更新创建deploy_CronHPA(self):
        data = {
            "hpa": yaml.safe_load(self.hpa_client.hpa_tpl.render(self.hpa_data)),
            "cronHpa": None
        }
        ret = self.hpa_client.add_hpa(data, self.deploy_name)
        logger.info(ret.text)
        assert ret.status_code == 200, "创建hpa失败: %s" % ret.text
        hpa_data = copy.deepcopy(self.hpa_data)
        hpa_data["ownerReferences"] = True
        v_data = {
            "hpa": yaml.safe_load(self.hpa_client.hpa_tpl.render(hpa_data)),
            "cronHpa": None
        }
        assert self.hpa_client.is_sub_dict(v_data, ret.json()), "测试创建hpa失败"

        # --- 更新的方式创建cronhpa同时删除hpa
        data = copy.deepcopy(self.cronhpa_data)
        data["cron_rules"] = [
            {
                "schedule": "0 0 * * *",
                "targetReplicas": 3
            }
        ]
        hpa_data = {
            "hpa": None,
            "cronHpa": yaml.safe_load(self.hpa_client.cronhpa_tpl.render(data))
        }
        ret = self.hpa_client.add_hpa(hpa_data, self.deploy_name)
        logger.info(ret.text)
        assert ret.status_code == 200, "创建cronhpa失败: %s" % ret.text
        data["ownerReferences"] = True
        v_data = {
            "hpa": None,
            "cronHpa": yaml.safe_load(self.hpa_client.cronhpa_tpl.render(data))
        }
        assert self.hpa_client.is_sub_dict(v_data, ret.json()), "创建cronhpa失败"

    def 测试创建deploy_CronHPA后更新创建deploy_HPA(self):
        data = copy.deepcopy(self.cronhpa_data)
        data["cron_rules"] = [
            {
                "schedule": "0 0 * * *",
                "targetReplicas": 3
            }
        ]
        hpa_data = {
            "hpa": None,
            "cronHpa": yaml.safe_load(self.hpa_client.cronhpa_tpl.render(data))
        }
        ret = self.hpa_client.add_hpa(hpa_data, self.deploy_name)
        logger.info(ret.text)
        assert ret.status_code == 200, "创建cronhpa失败: %s" % ret.text
        data["ownerReferences"] = True
        v_data = {
            "hpa": None,
            "cronHpa": yaml.safe_load(self.hpa_client.cronhpa_tpl.render(data))
        }
        assert self.hpa_client.is_sub_dict(v_data, ret.json()), "创建cronhpa失败"

        # --- 更新的方式创建hpa同时删除cronhpa
        data = {
            "hpa": yaml.safe_load(self.hpa_client.hpa_tpl.render(self.hpa_data)),
            "cronHpa": None
        }
        ret = self.hpa_client.add_hpa(data, self.deploy_name)
        logger.info(ret.text)
        assert ret.status_code == 200, "创建hpa失败: %s" % ret.text
        hpa_data = copy.deepcopy(self.hpa_data)
        hpa_data["ownerReferences"] = True
        v_data = {
            "hpa": yaml.safe_load(self.hpa_client.hpa_tpl.render(hpa_data)),
            "cronHpa": None
        }
        assert self.hpa_client.is_sub_dict(v_data, ret.json()), "测试创建hpa失败"

    def teardown_class(self):
        self.hpa_client.delete_hpa(self.deploy_name)
        self.deploy_client.delete_deplyment(self.k8s_namespace, self.deploy_name, self.region_name)
