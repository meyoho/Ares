from . import HPA
from new_case.containerplatform.resourcemanage.resource import Resource
import pytest
from common.settings import RERUN_TIMES, IMAGE, RESOURCE_PREFIX, K8S_NAMESPACE, REGION_NAME, get_featuregate_status
import copy
import yaml
from new_case.containerplatform.tapp.tapp import TApp
from common.log import logger


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
@pytest.mark.skipif(not get_featuregate_status(REGION_NAME, 'tapp'), reason="tapp功能开关未开启，默认不跑对应用例")
@pytest.mark.skipif(get_featuregate_status(REGION_NAME, 'hpav2'), reason="hpav2功能开关开启，默认不跑对应用例")
@pytest.mark.skipif(not get_featuregate_status(REGION_NAME, 'cron-hpa'), reason="cron-hpa功能开关未开启，默认不跑对应用例")
class TestTAppHPA(object):
    def setup_class(self):
        self.k8s_namespace = K8S_NAMESPACE
        self.resource_client = Resource()
        self.hpa_client = HPA()
        self.tapp_client = TApp()
        self.tapp_name = '{}-tapp-hpas'.format(RESOURCE_PREFIX)

        tapp_data = {
            "tapp_name": self.tapp_name,
            "namespace": self.k8s_namespace,
            "key": "tapp/name",
            "value": self.tapp_name,
            "image": IMAGE
        }
        self.tapp_client.delete_tapp_jinja2(self.k8s_namespace, self.tapp_name)
        ret = self.tapp_client.create_tapp_jinja2("test_data/tapp/tapp.jinja2", tapp_data)
        assert ret.status_code in (201, 409), "创建%stapp失败%s" % (self.tapp_name, ret.text)

        self.hpa_data = {
            "hpa_name": "tapp-" + self.tapp_name,
            "workload_name": self.tapp_name,
            "kind": "TApp",
            "ns_name": self.k8s_namespace
        }
        self.cronhpa_data = {
            "cronhpa_name": "tapp-" + self.tapp_name,
            "workload_name": self.tapp_name,
            "kind": "TApp",
            "ns_name": self.k8s_namespace
        }

    def 测试同时创建tapp_HPA和tapp_CronHPA(self):
        cron_hpa_data = copy.deepcopy(self.cronhpa_data)
        cron_hpa_data["cron_rules"] = [
            {
                "schedule": "0 0 * * *",
                "targetReplicas": 3
            }
        ]
        data = {
            "hpa": yaml.safe_load(self.hpa_client.hpa_tpl.render(self.hpa_data)),
            "cronHpa": yaml.safe_load(self.hpa_client.cronhpa_tpl.render(cron_hpa_data))
        }
        ret = self.hpa_client.add_hpa(data, self.tapp_name, "tapp")
        logger.info(ret.text)
        assert ret.status_code == 200, "同时创建cronhpa和hpa失败: %s" % ret.text
        hpa_data = copy.deepcopy(self.hpa_data)
        hpa_data["ownerReferences"] = True
        v_data = {
            "hpa": yaml.safe_load(self.hpa_client.hpa_tpl.render(hpa_data)),
            "cronHpa": None
        }
        assert self.hpa_client.is_sub_dict(v_data, ret.json()), "测试同时创建cronhpa和hpa失败"

    def 测试创建HPA后更新创建tapp_CronHPA(self):
        data = {
            "hpa": yaml.safe_load(self.hpa_client.hpa_tpl.render(self.hpa_data)),
            "cronHpa": None
        }
        ret = self.hpa_client.add_hpa(data, self.tapp_name, "tapp")
        logger.info(ret.text)
        assert ret.status_code == 200, "创建hpa失败: %s" % ret.text
        hpa_data = copy.deepcopy(self.hpa_data)
        hpa_data["ownerReferences"] = True
        v_data = {
            "hpa": yaml.safe_load(self.hpa_client.hpa_tpl.render(hpa_data)),
            "cronHpa": None
        }
        assert self.hpa_client.is_sub_dict(v_data, ret.json()), "测试创建hpa失败"

        # --- 更新的方式创建cronhpa同时删除hpa
        data = copy.deepcopy(self.cronhpa_data)
        data["cron_rules"] = [
            {
                "schedule": "0 0 * * *",
                "targetReplicas": 3
            }
        ]
        hpa_data = {
            "hpa": None,
            "cronHpa": yaml.safe_load(self.hpa_client.cronhpa_tpl.render(data))
        }
        ret = self.hpa_client.add_hpa(hpa_data, self.tapp_name, "tapp")
        logger.info(ret.text)
        assert ret.status_code == 200, "创建cronhpa失败: %s" % ret.text
        data["ownerReferences"] = True
        v_data = {
            "hpa": None,
            "cronHpa": yaml.safe_load(self.hpa_client.cronhpa_tpl.render(data))
        }
        assert self.hpa_client.is_sub_dict(v_data, ret.json()), "创建cronhpa失败"

    def 测试创建tapp_CronHPA后更新创建tapp_HPA(self):
        data = copy.deepcopy(self.cronhpa_data)
        data["cron_rules"] = [
            {
                "schedule": "0 0 * * *",
                "targetReplicas": 3
            }
        ]
        hpa_data = {
            "hpa": None,
            "cronHpa": yaml.safe_load(self.hpa_client.cronhpa_tpl.render(data))
        }
        ret = self.hpa_client.add_hpa(hpa_data, self.tapp_name, "tapp")
        logger.info(ret.text)
        assert ret.status_code == 200, "创建cronhpa失败: %s" % ret.text
        data["ownerReferences"] = True
        v_data = {
            "hpa": None,
            "cronHpa": yaml.safe_load(self.hpa_client.cronhpa_tpl.render(data))
        }
        assert self.hpa_client.is_sub_dict(v_data, ret.json()), "创建cronhpa失败"

        # --- 更新的方式创建hpa同时删除cronhpa
        data = {
            "hpa": yaml.safe_load(self.hpa_client.hpa_tpl.render(self.hpa_data)),
            "cronHpa": None
        }
        ret = self.hpa_client.add_hpa(data, self.tapp_name, "tapp")
        logger.info(ret.text)
        assert ret.status_code == 200, "创建hpa失败: %s" % ret.text
        hpa_data = copy.deepcopy(self.hpa_data)
        hpa_data["ownerReferences"] = True
        v_data = {
            "hpa": yaml.safe_load(self.hpa_client.hpa_tpl.render(hpa_data)),
            "cronHpa": None
        }
        assert self.hpa_client.is_sub_dict(v_data, ret.json()), "测试创建hpa失败"

    def teardown_class(self):
        self.hpa_client.delete_hpa(self.tapp_name, "tapp")
        self.tapp_client.delete_tapp_jinja2(self.k8s_namespace, self.tapp_name)
