import json
import sys
import time
import yaml
from common.base_request import Common
from common.log import logger
from new_case.operation.events.events import Events
from common.settings import DEFAULT_LABEL


class Cronjobs(Common):
    def __init__(self):
        self.body_template = self.generate_jinja_template("test_data/jobs/cronjob.jinja2")
        super(Cronjobs, self).__init__()
        self.events = Events()
        self.genetate_global_info()

    def job_history_list_url(self, selector="", limit=20):
        return "kubernetes/{region}/apis/batch/v1/namespaces/{namespace}/jobs?" \
               "limit={limit}&project={project}&fieldSelector={selector}" \
            .format(region=self.region_name, namespace=self.k8s_namespace,
                    limit=limit, project=self.project_name, selector=selector)

    def job_history_detail_url(self, job_name):
        return "kubernetes/{region}/apis/batch/v1/namespaces/{namespace}/jobs/{job_name}" \
            .format(region=self.region_name, namespace=self.k8s_namespace, job_name=job_name)

    def job_history_pods_list_url(self, job_uid):
        return "kubernetes/{region}/api/v1/namespaces/{namespace}/pods?labelSelector=controller-uid={job_uid}" \
            .format(region=self.region_name, namespace=self.k8s_namespace, job_uid=job_uid)

    def job_history_pod_url(self, podname):
        return "kubernetes/{region}/api/v1/namespaces/{namespace}/pods/{podname}" \
            .format(region=self.region_name, namespace=self.k8s_namespace, podname=podname)

    def job_history_logs_url(self, podname, container_name):
        return "acp/v1/kubernetes/{region}/namespaces/{namespace}/pods/{podname}/{container_name}/log?" \
               "previous=false&logFilePosition=end" \
               "&referenceTimestamp=newest&offsetFrom=2000000000&offsetTo=2000000100" \
            .format(region=self.region_name, namespace=self.k8s_namespace, podname=podname,
                    container_name=container_name)

    def job_history_kenvnts_url(self):
        return self.events.get_kevents_url()

    def cronjob_list_url(self, selector="", limit=20):
        return "kubernetes/{region}/apis/batch/v1beta1/namespaces/{namespace}/cronjobs?limit={limit}&project={project}" \
               "&fieldSelector={selector}" \
            .format(region=self.region_name, namespace=self.k8s_namespace, limit=limit,
                    project=self.project_name, selector=selector)

    def cronjob_detail_url(self, cronjob_name):
        return "kubernetes/{region}/apis/batch/v1beta1/namespaces/{namespace}/cronjobs/{cronjob_name}" \
            .format(region=self.region_name, namespace=self.k8s_namespace,
                    cronjob_name=cronjob_name)

    def cronjob_detail_jobs_url(self, cronjob_name, selector="", limit=20):
        return "kubernetes/{region}/apis/batch/v1/namespaces/{namespace}/jobs?" \
               "limit={limit}&project={project}&labelSelector=cronjob.{default_lable}/name={cronjob_name}&" \
               "fieldSelector={selector}" \
            .format(region=self.region_name, namespace=self.k8s_namespace, limit=limit, project=self.project_name,
                    cronjob_name=cronjob_name, selector=selector, default_lable=DEFAULT_LABEL)

    def cronjob_kevents_url(self):
        return self.events.get_kevents_url()

    def create_cronjob_url(self):
        return "kubernetes/{region}/apis/batch/v1beta1/namespaces/{namespace}/cronjobs".format(
            region=self.region_name,
            namespace=self.k8s_namespace
        )

    def execute_cronjob_manual_url(self, cronjob_name):
        return "acp/v1/kubernetes/{region}/cronjobs/{namespace}/{cronjob_name}/exec" \
            .format(region=self.region_name, cronjob_name=cronjob_name, namespace=self.k8s_namespace)

    def generate_dict_post_data(self, **kwargs):
        return json.dumps(yaml.safe_load(self.body_template.render(**kwargs)))

    def create_cronjob(self, auth=None, **kwargs):
        '''post 201'''
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        return self.send("post", path=self.create_cronjob_url(), data=self.generate_dict_post_data(**kwargs), auth=auth)

    def get_cronjob_detail(self, cronjob_name, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        return self.send("get", path=self.cronjob_detail_url(cronjob_name), auth=auth)

    def get_cronjob_job_historys(self, cronjob_name, selector="", limit=20):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        return self.send("get", path=self.cronjob_detail_jobs_url(cronjob_name, selector, limit))

    def get_cronjob_list(self, selector="", limit=20):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        return self.send("get", path=self.cronjob_list_url(selector, limit))

    def search_cronjob(self, selector="", limit=20):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        return self.send("get", self.cronjob_list_url(selector, limit))

    def update_cronjob(self, cronjobname, auth=None, **kwargs):
        '''put 204'''
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        return self.send("put", path=self.cronjob_detail_url(cronjobname), data=self.generate_dict_post_data(**kwargs),
                         auth=auth)

    def delete_cronjob(self, cronjob_name, auth=None):
        '''delete 204'''
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        return self.send("delete", path=self.cronjob_detail_url(cronjob_name), params={"propagationPolicy": "Background"}, auth=auth)

    def start_cronjob(self, cronjob_name):
        '''post 200'''
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        return self.send("post", self.execute_cronjob_manual_url(cronjob_name), json={})

    def get_cronjob_k8s_events(self, cronjob_name, page=1, page_size=20):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        payload = {
            "start_time": int(time.time()) - 1800,
            "end_time": int(time.time()),
            "cluster": self.region_name,
            "name": cronjob_name,
            "fuzzy": 'true',
            "kind": "CronJob",
            "namespace": self.k8s_namespace,
            "page": page,
            "page_size": page_size

        }
        return self.send("get", path=self.cronjob_kevents_url(), params=payload)

    def delete_jobhistory(self, job_name, auth=None):
        '''delete 204'''
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        return self.send("delete", path=self.job_history_detail_url(job_name), params={"propagationPolicy": "Background"}, auth=auth)

    def delete_jobhistory_pod(self, podname):
        '''delete 204'''
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        return self.send("delete", path=self.job_history_pod_url(podname))

    def get_jobhistory_detail(self, job_name, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        return self.send("get", path=self.job_history_detail_url(job_name), auth=auth)

    def get_jobhistory_list(self, selector="", limit=20):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        return self.send("get", path=self.job_history_list_url(selector, limit))

    def search_jobhistory(self, selector="", limit=20):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        return self.send("get", path=self.job_history_list_url(selector, limit))

    def get_jobhistoty_pods_list(self, job_uid):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        return self.send("get", path=self.job_history_pods_list_url(job_uid))

    def get_jobhistory_logs(self, podname, container_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        return self.send("get", path=self.job_history_logs_url(podname, container_name))

    def get_jobhistory_k8s_events(self, job_name, page=1, page_size=20):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        payload = {
            "start_time": int(time.time()) - 1800,
            "end_time": int(time.time()),
            "cluster": self.region_name,
            "name": job_name,
            "fuzzy": 'false',
            "kind": "Job",
            "namespace": self.k8s_namespace,
            "page": page,
            "page_size": page_size

        }
        return self.send("get", path=self.job_history_kenvnts_url(), params=payload)

    def wait_pod_scheduled(self, job_uid, timeout=120):
        end_time = time.time() + timeout
        while time.time() < end_time:
            ret = self.get_jobhistoty_pods_list(job_uid)
            if ret.status_code == 200 and len(self.get_value(ret.json(), "items")) > 0:
                return True
            else:
                time.sleep(1)
