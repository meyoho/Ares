import pytest
import copy
from common.log import logger
from . import Cronjobs
from common.settings import RERUN_TIMES, DEFAULT_LABEL


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestCronjobsCase(object):
    data_list = {
        "名称错误": [{
            "cronjob_name": "cron_job.test",
        }, {
            "kind": "Status",
            "apiVersion": "v1",
            "metadata": {},
            "status": "Failure",
            "message": "CronJob.batch \"cron_job.test\" is invalid: [metadata.name: Invalid value: \"cron_job.\
test\": a DNS-1123 subdomain must consist of lower case alphanumeric characters, '-' or '.', and must start and end \
with an alphanumeric character (e.g. 'example.com', regex used for validation is '[a-z0-9]([-a-z0-9]*[a-z0-9])?\
(\\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*'), spec.jobTemplate.spec.template.spec.containers[0].name: Invalid \
value: \"cron_job.test\": a DNS-1123 label must consist of lower case alphanumeric characters or '-', and must start \
and end with an alphanumeric character (e.g. 'my-name',  or '123-abc', regex used for validation is \
'[a-z0-9]([-a-z0-9]*[a-z0-9])?')]",
            "reason": "Invalid",
            "details": {
                "name": "cron_job.test",
                "group": "batch",
                "kind": "CronJob",
                "causes": [{
                    "reason": "FieldValueInvalid",
                    "message": "Invalid value: \"cron_job.test\": a DNS-1123 subdomain must \
consist of lower case alphanumeric characters, '-' or '.', and must start and end with an alphanumeric character \
(e.g. 'example.com', regex used for validation is '[a-z0-9]([-a-z0-9]*[a-z0-9])?(\\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*')",
                    "field": "metadata.name"
                }, {
                    "reason": "FieldValueInvalid",
                    "message": "Invalid value: \"cron_job.test\": a DNS-1123 label must consist \
of lower case alphanumeric characters or '-', and must start and end with an alphanumeric character (e.g. 'my-name',  \
or '123-abc', regex used for validation is '[a-z0-9]([-a-z0-9]*[a-z0-9])?')",
                    "field": "spec.jobTemplate.spec.template.spec.containers[0].name"
                }]
            },
            "code": 422
        }],
        "名称超长": [{
            "cronjob_name": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
        }, {
            "kind": "Status",
            "apiVersion": "v1",
            "metadata": {},
            "status": "Failure",
            "message": "CronJob.batch \"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\" is invalid: \
metadata.name: Invalid value: \"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\": must be no more than 52 characters",
            "reason": "Invalid",
            "details": {
                "name": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
                "group": "batch",
                "kind": "CronJob",
                "causes": [{
                    "reason": "FieldValueInvalid",
                    "message": "Invalid value: \"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\": must be no more than 52 characters",
                    "field": "metadata.name"
                }]
            },
            "code": 422
        }],
        "名称为空": [{
            "cronjob_name": "''",
        }, {
            "kind": "Status",
            "apiVersion": "v1",
            "metadata": {},
            "status": "Failure",
            "message": "CronJob.batch \"\" is invalid: [metadata.name: Required value: name or generateName \
is required, spec.jobTemplate.spec.template.spec.containers[0].name: Required value]",
            "reason": "Invalid",
            "details": {
                "group": "batch",
                "kind": "CronJob",
                "causes": [{
                    "reason": "FieldValueRequired",
                    "message": "Required value: name or generateName is required",
                    "field": "metadata.name"
                }, {
                    "reason": "FieldValueRequired",
                    "message": "Required value",
                    "field": "spec.jobTemplate.spec.template.spec.containers[0].name"
                }]
            },
            "code": 422
        }],
        "触发规则非法": [{
            "cronjob_name": "cronjob-test",
            "schedule": "* *"
        }, {
            "kind": "Status",
            "apiVersion": "v1",
            "metadata": {},
            "status": "Failure",
            "message": "CronJob.batch \"cronjob-test\" is invalid: spec.schedule: Invalid value: \"* *\": Expected exactly 5 fields, found 2: * *",
            "reason": "Invalid",
            "details": {
                "name": "cronjob-test",
                "group": "batch",
                "kind": "CronJob",
                "causes": [{
                    "reason": "FieldValueInvalid",
                    "message": "Invalid value: \"* *\": Expected exactly 5 fields, found 2: * *",
                    "field": "spec.schedule"
                }]
            },
            "code": 422
        }],
        "调度内容为空": [{
            "cronjob_name": "cronjob-test",
            "schedule": "''"
        }, {
            "kind": "Status",
            "apiVersion": "v1",
            "metadata": {},
            "status": "Failure",
            "message": "CronJob.batch \"cronjob-test\" is invalid: spec.schedule: Invalid value: \"'\": Expected exactly 5 fields, found 1: '",
            "reason": "Invalid",
            "details": {
                "name": "cronjob-test",
                "group": "batch",
                "kind": "CronJob",
                "causes": [{
                    "reason": "FieldValueInvalid",
                    "message": "Invalid value: \"'\": Expected exactly 5 fields, found 1: '",
                    "field": "spec.schedule"
                }]
            },
            "code": 422
        }],
        "成功任务历史数量为字母": [{
            "cronjob_name": "cronjob-test",
            "successfulJobsHistoryLimit": "a"
        }, {
            "kind": "Status",
            "apiVersion": "v1",
            "metadata": {},
            "status": "Failure",
            "message": "CronJob in version \"v1beta1\" cannot be handled as a CronJob: v1beta1.CronJob.Spec: v1beta1.CronJobSpec.SuccessfulJobsHistoryLimit: \
readUint32: unexpected character: \ufffd, error found in #10 byte of ...|yLimit\": \"a\", \"concu|..., bigger context ...|sHistoryLimit\": 3, \
\"successfulJobsHistoryLimit\": \"a\", \"concurrencyPolicy\": \"Allow\", \"suspend\": true,|...",
            "reason": "BadRequest",
            "code": 400
        }],
        "失败任务历史数量为字母": [{
            "cronjob_name": "cronjob-test",
            "failedJobsHistoryLimit": "c"
        }, {
            "kind": "Status",
            "apiVersion": "v1",
            "metadata": {},
            "status": "Failure",
            "message": "CronJob in version \"v1beta1\" cannot be handled as a CronJob: v1beta1.CronJob.Spec: v1beta1.CronJobSpec.FailedJobsHistoryLimit: \
readUint32: unexpected character: \ufffd, error found in #10 byte of ...|yLimit\": \"c\", \"succe|..., bigger context ...|onjob-test\"}, \"spec\": \
{\"failedJobsHistoryLimit\": \"c\", \"successfulJobsHistoryLimit\": 3, \"concurrencyP|...",
            "reason": "BadRequest",
            "code": 400
        }],
        "成功任务历史数量为空": [{
            "cronjob_name": "cronjob-test",
            "successfulJobsHistoryLimit": "''"
        }, {
            "kind": "Status",
            "apiVersion": "v1",
            "metadata": {},
            "status": "Failure",
            "message": "CronJob in version \"v1beta1\" cannot be handled as a CronJob: v1beta1.CronJob.Spec: v1beta1.CronJobSpec.SuccessfulJobsHistoryLimit: \
readUint32: unexpected character: \ufffd, error found in #10 byte of ...|yLimit\": \"\", \"concur|..., bigger context ...|sHistoryLimit\": 3, \
\"successfulJobsHistoryLimit\": \"\", \"concurrencyPolicy\": \"Allow\", \"suspend\": true, |...",
            "reason": "BadRequest",
            "code": 400
        }],
        "重试次数为字母": [{
            "cronjob_name": "cronjob-test",
            "backoffLimit": "a"
        }, {
            "kind": "Status",
            "apiVersion": "v1",
            "metadata": {},
            "status": "Failure",
            "message": "CronJob in version \"v1beta1\" cannot be handled as a CronJob: v1beta1.CronJob.Spec: v1beta1.CronJobSpec.JobTemplate: \
v1beta1.JobTemplateSpec.Spec: v1.JobSpec.BackoffLimit: readUint32: unexpected character: \ufffd, error found in #10 byte of ...|fLimit\": \"a\", \"paral|..., \
bigger context ...|: {\"activeDeadlineSeconds\": 600, \"backoffLimit\": \"a\", \"parallelism\": 1, \"completions\": 1, \"template\"|...",
            "reason": "BadRequest",
            "code": 400
        }],
        "并发次数为字母": [{
            "cronjob_name": "cronjob-test",
            "parallelism": "a",
        }, {
            "kind": "Status",
            "apiVersion": "v1",
            "metadata": {},
            "status": "Failure",
            "message": "CronJob in version \"v1beta1\" cannot be handled as a CronJob: v1beta1.CronJob.Spec: v1beta1.CronJobSpec.JobTemplate: \
v1beta1.JobTemplateSpec.Spec: v1.JobSpec.Parallelism: readUint32: unexpected character: \ufffd, error found in #10 byte of ...|lelism\": \"a\", \
\"compl|..., bigger context ...|Seconds\": 600, \"backoffLimit\": 6, \"parallelism\": \"a\", \"completions\": 1, \"template\": {\"metadata\": {\"l|...",
            "reason": "BadRequest",
            "code": 400
        }],
        "成功次数为字母": [{
            "cronjob_name": "cronjob-test",
            "completions": "a"
        }, {
            "kind": "Status",
            "apiVersion": "v1",
            "metadata": {},
            "status": "Failure",
            "message": "CronJob in version \"v1beta1\" cannot be handled as a CronJob: v1beta1.CronJob.Spec: v1beta1.CronJobSpec.JobTemplate: \
v1beta1.JobTemplateSpec.Spec: v1.JobSpec.Completions: readUint32: unexpected character: \ufffd, error found in #10 byte of ...|etions\": \"a\", \
\"templ|..., bigger context ...|ckoffLimit\": 6, \"parallelism\": 1, \"completions\": \"a\", \"template\": {\"metadata\": {\"labels\": {\"project.|...",
            "reason": "BadRequest",
            "code": 400
        }],
        "超时时间为字母": [{
            "cronjob_name": "cronjob-test",
            "activeDeadlineSeconds": "a"
        }, {
            "kind": "Status",
            "apiVersion": "v1",
            "metadata": {},
            "status": "Failure",
            "message": "CronJob in version \"v1beta1\" cannot be handled as a CronJob: v1beta1.CronJob.Spec: v1beta1.CronJobSpec.JobTemplate: \
v1beta1.JobTemplateSpec.Spec: v1.JobSpec.ActiveDeadlineSeconds: readUint64: unexpected character: \ufffd, error found in #10 byte of ...|econds\": \
\"a\", \"backo|..., bigger context ...|stamp\": null}, \"spec\": {\"activeDeadlineSeconds\": \"a\", \"backoffLimit\": 6, \"parallelism\": 1, \"completi|...",
            "reason": "BadRequest",
            "code": 400
        }],
        "重启规则为Always": [{
            "cronjob_name": "cronjob-test",
            "restartPolicy": "Always"
        }, {
            "kind": "Status",
            "apiVersion": "v1",
            "metadata": {},
            "status": "Failure",
            "message": "CronJob.batch \"cronjob-test\" is invalid: spec.jobTemplate.spec.template.spec.restartPolicy: Unsupported value: \"Always\": \
supported values: \"OnFailure\", \"Never\"",
            "reason": "Invalid",
            "details": {
                "name": "cronjob-test",
                "group": "batch",
                "kind": "CronJob",
                "causes": [{
                    "reason": "FieldValueNotSupported",
                    "message": "Unsupported value: \"Always\": supported values: \"OnFailure\", \"Never\"",
                    "field": "spec.jobTemplate.spec.template.spec.restartPolicy"
                }]
            },
            "code": 422
        }]
    }

    def setup_class(self):
        self.cronjobs = Cronjobs()
        self.create_cronjob_base_kwargs = {
            "namespace": self.cronjobs.k8s_namespace,
            "image": self.cronjobs.global_info["$IMAGE"],
            "project": self.cronjobs.project_name
        }

    @pytest.mark.parametrize("key", data_list)
    def 测试创建定时任务反向case(self, key):
        data = self.data_list[key]
        kwargs = self.create_cronjob_base_kwargs.copy()
        kwargs.update(copy.deepcopy(data[0]))
        v_data = copy.deepcopy(data[1])

        logger.info("create cronjob: %s" % data[0]["cronjob_name"])
        if self.cronjobs.get_cronjob_detail(data[0]["cronjob_name"]).status_code != 404:
            logger.info("the cronjob '%s' is already exists,delete it." % data[0]["cronjob_name"])
            ret = self.cronjobs.delete_cronjob(data[0]["cronjob_name"])
            assert ret.status_code == 200, "delete cronjob '%s' fail. reason: %s %s" % (data[0]["cronjob_name"], ret.status_code, ret.text)
        kwargs.update({"default_label": DEFAULT_LABEL})
        ret = self.cronjobs.create_cronjob(**kwargs)
        assert ret.status_code == v_data["code"] and self.cronjobs.is_sub_dict(v_data, ret.json()), \
            "创建定时任务%s: %s" % (data[0]["cronjob_name"], str(kwargs))
