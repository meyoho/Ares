import pytest
from common.settings import RERUN_TIMES
from new_case.containerplatform.resourcemanage.resource import Resource
from new_case.containerplatform.jobs.job import Job
from common import settings


@pytest.mark.metis
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestJobPostSuite(object):

    def setup_class(self):
        self.job = Job()
        self.resource_client = Resource()
        self.region_name = settings.REGION_NAME

    def 测试_创建job(self):
        data = self.job.data_list
        create_result = self.job.create_job_jinja2("./test_data/jobs/job.jinja2", data=data, region=self.region_name)
        assert create_result.status_code == 204, "创建job{}失败,{}".format(data['job_name'], create_result.text)

    def 测试_搜索job(self):
        data = self.job.data_list
        ser_result = self.job.search_job(region=self.region_name, job_name=data['job_name'])
        assert ser_result.status_code == 200, "没有搜索到{}，{}".format(data["job_name"], ser_result.text)

        global controller_uid
        controller_uid = self.job.get_value(ser_result.json(), 'items.0.metadata.uid')

        value = self.job.generate_jinja_data("./verify_data/jobs/search_job.jinja2", data)
        assert self.job.is_sub_dict(value, ser_result.json()), "搜索job比对数据失败，返回数据:{},期望数据:{}".\
            format(ser_result.json(), value)

    def 测试_更新job(self):
        data = self.job.data_list
        data.update({"controller_uid": controller_uid})
        data.update({"key": "key_a"})
        data.update({"value": "value_a"})
        update_result = self.job.update_job_jinja2(self.region_name, data['namespace'], data['job_name'],
                                                   "./test_data/jobs/update_job.jinja2", data=data)
        assert update_result.status_code == 200, "更新job:{}失败{}".format(data['job_name'], update_result.text)

        value = self.job.generate_jinja_data("./verify_data/jobs/update_job.jinja2", data)
        assert self.job.is_sub_dict(value, update_result.json()), "更新job比对数据失败，返回数据:{},期望数据:{}".\
            format(update_result.json(), value)

    def 测试_删除job(self):
        data = self.job.data_list
        delete_result = self.job.delete_job_jinja2(region=self.region_name, ns_name=data['namespace'],
                                                   job_name=data['job_name'])
        assert delete_result.status_code == 200, "删除job{}失败，{}".format(data['job_name'], delete_result)
