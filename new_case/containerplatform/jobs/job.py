import sys
from common.base_request import Common
from common.log import logger
from common import settings


class Job(Common):
    def __init__(self):
        super(Job, self).__init__()
        self.genetate_global_info()

    def create_job_url(self, region):
        return "acp/v1/resources/{}/resources".format(region)

    def common_job_url(self, region, ns_name, job_name):
        return "kubernetes/{}/apis/batch/v1/namespaces/{}/jobs/{}".format(region, ns_name, job_name)

    def search_job_url(self, region, limit=1, job_name=''):
        return "acp/v1/resources/search/kubernetes/{}/apis/batch/v1/jobs?limit={}&keyword={}&field=metadata.name".\
            format(region, limit, job_name)

    def create_job_jinja2(self, file, data, region):
        path = self.create_job_url(region)
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=path, json=data, params={})

    def delete_job_jinja2(self, region, ns_name, job_name, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.common_job_url(region=region, ns_name=ns_name, job_name=job_name)
        return self.send(method='delete', path=url, auth=auth)

    def search_job(self, region, job_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.search_job_url(region=region, job_name=job_name)
        return self.send(method='get', path=url)

    def update_job_jinja2(self, region, ns_name, job_name, file, data):
        path = self.common_job_url(region=region, ns_name=ns_name, job_name=job_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='put', path=path, json=data, params={})

    data_list = {
        "job_name": '{}-job'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME,
        "image": settings.IMAGE
    }
