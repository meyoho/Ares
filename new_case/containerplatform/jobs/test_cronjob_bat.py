import random
import pytest
import yaml
from common import settings
from common.log import logger
from new_case.operation.events.events import Events
from . import Cronjobs
from common.settings import RERUN_TIMES, CASE_TYPE, RESOURCE_PREFIX
from configparser import ConfigParser


@pytest.mark.archon
@pytest.mark.BAT
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestCronjobsCase(object):
    def setup_class(self):
        self.cronjobs = Cronjobs()
        self.events = Events()
        self.cronjob_name = "{}-ares-cronjob-test".format(settings.RESOURCE_PREFIX)
        self.create_cronjob_base_kwargs = {
            "namespace": self.cronjobs.k8s_namespace,
            "image": self.cronjobs.global_info["$IMAGE"],
            "project": self.cronjobs.project_name,
            "default_label": settings.DEFAULT_LABEL,
            "username": settings.USERNAME
        }
        self.cronjob_exe_methods = {
            "manual": {},
            "auto": {
                # "schedule": "0-59 * * * *",
                "suspend": "false"
            }
        }
        self.cronjob_policys = {
            "allow": {
                "concurrencyPolicy": "Allow"
            },
            "forbid": {
                "concurrencyPolicy": "Forbid"
            },
            "replace": {
                "concurrencyPolicy": "Replace"
            }
        }
        self.cronjob_types = {
            "single": {},
            "parallel": {
                "parallelism": 2
            },
            "fixed": {
                "parallelism": 2,
                "completions": 2
            }
        }
        self.job_template = self.cronjobs.generate_jinja_template("verify_data/jobs/jobs.jinja2")
        self.case_config = ConfigParser()
        self.case_config.add_section("case_config")
        self.case_config_file_name = "cronjob_case_tmp.conf"

    @pytest.mark.prepare
    def 测试创建定时任务L0(self):
        # 创建所有类型的定时任务
        logger.info("create cronjobs")

        exec_method = random.choice(list(self.cronjob_exe_methods.keys()))
        cronjob_policy = random.choice(list(self.cronjob_policys.keys()))
        cronjob_type = random.choice(list(self.cronjob_types.keys()))

        cronjob_name = "-".join([exec_method, cronjob_policy, cronjob_type, RESOURCE_PREFIX])
        logger.info("create cronjob: %s" % (cronjob_name))
        kwargs = self.create_cronjob_base_kwargs.copy()
        kwargs.update(self.cronjob_exe_methods[exec_method])
        kwargs.update(self.cronjob_policys[cronjob_policy])
        kwargs.update(self.cronjob_types[cronjob_type])
        kwargs["cronjob_name"] = cronjob_name
        # 检查定时任务是否存在
        logger.info(
            "check if the cronjob '%s' is exists." % cronjob_name)
        if self.cronjobs.get_cronjob_detail(cronjob_name).status_code != 404:
            logger.info("the cronjob '%s' is already exists,delete it." % cronjob_name)
            ret = self.cronjobs.delete_cronjob(cronjob_name)
            assert ret.status_code == 200, "delete cronjob '%s' fail. reason: %s %s" % (cronjob_name, ret.status_code, ret.text)
        kwargs.update({"default_label": settings.DEFAULT_LABEL})
        ret = self.cronjobs.create_cronjob(**kwargs)
        assert ret.status_code == 201, "创建定时任务失败:{}".format(ret.text)
        assert self.cronjobs.is_sub_dict(yaml.safe_load(self.cronjobs.body_template.render(**kwargs)), ret.json()), "创建定时任务比对数据失败"

        self.cronjob_name = "-".join([exec_method, cronjob_policy, cronjob_type, RESOURCE_PREFIX])
        self.case_config.set("case_config", "cronjob_name", self.cronjob_name)
        self.case_config.write(open(self.case_config_file_name, "w"))

    @pytest.mark.upgrade
    def 测试获取定时任务(self):
        ret = self.cronjobs.get_cronjob_list()
        assert ret.status_code == 200, '获取定时任务失败'
        assert len(ret.json()['items']) > 0, '定时任务不存在'
        self.cronjob_name = ret.json()['items'][0]['metadata']['name']
        self.case_config.set("case_config", "cronjob_name", self.cronjob_name)
        self.case_config.write(open(self.case_config_file_name, "w"))

    @pytest.mark.upgrade
    def 测试更新定时任务L0(self):
        # 检查定时任务是否存在
        self.case_config.read(self.case_config_file_name)
        self.cronjob_name = self.case_config.get("case_config", "cronjob_name")
        logger.info("check if the cronjob '%s' is exists." % self.cronjob_name)
        assert self.cronjobs.get_cronjob_detail(self.cronjob_name).status_code == 200, \
            "定时任务: %s不存在" % self.cronjob_name
        data = self.create_cronjob_base_kwargs.copy()
        data.update(self.cronjob_exe_methods[self.cronjob_name.split('-')[0]])
        data.update(self.cronjob_policys[self.cronjob_name.split('-')[1]])
        data.update(self.cronjob_types[self.cronjob_name.split('-')[2]])
        data["succes_percent"] = 20
        data["sleep_second"] = 80
        data["successfulJobsHistoryLimit"] = 20
        data["failedJobsHistoryLimit"] = 20
        data["cronjob_name"] = self.cronjob_name
        data.update({"default_label": settings.DEFAULT_LABEL})
        ret = self.cronjobs.update_cronjob(self.cronjob_name, **data)
        assert ret.status_code == 200, "更新定时任务'%s'失败. 原因: %s %s " % (
            self.cronjob_name, ret.status_code, ret.text)
        assert self.cronjobs.is_sub_dict(yaml.safe_load(self.cronjobs.body_template.render(**data)), ret.json()), \
            "更新定时任务'%s'失败，数据没有更新" % self.cronjob_name

    @pytest.mark.upgrade
    def 测试执行定时任务L0(self):
        # 检查定时任务是否存在
        self.case_config.read(self.case_config_file_name)
        self.cronjob_name = self.case_config.get("case_config", "cronjob_name")
        logger.info("check if the cronjob '%s' is exists." % self.cronjob_name)
        assert self.cronjobs.get_cronjob_detail(self.cronjob_name).status_code == 200, \
            "定时任务: %s不存在" % self.cronjob_name
        ret = self.cronjobs.start_cronjob(self.cronjob_name)
        assert ret.status_code == 200, "手动执行定时任务 '%s' 失败. 原因: %s %s " % (
            self.cronjob_name, ret.status_code, ret.text)
        self.case_config.read(self.case_config_file_name)
        self.case_config.set("case_config", "job_history_name", self.cronjobs.get_value(ret.json(), "metadata.name"))
        self.case_config.write(open(self.case_config_file_name, "w"))
        data = self.create_cronjob_base_kwargs.copy()
        data.update(self.cronjob_exe_methods[self.cronjob_name.split('-')[0]])
        data.update(self.cronjob_policys[self.cronjob_name.split('-')[1]])
        data.update(self.cronjob_types[self.cronjob_name.split('-')[2]])
        data["succes_percent"] = 20
        data["sleep_second"] = 80
        data["successfulJobsHistoryLimit"] = 20
        data["failedJobsHistoryLimit"] = 20
        data["cronjob_name"] = self.cronjob_name
        data.update({"default_label": settings.DEFAULT_LABEL})
        assert self.cronjobs.is_sub_dict(yaml.safe_load(self.job_template.render(**data)), ret.json()), "校验定时任务记录"

    @pytest.mark.upgrade
    def 测试查看定时任务详情L0(self):
        # 查看定时任务详情
        self.case_config.read(self.case_config_file_name)
        self.cronjob_name = self.case_config.get("case_config", "cronjob_name")
        ret = self.cronjobs.get_cronjob_detail(self.cronjob_name)
        data = self.create_cronjob_base_kwargs.copy()
        data.update(self.cronjob_exe_methods[self.cronjob_name.split('-')[0]])
        data.update(self.cronjob_policys[self.cronjob_name.split('-')[1]])
        data.update(self.cronjob_types[self.cronjob_name.split('-')[2]])
        data["succes_percent"] = 20
        data["sleep_second"] = 80
        data["successfulJobsHistoryLimit"] = 20
        data["failedJobsHistoryLimit"] = 20
        data["cronjob_name"] = self.cronjob_name
        data.update({"default_label": settings.DEFAULT_LABEL})
        assert ret.status_code == 200, "获取定时任务详情 %s 失败, %s" % (self.cronjob_name, ret.status_code)
        assert self.cronjobs.is_sub_dict(yaml.safe_load(self.cronjobs.body_template.render(**data)), ret.json()), \
            "获取定时任务详情 %s 失败，数据不符" % self.cronjob_name

    @pytest.mark.upgrade
    def 测试获取定时任务历史详情L0(self):
        # 获取定时任务历史详情
        self.case_config.read(self.case_config_file_name)
        job_history_name = self.case_config.get("case_config", "job_history_name")
        self.cronjob_name = self.case_config.get("case_config", "cronjob_name")
        ret = self.cronjobs.get_jobhistory_detail(job_history_name)
        data = self.create_cronjob_base_kwargs.copy()
        data.update(self.cronjob_exe_methods[self.cronjob_name.split('-')[0]])
        data.update(self.cronjob_policys[self.cronjob_name.split('-')[1]])
        data.update(self.cronjob_types[self.cronjob_name.split('-')[2]])
        data["succes_percent"] = 20
        data["sleep_second"] = 80
        data["successfulJobsHistoryLimit"] = 20
        data["failedJobsHistoryLimit"] = 20
        data["cronjob_name"] = self.cronjob_name
        data.update({"default_label": settings.DEFAULT_LABEL})
        assert ret.status_code == 200 and self.cronjobs.is_sub_dict(yaml.safe_load(self.job_template.render(**data)),
                                                                    ret.json()), "获取定时任务历史详情 %s " % job_history_name

    @pytest.mark.upgrade
    def 测试获取定时任务列表L0(self):
        # 获取定时任务列表
        ret = self.cronjobs.get_cronjob_list()
        assert ret.status_code == 200 and len(self.cronjobs.get_value(ret.json(), "items")) > 0, "获取定时任务列表"

    @pytest.mark.upgrade
    def 测试搜索定时任务L0(self):
        # 搜索定时任务
        self.case_config.read(self.case_config_file_name)
        self.cronjob_name = self.case_config.get("case_config", "cronjob_name")
        ret = self.cronjobs.search_cronjob(selector="metadata.name=" + self.cronjob_name)
        assert ret.status_code == 200 and len(self.cronjobs.get_value(ret.json(), "items")) > 0, \
            "搜索定时任务 %s %s %s" % (ret.status_code, self.cronjob_name, ret.text)

    @pytest.mark.upgrade
    def 测试获取定时任务历史列表L0(self):
        # 获取定时任务历史列表
        ret = self.cronjobs.get_jobhistory_list()
        assert ret.status_code == 200, "获取定时任务历史列表"
        assert len(self.cronjobs.get_value(ret.json(), "items")) > 0, "获取定时任务历史列表为空"

    @pytest.mark.upgrade
    def 测试搜索定时任务历史L0(self):
        # 搜索定时任务历史
        self.case_config.read(self.case_config_file_name)
        self.job_history_name = self.case_config.get("case_config", "job_history_name")
        ret = self.cronjobs.search_jobhistory(selector="metadata.name=" + self.job_history_name)
        assert ret.status_code == 200 and len(self.cronjobs.get_value(ret.json(), "items")) > 0, \
            "搜索定时任务历史 %s %s %s" % (ret.status_code, self.job_history_name, ret.text)

    @pytest.mark.delete
    def 测试删除定时任务L0(self):
        # 删除定时任务
        self.case_config.read(self.case_config_file_name)
        self.cronjob_name = self.case_config.get("case_config", "cronjob_name")
        del_ret = self.cronjobs.delete_cronjob(self.cronjob_name)
        assert del_ret.status_code == 200, "删除定时任务 %s %s" % (self.cronjob_name, del_ret.text)

    def teardown_class(self):
        if CASE_TYPE not in ("prepare", "upgrade", "delete"):
            logger.info("delete cronjobs")
            self.case_config.read(self.case_config_file_name)
            self.cronjob_name = self.case_config.get("case_config", "cronjob_name")
            self.cronjobs.delete_cronjob(self.cronjob_name)
        # if os.path.exists(self.case_config_file_name) and os.path.isfile(self.case_config_file_name):
        #     logger.info("delete config file")
        #     os.remove(self.case_config_file_name)
