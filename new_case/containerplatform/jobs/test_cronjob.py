import random
import os
import time
from time import sleep
import pytest
import yaml
from common import settings
from common.log import logger
from new_case.operation.events.events import Events
from . import Cronjobs
from common.settings import RERUN_TIMES, AUDIT_UNABLED, RESOURCE_PREFIX
from configparser import ConfigParser


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=0)
class TestCronjobsCase(object):
    def setup_class(self):
        self.cronjobs = Cronjobs()
        self.events = Events()
        self.cronjob_name = "{}-ares-cronjob-test".format(settings.RESOURCE_PREFIX)
        self.create_cronjob_base_kwargs = {
            "namespace": self.cronjobs.k8s_namespace,
            "image": self.cronjobs.global_info["$IMAGE"],
            "project": self.cronjobs.project_name,
            "default_label": settings.DEFAULT_LABEL,
            "username": settings.USERNAME
        }
        self.cronjob_exe_methods = {
            "manual": {},
            "auto": {
                # "schedule": "0-59 * * * *",
                "suspend": "false"
            }
        }
        self.cronjob_policys = {
            "allow": {
                "concurrencyPolicy": "Allow"
            },
            "forbid": {
                "concurrencyPolicy": "Forbid"
            },
            "replace": {
                "concurrencyPolicy": "Replace"
            }
        }
        self.cronjob_types = {
            "single": {},
            "parallel": {
                "parallelism": 2
            },
            "fixed": {
                "parallelism": 2,
                "completions": 2
            }
        }
        self.job_template = self.cronjobs.generate_jinja_template("verify_data/jobs/jobs.jinja2")
        self.case_config = ConfigParser()
        self.case_config.add_section("case_config")
        self.case_config_file_name = "cronjob_case_tmp.conf"

    def 测试创建定时任务_0(self):
        # 创建所有类型的定时任务
        logger.info("create cronjobs")
        results = {"flag": True}
        for exec_method in self.cronjob_exe_methods:
            for cronjob_policy in self.cronjob_policys:
                for cronjob_type in self.cronjob_types:
                    cronjob_name = "-".join([exec_method, cronjob_policy, cronjob_type, RESOURCE_PREFIX])
                    logger.info("create cronjob: %s" % cronjob_name)
                    kwargs = self.create_cronjob_base_kwargs.copy()
                    kwargs.update(self.cronjob_exe_methods[exec_method])
                    kwargs.update(self.cronjob_policys[cronjob_policy])
                    kwargs.update(self.cronjob_types[cronjob_type])
                    kwargs["succes_percent"] = 20
                    kwargs["sleep_second"] = 80
                    kwargs["successfulJobsHistoryLimit"] = 20
                    kwargs["failedJobsHistoryLimit"] = 20
                    kwargs["cronjob_name"] = cronjob_name
                    # 检查定时任务是否存在
                    logger.info(
                        "check if the cronjob '%s' is exists." % cronjob_name)
                    if self.cronjobs.get_cronjob_detail(cronjob_name).status_code != 404:
                        logger.info("the cronjob '%s' is already exists,delete it." % cronjob_name)
                        ret = self.cronjobs.delete_cronjob(cronjob_name)
                        assert ret.status_code == 200, "delete cronjob '%s' fail. reason: %s %s" % (cronjob_name, ret.status_code, ret.text)
                    kwargs.update({"default_label": settings.DEFAULT_LABEL})
                    ret = self.cronjobs.create_cronjob(**kwargs)
                    self.cronjobs.update_result(
                        results,
                        ret.status_code == 201 and self.cronjobs.is_sub_dict(yaml.safe_load(self.cronjobs.body_template.render(**kwargs)), ret.json()),
                        "create_cronjob_%s" % (cronjob_name))
                    sleep(2)

        exec_method = random.choice(list(self.cronjob_exe_methods.keys()))
        policy = random.choice(list(self.cronjob_policys.keys()))
        job_type = random.choice(list(self.cronjob_types.keys()))

        self.cronjob_name = "-".join([exec_method, policy, job_type, RESOURCE_PREFIX])
        self.case_config.set("case_config", "cronjob_name", self.cronjob_name)
        self.case_config.write(open(self.case_config_file_name, "w"))
        assert results["flag"], results

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 测试定时任务创建审计(self):
        self.case_config.read(self.case_config_file_name)
        self.cronjob_name = self.case_config.get("case_config", "cronjob_name")
        payload = {"user_name": settings.USERNAME, "operation_type": "create", "resource_type": "cronjobs",
                   "resource_name": self.cronjob_name}
        result = self.cronjobs.search_audit(payload)
        payload.update({"namespace": self.cronjobs.k8s_namespace, "region_name": settings.REGION_NAME, "code": 201})
        values = self.cronjobs.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.cronjobs.is_sub_dict(values, result.json()), "审计数据不符合预期"

    def 测试更新定时任务_1(self):
        # 检查定时任务是否存在
        self.case_config.read(self.case_config_file_name)
        self.cronjob_name = self.case_config.get("case_config", "cronjob_name")
        logger.info("check if the cronjob '%s' is exists." % self.cronjob_name)
        assert self.cronjobs.get_cronjob_detail(self.cronjob_name).status_code == 200, \
            "定时任务: %s不存在" % self.cronjob_name
        data = self.create_cronjob_base_kwargs.copy()
        data.update(self.cronjob_exe_methods[self.cronjob_name.split('-')[0]])
        data.update(self.cronjob_policys[self.cronjob_name.split('-')[1]])
        data.update(self.cronjob_types[self.cronjob_name.split('-')[2]])
        data["succes_percent"] = 20
        data["sleep_second"] = 80
        data["successfulJobsHistoryLimit"] = 20
        data["failedJobsHistoryLimit"] = 20
        data["cronjob_name"] = self.cronjob_name
        data.update({"default_label": settings.DEFAULT_LABEL})
        ret = self.cronjobs.update_cronjob(self.cronjob_name, **data)
        assert ret.status_code == 200, "更新定时任务'%s'失败. 原因: %s %s " % (
            self.cronjob_name, ret.status_code, ret.text)
        assert self.cronjobs.is_sub_dict(yaml.safe_load(self.cronjobs.body_template.render(**data)), ret.json()), \
            "更新定时任务'%s'失败，数据没有更新" % self.cronjob_name

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 测试定时任务更新审计(self):
        self.case_config.read(self.case_config_file_name)
        self.cronjob_name = self.case_config.get("case_config", "cronjob_name")
        payload = {"user_name": settings.USERNAME, "operation_type": "update", "resource_type": "cronjobs",
                   "resource_name": self.cronjob_name}
        result = self.cronjobs.search_audit(payload)
        payload.update({"namespace": self.cronjobs.k8s_namespace, "region_name": settings.REGION_NAME})
        values = self.cronjobs.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.cronjobs.is_sub_dict(values, result.json()), "审计数据不符合预期"

    def 测试执行定时任务_2(self):
        # 检查定时任务是否存在
        self.case_config.read(self.case_config_file_name)
        self.cronjob_name = self.case_config.get("case_config", "cronjob_name")
        logger.info("check if the cronjob '%s' is exists." % self.cronjob_name)
        assert self.cronjobs.get_cronjob_detail(self.cronjob_name).status_code == 200, \
            "定时任务: %s不存在" % self.cronjob_name
        ret = self.cronjobs.start_cronjob(self.cronjob_name)
        assert ret.status_code == 200, "手动执行定时任务 '%s' 失败. 原因: %s %s " % (
            self.cronjob_name, ret.status_code, ret.text)
        self.case_config.read(self.case_config_file_name)
        self.case_config.set("case_config", "job_history_name", self.cronjobs.get_value(ret.json(), "metadata.name"))
        self.case_config.set("case_config", "job_history_uid", self.cronjobs.get_value(ret.json(), "metadata.uid"))
        self.case_config.write(open(self.case_config_file_name, "w"))
        data = self.create_cronjob_base_kwargs.copy()
        data.update(self.cronjob_exe_methods[self.cronjob_name.split('-')[0]])
        data.update(self.cronjob_policys[self.cronjob_name.split('-')[1]])
        data.update(self.cronjob_types[self.cronjob_name.split('-')[2]])
        data["succes_percent"] = 20
        data["sleep_second"] = 80
        data["successfulJobsHistoryLimit"] = 20
        data["failedJobsHistoryLimit"] = 20
        data["cronjob_name"] = self.cronjob_name
        data.update({"default_label": settings.DEFAULT_LABEL})
        assert self.cronjobs.is_sub_dict(yaml.safe_load(self.job_template.render(**data)), ret.json()), "校验定时任务记录"
        sleep(3)
        self.cronjobs.start_cronjob(self.cronjob_name)

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 不测试任务创建审计(self):
        self.case_config.read(self.case_config_file_name)
        self.job_history_name = self.case_config.get("case_config", "job_history_name")
        payload = {"user_name": settings.USERNAME, "operation_type": "create", "resource_type": "jobs",
                   "resource_name": self.job_history_name}
        result = self.cronjobs.search_audit(payload)
        payload.update({"namespace": self.cronjobs.k8s_namespace, "region_name": settings.REGION_NAME})
        values = self.cronjobs.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.cronjobs.is_sub_dict(values, result.json()), "审计数据不符合预期"

    def 测试查看定时任务执行记录_11(self):
        # 检查定时任务是否存在
        self.case_config.read(self.case_config_file_name)
        self.cronjob_name = self.case_config.get("case_config", "cronjob_name")
        logger.info("check if the cronjob '%s' is exists." % self.cronjob_name)
        assert self.cronjobs.get_cronjob_detail(self.cronjob_name).status_code == 200, \
            "定时任务: %s不存在" % self.cronjob_name
        # 查看定时任务执行记录
        ret = self.cronjobs.get_cronjob_job_historys(self.cronjob_name)
        assert ret.status_code == 200, "获取定时任务执行记录 %s 失败" % self.cronjob_name
        assert len(self.cronjobs.get_value(ret.json(), "items")) > 0, \
            "获取定时任务执行记录%s失败,数据为空" % self.cronjob_name

    def 测试获取定时任务历史pod列表_12(self):
        # 获取定时任务历史pod 列表
        self.case_config.read(self.case_config_file_name)
        self.job_history_uid = self.case_config.get("case_config", "job_history_uid")
        self.cronjob_name = self.case_config.get("case_config", "cronjob_name")
        self.job_history_name = self.case_config.get("case_config", "job_history_name")
        self.cronjobs.wait_pod_scheduled(self.job_history_uid)
        ret = self.cronjobs.get_jobhistoty_pods_list(self.job_history_uid)
        logger.info(ret.text)
        assert ret.status_code == 200 and len(self.cronjobs.get_value(ret.json(), "items")) > 0, \
            "获取定时任务历史pod列表 %s" % self.job_history_name
        self.case_config.set("case_config", "job_history_podname",
                             self.cronjobs.get_value(ret.json(), "items.0.metadata.name"))
        self.case_config.set("case_config", "job_history_pod_container_name",
                             self.cronjobs.get_value(ret.json(), "items.0.spec.containers.0.name"))
        self.case_config.write(open(self.case_config_file_name, "w"))
        assert self.cronjobs.get_value(ret.json(), "items.0.spec.containers.0.name") == self.cronjob_name, "容器名称错误"

    def 测试获取定时任务列表分页_13(self):
        # 获取定时任务列表分页
        ret = self.cronjobs.get_cronjob_list(limit=2)
        assert ret.status_code == 200 and len(self.cronjobs.get_value(ret.json(), "items")) == 2, \
            "获取定时任务列表分页"

    def 测试获取定时任务k8s事件_14(self):
        # 获取定时任务历史k8s事件
        self.case_config.read(self.case_config_file_name)
        self.cronjob_name = self.case_config.get("case_config", "cronjob_name")
        etime = time.time() + 180
        while time.time() < etime:
            r = self.cronjobs.get_cronjob_k8s_events(self.cronjob_name)
            if r.status_code == 200:
                if self.cronjobs.get_value(r.json(), "total_items") > 0:
                    break
            time.sleep(1)
        ret = self.cronjobs.get_cronjob_k8s_events(self.cronjob_name)
        assert ret.status_code == 200 and self.cronjobs.get_value(ret.json(), "total_items") > 0, \
            "获取定时任务k8s事件,name is {},text is {}".format(self.cronjob_name, ret.text)

    def 测试获取定时任务历史列表分页_15(self):
        # 获取定时任务历史列表分页
        ret = self.cronjobs.get_jobhistory_list(limit=2)
        assert ret.status_code == 200, "获取定时任务历史列表"
        assert len(self.cronjobs.get_value(ret.json(), "items")) == 2, "获取定时任务历史列表分页"

    def 测试获取任务历史日志_16(self):
        # 获取定时任务历史日志
        self.case_config.read(self.case_config_file_name)
        self.job_history_podname = self.case_config.get("case_config", "job_history_podname")
        self.job_history_pod_container_name = self.case_config.get("case_config", "job_history_pod_container_name")

        etime = time.time() + 120
        while time.time() < etime:
            r = self.cronjobs.get_jobhistory_logs(self.job_history_podname, self.job_history_pod_container_name)
            if r.status_code == 200:
                if len(self.cronjobs.get_value(r.json(), "logs")) > 0:
                    break
            time.sleep(1)
        ret = self.cronjobs.get_jobhistory_logs(self.job_history_podname, self.job_history_pod_container_name)
        assert ret.status_code == 200, "获取日志失败:{}".format(ret.text)
        assert len(self.cronjobs.get_value(r.json(), "logs")) > 0, "日志条数为0,{}".format(ret.text)
        assert self.cronjobs.get_value(r.json(), "info.podName") == self.job_history_podname, \
            "获取日志返回信息podName期望是:{},实际是:{}".format(self.job_history_podname, self.cronjobs.get_value(ret.json(), "info.podName"))
        assert self.cronjobs.get_value(r.json(), "info.containerName") == self.job_history_pod_container_name, \
            "获取日志返回信息containrName期望是{},实际是:{}".format(self.job_history_pod_container_name, self.cronjobs.get_value(ret.json(), "info.containerName"))

    def 测试删除任务历史pod_18(self):
        # 删除定时任务历史pod
        self.case_config.read(self.case_config_file_name)
        self.job_history_podname = self.case_config.get("case_config", "job_history_podname")
        ret = self.cronjobs.delete_jobhistory_pod(self.job_history_podname)
        assert ret.status_code == 200, "删除定时任务历史pod %s" % self.job_history_podname

    def 测试删除任务历史_9(self):
        # 删除定时任务历史
        self.case_config.read(self.case_config_file_name)
        self.job_history_name = self.case_config.get("case_config", "job_history_name")
        self.job_history_uid = self.case_config.get("case_config", "job_history_uid")
        ret = self.cronjobs.delete_jobhistory(self.job_history_name)
        assert ret.status_code == 200, "删除任务历史 %s" % self.job_history_name
        ret = self.cronjobs.get_jobhistory_detail(self.job_history_name)
        assert ret.status_code == 404, "删除任务历史 %s 失败" % self.job_history_name

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 测试任务删除审计(self):
        self.case_config.read(self.case_config_file_name)
        self.job_history_name = self.case_config.get("case_config", "job_history_name")
        payload = {"user_name": settings.USERNAME, "operation_type": "delete", "resource_type": "jobs",
                   "resource_name": self.job_history_name}
        result = self.cronjobs.search_audit(payload)
        payload.update({"namespace": self.cronjobs.k8s_namespace, "region_name": settings.REGION_NAME})
        values = self.cronjobs.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.cronjobs.is_sub_dict(values, result.json()), "审计数据不符合预期"

    def 测试删除定时任务_10(self):
        # 删除定时任务
        self.case_config.read(self.case_config_file_name)
        self.cronjob_name = self.case_config.get("case_config", "cronjob_name")
        if self.cronjobs.get_cronjob_detail(self.cronjob_name).status_code == 200:
            del_ret = self.cronjobs.delete_cronjob(self.cronjob_name)
            assert del_ret.status_code == 200, "删除定时任务 %s %s" % (self.cronjob_name, del_ret.text)
        etime = time.time() + 60
        while time.time() < etime:
            ret = self.cronjobs.get_cronjob_job_historys(self.cronjob_name)
            if ret.status_code == 200:
                if len(self.cronjobs.get_value(ret.json(), "items")) == 0:
                    break
            time.sleep(2)
        ret = self.cronjobs.get_cronjob_job_historys(self.cronjob_name)
        assert ret.status_code == 200 and len(self.cronjobs.get_value(ret.json(), "items")) == 0, \
            "删除定时任务 %s 后任务记录没有删除%s" % (self.cronjob_name, ret.text)

    def 测试获取定时任务历史k8s事件_17(self):
        # 获取定时任务历史k8s事件
        self.case_config.read(self.case_config_file_name)
        self.job_history_name = self.case_config.get("case_config", "job_history_name")
        etime = time.time() + 300
        while time.time() < etime:
            r = self.cronjobs.get_jobhistory_k8s_events(self.job_history_name)
            if r.status_code == 200:
                if self.cronjobs.get_value(r.json(), "total_items") > 0:
                    break
            time.sleep(3)
        ret = self.cronjobs.get_jobhistory_k8s_events(self.job_history_name)
        assert ret.status_code == 200 and self.cronjobs.get_value(ret.json(), "total_items") > 0, \
            "获取定时任务历史k8s事件,name is {},text is {}".format(self.job_history_name, ret.text)

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 测试定时任务删除审计(self):
        self.case_config.read(self.case_config_file_name)
        self.cronjob_name = self.case_config.get("case_config", "cronjob_name")
        payload = {"user_name": settings.USERNAME, "operation_type": "delete", "resource_type": "cronjobs",
                   "resource_name": self.cronjob_name}
        result = self.cronjobs.search_audit(payload)
        payload.update({"namespace": self.cronjobs.k8s_namespace, "region_name": settings.REGION_NAME})
        values = self.cronjobs.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.cronjobs.is_sub_dict(values, result.json()), "审计数据不符合预期"

    def teardown_class(self):
        logger.info("delete cronjobs")
        for exec_method in self.cronjob_exe_methods:
            for cronjob_policy in self.cronjob_policys:
                for cronjob_type in self.cronjob_types:
                    cronjob_name = "-".join([exec_method, cronjob_policy, cronjob_type, RESOURCE_PREFIX])
                    self.cronjobs.delete_cronjob(cronjob_name)
        if os.path.exists(self.case_config_file_name) and os.path.isfile(self.case_config_file_name):
            logger.info("delete config file")
            os.remove(self.case_config_file_name)
