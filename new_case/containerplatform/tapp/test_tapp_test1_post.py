import pytest
from new_case.containerplatform.tapp.tapp import TApp
from common import settings
from common.base_request import Common
from common.settings import K8S_NAMESPACE, REGION_NAME, DEFAULT_LABEL
from new_case.containerplatform.configmap.configmap import Configmap
from new_case.containerplatform.persistentvolumeclaims.pvc import Pvc
from new_case.containerplatform.secret.secret import Secret
import base64
import json


@pytest.mark.metis
@pytest.mark.tapp
@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
@pytest.mark.skipif(not settings.get_featuregate_status(REGION_NAME, 'tapp'), reason="tapp功能开关未开启，默认不跑对应用例")
class TestTAppSuitePost(object):

    def setup_class(self):
        self.tapp = TApp()
        self.k8s_namespace = K8S_NAMESPACE
        self.region_name = REGION_NAME
        self.default_label = DEFAULT_LABEL

        self.configmap_tool = Configmap()
        self.verify_template_configmap = Common.generate_jinja_template(self,
                                                                        './verify_data/configmap/create_response.jinja2')

        self.pvc = Pvc()
        self.pvcname = '{}-ares-tapp-pvc'.format(settings.RESOURCE_PREFIX).replace('_', '-')

        self.secret_tool = Secret()
        self.secret_name = "{}-ares-tapp-secret".format(settings.RESOURCE_PREFIX)
        self.verify_template = Common.generate_jinja_template(self, './verify_data/secret/create_response.jinja2')

    @pytest.mark.prepare
    def 测试_创建TApp_不使用templatePool(self):
        data = self.tapp.data_list_notemplatePool
        region_name = self.region_name
        ns_name = data["namespace"]
        tapp_name = data['tapp_name']
        data.update({"default_label": settings.DEFAULT_LABEL})
        self.tapp.delete_tapp_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])

        create_result = self.tapp.create_tapp_jinja2("./test_data/tapp/tapp.jinja2", data=data)
        assert create_result.status_code == 201, "创建{}tapp失败{}".format(data['tapp_name'], create_result.text)

        detail_result = self.tapp.get_tapp_detail_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])
        assert detail_result.status_code == 200, "获取tapp{}详情页失败,{}".format(data['tapp_name'], detail_result.text)

        ret = self.tapp.get_status("kubernetes/{}/apis/apps.tkestack.io/v1/namespaces/{}/tapps/{}"
                                   .format(region_name, ns_name, tapp_name), "status.appStatus", expect_value='Running')
        assert ret, "创建tapp后，验证tapp状态出错: tapp：{} is not running".format(data["tapp_name"])

        value = self.tapp.generate_jinja_data("./verify_data/tapp/create_tapp.jinja2", data)
        assert self.tapp.is_sub_dict(value, create_result.json()), "创建TApp_比对数据失败，返回数据:{},期望数据:{}" \
            .format(create_result.json(), value)

    @pytest.mark.skipif(settings.AUDIT_UNABLED, reason="do not have audit")
    def 不测试Tapp创建审计(self):
        payload = {"user_name": settings.USERNAME, "operation_type": "create", "resource_type": "tapps",
                   "resource_name": self.tapp.data_list_notemplatePool['tapp_name']}
        result = self.tapp.search_audit(payload)
        payload.update({"namespace": self.tapp.data_list_notemplatePool["namespace"],
                        "region_name": settings.REGION_NAME, "code": 201})
        values = self.tapp.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.tapp.is_sub_dict(values, result.json()), "审计数据不符合预期"

    def 测试_创建TApp_必填参数_可选参数有三个_节点异常策略和更新策略和组件标签(self):
        data = self.tapp.data_list_force_strategy_labels
        region_name = self.region_name
        ns_name = data["namespace"]
        tapp_name = data['tapp_name']
        data.update({"default_label": settings.DEFAULT_LABEL})
        self.tapp.delete_tapp_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])

        create_result = self.tapp.create_tapp_jinja2("./test_data/tapp/tapp.jinja2", data=data)
        assert create_result.status_code == 201, "创建{}tapp失败{}".format(data['tapp_name'], create_result.text)

        detail_result = self.tapp.get_tapp_detail_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])
        assert detail_result.status_code == 200, "获取tapp{}详情页失败,{}".format(data['tapp_name'], detail_result.text)

        ret = self.tapp.get_status("kubernetes/{}/apis/apps.tkestack.io/v1/namespaces/{}/tapps/{}"
                                   .format(region_name, ns_name, tapp_name), "status.appStatus", expect_value='Running')
        assert ret, "创建tapp后，验证tapp状态出错: tapp：{} is not running".format(data["tapp_name"])

        value = self.tapp.generate_jinja_data("./verify_data/tapp/create_tapp.jinja2", data)
        assert self.tapp.is_sub_dict(value, create_result.json()), "创建TApp_比对数据失败，返回数据:{},期望数据:{}" \
            .format(create_result.json(), value)

    def 测试_创建TApp_必填参数_可选参数有三个_启动命令和参数和健康检查(self):
        data = self.tapp.data_list_args_livenessprobe
        region_name = self.region_name
        ns_name = data["namespace"]
        tapp_name = data['tapp_name']
        data.update({"default_label": settings.DEFAULT_LABEL})
        self.tapp.delete_tapp_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])
        create_result = self.tapp.create_tapp_jinja2("./test_data/tapp/tapp.jinja2", data=data)
        assert create_result.status_code == 201, "创建{}tapp失败{}".format(data['tapp_name'], create_result.text)

        detail_result = self.tapp.get_tapp_detail_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])
        assert detail_result.status_code == 200, "获取tapp{}详情页失败,{}".format(data['tapp_name'], detail_result.text)

        ret = self.tapp.get_status("kubernetes/{}/apis/apps.tkestack.io/v1/namespaces/{}/tapps/{}"
                                   .format(region_name, ns_name, tapp_name), "status.appStatus", expect_value='Running')
        assert ret, "创建tapp后，验证tapp状态出错: tapp：{} is not running".format(data["tapp_name"])

        value = self.tapp.generate_jinja_data("./verify_data/tapp/create_tapp.jinja2", data)
        assert self.tapp.is_sub_dict(value, create_result.json()), "创建TApp_比对数据失败，返回数据:{},期望数据:{}" \
            .format(create_result.json(), value)

    def 测试_创建TApp_必填参数_可选参数有两个_有一个配置引用_configmap和环境变量(self):
        result = {"flag": True}
        # 创建configmap
        configmap_name = self.tapp.data_list_configmap_evn["configmap_name"]
        cm_data = {
            "configmap_name": configmap_name,
            "description": "key-value",
            "namespace": settings.K8S_NAMESPACE,
            "configmap_key": "key",
            "configmap_value": "value"
        }
        self.configmap_tool.delete_configmap(configmap_name)
        ret = self.configmap_tool.create_configmap(data=cm_data)
        assert ret.status_code == 201, "创建配置字典失败:{}".format(ret.text)

        value = self.verify_template_configmap.render(cm_data)
        assert self.configmap_tool.is_sub_dict(json.loads(value), ret.json()), \
            "创建configmap比对数据失败，返回数据:{},期望数据:{}".format(ret.json(), value)

        # 创建tapp
        data = self.tapp.data_list_configmap_evn
        region_name = self.region_name
        ns_name = data["namespace"]
        tapp_name = data['tapp_name']
        data.update({"default_label": settings.DEFAULT_LABEL})
        self.tapp.delete_tapp_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])
        create_result = self.tapp.create_tapp_jinja2("./test_data/tapp/tapp.jinja2", data=data)
        assert create_result.status_code == 201, "创建{}tapp失败{}".format(data['tapp_name'], create_result.text)

        detail_result = self.tapp.get_tapp_detail_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])
        assert detail_result.status_code == 200, "获取tapp{}详情页失败,{}".format(data['tapp_name'], detail_result.text)

        ret = self.tapp.get_status("kubernetes/{}/apis/apps.tkestack.io/v1/namespaces/{}/tapps/{}"
                                   .format(region_name, ns_name, tapp_name), "status.appStatus", expect_value='Running')
        assert ret, "创建tapp后，验证tapp状态出错: tapp：{} is not running".format(data["tapp_name"])

        value = self.tapp.generate_jinja_data("./verify_data/tapp/create_tapp.jinja2", data)
        assert self.tapp.is_sub_dict(value, create_result.json()), \
            "创建TApp_比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

        # 验证
        content = detail_result.json()
        v1 = self.tapp.get_value(content, 'spec.template.spec.containers.0.envFrom.0.configMapRef.name')
        result = self.tapp.update_result(result, v1 == configmap_name, '环境变量完整引用configmap失败')

        # 删除configmap
        ret = self.configmap_tool.delete_configmap(configmap_name=data['configmap_name'])
        assert ret.status_code == 200, "删除配置字典{}失败:{}".format(data['configmap_name'], ret.text)
        assert result['flag'], result

    @pytest.mark.skipif(not settings.SC_NAME, reason="没有存储类，不运行当前用例")
    def 测试_创建TApp_必填参数_可选参数有两个_存储卷挂载和存储卷(self):
        result = {"flag": True}
        # 创建pvc
        pvc_data = {
            "pvc_name": self.pvcname,
            "namespace": settings.K8S_NAMESPACE,
            "storageclassname": settings.SC_NAME
        }
        data = self.tapp.data_list_volume
        self.tapp.delete_tapp_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])
        self.pvc.delete_pvc(settings.K8S_NAMESPACE, self.pvcname)
        self.pvc.check_exists(self.pvc.get_common_pvc_url_v1(pvc_data["namespace"], pvc_data['pvc_name']), 404)

        create_result = self.pvc.create_pvc_jinja2("./test_data/pvc/pvc.jinja2", data=pvc_data)
        assert create_result.status_code == 201, "创建pvc{}失败:{}".format(pvc_data['pvc_name'], create_result.text)

        pvc_status = self.pvc.get_status(self.pvc.get_common_pvc_url_v1(pvc_name=pvc_data["pvc_name"]), "status.phase",
                                         "Bound")
        assert pvc_status, "创建pvc状态错误:{}的状态没有被绑定".format(pvc_data["pvc_name"])

        value = self.pvc.generate_jinja_data("./verify_data/pvc/create-pvc.jinja2", pvc_data)
        assert self.pvc.is_sub_dict(value, create_result.json()), \
            "创建pvc_必填参数_可选参数为空比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

        # 创建tapp
        region_name = self.region_name
        ns_name = data["namespace"]
        tapp_name = data['tapp_name']
        data.update({"default_label": settings.DEFAULT_LABEL})

        create_result = self.tapp.create_tapp_jinja2("./test_data/tapp/tapp.jinja2", data=data)
        assert create_result.status_code == 201, "创建{}tapp失败{}".format(data['tapp_name'], create_result.text)

        detail_result = self.tapp.get_tapp_detail_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])
        assert detail_result.status_code == 200, "获取tapp{}详情页失败,{}".format(data['tapp_name'], detail_result.text)

        ret = self.tapp.get_status("kubernetes/{}/apis/apps.tkestack.io/v1/namespaces/{}/tapps/{}".
                                   format(region_name, ns_name, tapp_name), "status.appStatus", expect_value='Running')
        assert ret, "创建tapp后，验证tapp状态出错: tapp：{} is not running".format(data["tapp_name"])

        value = self.tapp.generate_jinja_data("./verify_data/tapp/create_tapp.jinja2", data)
        assert self.tapp.is_sub_dict(value, create_result.json()), "创建TApp_比对数据失败，返回数据:{},期望数据:{}".format(
            create_result.json(), value)

        # 验证
        content = detail_result.json()
        claimName = self.tapp.get_value(content, 'spec.template.spec.volumes.0.persistentVolumeClaim.claimName')
        result = self.tapp.update_result(result, claimName == self.pvcname, '部署挂载pvc失败')

        # 删除tapp
        self.tapp.delete_tapp_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])
        self.tapp.check_exists(self.tapp.get_common_tapp_url(ns_name=data["namespace"], tapp_name=data['tapp_name']),
                               404)

        # 删除pvc
        delete_result = self.pvc.delete_pvc(ns_name=pvc_data["namespace"], pvc_name=pvc_data["pvc_name"])
        assert delete_result.status_code == 200, "删除pvc失败{}".format(delete_result.text)
        assert result['flag'], result

    def 测试_创建TApp_必填参数_可选参数有两个_网络模式和配置引用_secret(self):
        result = {"flag": True}
        # 创建secret
        secret_data = {
            "secret_name": self.secret_name,
            "secret_type": "Opaque",
            "opaque_key": "opaque_key",
            "opaque_value": str(base64.b64encode("opaque_value".encode('utf-8')), 'utf8')
        }
        secret_data.update({"K8S_NAMESPACE": settings.K8S_NAMESPACE})
        self.secret_tool.delete_secret(self.secret_name)
        ret = self.secret_tool.create_secret('./test_data/secret/create_secret.jinja2', data=secret_data)

        assert ret.status_code == 201, "创建{}类型保密字典失败:{}".format(secret_data['secret_type'], ret.text)
        value = self.verify_template.render(secret_data)
        assert self.secret_tool.is_sub_dict(json.loads(value), ret.json()), "创建保密字典比对数据失败，返回数据:{},期望数据:{}".format(
            ret.json(), json.loads(value))

        # 创建tapp
        data = self.tapp.data_list_hostnetwork_secret
        region_name = self.region_name
        ns_name = data["namespace"]
        tapp_name = data['tapp_name']
        data.update({"default_label": settings.DEFAULT_LABEL})
        self.tapp.delete_tapp_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])
        create_result = self.tapp.create_tapp_jinja2("./test_data/tapp/tapp.jinja2", data=data)
        assert create_result.status_code == 201, "创建{}tapp失败{}".format(data['tapp_name'], create_result.text)

        detail_result = self.tapp.get_tapp_detail_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])
        assert detail_result.status_code == 200, "获取tapp{}详情页失败,{}".format(data['tapp_name'], detail_result.text)

        ret = self.tapp.get_status("kubernetes/{}/apis/apps.tkestack.io/v1/namespaces/{}/tapps/{}"
                                   .format(region_name, ns_name, tapp_name), "status.appStatus", expect_value='Running')
        assert ret, "创建tapp后，验证tapp状态出错: tapp：{} is not running".format(data["tapp_name"])

        value = self.tapp.generate_jinja_data("./verify_data/tapp/create_tapp.jinja2", data)
        assert self.tapp.is_sub_dict(value, create_result.json()), "创建TApp_比对数据失败，返回数据:{},期望数据:{}".format(
            create_result.json(), value)

        # 验证
        content = detail_result.json()
        v1 = self.tapp.get_value(content, 'spec.template.spec.containers.0.envFrom.0.secretRef.name')
        result = self.tapp.update_result(result, v1 == self.secret_name, '环境变量完整引用secret失败')

        ret = self.secret_tool.delete_secret(data["secret_name"])
        assert ret.status_code == 200, "删除{}类型的保密字典失败:{}".format(data["secret_type"], ret.text)
        assert result['flag'], result

    def 测试_创建TApp_必填参数_可选端口(self):
        data = self.tapp.data_list_port
        region_name = self.region_name
        ns_name = data["namespace"]
        tapp_name = data['tapp_name']
        data.update({"default_label": settings.DEFAULT_LABEL})
        self.tapp.delete_tapp_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])
        create_result = self.tapp.create_tapp_jinja2("./test_data/tapp/tapp.jinja2", data=data)
        assert create_result.status_code == 201, "创建{}tapp失败{}".format(data['tapp_name'], create_result.text)

        detail_result = self.tapp.get_tapp_detail_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])
        assert detail_result.status_code == 200, "获取tapp{}详情页失败,{}".format(data['tapp_name'], detail_result.text)

        ret = self.tapp.get_status("kubernetes/{}/apis/apps.tkestack.io/v1/namespaces/{}/tapps/{}"
                                   .format(region_name, ns_name, tapp_name), "status.appStatus", expect_value='Running')
        assert ret, "创建tapp后，验证tapp状态出错: tapp：{} is not running".format(data["tapp_name"])

        value = self.tapp.generate_jinja_data("./verify_data/tapp/create_tapp.jinja2", data)
        assert self.tapp.is_sub_dict(value, create_result.json()), "创建TApp_比对数据失败，返回数据:{},期望数据:{}".format(
            create_result.json(), value)
