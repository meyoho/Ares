import sys
from common.base_request import Common
from common.log import logger
from common import settings
from time import sleep
import random


class TApp(Common):
    def __init__(self):
        super(TApp, self).__init__()
        self.genetate_global_info()

    def get_common_tapp_url(self, ns_name=settings.K8S_NAMESPACE, tapp_name=''):
        return 'kubernetes/{}/apis/apps.tkestack.io/v1/namespaces/{}/tapps/{}'.format(self.region_name, ns_name,
                                                                                      tapp_name)

    def get_common_tapp_list_url(self, ns_name=settings.K8S_NAMESPACE, limit=1, cnt=""):
        return limit and "kubernetes/{}/apis/apps.tkestack.io/v1/namespaces/{}/tapps/?limit={}&continue={}" \
            .format(self.region_name, ns_name, limit, cnt)

    def get_common_tapp_search_url(self, ns_name=settings.K8S_NAMESPACE, limit=1, cnt="", tapp_name=''):
        return limit and "kubernetes/{}/apis/apps.tkestack.io/v1/namespaces/{}/tapps/?limit={}&continue={}" \
                         "&fieldSelector=metadata.name={}".format(self.region_name, ns_name, limit, cnt, tapp_name)

    def get_common_tapp_search_url_v1(self, ns_name=settings.K8S_NAMESPACE, limit=1, tapp_name=''):
        return limit and "acp/v1/resources/search/kubernetes/{}/apis/apps.tkestack.io/v1/namespaces/{}/tapps?" \
                         "limit={}&keyword={}&field=metadata.name".format(self.region_name, ns_name, limit, tapp_name)

    def get_tapp_pod_url(self, ns_name=settings.K8S_NAMESPACE, limit=10, pro_name=settings.PROJECT_NAME,
                         project=settings.PROJECT_NAME, tapp_name=''):
        return limit and "kubernetes/{}/api/v1/namespaces/{}/pods?limit={}&project={}&" \
                         "labelSelector=project.alauda.io/name={},service.alauda.io/name=tapp-{}". \
            format(self.region_name, ns_name, limit, pro_name, project, tapp_name)

    def create_tapp_jinja2(self, file, data):
        path = self.get_common_tapp_url()
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=path, json=data, params={})

    def get_tapp_detail_jinja2(self, ns_name, tapp_name):
        path = self.get_common_tapp_url(ns_name=ns_name, tapp_name=tapp_name)
        return self.send(method='get', path=path, params={})

    def update_tapp_jinja2(self, tapp_name, file, data):
        path = self.get_common_tapp_url(tapp_name=tapp_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='put', path=path, json=data, params={})

    def update_tapp_jinja2_patch(self, tapp_name, file, data):
        path = self.get_common_tapp_url(tapp_name=tapp_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='patch', path=path, json=data, params={},
                         headers={"Content-Type": "application/merge-patch+json"})

    def search_tapp_jinja2(self, ns_name="", limit=None, cnt="", tapp_name=""):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_tapp_search_url(ns_name=ns_name, limit=limit, cnt=cnt, tapp_name=tapp_name)
        return self.send(method='get', path=url)

    def search_tapp_jinja2_v1(self, ns_name="", limit=None, tapp_name=""):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_tapp_search_url_v1(ns_name=ns_name, limit=limit, tapp_name=tapp_name)
        return self.send(method='get', path=url)

    def get_tapp_list_jinja2(self, limit=None, cnt=""):
        path = self.get_common_tapp_list_url(limit=limit, cnt=cnt)
        return self.send(method='get', path=path, params={})

    def delete_tapp_jinja2(self, ns_name, tapp_name, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_tapp_url(ns_name, tapp_name)
        return self.send(method='delete', path=url, params={}, auth=auth)

    def get_tapp_pods(self, ns_name, tapp_name, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_tapp_pod_url(ns_name=ns_name, tapp_name=tapp_name)
        return self.send(method='get', path=url, auth=auth)

    def get_tapp_podname(self, data, key):
        cnt = 0
        while cnt < 10:
            cnt += 1
            ret = self.get_tapp_pods(ns_name=data['namespace'], tapp_name=data['tapp_name'])
            assert ret.status_code == 200, "获取容器组失败:{}".format(ret.text)
            if len(ret.json()["items"]):
                return self.get_value(ret.json(), key)
            sleep(3)
        return ""

    def get_tapp_logs(self, ns_name, pod_name, container_name):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        url = "acp/v1/kubernetes/{}/namespaces/{}/pods/{}/{}/log?previous=false&logFilePosition=end" \
              "&referenceTimestamp=newest&offsetFrom=2000000000&offsetTo=2000000100". \
            format(self.region_name, ns_name, pod_name, container_name)
        return self.send(method="get", path=url)

    def tapp_gated_launch(self, tapp_name, file, data):
        path = self.get_common_tapp_url(tapp_name=tapp_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='patch', path=path, json=data, params={},
                         headers={"Content-Type": "application/merge-patch+json"})

    def rebuild_tapp_pod(self, ns_name, pod_name):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        url = "kubernetes/{}/api/v1/namespaces/{}/pods/{}".format(self.region_name, ns_name, pod_name)
        return self.send(method="delete", path=url)

    def stop_start_tapp(self, ns_name, tapp_name, file, data):
        path = self.get_common_tapp_url(ns_name=ns_name, tapp_name=tapp_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='patch', path=path, json=data, params={},
                         headers={"Content-Type": "application/merge-patch+json"})

    def delete_pod(self, ns_name, tapp_name, file, data):
        path = self.get_common_tapp_url(ns_name=ns_name, tapp_name=tapp_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='patch', path=path, json=data, params={},
                         headers={"Content-Type": "application/merge-patch+json"})

    # 创建tapp_不使用templatePool
    data_list_notemplatePool = {
        "tapp_name": '{}-example-tapp'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "key": "key1",
        "value": "value1"
    }

    # 创建tapp_必填参数
    data_list = {
        "tapp_name": '{}-tapp'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "project": settings.PROJECT_NAME,
        "key": "key1",
        "value": "value1"
    }

    # 创建tapp_必填参数_可选参数有三个_节点异常策略和更新策略和组件标签
    data_list_force_strategy_labels = {
        "tapp_name": '{}-tapp-force_strategy_labels'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "project": settings.PROJECT_NAME,
        "key": "key1",
        "value": "value1",
        "replicas": 2,
        "maxunavailable": 1,
        "forcedeletepod": "true"
    }

    # 创建tapp_必填参数_可选参数有三个_启动命令和参数和健康检查
    data_list_args_livenessprobe = {
        "tapp_name": '{}-tapp-args_livenessprobe'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "project": settings.PROJECT_NAME,
        "key": "key1",
        "value": "value1",
        "args": " ",
        "args_1": "-c",
        "args_2": "while true; do sleep 30; echo 123; done",
        "command": " ",
        "command_1": "/bin/sh",
        "livenessProbe": " ",
        "failurethreshold": 5,
        "path": "/",
        "port": 80,
        "scheme": "HTTP",
        "initialdelayseconds": 300,
        "periodseconds": 60,
        "successthreshold": 1,
        "timeoutSeconds": 30
    }

    # 创建tapp_必填参数_可选参数有两个_有一个配置引用_configmap和环境变量
    data_list_configmap_evn = {
        "tapp_name": '{}-tapp_configmap_evn'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "project": settings.PROJECT_NAME,
        "key": "key1",
        "value": "value1",
        "envFrom": " ",
        "configMapRef": " ",
        "configmap_name": "{}-ares-tapp-configmap".format(settings.RESOURCE_PREFIX),
        "env": " ",
        "env_name": "env-name1",
        "env_value": "env-value1"
    }

    # 创建tapp_必填参数_可选参数有两个_存储卷挂载和存储卷
    data_list_volume = {
        "tapp_name": '{}-tapp_volume'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "project": settings.PROJECT_NAME,
        "key": "key1",
        "value": "value1",
        "volumeMounts": " ",
        "mountpath": "/pvc/test",
        "volume_name": "new-volume",
        "volumes": " ",
        "claimname": '{}-ares-tapp-pvc'.format(settings.RESOURCE_PREFIX).replace('_', '-')
    }

    # 创建tapp_必填参数_可选参数有两个_网络模式和配置引用_secret
    data_list_hostnetwork_secret = {
        "tapp_name": '{}-tapp_hostnetwork_secret'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "project": settings.PROJECT_NAME,
        "key": "key1",
        "value": "value1",
        "hostNetwork": " ",
        "hostnetwork": "true",
        "envFrom": " ",
        "secretRef": " ",
        "secret_name": "{}-ares-tapp-secret".format(settings.RESOURCE_PREFIX)
    }

    # 创建tapp_必填参数_可选端口
    data_list_port = {
        "tapp_name": '{}-tapp_port'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "project": settings.PROJECT_NAME,
        "key": "key1",
        "value": "value1",
        "ports": " ",
        "protocol": "TCP",
        "hostPort": random.randrange(32000, 34000, 1),
        "containerPort": 80
    }
