import pytest
from new_case.containerplatform.tapp.tapp import TApp
from common import settings
from common.settings import K8S_NAMESPACE, REGION_NAME, DEFAULT_LABEL
from time import sleep


@pytest.mark.archon
@pytest.mark.tapp_controller
@pytest.mark.tapp
@pytest.mark.BAT
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
@pytest.mark.skipif(not settings.get_featuregate_status(REGION_NAME, 'tapp'), reason="tapp功能开关未开启，默认不跑对应用例")
class TestTAppSuite(object):

    def setup_class(self):
        self.tapp = TApp()
        self.k8s_namespace = K8S_NAMESPACE
        self.region_name = REGION_NAME
        self.default_label = DEFAULT_LABEL

    @pytest.mark.prepare
    def 测试_创建TApp(self):
        data = self.tapp.data_list
        region_name = self.region_name
        ns_name = data["namespace"]
        tapp_name = data['tapp_name']
        data.update({"default_label": settings.DEFAULT_LABEL})
        self.tapp.delete_tapp_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])

        create_result = self.tapp.create_tapp_jinja2("./test_data/tapp/tapp.jinja2", data=data)
        assert create_result.status_code == 201, "创建{}tapp失败{}".format(data['tapp_name'], create_result.text)

        detail_result = self.tapp.get_tapp_detail_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])
        assert detail_result.status_code == 200, "获取tapp{}详情页失败,{}".format(data['tapp_name'], detail_result.text)

        ret = self.tapp.get_status("kubernetes/{}/apis/apps.tkestack.io/v1/namespaces/{}/tapps/{}".
                                   format(region_name, ns_name, tapp_name), "status.appStatus", expect_value='Running')
        assert ret, "创建tapp后，验证tapp状态出错: tapp：{} is not running".format(data["tapp_name"])

        value = self.tapp.generate_jinja_data("./verify_data/tapp/create_tapp.jinja2", data)
        assert self.tapp.is_sub_dict(value, create_result.json()), "创建TApp_比对数据失败，返回数据:{},期望数据:{}" \
            .format(create_result.json(), value)

    @pytest.mark.upgrade
    def 测试_获取tapp列表(self):
        data = self.tapp.data_list
        ret = self.tapp.get_tapp_list_jinja2(limit=20)
        assert ret.status_code == 200, "获取tapp列表失败:{}".format(ret.text)

        value = self.tapp.generate_jinja_data("./verify_data/tapp/list_tapp.jinja2", data)
        assert self.tapp.is_sub_dict(value, ret.json()), "获取tapp列表比对数据失败，返回数据:{},期望数据:{}" \
            .format(ret.json(), value)

    @pytest.mark.upgrade
    def 测试_获取tapp容器组(self):
        data = self.tapp.data_list
        ret = self.tapp.get_tapp_pods(ns_name=data['namespace'], tapp_name=data['tapp_name'])
        assert ret.status_code == 200, "获取容器组失败:{}".format(ret.text)
        global pod_name
        pod_name = self.tapp.get_tapp_podname(data, "items.0.metadata.name")

        global container_name
        container_name = self.tapp.get_tapp_podname(data, 'items.0.spec.containers.0.name')

        data.update({'pod_name': pod_name})
        value = self.tapp.generate_jinja_data("./verify_data/tapp/list_pod.jinja2", data)
        assert self.tapp.is_sub_dict(value, ret.json()), "获取容器组比对数据失败，返回数据:{},期望数据:{}" \
            .format(ret.json(), value)

    @pytest.mark.upgrade
    def 测试_获取日志(self):
        data = self.tapp.data_list
        sleep(10)
        ret = self.tapp.get_tapp_logs(self.k8s_namespace, pod_name, container_name)
        assert ret.status_code == 200, "获取日志失败:{}".format(ret.text)
        data.update({'pod_name': pod_name, 'container_name': container_name})
        values = self.tapp.generate_jinja_data("verify_data/tapp/log_tapp.jinja2", data)
        assert self.tapp.is_sub_dict(values, ret.json()), "获取日志比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)
        assert len(self.tapp.get_value(ret.json(), 'logs')) != 0, "获取日志为空 {}".format(ret.json())

    @pytest.mark.upgrade
    def 测试_更新tapp(self):
        data = self.tapp.data_list
        detail_result = self.tapp.get_tapp_detail_jinja2(ns_name=data['namespace'], tapp_name=data['tapp_name'])
        assert detail_result.status_code == 200, "获取tapp详情失败:{}".format(detail_result.text)

        resourceversion = self.tapp.get_value(detail_result.json(), "metadata.resourceVersion")
        data.update({"value": "value1-a",
                     "metadata_resourceVersion": {"resourceVersion": ""}, "resourceversion": resourceversion})

        update_result = self.tapp.update_tapp_jinja2(data['tapp_name'], './test_data/tapp/tapp.jinja2', data=data)
        assert update_result.status_code == 200, "更新tapp:{}失败:{}".format(data['tapp_name'], update_result.text)

        value = self.tapp.generate_jinja_data("./verify_data/tapp/create_tapp.jinja2", data)
        assert self.tapp.is_sub_dict(value, update_result.json()), \
            "更新TApp比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

    '''
    @pytest.mark.metis
    @pytest.mark.upgrade
    def 测试_pod销毁重建(self):
        data = self.tapp.data_list
        region_name = self.region_name
        ns_name = data["namespace"]
        tapp_name = data['tapp_name']
        ret = self.tapp.rebuild_tapp_pod(self.k8s_namespace, pod_name)
        assert ret.status_code == 200, "pod销毁重建失败:{}".format(ret.text)
        data.update({'pod_name': pod_name})
        values = self.tapp.generate_jinja_data("verify_data/tapp/rebuild_pod_tapp.jinja2", data)
        assert self.tapp.is_sub_dict(values, ret.json()), \
            "销毁重建容器比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)

        detail_result = self.tapp.get_tapp_detail_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])
        assert detail_result.status_code == 200, "获取tapp{}详情页失败,{}".format(data['tapp_name'], detail_result.text)

        ret = self.tapp.get_status("kubernetes/{}/apis/tke.cloud.tencent.com/v1/namespaces/{}/tapps/{}".
                                   format(region_name, ns_name, tapp_name), "status.appStatus", expect_value='Running')
        assert ret, "创建tapp后，验证tapp状态出错: tapp：{} is not running".format(data["tapp_name"])
    '''

    @pytest.mark.upgrade
    def 测试_tapp灰度发布(self):
        data = self.tapp.data_list
        data.update({"default_label": settings.DEFAULT_LABEL})
        ret = self.tapp.get_tapp_pods(ns_name=data['namespace'], tapp_name=data['tapp_name'])
        assert ret.status_code == 200, "获取容器组失败:{}".format(ret.text)
        global template_hash_key
        template_hash_key = self.tapp.get_value(ret.json(), 'items.0.metadata.labels.tapp_template_hash_key')

        global uniq_hash_key
        uniq_hash_key = self.tapp.get_value(ret.json(), 'items.0.metadata.labels.tapp_uniq_hash_key')

        data.update({'template_hash_key': template_hash_key, 'uniq_hash_key': uniq_hash_key})

        update_result = self.tapp.tapp_gated_launch(data['tapp_name'], './test_data/tapp/tapp_gated-launch.jinja2',
                                                    data=data)
        assert update_result.status_code == 200, "tapp灰度发布:{}失败:{}".format(data['tapp_name'], update_result.text)

        value = self.tapp.generate_jinja_data("./verify_data/tapp/gated-launch_tapp.jinja2", data)
        assert self.tapp.is_sub_dict(value, update_result.json()), \
            "TApp灰度发布比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

    @pytest.mark.upgrade
    def 测试_停止tapp(self):
        data = self.tapp.data_list
        data.update({'replicas': 0})
        ret = self.tapp.stop_start_tapp(data['namespace'], data['tapp_name'], "./test_data/tapp/tapp_stop_start.jinja2",
                                        data=data)
        assert ret.status_code == 200, "停止tapp失败:{}".format(ret.text)

        detail_result = self.tapp.get_tapp_detail_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])
        assert detail_result.status_code == 200, "获取tapp{}详情页失败,{}".format(data['tapp_name'], detail_result.text)
        value = self.tapp.get_value(detail_result.json(), 'spec.replicas')
        assert value == 0, "停止tapp后，验证状态出错：tapp{}不是停止状态".format(data['tapp_name'])

    @pytest.mark.upgrade
    def 测试_开始tapp(self):
        data = self.tapp.data_list
        region_name = self.region_name
        ns_name = data["namespace"]
        tapp_name = data['tapp_name']
        data.update({'replicas': 1})
        ret = self.tapp.stop_start_tapp(data['namespace'], data['tapp_name'], "./test_data/tapp/tapp_stop_start.jinja2",
                                        data=data)
        assert ret.status_code == 200, "开始tapp失败:{}".format(ret.text)

        detail_result = self.tapp.get_tapp_detail_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])
        assert detail_result.status_code == 200, "获取tapp{}详情页失败,{}".format(data['tapp_name'], detail_result.text)
        ret = self.tapp.get_status("kubernetes/{}/apis/apps.tkestack.io/v1/namespaces/{}/tapps/{}".
                                   format(region_name, ns_name, tapp_name), "status.appStatus", expect_value='Running')
        assert ret, "创建tapp后，验证tapp状态出错: tapp：{} is not running".format(data["tapp_name"])

    @pytest.mark.delete
    def 测试_pod删除(self):
        data = self.tapp.data_list
        data.update({'status': 'Killed'})
        ret = self.tapp.delete_pod(data['namespace'], data['tapp_name'], "./test_data/tapp/delete_pod.jinja2",
                                   data=data)
        assert ret.status_code == 200, "删除pod失败:{}".format(ret.text)

        detail_result = self.tapp.get_tapp_detail_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])
        assert detail_result.status_code == 200, "获取tapp{}详情页失败,{}".format(data['tapp_name'], detail_result.text)

        for i in range(0, 20):
            detail_result = self.tapp.get_tapp_detail_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])
            value = self.tapp.get_value(detail_result.json(), 'status.statuses.0')
            if value == 'Killed':
                break
            sleep(3)
        assert "删除pod后，验证状态出错：pod:{}没有删除成功{}".format(data['tapp_name'], detail_result.text)

    @pytest.mark.delete
    def 测试_删除tapp(self):
        data = self.tapp.data_list
        delete_result = self.tapp.delete_tapp_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])
        assert delete_result.status_code == 200, "删除{}tapp失败{}".format(data['tapp_name'], delete_result.text)
