import pytest
from new_case.containerplatform.tapp.tapp import TApp
from common import settings


@pytest.mark.archon
@pytest.mark.upgrade
@pytest.mark.tapp
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
@pytest.mark.skipif(not settings.get_featuregate_status(settings.REGION_NAME, 'tapp'), reason="tapp功能开关未开启，默认不跑对应用例")
class TestTAppSuiteGet(object):

    def setup_class(self):
        self.tapp = TApp()

    def 测试_tapp详情(self):
        data = self.tapp.data_list_notemplatePool
        detail_result = self.tapp.get_tapp_detail_jinja2(ns_name=data['namespace'], tapp_name=data['tapp_name'])
        assert detail_result.status_code == 200, "获取tapp详情失败:{}".format(detail_result.text)
        # http://jira.alauda.cn/browse/ACP-1367
        values = self.tapp.generate_jinja_data("verify_data/tapp/detail_tapp.jinja2", data)
        assert self.tapp.is_sub_dict(values, detail_result.json()), \
            "获取tapp详情比对数据失败，返回数据:{},期望数据:{}".format(detail_result.json(), values)

    def 测试_搜索tapp(self):
        data = self.tapp.data_list_notemplatePool
        ser = self.tapp.search_tapp_jinja2(ns_name=data["namespace"], limit=20, tapp_name=data["tapp_name"])
        assert ser.status_code == 200, "没有搜索到{}，{}".format(data["tapp_name"], ser.text)

        value = self.tapp.generate_jinja_data("./verify_data/tapp/search_tapp.jinja2", data)
        assert self.tapp.is_sub_dict(value, ser.json()), "搜索tapp比对数据失败，返回数据:{},期望数据:{}".format(ser.json(), value)

    def 测试_搜索tapp_v1(self):
        data = self.tapp.data_list_notemplatePool
        ser = self.tapp.search_tapp_jinja2_v1(ns_name=data["namespace"], limit=20, tapp_name=data["tapp_name"])
        assert ser.status_code == 200, "没有搜索到{}，{}".format(data["tapp_name"], ser.text)

        value = self.tapp.generate_jinja_data("./verify_data/tapp/search_tapp.jinja2", data)
        assert self.tapp.is_sub_dict(value, ser.json()), "搜索tapp比对数据失败，返回数据:{},期望数据:{}".format(ser.json(), value)

    def 测试_获取tapp列表_有limit参数(self):
        ret = self.tapp.get_tapp_list_jinja2(limit=20)
        assert ret.status_code == 200, "获取tapp列表失败:{}".format(ret.text)
        ret2 = self.tapp.get_tapp_list_jinja2(limit=1)
        net_num = len(ret2.json()["items"])
        assert net_num == 1, "获取tapp列表,limit=1时失败,预期返回1个tapp,实际返回{}个".format(net_num)

        global name
        name = TApp.get_value(ret2.json(), "items.0.metadata.name")

        global cnt
        cnt = TApp.get_value(ret2.json(), "metadata.continue")

    def 测试_获取tapp列表_有limit参数和continue参数(self):
        ret = self.tapp.get_tapp_list_jinja2(limit=1, cnt=cnt)
        assert ret.status_code == 200, "获取tapp列表_有limit和continue参数:{}".format(ret.text)
        name_continue = TApp.get_value(ret.json(), "items.0.metadata.name")
        assert name_continue != name, "分页数据相同，tapp第一页数据:{},tapp第二页数据:{}".format(name, name_continue)
