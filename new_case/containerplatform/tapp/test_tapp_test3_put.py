import pytest
from new_case.containerplatform.tapp.tapp import TApp
from common.settings import K8S_NAMESPACE, REGION_NAME, DEFAULT_LABEL
from common import settings


@pytest.mark.metis
@pytest.mark.archon
@pytest.mark.upgrade
@pytest.mark.tapp
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
@pytest.mark.skipif(not settings.get_featuregate_status(REGION_NAME, 'tapp'), reason="tapp功能开关未开启，默认不跑对应用例")
class TestTAppSuitePut(object):
    def setup_class(self):
        self.tapp = TApp()
        self.k8s_namespace = K8S_NAMESPACE
        self.region_name = REGION_NAME
        self.default_label = DEFAULT_LABEL

    def 测试_更新TApp_不使用templatePool(self):
        data = self.tapp.data_list_notemplatePool
        region_name = self.region_name
        ns_name = data["namespace"]
        tapp_name = data['tapp_name']
        data.update({"default_label": settings.DEFAULT_LABEL})
        detail_result = self.tapp.get_tapp_detail_jinja2(ns_name=data['namespace'], tapp_name=data['tapp_name'])
        assert detail_result.status_code == 200, "获取tapp详情失败:{}".format(detail_result.text)

        resourceversion = self.tapp.get_value(detail_result.json(), "metadata.resourceVersion")
        data.update({"value": "value1-a",
                     "metadata_resourceVersion": {"resourceVersion": ""}, "resourceversion": resourceversion})

        update_result = self.tapp.update_tapp_jinja2(data['tapp_name'], './test_data/tapp/tapp.jinja2', data=data)
        assert update_result.status_code == 200, "更新tapp:{}_必填参数失败{}".format(data['tapp_name'], update_result.text)

        value = self.tapp.generate_jinja_data("./verify_data/tapp/create_tapp.jinja2", data)
        assert self.tapp.is_sub_dict(value, update_result.json()), \
            "更新TApp_必填参数比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

        ret = self.tapp.get_status("kubernetes/{}/apis/apps.tkestack.io/v1/namespaces/{}/tapps/{}"
                                   .format(region_name, ns_name, tapp_name), "status.appStatus", expect_value='Running')
        assert ret, "更新tapp后，验证tapp状态出错: tapp：{} is not running".format(data["tapp_name"])

    def 测试_更新TApp_不使用templatePool_更新资源限制_patch(self):
        data = self.tapp.data_list_notemplatePool
        region_name = self.region_name
        ns_name = data["namespace"]
        tapp_name = data['tapp_name']
        data.update({"default_label": settings.DEFAULT_LABEL, "cpu": "51m", "memory": "52m"})

        update_result = self.tapp.update_tapp_jinja2_patch(data['tapp_name'],
                                                           './test_data/tapp/tapp_update_patch.jinja2', data=data)
        assert update_result.status_code == 200, "更新tapp:{}资源限制失败{}".format(data['tapp_name'], update_result.text)

        value = self.tapp.generate_jinja_data("./verify_data/tapp/update_tapp_patch.jinja2", data)
        assert self.tapp.is_sub_dict(value, update_result.json()), \
            "更新TApp资源限制比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

        ret = self.tapp.get_status("kubernetes/{}/apis/apps.tkestack.io/v1/namespaces/{}/tapps/{}".
                                   format(region_name, ns_name, tapp_name), "status.appStatus", expect_value='Running')
        assert ret, "更新tapp后，验证tapp状态出错: tapp：{} is not running".format(data["tapp_name"])

    @pytest.mark.skipif(settings.AUDIT_UNABLED, reason="do not have audit")
    def 测试Tapp更新审计(self):
        payload = {"user_name": settings.USERNAME, "operation_type": "update", "resource_type": "tapps",
                   "resource_name": self.tapp.data_list_notemplatePool['tapp_name']}
        result = self.tapp.search_audit(payload)
        payload.update({"namespace": self.tapp.data_list_notemplatePool["namespace"],
                        "region_name": settings.REGION_NAME})
        values = self.tapp.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.tapp.is_sub_dict(values, result.json()), "审计数据不符合预期"

    def 测试_更新TApp_必填参数_可选参数有三个_节点异常策略和更新策略和组件标签(self):
        data = self.tapp.data_list_force_strategy_labels
        region_name = self.region_name
        ns_name = data["namespace"]
        tapp_name = data['tapp_name']
        data.update({"default_label": settings.DEFAULT_LABEL})

        detail_result = self.tapp.get_tapp_detail_jinja2(ns_name=data['namespace'], tapp_name=data['tapp_name'])
        assert detail_result.status_code == 200, "获取tapp详情失败:{}".format(detail_result.text)
        resourceversion = self.tapp.get_value(detail_result.json(), "metadata.resourceVersion")

        data.update({"forcedeletepod": "false", "metadata_resourceVersion": {"resourceVersion": ""},
                     "resourceversion": resourceversion})

        update_result = self.tapp.update_tapp_jinja2(data['tapp_name'], './test_data/tapp/tapp.jinja2', data=data)
        assert update_result.status_code == 200, "更新tapp:{}_必填参数_可选参数有三个_节点异常策略和更新策略和组件标签失败{}"\
            .format(data['tapp_name'], update_result.text)

        value = self.tapp.generate_jinja_data("./verify_data/tapp/create_tapp.jinja2", data)
        assert self.tapp.is_sub_dict(value, update_result.json()), \
            "更新TApp_必填参数_可选参数有三个_节点异常策略和更新策略和组件标签比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

        ret = self.tapp.get_status(
            "kubernetes/{}/apis/apps.tkestack.io/v1/namespaces/{}/tapps/{}"
            .format(region_name, ns_name, tapp_name), "status.appStatus", expect_value='Running')
        assert ret, "更新tapp后，验证tapp状态出错: tapp：{} is not running".format(data["tapp_name"])

    def 测试_更新TApp_必填参数_可选参数有三个_启动命令和参数和健康检查(self):
        data = self.tapp.data_list_args_livenessprobe
        region_name = self.region_name
        ns_name = data["namespace"]
        tapp_name = data['tapp_name']
        data.update({"default_label": settings.DEFAULT_LABEL})

        detail_result = self.tapp.get_tapp_detail_jinja2(ns_name=data['namespace'], tapp_name=data['tapp_name'])
        assert detail_result.status_code == 200, "获取tapp详情失败:{}".format(detail_result.text)
        resourceversion = self.tapp.get_value(detail_result.json(), "metadata.resourceVersion")

        data.update({"args_2": "while true; do sleep 30; echo 456; done",
                     "metadata_resourceVersion": {"resourceVersion": ""}, "resourceversion": resourceversion})

        update_result = self.tapp.update_tapp_jinja2(data['tapp_name'], './test_data/tapp/tapp.jinja2', data=data)
        assert update_result.status_code == 200, "更新tapp:{}_必填参数_可选参数有三个_启动命令和参数和健康检查{}" \
            .format(data['tapp_name'], update_result.text)

        value = self.tapp.generate_jinja_data("./verify_data/tapp/create_tapp.jinja2", data)
        assert self.tapp.is_sub_dict(value, update_result.json()), \
            "更新TApp_必填参数_可选参数有三个_启动命令和参数和健康检查比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

        ret = self.tapp.get_status("kubernetes/{}/apis/apps.tkestack.io/v1/namespaces/{}/tapps/{}"
                                   .format(region_name, ns_name, tapp_name), "status.appStatus", expect_value='Running')
        assert ret, "更新tapp后，验证tapp状态出错: tapp：{} is not running".format(data["tapp_name"])

    def 测试_更新TApp_必填参数_可选端口(self):
        data = self.tapp.data_list_port
        region_name = self.region_name
        ns_name = data["namespace"]
        tapp_name = data['tapp_name']
        data.update({"default_label": settings.DEFAULT_LABEL})

        detail_result = self.tapp.get_tapp_detail_jinja2(ns_name=data['namespace'], tapp_name=data['tapp_name'])
        assert detail_result.status_code == 200, "获取tapp详情失败:{}".format(detail_result.text)
        resourceversion = self.tapp.get_value(detail_result.json(), "metadata.resourceVersion")

        data.update({"protocol": "UDP", "metadata_resourceVersion": {"resourceVersion": ""},
                     "resourceversion": resourceversion})

        update_result = self.tapp.update_tapp_jinja2(data['tapp_name'], './test_data/tapp/tapp.jinja2', data=data)
        assert update_result.status_code == 200, "更新tapp:{}_必填参数_可选端口{}" .format(data['tapp_name'], update_result.text)

        value = self.tapp.generate_jinja_data("./verify_data/tapp/create_tapp.jinja2", data)
        assert self.tapp.is_sub_dict(value, update_result.json()), "更新TApp_必填参数_可选端口，返回数据:{},期望数据:{}"\
            .format(update_result.json(), value)

        ret = self.tapp.get_status("kubernetes/{}/apis/apps.tkestack.io/v1/namespaces/{}/tapps/{}"
                                   .format(region_name, ns_name, tapp_name), "status.appStatus", expect_value='Running')
        assert ret, "更新tapp后，验证tapp状态出错: tapp：{} is not running".format(data["tapp_name"])
