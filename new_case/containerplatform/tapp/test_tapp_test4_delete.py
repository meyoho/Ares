import pytest
from new_case.containerplatform.tapp.tapp import TApp
from common import settings


@pytest.mark.archon
@pytest.mark.delete
@pytest.mark.metis
@pytest.mark.tapp
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
@pytest.mark.skipif(not settings.get_featuregate_status(settings.REGION_NAME, 'tapp'), reason="tapp功能开关未开启，默认不跑对应用例")
class TestTAppSuiteDelete(object):
    def setup_class(self):
        self.tapp = TApp()

    def 测试_删除TApp_不使用templatePool(self):
        data = self.tapp.data_list_notemplatePool
        delete_result = self.tapp.delete_tapp_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])
        assert delete_result.status_code == 200, "删除{}tapp失败{}".format(data['tapp_name'], delete_result.text)

    @pytest.mark.skipif(settings.AUDIT_UNABLED, reason="do not have audit")
    def 测试Tapp删除审计(self):
        payload = {"user_name": settings.USERNAME, "operation_type": "delete", "resource_type": "tapps",
                   "resource_name": self.tapp.data_list_notemplatePool['tapp_name']}
        result = self.tapp.search_audit(payload)
        payload.update({"namespace": self.tapp.data_list["namespace"], "region_name": settings.REGION_NAME})
        values = self.tapp.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.tapp.is_sub_dict(values, result.json()), "审计数据不符合预期"

    def 测试_删除TApp_必填参数_可选参数有三个_节点异常策略和更新策略和组件标签(self):
        data = self.tapp.data_list_force_strategy_labels
        delete_result = self.tapp.delete_tapp_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])
        assert delete_result.status_code == 200, "删除{}tapp失败{}".format(data['tapp_name'], delete_result.text)

    def 测试_删除TApp_必填参数_可选参数有三个_启动命令和参数和健康检查(self):
        data = self.tapp.data_list_args_livenessprobe
        delete_result = self.tapp.delete_tapp_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])
        assert delete_result.status_code == 200, "删除{}tapp失败{}".format(data['tapp_name'], delete_result.text)

    def 测试_删除TApp_必填参数_可选参数有两个_有一个配置引用_configmap和环境变量(self):
        data = self.tapp.data_list_configmap_evn
        delete_result = self.tapp.delete_tapp_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])
        assert delete_result.status_code == 200, "删除{}tapp失败{}".format(data['tapp_name'], delete_result.text)

    def 测试_删除TApp_必填参数_可选参数有两个_网络模式和配置引用_secret(self):
        data = self.tapp.data_list_hostnetwork_secret
        delete_result = self.tapp.delete_tapp_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])
        assert delete_result.status_code == 200, "删除{}tapp失败{}".format(data['tapp_name'], delete_result.text)

    def 测试_删除TApp_必填参数_可选端口(self):
        data = self.tapp.data_list_port
        delete_result = self.tapp.delete_tapp_jinja2(ns_name=data["namespace"], tapp_name=data['tapp_name'])
        assert delete_result.status_code == 200, "删除{}tapp失败{}".format(data['tapp_name'], delete_result.text)
