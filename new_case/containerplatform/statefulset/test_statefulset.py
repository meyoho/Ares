import pytest
from new_case.containerplatform.statefulset.statefulset import Statefulset
from common import settings
from common.settings import K8S_NAMESPACE


@pytest.mark.archon
@pytest.mark.BAT
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestStatefulsetSuite(object):

    def setup_class(self):
        self.statefulset = Statefulset()
        self.k8s_namespace = K8S_NAMESPACE

    @pytest.mark.metis
    @pytest.mark.prepare
    def 测试_创建Statefulset(self):
        data = self.statefulset.data_list
        self.statefulset.delete_statefulset(ns_name=data['namespace'], statefulset_name=data['statefulset_name'])

        create_result = self.statefulset.create_statefulset_jinja2("./test_data/statefulset/statefulset.jinja2",
                                                                   data=data)
        assert create_result.status_code == 201, "创建{}有状态副本集失败{}".format(data['statefulset_name'], create_result.text)

        detail_result = self.statefulset.detail_statefulset_jinja2(ns_name=data['namespace'],
                                                                   statefulset_name=data['statefulset_name'])
        assert detail_result.status_code == 200, "获取有状态副本集{}详情页失败,{}".format(data['statefulset_name'],
                                                                             detail_result.text)

        statefulset_status = self.statefulset.get_statefulset_status(ns_name=data['namespace'],
                                                                     statefulset_name=data["statefulset_name"])
        assert statefulset_status, "创建部署后，验证有状态副本集状态出错: 状态副本集：{} is not running".format(data["statefulset_name"])

        value = self.statefulset.generate_jinja_data("./verify_data/statefulset/create_statefulset.jinja2", data)
        assert self.statefulset.is_sub_dict(value,
                                            create_result.json()), "创建statefulset_必填参数比对数据失败，返回数据:{},期望数据:{}".format(
            create_result.json(), value)

    @pytest.mark.upgrade
    def 测试_获取statefulset列表(self):
        data = self.statefulset.data_list
        ret = self.statefulset.get_statefulset_list(limit=20)
        assert ret.status_code == 200, "获取有状态副本集列表失败:{}".format(ret.text)
        value = self.statefulset.generate_jinja_data("./verify_data/statefulset/list_statefulset.jinja2", data)
        assert self.statefulset.is_sub_dict(value, ret.json()), "获取statefulset列表比对数据失败，返回数据:{},期望数据:{}" \
            .format(ret.json(), value)

    @pytest.mark.metis
    @pytest.mark.upgrade
    def 测试_获取statefulset容器组(self):
        data = self.statefulset.data_list
        ret = self.statefulset.get_statefulset_pods(ns_name=data['namespace'],
                                                    statefulset_name=data['statefulset_name'])
        assert ret.status_code == 200, "获取容器组失败:{}".format(ret.text)
        global pod_name
        pod_name = self.statefulset.get_value(ret.json(), 'items.0.metadata.name')
        global container_name
        container_name = self.statefulset.get_value(ret.json(), 'items.0.spec.containers.0.name')
        data.update({'pod_name': pod_name})
        value = self.statefulset.generate_jinja_data("./verify_data/statefulset/list_pod.jinja2", data)
        assert self.statefulset.is_sub_dict(value, ret.json()), "获取容器组比对数据失败，返回数据:{},期望数据:{}".format(ret.json(), value)

    @pytest.mark.upgrade
    def 测试_获取日志(self):
        data = self.statefulset.data_list
        ret = self.statefulset.get_statefulset_logs(self.k8s_namespace, pod_name, container_name)
        assert ret.status_code == 200, "获取日志失败:{}".format(ret.text)
        data.update({'pod_name': pod_name, 'container_name': container_name})
        values = self.statefulset.generate_jinja_data("verify_data/statefulset/log_statefulset.jinja2", data)
        assert self.statefulset.is_sub_dict(values, ret.json()), "获取日志比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)
        assert len(self.statefulset.get_value(ret.json(), 'logs')) != 0, "获取日志为空 {}".format(ret.json())

    @pytest.mark.upgrade
    def 测试_获取告警列表(self):
        data = self.statefulset.data_list
        ret = self.statefulset.get_statefulset_alarm(ns_name=data['namespace'],
                                                     statefulset_name=data['statefulset_name'])
        assert ret.status_code == 200, "获取告警列表失败:{}".format(ret.text)
        value = self.statefulset.generate_jinja_data("verify_data/statefulset/statefulset_alarm.jinja2", data)
        assert self.statefulset.is_sub_dict(value, ret.json()), "获取告警列表比对数据失败，返回数据:{},期望数据:{}" \
            .format(ret.json(), value)

    @pytest.mark.metis
    @pytest.mark.upgrade
    def 测试_容器重建(self):
        data = self.statefulset.data_list
        ret = self.statefulset.delete_pod(self.k8s_namespace, pod_name)
        assert ret.status_code == 200, "容器重建失败:{}".format(ret.text)
        data.update({'pod_name': pod_name})
        values = self.statefulset.generate_jinja_data("verify_data/statefulset/delete_pod_statefulset.jinja2", data)
        assert self.statefulset.is_sub_dict(values, ret.json()), \
            "重建容器比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)
        detail_result = self.statefulset.detail_statefulset_jinja2(ns_name=data['namespace'],
                                                                   statefulset_name=data['statefulset_name'])
        assert detail_result.status_code == 200, "获取有状态副本集{}详情页失败,{}".format(data['statefulset_name'],
                                                                             detail_result.text)

        statefulset_status = self.statefulset.get_statefulset_status(ns_name=data['namespace'],
                                                                     statefulset_name=data["statefulset_name"])
        assert statefulset_status, "创建有状态副本集后，验证有状态副本集状态出错: 有状态副本集：{} is not running".format(data["statefulset_name"])

    @pytest.mark.metis
    @pytest.mark.upgrade
    def 测试_停止statefulset(self):
        data = self.statefulset.data_list
        data.update({'replicas': 0})
        ret = self.statefulset.stop_start_statefulset(data['namespace'], data['statefulset_name'],
                                                      "./test_data/statefulset/statefulset.jinja2", data=data)
        assert ret.status_code == 200, "停止statefulset失败:{}".format(ret.text)

        detail_result = self.statefulset.detail_statefulset_jinja2(ns_name=data['namespace'],
                                                                   statefulset_name=data['statefulset_name'])
        assert detail_result.status_code == 200, "获取有状态副本集{}详情页失败,{}".format(data['statefulset_name'],
                                                                             detail_result.text)
        value = self.statefulset.get_value(detail_result.json(), 'spec.replicas')
        assert value == 0, "停止有状态副本集后，验证状态出错：有状态副本集{}不是停止状态".format(data['statefulset_name'])

    @pytest.mark.metis
    @pytest.mark.upgrade
    def 测试_开始statefulset(self):
        data = self.statefulset.data_list
        data.update({'replicas': 1})
        ret = self.statefulset.stop_start_statefulset(data['namespace'], data['statefulset_name'],
                                                      "./test_data/statefulset/statefulset.jinja2", data=data)
        assert ret.status_code == 200, "开始statefulset失败:{}".format(ret.text)

        detail_result = self.statefulset.detail_statefulset_jinja2(ns_name=data['namespace'],
                                                                   statefulset_name=data['statefulset_name'])
        assert detail_result.status_code == 200, "获取有状态副本集{}详情页失败,{}".format(data['statefulset_name'],
                                                                             detail_result.text)

        statefulset_status = self.statefulset.get_statefulset_status(ns_name=data['namespace'],
                                                                     statefulset_name=data["statefulset_name"])
        assert statefulset_status, "开始有状态副本集后，验证有状态副本集状态出错: 有状态副本集：{} is not running".format(data["statefulset_name"])

    @pytest.mark.metis
    @pytest.mark.upgrade
    def 测试_更新statefulset(self):
        data = self.statefulset.data_list
        data.update({"memory": "51Mi"})
        update_result = self.statefulset.update_statefulset_jinja2(data['namespace'], data['statefulset_name'],
                                                                   "./test_data/statefulset/statefulset.jinja2",
                                                                   data=data)
        assert update_result.status_code == 200, "更新有状态副本集{}失败:{}".format(data['statefulset_name'], update_result.text)

        value = self.statefulset.generate_jinja_data("./verify_data/statefulset/create_statefulset.jinja2", data)
        assert self.statefulset.is_sub_dict(value, update_result.json()), \
            "更新statefulset_必填参数比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

        detail_result = self.statefulset.detail_statefulset_jinja2(ns_name=data['namespace'],
                                                                   statefulset_name=data['statefulset_name'])
        assert detail_result.status_code == 200, "获取有状态副本集{}详情页失败,{}".format(data['statefulset_name'],
                                                                             detail_result.text)

        statefulset_status = self.statefulset.get_statefulset_status(ns_name=data['namespace'],
                                                                     statefulset_name=data["statefulset_name"])
        assert statefulset_status, "更新有状态副本集，验证有状态副本集状态出错: 有状态副本集：{} is not running".format(data["statefulset_name"])

    @pytest.mark.upgrade
    def 测试_statefulset详情(self):
        data = self.statefulset.data_list
        detail_result = self.statefulset.detail_statefulset_jinja2(ns_name=data['namespace'],
                                                                   statefulset_name=data['statefulset_name'])
        assert detail_result.status_code == 200, "获取部署{}详情页失败,{}".format(data['statefulset_name'], detail_result.text)

        value = self.statefulset.generate_jinja_data("./verify_data/statefulset/create_statefulset.jinja2", data)
        assert self.statefulset.is_sub_dict(value, detail_result.json()), \
            "statefulset详情比对数据失败，返回数据:{},期望数据:{}".format(detail_result.json(), value)

    @pytest.mark.delete
    def 测试_删除Statefulset(self):
        data = self.statefulset.data_list
        delete_result = self.statefulset.delete_statefulset(ns_name=data['namespace'],
                                                            statefulset_name=data['statefulset_name'])
        assert delete_result.status_code == 200, "删除部有状态副本集{}失败:{}".format(data['statefulset_name'], delete_result.text)
