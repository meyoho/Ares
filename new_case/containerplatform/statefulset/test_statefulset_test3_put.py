import pytest
from new_case.containerplatform.statefulset.statefulset import Statefulset
from common import settings
from new_case.operation.log.log import Log


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestStatefulsetSuitePut(object):

    def setup_class(self):
        self.statefulset = Statefulset()
        self.log = Log()

    @pytest.mark.metis
    def 测试_更新Statefulset_必填参数_可选参数有两个_更新策略和组件标签(self):
        data = self.statefulset.data_list_strategy_labels
        data.update({"maxsurge": "40%"})
        data.update({"metadata_labels": {"key1-a": "value1", "key2-b": "value2"}})
        update_result = self.statefulset.update_statefulset_jinja2(data['namespace'], data['statefulset_name'],
                                                                   "./test_data/statefulset/statefulset.jinja2", data=data)
        assert update_result.status_code == 200, "更新有状态副本集{}失败:{}".format(data['statefulset_name'], update_result.text)

        detail_result = self.statefulset.detail_statefulset_jinja2(ns_name=data['namespace'], statefulset_name=data['statefulset_name'])
        assert detail_result.status_code == 200, "获取有状态副本集{}详情页失败,{}".format(data['statefulset_name'], detail_result.text)

        statefulset_status = self.statefulset.get_statefulset_status(ns_name=data['namespace'], statefulset_name=data["statefulset_name"])
        assert statefulset_status, "更新有状态副本集后，验证有状态副本集状态出错: 部署：{} is not running".format(data["statefulset_name"])

        value = self.statefulset.generate_jinja_data("./verify_data/statefulset/create_statefulset.jinja2", data)
        assert self.statefulset.is_sub_dict(value, update_result.json()), \
            "更新Statefulset_必填参数_可选参数有两个_更新策略和组件标签比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

    @pytest.mark.metis
    def 测试_更新Statefulset_必填参数_可选参数有三个_启动命令和参数和健康检查(self):
        data = self.statefulset.data_list_args_livenessprobe
        data.update({"args_2": "while true; do sleep 30; echo 456; done"})
        update_result = self.statefulset.update_statefulset_jinja2(data['namespace'], data['statefulset_name'],
                                                                   "./test_data/statefulset/statefulset.jinja2", data=data)
        assert update_result.status_code == 200, "更新有状态副本集{}失败:{}".format(data['statefulset_name'], update_result.text)

        detail_result = self.statefulset.detail_statefulset_jinja2(ns_name=data['namespace'],
                                                                   statefulset_name=data['statefulset_name'])
        assert detail_result.status_code == 200, "获取有状态副本集{}详情页失败,{}".format(data['statefulset_name'], detail_result.text)

        statefulset_status = self.statefulset.get_statefulset_status(ns_name=data['namespace'],
                                                                     statefulset_name=data["statefulset_name"])
        assert statefulset_status, "更新有状态副本集后，验证有状态副本集状态出错: 有状态副本集：{} is not running".format(data["statefulset_name"])

        value = self.statefulset.generate_jinja_data("./verify_data/statefulset/create_statefulset.jinja2", data)
        assert self.statefulset.is_sub_dict(value, update_result.json()), \
            "更新Statefulset_必填参数_可选参数有三个_启动命令和参数和健康检查比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

    @pytest.mark.metis
    def 测试_更新Statefulset_必填参数_可选端口(self):
        data = self.statefulset.data_list_port
        data.update({"protocol": "UDP"})
        update_result = self.statefulset.update_statefulset_jinja2(data['namespace'], data['statefulset_name'],
                                                                   "./test_data/statefulset/statefulset.jinja2",
                                                                   data=data)
        assert update_result.status_code == 200, "更新有状态副本集{}失败:{}".format(data['statefulset_name'], update_result.text)

        detail_result = self.statefulset.detail_statefulset_jinja2(ns_name=data['namespace'],
                                                                   statefulset_name=data['statefulset_name'])
        assert detail_result.status_code == 200, "获取有状态副本集{}详情页失败,{}".format(data['statefulset_name'],
                                                                             detail_result.text)

        statefulset_status = self.statefulset.get_statefulset_status(ns_name=data['namespace'],
                                                                     statefulset_name=data["statefulset_name"])
        assert statefulset_status, "更新有状态副本集后，验证有状态副本集状态出错: 有状态副本集：{} is not running".format(data["statefulset_name"])

        value = self.statefulset.generate_jinja_data("./verify_data/statefulset/create_statefulset.jinja2", data)
        assert self.statefulset.is_sub_dict(value, update_result.json()), \
            "更新Statefulset_必填参数_可选参数有三个_启动命令和参数和健康检查比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

    def 测试更新statefulset事件(self):
        payload = {"cluster": settings.REGION_NAME,
                   "kind": "StatefulSet",
                   "name": self.statefulset.data_list_args_livenessprobe["statefulset_name"], "namespace": settings.K8S_NAMESPACE, "page": 1, "page_size": 20}
        kevents_results = self.statefulset.get_kevents(payload=payload, expect_value="SuccessfulCreate", times=1)
        assert kevents_results, "更新statefulset获取事件获取失败"
