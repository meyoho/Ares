import pytest
from new_case.containerplatform.statefulset.statefulset import Statefulset
from common import settings
from new_case.containerplatform.persistentvolumeclaims.pvc import Pvc


@pytest.mark.archon
@pytest.mark.metis
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestStatefulsetSuiteDelete(object):

    def setup_class(self):
        self.statefulset = Statefulset()

        self.pvc = Pvc()
        self.pvcname = '{}-ares-deployment-pvc'.format(settings.RESOURCE_PREFIX).replace('_', '-')

    def 测试_删除Statefulset_必填参数_可选参数有两个_更新策略和组件标签(self):
        data = self.statefulset.data_list_strategy_labels
        delete_result = self.statefulset.delete_statefulset(ns_name=data['namespace'],
                                                            statefulset_name=data['statefulset_name'])
        assert delete_result.status_code == 200, "删除有状态副本集{}失败:{}".format(data['statefulset_name'], delete_result.text)

    def 测试_删除Statefulset_必填参数_可选参数有三个_启动命令和参数和健康检查(self):
        data = self.statefulset.data_list_args_livenessprobe
        delete_result = self.statefulset.delete_statefulset(ns_name=data['namespace'],
                                                            statefulset_name=data['statefulset_name'])
        assert delete_result.status_code == 200, "删除有状态副本集{}失败:{}".format(data['statefulset_name'], delete_result.text)

    def 测试_删除Statefulset_必填参数_可选参数有一个配置引用_configmap(self):
        data = self.statefulset.data_list_configmap
        delete_result = self.statefulset.delete_statefulset(ns_name=data['namespace'],
                                                            statefulset_name=data['statefulset_name'])
        assert delete_result.status_code == 200, "删除有状态副本集{}失败:{}".format(data['statefulset_name'], delete_result.text)

    def 测试_删除Statefulset_必填参数_可选参数有两个_网络模式和配置引用_secret(self):
        data = self.statefulset.data_list_hostnetwork_secret
        delete_result = self.statefulset.delete_statefulset(ns_name=data['namespace'],
                                                            statefulset_name=data['statefulset_name'])
        assert delete_result.status_code == 200, "删除有状态副本集{}失败:{}".format(data['statefulset_name'], delete_result.text)

    def 测试_删除Statefulset_必填参数_可选参数亲和性(self):
        data_affinify_base = self.statefulset.data_list_affinify_base
        delete_result = self.statefulset.delete_statefulset(ns_name=data_affinify_base['namespace'],
                                                            statefulset_name=data_affinify_base['statefulset_name'])
        assert delete_result.status_code == 200, "删除有状态副本集{}失败:{}".format(data_affinify_base['statefulset_name'],
                                                                          delete_result.text)

        data_affinify_app_name = self.statefulset.data_list_affinify_app_name
        delete_result = self.statefulset.delete_statefulset(ns_name=data_affinify_app_name['namespace'],
                                                            statefulset_name=data_affinify_app_name['statefulset_name'])
        assert delete_result.status_code == 200, "删除有状态副本集{}失败:{}".format(data_affinify_app_name['statefulset_name'],
                                                                          delete_result.text)

        data_antiaffinify_app_name = self.statefulset.data_list_antiaffinify_app_name
        delete_result = self.statefulset.delete_statefulset(ns_name=data_antiaffinify_app_name['namespace'],
                                                            statefulset_name=data_antiaffinify_app_name[
                                                                'statefulset_name'])
        assert delete_result.status_code == 200, "删除有状态副本集{}失败:{}".format(
            data_antiaffinify_app_name['statefulset_name'],
            delete_result.text)

    def 测试_删除Statefulset_必填参数_可选端口(self):
        data = self.statefulset.data_list_port
        delete_result = self.statefulset.delete_statefulset(ns_name=data['namespace'],
                                                            statefulset_name=data['statefulset_name'])
        assert delete_result.status_code == 200, "删除有状态副本集{}失败:{}".format(data['statefulset_name'], delete_result.text)
