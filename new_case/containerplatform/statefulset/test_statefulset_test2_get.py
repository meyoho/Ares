import pytest
from new_case.containerplatform.statefulset.statefulset import Statefulset
from common import settings


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestStatefulsetSuiteGet(object):
    def setup_class(self):
        self.statefulset = Statefulset()

    def 测试_获取有状态副本集列表_有limit参数(self):
        ret = self.statefulset.get_statefulset_list(limit=20)
        assert ret.status_code == 200, "获取有状态副本集列表失败:{}".format(ret.text)
        ret2 = self.statefulset.get_statefulset_list(limit=1)
        statefulset_num = len(ret2.json()["items"])
        assert statefulset_num == 1, "获取有状态副本集列表,limit=1时失败,预期返回1个部署,实际返回{}个".format(statefulset_num)

        global name
        name = Statefulset.get_value(ret2.json(), "items.0.metadata.name")

        global cnt
        cnt = Statefulset.get_value(ret2.json(), "metadata.continue")

    def 测试_获取有状态副本集列表_有limit参数和continue参数(self):
        ret = self.statefulset.get_statefulset_list(limit=1, cnt=cnt)
        assert ret.status_code == 200, "获取有状态副本集列表_有limit和continue参数:{}".format(ret.text)
        name_continue = Statefulset.get_value(ret.json(), "items.0.metadata.name")
        assert name_continue != name, "分页数据相同，有状态副本集第一页数据:{},有状态副本集第二页数据:{}".format(name, name_continue)

    def 测试_搜索有状态副本集(self):
        data = self.statefulset.data_list_strategy_labels
        ser = self.statefulset.search_statefulset(ns_name=data['namespace'], statefulset_name=data['statefulset_name'])
        assert ser.status_code == 200, "没有搜索到{}，{}".format(data["statefulset_name"], ser.text)

        value = self.statefulset.generate_jinja_data("./verify_data/statefulset/search_statefulset.jinja2", data)
        assert self.statefulset.is_sub_dict(value, ser.json()), "搜索有状态副本集比对数据失败，返回数据:{},期望数据:{}".format(ser.json(), value)
