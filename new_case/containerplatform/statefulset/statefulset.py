import sys
from common.base_request import Common
from common.log import logger
from common import settings
from time import sleep
import random


class Statefulset(Common):
    def __init__(self):
        super(Statefulset, self).__init__()
        self.genetate_global_info()

    def get_common_statefulset_url(self, ns_name=settings.K8S_NAMESPACE, statefulset_name=''):
        return 'kubernetes/{}/apis/apps/v1/namespaces/{}/statefulsets/{}'.format(self.region_name, ns_name, statefulset_name)

    def get_common_statefulset_list_url(self, ns_name=settings.K8S_NAMESPACE, limit=1, cnt=""):
        return limit and "kubernetes/{}/apis/apps/v1/namespaces/{}/statefulsets?limit={}&continue={}" \
            .format(self.region_name, ns_name, limit, cnt)

    def get_statefulset_pod_url(self, ns_name=settings.K8S_NAMESPACE, limit=1, statefulset_name=''):
        return limit and "kubernetes/{}/api/v1/namespaces/{}/pods?limit={}&labelSelector=app={}" \
            .format(self.region_name, ns_name, limit, statefulset_name)

    def get_common_statefulset_search_url(self, ns_name=settings.K8S_NAMESPACE, limit=1, cnt="", statefulset_name=''):
        return limit and "acp/v1/resources/search/kubernetes/{}/apis/apps/v1/namespaces/{}/statefulsets?" \
                         "limit={}&continue={}&keyword={}&field=metadata.name".format(self.region_name, ns_name, limit, cnt, statefulset_name)

    def create_statefulset_jinja2(self, file, data):
        path = self.get_common_statefulset_url()
        data = self.generate_jinja_data(file, data)
        logger.info(data)
        return self.send(method='post', path=path, json=data, params={})

    def detail_statefulset_jinja2(self, ns_name, statefulset_name):
        path = self.get_common_statefulset_url(ns_name=ns_name, statefulset_name=statefulset_name)
        return self.send(method='get', path=path, params={})

    def search_statefulset(self, ns_name, statefulset_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_statefulset_search_url(ns_name=ns_name, statefulset_name=statefulset_name)
        return self.send(method='get', path=url)

    # 状态判断链接文档：http://confluence.alauda.cn/pages/viewpage.action?pageId=28508749
    def get_statefulset_status(self, ns_name, statefulset_name, key_spec_replicas="spec.replicas",
                               key_status_replicas="status.replicas", key_status_readyreplicas="status.readyReplicas",
                               key_status_observedgeneration="status.observedGeneration", key_metadata_generation="metadata.generation",
                               key_status_current_revision="status.currentRevision", key_status_current_replicas="status.currentReplicas",
                               key_status_updatedrevision="status.updateRevision"):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        cnt = 0
        flag = False
        while cnt < 60 and not flag:
            cnt += 1
            sleep(3)
            url = "kubernetes/{}/apis/apps/v1/namespaces/{}/statefulsets/{}".format(self.region_name, ns_name, statefulset_name)
            response = self.send(method="GET", path=url)
            assert response.status_code == 200, "get statefulset status failed"

            value_spec_replicas = self.get_value(response.json(), key_spec_replicas)
            if 'currentReplicas' in response.text and "currentRevision" in response.text and "updateRevision" in response.text:
                current_revision = self.get_value(response.json(), key_status_current_revision)
                current_replicas = self.get_value(response.json(), key_status_current_replicas)
                value_status_updatedrevision = self.get_value(response.json(), key_status_updatedrevision)
            else:
                current_revision = 0
                current_replicas = 0
                value_status_updatedrevision = 0
            value_status_replicas = self.get_value(response.json(), key_status_replicas)
            if 'readyReplicas' in response.text:
                value_status_readyreplicas = self.get_value(response.json(), key_status_readyreplicas)
            else:
                value_status_readyreplicas = 0
            if 'observedGeneration' in response.text:
                value_status_observedgeneration = self.get_value(response.json(), key_status_observedgeneration)
            else:
                value_status_observedgeneration = 0
            value_metadata_generation = self.get_value(response.json(), key_metadata_generation)
            if value_spec_replicas > 0 and value_status_replicas == value_spec_replicas and \
                    value_status_readyreplicas == value_spec_replicas and \
                    value_status_observedgeneration >= value_metadata_generation and \
                    value_status_updatedrevision == current_revision and current_replicas == value_spec_replicas:
                flag = True
                break
        return flag

    def get_statefulset_list(self, limit=None, cnt=""):
        path = self.get_common_statefulset_list_url(limit=limit, cnt=cnt)
        return self.send(method='get', path=path, params={})

    def get_statefulset_pods(self, ns_name, statefulset_name, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_statefulset_pod_url(ns_name=ns_name, statefulset_name=statefulset_name)
        return self.send(method='get', path=url, auth=auth)

    def get_statefulset_logs(self, ns_name, pod_name, container_name):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        url = "acp/v1/kubernetes/{}/namespaces/{}/pods/{}/{}/log?previous=false&logFilePosition=end" \
              "&referenceTimestamp=newest&offsetFrom=2000000000&offsetTo=2000000100". \
            format(self.region_name, ns_name, pod_name, container_name)
        return self.send(method="get", path=url)

    def delete_pod(self, ns_name, pod_name):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        url = "kubernetes/{}/api/v1/namespaces/{}/pods/{}".format(self.region_name, ns_name, pod_name)
        return self.send(method="delete", path=url)

    def stop_start_statefulset(self, ns_name, statefulset_name, file, data):
        path = self.get_common_statefulset_url(ns_name=ns_name, statefulset_name=statefulset_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='put', path=path, json=data, params={})

    def update_statefulset_jinja2(self, ns_name, statefulset_name, file, data):
        path = self.get_common_statefulset_url(ns_name=ns_name, statefulset_name=statefulset_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='put', path=path, json=data, params={})

    def delete_statefulset(self, ns_name, statefulset_name, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_statefulset_url(ns_name, statefulset_name)
        return self.send(method='delete', path=url, auth=auth)

    def get_statefulset_alarm(self, ns_name, statefulset_name):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        url = "kubernetes/{}/apis/monitoring.coreos.com/v1/namespaces/{}/prometheusrules?" \
              "limit=20&labelSelector=alert.alauda.io/owner=System,alert.alauda.io/namespace={}," \
              "alert.alauda.io/kind=Statefulset,alert.alauda.io/name={}".format(self.region_name, ns_name,
                                                                                ns_name, statefulset_name)
        return self.send(method="get", path=url)

    # 创建Statefulset_必填参数
    data_list = {
        "statefulset_name": '{}-statefulset'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME
    }

    # 创建Statefulset_必填参数_可选参数有两个_更新策略和组件标签
    data_list_strategy_labels = {
        "statefulset_name": '{}-statefulset-strategy-labels'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME,
        "maxsurge": "50%",
        "maxunavailable": "20%",
        "metadata_labels": {"key1": "value1", "key2": "value2"}
    }

    # 创建Statefulset_必填参数_可选参数有三个_启动命令和参数和健康检查
    data_list_args_livenessprobe = {
        "statefulset_name": '{}-statefulset-strategy-args-livenessprobe'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME,
        "args": " ",
        "args_1": "-c",
        "args_2": "while true; do sleep 30; echo 123; done",
        "command": " ",
        "command_1": "/bin/sh",
        "livenessProbe": " ",
        "failurethreshold": 5,
        "path": "/",
        "port": 80,
        "scheme": "HTTP",
        "initialdelayseconds": 300,
        "periodseconds": 60,
        "successthreshold": 1,
        "timeoutSeconds": 30
    }

    # 创建Statefulset_必填参数_可选参数有一个配置引用-configmap
    data_list_configmap = {
        "statefulset_name": '{}-statefulset-configmap'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME,
        "envFrom": " ",
        "configMapRef": " ",
        "configmap_name": "{}-ares-statefulset-configmap".format(settings.RESOURCE_PREFIX)
    }

    # 创建Statefulset_必填参数_可选参数有两个_存储卷挂载和存储卷
    data_list_volume = {
        "statefulset_name": '{}-statefulset-volume'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME,
        "volumeMounts": " ",
        "mountpath": "/pvc/test",
        "volume_name": "new-volume",
        "volumes": " ",
        "claimname": '{}-ares-statefulset-pvc'.format(settings.RESOURCE_PREFIX).replace('_', '-')
    }

    # 创建Statefulset_必填参数有两个_网络模式和配置引用_secret
    data_list_hostnetwork_secret = {
        "statefulset_name": '{}-statefulset-hostnetwork-secret'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME,
        "hostNetwork": " ",
        "hostnetwork": "true",
        "envFrom": " ",
        "secretRef": " ",
        "secret_name": "{}-ares-statefulset-secret".format(settings.RESOURCE_PREFIX)
    }

    # 创建Statefulset_亲和性_基础Statefulset
    data_list_affinify_base = {
        "statefulset_name": '{}-appbaseforaffinity'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME,
    }

    # 创建Statefulset_亲和性_亲和Statefulset
    data_list_affinify_app_name = {
        "statefulset_name": '{}-affinity'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME,
        "affinity": " ",
        "affinity_type": "podAffinity",
        "statefulset_name_affinify_base": '{}-appbaseforaffinity'.format(settings.RESOURCE_PREFIX).replace('_', '-')
    }

    # 创建Statefulset_亲和性_反亲和Statefulset
    data_list_antiaffinify_app_name = {
        "statefulset_name": '{}-antiaffinify'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME,
        "affinity": " ",
        "affinity_type": "podAntiAffinity",
        "statefulset_name_affinify_base": '{}-appbaseforaffinity'.format(settings.RESOURCE_PREFIX).replace('_', '-')
    }

    # 创建Statefulset_端口
    data_list_port = {
        "statefulset_name": '{}-statefulset-port'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME,
        "ports": " ",
        "protocol": "TCP",
        "hostPort": random.randrange(32000, 34000, 1),
        "containerPort": 80
    }
