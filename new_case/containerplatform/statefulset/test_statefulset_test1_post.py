import pytest
from new_case.containerplatform.statefulset.statefulset import Statefulset
from common import settings
from common.base_request import Common
from new_case.containerplatform.configmap.configmap import Configmap
from new_case.containerplatform.persistentvolumeclaims.pvc import Pvc
from new_case.operation.log.log import Log
from new_case.containerplatform.secret.secret import Secret
import base64
import json


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestStatefulsetSuitePost(object):

    def setup_class(self):
        self.statefulset = Statefulset()

        self.configmap_tool = Configmap()
        self.configmap_name = "{}-ares-statefulset-configmap".format(settings.RESOURCE_PREFIX)
        self.verify_template_configmap = Common.generate_jinja_template(self,
                                                                        './verify_data/configmap/create_response.jinja2')

        self.pvc = Pvc()
        self.pvcname = '{}-ares-statefulset-pvc'.format(settings.RESOURCE_PREFIX).replace('_', '-')

        self.log = Log()

        self.secret_tool = Secret()
        self.secret_name = "{}-ares-statefulset-secret".format(settings.RESOURCE_PREFIX)
        self.verify_template = Common.generate_jinja_template(self, './verify_data/secret/create_response.jinja2')

    @pytest.mark.metis
    def 测试_创建Statefulset_必填参数_可选参数有两个_更新策略和组件标签(self):
        data = self.statefulset.data_list_strategy_labels
        self.statefulset.delete_statefulset(ns_name=data['namespace'], statefulset_name=data['statefulset_name'])
        create_result = self.statefulset.create_statefulset_jinja2("./test_data/statefulset/statefulset.jinja2",
                                                                   data=data)
        assert create_result.status_code == 201, "创建{}有状态副本集失败{}".format(data['statefulset_name'], create_result.text)

        detail_result = self.statefulset.detail_statefulset_jinja2(ns_name=data['namespace'],
                                                                   statefulset_name=data['statefulset_name'])
        assert detail_result.status_code == 200, "获取有状态副本集{}详情页失败,{}".format(data['statefulset_name'],
                                                                             detail_result.text)

        statefulset_status = self.statefulset.get_statefulset_status(ns_name=data['namespace'],
                                                                     statefulset_name=data["statefulset_name"])
        assert statefulset_status, "创建有状态副本集后，验证有状态副本集状态出错: 有状态副本集：{} is not running".format(data["statefulset_name"])

        value = self.statefulset.generate_jinja_data("./verify_data/statefulset/create_statefulset.jinja2", data)
        assert self.statefulset.is_sub_dict(value, create_result.json()), \
            "创建Statefulset_必填参数_可选参数有两个_更新策略和组件标签比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

    def 测试创建statefulset事件(self):
        payload = {"cluster": settings.REGION_NAME,
                   "kind": "StatefulSet",
                   "name": self.statefulset.data_list_strategy_labels["statefulset_name"],
                   "namespace": settings.K8S_NAMESPACE, "page": 1, "page_size": 20}
        kevents_results = self.statefulset.get_kevents(payload=payload, expect_value="SuccessfulCreate", times=1)
        assert kevents_results, "创建statefulset获取事件获取失败"

    @pytest.mark.metis
    def 测试_创建Statefulset_必填参数_可选参数有三个_启动命令和参数和健康检查(self):
        data = self.statefulset.data_list_args_livenessprobe
        self.statefulset.delete_statefulset(ns_name=data['namespace'], statefulset_name=data['statefulset_name'])
        create_result = self.statefulset.create_statefulset_jinja2("./test_data/statefulset/statefulset.jinja2",
                                                                   data=data)
        assert create_result.status_code == 201, "创建{}有状态副本集失败{}".format(data['statefulset_name'], create_result.text)

        detail_result = self.statefulset.detail_statefulset_jinja2(ns_name=data['namespace'],
                                                                   statefulset_name=data['statefulset_name'])
        assert detail_result.status_code == 200, "获取有状态副本集{}详情页失败,{}".format(data['statefulset_name'],
                                                                             detail_result.text)

        statefulset_status = self.statefulset.get_statefulset_status(ns_name=data['namespace'],
                                                                     statefulset_name=data["statefulset_name"])
        assert statefulset_status, "创建有状态副本集后，验证有状态副本集状态出错: 部署：{} is not running".format(data["statefulset_name"])

        value = self.statefulset.generate_jinja_data("./verify_data/statefulset/create_statefulset.jinja2", data)
        assert self.statefulset.is_sub_dict(value, create_result.json()), \
            "创建Statefulset_必填参数_可选参数有两个_更新策略和组件标签比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

    @pytest.mark.metis
    def 测试_创建Statefulset_必填参数_可选参数有一个配置引用_configmap(self):
        result = {"flag": True}
        # 创建configmap
        cm_data = {
            "configmap_name": self.configmap_name,
            "description": "key-value",
            "namespace": settings.K8S_NAMESPACE,
            "configmap_key": "key",
            "configmap_value": "value"
        }
        self.configmap_tool.delete_configmap(self.configmap_name)
        ret = self.configmap_tool.create_configmap(data=cm_data)
        assert ret.status_code == 201, "创建配置字典失败:{}".format(ret.text)

        value = self.verify_template_configmap.render(cm_data)
        assert self.configmap_tool.is_sub_dict(json.loads(value),
                                               ret.json()), "创建configmap比对数据失败，返回数据:{},期望数据:{}".format(ret.json(),
                                                                                                       value)

        # 创建statefulset
        data = self.statefulset.data_list_configmap
        self.statefulset.delete_statefulset(ns_name=data['namespace'], statefulset_name=data['statefulset_name'])
        create_result = self.statefulset.create_statefulset_jinja2("./test_data/statefulset/statefulset.jinja2",
                                                                   data=data)
        assert create_result.status_code == 201, "创建{}有状态副本集失败{}".format(data['statefulset_name'], create_result.text)

        value = self.statefulset.generate_jinja_data("./verify_data/statefulset/create_statefulset.jinja2", data)
        assert self.statefulset.is_sub_dict(value, create_result.json()), \
            "创建Statefulset_必填参数_可选参数有一个配置引用_configmap比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

        # 验证
        detail_result = self.statefulset.detail_statefulset_jinja2(ns_name=data['namespace'],
                                                                   statefulset_name=data['statefulset_name'])
        assert detail_result.status_code == 200, "获取有状态副本集{}详情页失败,{}".format(data['statefulset_name'],
                                                                             detail_result.text)

        statefulset_status = self.statefulset.get_statefulset_status(ns_name=data['namespace'],
                                                                     statefulset_name=data["statefulset_name"])
        assert statefulset_status, "创建有状态副本集后，验证有状态副本集状态出错: 有状态副本集：{} is not running".format(data["statefulset_name"])

        content = detail_result.json()
        v1 = self.statefulset.get_value(content, 'spec.template.spec.containers.0.envFrom.0.configMapRef.name')
        result = self.statefulset.update_result(result, v1 == self.configmap_name, '环境变量完整引用configmap失败')

        ret = self.configmap_tool.delete_configmap(configmap_name=data['configmap_name'])
        assert ret.status_code == 200, "删除配置字典{}失败:{}".format(data['configmap_name'], ret.text)
        assert result['flag'], result

    @pytest.mark.metis
    @pytest.mark.skipif(not settings.SC_NAME, reason="没有存储类，不运行当前用例")
    def 测试_创建statefulset_必填参数_可选参数有两个_存储卷挂载和存储卷(self):
        result = {"flag": True}
        # 创建pvc
        pvc_data = {
            "pvc_name": self.pvcname,
            "namespace": settings.K8S_NAMESPACE,
            "storageclassname": settings.SC_NAME
        }
        data = self.statefulset.data_list_volume
        self.statefulset.delete_statefulset(ns_name=data['namespace'], statefulset_name=data['statefulset_name'])
        self.pvc.delete_pvc(settings.K8S_NAMESPACE, self.pvcname)
        self.pvc.check_exists(self.pvc.get_common_pvc_url_v1(pvc_data["namespace"], pvc_data['pvc_name']), 404)

        create_result = self.pvc.create_pvc_jinja2("./test_data/pvc/pvc.jinja2", data=pvc_data)
        assert create_result.status_code == 201, "创建pvc{}失败:{}".format(pvc_data['pvc_name'], create_result.text)

        pvc_status = self.pvc.get_status(self.pvc.get_common_pvc_url_v1(pvc_name=pvc_data["pvc_name"]), "status.phase",
                                         "Bound")
        assert pvc_status, "创建pvc状态错误:{}的状态没有被绑定".format(pvc_data["pvc_name"])

        value = self.pvc.generate_jinja_data("./verify_data/pvc/create-pvc.jinja2", pvc_data)
        assert self.pvc.is_sub_dict(value, create_result.json()), \
            "创建pvc_必填参数_可选参数为空比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

        # 创建statefulset
        create_result = self.statefulset.create_statefulset_jinja2("./test_data/statefulset/statefulset.jinja2",
                                                                   data=data)
        assert create_result.status_code == 201, "创建{}有状态副本集失败{}".format(data['statefulset_name'], create_result.text)

        value = self.statefulset.generate_jinja_data("./verify_data/statefulset/create_statefulset.jinja2", data)
        assert self.statefulset.is_sub_dict(value, create_result.json()), \
            "创建tatefulset_必填参数_可选参数只有一个_存储比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

        # 验证
        detail_result = self.statefulset.detail_statefulset_jinja2(ns_name=data['namespace'],
                                                                   statefulset_name=data['statefulset_name'])
        assert detail_result.status_code == 200, "获取有状态副本集{}详情页失败,{}".format(data['statefulset_name'],
                                                                             detail_result.text)

        statefulset_status = self.statefulset.get_statefulset_status(ns_name=data['namespace'],
                                                                     statefulset_name=data["statefulset_name"])
        assert statefulset_status, "创建有状态副本集后，验证有状态副本集状态出错: 有状态副本集：{} is not running".format(data["statefulset_name"])

        content = detail_result.json()
        claimName = self.statefulset.get_value(content, 'spec.template.spec.volumes.0.persistentVolumeClaim.claimName')
        result = self.statefulset.update_result(result, claimName == self.pvcname, '有状态副本集挂载pvc失败')

        # 删除statefulset
        self.statefulset.delete_statefulset(ns_name=data['namespace'], statefulset_name=data['statefulset_name'])
        self.statefulset.check_exists(self.statefulset.get_common_statefulset_url(ns_name=data['namespace'],
                                                                                  statefulset_name=data[
                                                                                      'statefulset_name']), 404)
        # 删除pvc
        delete_result = self.pvc.delete_pvc(ns_name=pvc_data["namespace"], pvc_name=pvc_data["pvc_name"])
        assert delete_result.status_code == 200, "删除pvc失败{}".format(delete_result.text)
        assert result['flag'], result

    @pytest.mark.metis
    def 测试_创建Statefulset_必填参数_可选参数有两个_网络模式和配置引用_secret(self):
        result = {"flag": True}
        # 创建secret
        secret_data = {
            "secret_name": self.secret_name,
            "secret_type": "Opaque",
            "opaque_key": "opaque_key",
            "opaque_value": str(base64.b64encode("opaque_value".encode('utf-8')), 'utf8')
        }
        secret_data.update({"K8S_NAMESPACE": settings.K8S_NAMESPACE})
        self.secret_tool.delete_secret(self.secret_name, settings.K8S_NAMESPACE)
        ret = self.secret_tool.create_secret('./test_data/secret/create_secret.jinja2', data=secret_data)

        assert ret.status_code == 201, "创建{}类型保密字典失败:{}".format(secret_data['secret_type'], ret.text)
        value = self.verify_template.render(secret_data)
        assert self.secret_tool.is_sub_dict(json.loads(value), ret.json()), "创建保密字典比对数据失败，返回数据:{},期望数据:{}".format(
            ret.json(), json.loads(value))

        # 创建statefulset
        data = self.statefulset.data_list_hostnetwork_secret
        self.statefulset.delete_statefulset(ns_name=data['namespace'], statefulset_name=data['statefulset_name'])
        create_result = self.statefulset.create_statefulset_jinja2("./test_data/statefulset/statefulset.jinja2",
                                                                   data=data)
        assert create_result.status_code == 201, "创建{}有状态副本集失败{}".format(data['statefulset_name'], create_result.text)

        value = self.statefulset.generate_jinja_data("./verify_data/statefulset/create_statefulset.jinja2", data)
        assert self.statefulset.is_sub_dict(value, create_result.json()), \
            "创建Statefulset_必填参数_可选参数只有一个_配置引用_secret比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

        # 验证
        detail_result = self.statefulset.detail_statefulset_jinja2(ns_name=data['namespace'],
                                                                   statefulset_name=data['statefulset_name'])
        assert detail_result.status_code == 200, "获取{}有状态副本集详情页失败,{}".format(data['statefulset_name'],
                                                                             detail_result.text)
        statefulset_status = self.statefulset.get_statefulset_status(ns_name=data['namespace'],
                                                                     statefulset_name=data["statefulset_name"])
        assert statefulset_status, "创建有状态副本集后，验证有状态副本集状态出错: 有状态副本集：{} is not running".format(data["statefulset_name"])
        content = detail_result.json()
        v1 = self.statefulset.get_value(content, 'spec.template.spec.containers.0.envFrom.0.secretRef.name')
        result = self.statefulset.update_result(result, v1 == self.secret_name, '环境变量完整引用secret失败')

        ret = self.secret_tool.delete_secret(data["secret_name"])
        assert ret.status_code == 200, "删除{}类型的保密字典失败:{}".format(data["secret_type"], ret.text)
        assert result['flag'], result

    @pytest.mark.metis
    def 测试_创建Statefulset_必填参数_可选参数亲和性(self):
        result = {"flag": True}
        # 创建基础statefulset
        data_affinify_base = self.statefulset.data_list_affinify_base
        self.statefulset.delete_statefulset(ns_name=data_affinify_base['namespace'],
                                            statefulset_name=data_affinify_base['statefulset_name'])
        create_result = self.statefulset.create_statefulset_jinja2("./test_data/statefulset/statefulset.jinja2",
                                                                   data=data_affinify_base)
        assert create_result.status_code == 201, "创建基础{}有状态副本集失败{}".format(data_affinify_base['statefulset_name'],
                                                                           create_result.text)

        value = self.statefulset.generate_jinja_data("./verify_data/statefulset/create_statefulset.jinja2",
                                                     data_affinify_base)
        assert self.statefulset.is_sub_dict(value, create_result.json()), \
            "创建Statefulset_基础比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

        detail_result = self.statefulset.detail_statefulset_jinja2(ns_name=data_affinify_base['namespace'],
                                                                   statefulset_name=data_affinify_base[
                                                                       'statefulset_name'])
        assert detail_result.status_code == 200, "获取基础有状态副本集{}详情页失败,{}".format(data_affinify_base['statefulset_name'],
                                                                               detail_result.text)

        statefulset_status = self.statefulset.get_statefulset_status(ns_name=data_affinify_base['namespace'],
                                                                     statefulset_name=data_affinify_base[
                                                                         "statefulset_name"])
        assert statefulset_status, "创建有状态副本集后，验证有状态副本集状态出错: 有状态副本集：{} is not running".format(
            data_affinify_base["statefulset_name"])

        # 创建亲和statefulset
        data_affinify_app_name = self.statefulset.data_list_affinify_app_name
        self.statefulset.delete_statefulset(ns_name=data_affinify_app_name['namespace'],
                                            statefulset_name=data_affinify_app_name['statefulset_name'])
        create_affinity_result = self.statefulset.create_statefulset_jinja2(
            "./test_data/statefulset/statefulset.jinja2",
            data=data_affinify_app_name)
        assert create_affinity_result.status_code == 201, "创建亲和{}有状态副本集失败{}" \
            .format(data_affinify_app_name['statefulset_name'], create_affinity_result.text)

        detail_result = self.statefulset.detail_statefulset_jinja2(ns_name=data_affinify_app_name['namespace'],
                                                                   statefulset_name=data_affinify_app_name[
                                                                       'statefulset_name'])
        assert detail_result.status_code == 200, "获取基础有状态副本集{}详情页失败,{}".format(
            data_affinify_app_name['statefulset_name'], detail_result.text)

        statefulset_status = self.statefulset.get_statefulset_status(ns_name=data_affinify_app_name['namespace'],
                                                                     statefulset_name=data_affinify_app_name[
                                                                         "statefulset_name"])
        assert statefulset_status, "创建有状态副本集后，验证有状态副本集状态出错: 有状态副本集：{} is not running". \
            format(data_affinify_app_name["statefulset_name"])

        pod_result = self.statefulset.get_statefulset_pods(ns_name=data_affinify_base['namespace'],
                                                           statefulset_name=data_affinify_base['statefulset_name'])
        assert pod_result.status_code == 200, "获取基础有状态副本集容器组失败:{}".format(pod_result.text)
        hostip_base = self.statefulset.get_value(pod_result.json(), 'items.0.status.hostIP')

        pod_result = self.statefulset.get_statefulset_pods(ns_name=data_affinify_app_name['namespace'],
                                                           statefulset_name=data_affinify_app_name['statefulset_name'])
        assert pod_result.status_code == 200, "获取基础有状态副本集容器组失败:{}".format(pod_result.text)
        hostip_affinify = self.statefulset.get_value(pod_result.json(), 'items.0.status.hostIP')

        result = self.statefulset.update_result(result, hostip_base == hostip_affinify, "亲和性验证失败")

        # 创建反亲和statefulset
        data_antiaffinify_app_name = self.statefulset.data_list_antiaffinify_app_name
        self.statefulset.delete_statefulset(ns_name=data_antiaffinify_app_name['namespace'],
                                            statefulset_name=data_antiaffinify_app_name['statefulset_name'])
        create_antiaffinity_result = self.statefulset.create_statefulset_jinja2(
            "./test_data/statefulset/statefulset.jinja2", data=data_antiaffinify_app_name)
        assert create_antiaffinity_result.status_code == 201, "创建反亲和{}有状态副本集失败{}". \
            format(data_antiaffinify_app_name['statefulset_name'], create_antiaffinity_result.text)

        detail_result = self.statefulset.detail_statefulset_jinja2(ns_name=data_antiaffinify_app_name['namespace'],
                                                                   statefulset_name=data_antiaffinify_app_name[
                                                                       'statefulset_name'])
        assert detail_result.status_code == 200, "获取基础有状态副本集{}详情页失败,{}". \
            format(data_antiaffinify_app_name['statefulset_name'], detail_result.text)

        statefulset_status = self.statefulset.get_statefulset_status(ns_name=data_antiaffinify_app_name['namespace'],
                                                                     statefulset_name=data_antiaffinify_app_name[
                                                                         "statefulset_name"])
        assert statefulset_status, "创建有状态副本集后，验证有状态副本集状态出错: 有状态副本集：{} is not running". \
            format(data_antiaffinify_app_name["statefulset_name"])

        pod_result = self.statefulset.get_statefulset_pods(ns_name=data_antiaffinify_app_name['namespace'],
                                                           statefulset_name=data_antiaffinify_app_name[
                                                               'statefulset_name'])
        assert pod_result.status_code == 200, "获取基础有状态副本集容器组失败:{}".format(pod_result.text)
        hostip_antiaffinify = self.statefulset.get_value(pod_result.json(), 'items.0.status.hostIP')

        result = self.statefulset.update_result(result, hostip_base != hostip_antiaffinify, "反亲和性验证失败")
        assert result['flag'], result

    @pytest.mark.metis
    def 测试_创建Statefulset_必填参数_可选端口(self):
        data = self.statefulset.data_list_port
        self.statefulset.delete_statefulset(ns_name=data['namespace'], statefulset_name=data['statefulset_name'])
        create_result = self.statefulset.create_statefulset_jinja2("./test_data/statefulset/statefulset.jinja2",
                                                                   data=data)
        assert create_result.status_code == 201, "创建{}有状态副本集失败{}".format(data['statefulset_name'], create_result.text)

        detail_result = self.statefulset.detail_statefulset_jinja2(ns_name=data['namespace'],
                                                                   statefulset_name=data['statefulset_name'])
        assert detail_result.status_code == 200, "获取有状态副本集{}详情页失败,{}".format(data['statefulset_name'],
                                                                             detail_result.text)

        statefulset_status = self.statefulset.get_statefulset_status(ns_name=data['namespace'],
                                                                     statefulset_name=data["statefulset_name"])
        assert statefulset_status, "创建有状态副本集后，验证有状态副本集状态出错: 有状态副本集：{} is not running".format(data["statefulset_name"])

        value = self.statefulset.generate_jinja_data("./verify_data/statefulset/create_statefulset.jinja2", data)
        assert self.statefulset.is_sub_dict(value, create_result.json()), \
            "创建tatefulset_必填参数_可选参数有两个_更新策略和组件标签比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)
