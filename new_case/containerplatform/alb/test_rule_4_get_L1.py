import pytest
from common.settings import RERUN_TIMES, K8S_NAMESPACE, GLOBAL_ALB_NAME
from new_case.containerplatform.alb.alb import Alb
from new_case.containerplatform.alb.conftest import L1_rule_update, update_rule_casename


@pytest.mark.skipif(not GLOBAL_ALB_NAME, reason="没有传alb名称，默认不跑对应用例")
@pytest.mark.alb
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestDetailRule(object):

    def setup_class(self):
        self.alb_client = Alb()
        self.k8s_namespace = K8S_NAMESPACE

    @pytest.mark.upgrade
    @pytest.mark.parametrize("data", L1_rule_update, ids=update_rule_casename)
    def 测试获取规则详情L1(self, data):
        ret = self.alb_client.detail_rule(data['rule_name'])
        assert ret.status_code == 200, "获取规则详情失败:{}".format(ret.text)
        values = self.alb_client.generate_jinja_data("verify_data/alb/create_rule_response.jinja2",
                                                     data)
        assert self.alb_client.is_sub_dict(values, ret.json()), \
            "获取规则详情比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)

    def 测试获取规则详情L1_不存在(self):
        ret = self.alb_client.detail_rule('rule_name')
        assert ret.status_code == 404, "获取不存在的规则详情失败:{}".format(ret.text)
