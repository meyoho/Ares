import pytest
from common.settings import RERUN_TIMES, K8S_NAMESPACE, VM_IPS, USERNAME, REGION_NAME, AUDIT_UNABLED, DEFAULT_NS, \
    GLOBAL_ALB_NAME
from new_case.containerplatform.alb.alb import Alb
from new_case.containerplatform.alb.conftest import L1_rule_update, prepare_frontend, alb_ip, update_rule_casename


@pytest.mark.skipif(not GLOBAL_ALB_NAME, reason="没有传alb名称，默认不跑对应用例")
@pytest.mark.alb
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestPutRule(object):

    def setup_class(self):
        self.alb_client = Alb()
        self.k8s_namespace = K8S_NAMESPACE

    @pytest.mark.upgrade
    @pytest.mark.parametrize("data", L1_rule_update, ids=update_rule_casename)
    def 测试更新规则和地址访问L1(self, data):
        ret = self.alb_client.detail_rule(data['rule_name'])
        assert ret.status_code == 200, "获取规则详情失败:{}".format(ret.text)
        resourceVersion = self.alb_client.get_value(ret.json(), "metadata.resourceVersion")
        data.update({'resourceVersion': resourceVersion})

        ret = self.alb_client.update_rule(data['rule_name'], "test_data/alb/create_rule.jinja2",
                                          data)
        assert ret.status_code == 200, "更新规则失败:{}".format(ret.text)
        values = self.alb_client.generate_jinja_data("verify_data/alb/create_rule_response.jinja2",
                                                     data)
        assert self.alb_client.is_sub_dict(values, ret.json()), \
            "更新规则比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)
        if not VM_IPS:
            pytest.skip("没有传虚拟机IP，不需要验证访问")
        assert alb_ip, "获取alb ip失败"
        type = self.alb_client.get_uuid_accord_name(prepare_frontend, {'frontend_name': data['frontend_name']},
                                                    'protocol')
        port = self.alb_client.get_uuid_accord_name(prepare_frontend, {'frontend_name': data['frontend_name']},
                                                    'port')
        if type == 'tcp':
            type = 'http'
        if 'host' in data:
            host = data['host']
        else:
            host = alb_ip
        if 'url' in data:
            if 'url_type' in data:
                url = data['url'].split('-')[0] + '-b'
            else:
                url = data['url']
        else:
            url = ''
        param = header = cookie = ""
        if 'param' in data:
            if 'OR' in data['param']:
                param_key = data['param'].split('(')[1].split(')')[0].split(' ')[2]
                param_value = data['param'].split('(')[1].split(')')[0].split(' ')[3]
            else:
                param_key = data['param'].split(' ')[2]
                param_value = data['param'].split(' ')[3]
            param = "?{}={}".format(param_key, param_value)
        if 'header' in data:
            if 'OR' in data['header']:
                header_key = data['header'].split('(')[1].split(')')[0].split(' ')[2]
                header_value = data['header'].split('(')[1].split(')')[0].split(' ')[3]
            else:
                header_key = data['header'].split(' ')[2]
                header_value = data['header'].split(' ')[3]
            header = " -H {}:{} ".format(header_key, header_value)
        if 'cookie' in data:
            if 'OR' in data['cookie']:
                cookie_key = data['cookie'].split('(')[1].split(')')[0].split(' ')[2]
                cookie_value = data['cookie'].split('(')[1].split(')')[0].split(' ')[3]
            else:
                cookie_key = data['cookie'].split(' ')[2]
                cookie_value = data['cookie'].split(' ')[3]
            cookie = " --cookie {}={} ".format(cookie_key, cookie_value)

        ip = VM_IPS.split(';')[0]
        rule_cmd = "date&&curl --connect-timeout 10 -ivk -H \"Host: {host}\" {type}://{alb_ip}:{port}{url}{param} {header} {cookie}".format(
            alb_ip=alb_ip, host=host, type=type, port=port, url=url, param=param, header=header, cookie=cookie)
        expect = '200 OK'
        if 'rewrite' in data:
            expect = 'redirect'
        if 'redirect_code' in data:
            expect = str(data['redirect_code'])
        if 'backend_protocol' in data:
            expect = '403 Forbidden'
        if 'ip' in data and 'RANGE SRC_IP' not in data['ip']:
            expect = '404 Not Found'
        if type == 'https' and 'secret_name' not in data:
            expect = '404 Not Found'
        if 'service_name' not in data:
            expect = '502 Bad Gateway'
        visit_result = self.alb_client.access_alb(rule_cmd, ip, expect)
        assert visit_result[0], "更新规则后，生成的访问地址，访问失败"

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 测试规则更新审计(self):
        payload = {"user_name": USERNAME, "operation_type": "update", "resource_type": "rules",
                   "resource_name": L1_rule_update[0]['rule_name']}
        result = self.alb_client.search_audit(payload)
        payload.update({"namespace": DEFAULT_NS, "region_name": REGION_NAME})
        values = self.alb_client.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.alb_client.is_sub_dict(values, result.json()), "审计数据不符合预期"
