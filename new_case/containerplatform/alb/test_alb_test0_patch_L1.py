import pytest
from common.settings import RERUN_TIMES, K8S_NAMESPACE, USERNAME, REGION_NAME, PROJECT_NAME, AUDIT_UNABLED, DEFAULT_NS, \
    GLOBAL_ALB_NAME
from new_case.containerplatform.alb.alb import Alb
from new_case.containerplatform.alb.conftest import l1_alb, update_alb_casename


@pytest.mark.skipif(not GLOBAL_ALB_NAME, reason="没有传alb名称，默认不跑对应用例")
@pytest.mark.alb
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestPatchAlb(object):

    def setup_class(self):
        self.alb_client = Alb()
        self.k8s_namespace = K8S_NAMESPACE

    @pytest.mark.parametrize("data", l1_alb, ids=update_alb_casename)
    def 测试负载均衡更新项目(self, data):
        ret = self.alb_client.update_alb(data['alb_name'], 'test_data/alb/patch_alb.jinja2', data)
        assert ret.status_code == 200, "负载均衡更新项目失败:{}".format(ret.text)
        values = self.alb_client.generate_jinja_data("verify_data/alb/patch_alb_response.jinja2", data)
        assert self.alb_client.is_sub_dict(values, ret.json()), \
            "负载均衡更新项目比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)

        ret = self.alb_client.list_alb(project=PROJECT_NAME, selector=data['alb_name'])
        assert ret.status_code == 200, "负载均衡按项目和名称过滤失败:{}".format(ret.text)
        content = self.alb_client.get_k8s_resource_data(ret.json(), data['alb_name'], list_key="items")
        values = self.alb_client.generate_jinja_data("verify_data/alb/alb_response.jinja2", data)
        if data['project_name'] in [PROJECT_NAME, "ALL_ALL"]:
            assert self.alb_client.is_sub_dict(values, content), \
                "负载均衡按项目和名称过滤比对数据失败，返回数据{}，期望数据{}".format(content, values)
        else:
            assert not self.alb_client.is_sub_dict(values, content), \
                "负载均衡按项目和名称过滤比对数据失败，返回数据{}，期望数据{}".format(ret.text, 0)

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 测试负载均衡更新项目审计(self):
        payload = {"user_name": USERNAME, "operation_type": "patch", "resource_type": "alaudaloadbalancer2",
                   "resource_name": l1_alb[0]['alb_name']}
        result = self.alb_client.search_audit(payload)
        payload.update({"namespace": DEFAULT_NS, "region_name": REGION_NAME})
        values = self.alb_client.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.alb_client.is_sub_dict(values, result.json()), "审计数据不符合预期"
