import pytest
from common.settings import RERUN_TIMES, K8S_NAMESPACE, GLOBAL_ALB_NAME
from new_case.containerplatform.alb.alb import Alb
from new_case.containerplatform.alb.conftest import L1_rule, create_rule_casename


@pytest.mark.skipif(not GLOBAL_ALB_NAME, reason="没有传alb名称，默认不跑对应用例")
@pytest.mark.alb
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestListRule(object):

    def setup_class(self):
        self.alb_client = Alb()
        self.k8s_namespace = K8S_NAMESPACE
        self.alb_name = GLOBAL_ALB_NAME

    @pytest.mark.upgrade
    @pytest.mark.parametrize("data", L1_rule, ids=create_rule_casename)
    def 测试获取规则列表L1(self, data):
        ret = self.alb_client.list_rule(self.alb_name, data['frontend_name'])
        assert ret.status_code == 200, "规则列表失败:{}".format(ret.text)
        content = self.alb_client.get_k8s_resource_data(ret.json(), data['rule_name'], list_key="items")
        values = self.alb_client.generate_jinja_data("verify_data/alb/create_rule_response.jinja2", data)
        assert self.alb_client.is_sub_dict(values, content), \
            "规则列表比对数据失败，返回数据{}，期望数据{}".format(content, values)

    def 测试获取规则列表L1_不存在(self):
        ret = self.alb_client.list_rule(self.alb_name, 'frontend_name')
        assert ret.status_code == 200, "规则列表失败:{}".format(ret.text)
        items = self.alb_client.get_value(ret.json(), "items")
        assert items == [], "获取不存在的端口下的规则列表失败:{}".format(ret.text)

    def 测试获取规则列表L1_有limit和continue参数(self):
        ret = self.alb_client.list_rule(self.alb_name, L1_rule[0]['frontend_name'], limit=1)
        assert ret.status_code == 200, "获取规则列表失败:{}".format(ret.text)
        continues = self.alb_client.get_value(ret.json(), "metadata.continue")
        ret_cnt = self.alb_client.list_rule(self.alb_name, L1_rule[0]['frontend_name'], limit=1,
                                            continues=continues)
        assert ret_cnt.status_code == 200, "获取规则列表失败:{}".format(ret_cnt.text)
        assert ret.json() != ret_cnt.json(), "分页数据相同，第一页数据:{},第二页数据:{}".format(ret.json(), ret_cnt.json())
