from time import sleep

from common import settings
from common.base_request import Common
from common.log import logger


class Alb(Common):

    def search_alb(self, name, region_name=settings.REGION_NAME):
        url = "acp/v1/resources/search/kubernetes/{}/apis/crd.alauda.io/v1/namespaces/{}/alaudaloadbalancer2" \
              "?limit=20&keyword={}&field=metadata.name".format(region_name, settings.DEFAULT_NS, name)
        return self.send('get', url)

    def list_alb(self, limit=20, selector='', project=''):
        url = "kubernetes/{}/apis/crd.alauda.io/v1/alaudaloadbalancer2?limit={}".format(
            self.region_name, limit)
        if selector != '':
            url = "{}&keyword={}&field=metadata.name".format(url, selector)
        if project != '':
            url = "{}&labelSelector=project.{}/name%20in%20(ALL_ALL,{})".format(
                url, settings.DEFAULT_LABEL, project)
        return self.send('get', path=url)

    def alb_url(self, alb_name, alb_namespace=settings.DEFAULT_NS):
        return "kubernetes/{}/apis/crd.alauda.io/v1/namespaces/{}/alaudaloadbalancer2/{}".format(
            self.region_name, alb_namespace, alb_name)

    def detail_alb(self, alb_name, alb_namespace=settings.DEFAULT_NS):
        url = self.alb_url(alb_name, alb_namespace)
        return self.send('get', path=url)

    def update_alb(self,
                   alb_name,
                   file,
                   data,
                   alb_namespace=settings.DEFAULT_NS):
        url = self.alb_url(alb_name, alb_namespace)
        data = self.generate_jinja_data(file, data)
        return self.send(
            'patch',
            path=url,
            json=data,
            headers={"Content-Type": "application/merge-patch+json"})

    def create_alb(self, file, data, alb_namespace=settings.DEFAULT_NS):
        url = "apis/app.alauda.io/v1alpha1/namespaces/{}/helmrequests".format(
            alb_namespace)
        data = self.generate_jinja_data(file, data)
        return self.send('post', path=url, json=data)

    def delete_alb(self,
                   alb_name,
                   alb_namespace=settings.DEFAULT_NS,
                   region_name=settings.REGION_NAME):
        url = "apis/app.alauda.io/v1alpha1/namespaces/{}/helmrequests/{}".format(
            alb_namespace, region_name + '-' + alb_name)
        return self.send('delete', path=url)

    def list_frontend(self,
                      alb_name,
                      alb_namespace=settings.DEFAULT_NS,
                      limit=20,
                      continues=''):
        url = "kubernetes/{}/apis/crd.alauda.io/v1/namespaces/{}/frontends?limit={}" \
              "&labelSelector=alb2.{}/name={}".format(self.region_name, alb_namespace, limit, settings.DEFAULT_LABEL,
                                                      alb_name)
        if continues != '':
            url = '{}&continue={}'.format(url, continues)
        return self.send('get', path=url)

    def create_frontend(self, file, data, alb_namespace=settings.DEFAULT_NS):
        url = "kubernetes/{}/apis/crd.alauda.io/v1/namespaces/{}/frontends".format(
            self.region_name, alb_namespace)
        data = self.generate_jinja_data(file, data)
        return self.send('post', path=url, json=data)

    def common_frontend_url(self,
                            frontend_name,
                            alb_namespace=settings.DEFAULT_NS):
        return "kubernetes/{}/apis/crd.alauda.io/v1/namespaces/{}/frontends/{}".format(
            self.region_name, alb_namespace, frontend_name)

    def detail_frontend(self, frontend_name, alb_namespace=settings.DEFAULT_NS):
        url = self.common_frontend_url(frontend_name, alb_namespace)
        return self.send('get', path=url)

    def update_frontend(self,
                        frontend_name,
                        file,
                        data,
                        alb_namespace=settings.DEFAULT_NS):
        url = self.common_frontend_url(frontend_name, alb_namespace)
        data = self.generate_jinja_data(file, data)
        return self.send('put', path=url, json=data)

    def delete_frontend(self, frontend_name, alb_namespace=settings.DEFAULT_NS):
        url = self.common_frontend_url(frontend_name, alb_namespace)
        return self.send('delete', path=url)

    def list_rule(self,
                  alb_name,
                  frontend_name,
                  alb_namespace=settings.DEFAULT_NS,
                  limit=20,
                  continues=''):
        url = "kubernetes/{region}/apis/crd.alauda.io/v1/namespaces/{ns}/rules?limit={limit}&labelSelector=alb2.{default_label}/name={alb_name}," \
              "alb2.{default_label}/frontend={frontend_name}".format(region=self.region_name, ns=alb_namespace,
                                                                     limit=limit, alb_name=alb_name,
                                                                     frontend_name=frontend_name,
                                                                     default_label=settings.DEFAULT_LABEL)
        if continues != '':
            url = '{}&continue={}'.format(url, continues)
        return self.send('get', path=url)

    def create_rule(self, file, data, alb_namespace=settings.DEFAULT_NS):
        url = "kubernetes/{}/apis/crd.alauda.io/v1/namespaces/{}/rules".format(
            self.region_name, alb_namespace)
        data = self.generate_jinja_data(file, data)
        return self.send('post', path=url, json=data)

    def common_rule_url(self, rule_name, alb_namespace=settings.DEFAULT_NS):
        return "kubernetes/{}/apis/crd.alauda.io/v1/namespaces/{}/rules/{}".format(
            self.region_name, alb_namespace, rule_name)

    def detail_rule(self, rule_name, alb_namespace=settings.DEFAULT_NS):
        url = self.common_rule_url(rule_name, alb_namespace)
        return self.send('get', path=url)

    def update_rule(self,
                    rule_name,
                    file,
                    data,
                    alb_namespace=settings.DEFAULT_NS):
        url = self.common_rule_url(rule_name, alb_namespace)
        data = self.generate_jinja_data(file, data)
        return self.send('put', path=url, json=data)

    def delete_rule(self, rule_name, alb_namespace=settings.DEFAULT_NS):
        url = self.common_rule_url(rule_name, alb_namespace)
        return self.send('delete', path=url)

    def access_alb(self, cmd, ip, expected_value):
        n = 0
        while n < 30:
            n += 1
            sleep(5)
            ret = self.excute_script(cmd=cmd, ip=ip)
            logger.info(expected_value)
            if expected_value in ret[1]:
                return True, ret[1]
        return False, "access alb failed"
