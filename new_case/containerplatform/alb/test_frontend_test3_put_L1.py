import pytest
from common.settings import K8S_NAMESPACE, USERNAME, REGION_NAME, AUDIT_UNABLED, DEFAULT_NS, GLOBAL_ALB_NAME
from new_case.containerplatform.alb.alb import Alb
from new_case.containerplatform.alb.conftest import L1_frontend_update, update_frontend_casename


@pytest.mark.skipif(not GLOBAL_ALB_NAME, reason="没有传alb名称，默认不跑对应用例")
@pytest.mark.alb
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=2, reruns_delay=3)
class TestUpdateFrontend(object):

    def setup_class(self):
        self.alb_client = Alb()
        self.k8s_namespace = K8S_NAMESPACE

    @pytest.mark.upgrade
    @pytest.mark.parametrize("data", L1_frontend_update, ids=update_frontend_casename)
    def 测试更新监听端口L1(self, data):
        ret = self.alb_client.detail_frontend(data['frontend_name'])
        assert ret.status_code == 200, "获取监听端口详情失败:{}".format(ret.text)
        resourceVersion = self.alb_client.get_value(ret.json(), "metadata.resourceVersion")

        data.update({'resourceVersion': resourceVersion})
        ret = self.alb_client.update_frontend(data['frontend_name'], "test_data/alb/create_frontend.jinja2",
                                              data)
        assert ret.status_code == 200, "更新监听端口失败:{}".format(ret.text)
        values = self.alb_client.generate_jinja_data("verify_data/alb/create_frontend_response.jinja2",
                                                     data)
        assert self.alb_client.is_sub_dict(values, ret.json()), \
            "更新监听端口比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 测试监听端口更新审计(self):
        payload = {"user_name": USERNAME, "operation_type": "update", "resource_type": "frontends",
                   "resource_name": L1_frontend_update[0]['frontend_name']}
        result = self.alb_client.search_audit(payload)
        payload.update({"namespace": DEFAULT_NS, "region_name": REGION_NAME})
        values = self.alb_client.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.alb_client.is_sub_dict(values, result.json()), "审计数据不符合预期"
