import pytest
from common.settings import RERUN_TIMES, K8S_NAMESPACE, GLOBAL_ALB_NAME
from new_case.containerplatform.alb.alb import Alb
from new_case.containerplatform.alb.conftest import L1_frontend_update, update_frontend_casename


@pytest.mark.skipif(not GLOBAL_ALB_NAME, reason="没有传alb名称，默认不跑对应用例")
@pytest.mark.alb
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestDetailFrontend(object):

    def setup_class(self):
        self.alb_client = Alb()
        self.k8s_namespace = K8S_NAMESPACE

    @pytest.mark.upgrade
    @pytest.mark.parametrize("data", L1_frontend_update, ids=update_frontend_casename)
    def 测试监听端口详情L1(self, data):
        ret = self.alb_client.detail_frontend(data['frontend_name'])
        assert ret.status_code == 200, "获取监听端口详情失败:{}".format(ret.text)
        values = self.alb_client.generate_jinja_data("verify_data/alb/create_frontend_response.jinja2",
                                                     data)
        assert self.alb_client.is_sub_dict(values, ret.json()), \
            "获取监听端口详情比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)

    def 测试监听端口详情L1_不存在(self):
        ret = self.alb_client.detail_frontend('frontend_name')
        assert ret.status_code == 404, "获取不存在的端口详情失败:{}".format(ret.text)
