import pytest

from common.settings import RERUN_TIMES, K8S_NAMESPACE, PROJECT_NAME, GLOBAL_ALB_NAME
from new_case.containerplatform.alb.alb import Alb
from new_case.containerplatform.alb.conftest import create_alb, alb_casename


@pytest.mark.skipif(not GLOBAL_ALB_NAME, reason="没有传alb名称，默认不跑对应用例")
@pytest.mark.alb
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestCreateAlb(object):

    def setup_class(self):
        self.alb_client = Alb()
        self.k8s_namespace = K8S_NAMESPACE

    @pytest.mark.captain
    @pytest.mark.prepare
    @pytest.mark.parametrize("data", create_alb, ids=alb_casename)
    def 测试创建负载均衡(self, data):
        self.alb_client.delete_alb(data['alb_name'])
        ret = self.alb_client.create_alb('test_data/alb/create_alb.jinja2', data)
        assert ret.status_code == 201, "创建负载均衡hr失败:{}".format(ret.text)
        values = self.alb_client.generate_jinja_data("verify_data/alb/create_alb_hr_response.jinja2", data)
        assert self.alb_client.is_sub_dict(values, ret.json()), \
            "创建负载均衡hr比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)

        # http://jira.alauda.cn/browse/ACP-1523 有BUG 导致创建完需要较长时间才能获取到
        create_flag = self.alb_client.check_exists(
            url=self.alb_client.alb_url(data['alb_name']), expect_status=200, expect_cnt=20)
        assert create_flag, "创建负载均衡alb失败"
        ret = self.alb_client.detail_alb(data['alb_name'])
        assert ret.status_code == 200, "获取负载均衡按详情失败:{}".format(ret.text)
        values = self.alb_client.generate_jinja_data("verify_data/alb/create_alb_response.jinja2", data)
        assert self.alb_client.is_sub_dict(values, ret.json()), \
            "获取负载均衡详情比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)

        ret = self.alb_client.list_alb(project=PROJECT_NAME, selector=data['alb_name'])
        assert ret.status_code == 200, "负载均衡按项目和名称过滤失败:{}".format(ret.text)
        content = self.alb_client.get_k8s_resource_data(ret.json(), data['alb_name'], list_key="items")
        values = self.alb_client.generate_jinja_data("verify_data/alb/create_alb_response.jinja2", data)
        if data['project_name'] in [PROJECT_NAME, "ALL_ALL"]:
            assert self.alb_client.is_sub_dict(values, content), \
                "负载均衡按项目和名称过滤比对数据失败，返回数据{}，期望数据{}".format(content, values)
        else:
            assert not self.alb_client.is_sub_dict(values, content), \
                "负载均衡按项目和名称过滤比对数据失败，返回数据{}，期望数据{}".format(ret.text, 0)

    @pytest.mark.upgrade
    def 测试搜索负载均衡(self):
        data = create_alb[0]
        alb_name = data['alb_name']
        names = [alb_name, alb_name.upper(), alb_name.capitalize(), alb_name[:-1]]
        for name in names:
            ret = self.alb_client.search_alb(name)
            assert ret.status_code == 200, "搜索负载均衡失败:{}".format(ret.text)
            values = self.alb_client.generate_jinja_data("verify_data/alb/create_alb_response.jinja2", data)
            content = self.alb_client.get_k8s_resource_data(ret.json(), data['alb_name'], 'items')
            assert self.alb_client.is_sub_dict(values, content), \
                "搜索负载均衡比对数据失败，返回数据{}，期望数据{}".format(content, values)

    def 测试搜索不存在的负载均衡(self):
        name = create_alb[0]['alb_name'] + 'notexist'
        ret = self.alb_client.search_alb(name)
        assert ret.status_code == 200, "搜索负载均衡失败:{}".format(ret.text)
        content = self.alb_client.get_value(ret.json(), 'items')
        assert content == [], \
            "搜索不存在的负载均衡比对数据失败，返回数据{}，期望数据{}".format(content, [])

    @pytest.mark.captain
    @pytest.mark.delete
    @pytest.mark.parametrize("data", create_alb, ids=alb_casename)
    def 测试删除负载均衡(self, data):
        ret = self.alb_client.delete_alb(data['alb_name'])
        assert ret.status_code == 200, "删除负载均衡hr失败:{}".format(ret.text)
        delete_flag = self.alb_client.check_exists(
            self.alb_client.alb_url(data['alb_name']), 404)
        assert delete_flag, "删除负载均衡alb失败"
