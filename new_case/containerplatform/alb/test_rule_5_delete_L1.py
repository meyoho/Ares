import pytest
from common.settings import RERUN_TIMES, K8S_NAMESPACE, USERNAME, REGION_NAME, AUDIT_UNABLED, DEFAULT_NS, \
    GLOBAL_ALB_NAME
from new_case.containerplatform.alb.alb import Alb
from new_case.containerplatform.alb.conftest import L1_rule_update, update_rule_casename


@pytest.mark.skipif(not GLOBAL_ALB_NAME, reason="没有传alb名称，默认不跑对应用例")
@pytest.mark.alb
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestDeleteRule(object):

    def setup_class(self):
        self.alb_client = Alb()
        self.k8s_namespace = K8S_NAMESPACE

    @pytest.mark.delete
    @pytest.mark.parametrize("data", L1_rule_update, ids=update_rule_casename)
    def 测试删除规则L1(self, data):
        ret = self.alb_client.delete_rule(data['rule_name'])
        assert ret.status_code == 200, "删除规则失败:{}".format(ret.text)
        delete_flag = self.alb_client.check_exists(
            self.alb_client.common_rule_url(data['rule_name']), 404)
        assert delete_flag, "删除规则失败"

    def 测试删除规则L1_不存在(self):
        ret = self.alb_client.delete_rule('rule_name')
        assert ret.status_code == 404, "删除不存在的规则失败:{}".format(ret.text)

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 测试规则删除审计(self):
        payload = {"user_name": USERNAME, "operation_type": "delete", "resource_type": "rules",
                   "resource_name": L1_rule_update[0]['rule_name']}
        result = self.alb_client.search_audit(payload)
        payload.update({"namespace": DEFAULT_NS, "region_name": REGION_NAME})
        values = self.alb_client.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.alb_client.is_sub_dict(values, result.json()), "审计数据不符合预期"
