import pytest
from common.settings import RERUN_TIMES, K8S_NAMESPACE, VM_IPS, USERNAME, REGION_NAME, AUDIT_UNABLED, DEFAULT_NS, \
    GLOBAL_ALB_NAME, CASE_TYPE
from new_case.containerplatform.alb.alb import Alb
from new_case.containerplatform.alb.conftest import L1_frontend, alb_ip, create_frontend_casename


@pytest.mark.skipif(not GLOBAL_ALB_NAME, reason="没有传alb名称，默认不跑对应用例")
@pytest.mark.alb
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestDeleteFrontend(object):

    def setup_class(self):
        self.alb_client = Alb()
        self.k8s_namespace = K8S_NAMESPACE

    @pytest.mark.delete
    @pytest.mark.parametrize("data", L1_frontend, ids=create_frontend_casename)
    def 测试删除监听端口L1(self, data):
        ret = self.alb_client.delete_frontend(data['frontend_name'])
        assert ret.status_code == 200, "删除监听端口失败:{}".format(ret.text)
        delete_flag = self.alb_client.check_exists(
            self.alb_client.common_frontend_url(data['frontend_name']), 404)
        assert delete_flag, "删除监听端口失败"
        if CASE_TYPE == "delete":
            pytest.skip("删除数据不需要验证访问")
        type = data['protocol']
        if data['protocol'] == 'tcp':
            type = 'http'
        expect = 'Failed connect'
        if not VM_IPS:
            pytest.skip("没有传虚拟机IP，不需要验证访问")
        ip = VM_IPS.split(';')[0]
        assert alb_ip, "获取alb ip失败"
        frontend_cmd = "date&&curl --connect-timeout 10 -ivk {}://{}:{}".format(type, alb_ip, data['port'])
        visit_result = self.alb_client.access_alb(frontend_cmd, ip, expect)
        assert visit_result[0], "删除监听端口后，之前生成的访问地址，访问失败"

    def 测试删除监听端口L1_不存在(self):
        ret = self.alb_client.delete_frontend('frontend_name')
        assert ret.status_code == 404, "删除不存在的监听端口失败:{}".format(ret.text)

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 测试监听端口删除审计(self):
        payload = {"user_name": USERNAME, "operation_type": "delete", "resource_type": "frontends",
                   "resource_name": L1_frontend[0]['frontend_name']}
        result = self.alb_client.search_audit(payload)
        payload.update({"namespace": DEFAULT_NS, "region_name": REGION_NAME})
        values = self.alb_client.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.alb_client.is_sub_dict(values, result.json()), "审计数据不符合预期"
