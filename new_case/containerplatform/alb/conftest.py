import pytest
from common import settings
from common.settings import VM_IPS
from new_case.containerplatform.alb.alb import Alb
from new_case.containerplatform.application.application import Application
from new_case.containerplatform.secret.secret import Secret
from common.log import logger

alb_app = {
    'project_name': settings.PROJECT_NAME,
    'namespace': settings.K8S_NAMESPACE,
    'app_name': "{}-ares-acp-foralb".format(settings.RESOURCE_PREFIX),
    'deployment_name': "{}-ares-acp-foralb-dep".format(settings.RESOURCE_PREFIX),
    'service_name': "{}-ares-acp-foralb-svc".format(settings.RESOURCE_PREFIX),
    'image': settings.IMAGE,
    'replicas': 2,
    'secret_name': "{}-ares-acp-foralb".format(settings.RESOURCE_PREFIX),
    'secret_key': "tls.crt"
}
l0_alb = {
    'alb_namespace': settings.DEFAULT_NS,
    'alb_name': settings.GLOBAL_ALB_NAME,
    'region_name': settings.REGION_NAME,
    'project_name': "ALL_ALL"
}

l0_frontend = {
    'alb_namespace': settings.DEFAULT_NS,
    'alb_name': settings.GLOBAL_ALB_NAME,
    'port': 11180,
    'protocol': 'http',
    'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11180),
    'policy': ''
}
l0_frontend_update = {
    'alb_namespace': settings.DEFAULT_NS,
    'alb_name': settings.GLOBAL_ALB_NAME,
    'port': 11180,
    'protocol': 'http',
    'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11180),
    'policy': 'sip-hash',
    'service_name': alb_app['service_name'],
    'namespace': settings.K8S_NAMESPACE
}

l0_rule = {
    'alb_namespace': settings.DEFAULT_NS,
    'alb_name': settings.GLOBAL_ALB_NAME,
    'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11180),
    'description': 'http 1 个内部路由，规则指标host,不会话保持，没有url重写',
    'host': "rule.http.test",
    'service_name': alb_app['service_name'],
    'namespace': settings.K8S_NAMESPACE,
    'rule_name': '{}-{}-l0'.format(settings.GLOBAL_ALB_NAME, 11180)
}
l0_rule_update = {
    'alb_namespace': settings.DEFAULT_NS,
    'alb_name': settings.GLOBAL_ALB_NAME,
    'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11180),
    'description': 'http 1 个内部路由，规则指标host+url,源地址哈希，没有url重写',
    'host': "rule.http.test",
    'url': "/update",
    'service_name': alb_app['service_name'],
    'namespace': settings.K8S_NAMESPACE,
    'policy': 'sip-hash',
    'rule_name': l0_rule['rule_name']
}
create_alb = [
    {
        'alb_name': "{}-ares-create".format(settings.RESOURCE_PREFIX),
        'cluster_name': settings.REGION_NAME,
        'address': '1.1.1.1',
        'replicas': 0,
        'nodeSelector': {
            "node-role.kubernetes.io/master": ""
        },
        'project_name': "ALL_ALL",
        "namespace": settings.DEFAULT_NS
    },
    {
        'alb_name': "{}-ares-create-domain".format(settings.RESOURCE_PREFIX),
        'cluster_name': settings.REGION_NAME,
        'address': 'ares.create.domain',
        'replicas': 0,
        'nodeSelector': {
            "node-role.kubernetes.io/node": ""
        },
        'project_name': "notfound",
        "namespace": settings.DEFAULT_NS
    }, {
        'alb_name': "{}-ares-create-project".format(settings.RESOURCE_PREFIX),
        'cluster_name': settings.REGION_NAME,
        'address': '1.1.1.1',
        'replicas': 0,
        'nodeSelector': {
            "node-role.kubernetes.io/error": ""
        },
        'project_name': settings.PROJECT_NAME,
        "namespace": settings.DEFAULT_NS
    }
]
alb_casename = ["分配到全部项目", "分配到不存在的项目", "分配到指定项目"]
l1_alb = [
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'region_name': settings.REGION_NAME,
        'project_name': ""
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'region_name': settings.REGION_NAME,
        'project_name': "error"
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'region_name': settings.REGION_NAME,
        'project_name': settings.PROJECT_NAME
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'region_name': settings.REGION_NAME,
        'project_name': "ALL_ALL"
    },
]
update_alb_casename = ["不分配项目", "分配到不存在的项目", "分配到指定项目", "分配到全部项目"]
create_frontend_casename = ["tcp协议端口无默认路由无会话保持", "tcp协议端口有默认路由有原地址哈希会话保持", "http协议端口有默认路由无会话保持",
                            "http协议端口有默认路由有原地址哈希会话保持", "http协议端口有默认路由有cookie会话保持", "https协议端口有默认路由有原地址哈希会话保持",
                            "https协议端口无默认路由无会话保持"]
L1_frontend = [
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'port': 11181,
        'protocol': 'tcp',
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11181),
        'policy': ''
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'port': 11182,
        'protocol': 'tcp',
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11182),
        'policy': 'sip-hash',
        'service_name': alb_app['service_name'],
        'namespace': settings.K8S_NAMESPACE
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'port': 11183,
        'protocol': 'http',
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11183),
        'policy': '',
        'service_name': alb_app['service_name'],
        'namespace': settings.K8S_NAMESPACE
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'port': 11184,
        'protocol': 'http',
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11184),
        'policy': 'sip-hash',
        'service_name': alb_app['service_name'],
        'namespace': settings.K8S_NAMESPACE
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'port': 11185,
        'protocol': 'http',
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11185),
        'policy': 'cookie',
        'attribute': 'JESSIONID',
        'service_name': alb_app['service_name'],
        'namespace': settings.K8S_NAMESPACE
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'port': 11186,
        'protocol': 'https',
        'secret_name': alb_app['secret_name'],
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11186),
        'policy': 'sip-hash',
        'service_name': alb_app['service_name'],
        'namespace': settings.K8S_NAMESPACE
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'port': 11187,
        'protocol': 'https',
        'secret_name': alb_app['secret_name'],
        'namespace': settings.K8S_NAMESPACE,
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11187),
        'policy': ''
    },
]
update_frontend_casename = ["tcp协议端口有默认路由无会话保持", "tcp协议端口有多个默认路由有原地址哈希会话保持", "http协议端口无默认路由无会话保持",
                            "http协议端口有默认路由无原地址哈希会话保持", "http协议端口有多个默认路由有cookie会话保持", "https协议端口无默认路由无会话保持",
                            "https协议端口有多个默认路由无会话保持"]
L1_frontend_update = [
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'port': 11181,
        'protocol': 'tcp',
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11181),
        'policy': '',
        'service_name': alb_app['service_name'],
        'namespace': settings.K8S_NAMESPACE
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'port': 11182,
        'protocol': 'tcp',
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11182),
        'policy': 'sip-hash',
        'service_name': alb_app['service_name'],
        'service_name2': alb_app['service_name'],
        'service_name3': alb_app['service_name'],
        'namespace': settings.K8S_NAMESPACE
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'port': 11183,
        'protocol': 'http',
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11183),
        'policy': '',
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'port': 11184,
        'protocol': 'http',
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11184),
        'policy': '',
        'service_name': alb_app['service_name'],
        'namespace': settings.K8S_NAMESPACE
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'port': 11185,
        'protocol': 'http',
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11185),
        'policy': 'cookie',
        'attribute': 'JESSIONID',
        'service_name': alb_app['service_name'],
        'service_name2': alb_app['service_name'],
        'service_name3': alb_app['service_name'],
        'namespace': settings.K8S_NAMESPACE
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'port': 11186,
        'protocol': 'https',
        'secret_name': alb_app['secret_name'],
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11186),
        'policy': '',
        'namespace': settings.K8S_NAMESPACE
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'port': 11187,
        'protocol': 'https',
        'secret_name': alb_app['secret_name'],
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11187),
        'policy': '',
        'service_name': alb_app['service_name'],
        'service_name2': alb_app['service_name'],
        'service_name3': alb_app['service_name'],
        'namespace': settings.K8S_NAMESPACE
    },
]

L1_rule = [
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11188),
        'description': 'http1个内部路由，规则指标host+header(equal)+url param(equal),Cookie Key，没有url重写',
        'host': "rule.http.test.param",
        'header': "EQ HEADER h h",
        'param': "EQ PARAM pa pa",
        'policy': 'cookie',
        'attribute': 'JESSIONID',
        'service_name': alb_app['service_name'],
        'namespace': settings.K8S_NAMESPACE,
        'rule_name': '{}-{}-1'.format(settings.GLOBAL_ALB_NAME, 11188),
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11188),
        'description': 'http1个内部路由，规则指标host+ip(all)+cookie(equal),不会话保持，url重写',
        'host': "rule.http.test.rewrite",
        'ip': "OR (RANGE SRC_IP 0.0.0.0 255.255.255.255) (EQ SRC_IP {})".format(VM_IPS.split(';')[0]),
        'cookie': "EQ COOKIE cookie cookie",
        'rewrite': "/redirect",
        'service_name': alb_app['service_name'],
        'namespace': settings.K8S_NAMESPACE,
        'rule_name': '{}-{}-2'.format(settings.GLOBAL_ALB_NAME, 11188),
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11188),
        'description': 'http3个内部路由，规则指标host+url,源地址哈希，没有url重写',
        'host': "rule.http.test.url",
        'url': "/testhttp",
        'policy': 'sip-hash',
        'service_name': alb_app['service_name'],
        'service_name2': alb_app['service_name'],
        'service_name3': alb_app['service_name'],
        'namespace': settings.K8S_NAMESPACE,
        'rule_name': '{}-{}-3'.format(settings.GLOBAL_ALB_NAME, 11188),
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11189),
        'description': 'https1个内部路由，有默认证书，规则指标host+header(all)+url param(all),Cookie Key，没有url重写',
        'secret_name': alb_app['secret_name'],
        'host': "rule.https.test.header",
        'header': "OR (EQ HEADER h h) (RANGE HEADER h a z) (REGEX HEADER h header)",
        'param': "OR (EQ PARAM pa pa) (RANGE PARAM pa 1 5)",
        'policy': 'cookie',
        'attribute': 'JESSIONID',
        'service_name': alb_app['service_name'],
        'namespace': settings.K8S_NAMESPACE,
        'rule_name': '{}-{}-4'.format(settings.GLOBAL_ALB_NAME, 11189),
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11189),
        'description': 'https1个内部路由，有默认证书，规则指标host+url,不会话保持，url重写',
        'secret_name': alb_app['secret_name'],
        'host': "rule.https.test.cookie",
        'url': "/testurl",
        'rewrite': "/redirect",
        'service_name': alb_app['service_name'],
        'namespace': settings.K8S_NAMESPACE,
        'rule_name': '{}-{}-5'.format(settings.GLOBAL_ALB_NAME, 11189),
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11189),
        'description': 'https3个内部路由，有默认证书，规则指标host+ip(range)+cookie(equal),不会话保持，没有url重写',
        'secret_name': alb_app['secret_name'],
        'host': "rule.https.test.redirect",
        'ip': "RANGE SRC_IP 0.0.0.0 255.255.255.255",
        'cookie': "EQ COOKIE cookie cookie",
        'service_name': alb_app['service_name'],
        'service_name2': alb_app['service_name'],
        'service_name3': alb_app['service_name'],
        'namespace': settings.K8S_NAMESPACE,
        'rule_name': '{}-{}-6'.format(settings.GLOBAL_ALB_NAME, 11189),
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11188),
        'description': 'http1个内部路由，规则指标url+打开enableCORS',
        'url': "/enablecors",
        'service_name': alb_app['service_name'],
        'enable_cors': 'True',
        'namespace': settings.K8S_NAMESPACE,
        'rule_name': '{}-{}-7'.format(settings.GLOBAL_ALB_NAME, 11188),
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11188),
        'description': 'http1个内部路由，规则指标url+重定向302',
        'url': "/redirectcode",
        'service_name': alb_app['service_name'],
        'redirect_code': 302,
        'redirect_url': settings.API_URL,
        'namespace': settings.K8S_NAMESPACE,
        'rule_name': '{}-{}-8'.format(settings.GLOBAL_ALB_NAME, 11188),
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11188),
        'description': 'http1个内部路由，规则指标url+关闭CORS+关闭重定向+后端协议HTTP',
        'url': "/backendprotocol",
        'enable_cors': 'False',
        'redirect_url': '',
        'backend_protocol': 'HTTP',
        'service_name': alb_app['service_name'],
        'namespace': settings.K8S_NAMESPACE,
        'rule_name': '{}-{}-9'.format(settings.GLOBAL_ALB_NAME, 11188),
    },
]
create_rule_casename = []
for rule in L1_rule:
    create_rule_casename.append(rule["description"])

L1_rule_update = [
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11188),
        'description': 'http更新成1个内部路由，规则指标host+header(regex)+url param(range),不会话保持，没有url重写',
        'host': "rule.http.test.param",
        'header': "REGEX HEADER h header",
        'param': "RANGE PARAM pa 1 5",
        'policy': '',
        'service_name': alb_app['service_name'],
        'namespace': settings.K8S_NAMESPACE,
        'rule_name': L1_rule[0]['rule_name']
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11188),
        'description': 'http更新成1个内部路由，规则指标url正则+ip(equal)+cookie,不会话保持，url重写',
        'url_type': 'REGEX',
        'url': "/httpurl-(a|b|c)",
        'ip': "EQ SRC_IP {}".format(VM_IPS.split(';')[0]),
        'cookie': "EQ COOKIE cookie cookie",
        'rewrite': "/redirect",
        'service_name': alb_app['service_name'],
        'namespace': settings.K8S_NAMESPACE,
        'rule_name': L1_rule[1]['rule_name']
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11188),
        'description': 'http更新成没有内部路由，规则指标host+url,不会话保持，没有url重写',
        'host': "rule.http.test.host",
        'url': "/httpupdate",
        'policy': '',
        'rule_name': L1_rule[2]['rule_name']
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11189),
        'description': 'https更新成没有内部路由，没有默认证书，规则指标host+header(range)+url param(equal),源地址哈希，没有url重写',
        'host': "rule.https.test.header",
        'header': "RANGE HEADER h a z",
        'param': "EQ PARAM pa pa",
        'policy': 'sip-hash',
        'rule_name': L1_rule[3]['rule_name']
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11189),
        'description': 'https更新成2个内部路由，有默认证书，规则指标host+url+ip(range),Cookie Key，没有url重写',
        'secret_name': alb_app['secret_name'],
        'host': "rule.https.test.url",
        'url': "/httpsurl",
        'ip': "RANGE SRC_IP 0.0.0.0 255.255.255.255",
        'policy': 'cookie',
        'attribute': 'JESSIONID',
        'service_name': alb_app['service_name'],
        'service_name2': alb_app['service_name'],
        'namespace': settings.K8S_NAMESPACE,
        'rule_name': L1_rule[4]['rule_name']
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11189),
        'description': 'https1个内部路由，有默认证书，规则指标host,不会话保持，有url重写',
        'secret_name': alb_app['secret_name'],
        'host': "rule.https.test.redirect",
        'rewrite': "/redirect",
        'service_name': alb_app['service_name'],
        'namespace': settings.K8S_NAMESPACE,
        'rule_name': L1_rule[5]['rule_name']
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11188),
        'description': '更新成http1个内部路由，规则指标url+关闭CORS+重定向301',
        'url': "/enablecors",
        'service_name': alb_app['service_name'],
        'enable_cors': 'False',
        'redirect_code': 301,
        'redirect_url': settings.API_URL,
        'namespace': settings.K8S_NAMESPACE,
        'rule_name': L1_rule[6]['rule_name']
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11188),
        'description': '更新成http1个内部路由，规则指标url+重定向307',
        'url': "/redirectcode",
        'service_name': alb_app['service_name'],
        'redirect_code': 307,
        'redirect_url': settings.API_URL,
        'namespace': settings.K8S_NAMESPACE,
        'rule_name': L1_rule[7]['rule_name']
    },
    {
        'alb_namespace': settings.DEFAULT_NS,
        'alb_name': settings.GLOBAL_ALB_NAME,
        'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11188),
        'description': '更新成http1个内部路由，规则指标url+关闭CORS+关闭重定向+后端协议HTTPS',
        'url': "/backendprotocol",
        'enable_cors': 'False',
        'redirect_url': '',
        'backend_protocol': 'HTTPS',
        'namespace': 'default',
        'service_name': 'kubernetes',
        'port': 443,
        'rule_name': L1_rule[8]['rule_name']
    },
]
update_rule_casename = []
for rule in L1_rule_update:
    update_rule_casename.append(rule["description"])

alb_client = Alb()
try:
    ret = alb_client.detail_alb(settings.GLOBAL_ALB_NAME, settings.DEFAULT_NS)
    alb_ip = alb_client.get_value(ret.json(), "spec.address")
except Exception as e:
    logger.error("获取alb地址出错:{},返回信息{}".format(e, ret.text))
    alb_ip = ""

prepare_frontend = [{
    'alb_namespace': settings.DEFAULT_NS,
    'alb_name': settings.GLOBAL_ALB_NAME,
    'port': 11188,
    'protocol': 'http',
    'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11188),
    'policy': '',
    'namespace': settings.K8S_NAMESPACE
}, {
    'alb_namespace': settings.DEFAULT_NS,
    'alb_name': settings.GLOBAL_ALB_NAME,
    'port': 11189,
    'protocol': 'https',
    'secret_name': alb_app['secret_name'],
    'frontend_name': '{}-{}'.format(settings.GLOBAL_ALB_NAME, 11189),
    'policy': '',
    'namespace': settings.K8S_NAMESPACE
}
]


@pytest.fixture(scope="session", autouse=True)
def prepare_template():
    secret_client = Secret()
    secret_client.create_secret('./test_data/secret/alb_secret.jinja2', alb_app)
    app_client = Application()
    app_client.create_app('./test_data/application/create_app.jinja2', alb_app)
    app_client.get_app_status(alb_app['namespace'], alb_app['app_name'], 'Running')
    for data in prepare_frontend:
        alb_client.create_frontend("test_data/alb/create_frontend.jinja2", data)
    prepare_data = {
        'app_name': alb_app['app_name'],
        'secret_name': alb_app['secret_name']
    }
    yield prepare_data
    if settings.CASE_TYPE not in ("prepare", "upgrade"):
        app_client.delete_app(alb_app['namespace'], prepare_data['app_name'])
        secret_client.delete_secret(prepare_data['secret_name'], alb_app['namespace'])
        for data in prepare_frontend:
            alb_client.delete_frontend(data['frontend_name'])
