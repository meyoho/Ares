import pytest
import copy
from common.settings import RERUN_TIMES, K8S_NAMESPACE, GLOBAL_ALB_NAME, DEFAULT_NS, VM_IPS, PROJECT_NAME
from new_case.containerplatform.alb.alb import Alb
from new_case.containerplatform.alb.conftest import l0_alb, l0_frontend, l0_frontend_update, l0_rule, l0_rule_update, \
    alb_app
from new_case.containerplatform.application.application import Application


@pytest.mark.skipif(not GLOBAL_ALB_NAME, reason="没有传alb名称，默认不跑对应用例")
@pytest.mark.alb
@pytest.mark.BAT
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestAlb(object):

    def setup_class(self):
        self.alb_client = Alb()
        self.k8s_namespace = K8S_NAMESPACE
        self.frontend_name = l0_frontend['frontend_name']
        self.alb_name = GLOBAL_ALB_NAME
        self.alb_namespace = DEFAULT_NS
        self.app_client = Application()
        # 删除旗下关联的规则
        ret = self.alb_client.list_rule(GLOBAL_ALB_NAME, l0_frontend['frontend_name'])
        for item in ret.json()['items']:
            rule_name = self.alb_client.get_value(item, "metadata.name")
            if not self.alb_client.check_exists(self.alb_client.common_rule_url(rule_name), 404, expect_cnt=1):
                self.alb_client.delete_rule(rule_name)

    def 测试负载均衡列表(self):
        ret = self.alb_client.list_alb()
        assert ret.status_code == 200, "获取负载均衡列表失败:{}".format(ret.text)
        content = self.alb_client.get_k8s_resource_data(ret.json(), self.alb_name, list_key="items")
        values = self.alb_client.generate_jinja_data("verify_data/alb/alb_response.jinja2", l0_frontend)
        assert self.alb_client.is_sub_dict(values, content), \
            "获取负载均衡列表比对数据失败，返回数据{}，期望数据{}".format(content, values)

    def 测试负载均衡按名称过滤(self):
        ret = self.alb_client.list_alb(selector=self.alb_name)
        assert ret.status_code == 200, "负载均衡按名称过滤失败:{}".format(ret.text)
        content = self.alb_client.get_k8s_resource_data(ret.json(), self.alb_name, list_key="items")
        values = self.alb_client.generate_jinja_data("verify_data/alb/alb_response.jinja2", l0_frontend)
        assert self.alb_client.is_sub_dict(values, content), \
            "负载均衡按名称过滤比对数据失败，返回数据{}，期望数据{}".format(content, values)

    def 测试负载均衡更新项目(self):
        ret = self.alb_client.update_alb(self.alb_name, 'test_data/alb/patch_alb.jinja2', l0_alb)
        assert ret.status_code == 200, "负载均衡更新项目失败:{}".format(ret.text)
        values = self.alb_client.generate_jinja_data("verify_data/alb/patch_alb_response.jinja2", l0_alb)
        assert self.alb_client.is_sub_dict(values, ret.json()), \
            "负载均衡更新项目比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)

    def 测试负载均衡按项目过滤(self):
        ret = self.alb_client.list_alb(project=PROJECT_NAME)
        assert ret.status_code == 200, "负载均衡按项目过滤失败:{}".format(ret.text)
        content = self.alb_client.get_k8s_resource_data(ret.json(), self.alb_name, list_key="items")
        values = self.alb_client.generate_jinja_data("verify_data/alb/alb_response.jinja2", l0_frontend)
        assert self.alb_client.is_sub_dict(values, content), \
            "负载均衡按项目过滤比对数据失败，返回数据{}，期望数据{}".format(content, values)

    def 测试负载均衡详情(self):
        ret = self.alb_client.detail_alb(self.alb_name, self.alb_namespace)
        assert ret.status_code == 200, "获取负载均衡详情失败:{}".format(ret.text)
        values = self.alb_client.generate_jinja_data("verify_data/alb/alb_response.jinja2", l0_frontend)
        assert self.alb_client.is_sub_dict(values, ret.json()), \
            "获取负载均衡详情比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)
        global alb_ip
        alb_ip = self.alb_client.get_value(ret.json(), "spec.address")

    def 测试添加监听端口http(self):
        self.alb_client.delete_frontend(l0_frontend['frontend_name'])
        ret = self.alb_client.create_frontend("test_data/alb/create_frontend.jinja2", l0_frontend)
        assert ret.status_code == 201, "添加监听端口http失败:{}".format(ret.text)
        values = self.alb_client.generate_jinja_data("verify_data/alb/create_frontend_response.jinja2", l0_frontend)
        assert self.alb_client.is_sub_dict(values, ret.json()), \
            "添加监听端口http比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)
        global resourceVersion
        resourceVersion = self.alb_client.get_value(ret.json(), "metadata.resourceVersion")

    def 测试监听端口列表(self):
        ret = self.alb_client.list_frontend(self.alb_name)
        assert ret.status_code == 200, "监听端口列表失败:{}".format(ret.text)
        content = self.alb_client.get_k8s_resource_data(ret.json(), l0_frontend['frontend_name'], list_key="items")
        values = self.alb_client.generate_jinja_data("verify_data/alb/create_frontend_response.jinja2", l0_frontend)
        assert self.alb_client.is_sub_dict(values, content), \
            "监听端口列表比对数据失败，返回数据{}，期望数据{}".format(content, values)

    def 测试更新监听端口http(self):
        l0_frontend_update.update({'resourceVersion': resourceVersion})
        ret = self.alb_client.update_frontend(l0_frontend['frontend_name'], "test_data/alb/create_frontend.jinja2",
                                              l0_frontend_update)
        assert ret.status_code == 200, "更新监听端口http失败:{}".format(ret.text)
        values = self.alb_client.generate_jinja_data("verify_data/alb/create_frontend_response.jinja2",
                                                     l0_frontend_update)
        assert self.alb_client.is_sub_dict(values, ret.json()), \
            "更新监听端口http比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)

    def 测试获取监听端口http详情(self):
        ret = self.alb_client.detail_frontend(l0_frontend_update['frontend_name'])
        assert ret.status_code == 200, "获取监听端口http详情失败:{}".format(ret.text)
        values = self.alb_client.generate_jinja_data("verify_data/alb/create_frontend_response.jinja2",
                                                     l0_frontend_update)
        assert self.alb_client.is_sub_dict(values, ret.json()), \
            "获取监听端口http详情比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)

    def 测试添加规则http(self):
        ret = self.alb_client.create_rule("test_data/alb/create_rule.jinja2", l0_rule)
        assert ret.status_code == 201, "添加规则http失败:{}".format(ret.text)
        values = self.alb_client.generate_jinja_data("verify_data/alb/create_rule_response.jinja2", l0_rule)
        assert self.alb_client.is_sub_dict(values, ret.json()), \
            "添加规则http比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)
        global resourceVersion
        resourceVersion = self.alb_client.get_value(ret.json(), "metadata.resourceVersion")

    def 测试规则列表(self):
        ret = self.alb_client.list_rule(self.alb_name, self.frontend_name)
        assert ret.status_code == 200, "规则列表失败:{}".format(ret.text)
        content = self.alb_client.get_k8s_resource_data(ret.json(), l0_rule['rule_name'], list_key="items")
        values = self.alb_client.generate_jinja_data("verify_data/alb/create_rule_response.jinja2", l0_rule)
        assert self.alb_client.is_sub_dict(values, content), \
            "规则列表比对数据失败，返回数据{}，期望数据{}".format(content, values)

    def 测试更新规则http(self):
        l0_rule_update.update({'resourceVersion': resourceVersion})
        ret = self.alb_client.update_rule(l0_rule['rule_name'], "test_data/alb/create_rule.jinja2",
                                          l0_rule_update)
        assert ret.status_code == 200, "更新规则http失败:{}".format(ret.text)
        values = self.alb_client.generate_jinja_data("verify_data/alb/create_rule_response.jinja2",
                                                     l0_rule_update)
        assert self.alb_client.is_sub_dict(values, ret.json()), \
            "更新规则http比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)

    def 测试获取规则http详情(self):
        ret = self.alb_client.detail_rule(l0_rule_update['rule_name'])
        assert ret.status_code == 200, "获取规则http详情失败:{}".format(ret.text)
        values = self.alb_client.generate_jinja_data("verify_data/alb/create_rule_response.jinja2",
                                                     l0_rule_update)
        assert self.alb_client.is_sub_dict(values, ret.json()), \
            "获取规则http详情比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)

    def 测试应用地址包含监听端口和规则生成的地址并验证访问(self):
        ret = self.app_client.get_app_address(alb_app['namespace'], alb_app['app_name'])
        assert ret.status_code == 200, "获取访问地址失败:{}".format(ret.text)
        verify_app_data = copy.deepcopy(alb_app)
        verify_app_data.update(l0_frontend_update)
        verify_app_data.update({'host': alb_ip})
        values = self.app_client.generate_jinja_data("verify_data/alb/address_frontend.jinja2", verify_app_data)
        assert self.app_client.is_sub_dict(values, ret.json()), \
            "获取访问地址-监听端口默认内部路由生成 比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)

        verify_app_data.update(l0_rule_update)
        values = self.app_client.generate_jinja_data("verify_data/alb/address_frontend.jinja2", verify_app_data)
        assert self.app_client.is_sub_dict(values, ret.json()), \
            "获取访问地址-规则生成 比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)
        if not VM_IPS:
            pytest.skip("没有传虚拟机IP，不需要验证访问")
        ip = VM_IPS.split(';')[0]
        frontend_cmd = "date&&curl --connect-timeout 10 -vi http://{}:{}".format(alb_ip, l0_frontend['port'])
        visit_result = self.alb_client.access_alb(frontend_cmd, ip, verify_app_data['app_name'])
        assert visit_result[0], "监听端口默认内部路由生成的访问地址，访问失败"

        rule_cmd = "date&&curl --connect-timeout 10 -vi -H \"Host: {host}\" http://{alb_ip}:{port}{url}".format(
            host=l0_rule_update['host'], alb_ip=alb_ip, port=l0_frontend['port'], url=l0_rule_update['url'])
        visit_result = self.alb_client.access_alb(rule_cmd, ip, verify_app_data['app_name'])
        assert visit_result[0], "规则生成的访问地址，访问失败"

    def 测试删除规则http(self):
        ret = self.alb_client.delete_rule(l0_rule_update['rule_name'])
        assert ret.status_code == 200, "删除规则http失败:{}".format(ret.text)
        delete_flag = self.alb_client.check_exists(
            self.alb_client.common_rule_url(l0_rule_update['rule_name']), 404)
        assert delete_flag, "删除规则http失败"

    def 测试删除监听端口http(self):
        ret = self.alb_client.delete_frontend(l0_frontend_update['frontend_name'])
        assert ret.status_code == 200, "删除监听端口http失败:{}".format(ret.text)
        delete_flag = self.alb_client.check_exists(
            self.alb_client.common_frontend_url(l0_frontend_update['frontend_name']), 404)
        assert delete_flag, "删除监听端口http失败"
