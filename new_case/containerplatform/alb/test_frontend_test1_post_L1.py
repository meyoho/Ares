import pytest

from common.settings import RERUN_TIMES, K8S_NAMESPACE, VM_IPS, GLOBAL_ALB_NAME, CASE_TYPE
from new_case.containerplatform.alb.alb import Alb
from new_case.containerplatform.alb.conftest import L1_frontend, alb_ip, create_frontend_casename


@pytest.mark.skipif(not GLOBAL_ALB_NAME, reason="没有传alb名称，默认不跑对应用例")
@pytest.mark.alb
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestPostFrontend(object):

    def setup_class(self):
        self.alb_client = Alb()
        self.k8s_namespace = K8S_NAMESPACE

    @pytest.mark.prepare
    @pytest.mark.parametrize("data", L1_frontend, ids=create_frontend_casename)
    def 测试添加监听端口和地址访问(self, data):
        # 删除旗下关联的规则
        ret = self.alb_client.list_rule(GLOBAL_ALB_NAME, data['frontend_name'])
        for item in ret.json()['items']:
            rule_name = self.alb_client.get_value(item, "metadata.name")
            if not self.alb_client.check_exists(self.alb_client.common_rule_url(rule_name), 404, expect_cnt=1):
                self.alb_client.delete_rule(rule_name)
        if not self.alb_client.check_exists(self.alb_client.common_frontend_url(data['frontend_name']), 404,
                                            expect_cnt=1):
            self.alb_client.delete_frontend(data['frontend_name'])

        ret = self.alb_client.create_frontend("test_data/alb/create_frontend.jinja2", data)
        assert ret.status_code == 201, "添加监听端口失败:{}".format(ret.text)
        values = self.alb_client.generate_jinja_data("verify_data/alb/create_frontend_response.jinja2", data)
        assert self.alb_client.is_sub_dict(values, ret.json()), \
            "添加监听端口比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)
        if CASE_TYPE == "prepare":
            pytest.skip("准备数据不需要验证访问")
        if not VM_IPS:
            pytest.skip("没有传虚拟机IP，不需要验证访问")
        type = data['protocol']
        if data['protocol'] == 'tcp':
            type = 'http'
        if 'service_name' in data:
            expect = '200 OK'
        else:
            if data['protocol'] == 'tcp':
                expect = 'Connection reset by peer'
            elif data['protocol'] == 'https':
                expect = '502 Bad Gateway'
            else:
                expect = 'Failed connect'
        ip = VM_IPS.split(';')[0]
        assert alb_ip, "获取alb ip失败"
        frontend_cmd = "date&&curl --connect-timeout 10 -vik {}://{}:{}".format(type, alb_ip, data['port'])
        visit_result = self.alb_client.access_alb(frontend_cmd, ip, expect)
        assert visit_result[0], "添加监听端口后，生成的访问地址，访问失败"
        if "'policy': 'sip-hash'" in str(data) and 'service_name' in data:
            visit_result1 = \
                self.alb_client.access_alb(frontend_cmd, ip, '200 OK')[1].split("I am on")[1].split('</h3>')[0]
            visit_result2 = \
                self.alb_client.access_alb(frontend_cmd, ip, '200 OK')[1].split("I am on")[1].split('</h3>')[0]
            assert visit_result1 == visit_result2, \
                "监听端口中的会话保持-源地址哈希，访问失败 第一次返回{} 第二次返回 {}".format(visit_result1, visit_result2)
        if "'policy': 'cookie'" in str(data) and 'service_name' in data:
            visit_result = self.alb_client.access_alb(frontend_cmd, ip, 'JESSIONID')
            assert visit_result[0], "添加监听端口后，生成的访问地址，访问结果中没有JESSIONID"
            JESSIONID = visit_result[1].split("JESSIONID=")[1].split(";")[0]
            visit_result1 = visit_result[1].split("I am on")[1].split('</h3>')[0]
            cookie_cmd = 'date&&curl --connect-timeout 10 -ivk {}://{}:{} -b JESSIONID={}'.format(type, alb_ip,
                                                                                                  data['port'],
                                                                                                  JESSIONID)
            visit_result = self.alb_client.access_alb(cookie_cmd, ip, '200 OK')
            visit_result2 = visit_result[1].split("I am on")[1].split('</h3>')[0]
            assert visit_result1 == visit_result2, \
                "监听端口中的会话保持-Cookie key，访问失败 第一次返回{} 第二次返回 {}".format(visit_result1, visit_result2)

    def 测试添加端口L1_已存在(self):
        data = L1_frontend[0]
        ret = self.alb_client.create_frontend("test_data/alb/create_frontend.jinja2", data)
        assert ret.status_code == 409, "创建已存在的监听端口失败:{}".format(ret.text)
