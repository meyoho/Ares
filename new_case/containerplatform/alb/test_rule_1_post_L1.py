import pytest

from common.settings import RERUN_TIMES, K8S_NAMESPACE, VM_IPS, GLOBAL_ALB_NAME, CASE_TYPE
from new_case.containerplatform.alb.alb import Alb
from new_case.containerplatform.alb.conftest import L1_rule, alb_ip, prepare_frontend, create_rule_casename


@pytest.mark.skipif(not GLOBAL_ALB_NAME, reason="没有传alb名称，默认不跑对应用例")
@pytest.mark.alb
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestPostRule(object):

    def setup_class(self):
        self.alb_client = Alb()
        self.k8s_namespace = K8S_NAMESPACE

    @pytest.mark.prepare
    @pytest.mark.parametrize("data", L1_rule, ids=create_rule_casename)
    def 测试添加规则和地址访问L1(self, data):
        self.alb_client.delete_rule(data['rule_name'])
        ret = self.alb_client.create_rule("test_data/alb/create_rule.jinja2", data)
        assert ret.status_code == 201, "添加规则失败:{}".format(ret.text)
        values = self.alb_client.generate_jinja_data("verify_data/alb/create_rule_response.jinja2", data)
        assert self.alb_client.is_sub_dict(values, ret.json()), \
            "添加规则比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)
        if CASE_TYPE == "prepare":
            pytest.skip("准备数据不需要跑验证")
        if not VM_IPS:
            pytest.skip("没有传虚拟机IP，不需要验证访问")
        assert alb_ip, "获取alb ip失败"
        type = self.alb_client.get_uuid_accord_name(prepare_frontend, {'frontend_name': data['frontend_name']},
                                                    'protocol')
        port = self.alb_client.get_uuid_accord_name(prepare_frontend, {'frontend_name': data['frontend_name']},
                                                    'port')
        if type == 'tcp':
            type = 'http'
        if 'host' in data:
            host = data['host']
        else:
            host = alb_ip
        if 'url' in data:
            url = data['url']
        else:
            url = ''
        param = header = cookie = ""
        if 'param' in data:
            if 'OR' in data['param']:
                param_key = data['param'].split('(')[1].split(')')[0].split(' ')[2]
                param_value = data['param'].split('(')[1].split(')')[0].split(' ')[3]
            else:
                param_key = data['param'].split(' ')[2]
                param_value = data['param'].split(' ')[3]
            param = "?{}={}".format(param_key, param_value)
        if 'header' in data:
            if 'OR' in data['header']:
                header_key = data['header'].split('(')[1].split(')')[0].split(' ')[2]
                header_value = data['header'].split('(')[1].split(')')[0].split(' ')[3]
            else:
                header_key = data['header'].split(' ')[2]
                header_value = data['header'].split(' ')[3]
            header = " -H {}:{} ".format(header_key, header_value)
        if 'cookie' in data:
            if 'OR' in data['cookie']:
                cookie_key = data['cookie'].split('(')[1].split(')')[0].split(' ')[2]
                cookie_value = data['cookie'].split('(')[1].split(')')[0].split(' ')[3]
            else:
                cookie_key = data['cookie'].split(' ')[2]
                cookie_value = data['cookie'].split(' ')[3]
            cookie = " --cookie {}={} ".format(cookie_key, cookie_value)
        ip = VM_IPS.split(';')[0]
        rule_cmd = "date&&curl --connect-timeout 10 -vik -H \"Host: {host}\" {type}://{alb_ip}:{port}{url}{param} {header} {cookie}".format(
            alb_ip=alb_ip, host=host, type=type, port=port, url=url, param=param, header=header, cookie=cookie)
        expect = '200 OK'
        if 'rewrite' in data:
            expect = 'redirect'
        if 'redirect_code' in data:
            expect = str(data['redirect_code'])
        if 'service_name' not in data:
            expect = '502 Bad Gateway'
        if 'ip' in data and 'RANGE SRC_IP' not in data['ip']:
            expect = '502 Bad Gateway'
        if type == 'https' and 'secret_name' not in data:
            expect = 'Failed connect'
        visit_result = self.alb_client.access_alb(rule_cmd, ip, expect)
        assert visit_result[0], "添加规则后，生成的访问地址，访问失败"
        assert visit_result == visit_result, "添加规则后，生成的访问地址，访问失败"
        if "'policy': 'sip-hash'" in str(data) and 'service_name' in data:
            visit_result1 = self.alb_client.access_alb(rule_cmd, ip, '200 OK')[1].split("I am on")[1].split('</h3>')[0]
            visit_result2 = self.alb_client.access_alb(rule_cmd, ip, '200 OK')[1].split("I am on")[1].split('</h3>')[0]
            assert visit_result1 == visit_result2, \
                "规则中的会话保持-源地址哈希，访问失败 第一次返回{} 第二次返回 {}".format(visit_result1, visit_result2)
        if "'policy': 'cookie'" in str(data) and 'service_name' in data:
            visit_result = self.alb_client.access_alb(rule_cmd, ip, 'JESSIONID')
            assert visit_result[0], "添加规则后，生成的访问地址，访问结果中没有JESSIONID"
            JESSIONID = visit_result[1].split("JESSIONID=")[1].split(";")[0]
            visit_result1 = visit_result[1].split("I am on")[1].split('</h3>')[0]
            cookie_cmd = '{} -b JESSIONID={}'.format(rule_cmd, JESSIONID)
            visit_result = self.alb_client.access_alb(cookie_cmd, ip, '200 OK')
            visit_result2 = visit_result[1].split("I am on")[1].split('</h3>')[0]
            assert visit_result1 == visit_result2, \
                "规则中的会话保持-Cookie key，访问失败 第一次返回{} 第二次返回 {}".format(visit_result1, visit_result2)
        if "'enable_cors': 'true'" in str(data) and 'service_name' in data:
            visit_result = self.alb_client.access_alb(rule_cmd, ip, 'Access-Control-Allow-Origin:')
            assert visit_result[0], "添加规则后，生成的访问地址，cors访问结果中没有Access-Control-Allow-Origin:"

    def 测试添加规则L1_已存在(self):
        data = L1_rule[0]
        ret = self.alb_client.create_rule("test_data/alb/create_rule.jinja2", data)
        assert ret.status_code == 409, "创建已存在的规则失败:{}".format(ret.text)
