import pytest
from common import settings
from new_case.containerplatform.storageclass.scs import Scs


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestScSuitePut(object):

    def setup_class(self):
        self.scs = Scs()

    def 测试_更新存储类_必填参数_name_provisioner_可选参数只有1个_annotations(self):
        data = Scs.data_list["必填参数_可选参数只有1个_annotations"]
        data.update({"annotations": {"storageclass.kubernetes.io/is-default-class": "true"}})
        update_result = self.scs.update_scs_jinja2(data['scs_name'], './test_data/scs/scs.jinja2', data=data)
        assert update_result.status_code == 200, "更新scs{}失败:{}".format(data['scs_name'], update_result.text)

        value = self.scs.generate_jinja_data("./verify_data/storageclass/update_scs.jinja2", data)
        assert self.scs.is_sub_dict(value, update_result.json()), \
            "更新存储类_必填参数_name_provisioner_可选参数只有1个_annotations比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

    @pytest.mark.skipif(settings.AUDIT_UNABLED, reason="do not have audit")
    def 测试存储类更新审计(self):
        payload = {"user_name": settings.USERNAME, "operation_type": "update", "resource_type": "storageclasses",
                   "resource_name": self.data_list_1[0]['scs_name']}
        result = self.scs.search_audit(payload)
        payload.update({"namespace": "", "region_name": settings.REGION_NAME})
        values = self.scs.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.scs.is_sub_dict(values, result.json()), "审计数据不符合预期"
