import sys
from common.base_request import Common
from common.exceptions import ParseResponseError
from common.log import logger
from common import settings


class Scs(Common):
    def __init__(self):
        super(Scs, self).__init__()
        self.genetate_global_info()
        self.scs_list_result = self.list_scs()

    def get_common_scs_url_v1(self, scs_name=''):
        return 'kubernetes/{}/apis/storage.k8s.io/v1/storageclasses/{}'.format(self.region_name, scs_name)

    def get_common_scs_list_url(self, limit=1, cnt=""):
        return limit and "kubernetes/{}/apis/storage.k8s.io/v1/storageclasses?limit={}&continue={}" \
            .format(self.region_name, limit, cnt)

    def get_common_scs_sesrch_url_v1(self, limit=1, scs_name=None):
        return limit and "acp/v1/resources/search/kubernetes/{}/apis/storage.k8s.io/v1/storageclasses?" \
                         "limit={}&keyword={}&field=metadata.name".format(self.region_name, limit, scs_name)

    def list_scs(self, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_scs_url_v1()
        return self.send(method='get', path=url, params={}, auth=auth)

    def create_scs(self, file, data, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_scs_url_v1()
        data = self.generate_data(file, data)
        return self.send(method='post', path=url, data=data, params={}, auth=auth)

    def update_scs(self, scs_name, file, data, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_scs_url_v1(scs_name)
        data = self.generate_data(file, data)
        return self.send(method='put', path=url, data=data, params={}, auth=auth)

    def get_scs_detail(self, scs_name, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_scs_url_v1(scs_name)
        return self.send(method='get', path=url, params={}, auth=auth)

    def delete_scs(self, scs_name, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_scs_url_v1(scs_name)
        return self.send(method='delete', path=url, params={}, auth=auth)

    def get_default_size(self):
        default_size = 0
        list_result = self.list_scs()
        if list_result.status_code != 200:
            return default_size
        for detail in list_result.json():
            try:
                default = self.get_value(detail,
                                         "kubernetes#metadata#annotations#storageclass.kubernetes.io/is-default-class",
                                         "#")
                if default == "true":
                    default_size += 1
            except (KeyError, ValueError, IndexError, ParseResponseError):
                continue
        return default_size

    def create_scs_jinja2(self, file, data):
        path = self.get_common_scs_url_v1()
        data = self.generate_jinja_data(file, data)
        logger.info(data)
        return self.send(method='post', path=path, json=data, params={})

    def get_scs_list(self, limit=None, cnt=""):
        path = self.get_common_scs_list_url(limit=limit, cnt=cnt)
        return self.send(method='get', path=path, params={})

    def update_scs_jinja2(self, scs_name, file, data):
        path = self.get_common_scs_url_v1(scs_name=scs_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='put', path=path, json=data, params={})

    def delete_scs_jinja2(self, scs_name, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_scs_url_v1(scs_name)
        return self.send(method='delete', path=url, params={}, auth=auth)

    def search_scs_jinja2_v1(self, limit=None, scs_name=""):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_scs_sesrch_url_v1(limit=limit, scs_name=scs_name)
        return self.send(method='get', path=url)

    data_list = {
        "必填参数_可选参数为空": {
            "scs_name": '{}-ares-sc'.format(settings.RESOURCE_PREFIX)
        },
        "必填参数_可选参数只有1个_annotations": {
            "scs_name": '{}-ares-sc-1'.format(settings.RESOURCE_PREFIX),
            "annotations": {"storageclass.kubernetes.io/is-default-class": "false"}
        },
        "必填参数_可选参数只有1个_clusterNamespace": {
            "scs_name": '{}-ares-sc-2'.format(settings.RESOURCE_PREFIX),
            "parameters_clusterNamespace": " ",
            "clustername": "rook-ceph"
        },
        "必填参数_可选参数只有1个_pool": {
            "scs_name": '{}-ares-sc-3'.format(settings.RESOURCE_PREFIX),
            "parameters_pool": " ",
            "pool": "replicapool"
        }
    }
