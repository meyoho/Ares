import pytest
from common import settings
from new_case.containerplatform.storageclass.scs import Scs


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestScSuiteDelete(object):

    def setup_class(self):
        self.scs = Scs()

    @pytest.mark.parametrize("key", Scs.data_list)
    def 测试_删除存储卷(self, key):
        data = Scs.data_list[key]
        delete_result = self.scs.delete_scs_jinja2(data['scs_name'])
        assert delete_result.status_code == 200, "删除存储卷{}失败:{}".format(data['scs_name'], delete_result.text)

    @pytest.mark.skipif(settings.AUDIT_UNABLED, reason="do not have audit")
    def 测试存储类删除审计(self):
        payload = {"user_name": settings.USERNAME, "operation_type": "delete", "resource_type": "storageclasses",
                   "resource_name": self.data_list_1[0]['scs_name']}
        result = self.scs.search_audit(payload)
        payload.update({"namespace": "", "region_name": settings.REGION_NAME})
        values = self.scs.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.scs.is_sub_dict(values, result.json()), "审计数据不符合预期"
