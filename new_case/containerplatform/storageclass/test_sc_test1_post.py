import pytest
from common import settings
from new_case.containerplatform.storageclass.scs import Scs


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestScSuitePost(object):

    def setup_class(self):
        self.scs = Scs()

    @pytest.mark.parametrize("key", Scs.data_list)
    def 测试_创建存储类(self, key):
        data = Scs.data_list[key]
        create_result = self.scs.create_scs_jinja2("./test_data/scs/scs.jinja2", data=data)
        assert create_result.status_code == 201, "创建存储类{}失败:{}".format(data['scs_name'], create_result.text)

        value = self.scs.generate_jinja_data("./verify_data/storageclass/create_scs.jinja2", data)
        assert self.scs.is_sub_dict(value, create_result.json()), \
            "创建存储类_必填参数_name_provisioner_可选参数为空比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

    @pytest.mark.skipif(settings.AUDIT_UNABLED, reason="do not have audit")
    def 不测试存储类创建审计(self):
        payload = {"user_name": settings.USERNAME, "operation_type": "create", "resource_type": "storageclasses",
                   "resource_name": self.data_list[0]['scs_name']}
        result = self.scs.search_audit(payload)
        payload.update({"namespace": "", "region_name": settings.REGION_NAME, "code": 201})
        values = self.scs.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.scs.is_sub_dict(values, result.json()), "审计数据不符合预期"
