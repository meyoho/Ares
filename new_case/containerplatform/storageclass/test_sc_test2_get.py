import pytest
from common import settings
from new_case.containerplatform.storageclass.scs import Scs


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestScSuiteGet(object):

    def setup_class(self):
        self.scs = Scs()

    def 测试_获取scs列表_有limit参数(self):
        ret = self.scs.get_scs_list(limit=20)
        assert ret.status_code == 200, "获取scs列表失败:{}".format(ret.text)
        ret2 = self.scs.get_scs_list(limit=1)
        scs_num = len(ret2.json()["items"])
        assert scs_num == 1, "获取scs列表,limit=1时失败,预期返回1个scs,实际返回{}个".format(scs_num)

        global name
        name = Scs.get_value(ret2.json(), "items.0.metadata.name")

        global cnt
        cnt = Scs.get_value(ret2.json(), "metadata.continue")

    def 测试_获取scs列表_有limit参数和continue参数(self):
        ret = self.scs.get_scs_list(limit=1, cnt=cnt)
        assert ret.status_code == 200, "获取scs列表_有limit和continue参数:{}".format(ret.text)
        name_continue = Scs.get_value(ret.json(), "items.0.metadata.name")
        assert name_continue != name, "分页数据相同，scs第一页数据:{},scs第二页数据:{}".format(name, name_continue)

    def 测试_搜索scs(self):
        data = Scs.data_list["必填参数_可选参数只有1个_annotations"]
        ser = self.scs.search_scs_jinja2_v1(limit=20, scs_name=data["scs_name"])
        assert ser.status_code == 200, "没有搜索到{}，{}".format(data["scs_name"], ser.text)

        value = self.scs.generate_jinja_data("./verify_data/storageclass/search_scs.jinja2", data)
        assert self.scs.is_sub_dict(value, ser.json()), "搜索scs比对数据失败，返回数据:{},期望数据:{}".format(ser.json(), value)
