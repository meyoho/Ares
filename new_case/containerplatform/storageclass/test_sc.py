import pytest

from common.settings import RERUN_TIMES, RESOURCE_PREFIX, CASE_TYPE
from new_case.containerplatform.storageclass.scs import Scs


@pytest.mark.archon
@pytest.mark.BAT
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestScSuite(object):
    def setup_class(self):
        self.sc = Scs()
        self.sc_name = '{}-ares-sc'.format(RESOURCE_PREFIX)
        self.sc_name2 = '{}-ares-sc-2'.format(RESOURCE_PREFIX)
        self.teardown_class(self)
        global data
        data = {"scs_name": self.sc_name}

    def teardown_class(self):
        if CASE_TYPE not in ("prepare", "upgrade", "delete"):
            self.sc.delete_scs_jinja2(self.sc_name)

    @pytest.mark.prepare
    def 测试存储类增加(self):
        createsc_result = self.sc.create_scs_jinja2("./test_data/scs/scs.jinja2",
                                                    {"scs_name": self.sc_name})
        assert createsc_result.status_code == 201, "创建存储类失败:{}".format(createsc_result.text)
        value = self.sc.generate_jinja_data("./verify_data/storageclass/create_scs.jinja2", data)
        assert self.sc.is_sub_dict(value, createsc_result.json()), \
            "创建存储卷比对数据失败，返回数据:{},期望数据:{}".format(createsc_result.json(), value)

    @pytest.mark.upgrade
    def 测试存储类列表(self):
        # list sc
        list_result = self.sc.get_scs_list(limit=20)
        assert list_result.status_code == 200, list_result.text
        content = self.sc.get_k8s_resource_data(list_result.json(), self.sc_name, list_key="items")
        value = self.sc.generate_jinja_data("./verify_data/storageclass/create_scs.jinja2", data)
        value.pop('apiVersion')
        value.pop('kind')
        assert self.sc.is_sub_dict(value, content), "获取存储卷列表比对数据失败，返回数据:{},期望数据:{}".format(content, value)

    @pytest.mark.upgrade
    def 测试存储类更新(self):
        # update sc
        data.update({"annotations": {"storageclass.kubernetes.io/is-default-class": "true"}})
        update_result = self.sc.update_scs_jinja2(data['scs_name'], './test_data/scs/scs.jinja2', data=data)
        assert update_result.status_code == 200, "更新存储类出错:{}".format(update_result.text)
        value = self.sc.generate_jinja_data("./verify_data/storageclass/update_scs.jinja2", data)
        assert self.sc.is_sub_dict(value, update_result.json()), \
            "更新存储卷比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

    @pytest.mark.upgrade
    def 测试存储类详情(self):
        # detail sc
        detail_result = self.sc.get_scs_detail(self.sc_name)
        assert detail_result.status_code == 200, detail_result.text
        value = self.sc.generate_jinja_data("./verify_data/storageclass/update_scs.jinja2", data)
        assert self.sc.is_sub_dict(value, detail_result.json()), \
            "获取存储卷详情比对数据失败，返回数据:{},期望数据:{}".format(detail_result.json(), value)

    @pytest.mark.delete
    def 测试存储类删除(self):
        # delete sc
        delete_result = self.sc.delete_scs_jinja2(self.sc_name)
        assert delete_result.status_code == 200, "删除存储类失败：{}".format(delete_result.text)
        assert self.sc.check_exists(self.sc.get_common_scs_url_v1(self.sc_name), 404)
