import pytest
from new_case.containerplatform.daemonset.daemonset import Daemonset
from common import settings


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestDaemonsetSuiteGet(object):

    def setup_class(self):
        self.daemonset = Daemonset()

    def 测试_获取守护进程集列表_有limit参数(self):
        ret = self.daemonset.get_daemonset_list(limit=20)
        assert ret.status_code == 200, "获取守护进程集列表失败:{}".format(ret.text)
        ret2 = self.daemonset.get_daemonset_list(limit=1)
        daemonset_num = len(ret2.json()["items"])
        assert daemonset_num == 1, "获取守护进程集列表,limit=1时失败,预期返回1个部署,实际返回{}个".format(daemonset_num)

        global name
        name = Daemonset.get_value(ret2.json(), "items.0.metadata.name")

        global cnt
        cnt = Daemonset.get_value(ret2.json(), "metadata.continue")

    def 测试_获取守护进程集列表_有limit参数和continue参数(self):
        ret = self.daemonset.get_daemonset_list(limit=1, cnt=cnt)
        assert ret.status_code == 200, "获取守护进程集列表_有limit和continue参数:{}".format(ret.text)
        name_continue = Daemonset.get_value(ret.json(), "items.0.metadata.name")
        assert name_continue != name, "分页数据相同，守护进程集第一页数据:{},守护进程集第二页数据:{}".format(name, name_continue)

    def 测试_搜索守护进程集(self):
        data = self.daemonset.data_list_configmap_labels
        ser = self.daemonset.search_daemonset(ns_name=data['namespace'], daemonset_name=data['daemonset_name'])
        assert ser.status_code == 200, "没有搜索到{}，{}".format(data["daemonset_name"], ser.text)

        value = self.daemonset.generate_jinja_data("./verify_data/daemonset/search_daemonset.jinja2", data)
        assert self.daemonset.is_sub_dict(value, ser.json()), \
            "搜索守护进程集比对数据失败，返回数据:{},期望数据:{}".format(ser.json(), value)
