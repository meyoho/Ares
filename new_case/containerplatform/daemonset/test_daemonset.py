import pytest
from new_case.containerplatform.daemonset.daemonset import Daemonset
from common import settings
from common.settings import K8S_NAMESPACE


@pytest.mark.BAT
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestDaemonsetSuite(object):

    def setup_class(self):
        self.daemonset = Daemonset()
        self.k8s_namespace = K8S_NAMESPACE

    @pytest.mark.metis
    @pytest.mark.prepare
    def 测试_创建Daemonset(self):
        data = self.daemonset.data_list
        self.daemonset.delete_daemonset(ns_name=data['namespace'], daemonset_name=data['daemonset_name'])
        create_result = self.daemonset.create_daemonset_jinja2("./test_data/daemonset/daemonset.jinja2", data=data)
        assert create_result.status_code == 201, "创建{}守护进程集失败{}".format(data['daemonset_name'], create_result.text)

        detail_result = self.daemonset.detail_daemonset_jinja2(ns_name=data['namespace'],
                                                               daemonset_name=data['daemonset_name'])
        assert detail_result.status_code == 200, "获取守护进程集{}详情页失败,{}".format(data['daemonset_name'], detail_result.text)

        daemonset_status = self.daemonset.get_daemonset_status(ns_name=data['namespace'],
                                                               daemonset_name=data["daemonset_name"])
        assert daemonset_status, "创建守护进程集后，验证守护进程集状态出错: 守护进程集：{} is not running".format(data["daemonset_name"])

        value = self.daemonset.generate_jinja_data("./verify_data/daemonset/create_daemonset.jinja2", data)
        assert self.daemonset.is_sub_dict(value, create_result.json()), "创建daemonset_必填参数比对数据失败，返回数据:{},期望数据:{}".format(
            create_result.json(), value)

    @pytest.mark.upgrade
    def 测试_获取Daemonset列表(self):
        data = self.daemonset.data_list
        ret = self.daemonset.get_daemonset_list(limit=20)
        assert ret.status_code == 200, "获取守护进程集列表失败:{}".format(ret.text)
        value = self.daemonset.generate_jinja_data("./verify_data/daemonset/list_daemonset.jinja2", data)
        assert self.daemonset.is_sub_dict(value, ret.json()), "获取Daemonset列表比对数据失败，返回数据:{},期望数据:{}" \
            .format(ret.json(), value)

    @pytest.mark.metis
    @pytest.mark.upgrade
    def 测试_获取Daemonset容器组(self):
        data = self.daemonset.data_list
        ret = self.daemonset.get_daemonset_pods(ns_name=data['namespace'], daemonset_name=data['daemonset_name'])
        assert ret.status_code == 200, "获取容器组失败:{}".format(ret.text)
        global pod_name
        pod_name = self.daemonset.get_value(ret.json(), 'items.0.metadata.name')
        global container_name
        container_name = self.daemonset.get_value(ret.json(), 'items.0.spec.containers.0.name')
        data.update({'pod_name': pod_name})
        value = self.daemonset.generate_jinja_data("./verify_data/daemonset/list_pod.jinja2", data)
        assert self.daemonset.is_sub_dict(value, ret.json()), "获取容器组比对数据失败，返回数据:{},期望数据:{}" \
            .format(ret.json(), value)

    @pytest.mark.upgrade
    def 测试_获取日志(self):
        data = self.daemonset.data_list
        ret = self.daemonset.get_daemonset_logs(self.k8s_namespace, pod_name, container_name)
        assert ret.status_code == 200, "获取日志失败:{}".format(ret.text)
        data.update({'pod_name': pod_name, 'container_name': container_name})
        values = self.daemonset.generate_jinja_data("verify_data/daemonset/log_daemonset.jinja2", data)
        assert self.daemonset.is_sub_dict(values, ret.json()), "获取日志比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)
        assert len(self.daemonset.get_value(ret.json(), 'logs')) != 0, "获取日志为空 {}".format(ret.json())

    @pytest.mark.upgrade
    def 测试_获取告警列表(self):
        data = self.daemonset.data_list
        ret = self.daemonset.get_daemonset_alarm(ns_name=data['namespace'], daemonset_name=data['daemonset_name'])
        assert ret.status_code == 200, "获取告警列表失败:{}".format(ret.text)
        value = self.daemonset.generate_jinja_data("verify_data/daemonset/daemonset_alarm.jinja2", data)
        assert self.daemonset.is_sub_dict(value, ret.json()), "获取告警列表比对数据失败，返回数据:{},期望数据:{}" \
            .format(ret.json(), value)

    @pytest.mark.metis
    @pytest.mark.upgrade
    def 测试_容器重建(self):
        data = self.daemonset.data_list
        ret = self.daemonset.delete_pod(self.k8s_namespace, pod_name)
        assert ret.status_code == 200, "容器重建失败:{}".format(ret.text)
        data.update({'pod_name': pod_name})
        values = self.daemonset.generate_jinja_data("verify_data/daemonset/delete_pod_daemonset.jinja2", data)
        assert self.daemonset.is_sub_dict(values, ret.json()), \
            "重建容器比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)
        detail_result = self.daemonset.detail_daemonset_jinja2(ns_name=data['namespace'],
                                                               daemonset_name=data['daemonset_name'])
        assert detail_result.status_code == 200, "获取守护进程集{}详情页失败,{}".format(data['deployment_name'], detail_result.text)

        daemonset_status = self.daemonset.get_daemonset_status(ns_name=data['namespace'],
                                                               daemonset_name=data["daemonset_name"])
        assert daemonset_status, "容器重建后，验证守护进程集状态出错: 守护进程集：{} is not running".format(data["daemonset_name"])

    @pytest.mark.upgrade
    def 测试_更新daemonset(self):
        data = self.daemonset.data_list
        data.update({"memory": "51Mi"})
        update_result = self.daemonset.update_daemonset_jinja2(data['namespace'], data['daemonset_name'],
                                                               "./test_data/daemonset/daemonset.jinja2", data=data)
        assert update_result.status_code == 200, "更新守护进程集{}失败:{}".format(data['daemonset_name'], update_result.text)

        value = self.daemonset.generate_jinja_data("./verify_data/daemonset/create_daemonset.jinja2", data)
        assert self.daemonset.is_sub_dict(value, update_result.json()), \
            "更新daemonset比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

        detail_result = self.daemonset.detail_daemonset_jinja2(ns_name=data['namespace'],
                                                               daemonset_name=data['daemonset_name'])
        assert detail_result.status_code == 200, "获取守护进程集{}详情页失败,{}".format(data['daemonset_name'], detail_result.text)
        daemonset_status = self.daemonset.get_daemonset_status(ns_name=data['namespace'],
                                                               daemonset_name=data["daemonset_name"])
        assert daemonset_status, "更新守护进程集，验证守护进程集状态出错: 守护进程集：{} is not running".format(data["daemonset_name"])

    @pytest.mark.upgrade
    def 测试daemonset详情(self):
        data = self.daemonset.data_list
        detail_result = self.daemonset.detail_daemonset_jinja2(ns_name=data['namespace'],
                                                               daemonset_name=data['daemonset_name'])
        assert detail_result.status_code == 200, "获取守护进程集{}详情页失败,{}".format(data['daemonset_name'], detail_result.text)

        value = self.daemonset.generate_jinja_data("./verify_data/daemonset/create_daemonset.jinja2", data)
        assert self.daemonset.is_sub_dict(value, detail_result.json()), \
            "测试daemonset比对数据失败，返回数据:{},期望数据:{}".format(detail_result.json(), value)

    @pytest.mark.metis
    @pytest.mark.delete
    def 测试_删除Daemonset(self):
        data = self.daemonset.data_list
        delete_result = self.daemonset.delete_daemonset(ns_name=data['namespace'],
                                                        daemonset_name=data['daemonset_name'])
        assert delete_result.status_code == 200, "删除守护进程集{}失败:{}".format(data['daemonset_name'], delete_result.text)
