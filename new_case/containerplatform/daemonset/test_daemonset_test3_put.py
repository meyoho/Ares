import pytest
from new_case.containerplatform.daemonset.daemonset import Daemonset
from common import settings


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestDeploymentSuitePut(object):

    def setup_class(self):
        self.daemonset = Daemonset()

    def 测试_更新Daemonset_必填参数_可选参数有两个_组件标签和配置引用(self):
        data = self.daemonset.data_list_configmap_labels
        data.update({"metadata_labels": {"key1-a": "value1", "key2-b": "value2"}})
        update_result = self.daemonset.update_daemonset_jinja2(data['namespace'], data['daemonset_name'],
                                                               "./test_data/daemonset/daemonset.jinja2", data=data)
        assert update_result.status_code == 200, "更新守护进程集{}失败:{}".format(data['daemonset_name'], update_result.text)

        detail_result = self.daemonset.detail_daemonset_jinja2(ns_name=data['namespace'],
                                                               daemonset_name=data['daemonset_name'])
        assert detail_result.status_code == 200, "获取守护进程集{}详情页失败,{}".format(data['daemonset_name'], detail_result.text)

        daemonset_status = self.daemonset.get_daemonset_status(ns_name=data['namespace'],
                                                               daemonset_name=data["daemonset_name"])
        assert daemonset_status, "创建守护进程集后，验证守护进程集状态出错: 守护进程集：{} is not running".format(data["daemonset_name"])

        value = self.daemonset.generate_jinja_data("./verify_data/daemonset/create_daemonset.jinja2", data)
        assert self.daemonset.is_sub_dict(value, update_result.json()), \
            "更新daemonset_必填参数_可选参数有两个_更新策略和组件标签比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

    # @pytest.mark.skip(reason="staging环境更新状态不是运行中，在本地复现不了")
    def 测试_更新Daemonset_必填参数_可选参数有三个_启动命令和参数和健康检查(self):
        data = self.daemonset.data_list_args_livenessprobe
        data.update({"args_2": "while true; do sleep 30; echo 456; done"})
        update_result = self.daemonset.update_daemonset_jinja2(data['namespace'], data['daemonset_name'],
                                                               "./test_data/daemonset/daemonset.jinja2", data=data)
        assert update_result.status_code == 200, "更新守护进程集{}失败:{}".format(data['daemonset_name'], update_result.text)

        detail_result = self.daemonset.detail_daemonset_jinja2(ns_name=data['namespace'], daemonset_name=data['daemonset_name'])
        assert detail_result.status_code == 200, "获取守护进程集{}详情页失败,{}".format(data['daemonset_name'], detail_result.text)

        daemonset_status = self.daemonset.get_daemonset_status(ns_name=data['namespace'],
                                                               daemonset_name=data["daemonset_name"])
        assert daemonset_status, "创建守护进程集后，验证守护进程集状态出错: 守护进程集：{} is not running".format(data["daemonset_name"])

        value = self.daemonset.generate_jinja_data("./verify_data/daemonset/create_daemonset.jinja2", data)
        assert self.daemonset.is_sub_dict(value, update_result.json()), \
            "更新daemonset_必填参数_可选参数有三个_启动命令和参数和健康检查比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

    def 测试_更新Daemonset_必填参数_可选端口(self):
        data = self.daemonset.data_list_port
        data.update({"protocol": "UDP"})
        update_result = self.daemonset.update_daemonset_jinja2(data['namespace'], data['daemonset_name'],
                                                               "./test_data/daemonset/daemonset.jinja2", data=data)
        assert update_result.status_code == 200, "更新守护进程集{}失败:{}".format(data['daemonset_name'], update_result.text)

        detail_result = self.daemonset.detail_daemonset_jinja2(ns_name=data['namespace'],
                                                               daemonset_name=data['daemonset_name'])
        assert detail_result.status_code == 200, "获取守护进程集{}详情页失败,{}".format(data['daemonset_name'], detail_result.text)

        daemonset_status = self.daemonset.get_daemonset_status(ns_name=data['namespace'],
                                                               daemonset_name=data["daemonset_name"])
        assert daemonset_status, "创建守护进程集后，验证守护进程集状态出错: 守护进程集：{} is not running".format(data["daemonset_name"])

        value = self.daemonset.generate_jinja_data("./verify_data/daemonset/create_daemonset.jinja2", data)
        assert self.daemonset.is_sub_dict(value, update_result.json()), \
            "更新daemonset_必填参数_可选参数有两个_更新策略和组件标签比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)
