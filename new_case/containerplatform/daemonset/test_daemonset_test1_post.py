import pytest
from new_case.containerplatform.daemonset.daemonset import Daemonset
from new_case.containerplatform.deployment.deployment import Deployment
from common import settings
from common.base_request import Common
from new_case.containerplatform.configmap.configmap import Configmap
from new_case.containerplatform.persistentvolumeclaims.pvc import Pvc
from new_case.containerplatform.secret.secret import Secret
import base64
import json


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestDaemonsetSuitePost(object):

    def setup_class(self):
        self.daemonset = Daemonset()
        self.deployment = Deployment()

        self.configmap_tool = Configmap()
        self.pvc = Pvc()

        self.secret_tool = Secret()
        self.secret_name = "{}-ares-daemonset-secret".format(settings.RESOURCE_PREFIX)
        self.verify_template = Common.generate_jinja_template(self, './verify_data/secret/create_response.jinja2')

        self.region_name = settings.REGION_NAME
        self.k8s_namespace = settings.K8S_NAMESPACE

    @pytest.mark.metis
    def 测试_创建Daemonset_必填参数_可选参数有两个_组件标签和配置引用(self):
        result = {"flag": True}
        data = self.daemonset.data_list_configmap_labels
        # 创建configmap
        cm_data = {
            "configmap_name": data["configmap_name"],
            "description": "key-value",
            "namespace": data["namespace"],
            "configmap_key": "key",
            "configmap_value": "value"
        }
        self.configmap_tool.delete_configmap(configmap_name=cm_data['configmap_name'])
        ret = self.configmap_tool.create_configmap(data=cm_data)
        assert ret.status_code == 201, "创建配置字典失败:{}".format(ret.text)

        # 创建daemonset
        self.daemonset.delete_daemonset(ns_name=data['namespace'], daemonset_name=data['daemonset_name'])

        create_result = self.daemonset.create_daemonset_jinja2("./test_data/daemonset/daemonset.jinja2", data=data)
        assert create_result.status_code == 201, "创建{}守护进程集失败{}".format(data['daemonset_name'], create_result.text)

        value = self.daemonset.generate_jinja_data("./verify_data/daemonset/create_daemonset.jinja2", data)
        assert self.daemonset.is_sub_dict(value, create_result.json()), \
            "创建daemonset_必填参数_可选参数有两个_更新策略和组件标签比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)
        # 验证
        detail_result = self.daemonset.detail_daemonset_jinja2(ns_name=data['namespace'],
                                                               daemonset_name=data['daemonset_name'])
        assert detail_result.status_code == 200, "获取守护进程集{}详情页失败,{}".format(data['daemonset_name'], detail_result.text)

        daemonset_status = self.daemonset.get_daemonset_status(ns_name=data['namespace'],
                                                               daemonset_name=data["daemonset_name"])
        assert daemonset_status, "创建守护进程集后，验证守护进程集状态出错: 守护进程集：{} is not running".format(data["daemonset_name"])

        content = detail_result.json()
        v1 = self.daemonset.get_value(content, 'spec.template.spec.containers.0.envFrom.0.configMapRef.name')
        result = self.daemonset.update_result(result, v1 == data["configmap_name"], '环境变量完整引用configmap失败')

        assert result['flag'], result

    def 测试创建daemonset事件(self):
        payload = {"cluster": settings.REGION_NAME,
                   "kind": "DaemonSet",
                   "name": self.daemonset.data_list_configmap_labels["daemonset_name"],
                   "namespace": settings.K8S_NAMESPACE,
                   "page": 1,
                   "page_size": 20}
        kevents_results = self.daemonset.get_kevents(payload=payload, expect_value="SuccessfulCreate", times=1)
        assert kevents_results, "创建daemonset获取事件获取失败"

    @pytest.mark.metis
    def 测试_创建Daemonset_必填参数_可选参数有三个_启动命令和参数和健康检查(self):
        data = self.daemonset.data_list_args_livenessprobe
        self.daemonset.delete_daemonset(ns_name=data['namespace'], daemonset_name=data['daemonset_name'])
        create_result = self.daemonset.create_daemonset_jinja2("./test_data/daemonset/daemonset.jinja2", data=data)
        assert create_result.status_code == 201, "创建{}守护进程集失败{}".format(data['daemonset_name'], create_result.text)

        detail_result = self.daemonset.detail_daemonset_jinja2(ns_name=data['namespace'],
                                                               daemonset_name=data['daemonset_name'])
        assert detail_result.status_code == 200, "获取守护进程集{}详情页失败,{}".format(data['daemonset_name'], detail_result.text)

        daemonset_status = self.daemonset.get_daemonset_status(ns_name=data['namespace'],
                                                               daemonset_name=data["daemonset_name"])
        assert daemonset_status, "创建守护进程集后，验证守护进程集状态出错: 守护进程集：{} is not running".format(data["daemonset_name"])

        value = self.daemonset.generate_jinja_data("./verify_data/daemonset/create_daemonset.jinja2", data)
        assert self.daemonset.is_sub_dict(value, create_result.json()), \
            "创建Daemonset_必填参数_可选参数有两个_更新策略和组件标签比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

    @pytest.mark.metis
    @pytest.mark.skipif(not settings.SC_NAME, reason="没有存储类，不运行当前用例")
    def 测试_创建Daemonset_必填参数_可选参数有两个_存储卷挂载和存储卷(self):
        result = {"flag": True}
        data = self.daemonset.data_list_volume
        # 创建pvc
        pvc_data = {
            "pvc_name": data["claimname"],
            "namespace": data["namespace"],
            "storageclassname": settings.SC_NAME
        }

        self.daemonset.delete_daemonset(ns_name=data['namespace'], daemonset_name=data['daemonset_name'])
        self.pvc.delete_pvc(ns_name=pvc_data["namespace"], pvc_name=pvc_data["pvc_name"])
        self.pvc.check_exists(self.pvc.get_common_pvc_url_v1(pvc_data["namespace"], pvc_data['pvc_name']), 404)

        create_result = self.pvc.create_pvc_jinja2("./test_data/pvc/pvc.jinja2", data=pvc_data)
        assert create_result.status_code == 201, "创建pvc{}失败:{}".format(pvc_data['pvc_name'], create_result.text)

        pvc_status = self.pvc.get_status(self.pvc.get_common_pvc_url_v1(pvc_name=pvc_data["pvc_name"]), "status.phase",
                                         "Bound")
        assert pvc_status, "创建pvc状态错误:{}的状态没有被绑定".format(pvc_data["pvc_name"])

        # 创建daemonset
        create_result = self.daemonset.create_daemonset_jinja2("./test_data/daemonset/daemonset.jinja2", data=data)
        assert create_result.status_code == 201, "创建{}守护进程集失败{}".format(data['daemonset_name'], create_result.text)

        value = self.daemonset.generate_jinja_data("./verify_data/daemonset/create_daemonset.jinja2", data)
        assert self.daemonset.is_sub_dict(value, create_result.json()), \
            "创建daemonset_必填参数_可选参数只有一个_存储比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

        # 验证
        detail_result = self.daemonset.detail_daemonset_jinja2(ns_name=data['namespace'],
                                                               daemonset_name=data['daemonset_name'])
        assert detail_result.status_code == 200, "获取守护进程集{}详情页失败,{}".format(data['daemonset_name'], detail_result.text)

        daemonset_status = self.daemonset.get_daemonset_status(ns_name=data['namespace'],
                                                               daemonset_name=data["daemonset_name"])
        assert daemonset_status, "创建守护进程集后，验证守护进程集状态出错: 守护进程集：{} is not running".format(data["daemonset_name"])

        content = detail_result.json()
        claimName = self.daemonset.get_value(content, 'spec.template.spec.volumes.0.persistentVolumeClaim.claimName')
        result = self.daemonset.update_result(result, claimName == data["claimname"], '守护进程集挂载pvc失败')

        # 删除daemonset
        self.daemonset.delete_daemonset(ns_name=data['namespace'], daemonset_name=data['daemonset_name'])
        self.daemonset.check_exists(self.daemonset.get_common_daemonset_url(ns_name=data['namespace'],
                                                                            daemonset_name=data['daemonset_name']), 404)
        # 删除pvc
        delete_result = self.pvc.delete_pvc(ns_name=pvc_data["namespace"], pvc_name=pvc_data["pvc_name"])
        assert delete_result.status_code == 200, "删除pvc失败{}".format(delete_result.text)
        assert result['flag'], result

    @pytest.mark.metis
    def 测试_创建Daemonset_必填参数_可选参数有两个_网络模式和配置引用_secret(self):
        result = {"flag": True}
        # 创建secret
        secret_data = {
            "secret_name": self.secret_name,
            "secret_type": "Opaque",
            "opaque_key": "opaque_key",
            "opaque_value": str(base64.b64encode("opaque_value".encode('utf-8')), 'utf8')
        }
        self.secret_tool.delete_secret(secret_data["secret_name"])
        secret_data.update({"K8S_NAMESPACE": settings.K8S_NAMESPACE})
        ret = self.secret_tool.create_secret('./test_data/secret/create_secret.jinja2', data=secret_data)

        assert ret.status_code == 201, "创建{}类型保密字典失败:{}".format(secret_data['secret_type'], ret.text)
        value = self.verify_template.render(secret_data)
        assert self.secret_tool.is_sub_dict(json.loads(value), ret.json()), "创建保密字典比对数据失败，返回数据:{},期望数据:{}".format(
            ret.json(), json.loads(value))

        # 创建daemonset
        data = self.daemonset.data_list_hostnetwork_secret
        self.daemonset.delete_daemonset(ns_name=data['namespace'], daemonset_name=data['daemonset_name'])
        create_result = self.daemonset.create_daemonset_jinja2("./test_data/daemonset/daemonset.jinja2", data=data)
        assert create_result.status_code == 201, "创建{}守护进程集失败{}".format(data['daemonset_name'], create_result.text)

        value = self.daemonset.generate_jinja_data("./verify_data/daemonset/create_daemonset.jinja2", data)
        assert self.daemonset.is_sub_dict(value, create_result.json()), \
            "创建daemonset_必填参数_可选参数只有一个_配置引用_secret比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

        # 验证
        detail_result = self.daemonset.detail_daemonset_jinja2(ns_name=data['namespace'],
                                                               daemonset_name=data['daemonset_name'])
        assert detail_result.status_code == 200, "获取守护进程集{}详情页失败,{}".format(data['daemonset_name'], detail_result.text)

        daemonset_status = self.daemonset.get_daemonset_status(ns_name=data['namespace'],
                                                               daemonset_name=data["daemonset_name"])
        assert daemonset_status, "创建守护进程集后，验证守护进程集状态出错: 守护进程集：{} is not running".format(data["daemonset_name"])

        content = detail_result.json()
        v1 = self.daemonset.get_value(content, 'spec.template.spec.containers.0.envFrom.0.secretRef.name')
        result = self.daemonset.update_result(result, v1 == self.secret_name, '环境变量完整引用secret失败')

        ret = self.secret_tool.delete_secret(data["secret_name"])
        assert ret.status_code == 200, "删除{}类型的保密字典失败:{}".format(data["secret_type"], ret.text)
        assert result['flag'], result

    @pytest.mark.metis
    def 测试_创建Daemonset_必填参数_可选参数亲和性(self):
        # 创建基础deployment
        data_affinify_base = self.daemonset.data_list_affinify_base
        self.deployment.delete_deplyment(ns_name=data_affinify_base['namespace'],
                                         deployment_name=data_affinify_base['deployment_name'],
                                         region_name=self.region_name)
        self.daemonset.delete_daemonset(ns_name=data_affinify_base['namespace'],
                                        daemonset_name=self.daemonset.data_list_affinify_app_name)
        create_result = self.deployment.create_deployment_jinja2("./test_data/deployment/deployment.jinja2",
                                                                 data=data_affinify_base, region_name=self.region_name,
                                                                 ns_name=self.k8s_namespace)
        assert create_result.status_code == 201, "创建基础{}部署失败{}".format(data_affinify_base['deployment_name'],
                                                                       create_result.text)
        detail_result = self.deployment.detail_deplyment_jinja2(ns_name=data_affinify_base['namespace'],
                                                                deployment_name=data_affinify_base['deployment_name'],
                                                                region_name=self.region_name)
        assert detail_result.status_code == 200, "获取基础部署{}详情页失败,{}".format(data_affinify_base['deployment_name'],
                                                                           detail_result.text)
        deployment_status = self.deployment.get_deployment_status(ns_name=data_affinify_base['namespace'],
                                                                  deployment_name=data_affinify_base["deployment_name"])
        assert deployment_status, "创建部署后，验证部署状态出错: 部署：{} is not running".format(data_affinify_base["deployment_name"])

        # 创建亲和daemonset
        data_affinify_app_name = self.daemonset.data_list_affinify_app_name
        create_affinity_result = self.daemonset.create_daemonset_jinja2("./test_data/daemonset/daemonset.jinja2",
                                                                        data=data_affinify_app_name)
        assert create_affinity_result.status_code == 201, "创建亲和{}守护进程集失败{}".format(
            data_affinify_app_name['daemonset_name'], create_affinity_result.text)

        detail_result = self.daemonset.detail_daemonset_jinja2(ns_name=data_affinify_app_name['namespace'],
                                                               daemonset_name=data_affinify_app_name['daemonset_name'])
        assert detail_result.status_code == 200, "获取亲和守护进程集{}详情页失败,{}".format(data_affinify_app_name['daemonset_name'],
                                                                              detail_result.text)

        # content = detail_result.json()
        # cnt = 0
        # flag = False
        # while cnt < 30 and not flag:
        #     cnt += 1
        #     sleep(3)
        #     value_numberready = self.daemonset.get_value(content, 'status.numberReady')
        #     value_desirednumberscheduled = self.daemonset.get_value(content, 'status.desiredNumberScheduled')
        #     if value_numberready != value_desirednumberscheduled and value_numberready == 1:
        #         flag = True
        #         break
        # return flag
        # assert flag, "亲和性验证失败"

    @pytest.mark.metis
    def 测试_创建Daemonset_必填参数_可选参数反亲和性(self):
        # 创建反亲和daemonset
        data_antiaffinify_app_name = self.daemonset.data_list_antiaffinify_app_name
        self.daemonset.delete_daemonset(ns_name=data_antiaffinify_app_name['namespace'],
                                        daemonset_name=data_antiaffinify_app_name['daemonset_name'])
        create_antiaffinity_result = self.daemonset.create_daemonset_jinja2("./test_data/daemonset/daemonset.jinja2",
                                                                            data=data_antiaffinify_app_name)
        assert create_antiaffinity_result.status_code == 201, "创建反亲和{}守护进程集署失败{}". \
            format(data_antiaffinify_app_name['daemonset_name'], create_antiaffinity_result.text)

        detail_result = self.daemonset.detail_daemonset_jinja2(ns_name=data_antiaffinify_app_name['namespace'],
                                                               daemonset_name=data_antiaffinify_app_name[
                                                                   'daemonset_name'])
        assert detail_result.status_code == 200, "获取反亲和守护进程集{}详情页失败,{}".format(
            data_antiaffinify_app_name['daemonset_name'], detail_result.text)
        # content = detail_result.json()
        # cnt = 0
        # flag = False
        # while cnt < 30 and not flag:
        #     cnt += 1
        #     sleep(3)
        #     value_numberready = self.daemonset.get_value(content, 'status.numberReady')
        #     value_desirednumberscheduled = self.daemonset.get_value(content, 'status.desiredNumberScheduled')
        #     if value_numberready != value_desirednumberscheduled and value_numberready == value_desirednumberscheduled - 1:
        #         flag = True
        #         break
        # return flag
        # assert flag, "反亲和性验证失败"

    @pytest.mark.metis
    def 测试_创建Daemonset_必填参数_可选端口(self):
        data = self.daemonset.data_list_port
        self.daemonset.delete_daemonset(ns_name=data['namespace'], daemonset_name=data['daemonset_name'])

        create_result = self.daemonset.create_daemonset_jinja2("./test_data/daemonset/daemonset.jinja2", data=data)
        assert create_result.status_code == 201, "创建{}守护进程集失败{}".format(data['daemonset_name'], create_result.text)

        detail_result = self.daemonset.detail_daemonset_jinja2(ns_name=data['namespace'],
                                                               daemonset_name=data['daemonset_name'])
        assert detail_result.status_code == 200, "获取守护进程集{}详情页失败,{}".format(data['daemonset_name'], detail_result.text)

        daemonset_status = self.daemonset.get_daemonset_status(ns_name=data['namespace'],
                                                               daemonset_name=data["daemonset_name"])
        assert daemonset_status, "创建守护进程集后，验证守护进程集状态出错: 守护进程集：{} is not running".format(data["daemonset_name"])

        value = self.daemonset.generate_jinja_data("./verify_data/daemonset/create_daemonset.jinja2", data)
        assert self.daemonset.is_sub_dict(value, create_result.json()), \
            "创建daemonset_必填参数比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)
