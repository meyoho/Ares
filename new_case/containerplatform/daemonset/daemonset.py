import sys
from common.base_request import Common
from common.log import logger
from common import settings
from time import sleep
import random


class Daemonset(Common):
    def __init__(self):
        super(Daemonset, self).__init__()
        self.genetate_global_info()

    def get_common_daemonset_url(self, ns_name=settings.K8S_NAMESPACE, daemonset_name=''):
        return 'kubernetes/{}/apis/apps/v1/namespaces/{}/daemonsets/{}'.format(self.region_name, ns_name, daemonset_name)

    def get_common_daemonset_list_url(self, ns_name=settings.K8S_NAMESPACE, limit=1, cnt=""):
        return limit and "kubernetes/{}/apis/apps/v1/namespaces/{}/daemonsets?limit={}&continue={}" \
            .format(self.region_name, ns_name, limit, cnt)

    def get_common_daemonset_search_url(self, ns_name=settings.K8S_NAMESPACE, limit=1, cnt="", daemonset_name=''):
        return limit and "acp/v1/resources/search/kubernetes/{}/apis/apps/v1/namespaces/{}/daemonsets?" \
                         "limit={}&continue={}&keyword={}&field=metadata.name".format(self.region_name, ns_name, limit, cnt, daemonset_name)

    def get_daemonset_pod_url(self, ns_name=settings.K8S_NAMESPACE, limit=1, daemonset_name=''):
        return limit and "kubernetes/{}/api/v1/namespaces/{}/pods?limit={}&labelSelector=app={}" \
            .format(self.region_name, ns_name, limit, daemonset_name)

    def create_daemonset_jinja2(self, file, data):
        path = self.get_common_daemonset_url()
        data = self.generate_jinja_data(file, data)
        logger.info(data)
        return self.send(method='post', path=path, json=data, params={})

    def detail_daemonset_jinja2(self, ns_name, daemonset_name):
        path = self.get_common_daemonset_url(ns_name=ns_name, daemonset_name=daemonset_name)
        return self.send(method='get', path=path, params={})

    # 状态判断链接文档：http://confluence.alauda.cn/pages/viewpage.action?pageId=28508749
    def get_daemonset_status(self, ns_name, daemonset_name, key_status_desirednumberscheduled="status.desiredNumberScheduled",
                             key_status_observedgeneration="status.observedGeneration", key_status_currentNumberScheduled="status.currentNumberScheduled",
                             key_metadata_generation="metadata.generation", key_numberready="status.numberReady", key_numberavailable="status.numberAvailable",
                             key_status_updatedreplicas="status.updatedNumberScheduled"):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        cnt = 0
        flag = False
        while cnt < 60 and not flag:
            try:
                cnt += 1
                sleep(5)
                url = "kubernetes/{}/apis/apps/v1/namespaces/{}/daemonsets/{}". \
                    format(self.region_name, ns_name, daemonset_name)
                response = self.send(method="GET", path=url)
                assert response.status_code == 200, "get daemonset status failed"
                value_status_desirednumberscheduled = self.get_value(response.json(), key_status_desirednumberscheduled)
                if 'updatedNumberScheduled' in response.text:
                    value_status_updatedreplicas = self.get_value(response.json(), key_status_updatedreplicas)
                else:
                    value_status_updatedreplicas = 0
                value_status_observedgeneration = self.get_value(response.json(), key_status_observedgeneration)
                value_status_currentNumberScheduled = self.get_value(response.json(), key_status_currentNumberScheduled)
                value_metadata_generation = self.get_value(response.json(), key_metadata_generation)
                value_numberready = self.get_value(response.json(), key_numberready)
                if 'numberAvailable' in response.text:
                    value_numberavailable = self.get_value(response.json(), key_numberavailable)
                else:
                    value_numberavailable = 0

                if value_status_desirednumberscheduled > 0 and value_status_observedgeneration >= value_metadata_generation and \
                        value_status_desirednumberscheduled == value_status_currentNumberScheduled and \
                        value_numberready == value_status_desirednumberscheduled and value_status_updatedreplicas == value_status_desirednumberscheduled and \
                        value_numberavailable == value_status_desirednumberscheduled:
                    flag = True
                    break
            except Exception as e:
                logger.error(e)
        return flag

    def get_daemonset_list(self, limit=None, cnt=""):
        path = self.get_common_daemonset_list_url(limit=limit, cnt=cnt)
        return self.send(method='get', path=path, params={})

    def delete_daemonset(self, ns_name, daemonset_name, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_daemonset_url(ns_name, daemonset_name)
        return self.send(method='delete', path=url, auth=auth)

    def get_daemonset_pods(self, ns_name, daemonset_name, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_daemonset_pod_url(ns_name=ns_name, daemonset_name=daemonset_name)
        return self.send(method='get', path=url, auth=auth)

    def delete_pod(self, ns_name, pod_name):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        url = "kubernetes/{}/api/v1/namespaces/{}/pods/{}".format(self.region_name, ns_name, pod_name)
        return self.send(method="delete", path=url)

    def get_daemonset_logs(self, ns_name, pod_name, container_name):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        url = "acp/v1/kubernetes/{}/namespaces/{}/pods/{}/{}/log?previous=false&logFilePosition=end" \
              "&referenceTimestamp=newest&offsetFrom=2000000000&offsetTo=2000000100". \
            format(self.region_name, ns_name, pod_name, container_name)
        return self.send(method="get", path=url)

    def search_daemonset(self, ns_name, daemonset_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_daemonset_search_url(ns_name=ns_name, daemonset_name=daemonset_name)
        return self.send(method='get', path=url)

    def update_daemonset_jinja2(self, ns_name, daemonset_name, file, data):
        path = self.get_common_daemonset_url(ns_name=ns_name, daemonset_name=daemonset_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='put', path=path, json=data, params={})

    def get_daemonset_alarm(self, ns_name, daemonset_name):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        url = "kubernetes/{}/apis/monitoring.coreos.com/v1/namespaces/{}/prometheusrules?" \
              "limit=20&labelSelector=alert.alauda.io/owner=System,alert.alauda.io/namespace={}," \
              "alert.alauda.io/kind=Daemonset,alert.alauda.io/name={}".format(self.region_name, ns_name, ns_name,
                                                                              daemonset_name)
        return self.send(method="get", path=url)

    def get_node(self):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        url = "kubernetes/{}/api/v1/nodes".format(self.region_name)
        return self.send(method="get", path=url)

    # 创建daemonset_必填参数
    data_list = {
        "daemonset_name": '{}-daemonset'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME
    }

    # 创建daemonset_必填参数_可选参数有两个_组件标签和配置引用
    data_list_configmap_labels = {
        "daemonset_name": '{}-daemonset-configmap-labels'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME,
        "metadata_labels": {"key1": "value1", "key2": "value2"},
        "envFrom": " ",
        "configMapRef": " ",
        "configmap_name": "{}-ares-daemonset-configmap".format(settings.RESOURCE_PREFIX)
    }

    # 创建daemonset_必填参数_可选参数有三个_启动命令和参数和健康检查
    data_list_args_livenessprobe = {
        "daemonset_name": '{}-daemonset-strategy-args-livenessprobe'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME,
        "args": " ",
        "args_1": "-c",
        "args_2": "while true; do sleep 30; echo 123; done",
        "command": " ",
        "command_1": "/bin/sh",
        "livenessProbe": " ",
        "failurethreshold": 5,
        "path": "/",
        "port": 80,
        "scheme": "HTTP",
        "initialdelayseconds": 300,
        "periodseconds": 60,
        "successthreshold": 1,
        "timeoutSeconds": 30
    }

    # 创建daemonset_必填参数_可选参数有两个_存储卷挂载和存储卷
    data_list_volume = {
        "daemonset_name": '{}-daemonset-volume'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME,
        "volumeMounts": " ",
        "mountpath": "/pvc/test",
        "volume_name": "new-volume",
        "volumes": " ",
        "claimname": '{}-ares-daemonset-pvc'.format(settings.RESOURCE_PREFIX).replace('_', '-')
    }

    # 创建daemonset_必填参数有两个_网络模式和配置引用_secret
    data_list_hostnetwork_secret = {
        "daemonset_name": '{}-hostnetwork-secret'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME,
        "envFrom": " ",
        "secretRef": " ",
        "secret_name": "{}-ares-daemonset-secret".format(settings.RESOURCE_PREFIX)
    }

    # 创建daemonset_亲和性_基础deployment
    data_list_affinify_base = {
        "deployment_name": '{}-appbaseforaffinity'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME,
    }

    # 创建daemonset_亲和性_亲和daemonset
    data_list_affinify_app_name = {
        "daemonset_name": '{}-affinity'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME,
        "affinity": " ",
        "affinity_type": "podAffinity",
        "deployment_name_affinify_base": 'alauda-appbaseforaffinity-daemonset'
    }

    # 创建daemonset_亲和性_反亲和daemonset
    data_list_antiaffinify_app_name = {
        "daemonset_name": '{}-antiaffinify'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME,
        "affinity": " ",
        "affinity_type": "podAntiAffinity",
        "deployment_name_affinify_base": 'alauda-appbaseforaffinity-daemonset'
    }

    # 创建daemonset_端口
    data_list_port = {
        "daemonset_name": '{}-daemonset-port'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME,
        "ports": " ",
        "protocol": "TCP",
        "hostPort": random.randrange(32000, 34000, 1),
        "containerPort": 80
    }
