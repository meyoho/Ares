import pytest
from new_case.containerplatform.daemonset.daemonset import Daemonset
from new_case.containerplatform.deployment.deployment import Deployment
from new_case.containerplatform.configmap.configmap import Configmap
from common import settings


@pytest.mark.archon
@pytest.mark.metis
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestDaemonsetSuiteDelete(object):

    def setup_class(self):
        self.daemonset = Daemonset()
        self.deployment = Deployment()
        self.region_name = settings.REGION_NAME
        self.configmap_tool = Configmap()

    def 测试_删除Daemonset_必填参数_可选参数有两个_组件标签和配置引用(self):
        data = self.daemonset.data_list_configmap_labels
        delete_result = self.daemonset.delete_daemonset(ns_name=data['namespace'],
                                                        daemonset_name=data['daemonset_name'])
        assert delete_result.status_code == 200, "删除守护进程集{}失败:{}".format(data['daemonset_name'], delete_result.text)
        ret = self.configmap_tool.delete_configmap(configmap_name=data['configmap_name'])
        assert ret.status_code == 200, "删除配置字典{}失败:{}".format(data['configmap_name'], ret.text)

    def 测试_删除Daemonset_必填参数_可选参数有三个_启动命令和参数和健康检查(self):
        data = self.daemonset.data_list_args_livenessprobe
        delete_result = self.daemonset.delete_daemonset(ns_name=data['namespace'],
                                                        daemonset_name=data['daemonset_name'])
        assert delete_result.status_code == 200, "删除守护进程集{}失败:{}".format(data['daemonset_name'], delete_result.text)

    def 测试_删除Daemonset_必填参数_可选参数有两个_网络模式和配置引用(self):
        data = self.daemonset.data_list_hostnetwork_secret
        delete_result = self.daemonset.delete_daemonset(ns_name=data['namespace'],
                                                        daemonset_name=data['daemonset_name'])
        assert delete_result.status_code == 200, "删除守护进程集{}失败:{}".format(data['daemonset_name'], delete_result.text)

    def 测试_删除Daemonset_必填参数_可选参数有两个_亲和和反亲和(self):
        data_affinify_base = self.daemonset.data_list_affinify_base
        delete_result = self.deployment.delete_deplyment(ns_name=data_affinify_base['namespace'],
                                                         deployment_name=data_affinify_base['deployment_name'],
                                                         region_name=self.region_name)
        assert delete_result.status_code == 200, "删除部署{}失败:{}".format(data_affinify_base['deployment_name'],
                                                                      delete_result.text)

        data_affinify_app_name = self.daemonset.data_list_affinify_app_name
        delete_result = self.daemonset.delete_daemonset(ns_name=data_affinify_app_name['namespace'],
                                                        daemonset_name=data_affinify_app_name['daemonset_name'])
        assert delete_result.status_code == 200, "删除守护进程集{}失败:{}".format(data_affinify_app_name['daemonset_name'],
                                                                         delete_result.text)

        data_antiaffinify_app_name = self.daemonset.data_list_antiaffinify_app_name
        delete_result = self.daemonset.delete_daemonset(ns_name=data_antiaffinify_app_name['namespace'],
                                                        daemonset_name=data_antiaffinify_app_name['daemonset_name'])
        assert delete_result.status_code == 200, "删除守护进程集{}失败:{}".format(data_antiaffinify_app_name['daemonset_name'],
                                                                         delete_result.text)

    def 测试_删除Daemonset_必填参数_可选端口(self):
        data = self.daemonset.data_list_port
        delete_result = self.daemonset.delete_daemonset(ns_name=data['namespace'],
                                                        daemonset_name=data['daemonset_name'])
        assert delete_result.status_code == 200, "删除守护进程集{}失败:{}".format(data['daemonset_name'], delete_result.text)
