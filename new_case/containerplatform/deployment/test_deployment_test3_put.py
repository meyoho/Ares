import pytest
from new_case.containerplatform.deployment.deployment import Deployment
from common import settings
from new_case.operation.log.log import Log
from time import time


@pytest.mark.erebus
@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestDeploymentSuitePut(object):

    def setup_class(self):
        self.deployment = Deployment()
        self.log = Log()
        self.region_name = settings.REGION_NAME

    @pytest.mark.metis
    def 测试_更新Deployment_必填参数_可选参数有两个_更新策略和组件标签(self):
        data = self.deployment.data_list_strategy_labels
        data.update({"maxsurge": "40%"})
        data.update({"metadata_labels": {"key1-a": "value1", "key2-b": "value2"}})
        update_result = self.deployment.update_deplyment_jinja2(data['namespace'], data['deployment_name'],
                                                                "./test_data/deployment/deployment.jinja2", data=data)
        assert update_result.status_code == 200, "更新部署{}失败:{}".format(data['deployment_name'], update_result.text)

        detail_result = self.deployment.detail_deplyment_jinja2(ns_name=data['namespace'],
                                                                deployment_name=data['deployment_name'], region_name=self.region_name)
        assert detail_result.status_code == 200, "获取部署{}详情页失败,{}".format(data['deployment_name'], detail_result.text)

        deployment_status = self.deployment.get_deployment_status(ns_name=data['namespace'], deployment_name=data["deployment_name"])
        assert deployment_status, "更新部署后，验证部署状态出错: 部署：{} is not running".format(data["deployment_name"])

        value = self.deployment.generate_jinja_data("./verify_data/deployment/create_deployment.jinja2", data)
        assert self.deployment.is_sub_dict(value, update_result.json()), \
            "更新Deployment_必填参数_可选参数有两个_更新策略和组件标签比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

    @pytest.mark.metis
    def 测试_更新deployment_必填参数_可选参数有三个_启动命令和参数和健康检查(self):
        data = self.deployment.data_list_args_livenessprobe
        data.update({"args_2": "while true; do sleep 30; echo 456; done"})
        update_result = self.deployment.update_deplyment_jinja2(data['namespace'], data['deployment_name'],
                                                                "./test_data/deployment/deployment.jinja2", data=data)
        assert update_result.status_code == 200, "更新部署{}失败:{}".format(data['deployment_name'], update_result.text)

        detail_result = self.deployment.detail_deplyment_jinja2(ns_name=data['namespace'],
                                                                deployment_name=data['deployment_name'], region_name=self.region_name)
        assert detail_result.status_code == 200, "获取部署{}详情页失败,{}".format(data['deployment_name'], detail_result.text)

        deployment_status = self.deployment.get_deployment_status(ns_name=data['namespace'], deployment_name=data["deployment_name"])
        assert deployment_status, "更新部署后，验证部署状态出错: 部署：{} is not running".format(data["deployment_name"])

        value = self.deployment.generate_jinja_data("./verify_data/deployment/create_deployment.jinja2", data)
        assert self.deployment.is_sub_dict(value, update_result.json()), \
            "更新Deployment_必填参数_可选参数有三个_启动命令和参数和健康检查比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

    @pytest.mark.skip(reason="日志case有问题,施源后续改")
    @pytest.mark.metis
    def 测试_更新Deployment_必填参数_可选参数有三个_环境变量和日志文件和排除日志文件(self):
        data = self.deployment.data_list_file_log_exclude_log
        data.update({"exclude_log_value": " "})
        update_result = self.deployment.update_deplyment_jinja2(data['namespace'], data['deployment_name'],
                                                                "./test_data/deployment/deployment.jinja2", data=data)
        assert update_result.status_code == 200, "更新部署{}失败:{}".format(data['deployment_name'], update_result.text)

        # 获取当前时间
        current_time = time()

        deployment_status = self.deployment.get_deployment_status(ns_name=data['namespace'],
                                                                  deployment_name=data["deployment_name"])
        assert deployment_status, "更新部署后，验证部署状态出错: 部署：{} is not running".format(data["deployment_name"])

        pod_result = self.deployment.get_deplyment_pods(ns_name=data['namespace'],
                                                        deployment_name=data['deployment_name'], region_name=self.region_name)
        assert pod_result.status_code == 200, "获取pod{}列表页失败,{}".format(data['deployment_name'], pod_result.text)
        pod_name = self.deployment.get_value_list(pod_result.json(), "items||metadata.name")

        value = self.deployment.generate_jinja_data("./verify_data/deployment/create_deployment.jinja2", data)
        assert self.deployment.is_sub_dict(value, update_result.json()), \
            "更新Deployment_必填参数_可选参数有两个_日志文件和排除日志文件比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

        # 验证
        params = {"pods": ",".join(pod_name), "paths": "hehe.txt", "start_time": current_time}
        log_result = self.log.get_logs_search(payload=params, cnt=100)
        assert self.log.get_value(log_result.json(), 'total_items') > 0, "搜索排除日志文件返回数据为空{}".format(log_result.text)

    @pytest.mark.metis
    def 测试_更新Deployment_必填参数_可选端口(self):
        data = self.deployment.data_list_port
        data.update({"protocol": "UDP"})
        update_result = self.deployment.update_deplyment_jinja2(data['namespace'], data['deployment_name'],
                                                                "./test_data/deployment/deployment.jinja2", data=data)
        assert update_result.status_code == 200, "更新部署{}失败:{}".format(data['deployment_name'], update_result.text)

        detail_result = self.deployment.detail_deplyment_jinja2(ns_name=data['namespace'],
                                                                deployment_name=data['deployment_name'], region_name=self.region_name)
        assert detail_result.status_code == 200, "获取部署{}详情页失败,{}".format(data['deployment_name'], detail_result.text)

        deployment_status = self.deployment.get_deployment_status(ns_name=data['namespace'],
                                                                  deployment_name=data["deployment_name"])
        assert deployment_status, "更新部署后，验证部署状态出错: 部署：{} is not running".format(data["deployment_name"])

        value = self.deployment.generate_jinja_data("./verify_data/deployment/create_deployment.jinja2", data)
        assert self.deployment.is_sub_dict(value, update_result.json()), \
            "更新Deployment_必填参数_可选端口比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

    def 测试更新deployment事件(self):
        payload = {"cluster": settings.REGION_NAME,
                   "kind": "Deployment",
                   "name": self.deployment.data_list_args_livenessprobe["deployment_name"], "namespace": settings.K8S_NAMESPACE, "page": 1, "page_size": 20}
        kevents_results = self.deployment.get_kevents(payload=payload, expect_value="ScalingReplicaSet", times=4)
        assert kevents_results, "更新deployment获取事件获取失败"
