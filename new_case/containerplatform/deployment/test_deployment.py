import pytest
from new_case.containerplatform.deployment.deployment import Deployment
from common import settings
from common.settings import K8S_NAMESPACE
from new_case.containerplatform.application.workload import Wordload


@pytest.mark.erebus
@pytest.mark.archon
@pytest.mark.BAT
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestDeploymentSuite(object):

    def setup_class(self):
        self.deployment = Deployment()
        self.k8s_namespace = K8S_NAMESPACE
        self.deployment_client = Wordload()
        self.region_name = settings.REGION_NAME

    @pytest.mark.metis
    @pytest.mark.prepare
    def 测试_创建Deployment(self):
        data = self.deployment.data_list
        self.deployment.delete_deplyment(ns_name=data['namespace'], deployment_name=data['deployment_name'],
                                         region_name=self.region_name)

        create_result = self.deployment.create_deployment_jinja2("./test_data/deployment/deployment.jinja2",
                                                                 data=data, region_name=self.region_name,
                                                                 ns_name=self.k8s_namespace)
        assert create_result.status_code == 201, "创建{}部署失败{}".format(data['deployment_name'], create_result.text)

        detail_result = self.deployment.detail_deplyment_jinja2(ns_name=data['namespace'],
                                                                deployment_name=data['deployment_name'],
                                                                region_name=self.region_name)
        assert detail_result.status_code == 200, "获取部署{}详情页失败,{}".format(data['deployment_name'], detail_result.text)

        deployment_status = self.deployment.get_deployment_status(ns_name=data['namespace'],
                                                                  deployment_name=data["deployment_name"])
        assert deployment_status, "创建部署后，验证部署状态出错: 部署：{} is not running".format(data["deployment_name"])

        value = self.deployment.generate_jinja_data("./verify_data/deployment/create_deployment.jinja2", data)
        assert self.deployment.is_sub_dict(value, create_result.json()), \
            "创建Deployment_必填参数比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

    @pytest.mark.upgrade
    def 测试_获取deployment列表(self):
        data = self.deployment.data_list
        ret = self.deployment.get_deplyment_list(limit=20)
        assert ret.status_code == 200, "获取部署列表失败:{}".format(ret.text)
        value = self.deployment.generate_jinja_data("./verify_data/deployment/list_deployment.jinja2", data)
        assert self.deployment.is_sub_dict(value, ret.json()), "获取Deployment列表比对数据失败，返回数据:{},期望数据:{}"\
            .format(ret.json(), value)

    @pytest.mark.metis
    @pytest.mark.upgrade
    def 测试_获取deployment容器组(self):
        data = self.deployment.data_list
        ret = self.deployment.get_deplyment_pods(ns_name=data['namespace'], deployment_name=data['deployment_name'],
                                                 region_name=self.region_name)
        assert ret.status_code == 200, "获取容器组失败:{}".format(ret.text)
        global pod_name
        pod_name = self.deployment.get_value(ret.json(), 'items.0.metadata.name')
        global container_name
        container_name = self.deployment.get_value(ret.json(), 'items.0.spec.containers.0.name')
        data.update({'pod_name': pod_name})
        value = self.deployment.generate_jinja_data("./verify_data/deployment/list_pod.jinja2", data)
        assert self.deployment.is_sub_dict(value, ret.json()), "获取容器组比对数据失败，返回数据:{},期望数据:{}"\
            .format(ret.json(), value)

    @pytest.mark.upgrade
    def 测试_获取日志(self):
        data = self.deployment.data_list
        ret = self.deployment.get_deployment_logs(self.k8s_namespace, pod_name, container_name)
        assert ret.status_code == 200, "获取日志失败:{}".format(ret.text)
        data.update({'pod_name': pod_name, 'container_name': container_name})
        values = self.deployment.generate_jinja_data("verify_data/deployment/log_deployment.jinja2", data)
        assert self.deployment.is_sub_dict(values, ret.json()), "获取日志比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)
        assert len(self.deployment.get_value(ret.json(), 'logs')) != 0, "获取日志为空 {}".format(ret.json())

    @pytest.mark.upgrade
    def 测试_获取告警列表(self):
        data = self.deployment.data_list
        ret = self.deployment.get_deployment_alarm(ns_name=data['namespace'], deployment_name=data['deployment_name'])
        assert ret.status_code == 200, "获取告警列表失败:{}".format(ret.text)
        value = self.deployment.generate_jinja_data("verify_data/deployment/deployment_alarm.jinja2", data)
        assert self.deployment.is_sub_dict(value, ret.json()), "获取告警列表比对数据失败，返回数据:{},期望数据:{}" \
            .format(ret.json(), value)

    @pytest.mark.metis
    @pytest.mark.upgrade
    def 测试_容器重建(self):
        data = self.deployment.data_list
        ret = self.deployment.delete_pod(self.k8s_namespace, pod_name)
        assert ret.status_code == 200, "容器重建失败:{}".format(ret.text)
        data.update({'pod_name': pod_name})
        values = self.deployment.generate_jinja_data("verify_data/deployment/delete_pod_deployment.jinja2", data)
        assert self.deployment.is_sub_dict(values, ret.json()), \
            "重建容器比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)
        detail_result = self.deployment.detail_deplyment_jinja2(ns_name=data['namespace'],
                                                                deployment_name=data['deployment_name'],
                                                                region_name=self.region_name)
        assert detail_result.status_code == 200, "获取部署{}详情页失败,{}".format(data['deployment_name'], detail_result.text)

        deployment_status = self.deployment.get_deployment_status(ns_name=data['namespace'], deployment_name=data["deployment_name"])
        assert deployment_status, "创建部署后，验证部署状态出错: 部署：{} is not running".format(data["deployment_name"])

    @pytest.mark.metis
    @pytest.mark.upgrade
    def 测试_停止deployment(self):
        data = self.deployment.data_list
        data.update({'replicas': 0})
        ret = self.deployment.stop_start_deployment(data['namespace'], data['deployment_name'],
                                                    "./test_data/deployment/deployment.jinja2", data=data)
        assert ret.status_code == 200, "停止deployment失败:{}".format(ret.text)

        detail_result = self.deployment.detail_deplyment_jinja2(ns_name=data['namespace'],
                                                                deployment_name=data['deployment_name'],
                                                                region_name=self.region_name)
        assert detail_result.status_code == 200, "获取部署{}详情页失败,{}".format(data['deployment_name'], detail_result.text)
        value = self.deployment.get_value(detail_result.json(), 'spec.replicas')
        assert value == 0, "停止部署后，验证状态出错：部署{}不是停止状态".format(data['deployment_name'])

    @pytest.mark.metis
    @pytest.mark.upgrade
    def 测试_开始deployment(self):
        data = self.deployment.data_list
        data.update({'replicas': 1})
        ret = self.deployment.stop_start_deployment(data['namespace'], data['deployment_name'],
                                                    "./test_data/deployment/deployment.jinja2", data=data)
        assert ret.status_code == 200, "开始deployment失败:{}".format(ret.text)

        detail_result = self.deployment.detail_deplyment_jinja2(ns_name=data['namespace'],
                                                                deployment_name=data['deployment_name'],
                                                                region_name=self.region_name)
        assert detail_result.status_code == 200, "获取部署{}详情页失败,{}".format(data['deployment_name'], detail_result.text)

        deployment_status = self.deployment.get_deployment_status(ns_name=data['namespace'], deployment_name=data["deployment_name"])
        assert deployment_status, "开始部署后，验证部署状态出错: 部署：{} is not running".format(data["deployment_name"])

    @pytest.mark.metis
    @pytest.mark.upgrade
    def 测试_更新deployment(self):
        data = self.deployment.data_list
        data.update({"memory": "51Mi"})
        update_result = self.deployment.update_deplyment_jinja2(data['namespace'], data['deployment_name'],
                                                                "./test_data/deployment/deployment.jinja2", data=data)
        assert update_result.status_code == 200, "更新部署{}失败:{}".format(data['deployment_name'], update_result.text)

        value = self.deployment.generate_jinja_data("./verify_data/deployment/create_deployment.jinja2", data)
        assert self.deployment.is_sub_dict(value, update_result.json()), \
            "更新Deployment_必填参数比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), value)

        detail_result = self.deployment.detail_deplyment_jinja2(ns_name=data['namespace'],
                                                                deployment_name=data['deployment_name'],
                                                                region_name=self.region_name)
        assert detail_result.status_code == 200, "获取部署{}详情页失败,{}".format(data['deployment_name'], detail_result.text)

        deployment_status = self.deployment.get_deployment_status(ns_name=data['namespace'], deployment_name=data["deployment_name"])
        assert deployment_status, "更新部署，验证部署状态出错: 部署：{} is not running".format(data["deployment_name"])

    @pytest.mark.upgrade
    def 测试部署详情(self):
        data = self.deployment.data_list
        detail_result = self.deployment.detail_deplyment_jinja2(ns_name=data['namespace'], deployment_name=data['deployment_name'],
                                                                region_name=self.region_name)
        assert detail_result.status_code == 200, "获取部署{}详情页失败,{}".format(data['deployment_name'], detail_result.text)

        value = self.deployment.generate_jinja_data("./verify_data/deployment/create_deployment.jinja2", data)
        assert self.deployment.is_sub_dict(value, detail_result.json()), \
            "创建Deployment_必填参数比对数据失败，返回数据:{},期望数据:{}".format(detail_result.json(), value)

    @pytest.mark.upgrade
    def 测试_创建自动扩缩容(self):
        data = self.deployment.hpa_list
        ret = self.deployment_client.create_hpa(self.k8s_namespace, './test_data/application/create_hpa.jinja2', data=data)

        assert ret.status_code == 201, "创建自动扩缩容失败:{}".format(ret.text)
        values = self.deployment_client.generate_jinja_data("verify_data/application/hpa_response.jinja2", data=data)
        assert self.deployment_client.is_sub_dict(values, ret.json()), \
            "创建自动扩缩容比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)

    @pytest.mark.upgrade
    def 测试_自动扩缩容详情(self):
        data = self.deployment.hpa_list
        ret = self.deployment_client.get_hpa(self.k8s_namespace, data['deployment_name'])
        assert ret.status_code == 200, "自动扩缩容详情失败:{}".format(ret.text)
        values = self.deployment_client.generate_jinja_data("verify_data/application/hpa_response.jinja2", data=data)
        assert self.deployment_client.is_sub_dict(values, ret.json()), \
            "自动扩缩容详情比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)

    @pytest.mark.upgrade
    def 测试_更新自动扩缩容(self):
        data = self.deployment.hpa_list
        data.update({"max": "1"})
        data.update({"percent": "90"})
        ret = self.deployment_client.update_hpa(self.k8s_namespace, data['deployment_name'],
                                                './test_data/application/create_hpa.jinja2', data=data)

        assert ret.status_code == 200, "更新自动扩缩容失败:{}".format(ret.text)
        values = self.deployment_client.generate_jinja_data("verify_data/application/hpa_response.jinja2", data=data)
        assert self.deployment_client.is_sub_dict(values, ret.json()), \
            "更新自动扩缩容比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)

    @pytest.mark.delete
    def 测试_删除自动扩缩容(self):
        data = self.deployment.hpa_list
        ret = self.deployment_client.delete_hpa(self.k8s_namespace, data['deployment_name'])
        assert ret.status_code == 200, "删除自动扩缩容失败:{}".format(ret.text)
        values = self.deployment_client.generate_jinja_data("verify_data/application/delete_hpa_response.jinja2", data)
        assert self.deployment_client.is_sub_dict(values, ret.json()), \
            "删除自动扩缩容比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)
        delete_flag = self.deployment_client.check_exists(
            self.deployment_client.common_hpa_url(self.k8s_namespace, data['deployment_name']), 404)
        assert delete_flag, "删除自动扩缩容失败"

    @pytest.mark.metis
    @pytest.mark.delete
    def 测试_删除创建Deployment(self):
        data = self.deployment.data_list
        delete_result = self.deployment.delete_deplyment(ns_name=data['namespace'],
                                                         deployment_name=data['deployment_name'],
                                                         region_name=self.region_name)
        assert delete_result.status_code == 200, "删除部署{}失败:{}".format(data['deployment_name'], delete_result.text)
