import pytest
from new_case.containerplatform.deployment.deployment import Deployment
from common import settings


@pytest.mark.erebus
@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestDeploymentSuiteGet(object):

    def setup_class(self):
        self.deployment = Deployment()

    def 测试_获取部署列表_有limit参数(self):
        ret = self.deployment.get_deplyment_list(limit=20)
        assert ret.status_code == 200, "获取部署列表失败:{}".format(ret.text)
        ret2 = self.deployment.get_deplyment_list(limit=1)
        deployment_num = len(ret2.json()["items"])
        assert deployment_num == 1, "获取部署列表,limit=1时失败,预期返回1个部署,实际返回{}个".format(deployment_num)

        global name
        name = Deployment.get_value(ret2.json(), "items.0.metadata.name")

        global cnt
        cnt = Deployment.get_value(ret2.json(), "metadata.continue")

    def 测试_获取部署列表_有limit参数和continue参数(self):
        ret = self.deployment.get_deplyment_list(limit=1, cnt=cnt)
        assert ret.status_code == 200, "获取部署列表_有limit和continue参数:{}".format(ret.text)
        name_continue = Deployment.get_value(ret.json(), "items.0.metadata.name")
        assert name_continue != name, "分页数据相同，部署第一页数据:{},部署第二页数据:{}".format(name, name_continue)

    def 测试_搜索部署(self):
        data = self.deployment.data_list_strategy_labels
        ser = self.deployment.search_deplyment(ns_name=data['namespace'], deployment_name=data['deployment_name'])
        assert ser.status_code == 200, "没有搜索到{}，{}".format(data["deployment_name"], ser.text)

        value = self.deployment.generate_jinja_data("./verify_data/deployment/search_deployment.jinja2", data)
        assert self.deployment.is_sub_dict(value, ser.json()), "搜索部署比对数据失败，返回数据:{},期望数据:{}".format(ser.json(), value)
