import sys
from common.base_request import Common
from common.log import logger
from common import settings
from time import sleep
import random


class Deployment(Common):
    def __init__(self):
        super(Deployment, self).__init__()
        self.genetate_global_info()

    def get_common_deployment_url(self, region_name=settings.REGION_NAME, ns_name=settings.K8S_NAMESPACE,
                                  deployment_name=''):
        return 'kubernetes/{}/apis/apps/v1/namespaces/{}/deployments/{}'.format(region_name, ns_name, deployment_name)

    def get_common_deployment_list_url(self, ns_name=settings.K8S_NAMESPACE, limit=1, cnt=""):
        return limit and "kubernetes/{}/apis/apps/v1/namespaces/{}/deployments?limit={}&continue={}"\
            .format(self.region_name, ns_name, limit, cnt)

    def get_common_deployment_search_url(self, ns_name=settings.K8S_NAMESPACE, limit=1, cnt="", deployment_name=''):
        return limit and "acp/v1/resources/search/kubernetes/{}/apis/apps/v1/namespaces/{}/deployments?" \
                         "limit={}&continue={}&keyword={}&field=metadata.name".format(self.region_name, ns_name, limit, cnt, deployment_name)

    def get_deployment_pod_url(self, region_name=settings.REGION_NAME, ns_name=settings.K8S_NAMESPACE, limit=10,
                               deployment_name=''):
        return limit and "kubernetes/{}/api/v1/namespaces/{}/pods?limit={}&labelSelector=app={}"\
            .format(region_name, ns_name, limit, deployment_name)

    def get_nodes_labels_url(self, region_name=settings.REGION_NAME):
        return "acp/v1/resources/{}/nodes/labels".format(region_name)

    def create_deployment_jinja2(self, file, data, region_name, ns_name):
        path = self.get_common_deployment_url(region_name, ns_name)
        data = self.generate_jinja_data(file, data)
        logger.info(data)
        return self.send(method='post', path=path, json=data, params={})

    def detail_deplyment_jinja2(self, ns_name, deployment_name, region_name):
        path = self.get_common_deployment_url(ns_name=ns_name, deployment_name=deployment_name, region_name=region_name)
        return self.send(method='get', path=path, params={})

    def get_deplyment_pods(self, ns_name, deployment_name, region_name, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_deployment_pod_url(ns_name=ns_name, deployment_name=deployment_name, region_name=region_name)
        return self.send(method='get', path=url, auth=auth)

    def get_deplyment_list(self, limit=None, cnt=""):
        path = self.get_common_deployment_list_url(limit=limit, cnt=cnt)
        return self.send(method='get', path=path, params={})

    def search_deplyment(self, ns_name, deployment_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_deployment_search_url(ns_name=ns_name, deployment_name=deployment_name)
        return self.send(method='get', path=url)

    def update_deplyment_jinja2(self, ns_name, deployment_name, file, data):
        path = self.get_common_deployment_url(ns_name=ns_name, deployment_name=deployment_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='put', path=path, json=data, params={})

    def delete_deplyment(self, ns_name, deployment_name, region_name, auth=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_deployment_url(ns_name=ns_name, deployment_name=deployment_name, region_name=region_name)
        return self.send(method='delete', path=url, auth=auth)

    # 状态判断链接文档：http://confluence.alauda.cn/pages/viewpage.action?pageId=28508749
    def get_deployment_status(self, ns_name, deployment_name, region_name=settings.REGION_NAME, key_spec_replicase="spec.replicas",
                              key_status_updatedreplicas="status.updatedReplicas",
                              key_status_replicas="status.replicas", key_status_availablereplicas="status.availableReplicas",
                              key_status_observedgeneration="status.observedGeneration", key_metadata_generation="metadata.generation"):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        cnt = 0
        flag = False
        while cnt < 30 and not flag:
            try:
                cnt += 1
                sleep(3)
                url = "kubernetes/{}/apis/apps/v1/namespaces/{}/deployments/{}". \
                    format(region_name, ns_name, deployment_name)
                response = self.send(method="GET", path=url)
                assert response.status_code == 200, "get deployment status failed"
                value_spec_replicase = self.get_value(response.json(), key_spec_replicase)
                if 'updatedReplicas' in response.text:
                    value_status_updatedreplicas = self.get_value(response.json(), key_status_updatedreplicas)
                else:
                    value_status_updatedreplicas = 0
                value_status_replicas = self.get_value(response.json(), key_status_replicas)

                if 'unavailableReplicas' not in response.text:
                    value_status_availablereplicas = self.get_value(response.json(), key_status_availablereplicas)
                else:
                    value_status_availablereplicas = 0
                value_status_observedgeneration = self.get_value(response.json(), key_status_observedgeneration)
                value_metadata_generation = self.get_value(response.json(), key_metadata_generation)
                if value_spec_replicase > 0 and value_status_updatedreplicas == value_spec_replicase and \
                        value_status_replicas == value_spec_replicase and \
                        value_status_availablereplicas == value_spec_replicase and \
                        value_status_observedgeneration >= value_metadata_generation:
                    flag = True
                    break
            except Exception as e:
                logger.error(e)
        return flag

    def get_deployment_logs(self, ns_name, pod_name, container_name):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        url = "acp/v1/kubernetes/{}/namespaces/{}/pods/{}/{}/log?previous=false&logFilePosition=end" \
              "&referenceTimestamp=newest&offsetFrom=2000000000&offsetTo=2000000100". \
            format(self.region_name, ns_name, pod_name, container_name)
        return self.send(method="get", path=url)

    def delete_pod(self, ns_name, pod_name):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        url = "kubernetes/{}/api/v1/namespaces/{}/pods/{}".format(self.region_name, ns_name, pod_name)
        return self.send(method="delete", path=url)

    def stop_start_deployment(self, ns_name, deployment_name, file, data):
        path = self.get_common_deployment_url(ns_name=ns_name, deployment_name=deployment_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='put', path=path, json=data, params={})

    def get_deployment_alarm(self, ns_name, deployment_name):
        logger.info(sys._getframe().f_code.co_name.center(50, "*"))
        url = "kubernetes/{}/apis/monitoring.coreos.com/v1/namespaces/{}/prometheusrules?" \
              "limit=20&labelSelector=alert.alauda.io/owner=System,alert.alauda.io/namespace={}," \
              "alert.alauda.io/kind=Deployment,alert.alauda.io/name={}".format(self.region_name, ns_name, ns_name,
                                                                               deployment_name)
        return self.send(method="get", path=url)

    def get_nodes_labels(self, region_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_nodes_labels_url(region_name)
        return self.send(method='get', path=url)

    def kubectl_exec(self, deployment_name, ns_name, region_name, cmd, expect_value, public_ip=''):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        public_ip = public_ip or self.global_info['public_ips'][0]
        pod_result = self.get_deplyment_pods(ns_name, deployment_name, region_name)
        pod_name = self.get_value(pod_result.json(), 'items.0.metadata.name')
        command = "kubectl exec -it {} -n {} -- {}".format(pod_name, ns_name, cmd)
        ret_excute = self.excute_script(command, public_ip)
        return expect_value in ret_excute[1], ret_excute[1]

    # 创建Deployment_必填参数
    data_list = {
        "deployment_name": '{}-deployment'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME
    }

    hpa_list = {
        'namespace': settings.K8S_NAMESPACE,
        'deployment_name': '{}-deployment-hpa'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        'replicas': 1,
        'max': 2,
        'min': 1,
        'percent': 10
    }

    # 创建Deployment_必填参数_可选参数有两个_更新策略和组件标签
    data_list_strategy_labels = {
        "deployment_name": '{}-deployment-strategy-labels'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME,
        "maxsurge": "50%",
        "maxunavailable": "20%",
        "metadata_labels": {"key1": "value1", "key2": "value2"}
    }
    # 创建Deployment_必填参数_可选参数有三个_启动命令和参数和健康检查
    data_list_args_livenessprobe = {
        "deployment_name": '{}-deployment-strategy-args-livenessprobe'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME,
        "args": " ",
        "args_1": "-c",
        "args_2": "while true; do sleep 30; echo 123; done",
        "command": " ",
        "command_1": "/bin/sh",
        "livenessProbe": " ",
        "failurethreshold": 5,
        "path": "/",
        "port": 80,
        "scheme": "HTTP",
        "initialdelayseconds": 300,
        "periodseconds": 60,
        "successthreshold": 1,
        "timeoutSeconds": 30
    }

    # 创建Deployment_必填参数_可选参数有一个配置引用-configmap
    data_list_configmap = {
        "deployment_name": '{}-deployment-configmap-2'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME,
        "image": settings.IMAGE,
        "envFrom": " ", "configMapRef": " ",
        "configmap_name": "{}-ares-deployment-configmap-2".format(settings.RESOURCE_PREFIX)
    }

    # 创建Deployment_必填参数_可选参数有两个_存储卷挂载和存储卷
    data_list_volume = {
        "deployment_name": '{}-deployment-volume'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME,
        "volumeMounts": " ",
        "mountpath": "/pvc/test",
        "volume_name": "new-volume",
        "volumes": " ",
        "claimname": '{}-ares-deployment-pvc'.format(settings.RESOURCE_PREFIX).replace('_', '-')
    }

    # 创建Deployment_必填参数_可选参数只有三个_环境变量和日志文件和排除日志文件
    data_list_file_log_exclude_log = {
        "deployment_name": '{}-deployment-file-log-exclude-log'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME,
        "env": " ",
        "env_name": "env-name1",
        "env_value": "env-value1",
        "file_log_name": "__FILE_LOG_PATH__",
        "file_log_value": "/var/*.*",
        "exclude_log_name": "__EXCLUDE_LOG_PATH__",
        "exclude_log_value": "/var/hehe.txt"
    }

    # 创建Deployment_必填参数有两个_网络模式和配置引用_secret
    data_list_hostnetwork_secret = {
        "deployment_name": '{}-deployment-hostnetwork-secret'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME,
        "hostNetwork": " ",
        "hostnetwork": "true",
        "envFrom": " ",
        "secretRef": " ",
        "secret_name": "{}-ares-deployment-secret".format(settings.RESOURCE_PREFIX)
    }

    # 创建Deployment_亲和性_基础deployment
    data_list_affinify_base = {
        "deployment_name": '{}-alauda-appbaseforaffinity'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME
    }

    # 创建Deployment_亲和性_亲和deployment
    data_list_affinify_app_name = {
        "deployment_name": '{}-alauda-app-affinity'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME,
        "affinity": " ",
        "affinity_type": "podAffinity",
        "deployment_name_affinify_base": '{}-alauda-appbaseforaffinity'.format(settings.RESOURCE_PREFIX).replace('_', '-')
    }

    # 创建Deployment_亲和性_反亲和deployment
    data_list_antiaffinify_app_name = {
        "deployment_name": '{}-alauda-app-antiaffinify'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME,
        "affinity": " ",
        "affinity_type": "podAntiAffinity",
        "deployment_name_affinify_base": '{}-alauda-appbaseforaffinity'.format(settings.RESOURCE_PREFIX).replace('_', '-')
    }

    # 创建Deployment_端口
    data_list_port = {
        "deployment_name": '{}-deployment-port'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME,
        "ports": " ",
        "protocol": "TCP",
        "hostPort": random.randrange(32000, 34000, 1),
        "containerPort": 80
    }

    # 创建Deployment_主机选择器
    data_list_nodeselector = {
        "deployment_name": '{}-deployment-nodeselector'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.K8S_NAMESPACE,
        "image": settings.IMAGE,
        "default_label": settings.DEFAULT_LABEL,
        "username": settings.USERNAME,
        "nodeSelector": " ",
        "nodelabel_key": "kubernetes.io/hostname"
    }
