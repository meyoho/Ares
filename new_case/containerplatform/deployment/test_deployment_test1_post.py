import pytest
from new_case.containerplatform.deployment.deployment import Deployment
from common import settings
from common.settings import K8S_NAMESPACE
from common.base_request import Common
from new_case.containerplatform.configmap.configmap import Configmap
from new_case.containerplatform.persistentvolumeclaims.pvc import Pvc
from new_case.operation.log.log import Log
from new_case.containerplatform.secret.secret import Secret
from time import time
import base64
import json
from time import sleep


@pytest.mark.erebus
@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestDeploymentSuitePost(object):

    def setup_class(self):
        self.deployment = Deployment()

        self.configmap_tool = Configmap()
        self.verify_template_configmap = Common.generate_jinja_template(self,
                                                                        './verify_data/configmap/create_response.jinja2')

        self.pvc = Pvc()
        self.pvcname = '{}-ares-deployment-pvc'.format(settings.RESOURCE_PREFIX).replace('_', '-')

        self.log = Log()

        self.secret_tool = Secret()
        self.secret_name = "{}-ares-deployment-secret".format(settings.RESOURCE_PREFIX)
        self.verify_template = Common.generate_jinja_template(self, './verify_data/secret/create_response.jinja2')

        self.region_name = settings.REGION_NAME
        self.k8s_namespace = K8S_NAMESPACE

    @pytest.mark.metis
    def 测试_创建Deployment_必填参数_可选参数有两个_更新策略和组件标签(self):
        data = self.deployment.data_list_strategy_labels
        self.deployment.delete_deplyment(ns_name=data['namespace'], deployment_name=data['deployment_name'],
                                         region_name=self.region_name)
        create_result = self.deployment.create_deployment_jinja2("./test_data/deployment/deployment.jinja2", data=data,
                                                                 region_name=self.region_name,
                                                                 ns_name=self.k8s_namespace)
        assert create_result.status_code == 201, "创建{}部署失败{}".format(data['deployment_name'], create_result.text)

        detail_result = self.deployment.detail_deplyment_jinja2(ns_name=data['namespace'],
                                                                deployment_name=data['deployment_name'],
                                                                region_name=self.region_name)
        assert detail_result.status_code == 200, "获取部署{}详情页失败,{}".format(data['deployment_name'], detail_result.text)

        deployment_status = self.deployment.get_deployment_status(ns_name=data['namespace'],
                                                                  deployment_name=data["deployment_name"])
        assert deployment_status, "创建部署后，验证部署状态出错: 部署：{} is not running".format(data["deployment_name"])

        value = self.deployment.generate_jinja_data("./verify_data/deployment/create_deployment.jinja2", data)
        assert self.deployment.is_sub_dict(value, create_result.json()), \
            "创建Deployment_必填参数_可选参数有两个_更新策略和组件标签比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

    def 测试创建deployment事件(self):
        payload = {"cluster": settings.REGION_NAME,
                   "kind": "Deployment",
                   "name": self.deployment.data_list_strategy_labels["deployment_name"],
                   "namespace": settings.K8S_NAMESPACE, "page": 1, "page_size": 20}
        kevents_results = self.deployment.get_kevents(payload=payload, expect_value="ScalingReplicaSet", times=2)
        assert kevents_results, "创建deployment获取事件获取失败"

    @pytest.mark.metis
    def 测试_创建Deployment_必填参数_可选参数有三个_启动命令和参数和健康检查(self):
        data = self.deployment.data_list_args_livenessprobe
        self.deployment.delete_deplyment(ns_name=data['namespace'], deployment_name=data['deployment_name'],
                                         region_name=self.region_name)
        create_result = self.deployment.create_deployment_jinja2("./test_data/deployment/deployment.jinja2", data=data,
                                                                 region_name=self.region_name,
                                                                 ns_name=self.k8s_namespace)
        assert create_result.status_code == 201, "创建{}部署失败{}".format(data['deployment_name'], create_result.text)

        detail_result = self.deployment.detail_deplyment_jinja2(ns_name=data['namespace'],
                                                                deployment_name=data['deployment_name'],
                                                                region_name=self.region_name)
        assert detail_result.status_code == 200, "获取部署{}详情页失败,{}".format(data['deployment_name'], detail_result.text)

        deployment_status = self.deployment.get_deployment_status(ns_name=data['namespace'],
                                                                  deployment_name=data["deployment_name"])
        assert deployment_status, "创建部署后，验证部署状态出错: 部署：{} is not running".format(data["deployment_name"])

        value = self.deployment.generate_jinja_data("./verify_data/deployment/create_deployment.jinja2", data)
        assert self.deployment.is_sub_dict(value, create_result.json()), \
            "创建Deployment_必填参数_可选参数有两个_更新策略和组件标签比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

    @pytest.mark.metis
    def 测试_创建Deployment_必填参数_可选参数有一个配置引用_configmap(self):
        result = {"flag": True}
        # 创建configmap
        configmap_name = self.deployment.data_list_configmap["configmap_name"]
        cm_data = {
            "configmap_name": configmap_name,
            "description": "key-value",
            "namespace": settings.K8S_NAMESPACE,
            "configmap_key": "key",
            "configmap_value": "value"
        }
        self.configmap_tool.delete_configmap(configmap_name)
        ret = self.configmap_tool.create_configmap(data=cm_data)
        assert ret.status_code == 201, "创建配置字典失败:{}".format(ret.text)

        value = self.verify_template_configmap.render(cm_data)
        assert self.configmap_tool.is_sub_dict(json.loads(value),
                                               ret.json()), "创建configmap比对数据失败，返回数据:{},期望数据:{}".format(ret.json(),
                                                                                                       value)

        # 创建deployment
        data = self.deployment.data_list_configmap
        self.deployment.delete_deplyment(ns_name=data['namespace'], deployment_name=data['deployment_name'],
                                         region_name=self.region_name)
        create_result = self.deployment.create_deployment_jinja2("./test_data/deployment/deployment.jinja2", data=data,
                                                                 region_name=self.region_name,
                                                                 ns_name=self.k8s_namespace)
        assert create_result.status_code == 201, "创建{}部署失败{}".format(data['deployment_name'], create_result.text)

        value = self.deployment.generate_jinja_data("./verify_data/deployment/create_deployment.jinja2", data)
        assert self.deployment.is_sub_dict(value, create_result.json()), \
            "创建Deployment_必填参数_可选参数有一个配置引用_configmap比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

        # 验证
        detail_result = self.deployment.detail_deplyment_jinja2(ns_name=data['namespace'],
                                                                deployment_name=data['deployment_name'],
                                                                region_name=self.region_name)
        assert detail_result.status_code == 200, "获取部署{}详情页失败,{}".format(data['deployment_name'], detail_result.text)

        deployment_status = self.deployment.get_deployment_status(ns_name=data['namespace'],
                                                                  deployment_name=data["deployment_name"])
        assert deployment_status, "创建部署后，验证部署状态出错: 部署：{} is not running".format(data["deployment_name"])

        content = detail_result.json()
        v1 = self.deployment.get_value(content, 'spec.template.spec.containers.0.envFrom.0.configMapRef.name')
        result = self.deployment.update_result(result, v1 == configmap_name, '环境变量完整引用configmap失败')

        ret = self.configmap_tool.delete_configmap(configmap_name=data['configmap_name'])
        assert ret.status_code == 200, "删除配置字典{}失败:{}".format(data['configmap_name'], ret.text)
        assert result['flag'], result

    @pytest.mark.metis
    @pytest.mark.skipif(not settings.SC_NAME, reason="没有存储类，不运行当前用例")
    def 测试_创建Deployment_必填参数_可选参数有两个_存储卷挂载和存储卷(self):
        result = {"flag": True}
        # 创建pvc
        pvc_data = {
            "pvc_name": self.pvcname,
            "namespace": settings.K8S_NAMESPACE,
            "storageclassname": settings.SC_NAME
        }
        data = self.deployment.data_list_volume
        self.deployment.delete_deplyment(ns_name=data['namespace'], deployment_name=data['deployment_name'],
                                         region_name=self.region_name)
        self.pvc.delete_pvc(settings.K8S_NAMESPACE, self.pvcname)
        self.pvc.check_exists(self.pvc.get_common_pvc_url_v1(pvc_data["namespace"], pvc_data['pvc_name']), 404)

        create_result = self.pvc.create_pvc_jinja2("./test_data/pvc/pvc.jinja2", data=pvc_data)
        assert create_result.status_code == 201, "创建pvc{}失败:{}".format(pvc_data['pvc_name'], create_result.text)

        pvc_status = self.pvc.get_status(self.pvc.get_common_pvc_url_v1(pvc_name=pvc_data["pvc_name"]), "status.phase",
                                         "Bound")
        assert pvc_status, "创建pvc状态错误:{}的状态没有被绑定".format(pvc_data["pvc_name"])

        value = self.pvc.generate_jinja_data("./verify_data/pvc/create-pvc.jinja2", pvc_data)
        assert self.pvc.is_sub_dict(value, create_result.json()), "创建pvc_必填参数_可选参数为空比对数据失败，返回数据:{},期望数据:{}".format(
            create_result.json(), value)

        # 创建deployment
        create_result = self.deployment.create_deployment_jinja2("./test_data/deployment/deployment.jinja2", data=data,
                                                                 region_name=self.region_name,
                                                                 ns_name=self.k8s_namespace)
        assert create_result.status_code == 201, "创建{}部署失败{}".format(data['deployment_name'], create_result.text)

        value = self.deployment.generate_jinja_data("./verify_data/deployment/create_deployment.jinja2", data)
        assert self.deployment.is_sub_dict(value, create_result.json()), \
            "创建Deployment_必填参数_可选参数只有一个_存储比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

        # 验证
        detail_result = self.deployment.detail_deplyment_jinja2(ns_name=data['namespace'],
                                                                deployment_name=data['deployment_name'],
                                                                region_name=self.region_name)
        assert detail_result.status_code == 200, "获取部署{}详情页失败,{}".format(data['deployment_name'], detail_result.text)

        deployment_status = self.deployment.get_deployment_status(ns_name=data['namespace'],
                                                                  deployment_name=data["deployment_name"])
        assert deployment_status, "创建部署后，验证部署状态出错: 部署：{} is not running".format(data["deployment_name"])

        content = detail_result.json()
        claimName = self.deployment.get_value(content, 'spec.template.spec.volumes.0.persistentVolumeClaim.claimName')
        result = self.deployment.update_result(result, claimName == self.pvcname, '部署挂载pvc失败')

        # 删除deployment
        self.deployment.delete_deplyment(ns_name=data['namespace'], deployment_name=data['deployment_name'],
                                         region_name=self.region_name)
        self.deployment.check_exists(self.deployment.get_common_deployment_url(ns_name=data['namespace'],
                                                                               deployment_name=data['deployment_name']),
                                     404)
        # 删除pvc
        delete_result = self.pvc.delete_pvc(ns_name=pvc_data["namespace"], pvc_name=pvc_data["pvc_name"])
        assert delete_result.status_code == 200, "删除pvc失败{}".format(delete_result.text)
        assert result['flag'], result

    @pytest.mark.skip(reason="日志case有问题,施源后续改")
    @pytest.mark.metis
    def 测试_创建Deployment_必填参数_可选参数有三个_环境变量和日志文件和排除日志文件(self):
        data = self.deployment.data_list_file_log_exclude_log
        self.deployment.delete_deplyment(ns_name=data['namespace'], deployment_name=data['deployment_name'],
                                         region_name=self.region_name)
        create_result = self.deployment.create_deployment_jinja2("./test_data/deployment/deployment.jinja2", data=data,
                                                                 region_name=self.region_name,
                                                                 ns_name=self.k8s_namespace)
        assert create_result.status_code == 201, "创建{}部署失败{}".format(data['deployment_name'], create_result.text)

        # 获取当前时间
        current_time = time()

        detail_result = self.deployment.detail_deplyment_jinja2(ns_name=data['namespace'],
                                                                deployment_name=data['deployment_name'],
                                                                region_name=self.region_name)
        assert detail_result.status_code == 200, "获取部署{}详情页失败,{}".format(data['deployment_name'], detail_result.text)

        deployment_status = self.deployment.get_deployment_status(ns_name=data['namespace'],
                                                                  deployment_name=data["deployment_name"])
        assert deployment_status, "创建部署后，验证部署状态出错: 部署：{} is not running".format(data["deployment_name"])
        pod_result = self.deployment.get_deplyment_pods(ns_name=data['namespace'],
                                                        deployment_name=data['deployment_name'],
                                                        region_name=self.region_name)
        assert pod_result.status_code == 200, "获取pod{}列表页失败,{}".format(data['deployment_name'], pod_result.text)
        pod_name = self.deployment.get_value_list(pod_result.json(), "items||metadata.name")

        value = self.deployment.generate_jinja_data("./verify_data/deployment/create_deployment.jinja2", data)
        assert self.deployment.is_sub_dict(value, create_result.json()), \
            "创建Deployment_必填参数_可选参数有两个_更新策略和组件标签比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

        # 验证
        result = {"flag": True}
        params = {"pods": ",".join(pod_name), "paths": "hchan.txt", "start_time": current_time}
        log_result = self.log.get_logs_search(payload=params, cnt=100)
        result = self.log.update_result(result, self.log.get_value(log_result.json(), 'total_items') > 0,
                                        "搜索日志文件返回数据为空{}".format(log_result.text))

        params = {"pods": pod_name, "paths": "hehe.txt", "start_time": current_time}
        log_result = self.log.get_logs_search(payload=params, cnt=5)
        result = self.log.update_result(result, self.log.get_value(log_result.json(), 'total_items') == 0,
                                        "搜索排除日志文件返回数据不为空{}".format(log_result.text))
        assert result['flag'], result

    @pytest.mark.metis
    def 测试_创建Deployment_必填参数_可选参数有两个_网络模式和配置引用_secret(self):
        result = {"flag": True}
        # 创建secret
        secret_data = {
            "secret_name": self.secret_name,
            "secret_type": "Opaque",
            "opaque_key": "opaque_key",
            "opaque_value": str(base64.b64encode("opaque_value".encode('utf-8')), 'utf8')}
        secret_data.update({"K8S_NAMESPACE": settings.K8S_NAMESPACE})
        self.secret_tool.delete_secret(self.secret_name)
        ret = self.secret_tool.create_secret('./test_data/secret/create_secret.jinja2', data=secret_data)

        assert ret.status_code == 201, "创建{}类型保密字典失败:{}".format(secret_data['secret_type'], ret.text)
        value = self.verify_template.render(secret_data)
        assert self.secret_tool.is_sub_dict(json.loads(value), ret.json()), "创建保密字典比对数据失败，返回数据:{},期望数据:{}".format(
            ret.json(), json.loads(value))

        # 创建deployment
        data = self.deployment.data_list_hostnetwork_secret
        self.deployment.delete_deplyment(ns_name=data['namespace'], deployment_name=data['deployment_name'],
                                         region_name=self.region_name)
        create_result = self.deployment.create_deployment_jinja2("./test_data/deployment/deployment.jinja2", data=data,
                                                                 region_name=self.region_name,
                                                                 ns_name=self.k8s_namespace)
        assert create_result.status_code == 201, "创建{}部署失败{}".format(data['deployment_name'], create_result.text)

        value = self.deployment.generate_jinja_data("./verify_data/deployment/create_deployment.jinja2", data)
        assert self.deployment.is_sub_dict(value, create_result.json()), \
            "创建Deployment_必填参数_可选参数只有一个_配置引用_secret比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

        # 验证
        detail_result = self.deployment.detail_deplyment_jinja2(ns_name=data['namespace'],
                                                                deployment_name=data['deployment_name'],
                                                                region_name=self.region_name)
        assert detail_result.status_code == 200, "获取部署{}详情页失败,{}".format(data['deployment_name'], detail_result.text)
        deployment_status = self.deployment.get_deployment_status(ns_name=data['namespace'],
                                                                  deployment_name=data["deployment_name"])
        assert deployment_status, "创建部署后，验证部署状态出错: 部署：{} is not running".format(data["deployment_name"])
        content = detail_result.json()
        v1 = self.deployment.get_value(content, 'spec.template.spec.containers.0.envFrom.0.secretRef.name')
        result = self.deployment.update_result(result, v1 == self.secret_name, '环境变量完整引用secret失败')

        ret = self.secret_tool.delete_secret(data["secret_name"])
        assert ret.status_code == 200, "删除{}类型的保密字典失败:{}".format(data["secret_type"], ret.text)
        assert result['flag'], result

    @pytest.mark.metis
    def 测试_创建Deployment_必填参数_可选参数亲和性(self):
        result = {"flag": True}
        # 创建基础deployment
        data_affinify_base = self.deployment.data_list_affinify_base
        self.deployment.delete_deplyment(ns_name=data_affinify_base['namespace'],
                                         deployment_name=data_affinify_base['deployment_name'],
                                         region_name=self.region_name)
        create_result = self.deployment.create_deployment_jinja2("./test_data/deployment/deployment.jinja2",
                                                                 data=data_affinify_base, region_name=self.region_name,
                                                                 ns_name=self.k8s_namespace)
        assert create_result.status_code == 201, "创建基础{}部署失败{}".format(data_affinify_base['deployment_name'],
                                                                       create_result.text)

        detail_result = self.deployment.detail_deplyment_jinja2(ns_name=data_affinify_base['namespace'],
                                                                deployment_name=data_affinify_base['deployment_name'],
                                                                region_name=self.region_name)
        assert detail_result.status_code == 200, "获取基础部署{}详情页失败,{}".format(data_affinify_base['deployment_name'],
                                                                           detail_result.text)

        deployment_status = self.deployment.get_deployment_status(ns_name=data_affinify_base['namespace'],
                                                                  deployment_name=data_affinify_base["deployment_name"])
        assert deployment_status, "创建部署后，验证部署状态出错: 部署：{} is not running".format(data_affinify_base["deployment_name"])

        pod_result = self.deployment.get_deplyment_pods(ns_name=data_affinify_base['namespace'],
                                                        deployment_name=data_affinify_base['deployment_name'],
                                                        region_name=self.region_name)
        assert pod_result.status_code == 200, "获取基础部署容器组失败:{}".format(pod_result.text)
        hostip_base = self.deployment.get_value(pod_result.json(), 'items.0.status.hostIP')

        # 创建亲和deployment
        data_affinify_app_name = self.deployment.data_list_affinify_app_name
        self.deployment.delete_deplyment(ns_name=data_affinify_app_name['namespace'],
                                         deployment_name=data_affinify_app_name['deployment_name'],
                                         region_name=self.region_name)
        create_affinity_result = self.deployment.create_deployment_jinja2("./test_data/deployment/deployment.jinja2",
                                                                          data=data_affinify_app_name,
                                                                          region_name=self.region_name,
                                                                          ns_name=self.k8s_namespace)
        assert create_affinity_result.status_code == 201, "创建亲和{}部署失败{}" \
            .format(data_affinify_app_name['deployment_name'], create_affinity_result.text)

        detail_result = self.deployment.detail_deplyment_jinja2(ns_name=data_affinify_app_name['namespace'],
                                                                deployment_name=data_affinify_app_name[
                                                                    'deployment_name'],
                                                                region_name=self.region_name)
        assert detail_result.status_code == 200, "获取基础部署{}详情页失败,{}".format(data_affinify_app_name['deployment_name'],
                                                                           detail_result.text)

        deployment_status = self.deployment.get_deployment_status(ns_name=data_affinify_app_name['namespace'],
                                                                  deployment_name=data_affinify_app_name[
                                                                      "deployment_name"])
        assert deployment_status, "创建部署后，验证部署状态出错: 部署：{} is not running".format(
            data_affinify_app_name["deployment_name"])

        pod_result = self.deployment.get_deplyment_pods(ns_name=data_affinify_app_name['namespace'],
                                                        deployment_name=data_affinify_app_name['deployment_name'],
                                                        region_name=self.region_name)
        assert pod_result.status_code == 200, "获取基础部署容器组失败:{}".format(pod_result.text)
        hostip_affinify = self.deployment.get_value(pod_result.json(), 'items.0.status.hostIP')

        result = self.deployment.update_result(result, hostip_base == hostip_affinify, "亲和性验证失败")

        # 创建反亲和deployment
        data_antiaffinify_app_name = self.deployment.data_list_antiaffinify_app_name
        self.deployment.delete_deplyment(ns_name=data_antiaffinify_app_name['namespace'],
                                         deployment_name=data_antiaffinify_app_name['deployment_name'],
                                         region_name=self.region_name)
        create_antiaffinity_result = self.deployment.create_deployment_jinja2(
            "./test_data/deployment/deployment.jinja2",
            data=data_antiaffinify_app_name,
            region_name=self.region_name, ns_name=self.k8s_namespace)
        assert create_antiaffinity_result.status_code == 201, "创建反亲和{}部署失败{}". \
            format(data_antiaffinify_app_name['deployment_name'], create_antiaffinity_result.text)

        detail_result = self.deployment.detail_deplyment_jinja2(ns_name=data_antiaffinify_app_name['namespace'],
                                                                deployment_name=data_antiaffinify_app_name[
                                                                    'deployment_name'],
                                                                region_name=self.region_name)
        assert detail_result.status_code == 200, "获取基础部署{}详情页失败,{}". \
            format(data_antiaffinify_app_name['deployment_name'], detail_result.text)

        deployment_status = self.deployment.get_deployment_status(ns_name=data_antiaffinify_app_name['namespace'],
                                                                  deployment_name=data_antiaffinify_app_name[
                                                                      "deployment_name"])
        assert deployment_status, "创建部署后，验证部署状态出错: 部署：{} is not running". \
            format(data_antiaffinify_app_name["deployment_name"])

        pod_result = self.deployment.get_deplyment_pods(ns_name=data_antiaffinify_app_name['namespace'],
                                                        deployment_name=data_antiaffinify_app_name['deployment_name'],
                                                        region_name=self.region_name)
        assert pod_result.status_code == 200, "获取基础部署容器组失败:{}".format(pod_result.text)
        hostip_antiaffinify = self.deployment.get_value(pod_result.json(), 'items.0.status.hostIP')

        result = self.deployment.update_result(result, hostip_base != hostip_antiaffinify, "反亲和性验证失败")

        assert result['flag'], result

    @pytest.mark.metis
    def 测试_创建Deployment_必填参数_可选端口(self):
        data = self.deployment.data_list_port
        self.deployment.delete_deplyment(ns_name=data['namespace'], deployment_name=data['deployment_name'],
                                         region_name=self.region_name)
        create_result = self.deployment.create_deployment_jinja2("./test_data/deployment/deployment.jinja2", data=data,
                                                                 region_name=self.region_name,
                                                                 ns_name=self.k8s_namespace)
        assert create_result.status_code == 201, "创建{}部署失败{}".format(data['deployment_name'], create_result.text)

        detail_result = self.deployment.detail_deplyment_jinja2(ns_name=data['namespace'],
                                                                deployment_name=data['deployment_name'],
                                                                region_name=self.region_name)
        assert detail_result.status_code == 200, "获取部署{}详情页失败,{}".format(data['deployment_name'], detail_result.text)

        deployment_status = self.deployment.get_deployment_status(ns_name=data['namespace'],
                                                                  deployment_name=data["deployment_name"])
        assert deployment_status, "创建部署后，验证部署状态出错: 部署：{} is not running".format(data["deployment_name"])

        value = self.deployment.generate_jinja_data("./verify_data/deployment/create_deployment.jinja2", data)
        assert self.deployment.is_sub_dict(value, create_result.json()), \
            "创建Deployment_必填参数_可选参数有两个_更新策略和组件标签比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

    def 测试_创建Deployment_必填参数_可选主机选择器(self):
        data = self.deployment.data_list_nodeselector
        nodelabel_result = self.deployment.get_nodes_labels(region_name=self.region_name)
        assert nodelabel_result.status_code == 200, "集群的主机标签获取失败:{}".format(nodelabel_result.text)

        global node
        keys = list(nodelabel_result.json().keys())
        node = keys[0]
        data.update({"nodelabel_value": node})

        self.deployment.delete_deplyment(ns_name=data['namespace'], deployment_name=data['deployment_name'],
                                         region_name=self.region_name)
        create_result = self.deployment.create_deployment_jinja2("./test_data/deployment/deployment.jinja2", data=data,
                                                                 region_name=self.region_name,
                                                                 ns_name=self.k8s_namespace)
        assert create_result.status_code == 201, "创建{}部署失败{}".format(data['deployment_name'], create_result.text)

        value = self.deployment.generate_jinja_data("./verify_data/deployment/create_deployment.jinja2", data)
        assert self.deployment.is_sub_dict(value, create_result.json()), \
            "创建Deployment_必填参数_可选参数有两个_更新策略和组件标签比对数据失败，返回数据:{},期望数据:{}".format(create_result.json(), value)

        pod_ret = self.deployment.get_deplyment_pods(ns_name=data['namespace'], deployment_name=data['deployment_name'],
                                                     region_name=self.region_name)
        assert pod_ret.status_code == 200, "获取容器组失败:{}".format(pod_ret.text)
        sleep(10)

        if self.deployment.get_value(pod_ret.json(), 'items') != []:
            global nodename
            nodename = self.deployment.get_value(pod_ret.json(), 'items.0.spec.nodeName')
            assert nodename == node, "pod的容器节点名称显示错误：{}".format(pod_ret)
