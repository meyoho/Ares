import pytest
from new_case.containerplatform.deployment.deployment import Deployment
from common import settings
from new_case.containerplatform.persistentvolumeclaims.pvc import Pvc


@pytest.mark.erebus
@pytest.mark.archon
@pytest.mark.metis
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestDeploymentSuiteDelete(object):

    def setup_class(self):
        self.deployment = Deployment()

        self.pvc = Pvc()
        self.pvcname = '{}-ares-deployment-pvc'.format(settings.RESOURCE_PREFIX).replace('_', '-')

        self.region_name = settings.REGION_NAME

    def 测试_删除Deployment_必填参数_可选参数有两个_更新策略和组件标签(self):
        data = self.deployment.data_list_strategy_labels
        delete_result = self.deployment.delete_deplyment(ns_name=data['namespace'],
                                                         deployment_name=data['deployment_name'],
                                                         region_name=self.region_name)
        assert delete_result.status_code == 200, "删除部署{}失败:{}".format(data['deployment_name'], delete_result.text)

    def 测试_删除Deployment_必填参数_可选参数有三个_启动命令和参数和健康检查(self):
        data = self.deployment.data_list_args_livenessprobe
        delete_result = self.deployment.delete_deplyment(ns_name=data['namespace'],
                                                         deployment_name=data['deployment_name'],
                                                         region_name=self.region_name)
        assert delete_result.status_code == 200, "删除部署{}失败:{}".format(data['deployment_name'], delete_result.text)

    def 测试_删除Deployment_必填参数_可选参数有一个配置引用_configmap(self):
        data = self.deployment.data_list_configmap
        delete_result = self.deployment.delete_deplyment(ns_name=data['namespace'],
                                                         deployment_name=data['deployment_name'],
                                                         region_name=self.region_name)
        assert delete_result.status_code == 200, "删除部署{}失败:{}".format(data['deployment_name'], delete_result.text)

    @pytest.mark.skip(reason="日志case有问题,施源后续改")
    def 测试_删除Deployment_必填参数_可选参数有三个_环境变量和日志文件和排除日志文件(self):
        data = self.deployment.data_list_file_log_exclude_log
        delete_result = self.deployment.delete_deplyment(ns_name=data['namespace'],
                                                         deployment_name=data['deployment_name'],
                                                         region_name=self.region_name)
        assert delete_result.status_code == 200, "删除部署{}失败:{}".format(data['deployment_name'], delete_result.text)

    def 测试_删除Deployment_必填参数_可选参数有两个_网络模式和配置引用_secret(self):
        data = self.deployment.data_list_hostnetwork_secret
        delete_result = self.deployment.delete_deplyment(ns_name=data['namespace'],
                                                         deployment_name=data['deployment_name'],
                                                         region_name=self.region_name)
        assert delete_result.status_code == 200, "删除部署{}失败:{}".format(data['deployment_name'], delete_result.text)

    def 测试_删除Deployment_必填参数_可选参数亲和性(self):
        data_affinify_base = self.deployment.data_list_affinify_base
        delete_result = self.deployment.delete_deplyment(ns_name=data_affinify_base['namespace'],
                                                         deployment_name=data_affinify_base['deployment_name'],
                                                         region_name=self.region_name)
        assert delete_result.status_code == 200, "删除部署{}失败:{}".format(data_affinify_base['deployment_name'],
                                                                      delete_result.text)

        data_affinify_app_name = self.deployment.data_list_affinify_app_name
        delete_result = self.deployment.delete_deplyment(ns_name=data_affinify_app_name['namespace'],
                                                         deployment_name=data_affinify_app_name['deployment_name'],
                                                         region_name=self.region_name)
        assert delete_result.status_code == 200, "删除部署{}失败:{}".format(data_affinify_app_name['deployment_name'],
                                                                      delete_result.text)

        data_antiaffinify_app_name = self.deployment.data_list_antiaffinify_app_name
        delete_result = self.deployment.delete_deplyment(ns_name=data_antiaffinify_app_name['namespace'],
                                                         deployment_name=data_antiaffinify_app_name['deployment_name'],
                                                         region_name=self.region_name)
        assert delete_result.status_code == 200, "删除部署{}失败:{}".format(data_antiaffinify_app_name['deployment_name'],
                                                                      delete_result.text)

    def 测试_删除Deployment_必填参数_可选端口(self):
        data = self.deployment.data_list_port
        delete_result = self.deployment.delete_deplyment(ns_name=data['namespace'],
                                                         deployment_name=data['deployment_name'],
                                                         region_name=self.region_name)
        assert delete_result.status_code == 200, "删除部署{}失败:{}".format(data['deployment_name'], delete_result.text)

    def 测试_删除Deployment_必填参数_可选主机选择器(self):
        data = self.deployment.data_list_nodeselector
        delete_result = self.deployment.delete_deplyment(ns_name=data['namespace'],
                                                         deployment_name=data['deployment_name'],
                                                         region_name=self.region_name)
        assert delete_result.status_code == 200, "删除部署{}失败:{}".format(data['deployment_name'], delete_result.text)
