import pytest
from common.settings import RERUN_TIMES, OVN_REGION
from new_case.containerplatform.ovn.conftest import L1_data, l1_create_casename
from new_case.containerplatform.ovn.ovn import Ovn


@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
@pytest.mark.skipif(not OVN_REGION, reason="没有传ovn集群名称，默认不跑对应用例")
class TestDeleteOvn(object):

    def setup_class(self):
        self.ovn_client = Ovn()

    @pytest.mark.delete
    @pytest.mark.parametrize("data", L1_data, ids=l1_create_casename)
    def 测试ovn集群删除子网_L1(self, data):
        ret = self.ovn_client.delete_subnet(data['subnet_name'])
        assert ret.status_code == 200, "ovn集群删除子网失败 {}".format(ret.text)
        # values = self.ovn_client.generate_jinja_data('verify_data/ovn/delete_response.jinja2', data)
        # assert self.ovn_client.is_sub_dict(values, ret.json()), \
        #     "ovn集群删除子网 比对数据失败，返回数据:{},期望数据:{}".format(ret.json(), values)
        delete_flag = self.ovn_client.check_exists(self.ovn_client.common_subnet_url(data['subnet_name']), 404)
        assert delete_flag, "ovn集群删除子网后还存在"
