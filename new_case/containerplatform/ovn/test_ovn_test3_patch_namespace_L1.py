import pytest
from common.settings import RERUN_TIMES, OVN_REGION
from new_case.containerplatform.ovn.conftest import l1_update_namespace, L1_data, l1_update_namespace_casename
from new_case.containerplatform.ovn.ovn import Ovn


@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
@pytest.mark.skipif(not OVN_REGION, reason="没有传ovn集群名称，默认不跑对应用例")
class TestPatchOvn(object):

    def setup_class(self):
        self.ovn_client = Ovn()

    @pytest.mark.upgrade
    @pytest.mark.parametrize("data", l1_update_namespace, ids=l1_update_namespace_casename)
    def 测试ovn集群子网更新命名空间_L1(self, data):
        index = l1_update_namespace.index(data)
        ret = self.ovn_client.patch_subnet(L1_data[index]['subnet_name'], 'test_data/ovn/patch_ovn_namespace.jinja2',
                                           data)
        assert ret.status_code == 200, "ovn集群更新命名空间失败 {}".format(ret.text)
        L1_data[index].update(data)
        values = self.ovn_client.generate_jinja_data('verify_data/ovn/create_response.jinja2', L1_data[index])
        assert self.ovn_client.is_sub_dict(values, ret.json()), \
            "ovn集群更新命名空间 比对数据失败，返回数据:{},期望数据:{}".format(ret.json(), values)
