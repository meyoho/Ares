import pytest
from common import settings
from common.settings import RERUN_TIMES, CASE_TYPE
from new_case.containerplatform.application.application import Application
from new_case.containerplatform.ovn.conftest import L0_data, l0_update_gateway, l0_update_namespace, \
    l0_update_whitelist, ovn_app1, ovn_app2
from new_case.containerplatform.ovn.ovn import Ovn


@pytest.mark.BAT
@pytest.mark.skipif(not settings.OVN_REGION, reason="没有传ovn集群名称，默认不跑对应用例")
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestOvn(object):

    def setup_class(self):
        self.ovn_client = Ovn()
        self.subnet_name = L0_data['subnet_name']
        self.app_client = Application()
        list_result = self.ovn_client.list_ips(self.subnet_name)
        for ips in list_result.json()['items']:
            ip = self.ovn_client.get_value(ips, 'metadata.name')
            self.ovn_client.delete_ip(ip)
            self.app_client.check_exists(
                "kubernetes/{}/apis/kubeovn.io/v1/ips/{}".format(settings.OVN_REGION, ip), 404,
                expect_cnt=30)

    def teardown_class(self):
        if CASE_TYPE not in ("prepare", "upgrade"):
            self.app_client.delete_app(ovn_app1['namespace'], ovn_app1['app_name'], settings.OVN_REGION)
            self.app_client.check_exists(
                self.app_client.common_app_url(ovn_app1['namespace'], ovn_app1['app_name'], settings.OVN_REGION), 404)
            self.app_client.delete_app(ovn_app2['namespace'], ovn_app2['app_name'], settings.OVN_REGION)
            self.app_client.check_exists(
                self.app_client.common_app_url(ovn_app2['namespace'], ovn_app2['app_name'], settings.OVN_REGION), 404)

    def 测试ovn集群获取子网configmap(self):
        ret = self.ovn_client.check_configmap(settings.OVN_REGION)
        assert ret.status_code == 200, "ovn集群获取子网configmap失败 {}".format(ret.text)
        values = self.ovn_client.generate_jinja_data('verify_data/ovn/configmap_response.jinja2',
                                                     {'region_name': settings.OVN_REGION})
        assert self.ovn_client.is_sub_dict(values, ret.json()), \
            "ovn集群获取子网configmap 比对数据失败，返回数据:{},期望数据:{}".format(ret.json(), values)

    def 测试非ovn集群获取子网configmap(self):
        ret = self.ovn_client.check_configmap(settings.REGION_NAME)
        assert ret.status_code == 200, "ovn集群获取子网configmap失败 {}".format(ret.text)
        values = self.ovn_client.generate_jinja_data('verify_data/ovn/configmap_response.jinja2',
                                                     {'region_name': settings.REGION_NAME})
        assert self.ovn_client.is_sub_dict(values, ret.json()), \
            "high集群获取子网configmap 比对数据失败，返回数据:{},期望数据:{}".format(ret.json(), values)

    @pytest.mark.prepare
    def 测试ovn集群创建子网_必填参数(self):
        self.ovn_client.delete_subnet(self.subnet_name)
        ret = self.ovn_client.create_subnet('test_data/ovn/create_ovn.jinja2', L0_data)
        assert ret.status_code == 201, "ovn集群创建子网失败 {}".format(ret.text)
        values = self.ovn_client.generate_jinja_data('verify_data/ovn/create_response.jinja2', L0_data)
        assert self.ovn_client.is_sub_dict(values, ret.json()), \
            "ovn集群创建子网 比对数据失败，返回数据:{},期望数据:{}".format(ret.json(), values)

    @pytest.mark.upgrade
    def 测试ovn集群子网列表(self):
        ret = self.ovn_client.list_subnet(limit=1000)
        assert ret.status_code == 200, "ovn集群子网列表失败 {}".format(ret.text)
        content = self.ovn_client.get_k8s_resource_data(ret.json(), self.subnet_name, list_key="items")
        values = self.ovn_client.generate_jinja_data('verify_data/ovn/create_response.jinja2', L0_data)
        assert self.ovn_client.is_sub_dict(values, content), \
            "ovn集群子网列表 比对数据失败，返回数据:{},期望数据:{}".format(content, values)

    @pytest.mark.upgrade
    def 测试ovn集群搜索子网(self):
        ret = self.ovn_client.search_subnet(self.subnet_name)
        assert ret.status_code == 200, "ovn集群搜索子网失败 {}".format(ret.text)
        content = self.ovn_client.get_k8s_resource_data(ret.json(), self.subnet_name, list_key="items")
        values = self.ovn_client.generate_jinja_data('verify_data/ovn/create_response.jinja2', L0_data)
        assert self.ovn_client.is_sub_dict(values, content), \
            "ovn集群搜索子网 比对数据失败，返回数据:{},期望数据:{}".format(content, values)

    @pytest.mark.upgrade
    def 测试ovn集群更新网关_必填参数(self):
        ret = self.ovn_client.patch_subnet(self.subnet_name, 'test_data/ovn/patch_ovn_gateway.jinja2',
                                           l0_update_gateway)
        assert ret.status_code == 200, "ovn集群更新网关失败 {}".format(ret.text)
        L0_data.update(l0_update_gateway)

        values = self.ovn_client.generate_jinja_data('verify_data/ovn/create_response.jinja2', L0_data)
        assert self.ovn_client.is_sub_dict(values, ret.json()), \
            "ovn集群更新网关 比对数据失败，返回数据:{},期望数据:{}".format(ret.json(), values)

    @pytest.mark.prepare
    def 测试ovn集群更新命名空间(self):
        ret = self.ovn_client.patch_subnet(self.subnet_name, 'test_data/ovn/patch_ovn_namespace.jinja2',
                                           l0_update_namespace)
        assert ret.status_code == 200, "ovn集群更新命名空间失败 {}".format(ret.text)
        L0_data.update(l0_update_namespace)

        values = self.ovn_client.generate_jinja_data('verify_data/ovn/create_response.jinja2', L0_data)
        assert self.ovn_client.is_sub_dict(values, ret.json()), \
            "ovn集群更新命名空间 比对数据失败，返回数据:{},期望数据:{}".format(ret.json(), values)

    @pytest.mark.prepare
    def 测试ovn集群创建应用(self):
        self.app_client.delete_app(ovn_app1['namespace'], ovn_app1['app_name'], settings.OVN_REGION)
        self.app_client.check_exists(
            self.app_client.common_app_url(ovn_app1['namespace'], ovn_app1['app_name'], settings.OVN_REGION), 404)

        ret = self.app_client.create_app('./test_data/application/create_app.jinja2', ovn_app1, ovn_app1['namespace'],
                                         settings.OVN_REGION)
        assert ret.status_code == 200, "创建应用使用分配子网失败:{}".format(ret.text)
        ret = self.app_client.get_app_status(ovn_app1['namespace'], ovn_app1['app_name'], 'Running',
                                             region_name=settings.OVN_REGION)
        assert ret, '创建应用使用分配子网后，应用状态不是运行中'
        ret = self.app_client.get_app_pod(ovn_app1['namespace'], ovn_app1['app_name'], settings.OVN_REGION)
        assert ret.status_code == 200, "获取容器组失败:{}".format(ret.text)

    @pytest.mark.prepare
    def 测试ovn集群创建应用固定IP(self):
        self.app_client.delete_app(ovn_app2['namespace'], ovn_app2['app_name'], settings.OVN_REGION)
        self.app_client.check_exists(
            self.app_client.common_app_url(ovn_app2['namespace'], ovn_app2['app_name'], settings.OVN_REGION), 404)
        ret = self.app_client.create_app('./test_data/application/create_app.jinja2', ovn_app2, ovn_app2['namespace'],
                                         region_name=settings.OVN_REGION)
        assert ret.status_code == 200, "创建应用使用固定IP失败:{}".format(ret.text)
        ret = self.app_client.get_app_status(ovn_app2['namespace'], ovn_app2['app_name'], 'Running',
                                             region_name=settings.OVN_REGION)
        assert ret, '创建应用使用固定IP后，应用状态不是运行中'
        ret = self.app_client.get_app_pod(ovn_app2['namespace'], ovn_app2['app_name'], settings.OVN_REGION)
        assert ret.status_code == 200, "获取容器组失败:{}".format(ret.text)
        pod_real_ip = self.app_client.get_value(ret.json(), 'items.0.status.podIP')
        assert pod_real_ip in ovn_app2['ovn_ip_pool'], '应用固定 IP{} 不是指定的IP{}'.format(pod_real_ip,
                                                                                    ovn_app2['ovn_ip_pool'])

    @pytest.mark.upgrade
    def 测试ovn集群子网已用IP(self):
        list_result = self.ovn_client.list_ips(self.subnet_name)
        assert list_result.status_code == 200, "ovn集群子网已用IP列表失败:{}".format(list_result.text)
        assert len(list_result.json()['items']) > 1, "ovn集群子网已用IP列表应该至少有两个 {}".format(list_result.text)

        ret = self.app_client.get_app_pod(ovn_app1['namespace'], ovn_app1['app_name'], settings.OVN_REGION)
        assert ret.status_code == 200, "获取容器组失败:{}".format(ret.text)
        pod_ip = self.app_client.get_value(ret.json(), 'items.0.status.podIP')
        pod_name = self.app_client.get_value(ret.json(), 'items.0.metadata.name')

        content = self.ovn_client.get_k8s_resource_data(list_result.json(), pod_ip, list_key="items")
        data = {
            'pod_ip': pod_ip,
            'subnet_name': self.subnet_name,
            'namespace': ovn_app1['namespace'],
            'pod_name': pod_name,
        }
        values = self.ovn_client.generate_jinja_data('verify_data/ovn/ip_response.jinja2', data)
        assert self.ovn_client.is_sub_dict(values, content), \
            "ovn集群子网列表 比对数据失败，返回数据:{},期望数据:{}".format(content, values)

    @pytest.mark.upgrade
    def 测试搜索IP(self):
        ip = ovn_app2['ovn_ip_pool'][0]
        search_result = self.ovn_client.search_ip(self.subnet_name, ip)
        assert search_result.status_code == 200, "搜索 IP失败:{}".format(search_result.text)
        assert ip in search_result.text, "{}不在已用 IP 列表中".format(ip)

    def 测试搜索不存在的IP(self):
        search_result = self.ovn_client.search_ip(self.subnet_name, "notexist")
        assert search_result.status_code == 200, "搜索不存在的IP失败:{}".format(search_result.text)
        content = self.ovn_client.get_value(search_result.json(), 'items')
        assert content == [], "搜索不存在的IP比对数据失败，返回数据{}，期望数据{}".format(content, [])

    @pytest.mark.upgrade
    def 测试ovn集群子网详情(self):
        ret = self.ovn_client.detail_subnet(self.subnet_name)
        assert ret.status_code == 200, "ovn集群子网详情失败 {}".format(ret.text)
        L0_data.update({'used': 2})
        values = self.ovn_client.generate_jinja_data('verify_data/ovn/detail_response.jinja2', L0_data)
        assert self.ovn_client.is_sub_dict(values, ret.json()), \
            "ovn集群子网详情 比对数据失败，返回数据:{},期望数据:{}".format(ret.json(), values)

    @pytest.mark.upgrade
    def 测试ovn集群更新白名单(self):
        ret = self.ovn_client.patch_subnet(self.subnet_name, 'test_data/ovn/patch_ovn_whitelist.jinja2',
                                           l0_update_whitelist)
        assert ret.status_code == 200, "ovn集群更新白名单失败 {}".format(ret.text)
        L0_data.update(l0_update_whitelist)
        values = self.ovn_client.generate_jinja_data('verify_data/ovn/detail_response.jinja2', L0_data)
        assert self.ovn_client.is_sub_dict(values, ret.json()), \
            "ovn集群更新白名单 比对数据失败，返回数据:{},期望数据:{}".format(ret.json(), values)

    @pytest.mark.delete
    def 测试ovn集群删除子网_必填参数(self):
        # 删除子网前先释放IP
        list_result = self.ovn_client.list_ips(self.subnet_name)
        for ips in list_result.json()['items']:
            ip = self.ovn_client.get_value(ips, 'metadata.name')
            self.ovn_client.delete_ip(ip)
        ret = self.ovn_client.delete_subnet(self.subnet_name)
        assert ret.status_code == 200, "ovn集群删除子网失败 {}".format(ret.text)
        # values = self.ovn_client.generate_jinja_data('verify_data/ovn/delete_response.jinja2', L0_data)
        # assert self.ovn_client.is_sub_dict(values, ret.json()), \
        #     "ovn集群删除子网 比对数据失败，返回数据:{},期望数据:{}".format(ret.json(), values)
