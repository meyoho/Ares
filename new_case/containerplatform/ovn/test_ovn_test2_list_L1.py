import pytest

from common.settings import RERUN_TIMES, OVN_REGION
from new_case.containerplatform.ovn.conftest import L1_data
from new_case.containerplatform.ovn.ovn import Ovn


@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
@pytest.mark.skipif(not OVN_REGION, reason="没有传ovn集群名称，默认不跑对应用例")
class TestListOvn(object):

    def setup_class(self):
        self.ovn_client = Ovn()

    @pytest.mark.upgrade
    def 测试_子网列表_L1_按名称搜索(self):
        data = L1_data[0]
        subnet_name = data['subnet_name']
        names = [subnet_name, subnet_name.upper(), subnet_name.capitalize(), subnet_name[:-1]]
        for name in names:
            ret = self.ovn_client.search_subnet(name)
            assert ret.status_code == 200, "ovn集群按名称搜索子网失败:{}".format(ret.text)
            values = self.ovn_client.generate_jinja_data("verify_data/ovn/create_response.jinja2", data)
            content = self.ovn_client.get_k8s_resource_data(ret.json(), data['subnet_name'], 'items')
            assert self.ovn_client.is_sub_dict(values, content), \
                "ovn集群按名称搜索子网比对数据失败，返回数据{}，期望数据{}".format(content, values)

    def 测试_子网列表_L1_按名称搜索_不存在(self):
        name = L1_data[0]['subnet_name'] + 'notexist'
        ret = self.ovn_client.search_subnet(name)
        assert ret.status_code == 200, "ovn集群按名称搜索子网失败:{}".format(ret.text)
        content = self.ovn_client.get_value(ret.json(), 'items')
        assert content == [], "ovn集群按名称搜索子网比对数据失败，返回数据{}，期望数据{}".format(content, [])

    def 测试获取子网列表L1_有limit和continue参数(self):
        ret = self.ovn_client.list_subnet(limit=1)
        assert ret.status_code == 200, "获取列表失败:{}".format(ret.text)
        continues = self.ovn_client.get_value(ret.json(), "metadata.continue")
        ret_cnt = self.ovn_client.list_subnet(limit=1, continues=continues)
        assert ret_cnt.status_code == 200, "获取列表失败:{}".format(ret_cnt.text)
        assert ret.json() != ret_cnt.json(), "分页数据相同，第一页数据:{},第二页数据:{}".format(ret.json(), ret_cnt.json())
