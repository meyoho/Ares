import pytest

from common.settings import RERUN_TIMES, OVN_REGION
from new_case.containerplatform.ovn.ovn import Ovn


@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
@pytest.mark.skipif(not OVN_REGION, reason="没有传ovn集群名称，默认不跑对应用例")
class TestDetailOvn(object):

    def setup_class(self):
        self.ovn_client = Ovn()

    @pytest.mark.upgrade
    def 测试ovn集群子网详情_L1_不存在(self):
        ret = self.ovn_client.detail_subnet('subnet_name')
        assert ret.status_code == 404, "ovn集群子网详情失败 {}".format(ret.text)
