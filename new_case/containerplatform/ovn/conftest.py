import pytest

from common.log import logger
from common.settings import RESOURCE_PREFIX, OVN_REGION, IMAGE
from new_case.containerplatform.namespace.namespace import Namespace
from new_case.platform.project.project import Project

project_name = "e2eproject-ovn"
ns_data = {
    "namespace_name": "{}-forovn".format(RESOURCE_PREFIX).replace('_', '-'),
    "display_name": "{}-forovn".format(RESOURCE_PREFIX).replace('_', '-'),
    "cluster": "{}".format(OVN_REGION),
    "project": project_name,
}
ns_data2 = {
    "namespace_name": "{}-forovn2".format(RESOURCE_PREFIX).replace('_', '-'),
    "display_name": "{}-forovn2".format(RESOURCE_PREFIX).replace('_', '-'),
    "cluster": "{}".format(OVN_REGION),
    "project": project_name,
}
ns_data3 = {
    "namespace_name": "{}-forovn3".format(RESOURCE_PREFIX).replace('_', '-'),
    "display_name": "{}-forovn3".format(RESOURCE_PREFIX).replace('_', '-'),
    "cluster": "{}".format(OVN_REGION),
    "project": project_name,
}
ovn_app1 = {
    'project_name': "e2e-test-ovn",
    'namespace': ns_data['namespace_name'],
    'app_name': "{}-ares-acp-forovn".format(RESOURCE_PREFIX),
    'deployment_name': "{}-ares-acp-forovn-subnet".format(RESOURCE_PREFIX),
    'image': IMAGE,
}
ovn_app2 = {
    'project_name': "e2e-test-ovn",
    'namespace': ns_data['namespace_name'],
    'app_name': "{}-ares-acp-forovn2-ip".format(RESOURCE_PREFIX),
    'deployment_name': "{}-ares-acp-forovn2-ip".format(RESOURCE_PREFIX),
    'image': IMAGE,
    'ovn_ip_pool': ["88.88.88.88"]
}
L0_data = {
    'subnet_name': "{}-ares-acp-subnet".format(RESOURCE_PREFIX),
    'cidr': '88.88.0.0/16'
}
namespace = Namespace()
try:
    nodes = namespace.send('get', "kubernetes/{}/api/v1/nodes".format(OVN_REGION))
    assert nodes.status_code == 200, "ovn集群获取node失败 {}".format(nodes.text)
    node_name = namespace.get_value(nodes.json(), "items.0.metadata.name")
except Exception as e:
    logger.error("获取node_name地址出错:{},返回信息:{}".format(e, nodes.text))
    node_name = ""

l0_update_gateway = {
    'gateway_type': "centralized",
    'gateway_node': node_name,
    'nat': False
}
l0_update_namespace = {
    'namespaces': [ns_data['namespace_name']]
}

l0_update_whitelist = {
    'private': True,
    'allow_subnets': ['100.64.0.0/16']
}
l1_create_casename = ["多个保留IP", "集中式网关类型", "开启子网隔离", "不允许外出流量NAT"]
L1_data = [
    {
        'subnet_name': "{}-ares-acp-preserve".format(RESOURCE_PREFIX),
        'cidr': '99.99.90.0/24',
        'exclude_ips': ['99.99.90.2', '99.99.90.3..99.99.90.5']
    }, {
        'subnet_name': "{}-ares-acp-centralized".format(RESOURCE_PREFIX),
        'cidr': '99.99.91.0/24',
        'gateway_type': "centralized",
        'gateway_node': node_name,
    }, {
        'subnet_name': "{}-ares-acp-isolation".format(RESOURCE_PREFIX),
        'cidr': '99.99.92.0/24',
        'private': True,
    }, {
        'subnet_name': "{}-ares-acp-nat".format(RESOURCE_PREFIX),
        'cidr': '99.99.93.0/24',
        'nat': False,
    }
]
l1_update_casename = ["多个保留IP更新为集中式网络类型不允许外出流量NAT",
                      "集中式网关类型更新为分布式网络类型不允许外出流量NAT",
                      "开启子网隔离更新为分布式网络类型不允许外出流量NAT",
                      "不允许外出流量NAT更新为允许外出流量NAT集中式网络"]

l1_update_gateway = [
    {
        'gateway_type': "centralized",
        'gateway_node': node_name,
        'nat': False
    },
    {
        'gateway_type': "distributed",
        'gateway_node': '',
        'nat': False
    },
    {
        'gateway_type': "distributed",
        'gateway_node': node_name,
        'nat': False
    },
    {
        'gateway_type': "centralized",
        'gateway_node': node_name,
        'nat': True
    },
]

l1_update_namespace = [
    {'namespaces': [ns_data3['namespace_name']]},
    {'namespaces': []},
    {'namespaces': ["notfound", ns_data2['namespace_name']]},
]
l1_update_namespace_casename = ["添加指定命名空间", "更新为无命名空间", "更新为一个存在的命名空间和不存在命名空间"]
l1_update_whitelist = [
    {
        'private': False,
        'allow_subnets': ['100.64.0.0/16']
    },
    {
        'private': True,
        'allow_subnets': ['100.64.0.0/16']
    },
    {
        'private': True,
        'allow_subnets': []
    },
]
l1_update_whitelist_casename = ["关闭白名单", "开启白名单指定白名单地址", "开启白名单不指定白名单地址"]


@pytest.fixture(scope="session", autouse=True)
def Prepare_template():
    project = Project()
    data = {
        "project_name": project_name,
        "regions": [OVN_REGION],
        "display_name": project_name,
        "description": "e2e test project",
    }
    create_project_result = project.create_project("./test_data/project/create_project.jinja2", data)

    assert create_project_result.status_code in (201, 409), "创建项目失败"
    namespace = Namespace()
    namespace.create_namespace('./test_data/namespace/create_namespace.jinja2', ns_data, OVN_REGION)
    namespace.create_namespace('./test_data/namespace/create_namespace.jinja2', ns_data2, OVN_REGION)
    namespace.create_namespace('./test_data/namespace/create_namespace.jinja2', ns_data3, OVN_REGION)

    yield
    # namespace.delete_namespace(ns_data['namespace_name'], OVN_REGION)
    # namespace.delete_namespace(ns_data2['namespace_name'], OVN_REGION)
    # namespace.delete_namespace(ns_data3['namespace_name'], OVN_REGION)
    # project.delete_project(project_name)
