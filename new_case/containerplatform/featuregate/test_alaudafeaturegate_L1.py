import pytest
from new_case.containerplatform.featuregate.featuregate import Featuregate
from common import settings


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestAlaudafeaturegateL1Suite(object):
    def setup_class(self):
        self.featuregate = Featuregate()

    def 测试_创建AlaudaFeatureGate资源_a功能开启_a功能依赖b功能和c功能_依赖类型为all_b功能开启_c功能开启_平台所有集群的a功能是启用状态(self):
        # 创建功能a
        data_a = self.featuregate.data_list_alaudafeature_a_all
        create_result = self.featuregate.create_resource_jinja2(settings.GLOBAL_REGION_NAME,
                                                                "./test_data/featuregate/create_alaudafeature.jinja2",
                                                                data=data_a)
        assert create_result.status_code == 204, "创建AlaudaFeatureGate资源{}功能失败{}".\
            format(data_a['featurename'], create_result.text)
        # 创建功能b
        data_b = self.featuregate.data_list_alaudafeature_b
        create_result = self.featuregate.create_resource_jinja2(settings.GLOBAL_REGION_NAME,
                                                                "./test_data/featuregate/create_alaudafeature.jinja2",
                                                                data=data_b)
        assert create_result.status_code == 204, "创建AlaudaFeatureGate资源{}功能失败{}".format(data_b['featurename'],
                                                                                        create_result.text)
        # 创建功能c
        data_c = self.featuregate.data_list_alaudafeature_c
        create_result = self.featuregate.create_resource_jinja2(settings.GLOBAL_REGION_NAME,
                                                                "./test_data/featuregate/create_alaudafeature.jinja2",
                                                                data=data_c)
        assert create_result.status_code == 204, "创建AlaudaFeatureGate资源{}功能失败{}".format(data_c['featurename'],
                                                                                        create_result.text)

        # 查询全局功能开关功能a
        getfeaturegate_result = self.featuregate.get_featuregate(name=data_a['featurename'])
        assert getfeaturegate_result.status_code == 200, "全局功能开关{}功能获取失败:{}".format(data_a['featurename'],
                                                                                    getfeaturegate_result.text)
        value = self.featuregate.generate_jinja_data("./verify_data/featuregate/getfeaturegate.jinja2", data_a)
        assert self.featuregate.is_sub_dict(value, getfeaturegate_result.json()), \
            "查询全局功能开关单个功能比对数据失败，返回数据:{},期望数据:{}".format(getfeaturegate_result.json(), value)

    def 测试_创建AlaudaFeatureGate资源_a功能开启_a功能依赖b功能和c功能_依赖类型为all_b功能关闭_c功能开启_平台所有集群的a功能是关闭状态(self):
        # 更新b功能为关闭
        data_b = self.featuregate.data_list_alaudafeature_b
        ret = self.featuregate.detail_resource(region=settings.GLOBAL_REGION_NAME,
                                               namespace=data_b['namespace'], kind='alaudafeaturegates',
                                               name=data_b['featurename'])
        assert ret.status_code == 200, "获取b功能详情失败:{}".format(ret.text)
        resourceVersion = ret.json()['metadata']['resourceVersion']
        data_b.update({"resourceVersion": resourceVersion, "enabled": "false"})
        update_ret = self.featuregate.update_resource(region=settings.GLOBAL_REGION_NAME, namespace=data_b["namespace"],
                                                      kind="alaudafeaturegates", name=data_b["featurename"],
                                                      file="./test_data/featuregate/update_alaudafeature.jinja2",
                                                      data=data_b)
        assert update_ret.status_code == 200, "更新b功能失败:{}".format(ret.text)

        # 查询全局功能开关功能a是关闭状态
        data_a = self.featuregate.data_list_alaudafeature_a_all
        data_a.update({"status": "false"})
        getfeaturegate_result = self.featuregate.get_featuregate(name=data_a['featurename'])
        assert getfeaturegate_result.status_code == 200, "全局功能开关{}功能获取失败:{}".format(data_a['featurename'],
                                                                                    getfeaturegate_result.text)
        value = self.featuregate.generate_jinja_data("./verify_data/featuregate/getfeaturegate.jinja2", data_a)
        assert self.featuregate.is_sub_dict(value, getfeaturegate_result.json()), \
            "查询全局功能开关单个功能比对数据失败，返回数据:{},期望数据:{}".format(getfeaturegate_result.json(), value)

    def 测试_创建AlaudaFeatureGate资源_a功能开启_a功能依赖b功能和c功能_依赖类型为any_b功能关闭_c功能开启_平台所有集群的a功能是启用状态(self):
        # 更新a功能依赖类型为any
        data_a = self.featuregate.data_list_alaudafeature_a_all
        ret = self.featuregate.detail_resource(region=settings.GLOBAL_REGION_NAME, namespace=data_a['namespace'],
                                               kind='alaudafeaturegates', name=data_a['featurename'])
        assert ret.status_code == 200, "获取a功能详情失败:{}".format(ret.text)
        resourceVersion = ret.json()['metadata']['resourceVersion']
        data_a.update({"resourceVersion": resourceVersion, "type": "any"})
        update_ret = self.featuregate.update_resource(region=settings.GLOBAL_REGION_NAME, namespace=data_a["namespace"],
                                                      kind="alaudafeaturegates", name=data_a["featurename"],
                                                      file="./test_data/featuregate/update_alaudafeature.jinja2",
                                                      data=data_a)
        assert update_ret.status_code == 200, "更新a功能失败:{}".format(ret.text)

        # 查询全局功能开关功能a是启用状态
        data_a.update({"status": "true"})
        getfeaturegate_result = self.featuregate.get_featuregate(name=data_a['featurename'])
        assert getfeaturegate_result.status_code == 200, "全局功能开关{}功能获取失败:{}".format(data_a['featurename'],
                                                                                    getfeaturegate_result.text)
        value = self.featuregate.generate_jinja_data("./verify_data/featuregate/getfeaturegate.jinja2", data_a)
        assert self.featuregate.is_sub_dict(value, getfeaturegate_result.json()), \
            "查询全局功能开关单个功能比对数据失败，返回数据:{},期望数据:{}".format(getfeaturegate_result.json(), value)

    def 测试_创建AlaudaFeatureGate资源_a功能开启_a功能依赖b功能和c功能_依赖类型为any_b功能关闭_c功能关闭_平台所有集群的a功能是关闭状态(self):
        # 更新c功能为关闭
        data_c = self.featuregate.data_list_alaudafeature_c
        ret = self.featuregate.detail_resource(region=settings.GLOBAL_REGION_NAME, namespace=data_c['namespace'],
                                               kind='alaudafeaturegates', name=data_c['featurename'])
        assert ret.status_code == 200, "获取c功能详情失败:{}".format(ret.text)
        resourceVersion = ret.json()['metadata']['resourceVersion']
        data_c.update({"resourceVersion": resourceVersion, "enabled": "false"})
        update_ret = self.featuregate.update_resource(region=settings.GLOBAL_REGION_NAME, namespace=data_c["namespace"],
                                                      kind="alaudafeaturegates", name=data_c["featurename"],
                                                      file="./test_data/featuregate/update_alaudafeature.jinja2",
                                                      data=data_c)
        assert update_ret.status_code == 200, "更新c功能失败:{}".format(ret.text)

        # 查询全局功能开关功能a是关闭状态
        data_a = self.featuregate.data_list_alaudafeature_a_all
        data_a.update({"status": "false"})
        getfeaturegate_result = self.featuregate.get_featuregate(name=data_a['featurename'])
        assert getfeaturegate_result.status_code == 200, "全局功能开关{}功能获取失败:{}".format(data_a['featurename'],
                                                                                    getfeaturegate_result.text)
        value = self.featuregate.generate_jinja_data("./verify_data/featuregate/getfeaturegate.jinja2", data_a)
        assert self.featuregate.is_sub_dict(value, getfeaturegate_result.json()), \
            "查询全局功能开关单个功能比对数据失败，返回数据:{},期望数据:{}".format(getfeaturegate_result.json(), value)

    def 测试_删除AlaudaFeatureGate资源a功能_b功能_c功能(self):
        # 删除b功能
        data_b = self.featuregate.data_list_alaudafeature_b
        delete_result_b = self.featuregate.delete_resource(region=settings.GLOBAL_REGION_NAME,
                                                           namespace=data_b['namespace'], kind='alaudafeaturegates',
                                                           name=data_b['featurename'])
        assert delete_result_b.status_code == 200, "删除AlaudaFeatureGate资源_b:{}".format(delete_result_b.text)
        ret_b = self.featuregate.delete_resource(region=settings.GLOBAL_REGION_NAME, namespace=data_b['namespace'],
                                                 kind='alaudafeaturegates', name=data_b['featurename'])
        assert ret_b.status_code == 404, "删除AlaudaFeatureGate资源_b失败:{}".format(ret_b.text)

        # 删除依赖b功能，查询a功能的状态
        data_a = self.featuregate.data_list_alaudafeature_a_all
        getfeaturegate_result = self.featuregate.get_featuregate(name=data_a['featurename'])
        assert getfeaturegate_result.status_code in (200, 500), "全局功能开关{}功能状态获取失败:{}".\
            format(data_a['featurename'], getfeaturegate_result.text)

        # 删除c功能
        data_c = self.featuregate.data_list_alaudafeature_c
        delete_result_c = self.featuregate.delete_resource(region=settings.GLOBAL_REGION_NAME,
                                                           namespace=data_c['namespace'], kind='alaudafeaturegates',
                                                           name=data_c['featurename'])
        assert delete_result_c.status_code == 200, "删除AlaudaFeatureGate资源_c:{}".format(delete_result_c.text)
        ret_c = self.featuregate.delete_resource(region=settings.GLOBAL_REGION_NAME, namespace=data_c['namespace'],
                                                 kind='alaudafeaturegates', name=data_c['featurename'])
        assert ret_c.status_code == 404, "删除AlaudaFeatureGate资源_c失败:{}".format(ret_c.text)

        # 删除a功能
        data_a = self.featuregate.data_list_alaudafeature_a_all
        delete_result_a = self.featuregate.delete_resource(region=settings.GLOBAL_REGION_NAME,
                                                           namespace=data_a['namespace'], kind='alaudafeaturegates',
                                                           name=data_a['featurename'])
        assert delete_result_a.status_code == 200, "删除AlaudaFeatureGate资源_a:{}".format(delete_result_a.text)
        ret_a = self.featuregate.delete_resource(region=settings.GLOBAL_REGION_NAME, namespace=data_a['namespace'],
                                                 kind='alaudafeaturegates', name=data_a['featurename'])
        assert ret_a.status_code == 404, "删除AlaudaFeatureGate资源_a失败:{}".format(ret_a.text)

        getfeaturegate_result = self.featuregate.get_featuregate(name=data_a['featurename'])
        assert getfeaturegate_result.status_code == 404, "全局功能开关{}功能获取失败:{}".format(data_a['featurename'],
                                                                                    getfeaturegate_result.text)

    def teardown_class(self):
        data_a = self.featuregate.data_list_alaudafeature_a_all
        self.featuregate.delete_resource(region=settings.GLOBAL_REGION_NAME, namespace=data_a['namespace'],
                                         kind='alaudafeaturegates', name=data_a['featurename'])
