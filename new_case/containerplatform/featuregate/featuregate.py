import sys
from common.base_request import Common
from common.log import logger
from common import settings
import json


class Featuregate(Common):
    def __init__(self):
        super(Featuregate, self).__init__()
        self.genetate_global_info()

    def create_resource_url(self, region):
        return 'acp/v1/resources/{}/resources'.format(region)

    def list_featuregate_url(self, region=None):
        if region:
            return 'fg/v1/{}/featuregates'.format(region)
        else:
            return 'fg/v1/featuregates'

    def get_featuregate_url(self, region=None, name=None):
        if region:
            return 'fg/v1/{}/featuregates/{}'.format(region, name)
        else:
            return 'fg/v1/featuregates/{}'.format(name)

    def common_resource_url(self, region, namespace=None, kind=None, name=None):
        if namespace:
            return 'kubernetes/{}/apis/alauda.io/v1/namespaces/{}/{}/{}'.format(region, namespace, kind, name)
        else:
            return 'kubernetes/{}/apis/alauda.io/v1/{}/{}'.format(region, kind, name)

    def create_resource_jinja2(self, region, file, data):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.create_resource_url(region)
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=url, json=data)

    def list_featuregate(self, region=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.list_featuregate_url(region)
        return self.send(method='get', path=url)

    def get_featuregate(self, region=None, name=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_featuregate_url(region, name)
        return self.send(method='get', path=url)

    def delete_resource(self, region, namespace=None, kind=None, name=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.common_resource_url(region, namespace, kind, name)
        return self.send(method='delete', path=url)

    def detail_resource(self, region, namespace=None, kind=None, name=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.common_resource_url(region, namespace, kind, name)
        return self.send(method='get', path=url)

    def update_resource(self, region, namespace=None, kind=None, name=None, file=None, data=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.common_resource_url(region, namespace, kind, name)
        data = json.dumps(self.generate_jinja_data(file, data))
        logger.info(data)
        return self.send(method='put', data=data, path=url)

    # 创建AlaudaFeatureGate资源_a_a功能不依赖任何功能_a功能开启
    data_list_alaudafeature_true = {
        "featurename": '{}-alaudafeature-a'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.DEFAULT_NS,
        "description": "全局开关，a功能不依赖任何功能_a功能开启",
        "enabled": "true",
        "stage": "Alpha",
        "status": "true"
    }

    # high集群创建ClusterAlaudaFeatureGate资源_a_a功能不依赖任何功能_a功能开启
    data_list_clusterfeature_true = {
       "clusterfeaturename": '{}-clusterfeature-a'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
       "description": "集群开关，a功能不依赖任何功能_a功能开启",
       "enabled": "true",
       "stage": "Alpha",
       "status": "true"
    }

    # 创建AlaudaFeatureGate资源_a功能_依赖类型为all
    data_list_alaudafeature_a_all = {
        "featurename": '{}-alaudafeature-a-all'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "namespace": settings.DEFAULT_NS,
        "description": "全局开关，a功能依赖类型为all_a功能开启",
        "dependency": " ",
        "feature_b": "b",
        "feature_c": "c",
        "type": "all",
        "enabled": "true",
        "stage": "Alpha",
        "status": "true"
    }

    # 创建AlaudaFeatureGate资源_b
    data_list_alaudafeature_b = {
        "featurename": "b",
        "namespace": settings.DEFAULT_NS,
        "description": "b",
        "enabled": "true",
        "stage": "Beta"
    }

    # 创建AlaudaFeatureGate资源_c
    data_list_alaudafeature_c = {
        "featurename": "c",
        "namespace": settings.DEFAULT_NS,
        "description": "全局开关，c功能",
        "enabled": "true",
        "stage": "EOF"
    }

    # 创建ClusterAlaudaFeatureGate资源_a功能_依赖类型为all
    data_list_clusteralaudafeature_a_all = {
        "clusterfeaturename": '{}-clusteralaudafeature-a-all'.format(settings.RESOURCE_PREFIX).replace('_', '-'),
        "description": "集群开关，a功能依赖类型为all_a功能开启",
        "dependency": " ",
        "feature_b": "b",
        "feature_c": "c",
        "type": "all",
        "enabled": "true",
        "stage": "Alpha",
        "status": "true"
    }

    # 创建ClusterAlaudaFeatureGate资源_b
    data_list_clusteralaudafeature_b = {
        "clusterfeaturename": "b",
        "description": "b",
        "enabled": "true",
        "stage": "Beta"
    }

    # 创建ClusterAlaudaFeatureGate资源_c
    data_list_clusteralaudafeature_c = {
        "clusterfeaturename": "c",
        "description": "集群开关，c功能",
        "enabled": "true",
        "stage": "EOF"
    }

    # 创建AlaudaFeatureGate资源_alpha_状态为true
    data_alaudafeature_alpha = {
        "featurename": "alpha",
        "namespace": settings.DEFAULT_NS,
        "description": "alpha",
        "enabled": "true",
        "stage": "Alpha"
    }

    # 创建AlaudaFeatureGate资源_a_状态为true
    data_alaudafeature_a = {
        "featurename": "a",
        "namespace": settings.DEFAULT_NS,
        "description": "a",
        "enabled": "true",
        "stage": "Alpha",
        "status": "true"
    }

    # 创建AlaudaFeatureGate资源_beta_状态为false
    data_alaudafeature_beta = {
        "featurename": "beta",
        "namespace": settings.DEFAULT_NS,
        "description": "beta",
        "enabled": "false",
        "stage": "Beta"
    }

    # 创建AlaudaFeatureGate资源_b_状态为true
    data_alaudafeature_b = {
        "featurename": "b",
        "namespace": settings.DEFAULT_NS,
        "description": "b",
        "enabled": "true",
        "stage": "Beta",
        "status": "false"
    }

    # 创建AlaudaFeatureGate资源_eof_状态为开启
    data_alaudafeature_eof = {
        "featurename": "eof",
        "namespace": settings.DEFAULT_NS,
        "description": "eof",
        "enabled": "true",
        "stage": "EOF"
    }

    # 创建AlaudaFeatureGate资源_c_状态为false
    data_alaudafeature_c = {
        "featurename": "c",
        "namespace": settings.DEFAULT_NS,
        "description": "c",
        "enabled": "false",
        "stage": "EOF",
        "status": "false"
    }
