import pytest
from new_case.containerplatform.featuregate.featuregate import Featuregate
from common import settings


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestAlaudafeaturegateStageL1Suite(object):
    def setup_class(self):
        self.featuregate = Featuregate()

    def 测试_创建AlaudaFeatureGate资源_a功能开启_成熟度Alpha开启_平台所有集群的a功能是启用状态(self):
        data_alpha = self.featuregate.data_alaudafeature_alpha
        data_a = self.featuregate.data_alaudafeature_a

        self.featuregate.delete_resource(region=settings.GLOBAL_REGION_NAME, namespace=data_alpha['namespace'],
                                         kind='alaudafeaturegates', name=data_alpha['featurename'])
        self.featuregate.check_exists(self.featuregate.common_resource_url(region=settings.GLOBAL_REGION_NAME,
                                                                           namespace=data_alpha['namespace'],
                                                                           kind='alaudafeaturegates',
                                                                           name=data_alpha['featurename']), 404)
        # 创建alpha

        self.featuregate.delete_resource(region=settings.GLOBAL_REGION_NAME,
                                         namespace=data_alpha['namespace'], kind='alaudafeaturegates',
                                         name=data_alpha['featurename'])

        create_result = self.featuregate.create_resource_jinja2(settings.GLOBAL_REGION_NAME,
                                                                "./test_data/featuregate/create_alaudafeature.jinja2",
                                                                data=data_alpha)
        assert create_result.status_code == 204, "创建AlaudaFeatureGate资源{}功能失败{}".format(data_alpha['featurename'],
                                                                                        create_result.text)

        # 创建a功能
        create_result = self.featuregate.create_resource_jinja2(settings.GLOBAL_REGION_NAME,
                                                                "./test_data/featuregate/create_alaudafeature.jinja2",
                                                                data=data_a)
        assert create_result.status_code == 204, "创建AlaudaFeatureGate资源{}功能失败{}". format(data_a['featurename'],
                                                                                         create_result.text)
        # 查询全局功能开关a功能的状态
        getfeaturegate_result = self.featuregate.get_featuregate(name=data_a['featurename'])
        assert getfeaturegate_result.status_code == 200, "全局功能开关{}功能获取失败:{}".format(data_a['featurename'],
                                                                                    getfeaturegate_result.text)
        value = self.featuregate.generate_jinja_data("./verify_data/featuregate/getfeaturegate.jinja2", data_a)
        assert self.featuregate.is_sub_dict(value, getfeaturegate_result.json()), \
            "查询全局功能开关单个功能比对数据失败，返回数据:{},期望数据:{}".format(getfeaturegate_result.json(), value)

        # 删除alpha
        delete_result_alpha = self.featuregate.delete_resource(region=settings.GLOBAL_REGION_NAME,
                                                               namespace=data_alpha['namespace'],
                                                               kind='alaudafeaturegates',
                                                               name=data_alpha['featurename'])
        assert delete_result_alpha.status_code == 200, "删除AlaudaFeatureGate资源_alpha:{}".format(delete_result_alpha.text)
        ret_alpha = self.featuregate.delete_resource(region=settings.GLOBAL_REGION_NAME,
                                                     namespace=data_alpha['namespace'], kind='alaudafeaturegates',
                                                     name=data_alpha['featurename'])
        assert ret_alpha.status_code == 404, "删除AlaudaFeatureGate资源_alpha失败:{}".format(ret_alpha.text)

        # 删除a
        delete_result_a = self.featuregate.delete_resource(region=settings.GLOBAL_REGION_NAME,
                                                           namespace=data_a['namespace'], kind='alaudafeaturegates',
                                                           name=data_a['featurename'])
        assert delete_result_a.status_code == 200, "删除AlaudaFeatureGate资源_a:{}". format(delete_result_a.text)
        ret_a = self.featuregate.delete_resource(region=settings.GLOBAL_REGION_NAME, namespace=data_a['namespace'],
                                                 kind='alaudafeaturegates', name=data_a['featurename'])
        assert ret_a.status_code == 404, "删除AlaudaFeatureGate资源_a失败:{}".format(ret_a.text)

    def 测试_创建AlaudaFeatureGate资源_b功能开启_成熟度Beta关闭_平台所有集群的a功能是关闭状态(self):
        data_beta = self.featuregate.data_alaudafeature_beta
        data_b = self.featuregate.data_alaudafeature_b

        self.featuregate.delete_resource(region=settings.GLOBAL_REGION_NAME, namespace=data_beta['namespace'],
                                         kind='alaudafeaturegates', name=data_beta['featurename'])
        self.featuregate.check_exists(self.featuregate.common_resource_url(region=settings.GLOBAL_REGION_NAME,
                                                                           namespace=data_beta['namespace'],
                                                                           kind='alaudafeaturegates',
                                                                           name=data_beta['featurename']), 404)
        # 创建beta
        self.featuregate.delete_resource(region=settings.GLOBAL_REGION_NAME, namespace=data_beta['namespace'],
                                         kind='alaudafeaturegates', name=data_beta['featurename'])

        create_result = self.featuregate.create_resource_jinja2(settings.GLOBAL_REGION_NAME,
                                                                "./test_data/featuregate/create_alaudafeature.jinja2",
                                                                data=data_beta)
        assert create_result.status_code == 204, "创建AlaudaFeatureGate资源{}功能失败{}".format(data_beta['featurename'],
                                                                                        create_result.text)
        # 创建b功能
        create_result = self.featuregate.create_resource_jinja2(settings.GLOBAL_REGION_NAME,
                                                                "./test_data/featuregate/create_alaudafeature.jinja2",
                                                                data=data_b)
        assert create_result.status_code == 204, "创建AlaudaFeatureGate资源{}功能失败{}".format(data_b['featurename'],
                                                                                        create_result.text)

        # 查询全局功能开关b功能的状态
        getfeaturegate_result = self.featuregate.get_featuregate(name=data_b['featurename'])
        assert getfeaturegate_result.status_code == 200, "全局功能开关{}功能获取失败:{}".format(data_b['featurename'],
                                                                                    getfeaturegate_result.text)
        value = self.featuregate.generate_jinja_data("./verify_data/featuregate/getfeaturegate.jinja2", data_b)
        assert self.featuregate.is_sub_dict(value, getfeaturegate_result.json()), \
            "查询全局功能开关单个功能比对数据失败，返回数据:{},期望数据:{}".format(getfeaturegate_result.json(), value)

        # 删除beta
        delete_result_beta = self.featuregate.delete_resource(region=settings.GLOBAL_REGION_NAME,
                                                              namespace=data_beta['namespace'],
                                                              kind='alaudafeaturegates', name=data_beta['featurename'])
        assert delete_result_beta.status_code == 200, "删除AlaudaFeatureGate资源_beta:{}".format(delete_result_beta.text)
        ret_beta = self.featuregate.delete_resource(region=settings.GLOBAL_REGION_NAME,
                                                    namespace=data_beta['namespace'],
                                                    kind='alaudafeaturegates', name=data_beta['featurename'])
        assert ret_beta.status_code == 404, "删除AlaudaFeatureGate资源_beta失败:{}".format(ret_beta.text)

        # 删除b
        delete_result_b = self.featuregate.delete_resource(region=settings.GLOBAL_REGION_NAME,
                                                           namespace=data_b['namespace'], kind='alaudafeaturegates',
                                                           name=data_b['featurename'])
        assert delete_result_b.status_code == 200, "删除AlaudaFeatureGate资源_a:{}".format(delete_result_b.text)
        ret_b = self.featuregate.delete_resource(region=settings.GLOBAL_REGION_NAME, namespace=data_b['namespace'],
                                                 kind='alaudafeaturegates', name=data_b['featurename'])
        assert ret_b.status_code == 404, "删除AlaudaFeatureGate资源_a失败:{}".format(ret_b.text)

    def 测试_创建AlaudaFeatureGate资源_c功能关闭_成熟度eof开启_平台所有集群的a功能是关闭状态(self):
        data_eof = self.featuregate.data_alaudafeature_eof
        data_c = self.featuregate.data_alaudafeature_c

        self.featuregate.delete_resource(region=settings.GLOBAL_REGION_NAME, namespace=data_eof['namespace'],
                                         kind='alaudafeaturegates', name=data_eof['featurename'])
        self.featuregate.check_exists(
            self.featuregate.common_resource_url(region=settings.GLOBAL_REGION_NAME, namespace=data_eof['namespace'],
                                                 kind='alaudafeaturegates', name=data_eof['featurename']), 404)
        # 创建eof
        create_result = self.featuregate.create_resource_jinja2(settings.GLOBAL_REGION_NAME,
                                                                "./test_data/featuregate/create_alaudafeature.jinja2",
                                                                data=data_eof)
        assert create_result.status_code == 204, "创建AlaudaFeatureGate资源{}功能失败{}".format(data_eof['featurename'],
                                                                                        create_result.text)
        # 创建c功能
        create_result = self.featuregate.create_resource_jinja2(settings.GLOBAL_REGION_NAME,
                                                                "./test_data/featuregate/create_alaudafeature.jinja2",
                                                                data=data_c)
        assert create_result.status_code == 204, "创建AlaudaFeatureGate资源{}功能失败{}".format(data_c['featurename'],
                                                                                        create_result.text)
        # 查询全局功能开关c功能的状态
        getfeaturegate_result = self.featuregate.get_featuregate(name=data_c['featurename'])
        assert getfeaturegate_result.status_code == 200, "全局功能开关{}功能获取失败:{}".format(data_c['featurename'],
                                                                                    getfeaturegate_result.text)
        value = self.featuregate.generate_jinja_data("./verify_data/featuregate/getfeaturegate.jinja2", data_c)
        assert self.featuregate.is_sub_dict(value, getfeaturegate_result.json()), \
            "查询全局功能开关单个功能比对数据失败，返回数据:{},期望数据:{}".format(getfeaturegate_result.json(), value)

        # 删除eof
        delete_result_eof = self.featuregate.delete_resource(region=settings.GLOBAL_REGION_NAME,
                                                             namespace=data_eof['namespace'], kind='alaudafeaturegates',
                                                             name=data_eof['featurename'])
        assert delete_result_eof.status_code == 200, "删除AlaudaFeatureGate资源_beta:{}".format(delete_result_eof.text)
        ret_eof = self.featuregate.delete_resource(region=settings.GLOBAL_REGION_NAME,
                                                   namespace=data_eof['namespace'], kind='alaudafeaturegate',
                                                   name=data_eof['featurename'])
        assert ret_eof.status_code == 404, "删除AlaudaFeatureGate资源_beta失败:{}".format(ret_eof.text)

        # 删除c
        delete_result_c = self.featuregate.delete_resource(region=settings.GLOBAL_REGION_NAME,
                                                           namespace=data_c['namespace'], kind='alaudafeaturegates',
                                                           name=data_c['featurename'])
        assert delete_result_c.status_code == 200, "删除AlaudaFeatureGate资源_a:{}".format(delete_result_c.text)
        ret_c = self.featuregate.delete_resource(region=settings.GLOBAL_REGION_NAME, namespace=data_c['namespace'],
                                                 kind='alaudafeaturegates', name=data_c['featurename'])
        assert ret_c.status_code == 404, "删除AlaudaFeatureGate资源_a失败:{}".format(ret_c.text)

    def teardown_class(self):
        # 创建alpha
        data_alpha = self.featuregate.data_alaudafeature_alpha
        self.featuregate.create_resource_jinja2(settings.GLOBAL_REGION_NAME,
                                                "./test_data/featuregate/create_alaudafeature.jinja2", data=data_alpha)

        # 创建beta
        data_beta = self.featuregate.data_alaudafeature_beta
        data_beta.update({"enabled": "true"})
        self.featuregate.create_resource_jinja2(settings.GLOBAL_REGION_NAME,
                                                "./test_data/featuregate/create_alaudafeature.jinja2", data=data_beta)
