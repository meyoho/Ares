import pytest
from new_case.containerplatform.featuregate.featuregate import Featuregate
from common import settings


@pytest.mark.archon
@pytest.mark.BAT
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestFeaturegateSuite(object):
    def setup_class(self):
        self.featuregate = Featuregate()

    @pytest.mark.prepare
    def 测试_创建AlaudaFeatureGate资源_a_a功能不依赖任何功能_a功能开启_平台所有集群的a功能是启用状态(self):
        data = self.featuregate.data_list_alaudafeature_true
        # 创建AlaudaFeatureGate资源
        create_result = self.featuregate.create_resource_jinja2(settings.GLOBAL_REGION_NAME,
                                                                "./test_data/featuregate/create_alaudafeature.jinja2", data=data)
        assert create_result.status_code == 204, "创建AlaudaFeatureGate资源{}功能失败{}".format(data['featurename'],
                                                                                        create_result.text)
        # 查询全局功能开关列表
        listfeaturegate_result = self.featuregate.list_featuregate()
        assert listfeaturegate_result.status_code == 200, "全局功能开关列表获取失败:{}".format(listfeaturegate_result.text)
        value = self.featuregate.generate_jinja_data("./verify_data/featuregate/listfeaturegate.jinja2", data)
        assert self.featuregate.is_sub_dict(value, listfeaturegate_result.json()), \
            "查询全局功能开关列表比对数据失败，返回数据:{},期望数据:{}".format(listfeaturegate_result.json(), value)
        # 查询全局功能开关单个功能
        getfeaturegate_result = self.featuregate.get_featuregate(name=data['featurename'])
        assert getfeaturegate_result.status_code == 200, "全局功能开关{}功能获取失败:{}".format(data['featurename'],
                                                                                    getfeaturegate_result.text)
        value = self.featuregate.generate_jinja_data("./verify_data/featuregate/getfeaturegate.jinja2", data)
        assert self.featuregate.is_sub_dict(value, getfeaturegate_result.json()), \
            "查询全局功能开关单个功能比对数据失败，返回数据:{},期望数据:{}".format(getfeaturegate_result.json(), value)

    @pytest.mark.upgrade
    def 测试_全局开关a功能开启_high集群创建ClusterAlaudaFeatureGate资源_a_a功能不依赖任何功能_a功能开启_high集群的a功能是启用状态(self):
        data = self.featuregate.data_list_clusterfeature_true
        # 创建ClusterAlaudaFeatureGate资源
        create_result = self.featuregate.create_resource_jinja2(settings.REGION_NAME,
                                                                "./test_data/featuregate/create_clusterfeature.jinja2",
                                                                data=data)
        assert create_result.status_code == 204, "创建ClusterAlaudaFeatureGate资源{}功能失败{}".format(data['clusterfeaturename'],
                                                                                               create_result.text)
        # 查询集群功能开关列表
        listclusterfeaturegate_result = self.featuregate.list_featuregate(settings.REGION_NAME)
        assert listclusterfeaturegate_result.status_code == 200, "集群功能开关列表获取失败:{}".format(listclusterfeaturegate_result.text)
        value = self.featuregate.generate_jinja_data("./verify_data/featuregate/listclusterfeaturegate.jinja2", data)
        assert self.featuregate.is_sub_dict(value, listclusterfeaturegate_result.json()), \
            "查询集群功能开关列表比对数据失败，返回数据:{},期望数据:{}".format(listclusterfeaturegate_result.json(), value)
        # 查询集群功能开关单个功能
        getclusterfeaturegate_result = self.featuregate.get_featuregate(settings.REGION_NAME, data['clusterfeaturename'])
        assert getclusterfeaturegate_result.status_code == 200, \
            "集群功能开关{}功能获取失败:{}".format(data['clusterfeaturename'], getclusterfeaturegate_result.text)
        value = self.featuregate.generate_jinja_data("./verify_data/featuregate/getclusterfeaturegate.jinja2", data)
        assert self.featuregate.is_sub_dict(value, getclusterfeaturegate_result.json()), \
            "查询集群功能开关单个功能比对数据失败，返回数据:{},期望数据:{}".format(getclusterfeaturegate_result.json(), value)

    @pytest.mark.delete
    def 测试_删除ClusterAlaudaFeatureGate资源_a(self):
        data = self.featuregate.data_list_clusterfeature_true
        delete_result = self.featuregate.delete_resource(region=settings.REGION_NAME, kind='clusteralaudafeaturegates',
                                                         name=data['clusterfeaturename'])
        assert delete_result.status_code == 200, "删除ClusterAlaudaFeatureGate资源_a:{}".format(delete_result.text)

        ret = self.featuregate.delete_resource(region=settings.REGION_NAME, kind='clusteralaudafeaturegates',
                                               name=data['clusterfeaturename'])
        assert ret.status_code == 404, "删除ClusterAlaudaFeatureGate资源_a失败:{}".format(ret.text)

        getclusterfeaturegate_result = self.featuregate.get_featuregate(settings.REGION_NAME, data['clusterfeaturename'])
        assert getclusterfeaturegate_result.status_code == 404, "集群功能开关{}功能获取失败:{}".format(data['clusterfeaturename'],
                                                                                           getclusterfeaturegate_result.text)

    @pytest.mark.delete
    def 测试_删除AlaudaFeatureGate资源_a(self):
        data = self.featuregate.data_list_alaudafeature_true
        delete_result = self.featuregate.delete_resource(region=settings.GLOBAL_REGION_NAME,
                                                         namespace=data['namespace'], kind='alaudafeaturegates', name=data['featurename'])
        assert delete_result.status_code == 200, "删除AlaudaFeatureGate资源_a:{}".format(delete_result.text)

        ret = self.featuregate.delete_resource(region=settings.GLOBAL_REGION_NAME, namespace=data['namespace'],
                                               kind='alaudafeaturegates', name=data['featurename'])
        assert ret.status_code == 404, "删除AlaudaFeatureGate资源_a失败:{}".format(ret.text)

        getfeaturegate_result = self.featuregate.get_featuregate(name=data['featurename'])
        assert getfeaturegate_result.status_code == 404, "全局功能开关{}功能获取失败:{}".format(data['featurename'],
                                                                                    getfeaturegate_result.text)
