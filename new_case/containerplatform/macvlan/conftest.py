import pytest
from common.settings import RESOURCE_PREFIX, MACVLAN_REGION, IMAGE
from new_case.containerplatform.namespace.namespace import Namespace
from new_case.platform.project.project import Project
from common.base_request import Common
from common.api_requests import AlaudaRequest

default_subnet_name = "default"
AlaudaRequest = AlaudaRequest()
if MACVLAN_REGION:
    default_subnet_info = AlaudaRequest.send('get', "kubernetes/{}/apis/kubeovn.io/v1/subnets/{}".format(MACVLAN_REGION,
                                                                                                         default_subnet_name))
    if default_subnet_info.status_code != 200:
        CIDR = GATEWAY = excludeIps = ""
    else:
        CIDR = Common.get_value(default_subnet_info.json(), "spec.cidrBlock")
        GATEWAY = Common.get_value(default_subnet_info.json(), "spec.gateway")
        excludeIps = Common.get_value(default_subnet_info.json(), "spec.excludeIps")
else:
    CIDR = GATEWAY = excludeIps = ""

project_name = "e2eproject-macvlan"
ns_data = {
    "namespace_name": "{}-formacvlan".format(RESOURCE_PREFIX).replace('_', '-'),
    "display_name": "{}-formacvlan".format(RESOURCE_PREFIX).replace('_', '-'),
    "cluster": "{}".format(MACVLAN_REGION),
    "project": project_name,
}

L0_data = {
    'subnet_name': "{}-ares-acp-subnet".format(RESOURCE_PREFIX),
    'cidr': CIDR,
    'gateway': GATEWAY
}

macvlan_app1 = {
    'project_name': "e2e-test-macvlan",
    'namespace': ns_data['namespace_name'],
    'app_name': "{}-ares-acp-formacvlan".format(RESOURCE_PREFIX),
    'deployment_name': "{}-ares-acp-formacvlan-subnet".format(RESOURCE_PREFIX),
    'image': IMAGE,
}
macvlan_app2 = {
    'project_name': "e2e-test-macvlan",
    'namespace': ns_data['namespace_name'],
    'app_name': "{}-ares-acp-formacvlan2-ip".format(RESOURCE_PREFIX),
    'deployment_name': "{}-ares-acp-formacvlan2-ip".format(RESOURCE_PREFIX),
    'image': IMAGE,
    'subnet_name': L0_data['subnet_name']
}

l0_update_gateway = {
    'gateway': '88.88.88.1'
}
l0_update_excludeIps = {
    'gateway': GATEWAY,
    'excludeIps': excludeIps
}
last_subnet_data = {
    'subnet_name': "{}-ares-acp-subnet".format(RESOURCE_PREFIX),
    'cidr': CIDR,
    'gateway': GATEWAY,
    'excludeIps': excludeIps
}


@pytest.fixture(scope="session", autouse=True)
def Prepare_template():
    project = Project()
    data = {
        "project_name": project_name,
        "regions": [MACVLAN_REGION],
        "display_name": project_name,
        "description": "e2e test project",
    }
    project.create_project("./test_data/project/create_project.jinja2", data)
    namespace = Namespace()
    namespace.create_namespace('./test_data/namespace/create_namespace.jinja2', ns_data, MACVLAN_REGION)

    yield
    # namespace.delete_namespace(ns_data['namespace_name'], MACVLAN_REGION)
    # project.delete_project(project_name)
