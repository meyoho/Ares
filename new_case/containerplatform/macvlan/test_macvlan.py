import pytest
from common import settings
from common.settings import CASE_TYPE
from new_case.containerplatform.application.application import Application
from new_case.containerplatform.macvlan.conftest import L0_data, l0_update_gateway, l0_update_excludeIps, \
    macvlan_app1, macvlan_app2, last_subnet_data
from new_case.containerplatform.macvlan.macvlan import Macvlan


@pytest.mark.skipif(settings.MACVLAN_REGION is None, reason="do not test macvlan")
@pytest.mark.MACVLAN
class TestMacvlan(object):
    def setup_class(self):
        self.macvlan_client = Macvlan()
        self.subnet_name = L0_data['subnet_name']
        self.app_client = Application()

    def teardown_class(self):
        if CASE_TYPE not in ("prepare", "upgrade"):
            self.app_client.delete_app(macvlan_app1['namespace'], macvlan_app1['app_name'], settings.MACVLAN_REGION)
            self.app_client.check_exists(
                    self.app_client.common_app_url(macvlan_app1['namespace'], macvlan_app1['app_name'],
                                                   settings.MACVLAN_REGION), 404)
            self.app_client.delete_app(macvlan_app2['namespace'], macvlan_app2['app_name'], settings.MACVLAN_REGION)
            self.app_client.check_exists(
                    self.app_client.common_app_url(macvlan_app2['namespace'], macvlan_app2['app_name'],
                                                   settings.MACVLAN_REGION), 404)

    def 测试macvlan集群获取子网configmap(self):
        ret = self.macvlan_client.check_configmap(settings.MACVLAN_REGION)
        assert ret.status_code == 200, "macvlan集群获取子网configmap失败 {}".format(ret.text)
        values = self.macvlan_client.generate_jinja_data('verify_data/macvlan/configmap_response.jinja2',
                                                         {'macvlan': True})
        assert self.macvlan_client.is_sub_dict(values, ret.json()), \
            "macvlan集群获取子网configmap 比对数据失败，返回数据:{},期望数据:{}".format(ret.json(), values)

    def 测试非macvlan集群获取子网configmap(self):
        ret = self.macvlan_client.check_configmap(settings.REGION_NAME)
        assert ret.status_code == 200, "非macvlan集群获取子网configmap失败 {}".format(ret.text)
        values = self.macvlan_client.generate_jinja_data('verify_data/macvlan/configmap_response.jinja2')
        assert self.macvlan_client.is_sub_dict(values, ret.json()), \
            "不支持子网功能的集群{}获取子网configmap 比对数据失败，返回数据:{},期望数据:{}".format(settings.MACVLAN_REGION, ret.json(), values)

    @pytest.mark.prepare
    def 测试macvlan集群创建子网_必填参数(self):
        self.macvlan_client.delete_subnet(self.subnet_name)
        ret = self.macvlan_client.create_subnet('test_data/macvlan/create_macvlan.jinja2', L0_data)
        assert ret.status_code == 201, "macvlan集群创建子网失败 {}".format(ret.text)
        values = self.macvlan_client.generate_jinja_data('verify_data/macvlan/create_response.jinja2', L0_data)
        assert self.macvlan_client.is_sub_dict(values, ret.json()), \
            "macvlan集群创建子网 比对数据失败，返回数据:{},期望数据:{}".format(ret.json(), values)

    @pytest.mark.upgrade
    def 测试macvlan集群子网列表(self):
        ret = self.macvlan_client.list_subnet(limit=1000)
        assert ret.status_code == 200, "macvlan集群子网列表失败 {}".format(ret.text)
        content = self.macvlan_client.get_k8s_resource_data(ret.json(), self.subnet_name, list_key="items")
        values = self.macvlan_client.generate_jinja_data('verify_data/macvlan/create_response.jinja2', L0_data)
        assert self.macvlan_client.is_sub_dict(values, content), \
            "macvlan集群子网列表 比对数据失败，返回数据:{},期望数据:{}".format(content, values)

    @pytest.mark.upgrade
    def 测试macvlan集群搜索子网(self):
        ret = self.macvlan_client.search_subnet(self.subnet_name)
        assert ret.status_code == 200, "macvlan集群搜索子网失败 {}".format(ret.text)
        content = self.macvlan_client.get_k8s_resource_data(ret.json(), self.subnet_name, list_key="items")
        values = self.macvlan_client.generate_jinja_data('verify_data/macvlan/create_response.jinja2', L0_data)
        assert self.macvlan_client.is_sub_dict(values, content), \
            "macvlan集群搜索子网 比对数据失败，返回数据:{},期望数据:{}".format(content, values)

    @pytest.mark.upgrade
    def 测试ovn集群更新网关_必填参数(self):
        ret = self.macvlan_client.patch_subnet(self.subnet_name, 'test_data/macvlan/patch_macvlan_gateway.jinja2',
                                               l0_update_gateway)
        assert ret.status_code == 200, "macvlan集群更新网关失败 {}".format(ret.text)
        L0_data.update(l0_update_gateway)

        values = self.macvlan_client.generate_jinja_data('verify_data/macvlan/create_response.jinja2', L0_data)
        assert self.macvlan_client.is_sub_dict(values, ret.json()), \
            "macvlan集群更新网关 比对数据失败，返回数据:{},期望数据:{}".format(ret.json(), values)

    @pytest.mark.prepare
    def 测试macvlan集群更新保留ip(self):
        ret = self.macvlan_client.patch_subnet(self.subnet_name, 'test_data/macvlan/patch_macvlan_gateway.jinja2',
                                               l0_update_excludeIps)
        assert ret.status_code == 200, "macvlan集群更新命名空间失败 {}".format(ret.text)
        L0_data.update(l0_update_excludeIps)

        values = self.macvlan_client.generate_jinja_data('verify_data/macvlan/create_response.jinja2', L0_data)
        assert self.macvlan_client.is_sub_dict(values, ret.json()), \
            "macvlan集群更新命名空间 比对数据失败，返回数据:{},期望数据:{}".format(ret.json(), values)

    @pytest.mark.metis
    @pytest.mark.prepare
    def 测试macvlan集群创建应用(self):
        self.app_client.delete_app(macvlan_app1['namespace'], macvlan_app1['app_name'], settings.MACVLAN_REGION)
        self.app_client.check_exists(
                self.app_client.common_app_url(macvlan_app1['namespace'], macvlan_app1['app_name'],
                                               settings.MACVLAN_REGION), 404)

        ret = self.app_client.create_app('./test_data/application/create_app.jinja2', macvlan_app1,
                                         macvlan_app1['namespace'],
                                         settings.MACVLAN_REGION)
        assert ret.status_code == 200, "创建应用使用分配子网失败:{}".format(ret.text)
        ret = self.app_client.get_app_status(macvlan_app1['namespace'], macvlan_app1['app_name'], 'Running',
                                             region_name=settings.MACVLAN_REGION)
        assert ret, '创建应用使用分配子网后，应用状态不是运行中'
        ret = self.app_client.get_app_pod(macvlan_app1['namespace'], macvlan_app1['app_name'], settings.MACVLAN_REGION)
        assert ret.status_code == 200, "获取容器组失败:{}".format(ret.text)

    @pytest.mark.metis
    @pytest.mark.prepare
    def 测试macvlan集群创建应用固定IP(self):
        self.app_client.delete_app(macvlan_app2['namespace'], macvlan_app2['app_name'], settings.MACVLAN_REGION)
        self.app_client.check_exists(
                self.app_client.common_app_url(macvlan_app2['namespace'], macvlan_app2['app_name'],
                                               settings.MACVLAN_REGION), 404)
        ret = self.app_client.get_app_pod(macvlan_app1['namespace'], macvlan_app1['app_name'], settings.MACVLAN_REGION)
        # 从已经running起来的pod中获取macvlan的一个已用ip,然后删除这个应用,然后把这个podip做为固定ip来新建应用
        macvlan_ip = self.app_client.get_value(ret.json(), 'items.0.status.podIP')
        macvlan_ip_pool = {"macvlan_ip_pool": [macvlan_ip]}
        macvlan_app2.update(macvlan_ip_pool)
        self.app_client.delete_app(macvlan_app1['namespace'], macvlan_app1['app_name'], settings.MACVLAN_REGION)
        self.app_client.check_exists(
                self.app_client.common_app_url(macvlan_app1['namespace'], macvlan_app1['app_name'],
                                               settings.MACVLAN_REGION), 404)
        self.app_client.check_exists(
                "kubernetes/{}/apis/kubeovn.io/v1/ips/{}".format(settings.MACVLAN_REGION, macvlan_ip), 404,
                expect_cnt=30)
        # 新建应用,使用固定ip
        ret = self.app_client.create_app('./test_data/application/create_app.jinja2', macvlan_app2,
                                         macvlan_app2['namespace'],
                                         region_name=settings.MACVLAN_REGION)
        assert ret.status_code == 200, "创建应用使用固定IP失败:{}".format(ret.text)
        ret = self.app_client.get_app_status(macvlan_app2['namespace'], macvlan_app2['app_name'], 'Running',
                                             region_name=settings.MACVLAN_REGION)
        assert ret, '创建应用使用固定IP后，应用状态不是运行中'
        ret = self.app_client.get_app_pod(macvlan_app2['namespace'], macvlan_app2['app_name'], settings.MACVLAN_REGION)
        assert ret.status_code == 200, "获取容器组失败:{}".format(ret.text)
        pod_real_ip = self.app_client.get_value(ret.json(), 'items.0.status.podIP')
        assert pod_real_ip in macvlan_app2['macvlan_ip_pool'], '应用固定 IP{} 不是指定的IP{}'.format(pod_real_ip,
                                                                                            macvlan_app2[
                                                                                                'macvlan_ip_pool'])

    @pytest.mark.upgrade
    def 测试搜索IP(self):
        ip = macvlan_app2['macvlan_ip_pool'][0]
        search_result = self.macvlan_client.search_ip(self.subnet_name, ip)
        assert search_result.status_code == 200, "搜索 IP失败:{}".format(search_result.text)
        assert ip in search_result.text, "{}不在已用 IP 列表中".format(ip)

    def 测试搜索不存在的IP(self):
        search_result = self.macvlan_client.search_ip(self.subnet_name, "notexist")
        assert search_result.status_code == 200, "搜索不存在的IP失败:{}".format(search_result.text)
        content = self.macvlan_client.get_value(search_result.json(), 'items')
        assert content == [], "搜索不存在的IP比对数据失败，返回数据{}，期望数据{}".format(content, [])

    @pytest.mark.upgrade
    def 测试macvlan集群子网详情(self):
        ret = self.macvlan_client.detail_subnet(self.subnet_name)
        assert ret.status_code == 200, "macvlan集群子网详情失败 {}".format(ret.text)
        last_subnet_data.update({'used': 1})
        values = self.macvlan_client.generate_jinja_data('verify_data/macvlan/detail_response.jinja2', last_subnet_data)
        assert self.macvlan_client.is_sub_dict(values, ret.json()), \
            "macvlan集群子网详情 比对数据失败，返回数据:{},期望数据:{}".format(ret.json(), values)

    @pytest.mark.delete
    def 测试macvlan集群删除子网_必填参数(self):
        # 删除子网前先释放IP
        ret = self.app_client.get_app_pod(macvlan_app2['namespace'], macvlan_app2['app_name'], settings.MACVLAN_REGION)
        macvlan_ip = self.app_client.get_value(ret.json(), 'items.0.status.podIP')
        self.app_client.delete_app(macvlan_app2['namespace'], macvlan_app2['app_name'], settings.MACVLAN_REGION)
        self.app_client.check_exists(
                self.app_client.common_app_url(macvlan_app2['namespace'], macvlan_app2['app_name'],
                                               settings.MACVLAN_REGION), 404)
        self.app_client.check_exists(
                "kubernetes/{}/apis/kubeovn.io/v1/ips/{}".format(settings.MACVLAN_REGION, macvlan_ip), 404,
                expect_cnt=30)
        list_result = self.macvlan_client.list_ips(self.subnet_name)
        for ips in list_result.json()['items']:
            ip = self.macvlan_client.get_value(ips, 'metadata.name')
            self.macvlan_client.delete_ip(ip)
        ret = self.macvlan_client.delete_subnet(self.subnet_name)
        assert ret.status_code == 200, "macvlan集群删除子网失败 {}".format(ret.text)
        values = self.macvlan_client.generate_jinja_data('verify_data/macvlan/delete_response.jinja2', L0_data)
        assert self.macvlan_client.is_sub_dict(values, ret.json()), \
            "macvlan集群删除子网 比对数据失败，返回数据:{},期望数据:{}".format(ret.json(), values)
