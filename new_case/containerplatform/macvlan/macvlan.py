from common import settings
from common.base_request import Common


class Macvlan(Common):
    # 查看confimap来判断是否支持子网功能
    def check_configmap(self, region_name=settings.MACVLAN_REGION):
        url = "kubernetes/{}/api/v1/namespaces/kube-public/configmaps?fieldSelector=metadata.name=cni-kube-ovn".format(
                region_name)
        return self.send('get', url)

    # 查看集群的子网列表
    def list_subnet(self, limit=20, continues='', region_name=settings.MACVLAN_REGION):
        url = "kubernetes/{}/apis/kubeovn.io/v1/subnets?limit={}".format(region_name, limit)
        if continues != '':
            url = '{}&continue={}'.format(url, continues)
        return self.send('get', url)

    # 对子网列表按名称进行过滤
    def search_subnet(self, name, region_name=settings.MACVLAN_REGION):
        url = "acp/v1/resources/search/kubernetes/{}/apis/kubeovn.io/v1/subnets?limit=20&keyword={}&field=metadata.name".format(
                region_name, name)
        return self.send('get', url)

    # 创建子网
    def create_subnet(self, file, data, region_name=settings.MACVLAN_REGION):
        url = "kubernetes/{}/apis/kubeovn.io/v1/subnets".format(region_name)
        data = self.generate_jinja_data(file, data)
        return self.send('post', url, json=data)

    # 返回子网通用的api地址
    def common_subnet_url(self, name, region_name=settings.MACVLAN_REGION):
        return "kubernetes/{}/apis/kubeovn.io/v1/subnets/{}".format(region_name, name)

    # 查看子网详情
    def detail_subnet(self, name, region_name=settings.MACVLAN_REGION):
        url = self.common_subnet_url(name, region_name)
        return self.send('get', url)

    # 更新子网
    def patch_subnet(self, name, file, data, region_name=settings.MACVLAN_REGION):
        url = self.common_subnet_url(name, region_name)
        data = self.generate_jinja_data(file, data)
        return self.send('patch', url, json=data, headers={"Content-Type": "application/merge-patch+json"})

    # 查看子网的已用ip列表
    def list_ips(self, name, limit=20, region_name=settings.MACVLAN_REGION):
        url = "kubernetes/{}/apis/kubeovn.io/v1/ips?limit={}&labelSelector=ovn.kubernetes.io/subnet={}&field=spec.ipAddress".format(
                region_name, limit, name)
        return self.send('get', url)

    # 删除子网
    def delete_subnet(self, name, region_name=settings.MACVLAN_REGION):
        url = self.common_subnet_url(name, region_name)
        return self.send('delete', url)

    # 删除子网ip
    def delete_ip(self, ip, region_name=settings.MACVLAN_REGION):
        url = "kubernetes/{}/apis/kubeovn.io/v1/ips/{}".format(region_name, ip)
        return self.send(method='delete', path=url)

    # 对已用ip列表按ip进行过滤
    def search_ip(self, subnet_name, ip, region_name=settings.MACVLAN_REGION):
        url = "acp/v1/resources/search/kubernetes/{}/apis/kubeovn.io/v1/ips?limit=20&labelSelector=ovn.kubernetes." \
              "io/subnet={}&keyword={}&field=spec.ipAddress".format(region_name, subnet_name, ip)
        return self.send('get', url)
