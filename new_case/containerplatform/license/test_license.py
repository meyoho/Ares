import pytest

from new_case.containerplatform.license.license import License


@pytest.mark.BAT
@pytest.mark.acp_containerplatform
@pytest.mark.skipif(License().generate_certificate() == 'error', reason="证书无法生成，默认不跑对应用例")
class TestLicenseSuite(object):
    def setup_class(self):
        self.license = License()
        self.data = {"days": 10, "products": ["ares-test"]}

    def 测试获取元数据(self):
        ret = self.license.get_metadata()
        assert ret.status_code == 200, "获取元数据失败:{}".format(ret.text)
        global cfc
        cfc = self.license.get_value(ret.json(), 'cfc')

    def 测试导入license(self):
        data = self.license.generate_certificate(cfc, 10, self.data['products'])
        import_ret = self.license.create(data)
        assert import_ret.status_code == 200, "导入license失败:{}".format(import_ret.text)
        global name
        name = self.license.get_value(import_ret.json(), 'metadata.name')
        self.data.update({"name": name})

    def 测试获取license列表(self):
        ret = self.license.list()
        assert ret.status_code == 200, "获取license列表失败:{}".format(ret.text)
        content = self.license.get_k8s_resource_data(ret.json(), name, list_key="items")
        values = self.license.generate_jinja_data('verify_data/license/license.jinja2', self.data)
        assert self.license.is_sub_dict(values, content), \
            "获取license列表比对数据失败，返回数据{}，期望数据{}".format(ret.text, values)

    def 测试验证证书(self):
        for product in self.data['products']:
            ret = self.license.verify(product)
            assert ret.status_code == 200, "验证证书失败:{}".format(ret.text)
            status = self.license.get_value(ret.json(), 'status.enabled')
            assert status, "验证证书比对数据失败，返回数据{}，期望数据{}".format(ret.text, True)

    def 测试删除证书(self):
        ret = self.license.delete(name)
        assert ret.status_code == 204, "删除证书失败:{}".format(ret.text)
