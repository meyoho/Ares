import time

import pytest

from new_case.containerplatform.license.license import License


@pytest.mark.acp_containerplatform
@pytest.mark.skipif(License().generate_certificate() == 'error', reason="证书无法生成，默认不跑对应用例")
class TestLicenseSuite(object):
    def setup_class(self):
        self.license = License()
        self.data = {"days": 10, "products": ["ares-test"]}
        ret = self.license.get_metadata()
        assert ret.status_code == 200, "获取元数据失败:{}".format(ret.text)
        global cfc
        cfc = self.license.get_value(ret.json(), 'cfc')
        self.namelist = []

    def teardown_class(self):
        for name in self.namelist:
            self.license.delete(name)

    def 测试导入license_多个产品(self):
        data = {"days": 1, "products": ["ares-test1", "ares-test2"], "type": "Internal"}
        global cert_data
        cert_data = self.license.generate_certificate(cfc, data['days'], data['products'])
        import_ret = self.license.create(cert_data)
        assert import_ret.status_code == 200, "导入license失败:{}".format(import_ret.text)
        name = self.license.get_value(import_ret.json(), 'metadata.name')
        # 验证证书时间
        start = time.mktime(
            time.strptime(self.license.get_value(import_ret.json(), 'spec.validity.notBefore'), '%Y-%m-%dT%H:%M:%SZ'))
        end = time.mktime(
            time.strptime(self.license.get_value(import_ret.json(), 'spec.validity.notAfter'), '%Y-%m-%dT%H:%M:%SZ'))
        assert int(end - start) == data['days'] * 24 * 60 * 60, \
            "验证证书时间比对数据失败，返回数据{}，期望数据{}".format(end - start, data['days'] * 24 * 60 * 60)
        self.namelist.append(name)
        # 验证证书产品
        for product in data['products']:
            ret = self.license.verify(product)
            assert ret.status_code == 200, "验证证书失败:{}".format(ret.text)
            status = self.license.get_value(ret.json(), 'status.enabled')
            assert status, "验证证书比对数据失败，返回数据{}，期望数据{}".format(ret.text, True)

    def 测试导入license_相同证书(self):
        import_ret = self.license.create(cert_data)
        assert import_ret.status_code == 409, "导入相同license失败:{}".format(import_ret.text)

    def 测试导入license_非法证书(self):
        data = "error"
        import_ret = self.license.create(data)
        assert import_ret.status_code == 400, "导入非法license失败:{}".format(import_ret.text)

    def 测试验证license_不存在的产品(self):
        ret = self.license.verify('error')
        assert ret.status_code == 200, "验证证书失败:{}".format(ret.text)
        status = self.license.get_value(ret.json(), 'status.enabled')
        assert status is False, "验证证书比对数据失败，返回数据{}，期望数据{}".format(ret.text, False)

    def 测试删除license_不存在(self):
        data = "error"
        ret = self.license.delete(data)
        assert ret.status_code == 404, "删除不存在的license失败:{}".format(ret.text)
