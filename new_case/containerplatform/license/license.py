from common.settings import LICENSE_URL
from common.base_request import Common


class License(Common):
    def __init__(self):
        super(License, self).__init__()

    def create(self, data):
        path = 'lic/v1/licenses'
        return self.send(method='post', path=path, data=data,
                         headers={"Content-Type": "application/octet-stream"})

    def get_metadata(self):
        path = 'lic/v1/metadata'
        return self.send(method='get', path=path)

    def list(self):
        path = 'lic/v1/licenses'
        return self.send(method='get', path=path)

    def verify(self, product_name):
        path = 'fg/v1/featuregates/license?product={}'.format(product_name)
        return self.send(method='get', path=path)

    def delete(self, name):
        path = 'lic/v1/licenses/{}'.format(name)
        return self.send(method='delete', path=path)

    def generate_certificate(self, cfc="", days=30,
                             products=['Container-Platform', 'DevOps', 'Service-Mesh', 'Machine-Learning',
                                       'API-Management-Platform'], type='Demo'):
        data = {"platform_info": cfc, "days": days, "products": products, "type": type}
        response = self.send(method="post", path=LICENSE_URL, json=data)
        return response.text if response.status_code == 200 else "error"
