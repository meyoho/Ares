import pytest

from common.settings import RERUN_TIMES
from new_case.containerplatform.resourcemanage.conftest import data_list
from new_case.containerplatform.resourcemanage.resource import Resource


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestListSuite(object):
    def setup_class(self):
        self.resource_client = Resource()

    def 测试_资源管理_命名空间相关_全部命名空间_按名称搜索(self):
        ret = self.resource_client.search_resources('', 'deployments', data_list['deployment_name'])
        assert ret.status_code == 200, "全部命名空间_按名称搜索deployment失败 {}".format(ret.text)
        values = self.resource_client.generate_jinja_data("verify_data/resource/create_deploy.jinja2", data_list)
        values.pop('kind')
        values.pop('apiVersion')
        content = self.resource_client.get_k8s_resource_data(ret.json(), data_list['deployment_name'], 'items')
        assert self.resource_client.is_sub_dict(values, content), \
            "全部命名空间_按名称搜索deployment比对数据失败，返回数据{}，期望数据{}".format(content, values)

    def 测试_资源管理_命名空间相关_全部命名空间_按名称搜索_不存在(self):
        ret = self.resource_client.search_resources('', 'deployments', 'feature_name')
        assert ret.status_code == 200, "全部命名空间_按名称搜索deployment失败 {}".format(ret.text)
        content = self.resource_client.get_k8s_resource_data(ret.json(), 'deployment_name', 'items')
        assert content == {}, "全部命名空间_按名称搜索deployment比对数据失败，返回数据{}，期望数据{}".format(content, {})

    def 测试_资源管理_集群相关_按名称搜索_不存在(self):
        ret = self.resource_client.search_resources('', 'features', 'deployment_name')
        assert ret.status_code == 200, "按名称搜索feature失败 {}".format(ret.text)
        content = self.resource_client.get_k8s_resource_data(ret.json(), 'deployment_name', 'items')
        assert content == {}, "按名称搜索feature比对数据失败，返回数据{}，期望数据{}".format(content, {})
