import pytest
from common.settings import RERUN_TIMES, K8S_NAMESPACE
from new_case.containerplatform.resourcemanage.conftest import data_list, feature_list
from new_case.containerplatform.resourcemanage.resource import Resource


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestPutSuite(object):
    def setup_class(self):
        self.resource_client = Resource()

    def 测试_资源管理_命名空间相关_更新deployment(self):
        ret = self.resource_client.detail_resource(K8S_NAMESPACE, 'deployments', data_list['deployment_name'])
        assert ret.status_code == 200, "获取deployment详情失败:{}".format(ret.text)
        resourceVersion = ret.json()['metadata']['resourceVersion']
        data_list.update({'resourceVersion': resourceVersion, 'replicas': 2})
        ret = self.resource_client.update_resource(K8S_NAMESPACE, 'deployments', data_list['deployment_name'],
                                                   'test_data/resources/create_deployment.jinja2', data_list)
        assert ret.status_code == 200, "更新deployment失败:{}".format(ret.text)
        values = self.resource_client.generate_jinja_data("verify_data/resource/create_deploy.jinja2", data_list)
        assert self.resource_client.is_sub_dict(values, ret.json()), \
            "更新deployment后比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)

    def 测试_资源管理_集群相关_更新feature(self):
        ret = self.resource_client.detail_resource('', 'features', feature_list['feature_name'])
        assert ret.status_code == 200, "获取feature详情失败:{}".format(ret.text)
        resourceVersion = ret.json()['metadata']['resourceVersion']
        feature_list.update({'resourceVersion': resourceVersion, 'version': '2.0'})
        ret = self.resource_client.update_resource('', 'features', feature_list['feature_name'],
                                                   'test_data/resources/create_feature.jinja2', feature_list)
        assert ret.status_code == 200, "更新feature失败:{}".format(ret.text)
        values = self.resource_client.generate_jinja_data("verify_data/resource/create_feature.jinja2", feature_list)
        assert self.resource_client.is_sub_dict(values, ret.json()), \
            "更新feature后比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)
