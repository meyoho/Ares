import pytest

from common.settings import RERUN_TIMES, K8S_NAMESPACE
from new_case.containerplatform.resourcemanage.conftest import data_list, feature_list, data_list_v1, feature_list_v1
from new_case.containerplatform.resourcemanage.resource import Resource


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestDeleteSuite(object):
    def setup_class(self):
        self.resource_client = Resource()

    def 测试_资源管理_命名空间相关_删除deployment(self):
        ret = self.resource_client.delete_resource(K8S_NAMESPACE, 'deployments', data_list['deployment_name'])
        assert ret.status_code == 200, "删除deployment失败:{}".format(ret.text)
        data_list.update({'replicas': 2})
        values = self.resource_client.generate_jinja_data("verify_data/resource/delete_deploy.jinja2", data_list)
        assert self.resource_client.is_sub_dict(values, ret.json()), \
            "删除deployment失败，返回数据{}，期望数据{}".format(ret.json(), values)

    def 测试_资源管理_集群相关_删除feature(self):
        ret = self.resource_client.delete_resource('', 'features', feature_list['feature_name'])
        assert ret.status_code == 200, "删除feature失败:{}".format(ret.text)
        values = self.resource_client.generate_jinja_data("verify_data/resource/delete_feature.jinja2", feature_list)
        assert self.resource_client.is_sub_dict(values, ret.json()), \
            "删除feature失败，返回数据{}，期望数据{}".format(ret.json(), values)

    def 测试删除v1资源(self):
        ret = self.resource_client.delete_resource(K8S_NAMESPACE, 'deployments', data_list_v1['deployment_name'])
        assert ret.status_code == 200, "删除deployment失败:{}".format(ret.text)
        ret = self.resource_client.delete_resource('', 'features', feature_list_v1['feature_name'])
        assert ret.status_code == 200, "删除feature失败:{}".format(ret.text)

    def 测试_资源管理_命名空间相关_删除deployment_不存在(self):
        ret = self.resource_client.delete_resource(K8S_NAMESPACE, 'deployments', 'deployment_name')
        assert ret.status_code == 404, "删除deployment失败:{}".format(ret.text)

    def 测试_资源管理_集群相关_删除feature_不存在(self):
        ret = self.resource_client.delete_resource('', 'features', 'feature_name')
        assert ret.status_code == 404, "删除feature失败:{}".format(ret.text)
