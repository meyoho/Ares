import sys
from common.base_request import Common
from common.log import logger


class Resource(Common):
    def get_resourcetype_url(self):
        return 'acp/v1/resources/{}/resourcetypes'.format(self.region_name)

    # create jobs only
    def _create_job(self, data):
        api = 'v1'
        url = "acp/{}/resources/{}/resources".format(api, self.region_name)
        return self.send(method="post", path=url, json=data)

    def create_resource_url(self, namespace, kind):
        api = self.get_api(kind)
        if namespace == '':
            return 'kubernetes/{}/{}/{}'.format(self.region_name, api, kind)
        else:
            return 'kubernetes/{}/{}/namespaces/{}/{}'.format(self.region_name, api, namespace, kind)

    def create_resource_url_v1(self, region):
        return 'acp/v1/resources/{}/resources'.format(region)

    def common_resource_in_namespace_url(self, namespace, kind, name):
        api = self.get_api(kind)
        if namespace == '':
            return 'kubernetes/{}/{}/{}/{}'.format(self.region_name, api, kind, name)
        else:
            return 'kubernetes/{}/{}/namespaces/{}/{}/{}'.format(self.region_name, api, namespace, kind, name)

    def list_resource_in_namespace_url(self, namespace, kind, limit=20):
        api = self.get_api(kind)
        if namespace == '':
            return 'kubernetes/{}/{}/{}?limit={}'.format(self.region_name, api, kind, limit)
        else:
            return 'kubernetes/{}/{}/namespaces/{}/{}?limit={}'.format(self.region_name, api, namespace, kind, limit)

    def search_resource_url(self, namespace, kind, name):
        api = self.get_api(kind)
        if namespace == '':
            return 'acp/v1/resources/search/kubernetes/{}/{}/{}?limit=20&field=metadata.name&keyword={}'.format(
                self.region_name, api, kind, name)
        else:
            return 'acp/v1/resources/search/kubernetes/{}/{}/namespaces/{}/{}?limit=20&field=metadata.name&keyword={}'.format(
                self.region_name, api, namespace, kind, name)

    def list_resourcetypes(self):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_resourcetype_url()
        return self.send(method='get', path=url)

    def get_api(self, kind):
        contents = self.list_resourcetypes().json()
        for content in contents:
            for con in content['resources']:
                if con['name'] == kind:
                    tmp = content['groupVersion']
                    if '/' in tmp:
                        api = 'apis/{}'.format(tmp)
                    else:
                        api = 'api/{}'.format(tmp)
                    return api
        return ''

    def get_resource_type_list(self, contents):
        resource_list = []
        for content in contents:
            for con in content['resources']:
                resource_list.append(con['kind'])
        return set(resource_list)

    def create_resource(self, namespace, kind, file, data):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.create_resource_url(namespace, kind)
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=url, json=data)

    def create_resource_jinja2_v1(self, region, file, data):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.create_resource_url_v1(region)
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=url, json=data)

    def update_resource(self, namespace, kind, name, file, data):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.common_resource_in_namespace_url(namespace, kind, name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='put', json=data, path=url)

    def detail_resource(self, namespace, kind, name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.common_resource_in_namespace_url(namespace, kind, name)
        return self.send(method='get', path=url)

    def delete_resource(self, namespace, kind, name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.common_resource_in_namespace_url(namespace, kind, name)
        return self.send(method='delete', path=url)

    def list_resource(self, namespace, kind, limit=20):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.list_resource_in_namespace_url(namespace, kind, limit)
        return self.send(method='get', path=url)

    def search_resources(self, namespace, kind, name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.search_resource_url(namespace, kind, name)
        return self.send(method='get', path=url)
