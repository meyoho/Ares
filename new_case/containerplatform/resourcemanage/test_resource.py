import pytest

from common.settings import RERUN_TIMES, GLOBAL_ALB_NAME, K8S_NAMESPACE, DEFAULT_NS
from new_case.containerplatform.resourcemanage.resource import Resource


@pytest.mark.archon
@pytest.mark.BAT
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestResourceSuite(object):
    def setup_class(self):
        self.resource_client = Resource()

    def 测试_获取资源管理列表(self):
        all_type = self.resource_client.list_resourcetypes()
        assert all_type.status_code == 200, all_type.text
        assert len(all_type.json()) != 0, all_type.text
        types = self.resource_client.get_resource_type_list(all_type.json())
        values = {'Deployment', 'StatefulSet', 'DaemonSet', 'ConfigMap', 'Secret', 'Service', 'Ingress',
                  'PersistentVolumeClaim', 'PersistentVolume', 'StorageClass', 'Namespace', 'Feature', 'Node'}
        assert values.issubset(types), "获取资源管理列表比对数据失败，返回数据{}，期望数据{}".format(types, values)

    def 测试_资源管理_获取所有命名空间下的ALB2(self):
        list_alb2 = self.resource_client.list_resource('', 'alaudaloadbalancer2')
        assert list_alb2.status_code == 200, '获取所有命名空间下的ALB2失败 {}'.format(list_alb2.text)
        assert GLOBAL_ALB_NAME in list_alb2.text, '获取所有命名空间下的ALB2失败 {}'.format(list_alb2.text)

    def 测试_资源管理_按名称搜索ALB2(self):
        search_alb2 = self.resource_client.search_resources(DEFAULT_NS, 'alaudaloadbalancer2', GLOBAL_ALB_NAME)
        assert search_alb2.status_code == 200, '按名称搜索ALB2失败 {}'.format(search_alb2.text)
        assert GLOBAL_ALB_NAME in search_alb2.text, '按名称搜索ALB2失败 {}'.format(search_alb2.text)

    def 测试_资源管理_获取所有命名空间(self):
        list_ns = self.resource_client.list_resource('', 'namespaces', 1000)
        assert list_ns.status_code == 200, '获取所有命名空间失败 {}'.format(list_ns.text)
        assert len(list_ns.json()['items']) != 0, "按名称搜索命名空间失败 {}".format(list_ns.text)
        assert K8S_NAMESPACE in list_ns.text, '获取所有命名空间失败 {}'.format(list_ns.text)

    def 测试_资源管理_按名称搜索命名空间(self):
        search_ns = self.resource_client.search_resources('', 'namespaces', K8S_NAMESPACE)
        assert search_ns.status_code == 200, "按名称搜索命名空间失败 {}".format(search_ns.text)
        assert len(search_ns.json()['items']) != 0, "按名称搜索命名空间失败 {}".format(search_ns.text)
        assert K8S_NAMESPACE in search_ns.text, "按名称搜索命名空间失败 {}".format(search_ns.text)
