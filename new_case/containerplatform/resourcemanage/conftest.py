from common.settings import RESOURCE_PREFIX, K8S_NAMESPACE, IMAGE

data_list = {
    'deployment_name': "{}-ares-acp-resource-deploy".format(RESOURCE_PREFIX),
    'namespace': K8S_NAMESPACE,
    'image': IMAGE
}

data_list_v1 = {
    'deployment_name': "{}-ares-acp-resource-deploy-v1".format(RESOURCE_PREFIX),
    'namespace': K8S_NAMESPACE,
    'image': IMAGE
}

feature_list = {
    'feature_name': "{}-ares-acp-resource-feature".format(RESOURCE_PREFIX),
    'version': '1.0'
}

feature_list_v1 = {
    'feature_name': "{}-ares-acp-resource-feature-v1".format(RESOURCE_PREFIX),
    'version': '1.0'
}
