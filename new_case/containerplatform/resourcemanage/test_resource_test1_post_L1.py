import pytest
from common.settings import RERUN_TIMES, K8S_NAMESPACE
from new_case.containerplatform.resourcemanage.conftest import data_list, feature_list, data_list_v1, feature_list_v1
from new_case.containerplatform.resourcemanage.resource import Resource
from common import settings


@pytest.mark.archon
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestPostSuite(object):
    def setup_class(self):
        self.resource_client = Resource()

    def 测试_资源管理_命名空间相关_创建资源对象_deployment(self):
        ret = self.resource_client.create_resource(K8S_NAMESPACE, 'deployments',
                                                   'test_data/resources/create_deployment.jinja2', data_list)
        assert ret.status_code == 201, "创建资源对象_deployment失败:{}".format(ret.text)
        values = self.resource_client.generate_jinja_data("verify_data/resource/create_deploy.jinja2", data_list)
        assert self.resource_client.is_sub_dict(values, ret.json()), \
            "创建资源对象_deployment比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)

    def 测试_资源管理_集群相关_创建资源对象_feature(self):
        ret = self.resource_client.create_resource('', 'features',
                                                   'test_data/resources/create_feature.jinja2', feature_list)
        assert ret.status_code == 201, "创建资源对象_feature失败:{}".format(ret.text)
        values = self.resource_client.generate_jinja_data("verify_data/resource/create_feature.jinja2", feature_list)
        assert self.resource_client.is_sub_dict(values, ret.json()), \
            "创建资源对象_feature比对数据失败，返回数据{}，期望数据{}".format(ret.json(), values)

    def 测试_资源管理_命名空间相关_创建资源对象_deployment_v1(self):
        ret = self.resource_client.create_resource_jinja2_v1(settings.REGION_NAME,
                                                             'test_data/resources/create_deployment_v1.jinja2',
                                                             data=data_list_v1)
        assert ret.status_code == 204, "创建资源对象_deployment{}失败:{}".format(data_list_v1['deployment_name'], ret.text)

    def 测试_资源管理_集群相关_创建资源对象_feature_v1(self):
        ret = self.resource_client.create_resource_jinja2_v1(settings.REGION_NAME,
                                                             'test_data/resources/create_feature_v1.jinja2',
                                                             data=feature_list_v1)
        assert ret.status_code == 204, "创建资源对象_feature{}失败:{}".format(feature_list_v1['feature_name'], ret.text)
