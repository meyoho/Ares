import pytest

from common import settings
from common.settings import RESOURCE_PREFIX
from new_case.containerplatform.namespace.namespace import Namespace
from new_case.platform.project.project import Project

project_name = "ares-calico{}".format(RESOURCE_PREFIX)
namespace_name = '{}-ns'.format(project_name)
data_list = {
    "外出流量NAT为false,ipip模式为CrossSubnet": {
        'subnet_name': "{}-ares-calico-subnet-1".format(RESOURCE_PREFIX),
        'calico_cidr': "55.55.0.0/16",
        'nat': "false",
        'ipipmode': "CrossSubnet"
    },
    "外出流量NAT为true,ipip模式为Never": {
        'subnet_name': "{}-ares-calico-subnet-2".format(RESOURCE_PREFIX),
        'calico_cidr': "66.66.0.0/16",
        'nat': "true",
        'ipipmode': "Never"
    },
    "外出流量NAT为false,ipip模式为Always": {
        'subnet_name': "{}-ares-calico-subnet-3".format(RESOURCE_PREFIX),
        'calico_cidr': "77.77.0.0/16",
        'nat': "false",
        'ipipmode': "Always"
    }

}
update_data_list = {
    "更新添加命名空间": {
        'subnet_name': "{}-ares-calico-subnet-1".format(RESOURCE_PREFIX),
        'calico_cidr': "55.55.0.0/16",
        'nat': "false",
        'namespaces': [namespace_name],
        'ipipmode': "CrossSubnet"
    }
}

update_data_ipip = {
    "更新ipip模式": {
        'subnet_name': "{}-ares-calico-subnet-2".format(settings.RESOURCE_PREFIX),
        'calico_cidr': "66.66.0.0/16",
        'nat': "false",
        'ipipmode': "Always"
    }
}

l1_data_list = {
    "外出流量NAT为true": {
        'subnet_name': "{}-ares-calico-subnet1".format(RESOURCE_PREFIX),
        'calico_cidr': "55.55.1.0/24",
        'nat': "true",
        'ipipmode': "CrossSubnet"
    }
}
l1_update_data_list = {
    "更新添加空的命名空间": {
        'subnet_name': "{}-ares-calico-subnet1".format(RESOURCE_PREFIX),
        'calico_cidr': "55.55.1.0/24",
        'nat': "true",
        'namespace': [],
        'ipipmode': "CrossSubnet"
    }
}


@pytest.fixture(scope="session", autouse=True)
def prepare_and_clear():
    project = Project()
    namespace = Namespace()

    data = {
        "project_name": project_name,
        "regions": [settings.CALICO_REGION],
        "display_name": project_name,
        "description": "e2e test project",
    }
    create_project_result = project.create_project("./test_data/project/create_project.jinja2", data)
    assert create_project_result.status_code in (201, 409), "创建项目失败"
    ns_data = {
        "namespace_name": namespace_name,
        "display_name": namespace_name,
        "cluster": settings.CALICO_REGION,
        "project": project_name,
        "ResourceQuota": "True",
        "morelabel": "False"
    }
    create_ns_result = namespace.create_namespace('./test_data/namespace/create_namespace.jinja2', ns_data,
                                                  region_name=settings.CALICO_REGION)
    assert create_ns_result.status_code in (200, 409, 500), "创建新命名空间失败 {}".format(create_ns_result.text)
