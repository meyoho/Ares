import pytest

from common.settings import RERUN_TIMES, CALICO_REGION
from new_case.containerplatform.calico.conftest import l1_data_list
from new_case.containerplatform.calico.subnet import Subnet


@pytest.mark.skipif(not CALICO_REGION, reason="没有传calico集群名称，默认不跑对应用例")
@pytest.mark.raven
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestListSuite(object):
    def setup_class(self):
        self.subnet_client = Subnet()

    def 测试_子网列表_L1_有limit和continue参数(self):
        ret = self.subnet_client.list_subnet(limit=1)
        assert ret.status_code == 200, "获取子网列表失败:{}".format(ret.text)
        continues = self.subnet_client.get_value(ret.json(), "metadata.continue")
        ret_cnt = self.subnet_client.list_subnet(limit=1, continues=continues)
        assert ret_cnt.status_code == 200, "获取子网列表失败:{}".format(ret_cnt.text)
        assert ret.json() != ret_cnt.json(), "分页数据相同，第一页数据:{},第二页数据:{}".format(ret.json(), ret_cnt.json())

    def 测试_子网列表_L1_按名称搜索(self):
        data = l1_data_list["外出流量NAT为true"]
        subnet_name = data['subnet_name']
        names = [subnet_name, subnet_name.upper(), subnet_name.capitalize(), subnet_name[:-1]]
        for name in names:
            ret = self.subnet_client.search_subnet(name)
            assert ret.status_code == 200, "按名称搜索子网失败:{}".format(ret.text)
            values = self.subnet_client.generate_jinja_data("verify_data/calico/create_response.jinja2", data)
            content = self.subnet_client.get_k8s_resource_data(ret.json(), data['subnet_name'], 'items')
            assert self.subnet_client.is_sub_dict(values, content), \
                "按名称搜索子网比对数据失败，返回数据{}，期望数据{}".format(content, values)

    def 测试_子网列表_L1_按名称搜索_不存在(self):
        name = l1_data_list["外出流量NAT为true"]['subnet_name'] + 'notexist'
        ret = self.subnet_client.search_subnet(name)
        assert ret.status_code == 200, "按名称搜索子网失败:{}".format(ret.text)
        content = self.subnet_client.get_value(ret.json(), 'items')
        assert content == [], "按名称搜索子网比对数据失败，返回数据{}，期望数据{}".format(content, [])
