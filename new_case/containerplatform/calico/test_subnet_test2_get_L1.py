import pytest

from common.settings import RERUN_TIMES, CALICO_REGION
from new_case.containerplatform.calico.conftest import l1_data_list
from new_case.containerplatform.calico.subnet import Subnet


@pytest.mark.skipif(not CALICO_REGION, reason="没有传calico集群名称，默认不跑对应用例")
@pytest.mark.raven
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestGetSuite(object):
    def setup_class(self):
        self.subnet_client = Subnet()

    @pytest.mark.parametrize("key", l1_data_list)
    def 测试_子网详情_L1(self, key):
        data = l1_data_list[key]
        detail_result = self.subnet_client.detail_subnet(data['subnet_name'])
        assert detail_result.status_code == 200, "获取子网详情失败:{}".format(detail_result.text)
        values = self.subnet_client.generate_jinja_data("verify_data/calico/detail_response.jinja2", data)
        assert self.subnet_client.is_sub_dict(values, detail_result.json()), \
            "获取子网详情比对数据失败，返回数据:{},期望数据:{}".format(detail_result.json(), values)

    def 测试_子网详情_L1_不存在(self):
        detail_result = self.subnet_client.detail_subnet('subnet_name')
        assert detail_result.status_code == 404, "获取子网详情失败:{}".format(detail_result.text)
