import pytest
from common import settings
from new_case.containerplatform.calico.conftest import l1_data_list
from new_case.containerplatform.calico.subnet import Subnet


@pytest.mark.skipif(not settings.CALICO_REGION, reason="没有传calico集群名称，默认不跑对应用例")
@pytest.mark.raven
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestPostSuite(object):
    def setup_class(self):
        self.subnet_client = Subnet()
        self.region_name = settings.CALICO_REGION

    @pytest.mark.parametrize("key", l1_data_list)
    def 测试_子网创建_L1(self, key):
        data = l1_data_list[key]
        createsubnet_result = self.subnet_client.create_subnet("./test_data/calico/calico-subnet.jinja2", data)
        assert createsubnet_result.status_code == 201, "创建子网失败:{}".format(createsubnet_result.text)
        values = self.subnet_client.generate_jinja_data("verify_data/calico/create_response.jinja2", data)
        assert self.subnet_client.is_sub_dict(values, createsubnet_result.json()), \
            "创建子网比对数据失败，返回数据:{},期望数据:{}".format(createsubnet_result.json(), values)

    @pytest.mark.skipif(settings.AUDIT_UNABLED, reason="do not have audit")
    def 不测试子网创建审计(self):
        payload = {"user_name": settings.USERNAME, "operation_type": "create", "resource_type": "subnets",
                   "resource_name": l1_data_list["外出流量NAT为true"]['subnet_name']}
        result = self.subnet_client.search_audit(payload)
        payload.update({"namespace": "", "region_name": self.region_name, "code": 201})
        values = self.subnet_client.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.subnet_client.is_sub_dict(values, result.json()), "审计数据不符合预期"
