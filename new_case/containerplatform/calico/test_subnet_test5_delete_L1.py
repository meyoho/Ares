import pytest
from common import settings
from new_case.containerplatform.calico.conftest import l1_data_list
from new_case.containerplatform.calico.subnet import Subnet


@pytest.mark.skipif(not settings.CALICO_REGION, reason="没有传calico集群名称，默认不跑对应用例")
@pytest.mark.raven
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestDeleteSuite(object):
    def setup_class(self):
        self.subnet_client = Subnet()
        self.region_name = settings.CALICO_REGION

    @pytest.mark.parametrize("key", l1_data_list)
    def 测试_子网删除_L1(self, key):
        data = l1_data_list[key]
        delete_result = self.subnet_client.delete_subnet(data['subnet_name'])
        assert delete_result.status_code == 200, "删除子网失败:{}".format(delete_result.text)
        values = self.subnet_client.generate_jinja_data("verify_data/calico/delete_response.jinja2", data)
        assert self.subnet_client.is_sub_dict(values, delete_result.json()), \
            "删除子网比对数据失败，返回数据:{},期望数据:{}".format(delete_result.json(), values)
        assert self.subnet_client.check_exists(self.subnet_client.get_common_subnet_url(data['subnet_name']), 404)

    def 测试_子网删除_L1_不存在(self):
        delete_result = self.subnet_client.delete_subnet('subnet_name')
        assert delete_result.status_code == 404, "删除子网失败:{}".format(delete_result.text)

    @pytest.mark.skipif(settings.AUDIT_UNABLED, reason="do not have audit")
    def 不测试子网删除审计(self):
        payload = {"user_name": settings.USERNAME, "operation_type": "delete", "resource_type": "subnets",
                   "resource_name": l1_data_list["外出流量NAT为true"]['subnet_name']}
        result = self.subnet_client.search_audit(payload)
        payload.update({"namespace": "", "region_name": self.region_name})
        values = self.subnet_client.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.subnet_client.is_sub_dict(values, result.json()), "审计数据不符合预期"
