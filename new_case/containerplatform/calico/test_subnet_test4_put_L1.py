import pytest
from common import settings
from new_case.containerplatform.calico.conftest import l1_update_data_list
from new_case.containerplatform.calico.subnet import Subnet


@pytest.mark.skipif(not settings.CALICO_REGION, reason="没有传calico集群名称，默认不跑对应用例")
@pytest.mark.raven
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestPutSuite(object):
    def setup_class(self):
        self.subnet_client = Subnet()
        self.region_name = settings.CALICO_REGION

    @pytest.mark.parametrize("key", l1_update_data_list)
    def 测试_子网更新_L1(self, key):
        data = l1_update_data_list[key]
        update_result = self.subnet_client.update_subnet(data['subnet_name'], "./test_data/calico/update-calico-subnet.jinja2",
                                                         data)
        assert update_result.status_code == 200, "更新子网出错:{}".format(update_result.text)
        values = self.subnet_client.generate_jinja_data("verify_data/calico/detail_response.jinja2", data)
        assert self.subnet_client.is_sub_dict(values, update_result.json()), \
            "更新子网比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), values)

    @pytest.mark.skipif(settings.AUDIT_UNABLED, reason="do not have audit")
    def 不测试子网更新审计(self):
        payload = {"user_name": settings.USERNAME, "operation_type": "update", "resource_type": "subnets",
                   "resource_name": l1_update_data_list["更新添加空的命名空间"]['subnet_name']}
        result = self.subnet_client.search_audit(payload)
        payload.update({"namespace": "", "region_name": self.region_name})
        values = self.subnet_client.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.subnet_client.is_sub_dict(values, result.json()), "审计数据不符合预期"
