import pytest
import IPy
from new_case.containerplatform.application.application import Application
from new_case.containerplatform.calico.conftest import data_list, update_data_list, project_name, namespace_name, update_data_ipip
from new_case.containerplatform.calico.subnet import Subnet
from common.settings import RERUN_TIMES, CALICO_REGION, IMAGE, CASE_TYPE
from new_case.containerplatform.namespace.namespace import Namespace
from time import sleep


@pytest.mark.skipif(not CALICO_REGION, reason="没有传calico集群名称，默认不跑对应用例")
@pytest.mark.raven
@pytest.mark.BAT
@pytest.mark.acp_containerplatform
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestSubnetSuite(object):
    def setup_class(self):
        self.subnet_client = Subnet()
        self.application = Application()
        self.ns_client = Namespace()
        self.namespace = namespace_name
        self.project_name = project_name
        self.app_name = "calico-subnet-app"
        self.app_name_ip = "calico-ip-app"
        self.region_name = CALICO_REGION
        list_result = self.subnet_client.list_ip(subnet_name=update_data_list["更新添加命名空间"]['subnet_name'])
        # 删除子网前先释放IP
        for ips in list_result.json()['items']:
            ip = self.subnet_client.get_value(ips, 'metadata.name')
            self.subnet_client.delete_ip(ip)
            self.subnet_client.check_exists(
                "kubernetes/{}/apis/kubeovn.io/v1/ips/{}".format(self.region_name, ip), 404,
                expect_cnt=30)

    def teardown_class(self):
        if CASE_TYPE not in ("prepare", "upgrade", "delete"):
            self.application.delete_app(self.namespace, self.app_name, self.region_name)
            self.application.check_exists(self.application.common_app_url(self.namespace, self.app_name, self.region_name),
                                          404)
            self.application.delete_app(self.namespace, self.app_name_ip, self.region_name)
            self.application.check_exists(
                self.application.common_app_url(self.namespace, self.app_name_ip, self.region_name),
                404)

    @pytest.mark.prepare
    @pytest.mark.parametrize("key", data_list)
    def 测试子网增加(self, key):
        data = data_list[key]
        createsubnet_result = self.subnet_client.create_subnet("./test_data/calico/calico-subnet.jinja2", data)
        assert createsubnet_result.status_code == 201, "创建子网失败:{}".format(createsubnet_result.text)
        values = self.subnet_client.generate_jinja_data("verify_data/calico/create_response.jinja2", data)
        assert self.subnet_client.is_sub_dict(values, createsubnet_result.json()), \
            "创建子网比对数据失败，返回数据:{},期望数据:{}".format(createsubnet_result.json(), values)

    @pytest.mark.upgrade
    @pytest.mark.parametrize("key", data_list)
    def 测试子网列表(self, key):
        data = data_list[key]
        list_result = self.subnet_client.list_subnet(limit=1000)
        assert list_result.status_code == 200, "获取子网列表失败:{}".format(list_result.text)
        content = self.subnet_client.get_k8s_resource_data(list_result.json(), data['subnet_name'], list_key="items")
        values = self.subnet_client.generate_jinja_data("verify_data/calico/create_response.jinja2", data)
        assert self.subnet_client.is_sub_dict(values, content), \
            "获取子网列表比对数据失败，返回数据:{},期望数据:{}".format(content, values)

    @pytest.mark.upgrade
    @pytest.mark.parametrize("key", data_list)
    def 测试子网详情(self, key):
        data = data_list[key]
        detail_result = self.subnet_client.detail_subnet(data['subnet_name'])
        assert detail_result.status_code == 200, "获取子网详情失败:{}".format(detail_result.text)
        values = self.subnet_client.generate_jinja_data("verify_data/calico/detail_response.jinja2", data)
        assert self.subnet_client.is_sub_dict(values, detail_result.json()), \
            "获取子网详情比对数据失败，返回数据:{},期望数据:{}".format(detail_result.json(), values)

    @pytest.mark.prepare
    @pytest.mark.parametrize("key", update_data_list)
    def 测试更新子网_更新命名空间(self, key):
        data = update_data_list[key]
        update_result = self.subnet_client.update_subnet(data['subnet_name'],
                                                         "./test_data/calico/update-calico-subnet.jinja2",
                                                         data)
        assert update_result.status_code == 200, "更新子网出错:{}".format(update_result.text)
        values = self.subnet_client.generate_jinja_data("verify_data/calico/detail_response.jinja2", data)
        assert self.subnet_client.is_sub_dict(values, update_result.json()), \
            "更新子网比对数据失败，返回数据:{},期望数据:{}".format(update_result.json(), values)
        sleep(3)
        get_ns_result = self.ns_client.get_namespaces(self.namespace, self.region_name)
        assert get_ns_result.status_code == 200, "获取命名空间失败 {}".format(get_ns_result.text)
        ipv4pools = self.ns_client.get_value(get_ns_result.json(),
                                             'metadata#annotations#cni.projectcalico.org/ipv4pools', '#')
        assert update_data_list["更新添加命名空间"]['subnet_name'] in ipv4pools, "命名空间注解未更新"

    @pytest.mark.parametrize("key", update_data_ipip)
    def 测试更新子网_更新网络模式(self, key):
        data = update_data_ipip[key]
        update_result = self.subnet_client.update_subnet(data_list['外出流量NAT为true,ipip模式为Never']['subnet_name'],
                                                         "./test_data/calico/update-calico-subnet-ipip.jinja2", data)
        assert update_result.status_code == 200, "更新网络模式出错:{}".format(update_result.text)

        values = self.subnet_client.generate_jinja_data("verify_data/calico/detail_response.jinja2", data)
        assert self.subnet_client.is_sub_dict(values, update_result.json()), \
            "更新网络模式比对数据失败，返回数据:{},期望数据:{}".format(
            update_result.json(), values)

    @pytest.mark.prepare
    def 测试创建应用使用的是子网内的IP(self):
        self.application.delete_app(self.namespace, self.app_name, self.region_name)
        app_data = {
            "app_name": self.app_name,
            "ns_name": self.namespace,
            "resources": [
                {
                    "deploy_name": self.app_name,
                    "ns_name": self.namespace,
                    "project": self.project_name,
                    "app_name": self.app_name,
                    "containers": [
                        {
                            "image": IMAGE,

                        }
                    ]
                }
            ]
        }
        ret = self.application.create_app_by_template(app_data, self.namespace, self.region_name)
        assert ret.status_code == 200, "应用创建失败 {}".format(ret.text)
        app_status = self.application.get_app_status(self.namespace, self.app_name, 'Running',
                                                     region_name=self.region_name)
        assert app_status, "创建应用后，验证应用状态出错：app: {} is not running".format(self.app_name)
        ret = self.application.get_app_pod(self.namespace, self.app_name, self.region_name)
        assert ret.status_code == 200, "获取容器组失败:{}".format(ret.text)
        podIP = self.application.get_value(ret.json(), 'items.0.status.podIP')
        assert podIP in IPy.IP(data_list["外出流量NAT为false,ipip模式为CrossSubnet"]['calico_cidr']), \
            "容器组IP{}不在子网内{}".format(podIP, data_list["外出流量NAT为false,ipip模式为CrossSubnet"]['calico_cidr'])

    @pytest.mark.prepare
    def 测试创建应用使用固定IP(self):
        self.application.delete_app(self.namespace, self.app_name_ip, self.region_name)
        app_data = {
            "app_name": self.app_name_ip,
            "ns_name": self.namespace,
            "resources": [
                {
                    "deploy_name": self.app_name_ip,
                    "ns_name": self.namespace,
                    "project": self.project_name,
                    "app_name": self.app_name_ip,
                    "calico_ip_pool": ["55.55.55.55"],
                    "containers": [
                        {
                            "image": IMAGE,

                        }
                    ]
                }
            ]
        }
        ret = self.application.create_app_by_template(app_data, self.namespace, self.region_name)
        assert ret.status_code == 200, "应用创建失败 {}".format(ret.text)
        app_status = self.application.get_app_status(self.namespace, self.app_name_ip, 'Running',
                                                     region_name=self.region_name)
        assert app_status, "创建应用后，验证应用状态出错：app: {} is not running".format(self.app_name_ip)
        ret = self.application.get_app_pod(self.namespace, self.app_name_ip, self.region_name)
        assert ret.status_code == 200, "获取容器组失败:{}".format(ret.text)
        podIP = self.application.get_value(ret.json(), 'items.0.status.podIP')
        assert podIP == "55.55.55.55", "容器组IP{}不是指定的固定IP{}".format(podIP, "55.55.55.55")

    @pytest.mark.upgrade
    def 测试获取子网详情的已用IP列表(self):
        list_result = self.subnet_client.list_ip(subnet_name=update_data_list["更新添加命名空间"]['subnet_name'])
        assert list_result.status_code == 200, "获取子网内已用IP列表失败:{}".format(list_result.text)
        assert "55.55.55.55" in list_result.text, "55.55.55.55不在已用 IP 列表中"

    @pytest.mark.upgrade
    def 测试搜索IP(self):
        search_result = self.subnet_client.search_ip(subnet_name=update_data_list["更新添加命名空间"]['subnet_name'],
                                                     ip="55.55.55.55")
        assert search_result.status_code == 200, "搜索 IP失败:{}".format(search_result.text)
        assert "55.55.55.55" in search_result.text, "55.55.55.55不在已用 IP 列表中"

    def 测试搜索不存在的IP(self):
        search_result = self.subnet_client.search_ip(subnet_name=update_data_list["更新添加命名空间"]['subnet_name'],
                                                     ip="notexist")
        assert search_result.status_code == 200, "搜索不存在的IP失败:{}".format(search_result.text)
        content = self.subnet_client.get_value(search_result.json(), 'items')
        assert content == [], "搜索不存在的IP比对数据失败，返回数据{}，期望数据{}".format(content, [])

    @pytest.mark.delete
    @pytest.mark.parametrize("key", data_list)
    def 测试删除子网(self, key):
        data = data_list[key]
        list_result = self.subnet_client.list_ip(subnet_name=update_data_list["更新添加命名空间"]['subnet_name'])
        # 删除子网前先释放IP
        for ips in list_result.json()['items']:
            ip = self.subnet_client.get_value(ips, 'metadata.name')
            self.subnet_client.delete_ip(ip)

        delete_result = self.subnet_client.delete_subnet(data['subnet_name'])
        assert delete_result.status_code == 200, "删除子网失败:{}".format(delete_result.text)
        values = self.subnet_client.generate_jinja_data("verify_data/calico/delete_response.jinja2", data)
        assert self.subnet_client.is_sub_dict(values, delete_result.json()), \
            "删除子网比对数据失败，返回数据:{},期望数据:{}".format(delete_result.json(), values)
        assert self.subnet_client.check_exists(self.subnet_client.get_common_subnet_url(data['subnet_name']), 404)
