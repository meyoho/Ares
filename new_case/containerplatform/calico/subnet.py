import sys

from common.base_request import Common
from common.log import logger
from common import settings


class Subnet(Common):

    def create_subnet_url(self, region_name=settings.CALICO_REGION):
        return "kubernetes/{}/apis/kubeovn.io/v1/subnets".format(region_name)

    def get_common_subnet_url(self, subnet_name='', region_name=settings.CALICO_REGION):
        return "kubernetes/{}/apis/kubeovn.io/v1/subnets/{}".format(region_name, subnet_name)

    def list_subnet_url(self, limit=20, continues='', selector='', region_name=settings.CALICO_REGION):
        path = "kubernetes/{}/apis/kubeovn.io/v1/subnets?limit={}".format(region_name, limit)
        if continues != '':
            path = '{}&continue={}'.format(path, continues)
        if selector != '':
            path = '{}&fieldSelector=metadata.name=={}'.format(path, selector)
        return path

    def list_ip_url(self, subnet_name, limit=20, region_name=settings.CALICO_REGION):
        return "kubernetes/{}/apis/kubeovn.io/v1/ips?limit={}&labelSelector=ovn.kubernetes.io/subnet={}" \
               "&field=spec.ipAddress".format(region_name, limit, subnet_name)

    def delete_ip_url(self, ip, region_name=settings.CALICO_REGION):
        return "kubernetes/{}/apis/kubeovn.io/v1/ips/{}".format(region_name, ip)

    def create_subnet(self, file, data):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.create_subnet_url()
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=url, json=data)

    def list_subnet(self, limit=20, continues='', selector=''):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.list_subnet_url(limit, continues, selector)
        return self.send(method='get', path=url)

    def detail_subnet(self, subnet_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_subnet_url(subnet_name)
        return self.send(method='get', path=url)

    def update_subnet(self, subnet_name, file, data):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_subnet_url(subnet_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='patch', path=url, json=data, headers={"Content-Type": "application/merge-patch+json"})

    def delete_subnet(self, subnet_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.get_common_subnet_url(subnet_name)
        return self.send(method='delete', path=url)

    def list_ip(self, subnet_name, limit=20, region_name=settings.CALICO_REGION):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.list_ip_url(subnet_name, limit, region_name)
        return self.send(method='get', path=url)

    def delete_ip(self, ip, region_name=settings.CALICO_REGION):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = self.delete_ip_url(ip, region_name)
        return self.send(method='delete', path=url)

    def search_subnet(self, name, region_name=settings.CALICO_REGION):
        url = "acp/v1/resources/search/kubernetes/{}/apis/kubeovn.io/v1/subnets?limit=20&keyword={}&field=metadata.name".format(
            region_name, name)
        return self.send('get', url)

    def search_ip(self, subnet_name, ip, region_name=settings.CALICO_REGION):
        url = "acp/v1/resources/search/kubernetes/{}/apis/kubeovn.io/v1/ips?limit=20&labelSelector=ovn.kubernetes.io" \
              "/subnet={}&keyword={}&field=spec.ipAddress".format(region_name, subnet_name, ip)
        return self.send('get', url)
