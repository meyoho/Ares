from common.base_request import Common


def get_node_info(data):
    node_infos = []
    items = Common.get_value(data, "items")
    for item in items:
        node_info = {}
        addr = Common.get_value(item, "status.addresses")
        for addr_info in addr:
            if addr_info["type"] == "InternalIP":
                node_info.update({"node_ip": addr_info["address"]})
            elif addr_info["type"] == "Hostname":
                node_info.update({"node_name": addr_info["address"]})
            else:
                pass
        lables = Common.get_value(item, "metadata.labels")
        for label_key in lables.keys():
            if label_key == "node-role.kubernetes.io/node":
                node_info.update({"node_type": "node"})
            elif label_key == "node-role.kubernetes.io/master":
                node_info.update({"node_type": "master"})
            else:
                pass
        node_info.update({"labels": Common.get_value(item, "metadata.labels")})
        node_info.update({"capacity": Common.get_value(item, "status.capacity")})
        node_info.update({"allocatable": Common.get_value(item, "status.allocatable")})
        node_infos.append(node_info)
    return node_infos
