import pytest
from new_case.cluster.old_cluster.cluster import Cluster
from new_case.cluster.old_cluster.qcloud import create_instance, get_instance, destroy_instance
from common.settings import RESOURCE_PREFIX, VM_PASSWORD, GLOBAL_REGION_NAME, REGISTRY, ENV, DEFAULT_NS, USERNAME
from new_case.containerplatform.secret.secret import Secret
from common.log import logger


@pytest.mark.furion
@pytest.mark.acp_infra
@pytest.mark.Regression
@pytest.mark.skip(reason="http://jira.alauda.cn/browse/TEST-406")
@pytest.mark.skipif(ENV not in ("int", "staging", "prod"),
                    reason="ENV is not int staging prod ,can not create vm to deploy region")
class TestClusterSuite(object):
    def setup_class(self):
        self.instances_id = []
        self.run_case = True
        try:
            self.cluster = Cluster()
            self.secret = Secret()
            self.secret.region_name = GLOBAL_REGION_NAME
            self.vxlan_cluster_name = '{}-ares-cluster8'.format(RESOURCE_PREFIX)
            ret_create = create_instance(2, "acp2.0-automation")
            assert ret_create["success"], ret_create["message"]
            self.instances_id = ret_create["instances_id"]
            ret_get = get_instance(self.instances_id)
            assert ret_get["success"], ret_get["message"]
            self.private_ips = ret_get['private_ips']
            self.public_ips = ret_get['public_ips']
            self.cluster.restart_sshd(self.public_ips[0])
            for public_ip in self.public_ips:
                self.cluster.excute_script("hostname node$RANDOM", public_ip, use_key=False, user="root")
            self.secret.delete_secret("cluster-{}".format(self.vxlan_cluster_name), ns_name=self.secret.default_ns)
        except Exception as e:
            destroy_instance(self.instances_id)
            self.run_case = False
            logger.error("创建机器失败:{}".format(e))

    def teardown_class(self):
        self.cluster.delete_cluster(self.vxlan_cluster_name)
        self.secret.delete_secret("cluster-{}".format(self.vxlan_cluster_name), ns_name=self.secret.default_ns)
        if self.instances_id:
            destroy_instance(self.instances_id)
            self.cluster.cleanup_cluster(self.public_ips)

    def 测试创建calico集群(self):
        if not self.run_case:
            return
        get_script = self.cluster.generate_install_cmd("test_data/cluster/one_node_cluster_cmd.json",
                                                       {"$cluster_name": self.vxlan_cluster_name,
                                                        "$node_ip": self.private_ips[0],
                                                        "$VM_USERNAME": "root",
                                                        "$VM_PASSWORD": VM_PASSWORD})
        assert get_script.status_code == 200, "获取创建集群脚本失败:{}".format(get_script.text)
        cmd = "export LANG=zh_CN.utf8;export LC_ALL=zh_CN.utf8;{}".format(get_script.json()["commands"]["install"])
        ret_excute = self.cluster.excute_script(cmd, self.public_ips[0], use_key=False, user="root")
        assert "Install successfully!" in ret_excute[1], "执行脚本失败:{}".format(ret_excute[1])
        is_exist = self.cluster.check_value_in_response("v2/regions/{}".format(self.cluster.account),
                                                        self.vxlan_cluster_name,
                                                        params={}, expect_cnt=60)
        assert is_exist, "添加集群超时"

        # 安装crd
        cmd = "helm repo add release http://7DO9QoLtDx1g:2wb4E1iydRj7@alauda-chart-release-new.ace-default.cloud.myalauda.cn/; " \
              "helm repo update;helm install --name cert-manager --namespace {ns} --set global.registry.address={registry} release/cert-manager" \
              " --version v2.6.5;helm install --debug release/alauda-cluster-base --name alauda-cluster-base --namespace {ns}  " \
              "--set global.registry.address={registry} --set global.auth.default_admin={user}" \
              " --set global.namespace={ns} --version v2.6.5".format(ns=DEFAULT_NS, user=USERNAME, registry=REGISTRY)
        ret_excute = self.cluster.excute_script(cmd, self.public_ips[0], use_key=False, user="root")
        assert "ContainerCreating" in ret_excute[1], "部署crd失败:{}".format(ret_excute[1])

    def 测试添加集群主机节点(self):
        if not self.run_case:
            return
        ret_addnode = self.cluster.add_nodes(self.vxlan_cluster_name, "test_data/cluster/addnode.json",
                                             {"$node_ip": self.private_ips[1],
                                              "$VM_USERNAME": "root",
                                              "$VM_PASSWORD": VM_PASSWORD})
        assert ret_addnode.status_code == 204, "添加节点失败:{}".format(ret_addnode.text)
        is_exist = self.cluster.check_value_in_response(self.cluster.get_node_url(self.vxlan_cluster_name),
                                                        self.private_ips[1],
                                                        params={}, expect_cnt=60)
        assert is_exist, "添加节点超时"

        ret_delete = self.cluster.delete_cluster(self.vxlan_cluster_name)
        assert ret_delete.status_code == 204, "删除集群失败:{}".format(ret_delete.text)

    def 测试接入集群(self):
        if not self.run_case:
            return
        data = {
            "name": self.vxlan_cluster_name,
            "ns": self.cluster.default_ns,
            "secret_name": "cluster-{}".format(self.vxlan_cluster_name),
            "k8s_addr": "https://{}:6443".format(self.private_ips[0])
        }
        result = self.cluster.create_cluster("./test_data/cluster/access_region.jinja2", data)
        assert result.status_code == 201, "接入集群失败:{}".format(result.text)
        value = self.cluster.generate_jinja_data("verify_data/cluster/access_region.jinja2", data)
        assert self.cluster.is_sub_dict(value, result.json()), \
            "接入集群比对数据失败，返回数据:{},期望数据:{}".format(result.json(), value)

    def 测试删除节点(self):
        if not self.run_case:
            return
        ret_node = self.cluster.get_node_list(self.vxlan_cluster_name)
        assert ret_node.status_code == 200, "获取node列表失败:{}".format(ret_node.text)
        node_name = self.cluster.get_value(ret_node.json(), "items.0.metadata.name")
        result = self.cluster.delete_node_new_api(self.vxlan_cluster_name, node_name)
        assert result.status_code == 200, "测试删除节点失败:{}".format(result.text)
        value = self.cluster.generate_jinja_data("verify_data/cluster/delete_region.jinja2",
                                                 {"name": node_name, "kind": "nodes"})
        assert self.cluster.is_sub_dict(value, result.json()), \
            "删除节点比对数据失败，返回数据:{},期望数据:{}".format(result.json(), value)

    def 测试删除集群(self):
        if not self.run_case:
            return
        result = self.cluster.delete_cluster_new_api(self.vxlan_cluster_name)
        assert result.status_code == 200, "测试删除集群失败:{}".format(result.text)
        value = self.cluster.generate_jinja_data("verify_data/cluster/delete_region.jinja2",
                                                 {"name": self.vxlan_cluster_name, "kind": "clusters"})
        assert self.cluster.is_sub_dict(value, result.json()), \
            "删除集群比对数据失败，返回数据:{},期望数据:{}".format(result.json(), value)
