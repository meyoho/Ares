from time import sleep, time
import random
import base64
import hashlib
import hmac
import requests
from subprocess import getoutput
from common import settings
from common.log import logger

secret_id = settings.SECRET_ID
secret_key = settings.SECRET_KEY
endpoint = "cvm.tencentcloudapi.com"
lb_name = "aketest{}".format(random.randint(0, 1000))


def get_string_to_sign(method, endpoint, params):
    """将参数拼接在URL"""
    s = method + endpoint + "/?"
    query_str = "&".join("%s=%s" % (k, params[k]) for k in sorted(params))
    return s + query_str


def sign_str(key, s, method):
    """用来做腾讯接口签名摘要"""
    key = str(base64.b64decode(key), encoding="utf-8")
    hmac_str = hmac.new(key.encode("utf8"), s.encode("utf8"), method).digest()
    return base64.b64encode(hmac_str)


def create_instance(num, name=''):
    instance_name = name or getoutput("git config user.name")
    params = {
        "Action": "RunInstances",
        # "Placement.Zone": "ap-beijing-3",
        "Placement.ProjectId": 1126842,
        # "Region": "ap-beijing",
        # "VirtualPrivateCloud.VpcId": "vpc-366ds668",
        # "VirtualPrivateCloud.SubnetId": "subnet-pfdj7bg5",
        "ImageId": "img-9qabwvbn",  # centos7.6
        # "InstanceType": "S2.LARGE8",
        "InstanceCount": num,
        "SystemDisk.DiskType": "CLOUD_PREMIUM",
        "SystemDisk.DiskSize": 50,
        "DataDisks.0.DiskType": "CLOUD_PREMIUM",
        "DataDisks.0.DiskSize": 50,
        "InstanceName": instance_name,
        "LoginSettings.Password": settings.VM_PASSWORD,
        # "SecurityGroupIds.0": "sg-52hnrp2p",
        "Version": "2017-03-12",
        # "InstanceChargeType": "SPOTPAID",
        # "InstanceMarketOptions.MarketType": "spot",
        # "InstanceMarketOptions.SpotOptions.MaxPrice":"",
        "HostName": "node",
        "InternetAccessible.InternetChargeType": "TRAFFIC_POSTPAID_BY_HOUR",
        "InternetAccessible.InternetMaxBandwidthOut": 1,
        "InternetAccessible.PublicIpAssigned": True,
        "TagSpecification.0.ResourceType": "instance",
        "TagSpecification.0.Tags.0.Key": "Group",
        "TagSpecification.0.Tags.0.Value": "QA",
        "UserData": "IyEvYmluL2Jhc2gKeXVtIGluc3RhbGwgLXkgc3NocGFzcwo=",
        "Timestamp": int(time()),
        "Nonce": random.randint(0, 1000),
        "SecretId": secret_id
    }
    if settings.ENV in ('int', 'staging', 'prod'):
        params.update({
            "Placement.Zone": "ap-chongqing-1",
            "Region": "ap-chongqing",
            "InstanceType": "SA1.LARGE8",
            "VirtualPrivateCloud.VpcId": "vpc-c9el9wfx",
            "VirtualPrivateCloud.SubnetId": "subnet-6ohk5y8a",
            "SecurityGroupIds.0": "sg-f9whhvbu"
        })
    else:
        return {"success": False, "message": "ENV is not int staging prod ,can not create vm to deploy region"}
    s = get_string_to_sign("GET", endpoint, params)
    params["Signature"] = sign_str(secret_key, s, hashlib.sha1)
    logger.info("start to create qcloud vm, url is :{},params is {}".format(endpoint, params))
    res = requests.get("https://" + endpoint, params=params)
    instances_id = []
    logger.info("create vm response:{}".format(res.text))
    instances_id += res.json()['Response'].get("InstanceIdSet")
    if not instances_id:
        return {"success": False, "message": "create qcloud vm failed: {}".format(res.text)}
    logger.info("create vm success, but need to sleep 120s")
    sleep(120)
    return {"success": True, "instances_id": instances_id, "message": "create qcloud vm over"}


def get_instance(instances_id):
    params = {
        "Action": "DescribeInstances",
        "Version": "2017-03-12",
        # "Region": "ap-beijing",
        "SecretId": secret_id,
        "Timestamp": int(time()),
        "Nonce": random.randint(0, 1000),
    }
    if settings.ENV in ('int', 'staging', 'prod'):
        params.update({"Region": "ap-chongqing"})
    else:
        pass
    for i in range(0, len(instances_id)):
        params.update({"InstanceIds." + str(i): str(instances_id[i])})
    s = get_string_to_sign("GET", endpoint, params)
    params["Signature"] = sign_str(secret_key, s, hashlib.sha1)
    res = requests.get("https://" + endpoint, params=params)
    logger.info(res.json())
    if "InstanceSet" in res.json()['Response']:
        private_ips = []
        public_ips = []
        instances_info = res.json()['Response']['InstanceSet']
        for instance_info in instances_info:
            private_ips.append(str(instance_info['PrivateIpAddresses'][0]))
            public_ips.append(str(instance_info['PublicIpAddresses'][0]))
        return {"success": True, "private_ips": private_ips, "public_ips": public_ips,
                "message": "describe qcloud vm over"}
    return {"success": False, "message": "get qcloud vm info failed:{}".format(res.text)}


def get_instanceid(instance_name):
    params = {
        "Action": "DescribeInstances",
        "Version": "2017-03-12",
        # "Region": "ap-beijing",
        "SecretId": secret_id,
        "Timestamp": int(time()),
        "Nonce": random.randint(0, 1000),
    }
    if settings.ENV in ('int', 'staging', 'prod'):
        params.update({"Region": "ap-chongqing"})
    else:
        pass
    params.update({"Filters.0.Name": "instance-name"})
    for i in range(0, 5):
        params.update({"Filters.0.Values." + str(i): instance_name + str(i)})
    # params.update({"vagueInstanceNam": instance_name})
    print(params)
    s = get_string_to_sign("GET", endpoint, params)
    params["Signature"] = sign_str(secret_key, s, hashlib.sha1)
    res = requests.get("https://" + endpoint, params=params)
    if "InstanceSet" in res.json()['Response']:
        instance_ids = []
        instances_info = res.json()['Response']['InstanceSet']
        for instance_info in instances_info:
            instance_ids.append(instance_info['InstanceId'])
        return instance_ids
    return []


def destroy_instance(instances_id):
    params = {
        "Action": "TerminateInstances",
        "Version": "2017-03-12",
        # "Region": "ap-beijing",
        "SecretId": secret_id,
        "Timestamp": int(time()),
        "Nonce": random.randint(0, 1000),
    }
    if settings.ENV in ('int', 'staging', 'prod'):
        params.update({"Region": "ap-chongqing"})
    else:
        pass
    for i in range(0, len(instances_id)):
        params.update({"InstanceIds." + str(i): str(instances_id[i])})
    s = get_string_to_sign("GET", endpoint, params)
    params["Signature"] = sign_str(secret_key, s, hashlib.sha1)
    logger.info("delete qcloud vm url is :{},params is {}".format(endpoint, params))
    res = requests.get("https://" + endpoint, params=params)
    logger.info(res.text)
    assert "RequestId" in res.json()['Response'], "delete qcloud vm failed,please check it:{}".format(res.text)
