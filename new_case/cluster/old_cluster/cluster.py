import sys
from common.base_request import Common
from common.log import logger


class Cluster(Common):
    def __init__(self):
        super(Cluster, self).__init__()
        self.account = "default"
        # self.genetate_global_info()

    def get_node_url(self, region_name, node_name=''):
        return node_name and "v2/regions/{}/{}/nodes/{}".format(self.account, region_name, node_name) or \
               "v2/regions/{}/{}/nodes".format(self.account, region_name)

    def generate_install_cmd(self, file, data):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = "v2/regions/{}/generate-installation-command".format(self.account)
        data = self.generate_data(file, data)
        return self.send(method="post", path=url, data=data, params={})

    def get_region_info(self, region_name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = "v2/regions/{}/{}".format(self.account, region_name)
        return self.send(method="get", path=url, params={})

    def get_region_list(self):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = "v2/regions/{}".format(self.account)
        return self.send(method="get", path=url, params={})

    def get_node_list(self, region_name):
        url = self.get_node_url(region_name)
        return self.send(method="get", path=url, params={})

    def add_nodes(self, region_name, file, data):
        url = self.get_node_url(region_name)
        data = self.generate_data(file, data)
        return self.send(method="post", path=url, data=data, params={})

    def delete_cluster(self, region_name):
        url = "v2/regions/{}/{}?force=true".format(self.account, region_name)
        return self.send(method="delete", path=url, params={})

    def cleanup_cluster(self, ips):
        for ip in ips:
            cmd = "curl http://get.alauda.cn/deploy/ake/cleanup |timeout 600 sudo sh;rm -rf ake"
            self.excute_script(cmd, ip, use_key=False, user="root")

    def restart_sshd(self, ip):
        cmd = "sudo sed -ir 's/#MaxSessions/MaxSessions/' /etc/ssh/sshd_config"
        self.excute_script(cmd, ip, use_key=False, user="root")
        self.excute_script("service sshd restart", ip, use_key=False, user="root")

    def create_cluster(self, file, data):
        path = "apis/clusterregistry.k8s.io/v1alpha1/namespaces/{}/clusters".format(self.default_ns)
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=path, json=data)

    def delete_cluster_new_api(self, name):
        path = "apis/clusterregistry.k8s.io/v1alpha1/namespaces/{}/clusters/{}".format(self.default_ns, name)
        return self.send(method="delete", path=path)

    def delete_node_new_api(self, cluster_name, node_name):
        path = "kubernetes/{}/api/v1/nodes/{}".format(cluster_name, node_name)
        return self.send(method="delete", path=path)
