# coding=utf-8
from new_case.cluster.cluster import Cluster
from common.settings import RERUN_TIMES
import pytest
from new_case.cluster.utils import get_node_info


@pytest.mark.cluster_registry_controller_manager
@pytest.mark.Regression
@pytest.mark.acp_infra
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestGetNode(object):
    def setup_class(self):
        self.cluster_client = Cluster()

    def 测试获取主机节点_有limit参数(self):
        result = self.cluster_client.get_region_nodes_with_limit()
        assert result.status_code == 200, "获取获取主机节点_有limit参数失败:{}".format(result.text)
        global node_infos
        node_infos = get_node_info(result.json())
        global cnt
        cnt = self.cluster_client.get_value(result.json(), "metadata.continue")
        values = self.cluster_client.generate_jinja_data("verify_data/cluster/node_list.jinja2", {"node_infos": node_infos})
        assert self.cluster_client.is_sub_dict(values, result.json()), \
            "获取集群节点比对数据失败，返回数据{}，期望数据{}".format(result.json(), values)

    def 测试获取主机节点_有limit和continue参数(self):
        result = self.cluster_client.get_region_nodes_with_limit(cnt=cnt)
        assert result.status_code == 200, "获取获取主机节点_有limit和continue参数:{}".format(result.text)
        node_infos_continue = get_node_info(result.json())
        assert node_infos_continue != node_infos, "分页数据相同，主机第一页数据:{},主机第二页数据:{}".format(node_infos, node_infos_continue)
