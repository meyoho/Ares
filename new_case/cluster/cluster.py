from common.base_request import Common
from common import settings


class Cluster(Common):
    def __init__(self):
        self.cluster_template = self.generate_jinja_template("verify_data/cluster/cluster_detail.jinja2")
        super(Cluster, self).__init__()

    def get_region_nodes(self):
        path = "kubernetes/{}/api/v1/nodes".format(self.region_name)
        return self.send(method='GET', path=path, params={})

    def get_region_nodes_with_limit(self, limit=1, cnt=""):
        path = "kubernetes/{}/api/v1/nodes?limit={}&continue={}".format(self.region_name, limit, cnt)
        return self.send(method='GET', path=path, params={})

    def get_region_list_with_limit(self, limit=1, cnt=""):
        path = "apis/clusterregistry.k8s.io/v1alpha1/namespaces/{}/clusters?limit={}&continue={}".format(settings.DEFAULT_NS, limit, cnt)
        return self.send(method='GET', path=path, params={})

    def get_node_detail(self, node_name):
        path = "kubernetes/{}/api/v1/nodes/{}".format(self.region_name, node_name)
        return self.send(method='GET', path=path, params={})

    def update_node_labels(self, node_name, data):
        path = "kubernetes/{}/api/v1/nodes/{}".format(self.region_name, node_name)
        return self.send(method='PATCH', path=path, json=data, headers={"Content-Type": "application/merge-patch+json"})

    def get_region_list_or_detail(self, list=True):
        path = list and "apis/clusterregistry.k8s.io/v1alpha1/namespaces/{}/clusters/".format(settings.DEFAULT_NS) or \
               "apis/clusterregistry.k8s.io/v1alpha1/namespaces/{}/clusters/{}".format(settings.DEFAULT_NS, self.region_name)
        return self.send(method='GET', path=path, params={})

    def get_region_feature(self, feature, region=settings.REGION_NAME):
        path = "kubernetes/{}/apis/infrastructure.alauda.io/v1alpha1/features?labelSelector=instanceType={}".format(region, feature)
        return self.send(method='GET', path=path, params={})
