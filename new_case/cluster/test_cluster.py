# coding=utf-8
from new_case.cluster.cluster import Cluster
from common.settings import RERUN_TIMES, DEFAULT_NS, DEFAULT_LABEL
import pytest
from new_case.cluster.utils import get_node_info
import yaml


@pytest.mark.cluster_registry_controller_manager
@pytest.mark.BAT
@pytest.mark.acp_infra
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestCluster(object):
    def setup_class(self):
        self.cluster_client = Cluster()

    def teardown_class(self):
        node_info = node_infos[0]
        node_info['labels'].update({"update": None})
        self.cluster_client.update_node_labels(node_name=node_info["node_name"], data={"metadata": {"labels": node_info["labels"]}})

    def 测试获取主机列表不带参数(self):
        result = self.cluster_client.get_region_nodes()
        assert result.status_code == 200, "获取集群节点失败"
        global node_infos
        node_infos = get_node_info(result.json())
        values = self.cluster_client.generate_jinja_data("verify_data/cluster/node_list.jinja2", {"node_infos": node_infos})
        assert self.cluster_client.is_sub_dict(values, result.json()), \
            "获取集群节点比对数据失败，返回数据{}，期望数据{}".format(result.json(), values)

    def 测试获取主机详情(self):
        node_name = node_infos[0]["node_name"]
        result = self.cluster_client.get_node_detail(node_name=node_name)
        assert result.status_code == 200, "获取节点详情失败:{}".format(result.text)
        values = self.cluster_client.generate_jinja_data("verify_data/cluster/node_detail.jinja2", node_infos[0])
        assert self.cluster_client.is_sub_dict(values, result.json()), \
            "获取节点详情比对数据失败，返回数据{}，期望数据{}".format(result.json(), values)

    def 测试更新主机标签添加一个键值对(self):
        node_info = node_infos[0]
        node_name = node_info["node_name"]
        node_labels = node_info["labels"]
        node_labels.update({"update": "ares-labels"})
        result = self.cluster_client.update_node_labels(node_name=node_name, data={"metadata": {"labels": node_info["labels"]}})
        assert result.status_code == 200, "更新节点labels失败:{}".format(result.text)
        values = self.cluster_client.generate_jinja_data("verify_data/cluster/node_detail.jinja2", node_info)
        assert self.cluster_client.is_sub_dict(values, result.json()), \
            "更新主机标签比对数据失败，返回数据{}，期望数据{}".format(result.json(), values)

    def 测试将节点设置为不可调度(self):
        node_info = node_infos[0]
        node_name = node_info["node_name"]
        result = self.cluster_client.update_node_labels(node_name=node_name, data={"spec": {"unschedulable": True}})
        assert result.status_code == 200, "更新节点为不可调度失败:{}".format(result.text)
        node_info.update({"unschedulable": True})
        values = self.cluster_client.generate_jinja_data("verify_data/cluster/node_detail.jinja2", node_info)
        assert self.cluster_client.is_sub_dict(values, result.json()), \
            "更新主机为不可调度比对数据失败，返回数据{}，期望数据{}".format(result.json(), values)

    def 测试将节点设置为可调度(self):
        node_info = node_infos[0]
        node_name = node_info["node_name"]
        result = self.cluster_client.update_node_labels(node_name=node_name, data={"spec": {"unschedulable": False}})
        assert result.status_code == 200, "更新节点为可调度失败:{}".format(result.text)
        if "unschedulable" in node_info:
            node_info.pop("unschedulable")
        values = self.cluster_client.generate_jinja_data("verify_data/cluster/node_detail.jinja2", node_info)
        assert self.cluster_client.is_sub_dict(values, result.json()), \
            "更新主机为可调度比对数据失败，返回数据{}，期望数据{}".format(result.json(), values)
        assert "unschedulable" not in result.json()["spec"].keys(), "主机还是不可调度:{}".format(result.text)

    def 测试获取集群列表不带参数(self):
        result = self.cluster_client.get_region_list_or_detail()
        assert result.status_code == 200, "获取集群列表失败"
        content = self.cluster_client.get_k8s_resource_data(result.json(), self.cluster_client.region_name, list_key="items")
        cluster_info = {
            "namespace": DEFAULT_NS,
            "cluster_name": self.cluster_client.region_name,
            # "cidr": self.cluster_client.get_value(content, "spec.kubernetesApiEndpoints.serverEndpoints.0.clientCIDR"),
            "lb_addr": self.cluster_client.get_value(content, "spec.kubernetesApiEndpoints.serverEndpoints.0.serverAddress"),
            "display_name": self.cluster_client.get_value(content, "metadata#annotations#{}/display-name".format(DEFAULT_LABEL), delimiter='#'),
            "version": self.cluster_client.get_value(content, "status.version"),
            "default_label": DEFAULT_LABEL
        }
        value = yaml.safe_load(self.cluster_client.cluster_template.render(cluster_info))
        assert self.cluster_client.is_sub_dict(value, content), \
            "获取集群列表比对数据失败，返回数据{}，期望数据{}".format(result.json(), value)

    def 测试获取集群详情(self):
        result = self.cluster_client.get_region_list_or_detail(list=False)
        assert result.status_code == 200, "获取集群详情失败"
        cluster_info = {
            "namespace": DEFAULT_NS,
            "cluster_name": self.cluster_client.region_name,
            # "cidr": self.cluster_client.get_value(result.json(), "spec.kubernetesApiEndpoints.serverEndpoints.0.clientCIDR"),
            "lb_addr": self.cluster_client.get_value(result.json(), "spec.kubernetesApiEndpoints.serverEndpoints.0.serverAddress"),
            "display_name": self.cluster_client.get_value(result.json(), "metadata#annotations#{}/display-name".format(DEFAULT_LABEL), delimiter='#'),
            "version": self.cluster_client.get_value(result.json(), "status.version"),
            "default_label": DEFAULT_LABEL
        }
        value = yaml.safe_load(self.cluster_client.cluster_template.render(cluster_info))
        assert self.cluster_client.is_sub_dict(value, result.json()), \
            "获取集群详情比对数据失败，返回数据{}，期望数据{}".format(result.json(), value)
