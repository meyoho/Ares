# coding=utf-8
from new_case.cluster.cluster import Cluster
from common.settings import RERUN_TIMES, DEFAULT_NS, DEFAULT_LABEL
import pytest
import yaml


@pytest.mark.cluster_registry_controller_manager
@pytest.mark.Regression
@pytest.mark.acp_infra
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestGetRegion(object):
    def setup_class(self):
        self.cluster_client = Cluster()

    def 测试获取集群列表_有limit参数(self):
        result = self.cluster_client.get_region_list_with_limit()
        assert result.status_code == 200, "获取获取集群列表_有limit参数失败:{}".format(result.text)
        global region_name
        region_name = self.cluster_client.get_value(result.json(), "items.0.metadata.name")
        display_name = self.cluster_client.get_value(result.json(), "items#0#metadata#annotations#{}/display-name".format(DEFAULT_LABEL), delimiter='#')
        global cnt
        cnt = self.cluster_client.get_value(result.json(), "metadata.continue")
        content = self.cluster_client.get_k8s_resource_data(result.json(), region_name, list_key="items")
        cluster_info = {
            "namespace": DEFAULT_NS,
            "cluster_name": region_name,
            "display_name": display_name,
            "serverEndpoints": self.cluster_client.get_value(content, "spec.kubernetesApiEndpoints.serverEndpoints.0"),
            "version": self.cluster_client.get_value(content, "status.version"),
            "default_label": DEFAULT_LABEL
        }
        value = yaml.safe_load(self.cluster_client.cluster_template.render(cluster_info))
        assert self.cluster_client.is_sub_dict(value, content), \
            "获取集群列表比对数据失败，返回数据{}，期望数据{}".format(content, value)

    def 测试获取集群列表_有limit和continue参数(self):
        result = self.cluster_client.get_region_list_with_limit(cnt=cnt)
        assert result.status_code == 200, "获取获取集群列表_有limit参数失败:{}".format(result.text)
        region_name_continue = self.cluster_client.get_value(result.json(), "items.0.metadata.name")
        display_name = self.cluster_client.get_value(result.json(), "items#0#metadata#annotations#{}/display-name".format(DEFAULT_LABEL), delimiter='#')
        content = self.cluster_client.get_k8s_resource_data(result.json(), region_name_continue, list_key="items")
        cluster_info = {
            "namespace": DEFAULT_NS,
            "cluster_name": region_name_continue,
            "serverEndpoints": self.cluster_client.get_value(content, "spec.kubernetesApiEndpoints.serverEndpoints.0"),
            "display_name": display_name,
            "version": self.cluster_client.get_value(content, "status.version"),
            "default_label": DEFAULT_LABEL
        }
        value = yaml.safe_load(self.cluster_client.cluster_template.render(cluster_info))
        assert self.cluster_client.is_sub_dict(value, content), \
            "获取集群列表比对数据失败，返回数据{}，期望数据{}".format(content, value)
        assert region_name_continue != region_name, "分页数据相同，集群第一页数据:{},集群第二页数据:{}".format(region_name, region_name_continue)

    def 测试获取集群监控feature(self):
        result = self.cluster_client.get_region_feature("prometheus")
        assert result.status_code == 200, "获取集群监控feature失败:{}".format(result.text)
        values = self.cluster_client.generate_jinja_data("verify_data/cluster/prometheus_feature.jinja2", {"namespace": DEFAULT_NS})
        assert self.cluster_client.is_sub_dict(values, result.json()), \
            "获取集群feature对比数据失败。返回数据{}，期望数据{}".format(result.json(), values)
