# coding=utf-8
from new_case.cluster.cluster import Cluster
from common.settings import RERUN_TIMES
import pytest
from new_case.cluster.utils import get_node_info


@pytest.mark.cluster_registry_controller_manager
@pytest.mark.Regression
@pytest.mark.acp_infra
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestUpdateNode(object):
    def setup_class(self):
        self.cluster_client = Cluster()
        result = self.cluster_client.get_region_nodes()
        assert result.status_code == 200, "获取集群节点失败"
        self.node_infos = get_node_info(result.json())

    def 测试添加多个主机标签(self):
        node_info = self.node_infos[0]
        node_name = node_info["node_name"]
        node_labels = node_info["labels"]
        node_labels.update({"update1": "ares-labels1", "update2": "ares-labels2"})
        result = self.cluster_client.update_node_labels(node_name=node_name, data={"metadata": {"labels": node_info["labels"]}})
        assert result.status_code == 200, "更新节点labels失败:{}".format(result.text)
        values = self.cluster_client.generate_jinja_data("verify_data/cluster/node_detail.jinja2", node_info)
        assert self.cluster_client.is_sub_dict(values, result.json()), \
            "更新主机标签比对数据失败，返回数据{}，期望数据{}".format(result.json(), values)

    def 测试删除多个主机标签(self):
        node_info = self.node_infos[0]
        node_name = node_info["node_name"]
        node_labels = node_info["labels"]
        node_labels.update({"update1": None, "update2": None})
        result = self.cluster_client.update_node_labels(node_name=node_name, data={"metadata": {"labels": node_info["labels"]}})
        assert result.status_code == 200, "更新节点labels失败:{}".format(result.text)
        node_labels.pop("update1")
        node_labels.pop("update2")
        values = self.cluster_client.generate_jinja_data("verify_data/cluster/node_detail.jinja2", node_info)
        assert self.cluster_client.is_sub_dict(values, result.json()), \
            "更新主机标签比对数据失败，返回数据{}，期望数据{}".format(result.json(), values)
