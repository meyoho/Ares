import pytest
from time import sleep
from hashlib import md5
from new_case.platform.rolemanager import RoleTemplate, role_data
from common.settings import RERUN_TIMES, USERNAME, RESOURCE_PREFIX, DEFAULT_LABEL


@pytest.mark.auth_controller2
@pytest.mark.apollo
@pytest.mark.platform_api
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestRoleTemplateSuite(object):

    def setup_class(self):
        self.role_client = RoleTemplate()
        self.role_name = "{}-ares-role".format(RESOURCE_PREFIX)
        self.md5_email = md5(USERNAME.encode(encoding="utf-8")).hexdigest()
        self.role_with_permission = "{}-with-permission".format(RESOURCE_PREFIX)
        self.role_with_custome_resource = "{}-with-custome-resource".format(RESOURCE_PREFIX)
        self.role_update = "{}-role-update".format(RESOURCE_PREFIX)

        self.teardown_class(self)

    def teardown_class(self):
        self.role_client.delete_role(self.role_name)
        self.role_client.delete_role(self.role_with_permission)
        self.role_client.delete_role(self.role_with_custome_resource)
        self.role_client.delete_role(self.role_update)

    @pytest.mark.BAT
    def 测试获取角色管理列表(self):
        list_result = self.role_client.get_roletemplate_list()
        assert list_result.status_code == 200, "获取角色列表失败,响应结果：{}".format(list_result.content)

        assert "acp-platform-admin" in list_result.text, "平台管理员角色不在列表中"

    @pytest.mark.BAT
    @pytest.mark.parametrize("key", role_data)
    def 测试获取角色详情(self, key):
        name = role_data[key]["name"]
        detail_result = self.role_client.get_roletemplate_detail(name)
        assert detail_result.status_code == 200, "获取系统角色:{}失败,响应结果:{}".format(key, detail_result.content)
        assert name in detail_result.text, "角色名称字段不在详情数据:{}".format(detail_result.text)

    @pytest.mark.BAT
    def 测试创建无权限的自定义角色(self):
        data = {
            "role_name": self.role_name,
            "display_name": self.role_name,
            "md5_email": self.md5_email,
            "rules": [],
            "customRules": [],
            "creator": USERNAME
        }
        create_result = self.role_client.create_role("./test_data/rolemanager/create_role.jinja2", data)
        assert create_result.status_code == 201, "创建自定义角色失败,响应结果：{}".format(create_result.content)
        assert self.role_name in create_result.text, "角色名称字段不在创建返回数据:{}".format(create_result.text)
        ret = self.role_client.get_roletemplate_detail(self.role_name)
        content = ret.json()
        assert ret.status_code == 200, "获取自定义角色详情失败,响应结果:{}".format(ret.text)
        value = self.role_client.generate_jinja_data("verify_data/rolemanager/roletemplate.jinja2", data)
        value['metadata']['annotations']['{}/description'.format(DEFAULT_LABEL)] = ''
        assert self.role_client.is_sub_dict(value, content), \
            "自定义角色详情页数据对比失败，返回数据:{},期望数据:{}".format(content, value)

    @pytest.mark.BAT
    def 测试获取自定义角色详情(self):
        detail_result = self.role_client.get_roletemplate_detail(self.role_name)
        assert detail_result.status_code == 200, "获取自定义角色失败,响应结果:{}".format(detail_result.content)
        assert self.role_name in detail_result.text, "角色名称字段不在详情数据:{}".format(detail_result.text)

    @pytest.mark.BAT
    def 测试获取自定义角色的clusterroles(self):
        name = 'custom-{}'.format(self.role_name)
        ret = self.role_client.check_clusterroles('clusterroles', name)

        assert ret, "创建自定义角色时，未生成clusterroles"

    @pytest.mark.BAT
    def 测试删除自定义角色(self):
        del_result = self.role_client.delete_role(self.role_name)
        assert del_result.status_code == 200, "删除自定义角色失败,响应结果:{}".format(del_result.content)

    @pytest.mark.BAT
    def 测试创建分配权限的自定义角色(self):
        data = {
            "role_name": self.role_with_permission,
            "display_name": self.role_with_permission,
            "md5_email": self.md5_email,
            "creator": USERNAME,
            "rules": [
                {
                    "module": "acp",
                    "functionResourceRef": "app",
                    "verbs": [
                        "get",
                        "list",
                        "watch",
                        "create",
                        "update",
                        "patch"
                    ]
                },
                {
                    "module": "acp",
                    "functionResourceRef": "apphelm",
                    "verbs": [
                        "get",
                        "list",
                        "watch"
                    ]
                }
            ],
            "customRules": []
        }
        create_result = self.role_client.create_role("./test_data/rolemanager/create_role.jinja2", data)
        assert create_result.status_code == 201, "创建自定义角色失败,响应结果：{}".format(create_result.content)
        assert self.role_with_permission in create_result.text, "角色名称字段不在创建返回数据:{}" \
            .format(create_result.text)
        ret = self.role_client.get_roletemplate_detail(self.role_with_permission)
        content = ret.json()
        assert ret.status_code == 200, "获取自定义角色详情失败,响应结果:{}".format(ret.text)
        value = self.role_client.generate_jinja_data("verify_data/rolemanager/roletemplate.jinja2", data)
        value['metadata']['annotations']['{}/description'.format(DEFAULT_LABEL)] = ''
        assert self.role_client.is_sub_dict(value, content), \
            "自定义角色详情页数据对比失败，返回数据:{},期望数据:{}".format(content, value)

    @pytest.mark.BAT
    def 测试创建添加自定义资源的自定义角色(self):
        data = {
            "role_name": self.role_with_custome_resource,
            "creator": USERNAME,
            "display_name": self.role_with_custome_resource,
            "md5_email": self.md5_email,
            "rules": [
                {
                    "module": "acp",
                    "functionResourceRef": "app",
                    "verbs": [
                        "get",
                        "list",
                        "watch",
                        "create",
                        "update",
                        "patch"
                    ]
                }
            ],
            "customRules": [
                {
                    "apiGroup": "customresource",
                    "resources": "customresource",
                    "verbs": [
                        "get",
                        "list",
                        "watch",
                        "create"
                    ]
                }
            ]
        }
        create_result = self.role_client.create_role("./test_data/rolemanager/create_role.jinja2", data)
        assert create_result.status_code == 201, "创建自定义角色失败,响应结果：{}".format(create_result.content)
        assert self.role_with_custome_resource in create_result.text, "角色名称字段不在创建返回数据:{}" \
            .format(create_result.text)
        ret = self.role_client.get_roletemplate_detail(self.role_with_custome_resource)
        content = ret.json()
        assert ret.status_code == 200, "获取自定义角色详情失败,响应结果:{}".format(ret.text)
        value = self.role_client.generate_jinja_data("verify_data/rolemanager/roletemplate.jinja2", data)
        value['metadata']['annotations']['{}/description'.format(DEFAULT_LABEL)] = ''
        assert self.role_client.is_sub_dict(value, content), \
            "自定义角色详情页数据对比失败，返回数据:{},期望数据:{}".format(content, value)

    @pytest.mark.BAT
    def 测试更新角色的基本信息(self):
        data = {
            "role_name": self.role_update,
            "display_name": self.role_update,
            "md5_email": self.md5_email,
            "creator": USERNAME,
            "rules": [
                {
                    "module": "acp",
                    "functionResourceRef": "app",
                    "verbs": [
                        "get",
                        "list",
                        "watch",
                        "create",
                        "update",
                        "patch"
                    ]
                }
            ],
            "customRules": []
        }
        create_result = self.role_client.create_role("./test_data/rolemanager/create_role.jinja2", data)
        assert create_result.status_code == 201, "创建自定义角色失败,响应结果：{}".format(create_result.content)
        content = create_result.json()
        uid = self.role_client.get_value(content, 'metadata.uid')

        sleep(2)

        ret = self.role_client.get_roletemplate_detail(self.role_update)
        assert ret.status_code, "获取角色详情失败,响应结果: {}".format(ret.text)
        resourceVersion = self.role_client.get_value(ret.json(), 'metadata.resourceVersion')

        data['description'] = "测试更新角色基本信息"
        data['resourceVersion'] = resourceVersion
        data['uid'] = uid

        ret = self.role_client.update_role(self.role_update, "./test_data/rolemanager/update_role.jinja2", data)
        assert ret.status_code == 200, "更新角色基本信息失败，响应结果: {}".format(ret.text)

        ret = self.role_client.get_roletemplate_detail(self.role_update)
        content = ret.json()
        assert ret.status_code == 200, "获取自定义角色详情失败,响应结果:{}".format(ret.text)
        value = self.role_client.generate_jinja_data("verify_data/rolemanager/roletemplate.jinja2", data)
        assert self.role_client.is_sub_dict(value, content), \
            "自定义角色详情页数据对比失败，返回数据:{},期望数据:{}".format(content, value)

    @pytest.mark.BAT
    def 测试角色列表分页(self):
        ret = self.role_client.get_roletemplate_list(params={"limit": 3})
        assert ret.status_code == 200, "获取角色列表失败, 返回值: {}".format(ret.text)
        cont = self.role_client.get_value(ret.json(), 'metadata.continue')
        assert len(cont) > 1, "分页查询时，返回的continue字段的值为空"
        ret = self.role_client.get_roletemplate_list(params={"limit": 1, "continue": cont})
        assert ret.status_code == 200, "获取角色列表失败, 返回值: {}".format(ret.text)

    def 测试新建角色_名称的长度大于63(self):
        data = {
            "role_name": 'l'*64,
            "display_name": self.role_name,
            "md5_email": self.md5_email,
            "rules": [],
            "customRules": [],
            "creator": USERNAME
        }
        create_result = self.role_client.create_role("./test_data/rolemanager/create_role.jinja2", data)
        assert create_result.status_code == 422, "创建名称长度大于63的自定义角色本应失败,响应结果：{}".format(create_result.content)
        value = self.role_client.generate_jinja_data("verify_data/rolemanager/name_display_name_error.jinja2",
                                                     {"name": "l"*64})
        content = create_result.json()
        assert self.role_client.is_sub_dict(value, content), \
            "自定义角色详情页数据对比失败，返回数据:{},期望数据:{}".format(content, value)

    def 测试新建角色_显示名称的长度大于30(self):
        data = {
            "role_name": 'display_name_gt_30',
            "display_name": 'd'*31,
            "md5_email": self.md5_email,
            "rules": [],
            "customRules": [],
            "creator": USERNAME
        }
        create_result = self.role_client.create_role("./test_data/rolemanager/create_role.jinja2", data)
        assert create_result.status_code == 422, "创建显示名称长度大于30的自定义角色本应失败,响应结果：{}".format(create_result.content)
        value = self.role_client.generate_jinja_data("verify_data/rolemanager/name_display_name_error.jinja2",
                                                     {"name": "display_name_gt_30"})
        content = create_result.json()
        assert self.role_client.is_sub_dict(value, content), \
            "自定义角色详情页数据对比失败，返回数据:{},期望数据:{}".format(content, value)
