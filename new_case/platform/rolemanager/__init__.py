import sys
from time import sleep
from common.base_request import Common
from common.log import logger
from new_case.containerplatform.resourcemanage.resource import Resource


class RoleTemplate(Common):

    def get_role_template_url(self, name=""):
        return name and "apis/auth.alauda.io/v1beta1/roletemplates/{}".format(name) \
               or "apis/auth.alauda.io/v1beta1/roletemplates"

    def get_roletemplate_list(self, params=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        if params is None:
            params = {"limit": 20}
        path = self.get_role_template_url()
        return self.send(method="GET", path=path, params=params)

    def get_roletemplate_detail(self, name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        path = self.get_role_template_url(name)
        return self.send(method="GET", path=path)

    def create_role(self, file, data):
        """
        创建role
        :param file:
        :param data:
        :return: idp详情
        """
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        path = self.get_role_template_url()
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=path, json=data)

    def delete_role(self, name):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        path = self.get_role_template_url(name)
        return self.send(method="DELETE", path=path)

    def update_role(self, name, file, data):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        path = self.get_role_template_url(name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='put', path=path, json=data)

    def check_clusterroles(self, kind, name, namespace='', cycle=10):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        count = 0
        while count < cycle:
            count += 1
            ret = Resource().search_resources(namespace, kind, name)
            logger.info("根据资源名称过滤clusterroles的结果: {}".format(ret.text))
            if name in ret.text:
                return True
            sleep(3)
        return False


role_data = {
    "平台管理员": {"name": "acp-platform-admin"},
    "平台审计人员": {"name": "acp-platform-auditor"},
    "项目管理员": {"name": "acp-project-admin"},
    "命名空间管理员": {"name": "acp-namespace-admin"},
    "开发人员": {"name": "acp-namespace-developer"}
}
