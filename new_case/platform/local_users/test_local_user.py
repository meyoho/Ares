import pytest
import base64
from hashlib import md5
from new_case.platform.local_users.local_users import LocalUsers
from new_case.platform.permission.user import User
from common.settings import RERUN_TIMES, get_token
from common.log import logger

local_user = LocalUsers()


@pytest.mark.apollo
@pytest.mark.platform_api
@pytest.mark.auth_controller2
@pytest.mark.dex
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestLocalUsers(object):
    def setup_class(self):
        self.user = User()
        self.user_name = 'local-user-test'
        self.user_email = 'local@user.test'
        self.user_pwd = 'abc123'
        self.new_pwd = '123abc'
        self.update_display_name = 'update display name'
        self.md5_name = md5(self.user_email.encode(encoding='utf-8')).hexdigest()
        self.data = {
            "md5_name": self.md5_name,
            "email": self.user_email,
            "username": self.user_name,
            "account": self.user_name,
            "password": str(base64.b64encode(self.user_pwd.encode(encoding='utf-8')), 'utf-8')
        }

    def teardown_class(self):
        local_user.delete_user(self.md5_name)

    def 测试创建本地用户(self):
        ret = local_user.create_users('test_data/local_user/local_user_create.jinja2', data=self.data)
        assert ret.status_code == 201, "创建本地用户失败，响应结果: {}".format(ret.text)

        value = local_user.generate_jinja_data('verify_data/local_user/local_user_create_response.jinja2',
                                               data=self.data)
        assert local_user.is_sub_dict(value, ret.json()), "创建本地用户，返回数据比对失败，预期值: {}, 实际值: {}"\
            .format(value, ret.json())

    def 测试更新本地用户显示名称(self):
        data = {
            "username": self.update_display_name
        }
        ret = local_user.common_operation(self.md5_name, 'test_data/local_user/local_user_common.jinja2', data=data)
        assert ret.status_code == 200, "更新本地用户显示名称失败，响应结果: {}".format(ret.text)

        user_detail = local_user.get_user_detail(self.md5_name)
        assert user_detail.status_code == 200, "获取用户详情失败, 响应结果: {}".format(user_detail.text)

        self.data['username'] = self.update_display_name
        value = local_user.generate_jinja_data('verify_data/local_user/local_user_create_response.jinja2',
                                               data=self.data)

        assert local_user.is_sub_dict(value, user_detail.json()), "用户详情数据比对失败，预期结果: {}, 实际结果: {}"\
            .format(value, user_detail.json())

    def 测试重置本地用户的密码(self):
        data = {
            "password": str(base64.b64encode(self.new_pwd.encode(encoding='utf-8')), 'utf-8')
        }

        ret = local_user.common_operation(self.md5_name, 'test_data/local_user/local_user_common.jinja2', data=data)
        assert ret.status_code == 200, "重置本地用户的密码失败，响应结果: {}".format(ret.text)

        try:
            login_ret = get_token(username=self.user_email, password=self.user_pwd)
        except Exception as e:
            if str(e) != 'get token info failed':
                assert False, "验证重置用户密码后，不能使用重置前的密码登录失败"
            else:
                pass
        else:
            assert isinstance(login_ret, tuple), "重置用户密码后，还能使用之前的密码登录"

        login_ret2 = get_token(username=self.user_email, password=self.new_pwd)
        assert isinstance(login_ret2, tuple), "重置用户密码后，使用重置后的密码登录失败"

    def 测试本地用户使用用户名登录(self):
        ret = get_token(username=self.user_name, password=self.new_pwd)
        assert isinstance(ret, tuple), "本地用户使用用户名登录平台失败, 响应结果: {}".format(ret)

    def 测试更新本地用户的密码(self):
        data = {
            "password": str(base64.b64encode(self.user_pwd.encode(encoding='utf-8')), 'utf-8'),
            "old_password": str(base64.b64encode(self.new_pwd.encode(encoding='utf-8')), 'utf-8')
        }
        ret = local_user.common_operation(self.md5_name, 'test_data/local_user/local_user_common.jinja2', data=data)
        assert ret.status_code == 200, "更新本地用户的密码失败，响应结果: {}".format(ret.text)
        try:
            login_ret = get_token(username=self.user_email, password=self.new_pwd)
        except Exception as e:
            if str(e) != 'get token info failed':
                assert False, "验证更新用户密码后，不能使用更新前的密码登录失败"
            else:
                pass
        else:
            assert isinstance(login_ret, tuple), "更新用户密码后，还能使用之前的密码登录"

        login_ret2 = get_token(username=self.user_email, password=self.user_pwd)
        assert isinstance(login_ret2, tuple), "更新用户密码后，使用更新后的密码登录失败"

    def 测试创建本地用户邮箱格式不正确(self):
        self.data['email'] = 'zzz'
        ret = local_user.create_users('test_data/local_user/local_user_create.jinja2', data=self.data)
        assert ret.status_code == 500, "邮箱格式不正确时创建本地用户本应该失败，响应结果: {}".format(ret.text)

        data = {'message': "email need satify regex"}
        value = local_user.generate_jinja_data('verify_data/local_user/local_user_create_error_response.jinja2',
                                               data=data)
        assert local_user.is_sub_dict(value, ret.json()), "创建本地用户，返回数据比对失败，预期值: {}, 实际值: {}"\
            .format(value, ret.json())

    @pytest.mark.skip(reason='http://jira.alauda.cn/browse/AIT-2354')
    def 测试创建本地用户_显示名称长度大于64(self):
        self.data['email'] = self.user_email
        self.data['username'] = 'd'*65
        ret = local_user.create_users('test_data/local_user/local_user_create.jinja2', data=self.data)
        assert ret.status_code == 500, "显示名称长度大于64位时创建本地用户本应该失败，响应结果: {}".format(ret.text)

    def 测试创建相同名称的本地用户(self):
        self.data['email'] = 'new@email.io'
        self.data['username'] = self.user_name
        ret = local_user.create_users('test_data/local_user/local_user_create.jinja2', data=self.data)
        assert ret.status_code == 500, "创建相同名称的本地用户本应该失败，响应结果: {}".format(ret.text)

        data = {
            'message': "Users 'new@email.io' is already exists"
        }
        value = local_user.generate_jinja_data('verify_data/local_user/local_user_create_error_response.jinja2',
                                               data=data)
        assert local_user.is_sub_dict(value, ret.json()), "创建本地用户，返回数据比对失败，预期值: {}, 实际值: {}"\
            .format(value, ret.json())

    def 测试创建相同邮箱的本地用户(self):
        self.data['email'] = self.user_email
        self.data['account'] = 'newusertest'
        ret = local_user.create_users('test_data/local_user/local_user_create.jinja2', data=self.data)
        assert ret.status_code == 500, "创建相同邮箱的本地用户本应该失败，响应结果: {}".format(ret.text)

        data = {
            'message': "Users '{}' is already exists".format(self.user_email)
        }
        value = local_user.generate_jinja_data('verify_data/local_user/local_user_create_error_response.jinja2',
                                               data=data)
        assert local_user.is_sub_dict(value, ret.json()), "创建本地用户，返回数据比对失败，预期值: {}, 实际值: {}"\
            .format(value, ret.json())

    def 测试删除本地用户(self):
        ret = local_user.delete_user(self.md5_name)
        assert ret.status_code == 200, "删除本地用户失败，响应结果: {}".format(ret.text)

        ret = self.user.filter_user(self.user_email)
        logger.info("用户过滤结果: {}".format(ret.text))
        assert self.user_email not in ret.text, "删除本地用户失败, 响应结果: {}".format(ret.text)
