from common.base_request import Common


class LocalUsers(Common):
    def _common_url(self, name=''):
        return name and "auth/v1/users/{}".format(name) or "auth/v1/users"

    def create_users(self, file, data):
        path = self._common_url()
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=path, json=data)

    def delete_user(self, name):
        path = self._common_url(name=name)
        return self.send(method='DELETE', path=path)

    def get_user_detail(self, name):
        path = self._common_url(name)
        return self.send(method='GET', path=path)

    def common_operation(self, name, file, data):
        """
        1.reset or update user's password
        2.update user's display name
        """
        path = self._common_url(name=name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='PATCH', path=path, json=data)

    def featuregates_is_open(self, feature_name, expected_status=True):
        path = 'fg/v1/featuregates'
        ret = self.send(method='GET', path=path)
        items = ret.json()['items']
        for item in items:
            if self.get_value(item, 'metadata.name') == feature_name and \
                    self.get_value(item, 'status.enabled') == expected_status:
                return True
        return False
