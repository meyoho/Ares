import sys
from common.base_request import Common
from common.log import logger


class Idp(Common):

    def create_idp(self, file, data, timeout=20):
        """
        创建idp
        :param file:
        :param data:
        :return: idp详情
        """
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        path = 'auth/v1/connectors'
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=path, json=data, timeout=timeout)

    def delete_idp(self, idp_name):
        """
        删除idp
        :param idp_name: idp名称
        :return:
        """
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        path = 'apis/dex.coreos.com/v1/namespaces/{}/connectors/{}'.format(
            self.default_ns, idp_name)
        return self.send(method='DELETE', path=path)

    def delete_idp_senior(self, idp_name, delete_users='false'):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        path = 'auth/v1/connectors/{}?delete-users={}'.format(idp_name, delete_users)
        return self.send(method='DELETE', path=path)

    def get_idp_list(self, params=None):
        """
        获取idp列表
        :return: idp列表
        """
        if params is None:
            params = {"limit": 20}
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        path = 'apis/dex.coreos.com/v1/namespaces/{}/connectors'.format(
            self.default_ns)
        return self.send(path=path, method='GET', params=params)

    def get_idp_detail(self, idp_name):
        """
        获取idp详情信息
        :param idp_name: idp名称
        :return: idp详情
        """
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        path = 'apis/dex.coreos.com/v1/namespaces/{}/connectors/{}'.format(
            self.default_ns, idp_name)
        return self.send(path=path, method='GET')

    def sync_ldap_and_local(self, user_source=None):
        """
        同步本地用户和ldap用户
        :param user_source: 用户来源，ldap或local， all代表全部
        :return: 同步状态
        """
        if user_source is None:
            user_source = {"type": "all"}
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        path = "auth/v1/users/sync?type={}".format(user_source)
        return self.send(method='PUT', path=path, json={}, timeout=60)

    def update_idp(self, file, data, idp_name, timeout=20):
        """
        更新idp
        :param file:
        :param data:
        :param idp_name: idp名称
        :return: idp
        """
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        path = "auth/v1/connectors/{}".format(idp_name)
        data = self.generate_jinja_data(file, data)
        return self.send(method='PUT', path=path, json=data, timeout=timeout)
