import pytest
import base64
import json
from common import settings
from common.settings import RERUN_TIMES
from new_case.platform.idp.idp import Idp
from new_case.platform.permission.user import User
from common.check_status import checker


@pytest.mark.skipif(not checker.ldap_available(), reason="LDAP不可用")
@pytest.mark.apollo
@pytest.mark.platform_api
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestIDPSuite(object):
    def setup_class(self):
        self.idp = Idp()
        self.user = User()
        self.idap_name = "{}-idp-ldap-test".format(settings.RESOURCE_PREFIX)
        self.ldap_without_group = "{}-ldap-without-group".format(settings.RESOURCE_PREFIX)
        self.oidc_name = "{}-idp-oidc-test".format(settings.RESOURCE_PREFIX)

    def teardown_class(self):
        self.idp.delete_idp(self.idap_name)
        self.idp.delete_idp(self.ldap_without_group)
        self.idp.delete_idp(self.oidc_name)

    @pytest.mark.BAT
    @pytest.mark.prepare
    def 测试创建_LDAP_IDP(self):
        data = {
            "description": "idp测试",
            "idp_name": self.idap_name,
            "ldap_host": settings.LDAP_HOST,
            "ldap_admin": "cn=admin,dc=alauda,dc=com",
            "ldap_password": "admin",
            "idp_display_name": "idp-ldap测试",
            "verify_username": "project@alauda.io",
            "namespace": self.idp.default_ns,
            "verify_password": "123456",
            "type": "ldap",
            "group_search": True
        }
        create_idp = self.idp.create_idp('test_data/idp/create_idp.jinja2', data)
        assert create_idp.status_code == 200, "创建idp失败, \n{}".format(create_idp.text)

        ldap_detail = self.idp.get_idp_detail(self.idap_name)
        assert ldap_detail.status_code == 200, "获取ldap详情失败，返回内容: {}".format(ldap_detail.text)

        content = ldap_detail.json()
        config = content['config']

        decodedBytes = base64.b64decode(config)
        decodedStr = str(decodedBytes, "utf-8")
        content['config'] = json.loads(decodedStr)

        verify_data = self.idp.generate_jinja_data('verify_data/idp/create_idp.jinja2', data)
        assert self.idp.is_sub_dict(verify_data, content), "ldap详情页的数据与预期的不一致，预期值: {}, 实际值: {}"\
            .format(verify_data, content)
        assert settings.get_token(data['idp_name'], data['verify_username'], data['verify_password']), \
            "idp创建测试失败，idp的用户登录报错"

    def 测试创建ldap_名称长度大于60(self):
        data = {
            "description": "idp测试",
            "idp_name": "l"*61,
            "ldap_host": settings.LDAP_HOST,
            "ldap_admin": "cn=admin,dc=alauda,dc=com",
            "ldap_password": "admin",
            "idp_display_name": "l"*61,
            "verify_username": "project@alauda.io",
            "namespace": self.idp.default_ns,
            "verify_password": "123456",
            "type": "ldap",
            "group_search": True
        }
        create_idp = self.idp.create_idp('test_data/idp/create_idp.jinja2', data)
        assert create_idp.status_code == 500, "创建名称长度大于60位的ldap本应该失败, \n{}".format(create_idp.text)

    def 测试创建ldap_不添加组设置(self):
        data = {
            "description": "idp测试",
            "idp_name": self.ldap_without_group,
            "ldap_host": settings.LDAP_HOST,
            "ldap_admin": "cn=admin,dc=alauda,dc=com",
            "ldap_password": "admin",
            "idp_display_name": "idp-ldap测试",
            "verify_username": "project@alauda.io",
            "namespace": self.idp.default_ns,
            "verify_password": "123456",
            "type": "ldap",
        }
        create_idp = self.idp.create_idp('test_data/idp/create_idp.jinja2', data)
        assert create_idp.status_code == 200, "创建ldap失败, \n{}".format(create_idp.text)

        ldap_detail = self.idp.get_idp_detail(self.ldap_without_group)
        assert ldap_detail.status_code == 200, "获取ldap详情失败，返回内容: {}".format(ldap_detail.text)

        content = ldap_detail.json()
        config = content['config']

        decodedBytes = base64.b64decode(config)
        decodedStr = str(decodedBytes, "utf-8")
        content['config'] = json.loads(decodedStr)

        verify_data = self.idp.generate_jinja_data('verify_data/idp/create_idp.jinja2', data)
        assert self.idp.is_sub_dict(verify_data, content), "ldap详情页的数据与预期的不一致，预期值: {}, 实际值: {}"\
            .format(verify_data, content)

    def 测试创建ldap_错误格式的过滤条件(self):
        data = {
            "description": "idp测试",
            "idp_name": "error-filter",
            "ldap_host": settings.LDAP_HOST,
            "ldap_admin": "cn=admin,dc=alauda,dc=com",
            "ldap_password": "admin",
            "idp_display_name": "idp-ldap测试",
            "verify_username": "project@alauda.io",
            "namespace": self.idp.default_ns,
            "verify_password": "123456",
            "type": "ldap",
            "filter": "mail=*.*"
        }
        create_idp = self.idp.create_idp('test_data/idp/create_idp.jinja2', data)
        assert create_idp.status_code == 500, "创建错误格式过滤条件的ldap本应该失败, \n{}".format(create_idp.text)

    @pytest.mark.BAT
    @pytest.mark.prepare
    def 测试创建_OIDC_IDP(self):
        data = {
            "description": "idp测试",
            "idp_name": self.oidc_name,
            "idp_display_name": "keycloak",
            "type": "oidc",
            "issuer_url": settings.OIDC_ISSUER_URL,
            "redirecturi": "{}/dex/callback".format(settings.API_URL),
            "clientsecret": settings.OIDC_SECRET_ID,
            "namespace": self.idp.default_ns,
            "verify_username": "admin",
            "verify_password": "admin",
        }
        create_idp = self.idp.create_idp('test_data/idp/create_idp.jinja2', data, timeout=60)
        assert create_idp.status_code == 200, "创建idp失败, \n{}".format(create_idp.text)

        oidc_detail = self.idp.get_idp_detail(self.oidc_name)
        assert oidc_detail.status_code == 200, "获取oidc详情失败，返回内容: {}".format(oidc_detail.text)

        content = oidc_detail.json()
        config = content['config']

        decodedBytes = base64.b64decode(config)
        decodedStr = str(decodedBytes, "utf-8")
        content['config'] = json.loads(decodedStr)

        verify_data = self.idp.generate_jinja_data('verify_data/idp/create_idp.jinja2', data)
        assert self.idp.is_sub_dict(verify_data, content), "oidc详情页的数据与预期的不一致，预期值: {}, 实际值: {}"\
            .format(verify_data, content)

    @pytest.mark.BAT
    @pytest.mark.upgrade
    def 测试同步全部用户(self):
        sync_user = self.idp.sync_ldap_and_local()
        assert sync_user.status_code == 200, "同步用户失败，\n{}".format(sync_user.text)
        values = self.idp.generate_jinja_data("verify_data/idp/sync_user.jinja2")
        assert self.idp.is_sub_dict(values, sync_user.json()), \
            "同步全部用户比对数据失败，返回数据:{},期望数据:{}".format(sync_user.json(), values)
        user_list = self.user.get_user_list_or_detail_senior()
        assert user_list.status_code == 200, "获取用户列表失败, \n{}".format(user_list.text)
        users = self.user.get_value_list(user_list.json()['items'], 'spec.email')
        # expect_list = {'platform@alauda.io', 'auditor@alauda.io', 'namespace@alauda.io', 'project@alauda.io'}
        expect_list = {'project@alauda.io'}
        assert expect_list.issubset(users), "用户同步失败，用户列表数据不全, 预期: {}, 实际: {}".format(expect_list, users)

    @pytest.mark.BAT
    @pytest.mark.upgrade
    def 测试同步本地用户(self):
        sync_user = self.idp.sync_ldap_and_local(user_source={"type": "local"})
        assert sync_user.status_code == 200, "同步用户失败，\n{}".format(sync_user.text)
        values = self.idp.generate_jinja_data("verify_data/idp/sync_user.jinja2")
        assert self.idp.is_sub_dict(values, sync_user.json()), \
            "同步本地用户比对数据失败，返回数据:{},期望数据:{}".format(sync_user.json(), values)

    @pytest.mark.BAT
    @pytest.mark.upgrade
    def 测试同步ldap用户(self):
        sync_user = self.idp.sync_ldap_and_local(user_source={"type": "ldap"})
        assert sync_user.status_code == 200, "同步用户失败，\n{}".format(sync_user.text)
        values = self.idp.generate_jinja_data("verify_data/idp/sync_user.jinja2")
        assert self.idp.is_sub_dict(values, sync_user.json()), \
            "同步ldap用户比对数据失败，返回数据:{},期望数据:{}".format(sync_user.json(), values)

    @pytest.mark.BAT
    @pytest.mark.upgrade
    def 测试更新_LDAP_IDP(self):
        data = {
            "description": "idp测试",
            "idp_name": self.idap_name,
            "ldap_host": settings.LDAP_HOST,
            "ldap_admin": "cn=admin,dc=alauda,dc=com",
            "ldap_password": "admin",
            "idp_display_name": "idp-ldap更新测试",
            "namespace": self.idp.default_ns,
            "verify_username": "project@alauda.io",
            "verify_password": "123456",
            "type": "ldap"
        }
        update_idp = self.idp.update_idp('test_data/idp/create_idp.jinja2', data, data['idp_name'])
        assert update_idp.status_code == 200, "更新ldap失败, \n{}".format(update_idp.text)

        ldap_detail = self.idp.get_idp_detail(self.idap_name)
        assert ldap_detail.status_code == 200, "获取ldap详情失败，返回内容: {}".format(ldap_detail.text)

        content = ldap_detail.json()
        config = content['config']

        decodedBytes = base64.b64decode(config)
        decodedStr = str(decodedBytes, "utf-8")
        content['config'] = json.loads(decodedStr)

        verify_data = self.idp.generate_jinja_data('verify_data/idp/create_idp.jinja2', data)
        assert self.idp.is_sub_dict(verify_data, content), "ldap详情页的数据与预期的不一致，预期值: {}, 实际值: {}" \
            .format(verify_data, content)

    @pytest.mark.BAT
    @pytest.mark.upgrade
    def 测试更新_OIDC_IDP(self):
        data = {
            "description": "idp测试",
            "idp_name": self.oidc_name,
            "idp_display_name": "keycloak更新测试",
            "type": "oidc",
            "issuer_url": "{}".format(settings.OIDC_ISSUER_URL),
            "redirecturi": "{}/dex/callback".format(settings.API_URL),
            "clientsecret": settings.OIDC_SECRET_ID,
            "namespace": self.idp.default_ns,
            "verify_username": "admin",
            "verify_password": "admin",
        }
        update_idp = self.idp.update_idp('test_data/idp/create_idp.jinja2', data, data['idp_name'], timeout=60)
        assert update_idp.status_code == 200, "更新oidc失败, \n{}".format(update_idp.text)

        oidc_detail = self.idp.get_idp_detail(self.oidc_name)
        assert oidc_detail.status_code == 200, "获取oidc详情失败，返回内容: {}".format(oidc_detail.text)

        content = oidc_detail.json()
        config = content['config']

        decodedBytes = base64.b64decode(config)
        decodedStr = str(decodedBytes, "utf-8")
        content['config'] = json.loads(decodedStr)

        verify_data = self.idp.generate_jinja_data('verify_data/idp/create_idp.jinja2', data)
        assert self.idp.is_sub_dict(verify_data, content), "oidc详情页的数据与预期的不一致，预期值: {}, 实际值: {}" \
            .format(verify_data, content)

    @pytest.mark.BAT
    @pytest.mark.upgrade
    def 测试获取IDP列表(self):
        idp_list_response = self.idp.get_idp_list()
        assert idp_list_response.status_code == 200, "获取idp列表失败:{}".format(idp_list_response.text)
        idp_list = self.idp.get_value_list(idp_list_response.json()['items'], 'id')
        assert {self.idap_name, self.oidc_name}.issubset(idp_list), "创建的idp不在idp列表中"

    @pytest.mark.upgrade
    def 测试获取IDP列表_有limit和continue参数(self):
        ret = self.idp.get_idp_list(params={"limit": 1})
        assert ret.status_code == 200, "获取idp列表失败:{}".format(ret.text)
        continues = self.idp.get_value(ret.json(), "metadata.continue")
        ret_cnt = self.idp.get_idp_list(params={"limit": 1, "continue": continues})
        assert ret_cnt.status_code == 200, "获取访问规则列表失败:{}".format(ret_cnt.text)
        assert ret.json() != ret_cnt.json(), "分页数据相同，第一页数据:{},第二页数据:{}".format(ret.json(), ret_cnt.json())

    @pytest.mark.BAT
    @pytest.mark.delete
    def 测试删除_ldap_idp(self):
        delete_idp = self.idp.delete_idp(self.idap_name)
        assert delete_idp.status_code == 200, "删除ldap idp失败, \n{}".format(delete_idp.text)

    @pytest.mark.BAT
    @pytest.mark.delete
    def 测试删除_oidc_idp(self):
        delete_idp = self.idp.delete_idp(self.oidc_name)
        assert delete_idp.status_code == 200, "删除ldap idp失败, \n{}".format(delete_idp.text)


@pytest.mark.apollo
@pytest.mark.platform_api
@pytest.mark.skipif(settings.AUDIT_UNABLED, reason="do not have audit")
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class notTestIDPAuditL1Suite(object):
    def setup_class(self):
        self.idp = Idp()
        self.idap_name = "{}-idp-ldap-test".format(settings.RESOURCE_PREFIX)

    def 测试IDP创建审计(self):
        payload = {"user_name": settings.USERNAME, "operation_type": "create", "resource_type": "connectors",
                   "resource_name": ""}
        result = self.idp.search_audit(payload)
        payload.update(
            {"namespace": settings.DEFAULT_NS, "region_name": settings.GLOBAL_REGION_NAME, "resource_name": " "})
        values = self.idp.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.idp.is_sub_dict(values, result.json()), "审计数据不符合预期"

    def 测试IDP更新审计(self):
        payload = {"user_name": settings.USERNAME, "operation_type": "update", "resource_type": "connectors",
                   "resource_name": self.idap_name}
        result = self.idp.search_audit(payload)
        payload.update({"namespace": settings.DEFAULT_NS, "region_name": settings.GLOBAL_REGION_NAME})
        values = self.idp.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.idp.is_sub_dict(values, result.json()), "审计数据不符合预期"

    def 测试IDP删除审计(self):
        payload = {"user_name": settings.USERNAME, "operation_type": "delete", "resource_type": "connectors",
                   "resource_name": self.idap_name}
        result = self.idp.search_audit(payload)
        payload.update({"namespace": settings.DEFAULT_NS, "region_name": settings.GLOBAL_REGION_NAME})
        values = self.idp.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.idp.is_sub_dict(values, result.json()), "审计数据不符合预期"
