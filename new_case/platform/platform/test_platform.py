import pytest
import yaml
from common.settings import RERUN_TIMES, USERNAME, AUDIT_UNABLED, K8S_NAMESPACE, PROJECT_NAME, REGION_NAME
from new_case.platform.platform.platform import Platform


@pytest.mark.apollo
@pytest.mark.BAT
@pytest.mark.platform_api
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestPlatformSuite(object):
    senior_data = {
        "全部参数": {
            "action": "create",
            "k8s_resource": "namespaces",
            "group": "",
            "namespace_name": K8S_NAMESPACE,
            "project_name": PROJECT_NAME,
            "cluster": REGION_NAME,
            "allow_status": "true"
        },
        "项目为空": {
            "action": "create",
            "k8s_resource": "namespaces",
            "group": "",
            "namespace_name": K8S_NAMESPACE,
            "cluster": REGION_NAME,
            "allow_status": "true"
        },
        "集群为空": {
            "action": "create",
            "k8s_resource": "namespaces",
            "group": "",
            "namespace_name": K8S_NAMESPACE,
            "project_name": PROJECT_NAME,
            "allow_status": "true"
        }
    }

    def setup(self):
        self.platform = Platform()

    def 测试获取产品列表(self):
        product_list = self.platform.get_product_list()
        assert product_list.status_code == 200, "获取产品列表失败，{}".format(product_list.text)
        value = self.platform.generate_jinja_data('verify_data/platform/product_list.jinja2')
        assert self.platform.is_sub_dict(value, product_list.json())

    def 测试资源鉴权标准API(self):
        data = {
            "action": "get",
            "k8s_resource": "views",
            "resource_name": "projectview",
            "group": "auth.alauda.io",
            "allow_status": "true"
        }
        ra = self.platform.resourceAuthentication('test_data/platform/resourceauthorization.jinja2', data)
        assert ra.status_code == 201, "资源鉴权失败，{}".format(ra.text)
        verify_data = self.platform.generate_jinja_template('verify_data/platform/resourceAuthorization.jinja2')
        assert self.platform.is_sub_dict(yaml.safe_load(verify_data.render(data)), ra.json())

    @pytest.mark.parametrize("key", senior_data)
    def 测试资源鉴权高级API(self, key):
        data = self.senior_data[key]
        ra = self.platform.resourceAuthentication_senior('test_data/platform/resourceauthorization_senior.jinja2', data)
        assert ra.status_code == 201, "资源鉴权失败，{}".format(ra.text)
        verify_data = self.platform.generate_jinja_template('verify_data/platform/resourceAuthorization_senior.jinja2')
        assert self.platform.is_sub_dict(yaml.safe_load(verify_data.render(data)), ra.json())


@pytest.mark.apollo
@pytest.mark.platform_api
@pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestAccessAuditL1Suite(object):
    def setup_class(self):
        self.platform = Platform()

    def 测试鉴权创建审计(self):
        payload = {"user_name": USERNAME, "operation_type": "create", "resource_type": "selfsubjectaccessreviews",
                   "resource_name": ""}
        result = self.platform.search_audit(payload)
        payload.update({"namespace": "", "region_name": "", "code": 201, "resource_name": " "})
        values = self.platform.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.platform.is_sub_dict(values, result.json()), "审计数据不符合预期"
