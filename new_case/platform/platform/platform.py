import sys

from common.base_request import Common
from common.log import logger


class Platform(Common):

    def get_product_list(self):
        """
        获取产品列表， 默认acp asm platform devops均已安装
        :return: 返回产品列表
        """
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        path = "apis/portal.alauda.io/v1alpha1/alaudaproducts"
        return self.send(method='GET', path=path)

    def resourceAuthentication(self, file=None, data=None, resourceauthorization=False, headers=None, region_name=None):
        """
        资源鉴权标准API, 鉴权global集群的资源
        :param region_name: 业务集群名称
        :param headers: 重写token
        :param resourceauthorization: 判断是否是权限测试
        :param file:
        :param data:
        :return: 用户是否有权限，status 判断是否有权限
        """
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        if region_name is None:
            path = "apis/authorization.k8s.io/v1/selfsubjectaccessreviews"
        else:
            path = "kubernetes/{}/apis/authorization.k8s.io/v1/selfsubjectaccessreviews".format(region_name)
        if resourceauthorization is False:
            data = self.generate_jinja_data(file, data)
        return self.send(method='POST', path=path, json=data, headers=headers)

    def resourceAuthentication_senior(self, file, data):
        """
        资源鉴权高级API
        :param file:
        :param data:
        :return: 用户是否有权限，status 判断是否有权限
        """
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        path = "auth/v1/selfsubjectaccessreviews"
        data = self.generate_jinja_data(file, data)
        return self.send(method='POST', path=path, json=data)
