import sys
import pytest
from common.base_request import Common
from common.log import logger
from common.settings import get_token, RERUN_TIMES


class Refresh(Common):

    def refresh_token(self, console_path='console-acp', refresh_token=None):
        logger.info(sys._getframe().f_code.co_name.center(50, '*'))
        url = '{}/api/v1/token/refresh?refresh_token={}'.format(
            console_path, refresh_token)
        return self.send(method='get', path=url, headers={})


@pytest.mark.dex
@pytest.mark.platform_api
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestRefreshToken(object):

    def setup_class(self):
        self.prev_refresh_token = get_token()[1]
        self.refresh = Refresh()

    def 测试刷新token(self):
        ret = self.refresh.refresh_token(refresh_token=self.prev_refresh_token)
        content = ret.json()
        assert ret.status_code == 200, '重新刷新token请求失败'
        assert 'refresh_token' in content and len(content['refresh_token']) > 0, '重新刷新token后，返回信息中缺少refresh_token字段'
        assert 'id_token' in content and len(content['id_token']) > 0, '重新刷新token后，返回信息中缺少id_token字段'
        assert 'token_type' in content and len(content['token_type']) > 0, '重新刷新token后，返回信息中缺少token_type字段'

    def 测试错误的refresh_token(self):
        ret = self.refresh.refresh_token(refresh_token='p5ckqp745mvs2dlzwrwx45cv2')
        assert ret.status_code == 500, '使用错误的refresh_token刷新token应该请求失败，实际：{}   {}'.format(ret.status_code, ret.text)

    def 测试已使用过的refresh_token(self):
        ret = self.refresh.refresh_token(refresh_token=self.prev_refresh_token)
        assert ret.status_code == 500, '使用已使用过的refresh_token刷新token应该请求失败，实际：{}   {}'.format(ret.status_code, ret.text)
