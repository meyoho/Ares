import pytest
from new_case.platform.project.project import Project, data_list
from common import settings
from time import sleep


@pytest.mark.agon
@pytest.mark.apollo
@pytest.mark.platform_api
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestProjectSuite(object):
    def setup_class(self):
        self.project = Project()

    def 测试更新项目quota为空(self):
        sleep(3)
        data = data_list[0]
        update_project_result = self.project.update_project(data["project_name"], "./test_data/project/update_project.jinja2", data)
        assert update_project_result.status_code == 200, "更新项目失败,响应结果：{}".format(update_project_result.content)

        value = self.project.generate_jinja_data("verify_data/project/create_response.jinja2", data)
        assert self.project.is_sub_dict(value, update_project_result.json()), \
            "更新项目比对数据失败，返回数据:{},期望数据:{}".format(update_project_result.json(), value)

    @pytest.mark.skipif(settings.AUDIT_UNABLED, reason="do not have audit")
    def 测试项目打补丁审计(self):
        payload = {"user_name": settings.USERNAME, "operation_type": "patch", "resource_type": "projects",
                   "resource_name": data_list[0]['project_name']}
        result = self.project.search_audit(payload)
        payload.update({"namespace": "", "region_name": settings.GLOBAL_REGION_NAME})
        values = self.project.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.project.is_sub_dict(values, result.json()), "审计数据不符合预期"
