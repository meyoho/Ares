import pytest
from time import sleep

from common.settings import REGION_NAME, RERUN_TIMES, RESOURCE_PREFIX
from new_case.containerplatform.namespace.namespace import Namespace
from new_case.platform.project.project import Project


@pytest.mark.agon
@pytest.mark.apollo
@pytest.mark.platform_api
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestProjectSceneSuite(object):
    def setup_class(self):
        self.project = Project()
        self.ns = Namespace()
        self.project_name = '{}-project-scene'.format(RESOURCE_PREFIX)
        self.display_name = '项目场景测试'
        self.ns_name = '{}-ns'.format(RESOURCE_PREFIX)

        self.teardown_class(self)

        data = {
            "project_name": self.project_name,
            "regions": [REGION_NAME],
            "display_name": self.display_name,
            "description": "描述",
        }
        create_project_result = self.project.create_project("./test_data/project/create_project.jinja2", data)
        assert create_project_result.status_code == 201, "创建项目失败,响应结果：{}".format(create_project_result.content)

        ns_data = {
            "namespace_name": self.ns_name,
            "display_name": '项目场景测试',
            "cluster": REGION_NAME,
            "project": self.project_name,
            "ResourceQuota": "True",
            "morelabel": "False"
        }

        create_ns_result = self.ns.create_namespace('./test_data/namespace/create_namespace.jinja2', ns_data, region_name=REGION_NAME)
        assert create_ns_result.status_code == 200, "创建新命名空间失败 {}".format(create_ns_result.text)

    def teardown_class(self):
        self.project.delete_project(self.project_name)

    def 测试项目下创建ns_项目配额被消耗(self):
        url = self.project.get_projectquota_url(self.project_name)
        ret = self.project.check_exists(url, 200, params={})
        assert ret, "获取项目配额失败"

        ret = self.project.get_project_quota(self.project_name)
        assert ret.status_code == 200, "获取项目配置失败: {}".format(ret.text)

        data = {
            "region_name": REGION_NAME,
            "project_name": self.project_name,
            "assign": True
        }

        value = self.project.generate_jinja_data('./test_data/project/project_quota.jinja2', data=data)

        assert self.project.is_sub_dict(value, ret.json()), "项目配额与预期不一致，预期值: {}, 实际值: {}".format(value, ret.json())

    def 测试更新项目下的ns配置_项目已消耗的配额也更新(self):
        data = {
            "ResourceQuota": "True",
            "requests_cpu": "101",
            "limits_cpu": "101",
            "requests_mem": "101Gi",
            "limits_mem": "101Gi",
            "requests_storage": "101Gi",
            "pvc": "101",
            "pod": "101"
        }
        ret = self.ns.update_resourcequota(self.ns_name, './test_data/namespace/update_resourcequota.jinja2', data=data)
        assert ret.status_code == 200, "更新ns配额失败: {}".format(ret.text)

        sleep(3)

        ret = self.project.get_project_quota(self.project_name)
        assert ret.status_code == 200, "获取项目配置失败: {}".format(ret.text)

        data = {
            "region_name": REGION_NAME,
            "project_name": self.project_name,
            "assign": True,
            "limits_cpu": "101",
            "limits_memory": "101Gi",
            "pv": "101",
            "pods": "101",
            "requests_cpu": "101",
            "requests_memory": "101Gi",
            "requests_storage": "101Gi"
        }

        value = self.project.generate_jinja_data('./test_data/project/project_quota.jinja2', data=data)

        assert self.project.is_sub_dict(value, ret.json()), "项目配额与预期不一致，预期值: {}, 实际值: {}".format(value, ret.json())

    def 测试删除项目下的ns_项目已消耗的配额被释放(self):
        ret = self.ns.delete_namespace(self.ns_name, region_name=REGION_NAME)
        assert ret.status_code == 200, "删除ns失败: {}".format(ret.text)

        url = self.ns.get_namespace_url(self.ns_name, region_name=REGION_NAME)
        ret = self.project.check_exists(url, 404, params={})

        assert ret, "ns没有被删除成功"

        ret = self.project.get_project_quota(self.project_name)
        assert ret.status_code == 200, "获取项目配置失败: {}".format(ret.text)

        data = {
            "region_name": REGION_NAME,
            "project_name": self.project_name,
            "assign": False
        }

        value = self.project.generate_jinja_data('./test_data/project/project_quota.jinja2', data=data)

        assert self.project.is_sub_dict(value, ret.json()), "项目配额与预期不一致，预期值: {}, 实际值: {}".format(value, ret.json())
