import pytest
from new_case.platform.project.project import Project, data_list, casename_list
from common import settings


@pytest.mark.agon
@pytest.mark.apollo
@pytest.mark.platform_api
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestProjectSuite(object):
    def setup_class(self):
        self.project = Project()

    @pytest.mark.parametrize("data", data_list, ids=casename_list)
    def 测试删除项目(self, data):
        delete_result = self.project.delete_project(data["project_name"])
        assert delete_result.status_code == 200, "删除项目失败，响应结果:{}".format(delete_result.content)
        # value = self.project.generate_data("verify_data/project/delete_response.json",
        #                                    {"$project_name": data["project_name"]})
        # assert self.project.is_sub_dict(json.loads(value), delete_result.json()), \
        #     "创建项目比对数据失败，返回数据:{},期望数据:{}".format(delete_result.json(), value)

        assert self.project.check_project_exists(data["project_name"], 404), "删除项目失败"

    @pytest.mark.skipif(settings.AUDIT_UNABLED, reason="do not have audit")
    def 不测试项目删除审计(self):
        payload = {"user_name": settings.USERNAME, "operation_type": "delete", "resource_type": "projects",
                   "resource_name": data_list[0]['project_name']}
        result = self.project.search_audit(payload)
        payload.update({"namespace": "", "region_name": settings.GLOBAL_REGION_NAME})
        values = self.project.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.project.is_sub_dict(values, result.json()), "审计数据不符合预期"
