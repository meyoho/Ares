import pytest
from new_case.platform.project.project import Project
from common.settings import RESOURCE_PREFIX, RERUN_TIMES, CASE_TYPE, USERNAME, REGION_NAME, CALICO_REGION


@pytest.mark.agon
@pytest.mark.apollo
@pytest.mark.platform_api
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestProjectSuite(object):

    def setup_class(self):
        self.project = Project()
        self.project.genetate_global_info()
        self.project_name = "{}-ares-testproject".format(RESOURCE_PREFIX)
        self.display_name = "项目显示名称"
        self.desc = "project description"
        self.update_project_name = "{}-ares-update-project".format(RESOURCE_PREFIX)
        self.update_desc = "update project description"
        self.project_without_region = "project-without-region"
        self.teardown_class(self)

    def teardown_class(self):
        self.project.delete_project(self.project_without_region)
        if CASE_TYPE not in ("prepare", "upgrade", "delete"):
            self.project.delete_project(self.project_name)

    @pytest.mark.BAT
    @pytest.mark.prepare
    def 测试创建项目_全部参数(self):
        # 创建项目
        data = {
            "project_name": self.project_name,
            "regions": [REGION_NAME],
            "display_name": self.display_name,
            "description": "描述",
        }
        create_project_result = self.project.create_project("./test_data/project/create_project.jinja2", data)
        assert create_project_result.status_code == 201, "创建项目失败,响应结果：{}".format(create_project_result.content)

        value = self.project.generate_jinja_data("verify_data/project/create_response.jinja2", data)
        assert self.project.is_sub_dict(value, create_project_result.json()), \
            "创建项目比对数据失败，返回数据:{},期望数据:{}".format(create_project_result.json(), value)

    def 测试创建项目_添加不存在的集群(self):
        data = {
            "project_name": "region-not-exist",
            "regions": ["not-exist"],
            "display_name": "显示名称",
            "description": "描述",
        }
        create_project_result = self.project.create_project("./test_data/project/create_project.jinja2", data)
        assert create_project_result.status_code == 404, "创建项目添加不存在的集群本应该失败,响应结果：{}".format(create_project_result.content)

        value = self.project.generate_jinja_data("verify_data/project/region_not_exist_error_message.jinja2", {"region_name": "not-exist"})
        assert self.project.is_sub_dict(value, create_project_result.json()), \
            "创建项目失败后数据对比失败，返回数据:{},期望数据:{}".format(create_project_result.json(), value)

    def 测试创建项目_不添加集群(self):
        data = {
            "project_name": self.project_without_region,
            "display_name": "显示名称",
            "description": "描述",
        }
        create_project_result = self.project.create_project("./test_data/project/create_project.jinja2", data)
        assert create_project_result.status_code == 201, "创建项目不添加集群失败,响应结果：{}".format(create_project_result.content)

        value = self.project.generate_jinja_data("verify_data/project/create_response.jinja2", data)
        assert self.project.is_sub_dict(value, create_project_result.json()), \
            "创建项目比对数据失败，返回数据:{},期望数据:{}".format(create_project_result.json(), value)

    @pytest.mark.BAT
    def 测试获取集群可用资源(self):
        data = {
            "region": self.project.region_name
        }
        ret = self.project.get_available_resource()
        assert ret.status_code == 200, "获取集群可用资源失败，返回值: {}".format(ret.text)
        content = self.project.get_k8s_resource_data(ret.json(), self.project.region_name, list_key="items")
        value = self.project.generate_jinja_data("verify_data/project/region_available_resources.jinja2", data)
        assert self.project.is_sub_dict(value, content), "获取集群可用资源数据对比失败，返回数据: {}, 期望数据: {}"\
            .format(content, value)

    @pytest.mark.BAT
    @pytest.mark.upgrade
    def 测试通过标准API获取项目列表_不带参数(self):
        ret = self.project.check_project_ready(self.project_name)
        assert ret, "项目状态不是正常状态"

        list_result = self.project.get_project_list_or_detail()
        assert list_result.status_code == 200, "通过标准API获取项目列表失败,响应结果:{}".format(list_result.content)

        content = self.project.get_k8s_resource_data(list_result.json(), self.project_name, list_key="items")
        data = {
            "project_name": self.project_name,
            "display_name": self.display_name,
            "description": "描述",
            "region_name": self.project.region_name,
            "user": USERNAME,
            "clusters": [
                {
                    "name": self.project.region_name
                }
            ]
        }
        value = self.project.generate_jinja_data("verify_data/project/project_detail.jinja2", data)
        assert self.project.is_sub_dict(value, content), "通过标准API获取项目列表比对数据失败，返回数据:{},期望数据:{}".format(content, value)

    @pytest.mark.BAT
    @pytest.mark.upgrade
    def 测试通过高级API获取项目列表(self):
        list_result = self.project.get_project_list()
        assert list_result.status_code == 200, "通过高级API获取项目列表失败,响应结果:{}".format(list_result.content)
        content = self.project.get_k8s_resource_data(list_result.json(), self.project_name, list_key="items")
        data = {
            "project_name": self.project_name,
            "display_name": self.display_name,
            "description": "描述",
            "region_name": self.project.region_name,
            "user": USERNAME,
            "clusters": [
                {
                    "name": self.project.region_name
                }
            ]
        }
        value = self.project.generate_jinja_data("verify_data/project/project_detail.jinja2", data)
        assert self.project.is_sub_dict(value, content), "通过高级API获取项目列表比对数据失败，返回数据:{},期望数据:{}".format(content, value)

    @pytest.mark.BAT
    @pytest.mark.upgrade
    def 测试更新项目基本信息_全部参数_信息正确(self):
        ret = self.project.check_project_ready(self.project_name)
        assert ret, "项目状态不是正常状态"
        data = {
            "update_desc": self.update_desc,
            "update_display_name": self.update_project_name,
            "username": USERNAME
        }
        update_project_result = self.project.update_project(self.project_name,
                                                            "./test_data/project/update_project_annotations.jinja2", data)
        assert update_project_result.status_code == 200, "更新项目失败,响应结果：{}".format(update_project_result.content)

        data = {
            "project_name": self.project_name,
            "display_name": self.update_project_name,
            "description": self.update_desc,
            "user": USERNAME,
            "clusters": [
                {
                    "name": self.project.region_name
                }
            ]
        }
        value = self.project.generate_jinja_data("verify_data/project/project_detail.jinja2", data)
        assert self.project.is_sub_dict(value, update_project_result.json()), \
            "更新项目比对数据失败，返回数据:{},期望数据:{}".format(update_project_result.json(), value)

    @pytest.mark.BAT
    @pytest.mark.upgrade
    def 测试获取项目详情(self):
        detail_result = self.project.get_project_list_or_detail(self.project_name)
        assert detail_result.status_code == 200, "获取项目详情失败,响应结果:{}".format(detail_result.content)
        data = {
            "project_name": self.project_name,
            "display_name": self.update_project_name,
            "description": self.update_desc,
            "user": USERNAME,
            "clusters": [
                {
                    "name": self.project.region_name
                }
            ]
        }
        value = self.project.generate_jinja_data("verify_data/project/project_detail.jinja2", data)
        assert self.project.is_sub_dict(value, detail_result.json()), \
            "获取项目详情比对数据失败，返回数据:{},期望数据:{}".format(detail_result.json(), value)

    @pytest.mark.BAT
    @pytest.mark.upgrade
    def 测试获取项目配额(self):
        quota_detail = self.project.get_project_quota(name=self.project_name)
        assert quota_detail.status_code == 200, "获取项目配额失败:{}".format(quota_detail.text)
        data = {
            "region": self.project.region_name,
            "project": self.project_name
        }
        value = self.project.generate_jinja_data('verify_data/project/project_quotas.jinja2', data)
        assert self.project.is_sub_dict(value, quota_detail.json()), "项目配额数据对比失败，返回值: {}, 预期值: {}"\
            .format(quota_detail.text, value)

    @pytest.mark.BAT
    @pytest.mark.upgrade
    def 测试项目添加集群和更新项目配额(self):
        data = {
            "clusters": [
                {
                    "name": self.project.region_name,
                    "cpu": "200",
                    "memory": "200Gi",
                    "persistentvolumeclaims": "200",
                    "pods": "200",
                    "storage": "200Gi"
                },
                {
                    "name": CALICO_REGION or "global",
                    "pods": "200",
                    "persistentvolumeclaims": "200",
                    "cpu": "200",
                    "memory": "200Gi",
                    "storage": "200Gi"
                }
            ]
        }
        update_project_result = self.project.update_project(self.project_name,
                                                            "./test_data/project/update_project_quota.jinja2",
                                                            data=data)
        assert update_project_result.status_code == 200, "项目添加集群和更新项目配额失败,响应结果：{}"\
            .format(update_project_result.content)

        detail_result = self.project.get_project_list_or_detail(self.project_name)
        assert detail_result.status_code == 200, "获取项目详情失败,响应结果:{}".format(detail_result.content)

        new_data = {
            "project_name": self.project_name,
            "display_name": self.update_project_name,
            "description": self.update_desc,
            "user": USERNAME,
            "clusters": data['clusters']
        }
        value = self.project.generate_jinja_data("verify_data/project/project_detail.jinja2", new_data)

        assert self.project.is_sub_dict(value, detail_result.json()), \
            "项目添加集群和更新项目配额比对数据失败，返回数据:{},期望数据:{}".format(detail_result.json(), value)

    @pytest.mark.BAT
    @pytest.mark.upgrade
    def 测试项目移除集群_多集群(self):
        data = {
            "clusters": [
                {
                    "name": self.project.region_name
                }
            ]
        }
        update_project_result = self.project.update_project(self.project_name, "./test_data/project/clear_cluster.jinja2", data)
        assert update_project_result.status_code == 200, "项目移除集群失败,响应结果：{}".format(update_project_result.content)

        detail_result = self.project.get_project_list_or_detail(self.project_name)
        assert detail_result.status_code == 200, "获取项目详情失败,响应结果:{}".format(detail_result.content)

        new_data = {
            "project_name": self.project_name,
            "display_name": self.update_project_name,
            "description": self.update_desc,
            "user": USERNAME,
            "clusters": [
                {
                    "name": self.project.region_name
                }
            ]
        }

        value = self.project.generate_jinja_data("verify_data/project/project_detail.jinja2", new_data)

        assert self.project.is_sub_dict(value, detail_result.json()), \
            "项目移除集群比对数据失败，返回数据:{},期望数据:{}".format(detail_result.json(), value)

    @pytest.mark.BAT
    def 测试项目移除集群_单集群(self):
        data = {}
        update_project_result = self.project.update_project(self.project_name,
                                                            "./test_data/project/clear_cluster.jinja2", data)
        assert update_project_result.status_code == 200, "项目移除集群失败,响应结果：{}".format(update_project_result.content)

        data = {
            "project_name": self.project_name,
            "display_name": self.update_project_name,
            "description": self.update_desc,
            "user": USERNAME,
        }

        value = self.project.generate_jinja_data("verify_data/project/project_detail.jinja2", data)
        assert self.project.is_sub_dict(value, update_project_result.json()), \
            "情况项目集群比对数据失败，返回数据:{},期望数据:{}".format(update_project_result.json(), value)

    @pytest.mark.BAT
    @pytest.mark.upgrade
    def 测试获取项目的命名空间(self):
        list_result = self.project.get_project_namespace_list()
        assert list_result.status_code == 200, "获取项目的命令空间失败,响应结果:{}".format(list_result.content)

        content = self.project.get_k8s_resource_data(list_result.json(), self.project.k8s_namespace, list_key="items")
        value = self.project.generate_jinja_data("verify_data/project/namespace_list.jinja2", {"namespace": self.project.k8s_namespace,
                                                                                               "project_name": self.project.project_name,
                                                                                               "region_name": self.project.region_name})
        assert self.project.is_sub_dict(value, content), "获取项目的命令空间比对数据失败，返回数据:{},期望数据:{}".format(content, value)

    def 测试过滤项目_根据名称过滤(self):
        ret = self.project.filter_project(self.project_name)
        assert ret.status_code == 200, "根据名称过滤项目失败, 响应结果: {}".format(ret.text)
        assert self.project_name in ret.text, "过滤结果中不包含项目: {}".format(self.project_name)

    def 测试过滤项目_根据显示名称过滤(self):
        ret = self.project.filter_project(self.update_project_name, filterby="display-name")
        assert ret.status_code == 200, "根据显示名称过滤项目失败, 响应结果: {}".format(ret.text)
        assert self.project_name in ret.text, "过滤结果中不包含项目: {}".format(self.project_name)

    def 测试项目分页(self):
        ret = self.project.get_project_list(params={"limit": 1})
        assert ret.status_code == 200, "获取项目列表失败, 响应结果: {}".format(ret.text)
        cont = self.project.get_value(ret.json(), 'metadata.continue')
        assert len(cont) > 1, "分页查询时，返回的continue字段的值为空"
        ret = self.project.get_project_list(params={"limit": 1, "continue": cont})
        assert ret.status_code == 200, "获取角色列表失败, 返回值: {}".format(ret.text)

    @pytest.mark.BAT
    @pytest.mark.delete
    def 测试删除项目(self):
        delete_result = self.project.delete_project(self.project_name)
        assert delete_result.status_code == 200, "删除项目失败，响应结果:{}".format(delete_result.content)
        assert self.project.check_project_exists(self.project_name, 404), "删除项目失败"
