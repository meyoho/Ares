import pytest
from new_case.platform.project.project import Project, data_list, casename_list
from common import settings


@pytest.mark.agon
@pytest.mark.apollo
@pytest.mark.platform_api
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestProjectSuite(object):
    def setup_class(self):
        self.project = Project()

    @pytest.mark.parametrize("data", data_list, ids=casename_list)
    def 测试创建项目(self, data):
        create_project_result = self.project.create_project("./test_data/project/create_project.jinja2", data)
        assert create_project_result.status_code == 201, "创建项目失败,响应结果：{}".format(create_project_result.content)

        value = self.project.generate_jinja_data("verify_data/project/create_response.jinja2", data)
        assert self.project.is_sub_dict(value, create_project_result.json()), \
            "创建项目比对数据失败，返回数据:{},期望数据:{}".format(create_project_result.json(), value)

    @pytest.mark.skipif(settings.AUDIT_UNABLED, reason="do not have audit")
    def 测试项目创建审计(self):
        payload = {"user_name": settings.USERNAME, "operation_type": "create", "resource_type": "projects",
                   "resource_name": data_list[0]['project_name']}
        result = self.project.search_audit(payload)
        payload.update({"namespace": "", "region_name": settings.GLOBAL_REGION_NAME, "code": 201})
        values = self.project.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.project.is_sub_dict(values, result.json()), "审计数据不符合预期"
