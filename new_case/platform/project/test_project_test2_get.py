import pytest
from new_case.platform.project.project import Project, data_list
from common.settings import RERUN_TIMES, DEFAULT_LABEL


@pytest.mark.apollo
@pytest.mark.platform_api
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestProjectSuite(object):

    def setup_class(self):
        self.project = Project()
        self.project_name = data_list[0]["project_name"]

    def 测试通过高级API获取项目列表_带labelSelector(self):
        list_result = self.project.get_project_list(params={"labelSelector": "{}/project.level=0".format(DEFAULT_LABEL)})
        assert list_result.status_code == 200, "通过高级API获取项目列表失败,响应结果:{}".format(list_result.content)
        content = self.project.get_value(list_result.json(), "items")
        assert len(content) == 0, "测试通过高级API获取项目列表_带labelSelector失败，返回数据:{}".format(content)

    def 测试通过高级API获取项目列表_带fieldSelector(self):
        list_result = self.project.get_project_list(params={"fieldSelector": "metadata.name={}".format(self.project_name)})
        assert list_result.status_code == 200, "通过高级API获取项目列表失败,响应结果:{}".format(list_result.content)
        content = self.project.get_value(list_result.json(), "items")
        assert len(content) == 1, "测试通过高级API获取项目列表_带fieldSelector失败，返回数据:{}".format(content)

    def 测试通过高级API获取项目列表_带fieldSelector和labelselecttor(self):
        list_result = self.project.get_project_list(params={"fieldSelector": "metadata.name={}".format(self.project_name),
                                                            "labelSelector": "{}/project.level=1".format(DEFAULT_LABEL)})
        assert list_result.status_code == 200, "通过高级API获取项目列表失败,响应结果:{}".format(list_result.content)
        content = self.project.get_value(list_result.json(), "items")
        assert len(content) == 1, "测试通过高级API获取项目列表_带fieldSelector和labelselecttor失败，返回数据:{}".format(content)

    def 测试通过高级API获取项目列表_带limit和continue(self):
        list_result = self.project.get_project_list(params={"limit": 1})
        assert list_result.status_code == 200, "通过高级API获取项目列表失败,响应结果:{}".format(list_result.content)
        continues = self.project.get_value(list_result.json(), "metadata.continue")
        ret_cnt = self.project.get_project_list(params={"limit": 1, "continue": continues})
        assert ret_cnt.status_code == 200, "通过高级API获取项目列表失败:{}".format(ret_cnt.text)
        assert list_result.json() != ret_cnt.json(), "分页数据相同，第一页数据:{},第二页数据:{}".format(list_result.json(), ret_cnt.json())
