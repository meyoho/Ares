import pytest

from new_case.platform.project.project import Project
from common import settings


@pytest.mark.agon
@pytest.mark.apollo
@pytest.mark.Low
@pytest.mark.flaky(reruns=settings.RERUN_TIMES, reruns_delay=3)
class TestProjectSuite(object):
    def setup_class(self):
        self.project = Project()
        self.data = [{
            "project_name": "{}-ares-test4{}".format(settings.RESOURCE_PREFIX, "12345678901234567"),
            "description": "项目名称为32个英文字符",
            "regions": [settings.REGION_NAME]
        },
            {
                "project_name": "{}-ares-test4{}".format(settings.RESOURCE_PREFIX, "123456789012345678"),
                "description": "项目名称超过32个英文字符(33个)",
                "regions": [settings.REGION_NAME]
            }]
        self.teardown_class(self)

    def teardown_class(self):
        for item in self.data:
            self.project.delete_project(item["project_name"])

    def 测试创建项目_项目名称等于32个英文字符(self):
        create_project_result = self.project.create_project("./test_data/project/create_project.jinja2", self.data[0])
        # 检查项目是否创建成功
        project_status_result = self.project.get_project_list_or_detail(self.data[0]["project_name"])
        project_status_code = project_status_result.status_code

        assert create_project_result.status_code == 201, "创建项目失败,响应结果：{}".format(create_project_result.content)
        value = self.project.generate_jinja_data("verify_data/project/create_response.jinja2", self.data[0])
        assert self.project.is_sub_dict(value, create_project_result.json()), \
            "创建项目比对数据失败，返回数据:{},期望数据:{}".format(create_project_result.json(), value)
        assert project_status_code == 200, "未能查找到创建的项目，创建失败,响应结果：{}".format(project_status_result.content)

    @pytest.mark.skip(reason="http://jira.alauda.cn/browse/AIT-1264")
    def 测试创建项目_项目名称超过32个英文文符(self):
        create_project_result = self.project.create_project("./test_data/project/create_project.jinja2", self.data[1])
        assert create_project_result.status_code == 422, "创建项目失败,响应结果：{}".format(create_project_result.content)
        # 预期返回的内容
        value = self.project.generate_jinja_data("verify_data/project/create_response_failure.jinja2", self.data[1])
        assert self.project.is_sub_dict(value, create_project_result.json()), \
            "创建项目比对数据失败，返回数据:{},期望数据:{}".format(create_project_result.json(), value)
