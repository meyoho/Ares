import json

from common.base_request import Common


class User(Common):
    def __init__(self):
        super(User, self).__init__()
        self.genetate_global_info()

    def get_user_list_or_detail(self, name="", params=None):
        """
        获取用列表，获取用户详情，查询用户
        :param name: 用户名称，适用于获取用户详情
        :param params: 查询参数，适用于查询用户
        :return:
        """
        path = name and "apis/auth.alauda.io/v1/users/{name}".format(name=name) or "apis/auth.alauda.io/v1/users"
        return self.send(method="GET", path=path, params=params)

    def delete_user(self, name):
        path = "apis/auth.alauda.io/v1/users/{name}".format(name=name)
        return self.send(method="DELETE", path=path)

    def get_user_role_list(self, label):
        path = "apis/auth.alauda.io/v1/userbindings?labelSelector={}".format(label)
        return self.send(method="GET", path=path)

    def get_role_list(self):
        path = "apis/rbac.authorization.k8s.io/v1/clusterroles"
        return self.send(method="GET", path=path)

    def get_role_rule_senior(self, name):
        """
        :param name: 角色名称
        :return: 角色权限
        """
        path = 'auth/v1/roles/{name}/rules'.format(name=name)
        return self.send(method='GET', path=path)

    def get_user_list_or_detail_senior(self, params=None, name=None):
        """
        高级api
        :param params: 查询参数
        :return: 用户列表 or 用户详情
        """
        path = name and 'auth/v1/users/{name}'.format(name=name) or 'auth/v1/users'
        return self.send(method='GET', path=path, params=params)

    def bind_user_senior(self, file, data):
        """
        给用户绑定角色
        :param file: 数据yaml
        :param data: 替换数据
        :return: 绑定结果
        """
        path = "auth/v1/userbindings"
        data = self.generate_jinja_data(file, data)
        return self.send(method='post', path=path, json=data)

    def unbind_user_senior(self, name):
        """
        解绑用户角色
        :param name: userbindings 名称
        :return: 解绑结果
        """
        path = "auth/v1/userbindings/{name}".format(name=name)
        return self.send(method='DELETE', path=path)

    def delete_invaild_user_senior(self, user_list, params=None):
        """
        清理无效用户
        :param user_list: 无效用户列表
        :return: 清理结果
        """
        if params is None:
            params = {"range": "all"}
        path = "auth/v1/users"
        return self.send(method="DELETE", data=json.dumps(user_list), path=path, params=params)

    def get_group_senior(self):
        """
        获取组列表
        :return: 组列表
        """
        path = "auth/v1/groups"
        return self.send(method="GET", path=path)

    def get_user_info(self, list_data, email):
        '''
        通过列表获取用户的信息
        :return: 用户信息
        '''
        items = list_data["items"]
        for item in items:
            if self.get_value(item, "spec.email") == email:
                return item
        return {}

    def filter_user(self, value, filterby='email', limit=20):
        """
        用户搜索
        :param value: 被搜索的对象
        :param filterby: 过滤条件：email、username、group
        :param limit: 每页包含的数据条数
        :return:
        """
        path = 'auth/v1/users?limit={}&filterBy={},{}'.format(limit, filterby, value)
        return self.send(method='get', path=path)

    def filter_invalid_user(self, valid='false', limit=20):
        path = 'apis/auth.alauda.io/v1/users?limit={}&labelSelector=auth.{}/user.valid={}'.format(limit, self.default_label, valid)
        return self.send(method='GET', path=path)
