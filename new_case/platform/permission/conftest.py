import pytest

from common.settings import DEFAULT_NS, LDAP_HOST
from new_case.platform.idp.idp import Idp


@pytest.fixture(scope="session", autouse=True)
def ldap_prepare():
    idp = Idp()
    data = {
        "description": "ldap 用户相关测试",
        "idp_name": 'ldap-user',
        "ldap_host": LDAP_HOST,
        "ldap_admin": "cn=admin,dc=alauda,dc=com",
        "ldap_password": "admin",
        "idp_display_name": "ldap-user",
        "verify_username": "project@alauda.io",
        "namespace": DEFAULT_NS,
        "verify_password": "123456",
        "type": "ldap",
        "group_search": True
    }

    create_idp = idp.create_idp('test_data/idp/create_idp.jinja2', data)
    assert create_idp.status_code == 200, "创建idp失败, \n{}".format(create_idp.text)
    yield
    idp.delete_idp('ldap-user')
