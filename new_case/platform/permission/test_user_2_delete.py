import pytest
from common.settings import RERUN_TIMES, USERNAME, GLOBAL_REGION_NAME, AUDIT_UNABLED
from new_case.platform.permission.user import User


@pytest.mark.apollo
@pytest.mark.platform_api
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestPermissionDELETESuite(object):
    def setup_class(self):
        self.user = User()
        self.base_email = "3608afda9b9767bbe9c309dd1dc23001"
        self.email = USERNAME
        list_user = self.user.get_user_list_or_detail_senior()
        assert list_user.status_code == 200, "获取用户列表失败:{}".format(list_user.text)
        content = self.user.get_k8s_resource_data(list_user.json(), self.email, list_key="items")
        self.base_email = self.user.get_value(content, "metadata.name")

    def 测试清理全部无效数据(self):
        delete_user = self.user.delete_invaild_user_senior([])
        assert delete_user.status_code == 200, "清理用户失败:{}".format(delete_user.text)

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 不测试用户删除审计(self):
        payload = {"user_name": USERNAME, "operation_type": "delete", "resource_type": "users",
                   "resource_name": self.base_email}
        result = self.user.search_audit(payload)
        payload.update({"namespace": "", "region_name": GLOBAL_REGION_NAME})
        values = self.user.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.user.is_sub_dict(values, result.json()), "审计数据不符合预期"
