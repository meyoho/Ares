import pytest
from new_case.platform.idp.idp import Idp
from new_case.platform.permission.user import User
from common.settings import RESOURCE_PREFIX, LDAP_HOST
from common.check_status import checker


@pytest.mark.skip(reason="http://jira.alauda.cn/browse/AIT-2415")
@pytest.mark.skipif(not checker.ldap_available(), reason="LDAP不可用")
@pytest.mark.apollo
@pytest.mark.platform_api
class TestUserSceneSuite(object):
    def setup_class(self):
        self.idp = Idp()
        self.user = User()
        self.idp_name = "{}-user-test".format(RESOURCE_PREFIX)
        self.idp.delete_idp(self.idp_name)

    def teardown_class(self):
        self.idp.sync_ldap_and_local()
        self.idp.delete_idp(self.idp_name)

    def 测试获取无效用户(self):
        data = {
            "description": "idp测试",
            "idp_name": self.idp_name,
            "ldap_host": LDAP_HOST,
            "ldap_admin": "cn=admin,dc=alauda,dc=com",
            "ldap_password": "admin",
            "idp_display_name": "idp-ldap测试",
            "verify_username": "project@alauda.io",
            "namespace": self.idp.default_ns,
            "verify_password": "123456",
            "type": "ldap",
            "group_search": True
        }
        create_idp = self.idp.create_idp('test_data/idp/create_idp.jinja2', data)
        assert create_idp.status_code == 200, "创建idp失败, \n{}".format(create_idp.text)

        ret = self.idp.sync_ldap_and_local(user_source=self.idp_name)
        assert ret.status_code == 200, "同步用户失败: {}".format(ret.text)

        ret = self.idp.delete_idp_senior(self.idp_name)
        assert ret.status_code == 200, "删除idp失败: {}".format(ret.text)

        ret = self.user.filter_invalid_user()
        assert ret.status_code == 200, "获取无效用户失败: {}".format(ret.text)

        item = self.idp.get_value(ret.json(), 'item')

        assert len(item) > 0, "idp删除后，该idp下的用户状态未自动重置为无效状态: {}".format(item)

    def 测试清理全部无效用户(self):
        delete_user = self.user.delete_invaild_user_senior([])
        assert delete_user.status_code == 200, "清理无效用户失败:{}".format(delete_user.text)

        ret = self.user.filter_invalid_user()
        assert ret.status_code == 200, "获取无效用户失败: {}".format(ret.text)

        item = self.idp.get_value(ret.json(), 'item')

        assert len(item) == 0, "清理无效用户后，用户列表中还存在无效用户: {}".format(item)
