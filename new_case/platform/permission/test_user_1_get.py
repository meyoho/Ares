import pytest
from common.settings import RERUN_TIMES, USERNAME, DEFAULT_LABEL
from new_case.platform.permission.user import User
import json


@pytest.mark.apollo
@pytest.mark.BAT
@pytest.mark.platform_api
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestPermissionGETSuite(object):
    def setup_class(self):
        self.user = User()
        self.email = USERNAME
        list_user = self.user.get_user_list_or_detail_senior(params="filterBy=email,{}".format(self.email))
        assert list_user.status_code == 200, "获取用户列表失败:{}".format(list_user.text)
        content = self.user.get_user_info(list_user.json(), self.email)
        self.name = self.user.get_value(content, "spec.username")
        self.base_email = self.user.get_value(content, "metadata.name")
        self.base58_name = self.user.get_value(content, "metadata#labels#auth.{}/user.username".format(DEFAULT_LABEL),
                                               delimiter="#")

        self.group_name = "development-team"
        self.role_name = "acp-platform-auditor"

    def 测试获取用户列表标准API(self):
        list_user = self.user.get_user_list_or_detail()
        assert list_user.status_code == 200, "获取用户列表失败:{}".format(list_user.text)
        content = self.user.get_k8s_resource_data(list_user.json(), self.base_email, list_key="items")

        data = {"email": self.email,
                "username": self.name,
                "base58_name": self.base58_name,
                "base_email": self.base_email}
        value = self.user.generate_jinja_data("verify_data/permission/user_list.jinja2", data)
        assert self.user.is_sub_dict(value, content), list_user.json()

    def 测试通过邮箱查询用户(self):
        list_user = self.user.get_user_list_or_detail_senior(params={"filterBy": "email,{}".format(self.email)})
        assert list_user.status_code == 200, "获取用户列表失败:{}".format(list_user.text)
        content = self.user.get_k8s_resource_data(list_user.json(), self.base_email, list_key="items")

        data = {"email": self.email,
                "username": self.name,
                "base58_name": self.base58_name,
                "base_email": self.base_email}
        value = self.user.generate_jinja_data("verify_data/permission/user_list.jinja2", data)
        assert self.user.is_sub_dict(value, content), list_user.json()

    def 测试通过组查询用户(self):
        list_user = self.user.get_user_list_or_detail_senior(params={"filterBy": "group,未分组"})
        assert list_user.status_code == 200, "获取用户列表失败:{}".format(list_user.text)

    def 测试获取用户详情标准API(self):
        detail_user = self.user.get_user_list_or_detail(self.base_email)
        assert detail_user.status_code == 200, "获取用户详情失败:{}".format(detail_user.text)
        data = {"email": self.email,
                "username": self.name,
                "base58_name": self.base58_name,
                "base_email": self.base_email}
        value = self.user.generate_jinja_data("verify_data/permission/user_detail.jinja2", data)
        assert self.user.is_sub_dict(value, detail_user.json()), "获取用户详情比对数据失败"

    @pytest.mark.parametrize("role_name", [
        # "acp-namespace-admin",
        # "acp-namespace-developer",
        "acp-platform-admin",
        "acp-platform-auditor",
        # "acp-project-admin"
    ])
    def 测试获取角色权限(self, role_name):
        role_rules = self.user.get_role_rule_senior(role_name)
        assert role_rules.status_code == 200, '获取角色权限错误:{}'.format(role_rules.text)
        content = self.user.generate_data("verify_data/permission/{}.json".format(role_name))
        assert self.user.is_sub_dict(json.loads(content), role_rules.json())
