from hashlib import md5

import pytest

from common.check_status import checker
from common.log import logger
from common.settings import (AUDIT_UNABLED, DEFAULT_LABEL, GLOBAL_REGION_NAME,
                             RERUN_TIMES, USERNAME)
from new_case.platform.idp.idp import Idp
from new_case.platform.permission.user import User

user_filter = {
            "用户名称_精确查找": {
                "value": "acp-project-admin@alauda.io",
                "filterby": "email"
            },
            "用户名称_模糊匹配": {
                "value": "acp",
                "filterby": "email"
            },
            "用户名称_包含大写字母": {
                "value": "Acp-projecT-admin@alauda.io",
                "filterby": "email"
            },
            "显示名称_精确查找": {
                "value": "acp-project-admin",
                "filterby": "username"
            },
            "显示名称_模糊查找": {
                "value": "acp",
                "filterby": "username"
            },
            "显示名称_包含大写字母": {
                "value": "aCP-projeCt-admin",
                "filterby": "username"
            },
            "用户组": {
                "value": "autotest",
                "filterby": "group"
            }
        }


@pytest.mark.skipif(not checker.ldap_available(), reason="LDAP不可用")
@pytest.mark.apollo
@pytest.mark.BAT
@pytest.mark.platform_api
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestPermissionL0Suite(object):
    def setup_class(self):
        self.user = User()
        self.idp = Idp()
        self.email = USERNAME
        list_user = self.user.get_user_list_or_detail_senior(params="filterBy=email,{}".format(self.email))
        assert list_user.status_code == 200, "获取用户列表失败:{}".format(list_user.text)
        content = self.user.get_user_info(list_user.json(), self.email)
        self.name = self.user.get_value(content, "spec.username")
        self.base_email = self.user.get_value(content, "metadata.name")
        self.base58_name = self.user.get_value(content, "metadata#labels#auth.{}/user.username".format(DEFAULT_LABEL),
                                               delimiter="#")

        self.role_name = "acp-platform-auditor"
        self.username = 'acp-project-admin@alauda.io'
        self.base58_email = md5('acp-project-admin@alauda.io'.encode(encoding="utf-8")).hexdigest()

        sync_user = self.idp.sync_ldap_and_local(user_source="ldap")
        logger.info("用户同步结果: {}".format(sync_user.text))
        assert sync_user.status_code == 200, "同步用户失败，\n{}".format(sync_user.text)

    def 测试获取用户列表高级API(self):
        list_user = self.user.get_user_list_or_detail_senior()
        assert list_user.status_code == 200, "获取用户列表失败:{}".format(list_user.text)
        content = self.user.get_k8s_resource_data(list_user.json(), self.base_email, list_key="items")
        data = {"email": self.email,
                "username": self.name,
                "base58_name": self.base58_name,
                "base_email": self.base_email}
        value = self.user.generate_jinja_data("verify_data/permission/user_list.jinja2", data)
        assert self.user.is_sub_dict(value, content), list_user.json()

    def 测试获取用户详情高级API(self):
        detail_user = self.user.get_user_list_or_detail_senior(name=self.base_email)
        assert detail_user.status_code == 200, "获取用户详情失败:{}".format(detail_user.text)
        data = {"email": self.email,
                "username": self.name, "base58_name": self.base58_name,
                "base_email": self.base_email}
        value = self.user.generate_jinja_data("verify_data/permission/user_detail_senior.jinja2", data)
        assert self.user.is_sub_dict(value, detail_user.json()), "获取用户详情比对数据失败"

    @pytest.mark.agon
    def 测试给用户绑定角色(self):
        bind_name = md5("{}-{}-platform".format(self.username, self.role_name).encode(encoding="utf-8")).hexdigest()
        base_email = md5(self.username.encode(encoding="utf-8")).hexdigest()
        # 先解绑一下
        self.user.unbind_user_senior(name=bind_name)
        data = {
            "name": bind_name,
            "role_level": "platform",
            "role_name": self.role_name,
            "role_displayname": "4bKQz96CPh",
            "user_email": self.username,
            "base_email": base_email,
            "REGION_NAME": "",
            "K8S_NAMESPACE": "",
            "PROJECT_NAME": "",
            "_role_displayname": "平台审计人员"
        }
        bind_user = self.user.bind_user_senior("./test_data/permission/bind_role.jinja2", data)
        assert bind_user.status_code == 201, "给用户绑定角色失败:{}".format(bind_user.text)

        data = {"name": bind_name,
                "role_level": "platform",
                "role_name": self.role_name,
                "username": base_email,
                "base58_name": "4bKQz96CPh",
                "email": self.username,
                "role_displayname": "平台审计人员"
                }
        value = self.user.generate_jinja_data("verify_data/permission/bind_role_response.jinja2", data)
        assert self.user.is_sub_dict(value, bind_user.json()), \
            "给用户绑定角色比对数据失败，返回数据:{},期望数据:{}".format(bind_user.json(), value)

    def 测试获取某个用户的角色列表(self):
        base_email = md5(self.username.encode(encoding="utf-8")).hexdigest()
        label_selector = "auth.{}/user.email={}".format(DEFAULT_LABEL, base_email)
        user_role_list = self.user.get_user_role_list(label=label_selector)
        assert user_role_list.status_code == 200, "获取用户角色列表失败:{}".format(user_role_list.text)
        content = self.user.get_k8s_resource_data(user_role_list.json(), self.role_name, list_key="items")
        bind_name = md5("{}-{}-platform".format(self.username, self.role_name).encode(encoding="utf-8")).hexdigest()
        data = {"name": bind_name,
                "role_level": "platform",
                "role_name": self.role_name,
                "base_email": base_email,
                "base58_name": "4bKQz96CPh",
                "email": self.username,
                "role_displayname": "平台审计人员"
                }
        value = self.user.generate_jinja_data("verify_data/permission/user_role_list.jinja2", data)
        assert self.user.is_sub_dict(value, content), "获取用户的角色列表比对数据失败，返回数据:{},期望数据:{}"\
            .format(content, value)

    @pytest.mark.agon
    def 测试解绑用户某个角色(self):
        bind_name = md5("{}-{}-platform".format(self.username, self.role_name).encode(encoding="utf-8")).hexdigest()
        delete_bind_user = self.user.unbind_user_senior(name=bind_name)
        assert delete_bind_user.status_code == 200, "解绑用户角色失败:{}".format(delete_bind_user.text)

    def 测试获取角色列表(self):
        role_list = self.user.get_role_list()
        assert role_list.status_code == 200, "获取角色列表失败:{}".format(role_list.text)

    def 测试获取用户组列表(self):
        role_group = self.user.get_group_senior()
        assert role_group.status_code == 200, "获取用户组列表失败:{}".format(role_group.text)

    @pytest.mark.parametrize('key', user_filter)
    def 测试过滤用户(self, key):
        self.idp.sync_ldap_and_local(user_source="ldap")
        ret = self.user.get_user_list_or_detail_senior({"limit": 200})
        logger.info("用户列表: {}".format(ret.text))
        value = user_filter[key]['value']
        filter_by = user_filter[key]['filterby']
        ret = self.user.filter_user(value, filter_by)
        assert ret.status_code == 200, "根据: {} 搜索用户失败, 响应内容: {}".format(key, ret.text)
        assert self.username in ret.text, "根据: {} 搜索用户失败, 用户: {}不包含在搜索结果中, 响应内容: {}" \
            .format(key, self.username, ret.text)

    def 测试删除用户(self):
        ret = self.user.delete_user(self.base58_email)
        assert ret.status_code == 200, "删除用户操作失败, 响应内容: {}".format(ret.text)
        ret = self.user.filter_user(self.username)
        assert ret.status_code == 200, "根据用户名搜索用户失败, 响应内容: {}".format(ret.text)
        assert self.username not in ret.text, "删除用户失败，用户还存在于用户列表中, 响应内容: {}".format(ret.text)

    def 测试用户列表分页(self):
        ret = self.user.get_user_list_or_detail(params={"limit": 1})
        assert ret.status_code == 200, "获取角色列表失败, 返回值: {}".format(ret.text)
        cont = self.user.get_value(ret.json(), 'metadata.continue')
        assert len(cont) > 1, "分页查询时，返回的continue字段的值为空"
        ret = self.user.get_user_list_or_detail(params={"limit": 1, "continue": cont})
        assert ret.status_code == 200, "获取角色列表失败, 返回值: {}".format(ret.text)


@pytest.mark.apollo
@pytest.mark.platform_api
@pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
@pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
class TestUserBindingAuditL1Suite(object):
    def setup_class(self):
        self.user = User()
        self.role_name = "acp-platform-auditor"
        self.bind_name = md5("{}-{}-platform".format(USERNAME, self.role_name).encode(encoding="utf-8")).hexdigest()

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 不测试用户绑定创建审计(self):
        payload = {"user_name": USERNAME, "operation_type": "create", "resource_type": "userbindings",
                   "resource_name": self.bind_name}
        result = self.user.search_audit(payload)
        payload.update({"namespace": "", "region_name": GLOBAL_REGION_NAME, "code": 201})
        values = self.user.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.user.is_sub_dict(values, result.json()), "审计数据不符合预期"

    @pytest.mark.skipif(AUDIT_UNABLED, reason="do not have audit")
    def 不测试用户绑定删除审计(self):
        payload = {"user_name": USERNAME, "operation_type": "delete", "resource_type": "userbindings",
                   "resource_name": self.bind_name}
        result = self.user.search_audit(payload)
        payload.update({"namespace": "", "region_name": GLOBAL_REGION_NAME})
        values = self.user.generate_jinja_data("./verify_data/audit/audit.jinja2", payload)
        assert self.user.is_sub_dict(values, result.json()), "审计数据不符合预期"
