from _md5 import md5
import xlrd
import pytest
import yaml
from common import settings
from common.base_request import Common
from common.settings import get_token, RERUN_TIMES
from new_case.platform.permission.user import User
from new_case.platform.platform.platform import Platform
from common.delayed_assert import expect, assert_expectations
from new_case.platform.idp.idp import Idp
from new_case.platform.rolemanager import RoleTemplate
from common.check_status import checker

idp_data = {
    "idp_name": "permissiontest",
    "ldap_host": settings.LDAP_HOST,
    "ldap_admin": "cn=admin,dc=alauda,dc=com",
    "ldap_password": "admin",
    "namespace": settings.DEFAULT_NS,
    "ldap_display_name": "权限测试",
    "verify_username": "alauda@qa.io",
    "verify_password": "123456",
    "type": "ldap",
    "group_search": True
}

user_list = [
    'acp-platform-admin@alauda.io',
    'acp-platform-auditor@alauda.io',
    'acp-project-admin@alauda.io',
    'acp-namespace-admin@alauda.io',
    'acp-namespace-developer@alauda.io'
]

title_list = ['organiation', 'permission_describe', 'group', 'k8s_resource', 'resource_scope', 'resource_cluster',
              'resource_namespace', 'resource_name', 'acp-platform-admin', 'acp-platform-auditor', 'acp-project-admin',
              'acp-namespace-admin', 'acp-namespace-developer', 'note']
dict_content = []


def read_excel(file):
    """
    读入excel文件
    :rtype : object
    :param file:
    :return: 数据对象
    """
    try:
        data = xlrd.open_workbook(file, encoding_override='utf-8')
        return data
    except Exception as err:
        print(err)


def dataProcessing():
    """
    :rtype : object
    :return list[dict]
    """
    data = read_excel('verify_data/permission/dataprovider.xlsx')
    table = data.sheet_by_name(u'dataprovider')
    for i in range(1, table.nrows):
        row_content = table.row_values(i, 0, table.ncols)
        dict_content.append(dict(zip(title_list, row_content)))
    return dict_content


class ResourceAuthorization(Common):

    def __init__(self):
        super().__init__()
        self.platform = Platform()
        self.idp = Idp()
        self.user = User()
        self.r_clinet = RoleTemplate()
        self.data_template = self.platform.generate_jinja_template('test_data/platform/resourceauthorization.jinja2')
        self.role_cn_en_name = {
            "acp-platform-admin": "平台管理员",
            "acp-platform-auditor": "平台审计人员",
            "acp-project-admin": "项目管理员",
            "acp-namespace-admin": "命名空间管理员",
            "acp-namespace-developer": "开发人员"
        }
        self.role_token = {
            "acp-platform-admin": "",
            "acp-platform-auditor": "",
            "acp-project-admin": "",
            "acp-namespace-admin": "",
            "acp-namespace-developer": ""
        }
        self.idp.delete_idp(idp_name=idp_data['idp_name'])

    def generation_token(self):
        create_idp = self.idp.create_idp('test_data/idp/create_idp.jinja2', idp_data)
        assert create_idp.status_code == 200, "创建idp失败"
        for role, email in zip(self.role_token.keys(), user_list):
            self.assign_role_to_user(role, email)
            self.role_token[role] = get_token(idp_data['idp_name'], email,
                                              password=idp_data["verify_password"])[0]
        return self.role_token

    def assign_role_to_user(self, role_name, email):
        role_level = role_name.split('-')[1]
        project_name = ""
        namespace_name = ""
        region_name = ""
        if role_level == 'project':
            project_name = self.project_name
        elif role_level == 'namespace':
            project_name = self.project_name
            namespace_name = self.k8s_namespace
            region_name = self.region_name
        bind_name = md5("{}-{}-{}".format(role_name, role_name, role_level).encode(encoding="utf-8")).hexdigest()
        assign_role_to_user_data = {
            "name": bind_name,
            "role_level": role_level,
            "role_name": role_name,
            "role_displayname": role_name,
            "user_email": email,
            "base_email": md5(email.encode("utf8")).hexdigest(),
            "REGION_NAME": region_name,
            "K8S_NAMESPACE": namespace_name,
            "PROJECT_NAME": project_name,
            "_role_displayname": self.role_cn_en_name[role_name],
        }
        self.user.unbind_user_senior(bind_name)
        bind_user = self.user.bind_user_senior("./test_data/permission/bind_role.jinja2", assign_role_to_user_data)
        assert bind_user.status_code == 201, "给用户绑定角色失败:{}".format(bind_user.text)
        ret = self.r_clinet.check_clusterroles('userbindings', bind_name)
        assert ret, "给用户绑定角色时未生成对应的userbindings，或者同步userbindings到业务集群失败"


@pytest.mark.skipif(not checker.ldap_available(), reason="LDAP不可用")
@pytest.mark.auth_controller2
@pytest.mark.platform_api
class TestResourceAuthorization(object):
    """
    权限测试， 标准api
    """
    data = dataProcessing()
    action = ['create', 'delete', 'get', 'update']
    role = [
        "acp-platform-admin",
        "acp-platform-auditor",
        "acp-project-admin",
        "acp-namespace-admin",
        "acp-namespace-developer"
    ]

    def setup_class(self):
        self.platform = Platform()
        self.idp = Idp()
        self.user = User()
        self.data_template = self.platform.generate_jinja_template('test_data/platform/resourceauthorization.jinja2')
        try:
            self.role_token = ResourceAuthorization().generation_token()
        except AssertionError:
            self.teardown_class(self)

    def api_request(self, data=None, token=None, region_name=None):
        region_name = region_name.lower()
        if region_name == 'business' or region_name == 'global/business':
            return self.platform.resourceAuthentication(
                data=data,
                resourceauthorization=True,
                headers={"Authorization": token},
                region_name=self.platform.region_name
            )
        elif region_name == 'global':
            return self.platform.resourceAuthentication(
                data=data,
                resourceauthorization=True,
                headers={"Authorization": token}
            )
        else:
            assert False, "资源集群不识别, region:{}, data:{}".format(region_name, data)

    def expect_permission_result(self, response, action, data_action, role, k8s_resource, resource_name):
        allowed = self.platform.get_value(response.json(), 'status.allowed')
        if data_action == '查看':
            if action == 'get':
                expect(
                    allowed is True,
                    "角色：{}，资源：{}，权限：{}，操作：{}，资源名称：{}，测试失败，预期：{}， 实际结果：{}".format(
                        role, k8s_resource, data_action, action, resource_name, True, allowed
                    ))
            else:
                expect(
                    allowed is False,
                    "角色：{}，资源：{}，权限：{}，操作：{}，资源名称：{}， 测试失败，预期：{}， 实际结果：{}".format(
                        role, k8s_resource, data_action, action, resource_name, False, allowed))
        elif data_action == '-':
            expect(
                allowed is False,
                "角色：{}，资源：{}，权限：{}，操作：{}，资源名称：{}，  测试失败，预期：{}， 实际结果：{}".format(
                    role, k8s_resource, data_action, action, resource_name, False, allowed))
        else:
            expect(
                allowed is True,
                "角色：{}，资源：{}，权限：{}，操作：{}，资源名称：{}，  测试失败，预期：{}， 实际结果：{}".format(
                    role, k8s_resource, data_action, action, resource_name, True, allowed))

    def teardown_class(self):
        self.idp.delete_idp(idp_name=idp_data['idp_name'])
        for email in user_list:
            self.user.delete_user(md5(email.encode("utf8")).hexdigest())

    # @pytest.mark.parametrize("data_provider", data)
    @pytest.mark.parametrize("action", action)
    @pytest.mark.parametrize("role", role)
    @pytest.mark.flaky(reruns=RERUN_TIMES, reruns_delay=3)
    def 测试权限(self, action, role):
        for data_provider in self.data:
            data_provider['action'] = action
            data = yaml.safe_load(
                self.data_template.render(
                    data_provider
                ).replace(
                    ": -", ": \"\""
                ).replace(
                    "用户 ns", self.platform.k8s_namespace
                ).replace(
                    "同项目名称", self.platform.project_name
                ).replace(
                    ": core", ": \"\""
                ).replace(
                    ": *", ": \'*\'"
                ).replace(
                    "alauda-system", self.platform.default_ns
                )
            )
            if data_provider['note'] != '暂不校验':
                rs = self.api_request(data, self.role_token[role], region_name=data_provider['resource_cluster'])
                assert rs.status_code == 201, "资源鉴权API请求失败, {}".format(rs.text)
                self.expect_permission_result(
                    rs, action, data_provider[role], role, data_provider['k8s_resource'], data_provider['resource_name']
                )
                assert_expectations()
