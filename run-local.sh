#!/usr/bin/env bash
find . -name \*.pyc -delete
docker run -t --rm --name ares \
	-v $(pwd):/app/ \
	-v $(pwd)/report:/app/report/ \
	index.alauda.cn/alaudak8s/ares
