# What is this repository for?

1. 该仓库是用来测试标注产品的API测试，使用的python pytest测试框架
2. pytest会自动收集指定目录下所有以<测试>开头的方法作为测试的case，并且最终会在Report下生成测试报告

# RUN E2E
本地执行：
1.首先运行环境需要安装python3+和pytest以及requests包，参考requirement.txt
解决本地python环境问题文档：https://www.jianshu.com/p/00af447f0005

2.然后设置需要测试对象的环境变量

查看common/settings文件定义的变量
如：API_URL，ACCOUNT, PASSWORD, REGION_NAME, REGISTRY_NAME等等

如果用到第三方服务，要保证第三方服务的可用性，如：jenkins，svn，gitlab等，同时将用到的参数写入环境变量中

3. 最后通过执行pytest <test_path>即可

容器执行：

1.下载镜像 index.alauda.cn/alaudak8s/ares

2.然后需要准备测试对象的环境变量写入一个环境变量文件，具体设置和本地运行一致。然后在运行容器时将这个环境变量文件挂载到容器

3.执行命令：
docker run -t --name vipercd \
	-v $(pwd)/report:/app/report/ \
	--env-file=./local-test.env \
	index.alauda.cn/alaudak8s/ares

# Code Standard

1. 写代码前要先将用例设计的思路发到测试群进行review，如果没有用例review直接写代码的，罚款

2. 提交代码一定要保证flake8通过

3. 只要有改动，一定要保证改动影响到的case跑通过之后才可以提交PR，如果发现代码问题导致测试失败，罚款。（因为环境脏数据导致失败或者代码不够健壮不算）

4. 在提PR时把改动需要测试的路径写在PR title上，流水线会获取pr的title在流水线里跑测试

5. 要保证case全部通过，如果环境不支持的case，Report结果标记为通过

6. 如果新增了环境变量，要case合并到master后及时更新私有部署自动化测试使用文档的说明

7. 添加新功能的case时，挑选一个critical级别的case，打上BAT的标签。

8. 在封装基础类方法时要考虑到通用性、可读性和易用性
