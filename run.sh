#!/bin/sh

set -o pipefail
set -o nounset

PLUGIN_NAME="${PLUGIN_NAME:-ares}"
RESULTS_DIR="${RESULTS_DIR:-/tmp/results}"

# run tests
sleep 10
python3 main.py
cp -r report/* ${RESULTS_DIR}
# cp test.log ${RESULTS_DIR}
# cat test.log

# Gather results into one file
cd ${RESULTS_DIR}
tar -czf ${PLUGIN_NAME}.tar.gz *

# Let the sonobuoy worker know the job is done
echo -n "${RESULTS_DIR}/${PLUGIN_NAME}.tar.gz" >"${RESULTS_DIR}/done"

sleep 120