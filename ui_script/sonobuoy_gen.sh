sonobuoy gen plugin --name `echo $2 | tr '_' '-'` --image $1 -e PUSH_GATE_WAY=http://pushgateway-api.alauda.cn/v1/tools/push_to_dashboard -e ENV=staging -e CASE_TYPE=$2 -e BRANCH_NAME=$3 -e BUILD_URL=$4 > $5
echo "  - mountPath: /dev/shm">>$5
echo "    name: shm">>$5
echo "  imagePullPolicy: Always">>$5
echo "extra-volumes:">>$5
echo "- name: shm">>$5
echo "  hotPath:">>$5
echo "    path: /dev/shm">>$5
cat $5

