import os
import sys
import tarfile

import json
import shutil

report_type = sys.argv[1]
tar_file = sys.argv[2]

class Result:
    def __init__(self):
        os.system("rm -rf " + "/tmp/{}".format(report_type))
        os.system("mkdir " + "/tmp/{}".format(report_type))
        if report_type == "ui_report":
            os.system("rm -rf ui_report")
            os.system("mkdir ui_report")

    def analyse(self):
        shutil.copyfile(tar_file, "ui_report/{name}.tar".format(name=report_type))
        plugins_folder = "/tmp/{name}/plugins".format(name = report_type)
        result = True
        for api_dir in os.listdir(plugins_folder):
            result_folder = plugins_folder + "/{dir}/results/global".format(dir=api_dir)
            if os.path.exists(result_folder):
                failed_file = result_folder + "/protractorFailuresReport/protractorTestErrors-3.xml"
                if(os.path.isfile(failed_file)):
                    os.system("cp {file} ui_report/{report}_{name}.xml".format(file=failed_file, report=report_type, name=api_dir))
                    result = result and False

            error_file = "/tmp/{name}/plugins/{dir}/errors".format(name=report_type,dir=api_dir)
            if(os.path.isfile(error_file)):
                result = result and False
        return result
        

    def untar(self, tarFile, desFolder):
        tar = tarfile.open(tarFile)
        names = tar.getnames()
        for name in names:
            tar.extract(name, desFolder)
        tar.close()

if __name__ == '__main__':
    result = Result()
    result.untar(tar_file, "/tmp/{}".format(report_type))
    if result.analyse() is False:
        exit(1)
