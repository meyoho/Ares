import base64
import os
import time
import requests
import re
import urllib3
from time import sleep


def get_list_from_str(string, separator=','):
    if string is not None and string != '':
        return string.split(separator)


ENV = os.getenv("ENV", "staging")
RESOURCE_PREFIX = os.getenv("RESOURCE_PREFIX",
                            "local").replace(".", "").replace("_",
                                                              "-").lower()[0:10]
if ENV == "int":
    # necessary
    API_URL = "https://int.alauda.cn"
    REGION_NAME = "cls-7758zpxq"
    CALICO_REGION = "calico-new"
    OVN_REGION = "ovn"
    MACVLAN_REGION = os.getenv("MACVLAN_REGION")
    GLOBAL_REGION_NAME = "global"
    REGISTRY = "index.alauda.cn"
    IMAGE = "{}/alaudaorg/qaimages:helloworld".format(REGISTRY)
    GLOBAL_ALB_NAME = "alb2"
    DEFAULT_NS = "alauda-system"
    DEFAULT_LABEL = "alauda.io"
    VM_IPS = "129.28.171.104;129.28.145.9;129.28.167.29;94.191.57.191"
    VM_USERNAME = "root"
    VM_PASSWORD = "alauda_qavm"
    VM_KEY = "./key.pem"
    # 必需,模板仓库的chart repo地址信息
    CHARTREPO_URL = "alauda-chart-incubator-new.ace-default.cloud.myalauda.cn/"
    CHARTREPO_TYPE = "http"
    CHARTREPO_USER = "7DO9QoLtDx1g"
    CHARTREPO_PASSWORD = "2wb4E1iydRj7"
    # 模板仓库类型Git
    GITURL_HTTPS = "https://github.com/alauda/captain-test-charts"
    # 模板仓库类型SVN
    SVNURL_HTTPS = "https://svn.apache.org/repos/asf/xml/xang/trunk"
    # devops
    JENKINS_URL = "http://10.0.128.76:32001"
    JENKINS_USERNAME = "admin"
    JENKINS_PASSWORD = "11d1a9fac3f5b2da4beb6b05fc143a2eb8"
    JENKINS_TOOL_NAME = 'local-ares-jenkins'
    # nfs服务器的地址
    NFS_IP = '10.0.128.115'
    NFS_PATH = '/exported/path'
    # 可用的存储类名称
    SC_NAME = "cephfs"
    USERNAME = "admin@cpaas.io"
    PASSWORD = "password"
    OIDC_ISSUER_URL = os.getenv('OIDC_HOST',
                                'http://keycloak.qa.alauda.cn/auth/realms/master')
    OIDC_SECRET_ID = os.getenv('OIDC_SECRET_ID',
                               '54043ce8-007d-490c-98db-b124acf1ab7b')
elif ENV == "staging":
    # necessary
    API_URL = "https://10.0.129.100"
    REGION_NAME = "high"
    CALICO_REGION = "cls-w5xrsf62"
    OVN_REGION = "high"
    MACVLAN_REGION = os.getenv("MACVLAN_REGION")
    FED_NAME = os.getenv("FED_NAME", 'mirror')
    GLOBAL_REGION_NAME = "global"
    REGISTRY = "10.0.129.100:60080"
    IMAGE = "{}/alaudaorg/qaimages:helloworld".format(REGISTRY)
    GLOBAL_ALB_NAME = "alb2"
    DEFAULT_NS = "alauda-system"
    DEFAULT_LABEL = "alauda.io"
    VM_IPS = "10.0.128.25"
    VM_USERNAME = "root"
    VM_PASSWORD = "07Apples"
    VM_KEY = ""
    # 必需,模板仓库的chart repo地址信息
    CHARTREPO_URL = "alauda-chart-incubator-new.ace-default.cloud.myalauda.cn/"
    CHARTREPO_TYPE = "http"
    CHARTREPO_USER = "7DO9QoLtDx1g"
    CHARTREPO_PASSWORD = "2wb4E1iydRj7"
    # 模板仓库类型Git
    GITURL_HTTPS = "https://github.com/alauda/captain-test-charts"
    # 模板仓库类型SVN
    SVNURL_HTTPS = "https://svn.apache.org/repos/asf/xml/xang/trunk"
    # devops
    JENKINS_URL = "http://10.0.128.241:32001"
    JENKINS_USERNAME = "admin"
    JENKINS_PASSWORD = "116a236e2354969f00a83fa26c5e5db565"
    JENKINS_TOOL_NAME = 'local-ares-jenkins'
    # nfs服务器的地址
    NFS_IP = '10.0.128.163'
    NFS_PATH = '/exported/path'
    # 可用的存储类名称
    SC_NAME = "cephfs"
    USERNAME = "admin@alauda.io"
    PASSWORD = "password"
    OIDC_ISSUER_URL = os.getenv('OIDC_HOST',
                                'http://keycloak.qa.alauda.cn/auth/realms/master')
    OIDC_SECRET_ID = os.getenv('OIDC_SECRET_ID',
                               '54043ce8-007d-490c-98db-b124acf1ab7b')
elif ENV == "prod":
    pass
else:
    # 测试必填参数
    API_URL = os.getenv("API_URL")
    OVN_REGION = os.getenv("OVN_REGION")
    REGISTRY = os.getenv("REGISTRY")
    USERNAME = os.getenv("USERNAME", "admin@cpaas.io")
    PASSWORD = os.getenv("PASSWORD", "password")

    REGION_NAME = os.getenv("REGION_NAME", OVN_REGION)
    IMAGE = "{}/alaudaorg/qaimages:helloworld".format(REGISTRY)

    # 容器平台必填参数
    # 必需,测试集群的alb名称
    GLOBAL_ALB_NAME = os.getenv("GLOBAL_ALB_NAME", "")
    # 必需,可用的存储类名称
    SC_NAME = os.getenv("SC_NAME", "")
    # 必需,模板仓库的chart repo地址信息
    CHARTREPO_URL = os.getenv("CHARTREPO_URL", "")
    CHARTREPO_TYPE = os.getenv("CHARTREPO_TYPE", "http")
    CHARTREPO_USER = os.getenv("CHARTREPO_USER", "chartmuseum")
    CHARTREPO_PASSWORD = os.getenv("CHARTREPO_PASSWORD", "chartmuseum")
    # 模板仓库类型Git
    GITURL_HTTPS = os.getenv("GITURL_HTTPS", "")
    # 模板仓库类型SVN
    SVNURL_HTTPS = os.getenv("SVNURL_HTTPS", "")

    # 如果需要测试calico 需要传该参数
    CALICO_REGION = os.getenv("CALICO_REGION", "")
    # 如果需要跑macvlan case需要传
    MACVLAN_REGION = os.getenv("MACVLAN_REGION", "")
    # 如果需要跑联邦case需要传联邦集群名称
    FED_NAME = os.getenv("FED_NAME", "")
    # nfs服务器的地址
    NFS_IP = os.getenv('NFS_IP', '10.0.128.163')
    NFS_PATH = os.getenv('NFS_PATH', '/exported/path')

    # alb 应用 智能运维日志测试等用例需要登陆对应集群验证必填
    VM_IPS = os.getenv("VM_IPS", "")
    VM_USERNAME = os.getenv("VM_USERNAME", "root")
    VM_PASSWORD = os.getenv("VM_PASSWORD", "07Apples")
    VM_KEY = os.getenv("VM_KEY", "")

    # devops需要传的参数，如果Jenkins不传则不跑Jenkins相关测试
    JENKINS_URL = os.getenv("JENKINS_URL", "")
    JENKINS_USERNAME = os.getenv("JENKINS_USERNAME", "")
    JENKINS_PASSWORD = os.getenv("JENKINS_PASSWORD", "")
    JENKINS_TOOL_NAME = os.getenv("JENKINS_TOOL_NAME", "")

    # 企业场景关于LDAP第三方用户的用例必填
    OIDC_ISSUER_URL = os.getenv('OIDC_HOST',
                                'http://keycloak.qa.alauda.cn/auth/realms/master')
    OIDC_SECRET_ID = os.getenv('OIDC_SECRET_ID',
                               '54043ce8-007d-490c-98db-b124acf1ab7b')

    # 关于全局的配置
    # global集群名称
    GLOBAL_REGION_NAME = os.getenv("GLOBAL_REGION_NAME", "global")
    # 默认部署的命名空间
    DEFAULT_NS = os.getenv("DEFAULT_NS", "cpaas-system")
    # 默认使用的label
    DEFAULT_LABEL = os.getenv("DEFAULT_LABEL", "cpaas.io")
if ENV in ("int", "staging", "prod") and RESOURCE_PREFIX == "local":
    PROXY = {
        "http": "http://alauda:Ah%23U4TSwnjERBU%40E1KZN8@139.186.17.154:52975",
        "https": "http://alauda:Ah%23U4TSwnjERBU%40E1KZN8@139.186.17.154:52975"
    }
else:
    PROXY = {}
# 模板仓库类型Git
CATALOG_REPO = os.getenv("CATALOG_REPO", "sy-test-2")
# 模板仓库类型SVN
SVNURL_HTTP = os.getenv("SVNURL_HTTP", "http://10.0.128.241:10009/alauda_test/")
SVNURL_HTTP_USER = os.getenv("SVNURL_HTTP_USER", "User_Name-01")
SVNURL_HTTP_PASSWORD = os.getenv("SVNURL_HTTP_PASSWORD", "alauda_Test-!@#")
# devops
HARBOR_URL = os.getenv("HARBOR_URL", "http://10.0.128.241:31104")
HARBOR_USERNAME = os.getenv("HARBOR_USERNAME", "admin")
HARBOR_PASSWORD = os.getenv("HARBOR_PASSWORD", "Harbor12345")
HARBOR_PROJECT = os.getenv("HARBOR_PROJECT", "e2e-automation")
HARBOR_REPO = os.getenv("HARBOR_REPO", "{}/helloworld".format(HARBOR_PROJECT))
GITLAB_URL = os.getenv("GITLAB_URL", "http://10.0.128.241:31101")
GITLAB_USERNAME = os.getenv("GITLAB_USERNAME", "root")
GITLAB_PASSWORD = os.getenv("GITLAB_PASSWORD", "tzrgg4d1zzPxAfpVgzET")
GITLAB_REPO = os.getenv("GITLAB_REPO", 'e2eproject')
GITLAB_TEMPLATE_REPO = os.getenv("GITLAB_TEMPLATE_REPO", "e2e-template")
SONAR_URL = os.getenv("SONAR_URL", "http://10.0.128.241:31342")
SONAR_USERNAME = os.getenv("SONAR_USERNAME", "admin")
SONAR_PASSWORD = os.getenv("SONAR_PASSWORD",
                           "a956fc83f27daf0ca4dfca1c63ab79e95aea8481")
DOCKER_REGISTRY_URL = os.getenv("DOCKER_REGISTRY_URL",
                                "http://10.0.128.241:32677")
JIRA_URL = os.getenv("JIRA_URL", "http://10.0.128.241:32002")
JIRA_USERNAME = os.getenv("JIRA_USERNAME", "admin")
JIRA_PASSWORD = os.getenv("JIRA_PASSWORD", "admin")
NEXUS_URL = os.getenv("NEXUS_URL", "http://10.0.128.241:32010")
NEXUS_USERNAME = os.getenv("NEXUS_USERNAME", "admin")
NEXUS_PASSWORD = os.getenv("NEXUS_PASSWORD", "admin")
# not necessary
TESTCASES = os.getenv("TESTCASES", "")
CASE_TYPE = os.getenv("CASE_TYPE", "BAT")
PROJECT_NAME = os.getenv("PROJECT_NAME", "e2eproject")
K8S_NAMESPACE = os.getenv("K8S_NAMESPACE", "e2enamespace")
CALICO_PROJECT_NAME = os.getenv("CALICO_PROJECT_NAME", "e2eproject-calico")
GLOBAL_INFO_FILE = "./temp_data/global_info{}.json".format(os.getpid())
RECIPIENTS = get_list_from_str(os.getenv("RECIPIENTS", "testing@alauda.io"))
# 腾讯的key
SECRET_ID = "AKID84kBMHwKUP4VggjwBAKFvxlJcgU3frtg"
SECRET_KEY = "aDlNSjBSZGRPdkxXUjZWZ2JHZnFPaGpXMklJa3d0WjA="

# 生成license 内部测试用 网络不通的话 不跑测试
LICENSE_URL = "http://10.0.128.217:8000/license"

# 重试次数
RERUN_TIMES = int(os.getenv("RERUN_TIMES", 0))
# 进程数
THREAD_NUM = os.getenv("THREAD_NUM", "3")
# 日志级别和存储位置
LOG_LEVEL = "INFO"
LOG_PATH = "./report"
# 邮件服务器地址
SMTP = {
    'host': os.getenv('SMTP_HOST', 'smtp.163.com'),
    'port': os.getenv('SMTP_PORT', 465),
    'username': os.getenv('SMTP_USERNAME', '15830736131@163.com'),
    'password': os.getenv('SMTP_PASSWORD', '07Apples'),
    'sender': os.getenv('EMAIL_FROM', '15830736131@163.com'),
    'debug_level': 0,
    'smtp_ssl': True
}
# 资源鉴权
LDAP_HOST = os.getenv('LDAP_HOST', '10.0.64.14:30431')

# 通知发送人
NOTI_USERNAME = get_list_from_str(os.getenv("NOTI_USERNAME", "15830736131@163.com,uiauto555@163.com"))
NOTI_PWD = get_list_from_str(os.getenv("NOTI_PWD", "JBNBOBADSXQXBUXO,123qwe"))


def retry(times=3, sleep_secs=10):
    def retry_deco(func):
        def retry_deco_wrapper(*args, **kwargs):
            count = 0
            success = False
            data = None
            while not success and count < times:
                count += 1
                try:
                    data = func(*args, **kwargs)
                    success = True
                except Exception as e:
                    print("get token error:{}, sleep 10s,times={}".format(e, count))
                    sleep(sleep_secs)
                    if count == times:
                        assert False, "get token info failed"
            return data

        return retry_deco_wrapper

    return retry_deco


@retry()
def get_token(idp_name='local', username=USERNAME, password=PASSWORD):
    url = API_URL + "/console-acp/api/v1/token/login"
    urllib3.disable_warnings()
    headers = {"Referer": "{url}/console-acp".format(url=API_URL)}
    r = requests.get(url, verify=False, timeout=15, proxies=PROXY, headers=headers)
    auth_url = r.json()["auth_url"]
    auth_path = '/'.join(auth_url.split("/")[-2:])
    auth_url = API_URL + '/' + auth_path
    r = requests.get(auth_url, verify=False, proxies=PROXY)
    content = r.text
    req = re.search('req=[a-zA-Z0-9]{25}', content).group(0)[4:]
    url = API_URL + "/dex/auth/{}?req=".format(idp_name) + req
    # generate connectorID
    requests.get(url, verify=False, timeout=10, proxies=PROXY)
    # login acp platform
    params = {"login": username, "encrypt": str(base64.b64encode(password.encode('utf-8')), "utf8")}

    response = requests.post(
        url, params=params, verify=False, timeout=10, proxies=PROXY)
    content = response.history[1].text

    code = re.search('[a-zA-Z0-9]{25}', content).group(0)
    url = API_URL + "/console-acp/api/v1/token/callback?code={}&state=alauda-console".format(
        code)

    r = requests.get(url, verify=False, proxies=PROXY, headers=headers)
    ret = r.json()
    token = ret['id_token']
    token_type = ret['token_type']
    refresh_token = ret['refresh_token']
    auth = "{} {}".format(token_type.capitalize(), token)
    return auth, refresh_token


# 发送请求的headers
headers = {"Content-Type": "application/json", "Authorization": get_token()[0]}


def get_audit():
    payload = {"page": 1, "page_size": 20, "user_name": USERNAME}
    current_time = int(time.time())
    times = {"start_time": current_time - 1800, "end_time": current_time}
    payload.update(times)
    url = API_URL + "/v1/kubernetes-audits"
    urllib3.disable_warnings()
    result = requests.get(
        url, params=payload, proxies=PROXY, headers=headers, verify=False)
    if result.status_code != 200 or result.json().get('total_items') == 0:
        return True
    return False


# 获取是否有审计
# AUDIT_UNABLED = get_audit()
AUDIT_UNABLED = True


def get_featuregate_status(region=None, name=None):
    if region:
        url = API_URL + '/fg/v1/{}/featuregates/{}'.format(region, name)
    else:
        url = API_URL + '/fg/v1/featuregates/{}'.format(name)
    response = requests.get(url, proxies=PROXY, headers=headers, verify=False)
    return response.json(
    )['spec']['enabled'] if response.status_code == 200 else False
