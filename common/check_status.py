# coding=utf-8
import string
import json
import os
from bs4 import BeautifulSoup
from common import settings
from common.api_requests import AlaudaRequest


class Checker(object):
    def __init__(self):
        super(Checker, self).__init__()
        self.req = AlaudaRequest()
        self.harbor_url = settings.HARBOR_URL
        self.jenkins_url = settings.JENKINS_URL
        self.gitlab_url = settings.GITLAB_URL
        self.ldap_url = "ldapadmin.qa.alauda.cn"
        self.smtp_server = settings.SMTP['host']
        self.smtp_port = settings.SMTP['port']
        self.smtp_ssl = settings.SMTP['smtp_ssl']

    def _get_harbor_systeminfo(self):
        self.req.endpoint = self.harbor_url
        return self.req.send(method='GET', path="api/systeminfo", timeout=5)

    def _get_jenkins_login(self):
        self.req.endpoint = self.jenkins_url
        return self.req.send(method='GET', path="login", auth=(settings.JENKINS_USERNAME, settings.JENKINS_PASSWORD),
                             timeout=5)

    def _get_gitlab_sign_in(self):
        self.req.endpoint = self.gitlab_url
        return self.req.send(method='GET', path="users/sign_in", timeout=5)

    def _get_ldap_login(self):
        self.req.endpoint = "http://{}".format(self.ldap_url.rsplit("/", 1)[-1])
        return self.req.send(method='GET', path="", timeout=5)

    @staticmethod
    def _str_to_bool(s):
        return s.lower() == "true"

    def harbor_available(self):
        if os.getenv("HARBOR_AVAILABLE"):
            return self._str_to_bool(os.getenv("HARBOR_AVAILABLE"))
        rsp = self._get_harbor_systeminfo()
        if rsp:
            if rsp.status_code == 200 and \
                    (rsp.json().get("registry_url")) == self.harbor_url.rsplit("/", 1)[-1]:
                os.environ["HARBOR_AVAILABLE"] = "True"
                return True
            else:
                os.environ["HARBOR_AVAILABLE"] = "False"
                return False
        else:
            os.environ["HARBOR_AVAILABLE"] = "False"
            return False

    def jenkins_available(self):
        if os.getenv("JENKINS_AVAILABLE"):
            return self._str_to_bool(os.getenv("JENKINS_AVAILABLE"))
        rsp = self._get_jenkins_login()
        if rsp:
            bs = BeautifulSoup(rsp.text, "html.parser")
            if self._get_jenkins_login().status_code == 200 and \
                    bs.title.string == "Sign in [Jenkins]":
                os.environ["JENKINS_AVAILABLE"] = "True"
                return True
            else:
                os.environ["JENKINS_AVAILABLE"] = "False"
                return False
        else:
            os.environ["JENKINS_AVAILABLE"] = "False"
            return False

    def gitlab_available(self):
        if os.getenv("GITLAB_AVAILABLE"):
            return self._str_to_bool(os.getenv("GITLAB_AVAILABLE"))
        rsp = self._get_gitlab_sign_in()
        if rsp:
            bs = BeautifulSoup(rsp.text, "html.parser")
            if self._get_gitlab_sign_in().status_code == 200 and \
                    bs.head.title.string == "Sign in · GitLab":
                os.environ["GITLAB_AVAILABLE"] = "True"
                return True
            else:
                os.environ["GITLAB_AVAILABLE"] = "False"
                return False
        os.environ["GITLAB_AVAILABLE"] = "False"
        return False

    def ldap_available(self):
        if os.getenv("LDAP_AVAILABLE"):
            return self._str_to_bool(os.getenv("LDAP_AVAILABLE"))
        rsp = self._get_ldap_login()
        if rsp.status_code == 200:
            os.environ["LDAP_AVAILABLE"] = "True"
            return True
        else:
            os.environ["LDAP_AVAILABLE"] = "False"
            return False

    def _del_notify(self, name):
        url = 'apis/aiops.alauda.io/v1beta1/notificationservers/{}'.format(name)
        return self.req.send(method='DELETE', path=url)

    def smtp_availabel(self):
        if os.getenv("SMTP_AVAILABLE"):
            return self._str_to_bool(os.getenv("SMTP_AVAILABLE"))
        from random import randint
        notify_name = 'check-available{}'.format(randint(0, 1000))
        self._del_notify(notify_name)
        data = {
            "apiVersion": "aiops.alauda.io/v1beta1",
            "kind": "NotificationServer",
            "metadata": {
                "labels": {
                    "${label}/type": "email"
                },
                "annotations": {
                    "${label}/description": "",
                    "${label}/display-name": notify_name
                },
                "name": notify_name
            },
            "spec": {
                "email": {
                    "host": self.smtp_server,
                    "port": self.smtp_port
                }
            }
        }
        t = string.Template(json.dumps(data))
        n_data = t.safe_substitute({'label': settings.DEFAULT_LABEL, 'name': notify_name})
        available = False
        try:
            ret = self.req.send(method='POST', path='apis/aiops.alauda.io/v1beta1/notificationservers', data=n_data, timeout=10)
            if ret.status_code == 201:
                available = True
        except Exception:
            pass

        self._del_notify(notify_name)
        os.environ["SMTP_AVAILABLE"] = str(available)
        return available


checker = Checker()
