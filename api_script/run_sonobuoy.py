import os
import sys

sonobuoy_image = sys.argv[1]
case_type = sys.argv[2]
api_image = sys.argv[2]
branch_name = sys.argv[3]
jenkins_url = sys.argv[4]
date = sys.argv[5]
print(sys.argv)
if len(sys.argv) > 6:
    case_type = sys.argv[6]
else:
    case_type = ""
test_name = case_type.replace("_", "-").lower()
if case_type:
    run_type = case_type + " and BAT"
    print("run case type is {}".format(run_type))
    cmd = "sonobuoy gen plugin --name {test_name} --image {api_image} -e CASE_TYPE='{run_type}'" \
          " -e RESOURCE_PREFIX={branch_name} -e JENKINS_LINK={jenkins_url} > tmp.yaml".format(test_name=test_name,
                                                                                              api_image=api_image,
                                                                                              run_type=run_type,
                                                                                              branch_name=branch_name,
                                                                                              jenkins_url=jenkins_url)
    print(cmd)
    os.system(cmd)
    os.system("echo '  imagePullPolicy: Always' >> tmp.yaml")
    os.system("cat tmp.yaml|tr -s '\n' > {case_type}.yaml".format(case_type=case_type))
    os.system("cat {case_type}.yaml".format(case_type=case_type))
    cmd = "sonobuoy  run --sonobuoy-image {sonobuoy_image} --plugin {case_type}.yaml" \
          " --context=automation --rbac=Enable -n ares-{test_name}-{date} --wait".format(sonobuoy_image=sonobuoy_image,
                                                                                         case_type=case_type,
                                                                                         test_name=test_name,
                                                                                         date=date)
    print(cmd)
    os.system(cmd)
else:
    for run_type in ["acp_containerplatform", "acp_infra", "platform_api", "devops_api", "acp_operation"]:
        test_name = run_type.replace("_", "-")
        cmd = "sonobuoy gen plugin --name {test_name} --image {api_image} -e CASE_TYPE='{run_type}'" \
              " -e RESOURCE_PREFIX={branch_name} -e JENKINS_LINK={jenkins_url} > tmp.yaml".format(test_name=test_name,
                                                                                                  api_image=api_image,
                                                                                                  run_type=run_type,
                                                                                                  branch_name=branch_name,
                                                                                                  jenkins_url=jenkins_url)
        print(cmd)
        os.system(cmd)
        os.system("echo '  imagePullPolicy: Always' >> tmp.yaml")
        os.system("cat tmp.yaml|tr -s '\n' > {case_type}.yaml".format(case_type=run_type))
        os.system("cat {case_type}.yaml".format(case_type=run_type))

    cmd = "sonobuoy  run --sonobuoy-image {sonobuoy_image} --plugin acp_containerplatform.yaml" \
          " --plugin acp_infra.yaml --plugin platform_api.yaml --plugin devops_api.yaml" \
          " --plugin acp_operation.yaml --context=automation --rbac=Enable -n ares-{test_name}-{date} --wait".format(
        sonobuoy_image=sonobuoy_image,
        test_name=case_type, date=date)
    print(cmd)
    os.system(cmd)
