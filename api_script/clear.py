# -*- coding:utf-8 -*-
import sys
import time
# 如果是python3 subprocess
# from subprocess import getoutput
from commands import getoutput
import json

# 传一个ns的参数
namespace = sys.argv[1]


class Clear(object):
    def __init__(self, resource_type):
        self.resource_type = resource_type

    # 获取超过12小时对于类型的资源名称
    def get_computes(self):
        computes = []
        get_cmd = "kubectl get {type} -n {ns} -o json".format(type=self.resource_type, ns=namespace)
        items = json.loads(getoutput(get_cmd))["items"]
        now_time = time.time()
        for item in items:
            create_time = int(time.mktime(time.strptime(item["metadata"]["creationTimestamp"], "%Y-%m-%dT%H:%M:%SZ")))
            if now_time - create_time > 3600 * 12:
                computes.append(item["metadata"]["name"])
        return " ".join(computes)

    # 删除对于类型的资源
    def delete_compute(self, computes):
        if computes:
            delete_cmd = "kubectl delete {type} -n {ns} {names}".format(type=self.resource_type, ns=namespace,
                                                                        names=computes)
            delete_output = getoutput(cmd=delete_cmd)
            # print(delete_cmd)
            print(delete_output)


if __name__ == '__main__':
    # 目前删除的资源类型
    resouece_types = ["deployment", "statefulset", "daemonset", "rs", "pod"]
    for resouece_type in resouece_types:
        client = Clear(resource_type=resouece_type)
        names = client.get_computes()
        client.delete_compute(computes=names)
