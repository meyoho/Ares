# -*- coding:utf-8 -*-
import os
import sys
import re
import requests
import tarfile
import json
import shutil

wechat_webhook = "https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=17e8613b-d7e5-42b8-b59a-73a2b6bda88e"

if len(sys.argv) == 2:
    plugins = ["acp-containerplatform", "platform-api", "acp-infra", "devops-api", "acp-operation"]
    tar_file = sys.argv[1]
else:
    plugins = [sys.argv[1]]
    tar_file = sys.argv[2]


class Check_Result():
    def __init__(self):
        os.system("rm -rf /tmp/api_report")
        self.plugins = plugins
        self.failed_cases = []
        self.third_failed_cases = []
        self.flag = True
        # 添加由于第三方问题引发失败的message ,这部分匹配逻辑后续需要补充
        self.fail_data = {
            "msg": [
                'refuse',
                'E       assert 200 == 503',
                'Internal error occurred: Invalid username or password'
            ]
        }

    def untar_file(self, tar_file, des_folder):
        tar = tarfile.open(tar_file)
        names = tar.getnames()
        for name in names:
            tar.extract(name, des_folder)
        tar.close()

    def get_result(self):
        shutil.copyfile(tar_file, "api_report/api_report.tar")

        self.analyse()

        self.third_case_send_message()

        self.failed_case_send_message()

        if len(self.plugins):
            self.flag = False
            print("有部分测试:({})没有执行,应该是登陆失败了，具体信息下载tar包查看具体日志".format(plugins))
        print("测试结果的状态:{}".format(self.flag))
        return self.flag

    # 通过日志判断是不是第三方组件导致错误的用例
    def is_third_failed_case(self, extra, detail):
        for msg in self.fail_data['msg']:
            regex = re.compile(msg)
            if bool(regex.search(extra)):
                # 如果是setup出错，具体信息只有其中一个case有
                # 所以收集其中一个然后获取其中的assert信息，如果是相同的信息则认为都是第三方组件造成的
                assert_error = re.findall(r"AssertionError:.{10}", detail)
                if len(assert_error) and assert_error[0] not in self.fail_data["msg"]:
                    self.fail_data["msg"].append(assert_error[0])
                return True
        return False

    def send_message(self, content, mentioned_list=[]):
        data = {
            "msgtype": "text",
            "text": {
                "content": content,
                "mentioned_mobile_list": mentioned_list
            }
        }
        print(data)
        requests.post(wechat_webhook, json=data)

    # 失败的用例分类发送企业微信消息
    def failed_case_send_message(self):
        infra_failed_case = 0
        containerplatform_failed_case = 0
        devops_failed_case = 0
        operation_failed_case = 0
        platform_failed_case = 0
        content = ""
        mentioned_list = []
        for failed_case in self.failed_cases:
            failed_type = failed_case.split("/")[1]
            if failed_type == "cluster":
                infra_failed_case += 1
            elif failed_type == "containerplatform":
                containerplatform_failed_case += 1
            elif failed_type == "devops":
                devops_failed_case += 1
            elif failed_type == "operation":
                operation_failed_case += 1
            elif failed_type == "platform":
                platform_failed_case += 1
        if infra_failed_case > 0:
            content += "基础设施的测试失败{num}个用例\n".format(num=infra_failed_case)
            mentioned_list.append("13752362332")
        if containerplatform_failed_case > 0:
            content += "容器平台测试失败{num}个用例\n".format(num=containerplatform_failed_case)
            mentioned_list.append("15061121196")
        if devops_failed_case > 0:
            content += "devops测试失败{num}个用例\n".format(num=devops_failed_case)
            mentioned_list.append("18771022056")
        if operation_failed_case > 0:
            content += "智能运维测试失败{num}个用例\n".format(num=operation_failed_case)
            mentioned_list.append("13752362332")
        if platform_failed_case > 0:
            content += "企业场景测试失败{num}个用例\n".format(num=platform_failed_case)
            mentioned_list.append("18627894580")
        mentioned_list = list(set(mentioned_list))
        print(mentioned_list)
        print(os.getenv("JENKINS_LINK"))
        if content and os.getenv("JENKINS_LINK"):
            content += "这是接口测试，请相关同事查看流水线:{link}".format(link=os.getenv("JENKINS_LINK"))
            self.send_message(content, mentioned_list)

    # 第三方组件导致失败的用例发送企业微信消息
    def third_case_send_message(self):
        if len(self.third_failed_cases) and os.getenv("JENKINS_LINK"):
            content = "因为第三方组件导致失败的case文件:{}".format(list(set(self.third_failed_cases)))
            content += "这是接口测试，请相关同事查看流水线:{link}".format(link=os.getenv("JENKINS_LINK"))
            self.send_message(content)

    # 分析测试报告
    def analyse(self):
        if os.path.exists("/tmp/api_report/plugins"):
            for api_dir in os.listdir("/tmp/api_report/plugins"):
                print(plugins)
                print(api_dir)
                self.plugins.remove(api_dir)
                # 将测试生成的xml文件拷贝到对应Jenkinsfile需要的路径下上传到jenkins展示测试结果
                shutil.copyfile("/tmp/api_report/plugins/{dir}/results/global/pytest.xml".format(dir=api_dir),
                                "api_report/{dir}-pytest.xml".format(dir=api_dir))
                file_name = "/tmp/api_report/plugins/{dir}/results/global/pytest.json".format(dir=api_dir)
                with open(file_name, 'r', encoding='utf-8') as f:
                    details = json.load(f)
                    if details["failed_num"] != 0:
                        print("{}总共失败了:{}个case".format(api_dir, details["failed_num"]))
                        case_details = details["case_details"]
                        for ind, case_detail in enumerate(case_details):
                            if case_detail["case_flag"] in ("Failed", "Error"):
                                print("第{}个失败case详情:{}".format(ind + 1, case_detail["case_detail"]))
                                extra = case_detail["extra"]
                                # print(extra)
                                if self.is_third_failed_case(extra, case_detail["case_detail"]):
                                    self.third_failed_cases.append(case_detail["case_location"].split("::")[0])
                                else:
                                    self.flag = False
                                    self.failed_cases.append(case_detail["case_location"])


if __name__ == '__main__':
    check_result = Check_Result()
    check_result.untar_file(tar_file, des_folder="/tmp/api_report")

    if check_result.get_result() is False:
        exit(1)
